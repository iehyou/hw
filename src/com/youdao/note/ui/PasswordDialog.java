package com.youdao.note.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public class PasswordDialog extends Dialog
{
  private LayoutInflater mInflater = null;
  private ViewGroup mLayout = null;

  public PasswordDialog(Context paramContext)
  {
    super(paramContext);
    this.mInflater = LayoutInflater.from(paramContext);
    this.mLayout = ((ViewGroup)this.mInflater.inflate(2130903100, null));
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(this.mLayout);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.PasswordDialog
 * JD-Core Version:    0.5.4
 */