package com.youdao.note.ui;

public abstract interface YNoteWebViewCallback
{
  public abstract void onWebViewLoadFailed(String paramString);

  public abstract void onWebViewLoadFinished();

  public abstract void onWebViewLoadStarted();

  public abstract boolean overrideUrlLoading(String paramString);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.YNoteWebViewCallback
 * JD-Core Version:    0.5.4
 */