package com.youdao.note.ui;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.youdao.note.utils.L;

public class YNoteWebViewClient extends WebViewClient
{
  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    L.d(this, "load url " + paramString);
    return true;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.YNoteWebViewClient
 * JD-Core Version:    0.5.4
 */