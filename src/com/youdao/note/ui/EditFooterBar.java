package com.youdao.note.ui;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.AudioResource;
import com.youdao.note.data.resource.AudioResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.ui.audio.AudioPlayerBar;
import com.youdao.note.ui.audio.AudioPlayerBar.AudioPlayListener;
import com.youdao.note.ui.audio.AudioRecordBar;
import com.youdao.note.ui.audio.AudioRecordBar.AudioRecordListener;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import java.io.File;
import java.io.IOException;

public class EditFooterBar extends FrameLayout
  implements View.OnClickListener, AudioRecordBar.AudioRecordListener, AudioPlayerBar.AudioPlayListener
{
  private AudioResourceMeta audioMeta;
  private View mActionBar;
  private EditActionListener mActionListener;
  private Context mContext;
  private AudioPlayerBar mPlayerBar;
  private AudioRecordBar mRecorderBar;

  public EditFooterBar(Context paramContext)
  {
    this(paramContext, null);
  }

  public EditFooterBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet, 0);
    this.mContext = paramContext;
    LayoutInflater.from(paramContext).inflate(2130903052, this, true);
    this.mActionBar = findViewById(2131165226);
    this.mRecorderBar = ((AudioRecordBar)findViewById(2131165228));
    this.mPlayerBar = ((AudioPlayerBar)findViewById(2131165227));
    init();
  }

  private void init()
  {
    this.mActionBar.findViewById(2131165222).setOnClickListener(this);
    this.mActionBar.findViewById(2131165223).setOnClickListener(this);
    this.mActionBar.findViewById(2131165225).setOnClickListener(this);
    this.mActionBar.findViewById(2131165224).setOnClickListener(this);
    this.mActionBar.findViewById(2131165221).setOnClickListener(this);
    this.mRecorderBar.setAudioRecordListener(this);
    this.mPlayerBar.setAudioPlayListener(this);
  }

  private void nofityError(int paramInt)
  {
    if (this.mActionListener == null)
      return;
    this.mActionListener.onError(paramInt);
  }

  public void destroy()
  {
    this.mPlayerBar.stop();
    if (!isRecording())
      return;
    this.mRecorderBar.stopRecord(false, 3);
  }

  public boolean isRecording()
  {
    return this.mRecorderBar.getVisibility() == 0;
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165222)
      if (this.mActionListener != null)
        this.mActionListener.onTakePhoto();
    do
    {
      do
      {
        do
        {
          do
          {
            return;
            if (paramView.getId() != 2131165223)
              break label52;
          }
          while (this.mActionListener == null);
          this.mActionListener.onPickPhoto();
          return;
          label52: if (paramView.getId() != 2131165225)
            break label78;
        }
        while (this.mActionListener == null);
        this.mActionListener.onDoodle();
        return;
        label78: if (paramView.getId() != 2131165224)
          break label104;
      }
      while (this.mActionListener == null);
      this.mActionListener.onHandwrite();
      label104: return;
    }
    while (paramView.getId() != 2131165221);
    startRecord();
  }

  public void onClose()
  {
    this.mRecorderBar.setVisibility(8);
    this.mPlayerBar.setVisibility(8);
    UIUtilities.makeViewVisiable(this.mActionBar);
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    return false;
  }

  public void onRecordFinished(File paramFile, int paramInt)
  {
    this.mRecorderBar.setVisibility(8);
    if ((paramFile != null) && (paramFile.exists()))
    {
      if (this.mActionListener != null)
      {
        if (this.audioMeta != null)
          this.audioMeta.setLength(paramFile.length());
        this.mActionListener.onRecordFinished(this.audioMeta);
      }
      this.audioMeta = null;
    }
    if (1 == paramInt);
    try
    {
      this.mPlayerBar.play(paramFile);
      this.mPlayerBar.pause();
      UIUtilities.makeViewVisiable(this.mPlayerBar);
      return;
    }
    catch (Exception localException)
    {
      L.e(this, "Failed to play audio.", localException);
      nofityError(-2);
      if (2 == paramInt)
      {
        startRecord();
        return;
      }
      onClose();
    }
  }

  public void onSeekComplete(MediaPlayer paramMediaPlayer)
  {
  }

  public void pause()
  {
    this.mPlayerBar.pause();
  }

  public void play(String paramString)
    throws IllegalStateException, IOException
  {
    this.mPlayerBar.play(paramString);
    this.mActionBar.setVisibility(8);
    this.mRecorderBar.setVisibility(8);
    UIUtilities.makeViewVisiable(this.mPlayerBar);
  }

  public void recoverPos()
  {
  }

  public void setActionListener(EditActionListener paramEditActionListener)
  {
    this.mActionListener = paramEditActionListener;
  }

  public void startRecord()
  {
    if (YNoteApplication.getInstance().isLoadRecortLibSuccess())
    {
      if (YNoteApplication.getInstance().isRecording())
      {
        UIUtilities.showToast(getContext(), 2131361866);
        return;
      }
      this.mRecorderBar.setVisibility(8);
      this.mActionBar.setVisibility(8);
      UIUtilities.makeViewVisiable(this.mRecorderBar);
      try
      {
        if (this.audioMeta == null)
          this.audioMeta = ((AudioResourceMeta)ResourceUtils.genEmptyAudioResource().getMeta());
        String str = YNoteApplication.getInstance().getDataSource().getResourcePath(this.audioMeta);
        this.mRecorderBar.init(str);
        return;
      }
      catch (IOException localIOException)
      {
        nofityError(-1);
        return;
      }
    }
    Toast.makeText(this.mContext, 2131362037, 0).show();
  }

  public static abstract interface EditActionListener
  {
    public static final int ERROR_AUDIO_PLAY = -2;
    public static final int ERROR_RECORD_INIT = -1;

    public abstract void onDoodle();

    public abstract void onError(int paramInt);

    public abstract void onHandwrite();

    public abstract void onPickPhoto();

    public abstract void onRecordFinished(AudioResourceMeta paramAudioResourceMeta);

    public abstract void onTakePhoto();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.EditFooterBar
 * JD-Core Version:    0.5.4
 */