package com.youdao.note.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class YNoteWebView extends WebView
{
  public YNoteWebView(Context paramContext)
  {
    super(paramContext);
    init();
  }

  public YNoteWebView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  public YNoteWebView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }

  private void init()
  {
    WebSettings localWebSettings = getSettings();
    localWebSettings.setBuiltInZoomControls(true);
    localWebSettings.setJavaScriptEnabled(true);
    localWebSettings.setBlockNetworkImage(false);
    localWebSettings.setLoadsImagesAutomatically(true);
    localWebSettings.setCacheMode(2);
    localWebSettings.setSavePassword(false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.YNoteWebView
 * JD-Core Version:    0.5.4
 */