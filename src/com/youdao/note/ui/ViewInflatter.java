package com.youdao.note.ui;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.youdao.note.data.NoteMeta;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ViewInflatter
{
  public static SimpleDateFormat labelFormator = new SimpleDateFormat("yyyy-MM");

  public static View inflateSectionLabel(Activity paramActivity, NoteMeta paramNoteMeta)
  {
    View localView = paramActivity.getLayoutInflater().inflate(2130903047, null);
    if (paramNoteMeta != null)
      ((TextView)localView.findViewById(2131165207)).setText(labelFormator.format(new Date(paramNoteMeta.getModifyTime())));
    return localView;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.ViewInflatter
 * JD-Core Version:    0.5.4
 */