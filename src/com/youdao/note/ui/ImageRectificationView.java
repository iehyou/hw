package com.youdao.note.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import com.youdao.note.activity.ActivityConsts;
import com.youdao.note.activity.resource.ImageRectificationActivity;
import com.youdao.note.tool.img.ImageProcess;
import com.youdao.note.utils.L;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.utils.Converters;

public class ImageRectificationView extends View
  implements ActivityConsts
{
  private static final int ANIMATION_FRAMES = 7;
  private static final float ANIMATION_SHRINK_RATIO = 0.8F;
  private static final int DASHED_LINE_SEGMENTS = 16;
  private static final float DASHED_LINE_SPACES_RATIO = 0.18F;
  public static final int EVENT_AUTO_DETECT = 1;
  public static final int EVENT_CANCEL_TASK = 5;
  public static final int EVENT_COMPLETE = 4;
  public static final int EVENT_ROTATE_ANTICLOCKWISE = 3;
  public static final int EVENT_ROTATE_CLOCKWISE = 2;
  private static final float SCALE_SIZE = 0.8F;
  private static final float WIDTH_BORDER = 1.5F;
  private static final float WIDTH_LINE = 1.0F;
  private static final int crossLength = 20;
  private PointF CENTRAL_POINT;
  private int SCREEN_HEIGHT;
  private int SCREEN_WIDTH;
  private float areaX;
  private float areaY;
  private boolean autoDetect = true;
  private AutoDetectTask autoDetectTask;
  private Handler convertAutoDetectHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      Context localContext = ImageRectificationView.this.getContext();
      int i;
      label21: boolean bool;
      label49: ImageView localImageView;
      if (ImageRectificationView.this.autoDetect)
      {
        i = 2131361990;
        Toast.makeText(localContext, i, 0).show();
        ImageRectificationView localImageRectificationView = ImageRectificationView.this;
        if (ImageRectificationView.this.autoDetect)
          break label105;
        bool = true;
        ImageRectificationView.access$002(localImageRectificationView, bool);
        localImageView = (ImageView)((Activity)ImageRectificationView.this.getContext()).findViewById(2131165257);
        if (!ImageRectificationView.this.autoDetect)
          break label111;
      }
      for (int j = 2130837625; ; j = 2130837624)
      {
        localImageView.setImageResource(j);
        return;
        i = 2131361991;
        break label21:
        label105: bool = false;
        label111: break label49:
      }
    }
  };
  boolean firstDraw = true;
  private int imageRotateDegree = 0;
  private float imageScale;
  private PointF[] imageVertices = new PointF[4];
  private Bitmap mImage;
  private Bitmap mImageSrc = null;
  private Matrix mMatrix = new Matrix();
  private Paint mPaint = new Paint();
  public Handler menuHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        return;
      case 1:
        ImageRectificationView.access$1702(ImageRectificationView.this, new ImageRectificationView.AutoDetectTask(ImageRectificationView.this, null));
        ImageRectificationView.this.autoDetectTask.execute(new Void[0]);
        return;
      case 2:
        ImageRectificationView.this.rotate(true);
        return;
      case 3:
        ImageRectificationView.this.rotate(false);
        return;
      case 4:
        ImageRectificationView.access$2002(ImageRectificationView.this, new ImageRectificationView.RectificationTask(ImageRectificationView.this, null));
        ImageRectificationView.this.rectificationTask.execute(new Void[0]);
        return;
      case 5:
      }
      L.d(this, "Event cancel task");
      if (ImageRectificationView.this.autoDetectTask != null)
      {
        ImageRectificationView.this.autoDetectTask.cancel(true);
        ImageRectificationView.access$1702(ImageRectificationView.this, null);
      }
      if (ImageRectificationView.this.rectificationTask != null)
      {
        ImageRectificationView.this.rectificationTask.cancel(true);
        ImageRectificationView.access$2002(ImageRectificationView.this, null);
      }
      try
      {
        ((Activity)ImageRectificationView.this.getContext()).dismissDialog(0);
        try
        {
          ((Activity)ImageRectificationView.this.getContext()).dismissDialog(1);
          return;
        }
        catch (IllegalArgumentException localIllegalArgumentException2)
        {
          return;
        }
      }
      catch (IllegalArgumentException localIllegalArgumentException1)
      {
      }
    }
  };
  private boolean moving = false;
  private int movingPointIndex = -1;
  private PointF movingPointInitial = new PointF();
  private PointF[] points = new PointF[4];
  private List<PointF> pointsAutoDetect = null;
  private PointF[] pointsBakForDrag = new PointF[4];
  private RectificationTask rectificationTask;
  private Handler toastHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      String str = paramMessage.getData().get("message").toString();
      Toast.makeText(ImageRectificationView.this.getContext(), str, 0).show();
    }
  };
  private PointF touchDownPoint = new PointF();

  public ImageRectificationView(Context paramContext)
  {
    super(paramContext);
  }

  public ImageRectificationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public ImageRectificationView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private PointF[] convertPoints(boolean paramBoolean, PointF[] paramArrayOfPointF)
  {
    PointF[] arrayOfPointF;
    if ((paramArrayOfPointF == null) || (paramArrayOfPointF.length != 4))
      arrayOfPointF = paramArrayOfPointF;
    label26: label308: int i;
    do
    {
      int k;
      do
      {
        return arrayOfPointF;
        arrayOfPointF = new PointF[4];
        if (!paramBoolean)
          break label308;
        k = 0;
      }
      while (k >= 4);
      float f3 = paramArrayOfPointF[k].x / this.mImageSrc.getWidth();
      float f4 = paramArrayOfPointF[k].y / this.mImageSrc.getHeight();
      int l = this.imageRotateDegree / 90;
      PointF localPointF = null;
      switch (l)
      {
      default:
      case 0:
      case 1:
      case 2:
      case 3:
      }
      while (true)
      {
        arrayOfPointF[k] = localPointF;
        ++k;
        break label26:
        localPointF = new PointF(this.areaX + f3 * this.mImage.getWidth(), this.areaY + f4 * this.mImage.getHeight());
        continue;
        localPointF = new PointF(this.areaX + this.mImage.getWidth() * (1.0F - f4), this.areaY + f3 * this.mImage.getHeight());
        continue;
        localPointF = new PointF(this.areaX + this.mImage.getWidth() * (1.0F - f3), this.areaY + this.mImage.getHeight() * (1.0F - f4));
        continue;
        localPointF = new PointF(this.areaX + f4 * this.mImage.getWidth(), this.areaY + this.mImage.getHeight() * (1.0F - f3));
      }
      label311: i = 0;
    }
    while (i >= 4);
    int j = this.imageRotateDegree / 90;
    float f1 = 0.0F;
    float f2 = 0.0F;
    switch (j)
    {
    default:
    case 0:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      arrayOfPointF[i] = new PointF(f1 * this.mImageSrc.getWidth(), f2 * this.mImageSrc.getHeight());
      ++i;
      break label311:
      f1 = (paramArrayOfPointF[i].x - this.areaX) / this.mImage.getWidth();
      f2 = (paramArrayOfPointF[i].y - this.areaY) / this.mImage.getHeight();
      continue;
      f1 = (paramArrayOfPointF[i].y - this.areaY) / this.mImage.getHeight();
      f2 = (this.areaX + this.mImage.getWidth() - paramArrayOfPointF[i].x) / this.mImage.getWidth();
      continue;
      f1 = (this.areaX + this.mImage.getWidth() - paramArrayOfPointF[i].x) / this.mImage.getWidth();
      f2 = (this.areaY + this.mImage.getHeight() - paramArrayOfPointF[i].y) / this.mImage.getHeight();
      continue;
      f1 = (this.areaY + this.mImage.getHeight() - paramArrayOfPointF[i].y) / this.mImage.getHeight();
      f2 = (paramArrayOfPointF[i].x - this.areaX) / this.mImage.getWidth();
    }
  }

  private PointF determineBoundaryPoint(PointF paramPointF1, PointF paramPointF2)
  {
    if (((isInPicture(paramPointF1)) && (isInPicture(paramPointF2))) || ((!isInPicture(paramPointF1)) && (!isInPicture(paramPointF2))))
    {
      localObject = null;
      return localObject;
    }
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < this.imageVertices.length; ++i)
    {
      PointF localPointF3 = ImageProcess.getLineSegmentIntersectionPoint(paramPointF1, paramPointF2, this.imageVertices[i], this.imageVertices[((i + 1) % this.imageVertices.length)]);
      if (localPointF3 == null)
        continue;
      localArrayList.add(localPointF3);
    }
    Object localObject = null;
    Iterator localIterator = localArrayList.iterator();
    label119: PointF localPointF1;
    while (true)
    {
      if (localIterator.hasNext());
      localPointF1 = (PointF)localIterator.next();
      if (localObject != null)
        break;
      localObject = localPointF1;
    }
    if (isInPicture(paramPointF1));
    for (PointF localPointF2 = paramPointF2; ; localPointF2 = paramPointF1)
    {
      float f = ImageProcess.getDistance(localObject, localPointF2);
      if (ImageProcess.getDistance(localPointF1, localPointF2) < f);
      localObject = localPointF1;
      break label119:
    }
  }

  private float determineScale(int paramInt1, int paramInt2)
  {
    float f1 = 1.0F;
    float f2 = 1.0F;
    if (paramInt1 > 0.8F * this.SCREEN_WIDTH)
      f1 = 0.8F * this.SCREEN_WIDTH / paramInt1;
    if (paramInt2 > 0.8F * this.SCREEN_HEIGHT)
      f2 = 0.8F * this.SCREEN_HEIGHT / paramInt2;
    return Math.min(f1, f2);
  }

  private void drawCross(PointF paramPointF, Canvas paramCanvas)
  {
    Paint localPaint = new Paint();
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setColor(-65536);
    localPaint.setStrokeWidth(1.5F);
    paramCanvas.drawLine(paramPointF.x - 10.0F, paramPointF.y, 10.0F + paramPointF.x, paramPointF.y, localPaint);
    paramCanvas.drawLine(paramPointF.x, paramPointF.y - 10.0F, paramPointF.x, 10.0F + paramPointF.y, localPaint);
  }

  private void drawDashedLine(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, Canvas paramCanvas)
  {
    Paint localPaint = new Paint();
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setStrokeWidth(1.0F);
    localPaint.setColor(-16776961);
    for (int i = 0; i < 16; ++i)
    {
      paramCanvas.drawLine(paramFloat1 + (paramFloat3 - paramFloat1) * i / 16.0F, paramFloat2 + (paramFloat4 - paramFloat2) * i / 16.0F, paramFloat1 + (paramFloat3 - paramFloat1) * (i + 1 - 0.36F) / 16.0F, paramFloat2 + (paramFloat4 - paramFloat2) * (i + 1 - 0.36F) / 16.0F, localPaint);
      paramCanvas.drawPoint(paramFloat1 + (paramFloat3 - paramFloat1) * (i + 1 - 0.18F) / 16.0F, paramFloat2 + (paramFloat4 - paramFloat2) * (i + 1 - 0.18F) / 16.0F, localPaint);
    }
  }

  private void drawShadow(Canvas paramCanvas, PointF[] paramArrayOfPointF)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    if (i < paramArrayOfPointF.length)
    {
      label11: StringBuilder localStringBuilder = new StringBuilder().append("p[").append(i).append("]:(").append(paramArrayOfPointF[i].x).append(",").append(paramArrayOfPointF[i].y).append(")");
      if (i == -1 + paramArrayOfPointF.length);
      for (String str = ""; ; str = ";")
      {
        localStringBuffer.append(str);
        ++i;
        break label11:
      }
    }
    Log.d("DRAWSHADOW", localStringBuffer.toString());
    Path localPath = new Path();
    if ((paramArrayOfPointF == null) || (paramArrayOfPointF.length < 3))
      return;
    localPath.moveTo(paramArrayOfPointF[0].x, paramArrayOfPointF[0].y);
    for (int j = 1; j < paramArrayOfPointF.length; ++j)
      localPath.lineTo(paramArrayOfPointF[j].x, paramArrayOfPointF[j].y);
    localPath.close();
    Paint localPaint = new Paint();
    localPaint.setStyle(Paint.Style.FILL);
    localPaint.setARGB(150, 0, 0, 0);
    paramCanvas.drawPath(localPath, localPaint);
  }

  private void drawShadows(Canvas paramCanvas)
  {
    StringBuffer localStringBuffer1 = new StringBuffer();
    for (int i = 0; i < this.points.length; ++i)
      localStringBuffer1.append("p[" + i + "]:(" + this.points[i].x + "," + this.points[i].y + ");");
    for (int j = 0; j < this.imageVertices.length; ++j)
      localStringBuffer1.append("i[" + j + "]:(" + this.imageVertices[j].x + "," + this.imageVertices[j].y + ");");
    Log.d("POINTS CONTEXT", localStringBuffer1.toString());
    ArrayList localArrayList1 = new ArrayList();
    int k = 0;
    int l = 0;
    if (l < this.points.length)
    {
      label207: int i11;
      if (isInPicture(this.points[l]))
      {
        localArrayList1.add(this.points[l]);
        if (!isInPicture(this.points[((l + 1) % this.points.length)]))
        {
          i11 = 0;
          if (i11 < this.imageVertices.length)
          {
            label271: PointF localPointF9 = determineBoundaryPoint(this.points[l], this.points[((l + 1) % this.points.length)]);
            if (localPointF9 == null)
              break label363;
            if (ImageProcess.containsInfinitePoint(new PointF[] { localPointF9 }))
              break label363;
            if (!ImageProcess.coarseEquals(this.points[l], localPointF9))
              localArrayList1.add(localPointF9);
          }
        }
      }
      for (k = 1; ; k = 1)
      {
        label357: ++l;
        break label207:
        label363: ++i11;
        break label271:
        boolean bool1 = localArrayList1.isEmpty();
        PointF localPointF1 = null;
        if (!bool1)
          localPointF1 = (PointF)localArrayList1.get(-1 + localArrayList1.size());
        if (isInPicture(this.points[((l + 1) % this.points.length)]))
          for (int i10 = 0; ; ++i10)
          {
            if (i10 < this.imageVertices.length);
            PointF localPointF8 = determineBoundaryPoint(this.points[l], this.points[((l + 1) % this.points.length)]);
            if (localPointF8 == null)
              continue;
            if (ImageProcess.containsInfinitePoint(new PointF[] { localPointF8 }))
              continue;
            if (localPointF1 != null)
              localArrayList1.addAll(findVertexPointsBetween(localPointF1, localPointF8));
            if (!ImageProcess.coarseEquals(this.points[((l + 1) % this.points.length)], localPointF8))
              localArrayList1.add(localPointF8);
            k = 0;
            break label357:
          }
        ArrayList localArrayList3 = new ArrayList();
        for (int i8 = 0; i8 < this.imageVertices.length; ++i8)
        {
          PointF localPointF7 = ImageProcess.getLineSegmentIntersectionPoint(this.points[l], this.points[((l + 1) % this.points.length)], this.imageVertices[i8], this.imageVertices[((i8 + 1) % this.imageVertices.length)]);
          if (localPointF7 == null)
            continue;
          if (ImageProcess.containsInfinitePoint(new PointF[] { localPointF7 }))
            continue;
          Iterator localIterator = localArrayList3.iterator();
          do
          {
            boolean bool2 = localIterator.hasNext();
            i9 = 0;
            if (!bool2)
              break label694;
          }
          while (!ImageProcess.coarseEquals((PointF)localIterator.next(), localPointF7));
          int i9 = 1;
          label694: if (i9 != 0)
            continue;
          localArrayList3.add(localPointF7);
        }
        if (localArrayList3.size() == 2)
        {
          PointF localPointF3 = (PointF)localArrayList3.get(0);
          PointF localPointF4 = (PointF)localArrayList3.get(1);
          PointF localPointF5 = this.points[l];
          PointF localPointF6 = this.points[((l + 1) % this.points.length)];
          if ((localPointF6.x - localPointF5.x) * (localPointF4.x - localPointF3.x) + (localPointF6.y - localPointF5.y) * (localPointF4.y - localPointF3.y) < 0.0F)
          {
            localArrayList3.set(0, localPointF4);
            localArrayList3.set(1, localPointF3);
          }
        }
        if (localArrayList3.isEmpty())
          continue;
        if (localPointF1 != null)
        {
          PointF localPointF2 = (PointF)localArrayList3.get(0);
          localArrayList1.addAll(findVertexPointsBetween(localPointF1, localPointF2));
        }
        localArrayList1.addAll(localArrayList3);
      }
    }
    if ((k != 0) && (localArrayList1.size() > 1))
      localArrayList1.addAll(findVertexPointsBetween((PointF)localArrayList1.get(-1 + localArrayList1.size()), (PointF)localArrayList1.get(0)));
    if (localArrayList1.size() == 1)
      localArrayList1.remove(0);
    PointF[] arrayOfPointF1 = (PointF[])localArrayList1.toArray(new PointF[localArrayList1.size()]);
    StringBuffer localStringBuffer2 = new StringBuffer();
    for (int i1 = 0; i1 < arrayOfPointF1.length; ++i1)
      localStringBuffer2.append("p[" + i1 + "]:(" + arrayOfPointF1[i1].x + "," + arrayOfPointF1[i1].y + ");");
    Log.d("TRUEPOINTS", localStringBuffer2.toString());
    for (int i2 = 0; ; ++i2)
    {
      if (i2 < arrayOfPointF1.length)
      {
        for (int i3 = 0; ; ++i3)
        {
          if (i3 >= this.imageVertices.length)
            break label1763;
          if (!ImageProcess.isIntersect(arrayOfPointF1[i2], this.imageVertices[i3], this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[((i2 + 1) % arrayOfPointF1.length)]))
            break;
        }
        int i4 = 1;
        ArrayList localArrayList2;
        for (int i5 = 0; ; ++i5)
        {
          if (i5 < arrayOfPointF1.length)
            if ((!ImageProcess.isIntersect(arrayOfPointF1[i2], this.imageVertices[i3], arrayOfPointF1[i5], arrayOfPointF1[((i5 + 1) % arrayOfPointF1.length)])) && (!ImageProcess.isIntersect(arrayOfPointF1[((i2 + 1) % arrayOfPointF1.length)], this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[i5], arrayOfPointF1[((i5 + 1) % arrayOfPointF1.length)])))
              break label1426;
          for (i4 = 0; ; i4 = 0)
          {
            while (true)
            {
              if (i4 != 0);
              PointF[] arrayOfPointF2 = new PointF[4];
              arrayOfPointF2[0] = arrayOfPointF1[i2];
              arrayOfPointF2[1] = this.imageVertices[i3];
              arrayOfPointF2[2] = this.imageVertices[((i3 + 1) % this.imageVertices.length)];
              arrayOfPointF2[3] = arrayOfPointF1[((i2 + 1) % arrayOfPointF1.length)];
              drawShadow(paramCanvas, arrayOfPointF2);
              localArrayList2 = new ArrayList();
              localArrayList2.add(arrayOfPointF1[i2]);
              for (int i6 = this.imageVertices.length; ; --i6)
              {
                if (i6 <= 0)
                  break label1700;
                localArrayList2.add(this.imageVertices[((i3 + i6) % this.imageVertices.length)]);
              }
              label1426: if ((ImageProcess.crossProduct(arrayOfPointF1[i2], arrayOfPointF1[((i2 + 1) % arrayOfPointF1.length)], arrayOfPointF1[((i2 + 2) % arrayOfPointF1.length)]) * ImageProcess.crossProduct(this.imageVertices[i3], this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[((i2 + 2) % arrayOfPointF1.length)]) >= 0.0F) && (ImageProcess.crossProduct(arrayOfPointF1[i2], arrayOfPointF1[((i2 + 1) % arrayOfPointF1.length)], arrayOfPointF1[((i2 + 3) % arrayOfPointF1.length)]) * ImageProcess.crossProduct(this.imageVertices[i3], this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[((i2 + 3) % arrayOfPointF1.length)]) >= 0.0F))
                break;
              i4 = 0;
            }
            if ((((!ImageProcess.coarseEquals(this.imageVertices[i3], arrayOfPointF1[((i2 + 2) % arrayOfPointF1.length)])) || (!ImageProcess.coarseEquals(this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[((i2 + 3) % arrayOfPointF1.length)])))) && (((!ImageProcess.coarseEquals(this.imageVertices[i3], arrayOfPointF1[((i2 + 3) % arrayOfPointF1.length)])) || (!ImageProcess.coarseEquals(this.imageVertices[((i3 + 1) % this.imageVertices.length)], arrayOfPointF1[((i2 + 2) % arrayOfPointF1.length)])))))
              break;
          }
        }
        for (int i7 = 1; i7 < arrayOfPointF1.length; ++i7)
          label1700: localArrayList2.add(arrayOfPointF1[((i2 + i7) % arrayOfPointF1.length)]);
        drawShadow(paramCanvas, (PointF[])localArrayList2.toArray(new PointF[localArrayList2.size()]));
      }
      label1763: return;
    }
  }

  private void drawVertexLine(PointF paramPointF1, PointF paramPointF2, Canvas paramCanvas)
  {
    Paint localPaint = new Paint();
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setColor(-65536);
    localPaint.setStrokeWidth(1.5F);
    paramCanvas.drawLine(paramPointF1.x, paramPointF1.y, paramPointF1.x + 0.05F * (paramPointF2.x - paramPointF1.x), paramPointF1.y + 0.05F * (paramPointF2.y - paramPointF1.y), localPaint);
  }

  private List<PointF> findVertexPointsBetween(PointF paramPointF1, PointF paramPointF2)
  {
    int i = 0;
    label2: int j = this.imageVertices.length;
    int k = 0;
    if (i < j)
    {
      if (!ImageProcess.isInSameLine(this.imageVertices[i], this.imageVertices[((i + 1) % this.imageVertices.length)], paramPointF1))
        break label280;
      k = i;
    }
    ArrayList localArrayList;
    for (int l = 0; ; ++l)
    {
      int i1 = this.imageVertices.length;
      int i2 = 0;
      if (l < i1)
      {
        if (!ImageProcess.isInSameLine(this.imageVertices[l], this.imageVertices[((l + 1) % this.imageVertices.length)], paramPointF2))
          continue;
        i2 = l;
      }
      if (i2 < k)
        i2 += this.imageVertices.length;
      if (i2 == k)
      {
        PointF localPointF2 = this.imageVertices[k];
        PointF localPointF3 = this.imageVertices[((k + 1) % this.imageVertices.length)];
        if ((paramPointF2.x - paramPointF1.x) * (localPointF3.x - localPointF2.x) + (paramPointF2.y - paramPointF1.y) * (localPointF3.y - localPointF2.y) < 0.0F)
          i2 += this.imageVertices.length;
      }
      localArrayList = new ArrayList();
      for (int i3 = k; ; ++i3)
      {
        if (i3 >= i2)
          break label292;
        PointF localPointF1 = this.imageVertices[((i3 + 1) % this.imageVertices.length)];
        if ((ImageProcess.coarseEquals(paramPointF1, localPointF1)) || (ImageProcess.coarseEquals(paramPointF2, localPointF1)))
          continue;
        localArrayList.add(localPointF1);
      }
      label280: ++i;
      break label2:
    }
    label292: return localArrayList;
  }

  private boolean isInPicture(PointF paramPointF)
  {
    return ImageProcess.isInRectangle(new PointF(this.areaX, this.areaY), new PointF(this.areaX + this.mImage.getWidth(), this.areaY + this.mImage.getHeight()), paramPointF);
  }

  private void onImageChanged(boolean paramBoolean)
  {
    System.gc();
    this.mMatrix.reset();
    this.mMatrix.postScale(this.imageScale, this.imageScale);
    this.mMatrix.postRotate(this.imageRotateDegree);
    this.mImageSrc.setHasAlpha(true);
    this.mImage = Bitmap.createBitmap(this.mImageSrc, 0, 0, this.mImageSrc.getWidth(), this.mImageSrc.getHeight(), this.mMatrix, true);
    if (paramBoolean)
    {
      this.areaX = ((this.SCREEN_WIDTH - this.mImage.getWidth()) / 2);
      this.areaY = ((this.SCREEN_HEIGHT - this.mImage.getHeight()) / 2);
    }
    for (int i = 0; i < this.imageVertices.length; ++i)
    {
      if (this.imageVertices[i] != null)
        continue;
      this.imageVertices[i] = new PointF();
    }
    int j = this.imageRotateDegree / 90;
    this.imageVertices[((this.imageVertices.length - j) % this.imageVertices.length)].x = this.areaX;
    this.imageVertices[((this.imageVertices.length - j) % this.imageVertices.length)].y = this.areaY;
    this.imageVertices[((1 + this.imageVertices.length - j) % this.imageVertices.length)].x = (this.areaX + this.mImage.getWidth());
    this.imageVertices[((1 + this.imageVertices.length - j) % this.imageVertices.length)].y = this.areaY;
    this.imageVertices[((2 + this.imageVertices.length - j) % this.imageVertices.length)].x = (this.areaX + this.mImage.getWidth());
    this.imageVertices[((2 + this.imageVertices.length - j) % this.imageVertices.length)].y = (this.areaY + this.mImage.getHeight());
    this.imageVertices[((3 + this.imageVertices.length - j) % this.imageVertices.length)].x = this.areaX;
    this.imageVertices[((3 + this.imageVertices.length - j) % this.imageVertices.length)].y = (this.areaY + this.mImage.getHeight());
  }

  private void rotate(boolean paramBoolean)
  {
    int i = this.imageRotateDegree;
    int j;
    label12: float f1;
    if (paramBoolean)
    {
      j = 90;
      this.imageRotateDegree = ((360 + (j + i)) % 360);
      if (this.imageRotateDegree % 180 != 0)
        break label111;
      f1 = determineScale(this.mImageSrc.getWidth(), this.mImageSrc.getHeight());
    }
    while (true)
    {
      for (int k = 0; ; ++k)
      {
        if (k >= this.points.length)
          break label134;
        this.points[k] = ImageProcess.rotatePoint(this.points[k], this.CENTRAL_POINT, this.imageScale, f1, paramBoolean);
      }
      j = -90;
      break label12:
      label111: f1 = determineScale(this.mImageSrc.getHeight(), this.mImageSrc.getWidth());
    }
    label134: this.imageScale = f1;
    onImageChanged(true);
    float f2;
    if (paramBoolean)
      f2 = -90.0F;
    while (true)
    {
      RotateAnimation localRotateAnimation = new RotateAnimation(f2, 0.0F, this.SCREEN_WIDTH / 2, this.SCREEN_HEIGHT / 2);
      localRotateAnimation.setDuration(1000L);
      localRotateAnimation.setFillAfter(true);
      startAnimation(localRotateAnimation);
      return;
      f2 = 90.0F;
    }
  }

  private void setInitialPoints()
  {
    int i = this.mImage.getWidth();
    int j = this.mImage.getHeight();
    this.areaX = ((this.SCREEN_WIDTH - this.mImage.getWidth()) / 2);
    this.areaY = ((this.SCREEN_HEIGHT - this.mImage.getHeight()) / 2);
    this.points[0] = new PointF(this.areaX, this.areaY);
    this.points[1] = new PointF(this.areaX + i, this.areaY);
    this.points[3] = new PointF(this.areaX, this.areaY + j);
    this.points[2] = new PointF(this.areaX + i, this.areaY + j);
  }

  public void onDestroy()
  {
    if (this.mImageSrc != null)
      this.mImageSrc.recycle();
    if (this.mImage == null)
      return;
    this.mImage.recycle();
  }

  protected void onDraw(Canvas paramCanvas)
  {
    if (this.mImageSrc == null);
    int k;
    do
    {
      return;
      if (this.firstDraw)
      {
        super.onDraw(paramCanvas);
        this.SCREEN_WIDTH = getWidth();
        this.SCREEN_HEIGHT = getHeight();
        this.CENTRAL_POINT = new PointF(this.SCREEN_WIDTH / 2, this.SCREEN_HEIGHT / 2);
        this.imageScale = determineScale(this.mImageSrc.getWidth(), this.mImageSrc.getHeight());
        L.d(this, "First onImageChanged");
        onImageChanged(true);
        this.menuHandler.sendEmptyMessage(1);
        setInitialPoints();
        this.firstDraw = false;
      }
      this.mPaint.setAntiAlias(true);
      this.mPaint.setStyle(Paint.Style.STROKE);
      this.mPaint.setStrokeWidth(1.5F);
      paramCanvas.drawBitmap(this.mImage, this.areaX, this.areaY, this.mPaint);
      this.mPaint.setColor(-16776961);
      Path localPath = new Path();
      localPath.moveTo(this.points[0].x, this.points[0].y);
      localPath.lineTo(this.points[1].x, this.points[1].y);
      localPath.lineTo(this.points[2].x, this.points[2].y);
      localPath.lineTo(this.points[3].x, this.points[3].y);
      localPath.close();
      paramCanvas.drawPath(localPath, this.mPaint);
      for (int i = 1; i < 4; ++i)
      {
        drawDashedLine(this.points[0].x + 0.25F * (this.points[1].x - this.points[0].x) * i, this.points[0].y + 0.25F * (this.points[1].y - this.points[0].y) * i, this.points[3].x + 0.25F * (this.points[2].x - this.points[3].x) * i, this.points[3].y + 0.25F * (this.points[2].y - this.points[3].y) * i, paramCanvas);
        drawDashedLine(this.points[0].x + 0.25F * (this.points[3].x - this.points[0].x) * i, this.points[0].y + 0.25F * (this.points[3].y - this.points[0].y) * i, this.points[1].x + 0.25F * (this.points[2].x - this.points[1].x) * i, this.points[1].y + 0.25F * (this.points[2].y - this.points[1].y) * i, paramCanvas);
      }
      drawShadows(paramCanvas);
      if (!this.moving)
        break label711;
      label605: k = 0;
    }
    while (k >= this.points.length);
    if (this.movingPointIndex == k)
      drawCross(this.points[k], paramCanvas);
    while (true)
    {
      ++k;
      break label605:
      drawVertexLine(this.points[k], this.points[((k - 1 + this.points.length) % this.points.length)], paramCanvas);
      drawVertexLine(this.points[k], this.points[((k + 1 + this.points.length) % this.points.length)], paramCanvas);
    }
    for (int j = 0; ; ++j)
    {
      label711: if (j < this.points.length);
      drawVertexLine(this.points[j], this.points[((j - 1 + this.points.length) % this.points.length)], paramCanvas);
      drawVertexLine(this.points[j], this.points[((j + 1 + this.points.length) % this.points.length)], paramCanvas);
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    System.gc();
    int i = paramMotionEvent.getAction();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    if (i == 0)
    {
      this.moving = true;
      this.touchDownPoint.x = f1;
      this.touchDownPoint.y = f2;
      this.areaX = (f1 + 2.0F * (this.areaX - f1));
      this.areaY = (f2 + 2.0F * (this.areaY - f2));
      for (int i1 = 0; i1 < this.pointsBakForDrag.length; ++i1)
      {
        this.pointsBakForDrag[i1] = new PointF(this.points[i1].x, this.points[i1].y);
        this.points[i1].x = (f1 + 2.0F * (this.points[i1].x - f1));
        this.points[i1].y = (f2 + 2.0F * (this.points[i1].y - f2));
      }
      this.imageScale = (2.0F * this.imageScale);
      onImageChanged(false);
      this.movingPointIndex = 0;
      float f3 = 3.4028235E+38F;
      for (int i2 = 0; i2 < this.points.length; ++i2)
      {
        float f4 = ImageProcess.getDistance(this.points[i2], this.touchDownPoint);
        if (f4 >= f3)
          continue;
        f3 = f4;
        this.movingPointIndex = i2;
      }
      this.movingPointInitial.x = this.points[this.movingPointIndex].x;
      this.movingPointInitial.y = this.points[this.movingPointIndex].y;
      this.touchDownPoint.x = f1;
      this.touchDownPoint.y = f2;
      postInvalidate();
      return true;
    }
    if (this.movingPointIndex != -1)
    {
      if (i == 2)
      {
        this.points[this.movingPointIndex].x = (f1 - this.touchDownPoint.x + this.movingPointInitial.x);
        this.points[this.movingPointIndex].y = (f2 - this.touchDownPoint.y + this.movingPointInitial.y);
        if (!ImageProcess.isValidPointsOrder(this.points[0], this.points[1], this.points[2], this.points[3]))
        {
          if (!ImageProcess.isValidPointsOrder(this.points[((1 + this.movingPointIndex) % this.points.length)], this.points[this.movingPointIndex], this.points[((2 + this.movingPointIndex) % this.points.length)], this.points[((3 + this.movingPointIndex) % this.points.length)]))
            break label726;
          PointF localPointF10 = this.points[((1 + this.movingPointIndex) % this.points.length)];
          this.points[((1 + this.movingPointIndex) % this.points.length)] = this.points[this.movingPointIndex];
          this.points[this.movingPointIndex] = localPointF10;
          PointF localPointF11 = this.pointsBakForDrag[((1 + this.movingPointIndex) % this.pointsBakForDrag.length)];
          this.pointsBakForDrag[((1 + this.movingPointIndex) % this.pointsBakForDrag.length)] = this.pointsBakForDrag[this.movingPointIndex];
          this.pointsBakForDrag[this.movingPointIndex] = localPointF11;
        }
        StringBuffer localStringBuffer2;
        for (this.movingPointIndex = ((1 + this.movingPointIndex) % this.points.length); ; this.movingPointIndex = ((3 + this.movingPointIndex) % this.points.length))
        {
          do
          {
            if (ImageProcess.isClockwise(this.points))
              break label1004;
            localStringBuffer2 = new StringBuffer();
            for (int l = 0; ; ++l)
            {
              if (l >= this.points.length)
                break label923;
              label726: localStringBuffer2.append("p[" + l + "]:(" + this.points[l].x + "," + this.points[l].y + ");");
            }
          }
          while (!ImageProcess.isValidPointsOrder(this.points[((3 + this.movingPointIndex) % this.points.length)], this.points[((1 + this.movingPointIndex) % this.points.length)], this.points[((2 + this.movingPointIndex) % this.points.length)], this.points[this.movingPointIndex]));
          PointF localPointF8 = this.points[((3 + this.movingPointIndex) % this.points.length)];
          this.points[((3 + this.movingPointIndex) % this.points.length)] = this.points[this.movingPointIndex];
          this.points[this.movingPointIndex] = localPointF8;
          PointF localPointF9 = this.pointsBakForDrag[((3 + this.movingPointIndex) % this.pointsBakForDrag.length)];
          this.pointsBakForDrag[((3 + this.movingPointIndex) % this.pointsBakForDrag.length)] = this.pointsBakForDrag[this.movingPointIndex];
          this.pointsBakForDrag[this.movingPointIndex] = localPointF9;
        }
        label923: Log.d("ANTICLOCKWISE", localStringBuffer2.toString());
        PointF localPointF6 = this.points[1];
        this.points[1] = this.points[3];
        this.points[3] = localPointF6;
        PointF localPointF7 = this.pointsBakForDrag[1];
        this.pointsBakForDrag[1] = this.pointsBakForDrag[3];
        this.pointsBakForDrag[3] = localPointF7;
        if (this.movingPointIndex == 3);
        for (this.movingPointIndex = 1; ; this.movingPointIndex = 3)
          do
          {
            label1004: postInvalidate();
            return true;
          }
          while (this.movingPointIndex != 1);
      }
      if (i == 1)
      {
        for (int j = 0; j < this.points.length; ++j)
        {
          this.points[j].x = this.pointsBakForDrag[j].x;
          this.points[j].y = this.pointsBakForDrag[j].y;
          this.pointsBakForDrag[j] = null;
        }
        this.imageScale /= 2.0F;
        onImageChanged(true);
        PointF localPointF1 = this.points[this.movingPointIndex];
        localPointF1.x += (f1 - this.touchDownPoint.x) / 2.0F;
        PointF localPointF2 = this.points[this.movingPointIndex];
        localPointF2.y += (f2 - this.touchDownPoint.y) / 2.0F;
        this.movingPointIndex = -1;
        this.moving = false;
        if (!ImageProcess.isValidPointsOrder(this.points[0], this.points[1], this.points[2], this.points[3]))
        {
          if (!ImageProcess.isValidPointsOrder(this.points[0], this.points[1], this.points[3], this.points[2]))
            break label1387;
          PointF localPointF5 = this.points[2];
          this.points[2] = this.points[3];
          this.points[3] = localPointF5;
        }
        StringBuffer localStringBuffer1;
        while (true)
        {
          if (ImageProcess.isClockwise(this.points))
            break label1458;
          localStringBuffer1 = new StringBuffer();
          for (int k = 0; ; ++k)
          {
            if (k >= this.points.length)
              break label1418;
            localStringBuffer1.append("p[" + k + "]:(" + this.points[k].x + "," + this.points[k].y + ");");
          }
          label1387: PointF localPointF4 = this.points[2];
          this.points[2] = this.points[1];
          this.points[1] = localPointF4;
        }
        label1418: Log.d("ANTICLOCKWISE", localStringBuffer1.toString());
        PointF localPointF3 = this.points[1];
        this.points[1] = this.points[3];
        this.points[3] = localPointF3;
        label1458: postInvalidate();
        return true;
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }

  public void setImage(Bitmap paramBitmap)
  {
    this.mImageSrc = paramBitmap;
  }

  private class AutoDetectTask extends AsyncTask<Void, Void, Void>
  {
    private boolean showDialog;

    private AutoDetectTask()
    {
    }

    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      if (ImageRectificationView.this.autoDetect)
      {
        if (ImageRectificationView.this.pointsAutoDetect == null)
        {
          Mat localMat1 = ImageProcess.BitmapToMat(ImageRectificationView.this.mImageSrc);
          ArrayList localArrayList = new ArrayList();
          for (int i = 0; i < 4; ++i)
            localArrayList.add(new Point());
          Mat localMat2 = Converters.vector_Point_to_Mat(localArrayList);
          ImageProcess.quadrangleLocation(localMat1.getNativeObjAddr(), localMat2.getNativeObjAddr());
          Converters.Mat_to_vector_Point(localMat2, localArrayList);
          ImageRectificationView.access$102(ImageRectificationView.this, ImageProcess.convertOpenCVPoint(localArrayList));
          ImageProcess.sortVertices(ImageRectificationView.this.pointsAutoDetect);
        }
        ImageRectificationView localImageRectificationView1 = ImageRectificationView.this;
        ImageRectificationView localImageRectificationView2 = ImageRectificationView.this;
        PointF[] arrayOfPointF = new PointF[4];
        arrayOfPointF[0] = ((PointF)ImageRectificationView.access$100(ImageRectificationView.this).get(0));
        arrayOfPointF[1] = ((PointF)ImageRectificationView.access$100(ImageRectificationView.this).get(1));
        arrayOfPointF[2] = ((PointF)ImageRectificationView.access$100(ImageRectificationView.this).get(2));
        arrayOfPointF[3] = ((PointF)ImageRectificationView.access$100(ImageRectificationView.this).get(3));
        ImageRectificationView.access$302(localImageRectificationView1, localImageRectificationView2.convertPoints(true, arrayOfPointF));
      }
      while (true)
      {
        ImageRectificationView.this.postInvalidate();
        ImageRectificationView.this.convertAutoDetectHandler.sendEmptyMessage(0);
        return null;
        ImageRectificationView.this.setInitialPoints();
      }
    }

    protected void onPostExecute(Void paramVoid)
    {
      if (this.showDialog)
        L.d(this, "AutoDetectTask over");
      try
      {
        ((Activity)ImageRectificationView.this.getContext()).dismissDialog(0);
        return;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
      }
    }

    protected void onPreExecute()
    {
      if ((ImageRectificationView.this.autoDetect) && (ImageRectificationView.this.pointsAutoDetect == null))
      {
        this.showDialog = true;
        ((Activity)ImageRectificationView.this.getContext()).showDialog(0);
        return;
      }
      this.showDialog = false;
    }
  }

  private class RectificationTask extends AsyncTask<Void, Void, Void>
  {
    private Bitmap destImg;
    private Bitmap frame;
    private Bitmap temp;

    private RectificationTask()
    {
    }

    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      int i;
      try
      {
        PointF[] arrayOfPointF = ImageRectificationView.this.convertPoints(false, ImageRectificationView.this.points);
        float f1 = 3.4028235E+38F;
        float f2 = 3.4028235E+38F;
        float f3 = -3.402824E+038F;
        float f4 = -3.402824E+038F;
        i = 0;
        int j = arrayOfPointF.length;
        if (i < j)
        {
          if (arrayOfPointF[i].x < arrayOfPointF[(i + 1)].x)
          {
            if (arrayOfPointF[i].x < f1)
              f1 = arrayOfPointF[i].x;
            if (arrayOfPointF[(i + 1)].x > f3)
              f3 = arrayOfPointF[(i + 1)].x;
          }
          while (arrayOfPointF[i].y < arrayOfPointF[(i + 1)].y)
          {
            if (arrayOfPointF[i].y < f2)
              f2 = arrayOfPointF[i].y;
            if (arrayOfPointF[(i + 1)].y <= f4)
              break label1602;
            f4 = arrayOfPointF[(i + 1)].y;
            break label1602:
            if (arrayOfPointF[(i + 1)].x < f1)
              f1 = arrayOfPointF[(i + 1)].x;
            if (arrayOfPointF[i].x <= f3)
              continue;
            f3 = arrayOfPointF[i].x;
          }
          if (arrayOfPointF[(i + 1)].y < f2)
            f2 = arrayOfPointF[(i + 1)].y;
          if (arrayOfPointF[i].y <= f4)
            break label1602;
          f4 = arrayOfPointF[i].y;
          break label1602:
        }
        float f5 = f3 - f1;
        float f6 = f4 - f2;
        Math.min(ImageRectificationView.this.SCREEN_WIDTH / f5, ImageRectificationView.this.SCREEN_HEIGHT / f6);
        Object localObject = ImageRectificationView.this.mImageSrc;
        Log.d("mImageSrc", "width=" + ImageRectificationView.this.mImageSrc.getWidth() + ", height=" + ImageRectificationView.this.mImageSrc.getHeight());
        if (2.0F < 1.0F)
        {
          ImageRectificationView.this.mMatrix.reset();
          ImageRectificationView.this.mMatrix.postScale(2.0F, 2.0F);
          ImageRectificationView.this.mImageSrc.setHasAlpha(true);
          localObject = Bitmap.createBitmap(ImageRectificationView.this.mImageSrc, 0, 0, ImageRectificationView.this.mImageSrc.getWidth(), ImageRectificationView.this.mImageSrc.getHeight(), ImageRectificationView.this.mMatrix, true);
          if (((Bitmap)localObject).getConfig() != Bitmap.Config.ARGB_8888)
          {
            Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
            Bitmap localBitmap = ((Bitmap)localObject).copy(localConfig, true);
            ((Bitmap)localObject).recycle();
            localObject = localBitmap;
          }
          int k = arrayOfPointF.length;
          for (int l = 0; l < k; ++l)
          {
            PointF localPointF = arrayOfPointF[l];
            localPointF.x = (2.0F * localPointF.x);
            localPointF.y = (2.0F * localPointF.y);
          }
        }
        Log.d("originalImage", "width=" + ((Bitmap)localObject).getWidth() + ", height=" + ((Bitmap)localObject).getHeight());
        Mat localMat1 = ImageProcess.BitmapToMat((Bitmap)localObject);
        ArrayList localArrayList1 = new ArrayList();
        for (int i1 = 0; i1 < 4; ++i1)
          localArrayList1.add(new Point(arrayOfPointF[i1].x, arrayOfPointF[i1].y));
        Mat localMat2 = Converters.vector_Point_to_Mat(localArrayList1);
        int[] arrayOfInt = ImageProcess.getDestImageSize(localMat1.getNativeObjAddr(), localMat2.getNativeObjAddr());
        if (arrayOfInt == null)
          break label1608;
        if (arrayOfInt.length < 2)
          break label1608;
        this.destImg = Bitmap.createBitmap(arrayOfInt[0], arrayOfInt[1], Bitmap.Config.ARGB_8888);
        this.destImg.setDensity(((Bitmap)localObject).getDensity());
        Mat localMat3 = ImageProcess.BitmapToMat(this.destImg);
        ImageProcess.rectify(localMat1.getNativeObjAddr(), localMat2.getNativeObjAddr(), localMat3.getNativeObjAddr(), arrayOfInt);
        ImageProcess.MatToBitmap(localMat3, this.destImg);
        ImageRectificationView.this.mMatrix.reset();
        ImageRectificationView.this.mMatrix.postRotate(ImageRectificationView.this.imageRotateDegree);
        this.destImg = Bitmap.createBitmap(this.destImg, 0, 0, this.destImg.getWidth(), this.destImg.getHeight(), ImageRectificationView.this.mMatrix, true);
        if (ImageRectificationView.this.mImage.getConfig() != Bitmap.Config.ARGB_8888)
        {
          this.temp = ImageRectificationView.this.mImage.copy(Bitmap.Config.ARGB_8888, true);
          ImageRectificationView.this.mImage.recycle();
          ImageRectificationView.access$1102(ImageRectificationView.this, this.temp);
        }
        AnimationDrawable localAnimationDrawable = new AnimationDrawable();
        int i2 = ImageRectificationView.this.mImage.getWidth();
        int i3 = ImageRectificationView.this.mImage.getHeight();
        for (int i4 = 0; i4 <= 7; ++i4)
        {
          Point localPoint = new Point(ImageRectificationView.this.areaX, ImageRectificationView.this.areaY);
          this.frame = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
          this.frame.setDensity(ImageRectificationView.this.mImage.getDensity());
          Mat localMat4 = ImageProcess.BitmapToMat(this.frame);
          ArrayList localArrayList2 = new ArrayList();
          for (int i5 = 0; i5 < 4; ++i5)
            localArrayList2.add(new Point(ImageRectificationView.this.points[i5].x - localPoint.x, ImageRectificationView.this.points[i5].y - localPoint.y));
          Mat localMat5 = Converters.vector_Point_to_Mat(localArrayList2);
          ArrayList localArrayList3 = new ArrayList();
          for (int i6 = 0; i6 < 4; ++i6)
            localArrayList3.add(new Point(ImageRectificationView.this.imageVertices[i6].x - localPoint.x, ImageRectificationView.this.imageVertices[i6].y - localPoint.y));
          Mat localMat6 = Converters.vector_Point_to_Mat(localArrayList3);
          ImageProcess.getAnimFrame(ImageProcess.BitmapToMat(ImageRectificationView.this.mImage).getNativeObjAddr(), localMat5.getNativeObjAddr(), localMat6.getNativeObjAddr(), localMat4.getNativeObjAddr(), i4, 7);
          ImageProcess.MatToBitmap(localMat4, this.frame);
          ImageRectificationView.this.mMatrix.reset();
          float f7 = 1.0F - 0.2F * i4 / 7.0F;
          ImageRectificationView.this.mMatrix.setScale(f7, f7);
          this.frame = Bitmap.createBitmap(this.frame, 0, 0, i2, i3, ImageRectificationView.this.mMatrix, true);
          this.temp = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
          Canvas localCanvas = new Canvas(this.temp);
          ImageRectificationView.this.mPaint.setStyle(Paint.Style.FILL);
          ImageRectificationView.this.mPaint.setColor(-16777216);
          localCanvas.drawRect(0.0F, 0.0F, i2, i3, ImageRectificationView.this.mPaint);
          localCanvas.drawBitmap(this.frame, i2 * (1.0F - f7) / 2.0F, i3 * (1.0F - f7) / 2.0F, ImageRectificationView.this.mPaint);
          this.frame.recycle();
          this.frame = this.temp;
          localAnimationDrawable.addFrame(new BitmapDrawable(this.frame), 100);
        }
        ((ImageRectificationActivity)ImageRectificationView.this.getContext()).completeRectification(this.destImg, localAnimationDrawable, 0.8F, ImageRectificationView.this.imageVertices[0]);
        label1602: label1477: return null;
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        System.gc();
        Message localMessage = new Message();
        Bundle localBundle = new Bundle();
        localBundle.putString("message", ImageRectificationView.this.getResources().getString(2131361994));
        localMessage.setData(localBundle);
        ImageRectificationView.this.toastHandler.sendMessage(localMessage);
        if (this.destImg != null)
        {
          this.destImg.recycle();
          this.destImg = null;
        }
        if (this.frame != null)
        {
          this.frame.recycle();
          this.frame = null;
        }
        if (this.temp != null)
        {
          this.temp.recycle();
          this.temp = null;
        }
        System.gc();
        break label1477:
        i += 2;
      }
      label1608: return (Void)null;
    }

    protected void onPostExecute(Void paramVoid)
    {
      try
      {
        ((Activity)ImageRectificationView.this.getContext()).dismissDialog(1);
        return;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
      }
    }

    protected void onPreExecute()
    {
      ((Activity)ImageRectificationView.this.getContext()).showDialog(1);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.ImageRectificationView
 * JD-Core Version:    0.5.4
 */