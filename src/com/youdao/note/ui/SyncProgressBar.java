package com.youdao.note.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SyncProgressBar extends FrameLayout
{
  private static final int MESSAGE_UPDATE = 1;
  private static final long UPDATE_INTERVAL = 100L;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if (SyncProgressBar.this.getVisibility() != 0);
      int i;
      do
      {
        do
          return;
        while (paramMessage.what != 1);
        i = SyncProgressBar.this.mProgressBar.getProgress();
        if (i >= SyncProgressBar.this.mNextProgress)
          continue;
        SyncProgressBar.this.innerSetProgress(i + 1);
        sendEmptyMessageDelayed(1, 100L);
      }
      while (i < 100);
      SyncProgressBar.this.setVisibility(8);
      if (SyncProgressBar.this.mListener != null)
        SyncProgressBar.this.mListener.onAnimationFinished();
      removeMessages(1);
    }
  };
  private SyncAnimationListener mListener;
  private volatile int mNextProgress;
  private TextView mPercentageView;
  private ProgressBar mProgressBar;

  public SyncProgressBar(Context paramContext)
  {
    this(paramContext, null);
  }

  public SyncProgressBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public SyncProgressBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    LayoutInflater.from(paramContext).inflate(2130903113, this, true);
    this.mProgressBar = ((ProgressBar)findViewById(2131165358));
    this.mPercentageView = ((TextView)findViewById(2131165359));
  }

  private void innerSetProgress(int paramInt)
  {
    this.mProgressBar.setProgress(paramInt);
    this.mPercentageView.setText(paramInt + "%");
  }

  public void finishSync(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mNextProgress = 100;
      this.mHandler.sendEmptyMessage(1);
      return;
    }
    this.mHandler.removeMessages(1);
    setVisibility(8);
  }

  public void setAnimationFinishListener(SyncAnimationListener paramSyncAnimationListener)
  {
    this.mListener = paramSyncAnimationListener;
  }

  public void setProgress(int paramInt)
  {
    if (getVisibility() != 0)
    {
      setVisibility(0);
      innerSetProgress(paramInt);
    }
    this.mNextProgress = paramInt;
    this.mHandler.sendEmptyMessage(1);
  }

  public static abstract interface SyncAnimationListener
  {
    public abstract void onAnimationFinished();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.SyncProgressBar
 * JD-Core Version:    0.5.4
 */