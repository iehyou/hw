package com.youdao.note.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class YNoteDialog extends AlertDialog
{
  private View cancelButton = null;
  private LayoutInflater mInflater = null;
  private ViewGroup mLayout = null;
  private View okButton = null;

  public YNoteDialog(Context paramContext)
  {
    super(paramContext);
    this.mInflater = LayoutInflater.from(paramContext);
    this.mLayout = ((ViewGroup)this.mInflater.inflate(2130903044, null));
    this.okButton = this.mLayout.findViewById(2131165204);
    this.okButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        YNoteDialog.this.dismiss();
      }
    });
    this.cancelButton = this.mLayout.findViewById(2131165205);
  }

  public YNoteDialog(Context paramContext, int paramInt)
  {
    this(paramContext);
    setTitle(paramInt);
  }

  public YNoteDialog(Context paramContext, String paramString)
  {
    this(paramContext);
    setTitle(paramString);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(this.mLayout);
  }

  public void setNegativeListener(View.OnClickListener paramOnClickListener)
  {
    this.cancelButton.setVisibility(0);
    this.cancelButton.setOnClickListener(paramOnClickListener);
  }

  public void setPositiveListener(View.OnClickListener paramOnClickListener)
  {
    this.okButton.setOnClickListener(paramOnClickListener);
  }

  public void setTitle(int paramInt)
  {
    ((TextView)this.mLayout.findViewById(2131165202)).setText(paramInt);
  }

  public void setTitle(String paramString)
  {
    ((TextView)this.mLayout.findViewById(2131165202)).setText(paramString);
  }

  public void setView(int paramInt)
  {
    setView(this.mInflater.inflate(paramInt, null));
  }

  public void setView(View paramView)
  {
    ((ViewGroup)this.mLayout.findViewById(2131165203)).addView(paramView);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.YNoteDialog
 * JD-Core Version:    0.5.4
 */