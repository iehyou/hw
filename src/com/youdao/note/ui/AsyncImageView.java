package com.youdao.note.ui;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.Snippet;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.AbstractAsyncRequestTask;
import com.youdao.note.task.network.GetSnippetTask;
import com.youdao.note.utils.L;
import java.io.InputStream;

public class AsyncImageView extends ImageView
{
  private GetSnippetTask mTask = null;

  public AsyncImageView(Context paramContext)
  {
    super(paramContext);
  }

  public AsyncImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public AsyncImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private void loadImg(byte[] paramArrayOfByte)
  {
    onFinishLoadImage(new BitmapDrawable(BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length)));
  }

  public void loadSnippet(NoteMeta paramNoteMeta)
  {
    DataSource localDataSource = YNoteApplication.getInstance().getDataSource();
    Snippet localSnippet = localDataSource.getSnippet(paramNoteMeta);
    if (localSnippet != null)
    {
      if (this.mTask != null)
        this.mTask.cancel(true);
      if (!localSnippet.exist())
        break label49;
      loadImg(localSnippet.getContentBytes());
    }
    return;
    label49: this.mTask = new GetSnippetTask(localSnippet.parseFID(), localSnippet.parseVERSION(), localDataSource, paramNoteMeta)
    {
      protected void onSucceed(byte[] paramArrayOfByte)
      {
        Snippet localSnippet = this.val$dataSource.getSnippet(this.val$noteMeta);
        if (localSnippet == null)
          return;
        localSnippet.setContentBytes(paramArrayOfByte);
        this.val$dataSource.writeSnippet(localSnippet);
        AsyncImageView.this.loadImg(localSnippet.getContentBytes());
      }
    };
    this.mTask.execute();
  }

  public void loadUri(String paramString)
  {
    new AbstractAsyncRequestTask(paramString, false, paramString)
    {
      protected Drawable handleResponse(InputStream paramInputStream)
        throws Exception
      {
        return new BitmapDrawable(paramInputStream);
      }

      protected void onFailed(Exception paramException)
      {
        L.e(AsyncImageView.this, "Load snippet failed " + this.val$uri, paramException);
      }

      protected void onSucceed(Drawable paramDrawable)
      {
        AsyncImageView.this.onFinishLoadImage(paramDrawable);
      }
    }
    .execute();
    L.d(this, "load snipt image " + paramString);
  }

  public void onFinishLoadImage(Drawable paramDrawable)
  {
    L.d(this, "async img loaded.");
    L.d(this, "" + paramDrawable.getIntrinsicHeight() + " " + paramDrawable.getMinimumWidth());
    super.setImageDrawable(paramDrawable);
    super.setVisibility(0);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.AsyncImageView
 * JD-Core Version:    0.5.4
 */