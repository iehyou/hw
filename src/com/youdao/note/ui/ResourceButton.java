package com.youdao.note.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

public class ResourceButton extends FrameLayout
{
  private TextView mResourceCount;

  public ResourceButton(Context paramContext)
  {
    this(paramContext, null);
  }

  public ResourceButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LayoutInflater.from(paramContext).inflate(2130903106, this, true);
    this.mResourceCount = ((TextView)findViewById(2131165351));
  }

  public void updateResourceCount(int paramInt)
  {
    if (paramInt == 0)
    {
      setVisibility(8);
      return;
    }
    if (getVisibility() != 0)
      setVisibility(0);
    this.mResourceCount.setText(Integer.toString(paramInt));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.ResourceButton
 * JD-Core Version:    0.5.4
 */