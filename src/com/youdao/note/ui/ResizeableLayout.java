package com.youdao.note.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class ResizeableLayout extends RelativeLayout
{
  private int mHeight = 0;
  private SizeChangeListener mSizeChangeListener = null;
  private int mWidth = 0;

  public ResizeableLayout(Context paramContext)
  {
    super(paramContext);
  }

  public ResizeableLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.mSizeChangeListener != null)
      this.mSizeChangeListener.onSizeChanged(getWidth(), getHeight(), this.mWidth, this.mHeight);
    this.mHeight = getHeight();
    this.mWidth = getWidth();
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void setSizeChangeListener(SizeChangeListener paramSizeChangeListener)
  {
    this.mSizeChangeListener = paramSizeChangeListener;
  }

  public static abstract interface SizeChangeListener
  {
    public abstract void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.ResizeableLayout
 * JD-Core Version:    0.5.4
 */