package com.youdao.note.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.youdao.note.utils.L;

public class ProgressView extends View
{
  private Rect mRect = null;

  public ProgressView(Context paramContext)
  {
    super(paramContext);
  }

  public ProgressView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    int i = paramCanvas.getHeight();
    int j = paramCanvas.getWidth();
    L.d(this, "height=" + i + " width=" + j);
    Paint localPaint1 = new Paint();
    this.mRect = new Rect(0, 0, j / 2, i);
    localPaint1.setColor(-16776961);
    localPaint1.setAlpha(180);
    paramCanvas.drawRect(this.mRect, localPaint1);
    Paint localPaint2 = new Paint();
    localPaint2.setColor(-16776961);
    localPaint2.setAlpha(80);
    paramCanvas.drawRect(j / 2, 0.0F, j, i, localPaint2);
    L.d(this, "Progress View drawed.");
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.ProgressView
 * JD-Core Version:    0.5.4
 */