package com.youdao.note.ui.audio;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.youdao.note.utils.UnitUtils;
import java.io.File;
import java.io.IOException;

public class AudioPlayerBar extends LinearLayout
  implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnSeekCompleteListener, View.OnTouchListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener
{
  private static final int MESSAGE_UPDATE_PROGRESS = 1;
  private static final int SEEK_BAR_STATUS_TRACK = 1;
  private static final int SEEK_BAR_STATUS_UNTRACK;
  private TextView mCurPosView;
  private TextView mDurationView;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if ((paramMessage.what != 1) || (AudioPlayerBar.this.mPlayer == null))
        return;
      if (AudioPlayerBar.this.mSeekBarStatus == 0)
      {
        AudioPlayerBar.this.mSeekBar.setProgress(AudioPlayerBar.this.mPlayer.getCurrentPosition());
        AudioPlayerBar.this.mCurPosView.setText(UnitUtils.getDuration(AudioPlayerBar.this.mPlayer.getCurrentPosition()));
      }
      if (!AudioPlayerBar.this.mPlayer.isPlaying())
        return;
      AudioPlayerBar.this.postUpdateSeekBar();
    }
  };
  private AudioPlayListener mListener;
  private int mNextSeekTo;
  private View mPauseButton;
  private View mPlayButton;
  private MediaPlayer mPlayer;
  private SeekBar mSeekBar;
  private int mSeekBarStatus = 0;
  private Uri mUri;

  public AudioPlayerBar(Context paramContext)
  {
    this(paramContext, null);
  }

  public AudioPlayerBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LayoutInflater.from(paramContext).inflate(2130903042, this, true);
    this.mSeekBar = ((SeekBar)findViewById(2131165194));
    this.mSeekBar.setOnSeekBarChangeListener(this);
    this.mPlayButton = findViewById(2131165192);
    this.mPauseButton = findViewById(2131165193);
    this.mCurPosView = ((TextView)findViewById(2131165195));
    this.mDurationView = ((TextView)findViewById(2131165196));
    findViewById(2131165197).setOnClickListener(this);
    this.mPlayButton.setOnClickListener(this);
    this.mPauseButton.setOnClickListener(this);
  }

  private void close()
  {
    stop();
    if (this.mListener == null)
      return;
    this.mListener.onClose();
  }

  private void innerPlay()
    throws IllegalArgumentException, IllegalStateException, IOException
  {
    this.mPlayer = MediaPlayer.create(getContext(), this.mUri);
    this.mPlayer.setAudioStreamType(3);
    this.mPlayer.setOnErrorListener(this);
    this.mPlayer.setOnCompletionListener(this);
    this.mPlayer.setOnSeekCompleteListener(this);
    this.mSeekBar.setMax(getDuration());
    this.mDurationView.setText(UnitUtils.getDuration(getDuration()));
    this.mPlayer.start();
    this.mPlayButton.setVisibility(8);
    this.mPauseButton.setVisibility(0);
    this.mHandler.sendEmptyMessage(1);
  }

  private void postUpdateSeekBar()
  {
    this.mHandler.sendEmptyMessageDelayed(1, 1000L);
  }

  public int getDuration()
  {
    return this.mPlayer.getDuration();
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165192)
      if (this.mPlayer != null)
        resume();
    do
    {
      return;
      try
      {
        innerPlay();
        return;
      }
      catch (Exception localException)
      {
        stop();
        return;
      }
      if (paramView.getId() != 2131165193)
        continue;
      pause();
      return;
    }
    while (paramView.getId() != 2131165197);
    close();
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    if (this.mListener != null)
      this.mListener.onCompletion(paramMediaPlayer);
    stop();
    this.mSeekBar.setProgress(0);
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    if (this.mListener != null)
      this.mListener.onError(paramMediaPlayer, paramInt1, paramInt2);
    stop();
    return false;
  }

  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    this.mNextSeekTo = paramInt;
    this.mCurPosView.setText(UnitUtils.getDuration(this.mNextSeekTo));
  }

  public void onSeekComplete(MediaPlayer paramMediaPlayer)
  {
    if (this.mListener == null)
      return;
    this.mListener.onSeekComplete(paramMediaPlayer);
  }

  public void onStartTrackingTouch(SeekBar paramSeekBar)
  {
    this.mSeekBarStatus = 1;
  }

  public void onStopTrackingTouch(SeekBar paramSeekBar)
  {
    if (this.mPlayer == null);
    try
    {
      innerPlay();
      this.mPlayer.seekTo(this.mNextSeekTo);
      if ((this.mPlayer == null) || (!this.mPlayer.isPlaying()))
        label22: this.mCurPosView.setText(2131361928);
      this.mSeekBarStatus = 0;
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      break label22:
      this.mPlayer.seekTo(this.mNextSeekTo);
    }
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    return false;
  }

  public void pause()
  {
    if (this.mPlayer == null)
      return;
    this.mPlayer.pause();
    this.mHandler.removeMessages(1);
    this.mCurPosView.setText(2131361928);
    this.mPauseButton.setVisibility(8);
    this.mPlayButton.setVisibility(0);
  }

  public void play(Uri paramUri)
    throws IllegalArgumentException, IllegalStateException, IOException
  {
    this.mUri = paramUri;
    innerPlay();
  }

  public void play(File paramFile)
    throws IllegalArgumentException, IllegalStateException, IOException
  {
    this.mUri = Uri.fromFile(paramFile);
    innerPlay();
  }

  public void play(String paramString)
    throws IllegalStateException, IOException
  {
    play(new File(paramString));
  }

  public void resume()
  {
    if (this.mPlayer == null)
      throw new IllegalStateException("You have not start any audio now.");
    this.mPlayer.start();
    this.mPlayButton.setVisibility(8);
    this.mPauseButton.setVisibility(0);
    postUpdateSeekBar();
  }

  public void seekTo(int paramInt)
  {
    this.mPlayer.seekTo(paramInt);
  }

  public void setAudioPlayListener(AudioPlayListener paramAudioPlayListener)
  {
    this.mListener = paramAudioPlayListener;
  }

  public void stop()
  {
    if (this.mPlayer != null)
      this.mPlayer.release();
    this.mPlayer = null;
    this.mSeekBar.setProgress(0);
    this.mPauseButton.setVisibility(8);
    this.mPlayButton.setVisibility(0);
  }

  public static abstract interface AudioPlayListener
  {
    public abstract void onClose();

    public abstract void onCompletion(MediaPlayer paramMediaPlayer);

    public abstract boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2);

    public abstract void onSeekComplete(MediaPlayer paramMediaPlayer);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.audio.AudioPlayerBar
 * JD-Core Version:    0.5.4
 */