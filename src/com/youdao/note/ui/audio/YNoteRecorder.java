package com.youdao.note.ui.audio;

import android.media.AudioRecord;
import android.os.Handler;
import android.os.Message;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class YNoteRecorder
{
  public static final int ERROR_RECORD_FAILED = 1;
  public static final int ERROR_SAVE_FAILED = 2;
  public static final int ERROR_SPACE_LIMIT = 3;
  public static final int INITIALIZED = 1;
  private static final int MESSAGE_SAVE_FAILED = 3;
  private static final int MESSAGE_SPACE_LIMIT = 4;
  private static final int MESSAGE_START_RECORD_FAILED = 2;
  private static final int MESSAGE_VOLUME = 1;
  public static final int PAUSED = 8;
  public static final int RECORDING = 2;
  private static boolean loadLibSuccess = true;
  private int mBufferSize;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if (paramMessage.what == 1)
        YNoteRecorder.this.mListener.onVolumChanged(((Double)paramMessage.obj).doubleValue());
      do
      {
        return;
        if (paramMessage.what == 2)
        {
          YNoteRecorder.this.mListener.onError(1);
          return;
        }
        if (paramMessage.what != 3)
          continue;
        YNoteRecorder.this.mListener.onError(2);
        return;
      }
      while (paramMessage.what != 4);
      YNoteRecorder.this.mListener.onError(3);
    }
  };
  private YNoteRecordListener mListener = null;
  private RecordThread mRecordThread;
  private int mSampleInRate;
  private Integer mState = Integer.valueOf(1);
  private FileOutputStream output;

  static
  {
    try
    {
      System.loadLibrary("ynote_lib");
      return;
    }
    catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
    {
      loadLibSuccess = false;
    }
  }

  public YNoteRecorder(File paramFile, int paramInt)
    throws FileNotFoundException
  {
    this.mSampleInRate = paramInt;
    this.mBufferSize = AudioRecord.getMinBufferSize(this.mSampleInRate, 16, 2);
    this.output = new FileOutputStream(paramFile);
  }

  public YNoteRecorder(String paramString, int paramInt)
    throws FileNotFoundException
  {
    this(new File(paramString), paramInt);
  }

  private double convertToDouble(short[] paramArrayOfShort, int paramInt)
  {
    return paramArrayOfShort[paramInt] / 32767.0D;
  }

  private double[] convertWaveData(short[] paramArrayOfShort, int paramInt)
  {
    double[] arrayOfDouble = new double[paramInt];
    for (int i = 0; i < paramInt; ++i)
      arrayOfDouble[i] = convertToDouble(paramArrayOfShort, i);
    return arrayOfDouble;
  }

  private void innerStartRecord()
  {
    this.mState = Integer.valueOf(2);
    this.mRecordThread = new RecordThread(null);
    this.mRecordThread.start();
  }

  public static boolean isLoadLibSuccess()
  {
    return loadLibSuccess;
  }

  private double volumeRMS(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble.length == 0)
      return d1;
    for (int i = 0; i < paramArrayOfDouble.length; ++i)
      d1 += paramArrayOfDouble[i];
    double d2 = d1 / paramArrayOfDouble.length;
    double d3 = 0.0D;
    for (int j = 0; j < paramArrayOfDouble.length; ++j)
      d3 += Math.pow(paramArrayOfDouble[j] - d2, 2.0D);
    return Math.pow(d3 / paramArrayOfDouble.length, 0.5D);
  }

  public int getRecordState()
  {
    synchronized (this.mState)
    {
      int i = this.mState.intValue();
      return i;
    }
  }

  public boolean isRecording()
  {
    while (true)
    {
      synchronized (this.mState)
      {
        if (this.mState.intValue() != 2)
          break label29;
        i = 1;
        return i;
      }
      label29: int i = 0;
    }
  }

  public void pause()
  {
    synchronized (this.mState)
    {
      if (this.mState.intValue() == 8)
        return;
      if (this.mState.intValue() == 2)
        break label48;
      throw new IllegalStateException("pause() can only be called when the state is recording.");
    }
    label48: this.mState = Integer.valueOf(8);
    monitorexit;
  }

  public void setVolumeChangeListener(YNoteRecordListener paramYNoteRecordListener)
  {
    this.mListener = paramYNoteRecordListener;
  }

  public void start()
  {
    synchronized (this.mState)
    {
      if ((this.mState.intValue() == 1) || (this.mState.intValue() == 8))
        break label45;
      throw new IllegalStateException("startRecord() can only be called when the state is inilized or paused.");
    }
    label45: innerStartRecord();
    monitorexit;
  }

  public void stopRecord()
  {
    synchronized (this.mState)
    {
      if (this.mState.intValue() == 1)
        return;
      this.mState = Integer.valueOf(1);
      while (true)
      {
        boolean bool = this.mRecordThread.isAlive();
        if (!bool)
          break;
        try
        {
          Thread.sleep(100L);
        }
        catch (InterruptedException localInterruptedException)
        {
          localInterruptedException.printStackTrace();
        }
      }
    }
    monitorexit;
  }

  private class RecordThread extends Thread
  {
    private static final int RECORD_RESULT_PAUSED = 0;
    private static final int RECORD_RESULT_STOPPED = 1;

    private RecordThread()
    {
    }

    private int recording(AudioRecord paramAudioRecord, short[] paramArrayOfShort, byte[] paramArrayOfByte)
    {
      while (true)
      {
        int i = YNoteRecorder.this.getRecordState();
        if (i == 8)
          return 0;
        if (i != 2);
        do
          return 1;
        while ((i != 2) && (i != 8));
        int j = paramAudioRecord.read(paramArrayOfShort, 0, YNoteRecorder.this.mBufferSize);
        double[] arrayOfDouble = YNoteRecorder.this.convertWaveData(paramArrayOfShort, j);
        YNoteRecorder.this.mHandler.sendMessage(Message.obtain(YNoteRecorder.this.mHandler, 1, Double.valueOf(YNoteRecorder.this.volumeRMS(arrayOfDouble))));
        int k = SimpleLame.encode(paramArrayOfShort, paramArrayOfShort, j, paramArrayOfByte);
        try
        {
          YNoteRecorder.this.output.write(paramArrayOfByte, 0, k);
        }
        catch (IOException localIOException)
        {
        }
        if (!"No space left on device".equals(localIOException.getMessage()))
          continue;
        L.d(this, "No space left on device.");
        YNoteRecorder.this.mHandler.sendEmptyMessage(4);
      }
    }

    // ERROR //
    public final void run()
    {
      // Byte code:
      //   0: new 37	android/media/AudioRecord
      //   3: dup
      //   4: iconst_1
      //   5: aload_0
      //   6: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   9: invokestatic 117	com/youdao/note/ui/audio/YNoteRecorder:access$200	(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
      //   12: bipush 16
      //   14: iconst_2
      //   15: aload_0
      //   16: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   19: invokestatic 35	com/youdao/note/ui/audio/YNoteRecorder:access$300	(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
      //   22: invokespecial 120	android/media/AudioRecord:<init>	(IIIII)V
      //   25: astore_1
      //   26: aload_0
      //   27: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   30: invokestatic 35	com/youdao/note/ui/audio/YNoteRecorder:access$300	(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
      //   33: newarray short
      //   35: astore_2
      //   36: ldc2_w 121
      //   39: ldc2_w 123
      //   42: iconst_2
      //   43: aload_2
      //   44: arraylength
      //   45: imul
      //   46: i2d
      //   47: dmul
      //   48: dadd
      //   49: d2i
      //   50: newarray byte
      //   52: astore_3
      //   53: bipush 237
      //   55: invokestatic 130	android/os/Process:setThreadPriority	(I)V
      //   58: aload_0
      //   59: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   62: invokestatic 117	com/youdao/note/ui/audio/YNoteRecorder:access$200	(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
      //   65: iconst_1
      //   66: aload_0
      //   67: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   70: invokestatic 117	com/youdao/note/ui/audio/YNoteRecorder:access$200	(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
      //   73: bipush 32
      //   75: invokestatic 134	com/youdao/note/ui/audio/SimpleLame:init	(IIII)V
      //   78: aload_1
      //   79: invokevirtual 137	android/media/AudioRecord:startRecording	()V
      //   82: aload_1
      //   83: invokevirtual 140	android/media/AudioRecord:getRecordingState	()I
      //   86: iconst_3
      //   87: if_icmpeq +15 -> 102
      //   90: aload_0
      //   91: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   94: invokestatic 49	com/youdao/note/ui/audio/YNoteRecorder:access$400	(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;
      //   97: iconst_2
      //   98: invokevirtual 111	android/os/Handler:sendEmptyMessage	(I)Z
      //   101: pop
      //   102: aload_0
      //   103: aload_1
      //   104: aload_2
      //   105: aload_3
      //   106: invokespecial 142	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:recording	(Landroid/media/AudioRecord;[S[B)I
      //   109: istore 8
      //   111: iload 8
      //   113: ifne +30 -> 143
      //   116: invokestatic 145	com/youdao/note/ui/audio/SimpleLame:close	()V
      //   119: aload_1
      //   120: invokevirtual 148	android/media/AudioRecord:stop	()V
      //   123: aload_1
      //   124: invokevirtual 151	android/media/AudioRecord:release	()V
      //   127: return
      //   128: astore 4
      //   130: aload_0
      //   131: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   134: invokestatic 49	com/youdao/note/ui/audio/YNoteRecorder:access$400	(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;
      //   137: iconst_2
      //   138: invokevirtual 111	android/os/Handler:sendEmptyMessage	(I)Z
      //   141: pop
      //   142: return
      //   143: aload_3
      //   144: invokestatic 155	com/youdao/note/ui/audio/SimpleLame:flush	([B)I
      //   147: istore 9
      //   149: aload_0
      //   150: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   153: invokestatic 81	com/youdao/note/ui/audio/YNoteRecorder:access$500	(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;
      //   156: aload_3
      //   157: iconst_0
      //   158: iload 9
      //   160: invokevirtual 87	java/io/FileOutputStream:write	([BII)V
      //   163: invokestatic 145	com/youdao/note/ui/audio/SimpleLame:close	()V
      //   166: aload_1
      //   167: invokevirtual 148	android/media/AudioRecord:stop	()V
      //   170: aload_1
      //   171: invokevirtual 151	android/media/AudioRecord:release	()V
      //   174: aload_0
      //   175: getfield 15	com/youdao/note/ui/audio/YNoteRecorder$RecordThread:this$0	Lcom/youdao/note/ui/audio/YNoteRecorder;
      //   178: invokestatic 81	com/youdao/note/ui/audio/YNoteRecorder:access$500	(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;
      //   181: invokevirtual 156	java/io/FileOutputStream:close	()V
      //   184: return
      //   185: astore 12
      //   187: ldc 158
      //   189: ldc 160
      //   191: aload 12
      //   193: invokestatic 166	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   196: pop
      //   197: return
      //   198: astore 10
      //   200: aload 10
      //   202: invokevirtual 169	java/io/IOException:printStackTrace	()V
      //   205: goto -42 -> 163
      //   208: astore 6
      //   210: invokestatic 145	com/youdao/note/ui/audio/SimpleLame:close	()V
      //   213: aload_1
      //   214: invokevirtual 148	android/media/AudioRecord:stop	()V
      //   217: aload_1
      //   218: invokevirtual 151	android/media/AudioRecord:release	()V
      //   221: aload 6
      //   223: athrow
      //   224: astore 7
      //   226: goto -5 -> 221
      //   229: astore 11
      //   231: goto -57 -> 174
      //   234: astore 14
      //   236: return
      //
      // Exception table:
      //   from	to	target	type
      //   78	82	128	java/lang/Exception
      //   174	184	185	java/io/IOException
      //   149	163	198	java/io/IOException
      //   102	111	208	finally
      //   143	149	208	finally
      //   149	163	208	finally
      //   200	205	208	finally
      //   210	221	224	java/lang/Exception
      //   163	174	229	java/lang/Exception
      //   116	127	234	java/lang/Exception
    }
  }

  public static abstract interface YNoteRecordListener
  {
    public abstract void onError(int paramInt);

    public abstract void onVolumChanged(double paramDouble);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.audio.YNoteRecorder
 * JD-Core Version:    0.5.4
 */