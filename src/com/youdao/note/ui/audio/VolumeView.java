package com.youdao.note.ui.audio;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class VolumeView extends View
{
  private static final int A_DIFF = 0;
  private static final int B_DIFF = 0;
  private static final int COVER = 1258291200;
  private static final int DEFAULT_WIDTH = 3;
  private static final int END_COLOR = -12482876;
  private static final int G_DIFF = 0;
  private static final int INTERVAL_GREY = -3552823;
  private static final int R_DIFF = 0;
  private static final int SHADOW_HEIGHT = 1;
  private static final int START_COLOR = -6372124;
  private double mMaxVolume = 0.1000000014901161D;
  private double mMinVolume = 0.0D;
  private Paint mPaint = new Paint();
  private double mVolume = 0.0D;

  static
  {
    G_DIFF = Color.green(-12482876) - Color.green(-6372124);
    B_DIFF = Color.blue(-12482876) - Color.blue(-6372124);
  }

  public VolumeView(Context paramContext)
  {
    this(paramContext, null);
  }

  public VolumeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void onDraw(Canvas paramCanvas)
  {
    int i = getHeight();
    int j = getWidth();
    int k = j / 3;
    int l = 0;
    if (l >= k)
      label18: return;
    if (l % 2 == 0)
      if (3 * (l + 1) < j * this.mVolume)
      {
        double d = 1.0D * (l + 1) / k;
        this.mPaint.setColor(Color.argb(Color.alpha(-6372124) + (int)(d * A_DIFF), Color.red(-6372124) + (int)(d * R_DIFF), Color.green(-6372124) + (int)(d * G_DIFF), Color.blue(-6372124) + (int)(d * B_DIFF)));
        label129: float f1 = l * 3;
        float f2 = 3 * (l + 1);
        float f3 = i;
        paramCanvas.drawRect(f1, 0.0F, 3 * (l + 1), i, this.mPaint);
        this.mPaint.setColor(1258291200);
        paramCanvas.drawRect(f1, 0.0F, f2, 0.0F + 1.0F, this.mPaint);
        this.mPaint.setColor(-1);
        paramCanvas.drawRect(f1, f3 - 1.0F, f2, f3, this.mPaint);
      }
    while (true)
    {
      ++l;
      break label18:
      this.mPaint.setColor(-3552823);
      break label129:
      this.mPaint.setColor(0);
      paramCanvas.drawRect(l * 3, 0.0F, 3 * (l + 1), i, this.mPaint);
    }
  }

  public void pause()
  {
    this.mVolume = 0.0D;
  }

  public void setVolume(double paramDouble)
  {
    if (paramDouble < this.mMinVolume)
      this.mMinVolume = paramDouble;
    if (paramDouble > this.mMaxVolume)
      this.mMaxVolume = paramDouble;
    if (this.mMaxVolume - this.mMinVolume > 1.0E-010D);
    for (this.mVolume = ((paramDouble - this.mMinVolume) / (this.mMaxVolume - this.mMinVolume)); ; this.mVolume = 1.0D)
    {
      invalidate();
      return;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.audio.VolumeView
 * JD-Core Version:    0.5.4
 */