package com.youdao.note.ui.audio;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.ActivityConsts;
import com.youdao.note.service.RecordService;
import com.youdao.note.service.RecordService.RecordDurationListener;
import com.youdao.note.service.RecordService.RecorderBinder;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import com.youdao.note.utils.UnitUtils;
import java.io.File;
import java.io.IOException;

public class AudioRecordBar extends LinearLayout
  implements RecordService.RecordDurationListener, View.OnClickListener, YNoteRecorder.YNoteRecordListener, ActivityConsts
{
  private boolean isUserPaused = true;
  private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if (!"android.intent.action.DEVICE_STORAGE_LOW".equals(paramIntent.getAction()))
        return;
      AudioRecordBar.this.pauseRecord();
      UIUtilities.showToast(AudioRecordBar.this.getContext(), 2131361926);
    }
  };
  private Context mContext;
  private TextView mDurationView;
  private File mFile;
  private AudioRecordListener mListener;
  private View mPauseButton;
  private View mRecordButton;
  private PhoneStateListener mRecordPhoneStateListener = new PhoneStateListener()
  {
    public void onCallStateChanged(int paramInt, String paramString)
    {
      if (paramInt == 0)
        if (!AudioRecordBar.this.isUserPaused)
        {
          L.d(this, "restart record.");
          AudioRecordBar.this.startRecord();
        }
      do
        return;
      while (AudioRecordBar.this.mRecordService.getRecordStatus() != 2);
      L.d(this, "pause record.");
      AudioRecordBar.access$302(AudioRecordBar.this, false);
      AudioRecordBar.this.pauseRecord();
    }
  };
  private RecordService mRecordService;
  private ServiceConnection mRecordServiceConnection = new ServiceConnection()
  {
    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
      AudioRecordBar.access$002(AudioRecordBar.this, ((RecordService.RecorderBinder)paramIBinder).getRecordService());
      try
      {
        AudioRecordBar.this.mRecordService.init(AudioRecordBar.this.getContext(), AudioRecordBar.this.mFile, AudioRecordBar.this);
        AudioRecordBar.this.startRecord();
        return;
      }
      catch (Exception localException)
      {
        AudioRecordBar.this.stopRecord(false, false, 3);
        L.e(this, "Init record service failed.", localException);
        UIUtilities.showToast(AudioRecordBar.this.getContext(), 2131361866);
      }
    }

    public void onServiceDisconnected(ComponentName paramComponentName)
    {
      AudioRecordBar.access$002(AudioRecordBar.this, null);
    }
  };
  private TextView mRecordingView;
  private View mStopButton;
  private long mTotalTimeMs = 0L;
  private VolumeView mVolumeView;
  private TelephonyManager tm;

  public AudioRecordBar(Context paramContext)
  {
    this(paramContext, null);
  }

  public AudioRecordBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    LayoutInflater.from(paramContext).inflate(2130903043, this, true);
    this.mPauseButton = findViewById(2131165193);
    this.mRecordButton = findViewById(2131165198);
    this.mStopButton = findViewById(2131165197);
    this.mVolumeView = ((VolumeView)findViewById(2131165200));
    this.mDurationView = ((TextView)findViewById(2131165196));
    this.mRecordingView = ((TextView)findViewById(2131165199));
    this.mDurationView.setText(UnitUtils.getDuration(this.mTotalTimeMs));
    this.mPauseButton.setOnClickListener(this);
    this.mRecordButton.setOnClickListener(this);
    this.mPauseButton.setOnClickListener(this);
    this.mStopButton.setOnClickListener(this);
    this.tm = ((TelephonyManager)getContext().getSystemService("phone"));
  }

  private void regist()
  {
    Intent localIntent = new Intent(getContext(), RecordService.class);
    getContext().bindService(localIntent, this.mRecordServiceConnection, 1);
    IntentFilter localIntentFilter = new IntentFilter("com.youdao.note.action.RECORD_PAUSE");
    localIntentFilter.addAction("com.youdao.note.action.RECORD_RECORD");
    localIntentFilter.addAction("com.youdao.note.action.RECORD_STOP");
    localIntentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
    getContext().registerReceiver(this.mBroadcastReceiver, localIntentFilter);
    this.tm.listen(this.mRecordPhoneStateListener, 32);
  }

  private void startRecord()
  {
    if ((!YNoteApplication.getInstance().isLoadRecortLibSuccess()) || (this.mRecordService != null));
    try
    {
      this.mRecordService.startRecord(this);
      label24: this.mRecordButton.setVisibility(8);
      this.mPauseButton.setVisibility(0);
      this.mRecordingView.setText(2131361860);
      onDurationChanged(this.mTotalTimeMs);
      return;
    }
    catch (Exception localException)
    {
      UIUtilities.showToast(getContext(), 2131361866);
      if (this.mListener != null);
      this.mListener.onRecordFinished(this.mFile, 3);
      break label24:
      Toast.makeText(this.mContext, 2131362037, 0).show();
    }
  }

  private void unRegist()
  {
    try
    {
      getContext().unbindService(this.mRecordServiceConnection);
      getContext().unregisterReceiver(this.mBroadcastReceiver);
      this.tm.listen(this.mRecordPhoneStateListener, 0);
      return;
    }
    catch (Exception localException)
    {
      L.e(this, "Unbind record service failed", localException);
    }
  }

  public void init(File paramFile)
    throws IOException
  {
    if (!paramFile.exists())
      FileUtils.createNewFile(paramFile);
    this.mFile = paramFile;
    regist();
  }

  public void init(String paramString)
    throws IOException
  {
    init(new File(paramString));
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165198)
      startRecord();
    do
    {
      return;
      if (paramView.getId() != 2131165193)
        continue;
      this.isUserPaused = true;
      pauseRecord();
      return;
    }
    while (paramView.getId() != 2131165197);
    stopRecord(true);
  }

  public void onDurationChanged(long paramLong)
  {
    if (this.mFile.length() > YNoteApplication.getInstance().getMaxResourceSize())
    {
      stopRecord(true, true, 2);
      UIUtilities.showToast(getContext(), 2131361863);
    }
    this.mTotalTimeMs = paramLong;
    this.mDurationView.setText(UnitUtils.getDuration(this.mTotalTimeMs));
  }

  public void onError(int paramInt)
  {
    if (paramInt == 1)
    {
      UIUtilities.showToast(getContext(), 2131361924);
      pauseRecord();
    }
    do
    {
      return;
      if (paramInt != 2)
        continue;
      UIUtilities.showToast(getContext(), 2131361927);
      stopRecord(true, false, 3);
      return;
    }
    while (paramInt != 3);
    UIUtilities.showToast(getContext(), 2131361926);
    pauseRecord();
  }

  public void onVolumChanged(double paramDouble)
  {
    if (this.mRecordButton.getVisibility() != 8)
      return;
    this.mVolumeView.setVolume(paramDouble);
  }

  public void pauseRecord()
  {
    if (this.mRecordService != null)
      this.mRecordService.pauseRecord();
    this.mPauseButton.setVisibility(8);
    this.mRecordButton.setVisibility(0);
    this.mVolumeView.pause();
    this.mRecordingView.setText(2131361928);
  }

  public void setAudioRecordListener(AudioRecordListener paramAudioRecordListener)
  {
    this.mListener = paramAudioRecordListener;
  }

  public void stopRecord(boolean paramBoolean)
  {
    stopRecord(true, paramBoolean, 1);
  }

  public void stopRecord(boolean paramBoolean, int paramInt)
  {
    stopRecord(true, paramBoolean, paramInt);
  }

  public void stopRecord(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    if (this.mRecordService != null)
    {
      if (paramBoolean1)
        this.mRecordService.stopRecord();
      if ((!paramBoolean2) && (this.mFile != null))
      {
        this.mFile.delete();
        this.mFile = null;
      }
      unRegist();
      if (this.mListener != null)
        this.mListener.onRecordFinished(this.mFile, paramInt);
    }
    this.mTotalTimeMs = 0L;
  }

  public static abstract interface AudioRecordListener
  {
    public static final int ACTION_NONE = 3;
    public static final int ACTION_PLAY = 1;
    public static final int ACTION_RECORD = 2;

    public abstract void onRecordFinished(File paramFile, int paramInt);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.audio.AudioRecordBar
 * JD-Core Version:    0.5.4
 */