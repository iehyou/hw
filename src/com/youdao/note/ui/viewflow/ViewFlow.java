package com.youdao.note.ui.viewflow;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AbsListView.LayoutParams;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Scroller;
import com.youdao.note.R.styleable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class ViewFlow extends AdapterView<Adapter>
{
  private static final int INVALID_SCREEN = -1;
  private static final int SNAP_VELOCITY = 1000;
  private static final int TOUCH_STATE_REST = 0;
  private static final int TOUCH_STATE_SCROLLING = 1;
  private Adapter mAdapter;
  private int mCurrentAdapterIndex;
  private int mCurrentBufferIndex;
  private int mCurrentScreen;
  private AdapterDataSetObserver mDataSetObserver;
  private boolean mFirstLayout = true;
  private FlowIndicator mIndicator;
  private float mLastMotionX;
  private float mLastMotionY;
  private int mLastOrientation = -1;
  private int mLastScrollDirection;
  private LinkedList<View> mLoadedViews;
  private int mMaximumVelocity;
  private int mNextScreen = -1;
  private Scroller mScroller;
  private int mSideBuffer = 2;
  private int mTouchSlop;
  private int mTouchState = 0;
  private VelocityTracker mVelocityTracker;
  private ViewSwitchListener mViewSwitchListener;
  private ViewTreeObserver.OnGlobalLayoutListener orientationChangeListener = new ViewTreeObserver.OnGlobalLayoutListener()
  {
    public void onGlobalLayout()
    {
      ViewFlow.this.getViewTreeObserver().removeGlobalOnLayoutListener(ViewFlow.this.orientationChangeListener);
      ViewFlow.this.setSelection(ViewFlow.this.mCurrentAdapterIndex);
    }
  };

  public ViewFlow(Context paramContext)
  {
    super(paramContext);
    this.mSideBuffer = 3;
    init();
  }

  public ViewFlow(Context paramContext, int paramInt)
  {
    super(paramContext);
    this.mSideBuffer = paramInt;
    init();
  }

  public ViewFlow(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mSideBuffer = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ViewFlow).getInt(0, 3);
    init();
  }

  private void init()
  {
    this.mLoadedViews = new LinkedList();
    this.mScroller = new Scroller(getContext());
    ViewConfiguration localViewConfiguration = ViewConfiguration.get(getContext());
    this.mTouchSlop = localViewConfiguration.getScaledTouchSlop();
    this.mMaximumVelocity = localViewConfiguration.getScaledMaximumFlingVelocity();
  }

  private void logBuffer()
  {
    Log.d("viewflow", "Size of mLoadedViews: " + this.mLoadedViews.size() + "X: " + this.mScroller.getCurrX() + ", Y: " + this.mScroller.getCurrY());
    Log.d("viewflow", "IndexInAdapter: " + this.mCurrentAdapterIndex + ", IndexInBuffer: " + this.mCurrentBufferIndex);
  }

  private View makeAndAddView(int paramInt, boolean paramBoolean, View paramView)
  {
    View localView = this.mAdapter.getView(paramInt, paramView, this);
    if (paramView != null);
    for (boolean bool = true; ; bool = false)
      return setupChild(localView, paramBoolean, bool);
  }

  private void postViewSwitched(int paramInt)
  {
    if (paramInt == 0)
      return;
    if (paramInt > 0)
    {
      this.mCurrentAdapterIndex = (1 + this.mCurrentAdapterIndex);
      this.mCurrentBufferIndex = (1 + this.mCurrentBufferIndex);
      int l = this.mCurrentAdapterIndex;
      int i1 = this.mSideBuffer;
      View localView2 = null;
      if (l > i1)
      {
        localView2 = (View)this.mLoadedViews.removeFirst();
        detachViewFromParent(localView2);
        this.mCurrentBufferIndex = (-1 + this.mCurrentBufferIndex);
      }
      int i2 = this.mCurrentAdapterIndex + this.mSideBuffer;
      if (i2 < this.mAdapter.getCount())
        this.mLoadedViews.addLast(makeAndAddView(i2, true, localView2));
    }
    while (true)
    {
      requestLayout();
      setVisibleView(this.mCurrentBufferIndex, true);
      if (this.mIndicator != null)
        this.mIndicator.onSwitched((View)this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
      if (this.mViewSwitchListener != null)
        this.mViewSwitchListener.onSwitched((View)this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
      logBuffer();
      return;
      this.mCurrentAdapterIndex = (-1 + this.mCurrentAdapterIndex);
      this.mCurrentBufferIndex = (-1 + this.mCurrentBufferIndex);
      int i = -1 + this.mAdapter.getCount() - this.mCurrentAdapterIndex;
      int j = this.mSideBuffer;
      View localView1 = null;
      if (i > j)
      {
        localView1 = (View)this.mLoadedViews.removeLast();
        detachViewFromParent(localView1);
      }
      int k = this.mCurrentAdapterIndex - this.mSideBuffer;
      if (k <= -1)
        continue;
      this.mLoadedViews.addFirst(makeAndAddView(k, false, localView1));
      this.mCurrentBufferIndex = (1 + this.mCurrentBufferIndex);
    }
  }

  private void resetFocus()
  {
    logBuffer();
    this.mLoadedViews.clear();
    removeAllViewsInLayout();
    for (int i = Math.max(0, this.mCurrentAdapterIndex - this.mSideBuffer); i < Math.min(this.mAdapter.getCount(), 1 + (this.mCurrentAdapterIndex + this.mSideBuffer)); ++i)
    {
      this.mLoadedViews.addLast(makeAndAddView(i, true, null));
      if (i != this.mCurrentAdapterIndex)
        continue;
      this.mCurrentBufferIndex = (-1 + this.mLoadedViews.size());
    }
    logBuffer();
    requestLayout();
  }

  private void setVisibleView(int paramInt, boolean paramBoolean)
  {
    this.mCurrentScreen = Math.max(0, Math.min(paramInt, -1 + getChildCount()));
    int i = this.mCurrentScreen * getWidth() - this.mScroller.getCurrX();
    this.mScroller.startScroll(this.mScroller.getCurrX(), this.mScroller.getCurrY(), i, 0, 0);
    if (i == 0)
      onScrollChanged(i + this.mScroller.getCurrX(), this.mScroller.getCurrY(), i + this.mScroller.getCurrX(), this.mScroller.getCurrY());
    if (paramBoolean)
    {
      invalidate();
      return;
    }
    postInvalidate();
  }

  private View setupChild(View paramView, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = -1;
    Object localObject = paramView.getLayoutParams();
    if (localObject == null)
      localObject = new AbsListView.LayoutParams(i, -2, 0);
    if (paramBoolean2)
    {
      if (paramBoolean1);
      while (true)
      {
        attachViewToParent(paramView, i, (ViewGroup.LayoutParams)localObject);
        return paramView;
        i = 0;
      }
    }
    if (paramBoolean1);
    while (true)
    {
      addViewInLayout(paramView, i, (ViewGroup.LayoutParams)localObject, true);
      return paramView;
      i = 0;
    }
  }

  private void snapToDestination()
  {
    int i = getWidth();
    snapToScreen((getScrollX() + i / 2) / i);
  }

  private void snapToScreen(int paramInt)
  {
    this.mLastScrollDirection = (paramInt - this.mCurrentScreen);
    if (!this.mScroller.isFinished())
      return;
    int i = Math.max(0, Math.min(paramInt, -1 + getChildCount()));
    this.mNextScreen = i;
    int j = i * getWidth() - getScrollX();
    this.mScroller.startScroll(getScrollX(), 0, j, 0, 2 * Math.abs(j));
    invalidate();
  }

  public void computeScroll()
  {
    if (this.mScroller.computeScrollOffset())
    {
      scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
      postInvalidate();
    }
    do
      return;
    while (this.mNextScreen == -1);
    this.mCurrentScreen = Math.max(0, Math.min(this.mNextScreen, -1 + getChildCount()));
    this.mNextScreen = -1;
    postViewSwitched(this.mLastScrollDirection);
  }

  public Adapter getAdapter()
  {
    return this.mAdapter;
  }

  public int getSelectedItemPosition()
  {
    return this.mCurrentAdapterIndex;
  }

  public View getSelectedView()
  {
    if (this.mCurrentBufferIndex < this.mLoadedViews.size())
      return (View)this.mLoadedViews.get(this.mCurrentBufferIndex);
    return null;
  }

  public int getViewsCount()
  {
    return this.mAdapter.getCount();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (paramConfiguration.orientation == this.mLastOrientation)
      return;
    this.mLastOrientation = paramConfiguration.orientation;
    getViewTreeObserver().addOnGlobalLayoutListener(this.orientationChangeListener);
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (getChildCount() == 0)
      return false;
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain();
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    switch (i)
    {
    default:
    case 0:
    case 2:
    case 1:
    case 3:
    }
    while (true)
    {
      label80: return false;
      if (!this.mScroller.isFinished())
        this.mScroller.abortAnimation();
      this.mLastMotionX = f1;
      this.mLastMotionY = f2;
      if (this.mScroller.isFinished());
      for (int i5 = 0; ; i5 = 1)
      {
        this.mTouchState = i5;
        break label80:
      }
      int k = (int)Math.abs(f1 - this.mLastMotionX);
      int l = (int)Math.abs(f2 - this.mLastMotionY);
      int i1;
      label182: int i2;
      int i3;
      if ((k > this.mTouchSlop) && (k > l))
      {
        i1 = 1;
        if (i1 != 0)
          this.mTouchState = 1;
        if (this.mTouchState != 1)
          continue;
        i2 = (int)(this.mLastMotionX - f1);
        this.mLastMotionX = f1;
        i3 = getScrollX();
        if (i2 >= 0)
          break label251;
        if (i3 > 0)
          scrollBy(Math.max(-i3, i2), 0);
      }
      while (true)
      {
        return true;
        i1 = 0;
        break label182:
        label251: if (i2 <= 0)
          continue;
        int i4 = getChildAt(-1 + getChildCount()).getRight() - i3 - getWidth();
        if (i4 <= 0)
          continue;
        scrollBy(Math.min(i4, i2), 0);
      }
      int j;
      if (this.mTouchState == 1)
      {
        VelocityTracker localVelocityTracker = this.mVelocityTracker;
        localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
        j = (int)localVelocityTracker.getXVelocity();
        if ((j <= 1000) || (this.mCurrentScreen <= 0))
          break label386;
        snapToScreen(-1 + this.mCurrentScreen);
      }
      while (true)
      {
        if (this.mVelocityTracker != null)
        {
          this.mVelocityTracker.recycle();
          this.mVelocityTracker = null;
        }
        this.mTouchState = 0;
        break label80:
        if ((j < -1000) && (this.mCurrentScreen < -1 + getChildCount()))
          label386: snapToScreen(1 + this.mCurrentScreen);
        snapToDestination();
      }
      this.mTouchState = 0;
    }
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 0;
    int j = getChildCount();
    for (int k = 0; k < j; ++k)
    {
      View localView = getChildAt(k);
      if (localView.getVisibility() == 8)
        continue;
      int l = localView.getMeasuredWidth();
      localView.layout(i, 0, i + l, localView.getMeasuredHeight());
      i += l;
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int i = View.MeasureSpec.getSize(paramInt1);
    if ((View.MeasureSpec.getMode(paramInt1) != 1073741824) && (!isInEditMode()))
      throw new IllegalStateException("ViewFlow can only be used in EXACTLY mode.");
    if ((View.MeasureSpec.getMode(paramInt2) != 1073741824) && (!isInEditMode()))
      throw new IllegalStateException("ViewFlow can only be used in EXACTLY mode.");
    int j = getChildCount();
    for (int k = 0; k < j; ++k)
      getChildAt(k).measure(paramInt1, paramInt2);
    if (!this.mFirstLayout)
      return;
    this.mScroller.startScroll(0, 0, i * this.mCurrentScreen, 0, 0);
    this.mFirstLayout = false;
  }

  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.mIndicator == null)
      return;
    int i = paramInt1 + (this.mCurrentAdapterIndex - this.mCurrentBufferIndex) * getWidth();
    this.mIndicator.onScrolled(i, paramInt2, paramInt3, paramInt4);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (getChildCount() == 0)
      return false;
    if (this.mVelocityTracker == null)
      this.mVelocityTracker = VelocityTracker.obtain();
    this.mVelocityTracker.addMovement(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    switch (i)
    {
    default:
    case 0:
    case 2:
    case 1:
    case 3:
    }
    while (true)
    {
      label80: return true;
      if (!this.mScroller.isFinished())
        this.mScroller.abortAnimation();
      this.mLastMotionX = f1;
      this.mLastMotionY = f2;
      if (this.mScroller.isFinished());
      for (int i5 = 0; ; i5 = 1)
      {
        this.mTouchState = i5;
        break label80:
      }
      int k = (int)Math.abs(f1 - this.mLastMotionX);
      int l = (int)Math.abs(f2 - this.mLastMotionY);
      int i1;
      label182: int i2;
      int i3;
      if ((k > this.mTouchSlop) && (k > l))
      {
        i1 = 1;
        if (i1 != 0)
          this.mTouchState = 1;
        if (this.mTouchState != 1)
          continue;
        i2 = (int)(this.mLastMotionX - f1);
        this.mLastMotionX = f1;
        i3 = getScrollX();
        if (i2 >= 0)
          break label251;
        if (i3 > 0)
          scrollBy(Math.max(-i3, i2), 0);
      }
      while (true)
      {
        return true;
        i1 = 0;
        break label182:
        label251: if (i2 <= 0)
          continue;
        int i4 = getChildAt(-1 + getChildCount()).getRight() - i3 - getWidth();
        if (i4 <= 0)
          continue;
        scrollBy(Math.min(i4, i2), 0);
      }
      int j;
      if (this.mTouchState == 1)
      {
        VelocityTracker localVelocityTracker = this.mVelocityTracker;
        localVelocityTracker.computeCurrentVelocity(1000, this.mMaximumVelocity);
        j = (int)localVelocityTracker.getXVelocity();
        if ((j <= 1000) || (this.mCurrentScreen <= 0))
          break label386;
        snapToScreen(-1 + this.mCurrentScreen);
      }
      while (true)
      {
        if (this.mVelocityTracker != null)
        {
          this.mVelocityTracker.recycle();
          this.mVelocityTracker = null;
        }
        this.mTouchState = 0;
        break label80:
        if ((j < -1000) && (this.mCurrentScreen < -1 + getChildCount()))
          label386: snapToScreen(1 + this.mCurrentScreen);
        snapToDestination();
      }
      snapToDestination();
      this.mTouchState = 0;
    }
  }

  public void setAdapter(Adapter paramAdapter)
  {
    setAdapter(paramAdapter, 0);
  }

  public void setAdapter(Adapter paramAdapter, int paramInt)
  {
    if (this.mAdapter != null)
      this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
    this.mAdapter = paramAdapter;
    if (this.mAdapter != null)
    {
      this.mDataSetObserver = new AdapterDataSetObserver();
      this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
    }
    if ((this.mAdapter == null) || (this.mAdapter.getCount() == 0))
      return;
    setSelection(paramInt);
  }

  public void setFlowIndicator(FlowIndicator paramFlowIndicator)
  {
    this.mIndicator = paramFlowIndicator;
    this.mIndicator.setViewFlow(this);
  }

  public void setOnViewSwitchListener(ViewSwitchListener paramViewSwitchListener)
  {
    this.mViewSwitchListener = paramViewSwitchListener;
  }

  public void setSelection(int paramInt)
  {
    this.mNextScreen = -1;
    this.mScroller.forceFinished(true);
    if (this.mAdapter == null);
    do
    {
      return;
      int i = Math.min(Math.max(paramInt, 0), -1 + this.mAdapter.getCount());
      ArrayList localArrayList = new ArrayList();
      while (!this.mLoadedViews.isEmpty())
      {
        View localView5 = (View)this.mLoadedViews.remove();
        localArrayList.add(localView5);
        detachViewFromParent(localView5);
      }
      View localView1;
      label97: View localView2;
      int j;
      label119: int l;
      View localView4;
      label162: LinkedList localLinkedList1;
      if (localArrayList.isEmpty())
      {
        localView1 = null;
        localView2 = makeAndAddView(i, true, localView1);
        this.mLoadedViews.addLast(localView2);
        j = 1;
        if (this.mSideBuffer - j < 0)
          break label265;
        int k = i - j;
        l = i + j;
        if (k >= 0)
        {
          LinkedList localLinkedList2 = this.mLoadedViews;
          if (!localArrayList.isEmpty())
            break label239;
          localView4 = null;
          localLinkedList2.addFirst(makeAndAddView(k, false, localView4));
        }
        if (l < this.mAdapter.getCount())
        {
          localLinkedList1 = this.mLoadedViews;
          if (!localArrayList.isEmpty())
            break label252;
        }
      }
      for (View localView3 = null; ; localView3 = (View)localArrayList.remove(0))
      {
        localLinkedList1.addLast(makeAndAddView(l, true, localView3));
        ++j;
        break label119:
        localView1 = (View)localArrayList.remove(0);
        break label97:
        label239: localView4 = (View)localArrayList.remove(0);
        label252: break label162:
      }
      label265: this.mCurrentBufferIndex = this.mLoadedViews.indexOf(localView2);
      this.mCurrentAdapterIndex = i;
      Iterator localIterator = localArrayList.iterator();
      while (localIterator.hasNext())
        removeDetachedView((View)localIterator.next(), false);
      requestLayout();
      setVisibleView(this.mCurrentBufferIndex, false);
      if (this.mIndicator == null)
        continue;
      this.mIndicator.onSwitched((View)this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
    }
    while (this.mViewSwitchListener == null);
    this.mViewSwitchListener.onSwitched((View)this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
  }

  class AdapterDataSetObserver extends DataSetObserver
  {
    AdapterDataSetObserver()
    {
    }

    public void onChanged()
    {
      View localView = ViewFlow.this.getChildAt(ViewFlow.this.mCurrentBufferIndex);
      if (localView != null);
      for (int i = 0; ; ++i)
      {
        if (i < ViewFlow.this.mAdapter.getCount())
        {
          if (!localView.equals(ViewFlow.this.mAdapter.getItem(i)))
            continue;
          ViewFlow.access$102(ViewFlow.this, i);
        }
        ViewFlow.this.resetFocus();
        return;
      }
    }

    public void onInvalidated()
    {
    }
  }

  public static abstract interface ViewSwitchListener
  {
    public abstract void onSwitched(View paramView, int paramInt);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.viewflow.ViewFlow
 * JD-Core Version:    0.5.4
 */