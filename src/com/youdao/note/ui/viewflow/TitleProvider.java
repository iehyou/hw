package com.youdao.note.ui.viewflow;

public abstract interface TitleProvider
{
  public abstract String getTitle(int paramInt);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.viewflow.TitleProvider
 * JD-Core Version:    0.5.4
 */