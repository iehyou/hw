package com.youdao.note.ui.viewflow;

public abstract interface FlowIndicator extends ViewFlow.ViewSwitchListener
{
  public abstract void onScrolled(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

  public abstract void setViewFlow(ViewFlow paramViewFlow);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.viewflow.FlowIndicator
 * JD-Core Version:    0.5.4
 */