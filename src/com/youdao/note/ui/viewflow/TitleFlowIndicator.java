package com.youdao.note.ui.viewflow;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.Adapter;
import android.widget.TextView;
import com.youdao.note.R.styleable;
import java.util.ArrayList;

public class TitleFlowIndicator extends TextView
  implements FlowIndicator
{
  private static final float CLIP_PADDING = 0.0F;
  private static final int FOOTER_COLOR = -15291;
  private static final float FOOTER_LINE_HEIGHT = 4.0F;
  private static final float FOOTER_TRIANGLE_HEIGHT = 10.0F;
  private static final int MONOSPACE = 3;
  private static final int SANS = 1;
  private static final boolean SELECTED_BOLD = false;
  private static final int SELECTED_COLOR = -15291;
  private static final int SERIF = 2;
  private static final int TEXT_COLOR = -5592406;
  private static final int TEXT_SIZE = 15;
  private static final float TITLE_PADDING = 10.0F;
  private float clipPadding;
  private int currentPosition = 0;
  private int currentScroll = 0;
  private float footerLineHeight;
  private float footerTriangleHeight;
  private Paint paintFooterLine;
  private Paint paintFooterTriangle;
  private Paint paintSelected;
  private Paint paintText;
  private Path path;
  private float titlePadding;
  private TitleProvider titleProvider = null;
  private Typeface typeface;
  private ViewFlow viewFlow;

  public TitleFlowIndicator(Context paramContext)
  {
    super(paramContext);
    initDraw(-5592406, 15.0F, -15291, false, 15.0F, 4.0F, -15291);
  }

  public TitleFlowIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i = paramAttributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "typeface", 0);
    int j = paramAttributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "textStyle", 0);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.TitleFlowIndicator);
    String str = localTypedArray.getString(10);
    int k = localTypedArray.getColor(8, -15291);
    this.footerLineHeight = localTypedArray.getDimension(7, 4.0F);
    this.footerTriangleHeight = localTypedArray.getDimension(9, 10.0F);
    int l = localTypedArray.getColor(2, -15291);
    boolean bool = localTypedArray.getBoolean(2, false);
    int i1 = localTypedArray.getColor(5, -5592406);
    float f1 = localTypedArray.getDimension(6, 15.0F);
    float f2 = localTypedArray.getDimension(4, f1);
    this.titlePadding = localTypedArray.getDimension(0, 10.0F);
    this.clipPadding = localTypedArray.getDimension(1, 0.0F);
    initDraw(i1, f1, l, bool, f2, this.footerLineHeight, k);
    if (str != null);
    for (this.typeface = Typeface.createFromAsset(paramContext.getAssets(), str); ; this.typeface = getTypefaceByIndex(i))
    {
      this.typeface = Typeface.create(this.typeface, j);
      return;
    }
  }

  private Rect calcBounds(int paramInt, Paint paramPaint)
  {
    String str = getTitle(paramInt);
    Rect localRect = new Rect();
    localRect.right = (int)paramPaint.measureText(str);
    localRect.bottom = (int)(paramPaint.descent() - paramPaint.ascent());
    return localRect;
  }

  private ArrayList<Rect> calculateAllBounds(Paint paramPaint)
  {
    ArrayList localArrayList = new ArrayList();
    if ((this.viewFlow != null) && (this.viewFlow.getAdapter() != null));
    for (int i = this.viewFlow.getAdapter().getCount(); ; i = 1)
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label155;
        Rect localRect = calcBounds(j, paramPaint);
        int k = localRect.right - localRect.left;
        int l = localRect.bottom - localRect.top;
        localRect.left = (getWidth() / 2 - k / 2 - this.currentScroll + j * getWidth());
        localRect.right = (k + localRect.left);
        localRect.top = 0;
        localRect.bottom = l;
        localArrayList.add(localRect);
      }
    label155: return localArrayList;
  }

  private void clipViewOnTheLeft(Rect paramRect, int paramInt)
  {
    paramRect.left = (0 + (int)this.clipPadding);
    paramRect.right = paramInt;
  }

  private void clipViewOnTheRight(Rect paramRect, int paramInt)
  {
    paramRect.right = (getLeft() + getWidth() - (int)this.clipPadding);
    paramRect.left = (paramRect.right - paramInt);
  }

  private String getTitle(int paramInt)
  {
    String str = "title " + paramInt;
    if (this.titleProvider != null)
      str = this.titleProvider.getTitle(paramInt);
    return str;
  }

  private Typeface getTypefaceByIndex(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return Typeface.DEFAULT;
    case 1:
      return Typeface.SANS_SERIF;
    case 2:
      return Typeface.SERIF;
    case 3:
    }
    return Typeface.MONOSPACE;
  }

  private void initDraw(int paramInt1, float paramFloat1, int paramInt2, boolean paramBoolean, float paramFloat2, float paramFloat3, int paramInt3)
  {
    this.paintText = new Paint();
    this.paintText.setColor(paramInt1);
    this.paintText.setTextSize(paramFloat1);
    this.paintText.setAntiAlias(true);
    this.paintSelected = new Paint();
    this.paintSelected.setColor(paramInt2);
    this.paintSelected.setTextSize(paramFloat2);
    this.paintSelected.setFakeBoldText(paramBoolean);
    this.paintSelected.setAntiAlias(true);
    this.paintFooterLine = new Paint();
    this.paintFooterLine.setStyle(Paint.Style.FILL_AND_STROKE);
    this.paintFooterLine.setStrokeWidth(paramFloat3);
    this.paintFooterLine.setColor(paramInt3);
    this.paintFooterTriangle = new Paint();
    this.paintFooterTriangle.setStyle(Paint.Style.FILL_AND_STROKE);
    this.paintFooterTriangle.setColor(paramInt3);
  }

  private int measureHeight(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    if (i == 1073741824)
      return j;
    Rect localRect = new Rect();
    localRect.bottom = (int)(this.paintText.descent() - this.paintText.ascent());
    return 10 + (localRect.bottom - localRect.top + (int)this.footerTriangleHeight + (int)this.footerLineHeight);
  }

  private int measureWidth(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    if (i != 1073741824)
      throw new IllegalStateException("ViewFlow can only be used in EXACTLY mode.");
    return j;
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    ArrayList localArrayList = calculateAllBounds(this.paintText);
    if ((this.viewFlow != null) && (this.viewFlow.getAdapter() != null));
    for (int i = this.viewFlow.getAdapter().getCount(); ; i = 1)
    {
      Rect localRect1 = (Rect)localArrayList.get(this.currentPosition);
      int j = localRect1.right - localRect1.left;
      if (localRect1.left < 0)
        clipViewOnTheLeft(localRect1, j);
      if (localRect1.right > getLeft() + getWidth())
        clipViewOnTheRight(localRect1, j);
      if (this.currentPosition <= 0)
        break;
      for (int i3 = -1 + this.currentPosition; ; --i3)
      {
        if (i3 < 0)
          break label250;
        Rect localRect5 = (Rect)localArrayList.get(i3);
        int i4 = localRect5.right - localRect5.left;
        if (localRect5.left >= 0)
          continue;
        clipViewOnTheLeft(localRect5, i4);
        if ((i3 >= i - 1) || (this.currentPosition == i3))
          continue;
        Rect localRect6 = (Rect)localArrayList.get(i3 + 1);
        if (10.0F + localRect5.right <= localRect6.left)
          continue;
        localRect5.left = (localRect6.left - (i4 + (int)this.titlePadding));
      }
    }
    if (this.currentPosition < i - 1)
      for (int i1 = 1 + this.currentPosition; i1 < i; ++i1)
      {
        label250: Rect localRect3 = (Rect)localArrayList.get(i1);
        int i2 = localRect3.right - localRect3.left;
        if (localRect3.right <= getLeft() + getWidth())
          continue;
        clipViewOnTheRight(localRect3, i2);
        if ((i1 <= 0) || (this.currentPosition == i1))
          continue;
        Rect localRect4 = (Rect)localArrayList.get(i1 - 1);
        if (localRect3.left - 10.0F >= localRect4.right)
          continue;
        localRect3.left = (localRect4.right + (int)this.titlePadding);
      }
    for (int k = 0; k < i; ++k)
    {
      String str = getTitle(k);
      Rect localRect2 = (Rect)localArrayList.get(k);
      if ((((localRect2.left <= getLeft()) || (localRect2.left >= getLeft() + getWidth()))) && (((localRect2.right <= getLeft()) || (localRect2.right >= getLeft() + getWidth()))))
        continue;
      Paint localPaint = this.paintText;
      if (Math.abs((localRect2.left + localRect2.right) / 2 - getWidth() / 2) < 20)
        localPaint = this.paintSelected;
      localPaint.setTypeface(this.typeface);
      paramCanvas.drawText(str, localRect2.left, localRect2.bottom, localPaint);
    }
    this.path = new Path();
    float f1 = -1 + getHeight();
    float f2;
    if (this.footerLineHeight % 2.0F == 1.0F)
      f2 = this.footerLineHeight / 2.0F;
    while (true)
    {
      int l = (int)(f1 - f2);
      this.path.moveTo(0.0F, l);
      this.path.lineTo(getWidth(), l);
      this.path.close();
      paramCanvas.drawPath(this.path, this.paintFooterLine);
      this.path = new Path();
      this.path.moveTo(getWidth() / 2, getHeight() - this.footerLineHeight - this.footerTriangleHeight);
      this.path.lineTo(getWidth() / 2 + this.footerTriangleHeight, getHeight() - this.footerLineHeight);
      this.path.lineTo(getWidth() / 2 - this.footerTriangleHeight, getHeight() - this.footerLineHeight);
      this.path.close();
      paramCanvas.drawPath(this.path, this.paintFooterTriangle);
      return;
      f2 = this.footerLineHeight / 2.0F - 1.0F;
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(measureWidth(paramInt1), measureHeight(paramInt2));
  }

  public void onScrolled(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.currentScroll = paramInt1;
    invalidate();
  }

  public void onSwitched(View paramView, int paramInt)
  {
    this.currentPosition = paramInt;
    invalidate();
  }

  public void setTitleProvider(TitleProvider paramTitleProvider)
  {
    this.titleProvider = paramTitleProvider;
  }

  public void setViewFlow(ViewFlow paramViewFlow)
  {
    this.viewFlow = paramViewFlow;
    invalidate();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.viewflow.TitleFlowIndicator
 * JD-Core Version:    0.5.4
 */