package com.youdao.note.ui.viewflow;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import com.youdao.note.R.styleable;

public class CircleFlowIndicator extends View
  implements FlowIndicator, Animation.AnimationListener
{
  private static final int BORDER = 2;
  private static final int STYLE_FILL = 1;
  private static final int STYLE_STROKE;
  private Animation animation;
  public Animation.AnimationListener animationListener = this;
  private int currentScroll = 0;
  private int fadeOutTime = 0;
  private int flowWidth = 0;
  private final Paint mPaintActive = new Paint(5);
  private final Paint mPaintInactive = new Paint(5);
  private float radius = 4.0F;
  private FadeTimer timer;
  private ViewFlow viewFlow;

  public CircleFlowIndicator(Context paramContext)
  {
    super(paramContext);
    initColors(-1, -1, 1, 0);
  }

  public CircleFlowIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CircleFlowIndicator);
    int i = localTypedArray.getInt(6, 1);
    int j = localTypedArray.getColor(0, -1);
    int k = localTypedArray.getInt(5, 0);
    int l = localTypedArray.getColor(1, 1157627903);
    this.radius = localTypedArray.getDimension(2, 4.0F);
    this.fadeOutTime = localTypedArray.getInt(4, 0);
    this.mPaintInactive.setStrokeWidth(2.0F);
    initColors(j, l, i, k);
  }

  private void initColors(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    switch (paramInt4)
    {
    default:
      this.mPaintInactive.setStyle(Paint.Style.STROKE);
      label30: this.mPaintInactive.setColor(paramInt2);
      switch (paramInt3)
      {
      default:
        this.mPaintActive.setStyle(Paint.Style.FILL);
      case 0:
      }
    case 1:
    }
    while (true)
    {
      this.mPaintActive.setColor(paramInt1);
      return;
      this.mPaintInactive.setStyle(Paint.Style.FILL);
      break label30:
      this.mPaintActive.setStyle(Paint.Style.STROKE);
    }
  }

  private int measureHeight(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    int k;
    if (i == 1073741824)
      k = j;
    do
    {
      return k;
      k = 4 + (int)(1.0F + (2.0F * (2.0F + this.radius) + getPaddingTop() + getPaddingBottom()));
    }
    while (i != -2147483648);
    return Math.min(k, j);
  }

  private int measureWidth(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    int l;
    if (i == 1073741824)
      l = j;
    do
    {
      return l;
      int k = 3;
      if (this.viewFlow != null)
        k = this.viewFlow.getViewsCount();
      l = (int)(getPaddingLeft() + getPaddingRight() + (-1 + k * 3) * this.radius + 2 * (k * 2));
    }
    while (i != -2147483648);
    return Math.min(l, j);
  }

  private void resetTimer()
  {
    if (this.fadeOutTime > 0)
    {
      if ((this.timer != null) && (this.timer._run))
        break label50;
      this.timer = new FadeTimer(null);
      this.timer.execute(new Void[0]);
    }
    return;
    label50: this.timer.resetTimer();
  }

  public void onAnimationEnd(Animation paramAnimation)
  {
    setVisibility(8);
  }

  public void onAnimationRepeat(Animation paramAnimation)
  {
  }

  public void onAnimationStart(Animation paramAnimation)
  {
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = 3;
    if (this.viewFlow != null)
      i = this.viewFlow.getViewsCount();
    float f1 = 2.0F * (2.0F + this.radius) + this.radius;
    int j = getPaddingLeft();
    for (int k = 0; k < i; ++k)
      paramCanvas.drawCircle(2.0F + (0.0F + (j + this.radius + f1 * k)), 2.0F + (getPaddingTop() + this.radius), this.radius, this.mPaintInactive);
    int l = this.flowWidth;
    float f2 = 0.0F;
    if (l != 0)
      f2 = this.currentScroll * (2.0F * (2.0F + this.radius) + this.radius) / this.flowWidth;
    paramCanvas.drawCircle(0.0F + (f2 + (2.0F + (j + this.radius))), 2.0F + (getPaddingTop() + this.radius), this.radius, this.mPaintActive);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(measureWidth(paramInt1), measureHeight(paramInt2));
  }

  public void onScrolled(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    setVisibility(0);
    resetTimer();
    this.currentScroll = paramInt1;
    this.flowWidth = this.viewFlow.getWidth();
    invalidate();
  }

  public void onSwitched(View paramView, int paramInt)
  {
  }

  public void setFillColor(int paramInt)
  {
    this.mPaintActive.setColor(paramInt);
    invalidate();
  }

  public void setStrokeColor(int paramInt)
  {
    this.mPaintInactive.setColor(paramInt);
    invalidate();
  }

  public void setViewFlow(ViewFlow paramViewFlow)
  {
    resetTimer();
    this.viewFlow = paramViewFlow;
    this.flowWidth = this.viewFlow.getWidth();
    invalidate();
  }

  private class FadeTimer extends AsyncTask<Void, Void, Void>
  {
    private boolean _run = true;
    private int timer = 0;

    private FadeTimer()
    {
    }

    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      while (this._run)
        try
        {
          Thread.sleep(1L);
          this.timer = (1 + this.timer);
          if (this.timer == CircleFlowIndicator.this.fadeOutTime);
          this._run = false;
        }
        catch (InterruptedException localInterruptedException)
        {
          localInterruptedException.printStackTrace();
        }
      return null;
    }

    protected void onPostExecute(Void paramVoid)
    {
      CircleFlowIndicator.access$302(CircleFlowIndicator.this, AnimationUtils.loadAnimation(CircleFlowIndicator.this.getContext(), 17432577));
      CircleFlowIndicator.this.animation.setAnimationListener(CircleFlowIndicator.this.animationListener);
      CircleFlowIndicator.this.startAnimation(CircleFlowIndicator.this.animation);
    }

    public void resetTimer()
    {
      this.timer = 0;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.viewflow.CircleFlowIndicator
 * JD-Core Version:    0.5.4
 */