package com.youdao.note.ui;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.L;
import com.youdao.note.utils.StringUtils;

public class DialogFactory
{
  public static Dialog getAboutDialog(Activity paramActivity)
  {
    YNoteDialog localYNoteDialog = new YNoteDialog(paramActivity, 2131361849);
    View localView = LayoutInflater.from(paramActivity).inflate(2130903040, null);
    localView.findViewById(2131165186).setOnClickListener(new View.OnClickListener(paramActivity)
    {
      public void onClick(View paramView)
      {
        DialogFactory.access$000(this.val$activity, this.val$activity.getResources().getString(2131361848));
      }
    });
    localYNoteDialog.setView(localView);
    return localYNoteDialog;
  }

  public static ProgressDialog getCancleableProgressDialog(int paramInt1, int paramInt2, Activity paramActivity, int paramInt3)
  {
    return getCancleableProgressDialog(paramActivity.getResources().getString(paramInt1), paramInt2, paramActivity, paramInt3, null);
  }

  public static ProgressDialog getCancleableProgressDialog(String paramString, int paramInt1, Activity paramActivity, int paramInt2, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    ProgressDialog localProgressDialog = new ProgressDialog(paramActivity);
    if (!TextUtils.isEmpty(paramString))
      localProgressDialog.setTitle(paramString);
    if (paramInt1 != -1)
      localProgressDialog.setMessage(paramActivity.getResources().getString(paramInt1));
    localProgressDialog.setIndeterminate(false);
    localProgressDialog.setProgressStyle(paramInt2);
    if (paramOnCancelListener != null)
      localProgressDialog.setOnCancelListener(paramOnCancelListener);
    return localProgressDialog;
  }

  public static Dialog getDeleteConfirmDialog(Activity paramActivity, NoteMeta paramNoteMeta, NoteDeletedCallback paramNoteDeletedCallback)
  {
    return new AlertDialog.Builder(paramActivity).setTitle("确认删除？").setMessage("删除后无法恢复").setPositiveButton(2131361963, new DialogInterface.OnClickListener(paramActivity, paramNoteMeta, paramNoteDeletedCallback)
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        YNoteApplication.getInstance().markDeleteNote(this.val$activity, this.val$deletedNoteMeta);
        this.val$callback.onNoteDeleted();
      }
    }).setNegativeButton(2131361962, null).create();
  }

  public static Dialog getNewFeaturesDialog(Activity paramActivity)
  {
    return new AlertDialog.Builder(paramActivity).setTitle(2131362006).setMessage(2131362007).setCancelable(true).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        YNoteApplication.getInstance().setNotFirst();
      }
    }).create();
  }

  public static Dialog getNoteBookPickDialog(Activity paramActivity, String paramString, NotebookChooseCallback paramNotebookChooseCallback)
  {
    NoteBook[] arrayOfNoteBook = YNoteApplication.getInstance().getDataSource().listAllNoteBooksAsArray();
    int k;
    String str2;
    String[] arrayOfString;
    for (int i = 0; ; ++i)
    {
      int j = arrayOfNoteBook.length;
      k = 0;
      String str1 = null;
      if (i < j)
      {
        if (!paramString.equals(arrayOfNoteBook[i].getNoteBookId()))
          continue;
        k = i;
        str1 = arrayOfNoteBook[i].getNoteBookId();
      }
      str2 = str1;
      arrayOfString = new String[arrayOfNoteBook.length];
      for (int l = 0; ; ++l)
      {
        if (l >= arrayOfNoteBook.length)
          break label102;
        arrayOfString[l] = arrayOfNoteBook[l].getTitle();
      }
    }
    label102: return new AlertDialog.Builder(paramActivity).setTitle("选择笔记本").setSingleChoiceItems(arrayOfString, k, new DialogInterface.OnClickListener(paramActivity, paramNotebookChooseCallback, str2, arrayOfNoteBook)
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        this.val$activity.dismissDialog(104);
        if (this.val$callback == null)
          return;
        this.val$callback.onNoteMoved(this.val$oriNoteBookId, this.val$noteBooks[paramInt].getNoteBookId());
      }
    }).setNegativeButton(2131361962, null).create();
  }

  public static Dialog getNoteDetailDialog(Activity paramActivity, String paramString)
  {
    DataSource localDataSource = YNoteApplication.getInstance().getDataSource();
    NoteMeta localNoteMeta = localDataSource.getNoteMetaById(paramString);
    String str1;
    label26: YNoteDialog localYNoteDialog;
    View localView;
    if (!YNoteApplication.getInstance().isLogin())
    {
      str1 = "未登录笔记本";
      String str2 = localNoteMeta.getSourceUrl().toLowerCase();
      localYNoteDialog = new YNoteDialog(paramActivity, 2131361957);
      localView = LayoutInflater.from(paramActivity).inflate(2130903091, null);
      ((TextView)localView.findViewById(2131165325)).setText("笔记本：" + str1);
      ((TextView)localView.findViewById(2131165326)).setText("更新时间：" + StringUtils.getPrettyTime(localNoteMeta.getModifyTime()));
      ((TextView)localView.findViewById(2131165327)).setText("创建时间：" + StringUtils.getPrettyTime(localNoteMeta.getCreateTime()));
      if (TextUtils.isEmpty(str2))
        break label236;
      TextView localTextView = (TextView)localView.findViewById(2131165329);
      localTextView.setText(str2);
      localTextView.setOnClickListener(new View.OnClickListener(str2, paramActivity)
      {
        public void onClick(View paramView)
        {
          String str = this.val$sourceUrl;
          if (!this.val$sourceUrl.startsWith("http://"))
            str = "http://" + str;
          DialogFactory.access$000(this.val$activity, str);
        }
      });
    }
    while (true)
    {
      localYNoteDialog.setView(localView);
      return localYNoteDialog;
      str1 = localDataSource.getNoteBookMetaById(localNoteMeta.getNoteBook()).getTitle();
      break label26:
      label236: localView.findViewById(2131165328).setVisibility(8);
    }
  }

  public static Dialog getNoteLongClickMenu(Activity paramActivity, NoteMeta paramNoteMeta)
  {
    return new AlertDialog.Builder(paramActivity).setTitle(paramNoteMeta.getTitle()).setItems(2131099650, new DialogInterface.OnClickListener(paramActivity, paramNoteMeta)
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        switch (paramInt)
        {
        default:
          return;
        case 0:
          YNoteApplication.getInstance().sendEditNote(this.val$activity, this.val$noteMeta.getNoteId());
          return;
        case 1:
          this.val$activity.showDialog(100);
          return;
        case 2:
        }
        this.val$activity.showDialog(105);
      }
    }).create();
  }

  private static void sendUrl(Activity paramActivity, String paramString)
  {
    try
    {
      Intent localIntent = new Intent();
      localIntent.setData(Uri.parse(paramString));
      localIntent.setAction("android.intent.action.VIEW");
      paramActivity.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      L.e(DialogFactory.class, "sb..", localActivityNotFoundException);
    }
  }

  public static abstract interface NoteDeletedCallback
  {
    public abstract void onNoteDeleted();
  }

  public static abstract interface NotebookChooseCallback
  {
    public abstract void onNoteMoved(String paramString1, String paramString2);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.DialogFactory
 * JD-Core Version:    0.5.4
 */