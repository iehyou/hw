package com.youdao.note.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AlphabetScrollbar extends LinearLayout
{
  public static final String BUNDLE_ALPHABET = "alphabet";
  public static final int MSG_ALPHABET = 1;
  private static final String[] alphabet = { "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
  private Context context;
  private boolean firstDraw = true;
  public Handler handler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      String str;
      int i;
      if (paramMessage.what == 1)
      {
        str = paramMessage.getData().getString("alphabet");
        i = -1;
      }
      for (int j = 0; ; ++j)
      {
        if (j < AlphabetScrollbar.alphabet.length)
        {
          if (!AlphabetScrollbar.alphabet[j].equals(str))
            continue;
          i = j;
        }
        if (i != -1)
        {
          AlphabetScrollbar.this.hightLightAlphabet(i);
          AlphabetScrollbar.this.postInvalidate();
        }
        super.handleMessage(paramMessage);
        return;
      }
    }
  };
  private int lastIndex = -1;
  private AlphabetView lastSelectedView;
  private OnAlphabetListener listener;
  private int scrollbarTop;

  public AlphabetScrollbar(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    init();
  }

  public AlphabetScrollbar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    init();
  }

  public AlphabetScrollbar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    init();
  }

  private void hightLightAlphabet(int paramInt)
  {
    if (paramInt < 0)
      paramInt = 0;
    if (paramInt > 26)
      paramInt = 26;
    this.lastSelectedView = ((AlphabetView)getChildAt(paramInt));
  }

  private void init()
  {
    String[] arrayOfString = alphabet;
    int i = arrayOfString.length;
    for (int j = 0; j < i; ++j)
      addView(new AlphabetView(arrayOfString[j]));
    setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
      {
        if ((paramMotionEvent.getAction() == 4) || (paramMotionEvent.getAction() == 1));
        int i;
        do
        {
          do
          {
            do
              return true;
            while ((paramMotionEvent.getAction() != 0) && (paramMotionEvent.getAction() != 2));
            AlphabetScrollbar.this.show();
            i = (int)((paramMotionEvent.getRawY() - AlphabetScrollbar.this.scrollbarTop) / (AlphabetScrollbar.this.getHeight() / 27.0F));
            if (i < 0)
              i = 0;
            if (i <= 26)
              continue;
            i = 26;
          }
          while (i == AlphabetScrollbar.this.lastIndex);
          AlphabetScrollbar.this.hightLightAlphabet(i);
          AlphabetScrollbar.access$102(AlphabetScrollbar.this, i);
        }
        while (AlphabetScrollbar.this.listener == null);
        String str = AlphabetScrollbar.alphabet[i];
        AlphabetScrollbar.this.listener.onAlphabetSelected(str);
        return true;
      }
    });
  }

  public void hide()
  {
    setBackgroundColor(0);
    setTextColor(0);
    postInvalidate();
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (!this.firstDraw)
      return;
    int[] arrayOfInt = new int[2];
    getLocationOnScreen(arrayOfInt);
    this.scrollbarTop = arrayOfInt[1];
    this.firstDraw = false;
  }

  public void setOnAlphabetListener(OnAlphabetListener paramOnAlphabetListener)
  {
    this.listener = paramOnAlphabetListener;
  }

  public void setTextColor(int paramInt)
  {
    int i = getChildCount();
    for (int j = 0; j < i; ++j)
    {
      View localView = getChildAt(j);
      if (!localView instanceof AlphabetView)
        continue;
      ((AlphabetView)localView).setTextColor(paramInt);
    }
  }

  public void show()
  {
    setBackgroundDrawable(getContext().getResources().getDrawable(2130837510));
    setTextColor(Color.parseColor("#FFFFFF"));
    postInvalidate();
  }

  private class AlphabetView extends TextView
  {
    public AlphabetView(String arg2)
    {
      super(AlphabetScrollbar.this.context);
      Object localObject;
      setTag(localObject);
      setText(localObject);
      setBackgroundColor(0);
      setTextColor(Color.parseColor("#FFFFFF"));
      setTextSize(2, 10.0F);
      setGravity(17);
      setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0F));
    }
  }

  public static abstract interface OnAlphabetListener
  {
    public abstract void onAlphabetSelected(String paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.AlphabetScrollbar
 * JD-Core Version:    0.5.4
 */