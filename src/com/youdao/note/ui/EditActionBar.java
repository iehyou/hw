package com.youdao.note.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class EditActionBar extends LinearLayout
{
  public EditActionBar(Context paramContext)
  {
    this(paramContext, null);
  }

  public EditActionBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LayoutInflater.from(paramContext).inflate(2130903053, this, true);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.EditActionBar
 * JD-Core Version:    0.5.4
 */