package com.youdao.note.ui.skitch.tool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.youdao.note.YNoteApplication;
import com.youdao.note.ui.skitch.SkitchConsts.Doodle;
import com.youdao.note.ui.skitch.doodle.DoodleView;

public class ColorBoxView extends AbstractPaintTool
  implements SkitchConsts.Doodle
{
  private static final Bitmap color1 = BitmapFactory.decodeResource(YNoteApplication.getInstance().getResources(), 2130837534);
  public static final int rgb1 = color1.getPixel(color1.getWidth() / 2, color1.getHeight() / 2);
  private final Bitmap color2 = BitmapFactory.decodeResource(getResources(), 2130837535);
  private final Bitmap color3 = BitmapFactory.decodeResource(getResources(), 2130837536);
  private final Bitmap color4 = BitmapFactory.decodeResource(getResources(), 2130837537);
  private final Bitmap color5 = BitmapFactory.decodeResource(getResources(), 2130837538);
  private final Bitmap color6 = BitmapFactory.decodeResource(getResources(), 2130837539);
  private Bitmap[] colors;
  private final Bitmap colors1;
  private final Bitmap colors2;
  private final Bitmap colors3;
  private final Bitmap colors4;
  private final Bitmap colors5;
  private final Bitmap colors6;
  private RectF r1;
  private RectF r2;
  private RectF r3;
  private RectF r4;
  private RectF r5;
  private RectF r6;
  private RectF[] rects;
  private final int rgb2;
  private final int rgb3;
  private final int rgb4;
  private final int rgb5;
  private final int rgb6;
  private final int[] rgbs;
  private final Bitmap[] selectedColors;
  private final Bitmap[] unSelectedColors;

  public ColorBoxView(Context paramContext)
  {
    super(paramContext);
    Bitmap[] arrayOfBitmap1 = new Bitmap[6];
    arrayOfBitmap1[0] = color1;
    arrayOfBitmap1[1] = this.color2;
    arrayOfBitmap1[2] = this.color3;
    arrayOfBitmap1[3] = this.color4;
    arrayOfBitmap1[4] = this.color5;
    arrayOfBitmap1[5] = this.color6;
    this.unSelectedColors = arrayOfBitmap1;
    this.colors1 = BitmapFactory.decodeResource(getResources(), 2130837540);
    this.colors2 = BitmapFactory.decodeResource(getResources(), 2130837541);
    this.colors3 = BitmapFactory.decodeResource(getResources(), 2130837542);
    this.colors4 = BitmapFactory.decodeResource(getResources(), 2130837543);
    this.colors5 = BitmapFactory.decodeResource(getResources(), 2130837544);
    this.colors6 = BitmapFactory.decodeResource(getResources(), 2130837545);
    Bitmap[] arrayOfBitmap2 = new Bitmap[6];
    arrayOfBitmap2[0] = this.colors1;
    arrayOfBitmap2[1] = this.colors2;
    arrayOfBitmap2[2] = this.colors3;
    arrayOfBitmap2[3] = this.colors4;
    arrayOfBitmap2[4] = this.colors5;
    arrayOfBitmap2[5] = this.colors6;
    this.selectedColors = arrayOfBitmap2;
    this.r1 = new RectF();
    this.r2 = new RectF();
    this.r3 = new RectF();
    this.r4 = new RectF();
    this.r5 = new RectF();
    this.r6 = new RectF();
    RectF[] arrayOfRectF = new RectF[6];
    arrayOfRectF[0] = this.r1;
    arrayOfRectF[1] = this.r2;
    arrayOfRectF[2] = this.r3;
    arrayOfRectF[3] = this.r4;
    arrayOfRectF[4] = this.r5;
    arrayOfRectF[5] = this.r6;
    this.rects = arrayOfRectF;
    this.colors = new Bitmap[6];
    this.rgb2 = this.color2.getPixel(this.color2.getWidth() / 2, this.color2.getHeight() / 2);
    this.rgb3 = this.color3.getPixel(this.color3.getWidth() / 2, this.color3.getHeight() / 2);
    this.rgb4 = this.color4.getPixel(this.color4.getWidth() / 2, this.color4.getHeight() / 2);
    this.rgb5 = this.color5.getPixel(this.color5.getWidth() / 2, this.color5.getHeight() / 2);
    this.rgb6 = this.color6.getPixel(this.color6.getWidth() / 2, this.color6.getHeight() / 2);
    int[] arrayOfInt = new int[6];
    arrayOfInt[0] = rgb1;
    arrayOfInt[1] = this.rgb2;
    arrayOfInt[2] = this.rgb3;
    arrayOfInt[3] = this.rgb4;
    arrayOfInt[4] = this.rgb5;
    arrayOfInt[5] = this.rgb6;
    this.rgbs = arrayOfInt;
  }

  public ColorBoxView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Bitmap[] arrayOfBitmap1 = new Bitmap[6];
    arrayOfBitmap1[0] = color1;
    arrayOfBitmap1[1] = this.color2;
    arrayOfBitmap1[2] = this.color3;
    arrayOfBitmap1[3] = this.color4;
    arrayOfBitmap1[4] = this.color5;
    arrayOfBitmap1[5] = this.color6;
    this.unSelectedColors = arrayOfBitmap1;
    this.colors1 = BitmapFactory.decodeResource(getResources(), 2130837540);
    this.colors2 = BitmapFactory.decodeResource(getResources(), 2130837541);
    this.colors3 = BitmapFactory.decodeResource(getResources(), 2130837542);
    this.colors4 = BitmapFactory.decodeResource(getResources(), 2130837543);
    this.colors5 = BitmapFactory.decodeResource(getResources(), 2130837544);
    this.colors6 = BitmapFactory.decodeResource(getResources(), 2130837545);
    Bitmap[] arrayOfBitmap2 = new Bitmap[6];
    arrayOfBitmap2[0] = this.colors1;
    arrayOfBitmap2[1] = this.colors2;
    arrayOfBitmap2[2] = this.colors3;
    arrayOfBitmap2[3] = this.colors4;
    arrayOfBitmap2[4] = this.colors5;
    arrayOfBitmap2[5] = this.colors6;
    this.selectedColors = arrayOfBitmap2;
    this.r1 = new RectF();
    this.r2 = new RectF();
    this.r3 = new RectF();
    this.r4 = new RectF();
    this.r5 = new RectF();
    this.r6 = new RectF();
    RectF[] arrayOfRectF = new RectF[6];
    arrayOfRectF[0] = this.r1;
    arrayOfRectF[1] = this.r2;
    arrayOfRectF[2] = this.r3;
    arrayOfRectF[3] = this.r4;
    arrayOfRectF[4] = this.r5;
    arrayOfRectF[5] = this.r6;
    this.rects = arrayOfRectF;
    this.colors = new Bitmap[6];
    this.rgb2 = this.color2.getPixel(this.color2.getWidth() / 2, this.color2.getHeight() / 2);
    this.rgb3 = this.color3.getPixel(this.color3.getWidth() / 2, this.color3.getHeight() / 2);
    this.rgb4 = this.color4.getPixel(this.color4.getWidth() / 2, this.color4.getHeight() / 2);
    this.rgb5 = this.color5.getPixel(this.color5.getWidth() / 2, this.color5.getHeight() / 2);
    this.rgb6 = this.color6.getPixel(this.color6.getWidth() / 2, this.color6.getHeight() / 2);
    int[] arrayOfInt = new int[6];
    arrayOfInt[0] = rgb1;
    arrayOfInt[1] = this.rgb2;
    arrayOfInt[2] = this.rgb3;
    arrayOfInt[3] = this.rgb4;
    arrayOfInt[4] = this.rgb5;
    arrayOfInt[5] = this.rgb6;
    this.rgbs = arrayOfInt;
  }

  public ColorBoxView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Bitmap[] arrayOfBitmap1 = new Bitmap[6];
    arrayOfBitmap1[0] = color1;
    arrayOfBitmap1[1] = this.color2;
    arrayOfBitmap1[2] = this.color3;
    arrayOfBitmap1[3] = this.color4;
    arrayOfBitmap1[4] = this.color5;
    arrayOfBitmap1[5] = this.color6;
    this.unSelectedColors = arrayOfBitmap1;
    this.colors1 = BitmapFactory.decodeResource(getResources(), 2130837540);
    this.colors2 = BitmapFactory.decodeResource(getResources(), 2130837541);
    this.colors3 = BitmapFactory.decodeResource(getResources(), 2130837542);
    this.colors4 = BitmapFactory.decodeResource(getResources(), 2130837543);
    this.colors5 = BitmapFactory.decodeResource(getResources(), 2130837544);
    this.colors6 = BitmapFactory.decodeResource(getResources(), 2130837545);
    Bitmap[] arrayOfBitmap2 = new Bitmap[6];
    arrayOfBitmap2[0] = this.colors1;
    arrayOfBitmap2[1] = this.colors2;
    arrayOfBitmap2[2] = this.colors3;
    arrayOfBitmap2[3] = this.colors4;
    arrayOfBitmap2[4] = this.colors5;
    arrayOfBitmap2[5] = this.colors6;
    this.selectedColors = arrayOfBitmap2;
    this.r1 = new RectF();
    this.r2 = new RectF();
    this.r3 = new RectF();
    this.r4 = new RectF();
    this.r5 = new RectF();
    this.r6 = new RectF();
    RectF[] arrayOfRectF = new RectF[6];
    arrayOfRectF[0] = this.r1;
    arrayOfRectF[1] = this.r2;
    arrayOfRectF[2] = this.r3;
    arrayOfRectF[3] = this.r4;
    arrayOfRectF[4] = this.r5;
    arrayOfRectF[5] = this.r6;
    this.rects = arrayOfRectF;
    this.colors = new Bitmap[6];
    this.rgb2 = this.color2.getPixel(this.color2.getWidth() / 2, this.color2.getHeight() / 2);
    this.rgb3 = this.color3.getPixel(this.color3.getWidth() / 2, this.color3.getHeight() / 2);
    this.rgb4 = this.color4.getPixel(this.color4.getWidth() / 2, this.color4.getHeight() / 2);
    this.rgb5 = this.color5.getPixel(this.color5.getWidth() / 2, this.color5.getHeight() / 2);
    this.rgb6 = this.color6.getPixel(this.color6.getWidth() / 2, this.color6.getHeight() / 2);
    int[] arrayOfInt = new int[6];
    arrayOfInt[0] = rgb1;
    arrayOfInt[1] = this.rgb2;
    arrayOfInt[2] = this.rgb3;
    arrayOfInt[3] = this.rgb4;
    arrayOfInt[4] = this.rgb5;
    arrayOfInt[5] = this.rgb6;
    this.rgbs = arrayOfInt;
  }

  private int currentColor()
  {
    return ((DoodleView)this.skitchCanvas).getPaintColor();
  }

  private void selectColor(int paramInt)
  {
    int i = 0;
    if (i < 6)
    {
      if (i == paramInt)
        label2: this.colors[i] = this.selectedColors[i];
      while (true)
      {
        ++i;
        break label2:
        this.colors[i] = this.unSelectedColors[i];
      }
    }
    setColor(this.rgbs[paramInt]);
  }

  private void setColor(int paramInt)
  {
    ((DoodleView)this.skitchCanvas).setPaintColor(paramInt);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = currentColor();
    for (int j = 0; ; ++j)
    {
      if (j < 6)
      {
        if (i != this.rgbs[j])
          continue;
        selectColor(j);
      }
      for (int k = 0; ; ++k)
      {
        if (k >= 6)
          return;
        paramCanvas.drawBitmap(this.colors[k], new Rect(0, 0, this.colors[k].getWidth(), this.colors[k].getHeight()), this.rects[k], null);
      }
    }
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    float f1 = (paramInt1 - 3.0F * PAD - 5.0F * PAD) / 6.0F;
    float f2 = paramInt2 - 2.0F * MARGIN_VERTICAL;
    float f3;
    if (f1 < f2)
      f3 = f1;
    while (true)
    {
      float f4 = paramInt2 / 2 - f3 / 2.0F;
      float f5 = f4 + f3;
      float f6 = (paramInt1 - 6.0F * f3) / 8.0F;
      float f7 = (paramInt1 - 6.0F * f3 - 5.0F * f6) / 2.0F;
      int i = 0;
      while (true)
      {
        if (i >= 6)
          return;
        this.rects[i].set(f7, f4, f7 + f3, f5);
        ++i;
        f7 += f6 + f3;
      }
      f3 = f2;
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 0)
      for (int i = 0; i < this.rects.length; ++i)
      {
        if (!this.rects[i].contains(paramMotionEvent.getX(), paramMotionEvent.getY()))
          continue;
        this.dismissTimer.setLastTouchTime(Long.valueOf(System.currentTimeMillis()));
        setColor(this.rgbs[i]);
        postInvalidate();
        return true;
      }
    return super.onTouchEvent(paramMotionEvent);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.tool.ColorBoxView
 * JD-Core Version:    0.5.4
 */