package com.youdao.note.ui.skitch.tool;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import com.youdao.note.ui.skitch.ISkitch;

public class AbstractPaintTool extends View
{
  protected DismissTimer dismissTimer;
  protected ISkitch skitchCanvas;

  public AbstractPaintTool(Context paramContext)
  {
    super(paramContext);
  }

  public AbstractPaintTool(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public AbstractPaintTool(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void dismiss()
  {
    if (getVisibility() != 0)
      return;
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.1F);
    localAlphaAnimation.setDuration(500L);
    setAnimation(localAlphaAnimation);
    setVisibility(4);
  }

  public void setSkitchCanvas(ISkitch paramISkitch)
  {
    this.skitchCanvas = paramISkitch;
  }

  public void show()
  {
    if (getVisibility() == 0)
      return;
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(0.1F, 1.0F);
    localAlphaAnimation.setDuration(500L);
    startAnimation(localAlphaAnimation);
    setVisibility(0);
    startDismissTimer();
  }

  public void startDismissTimer()
  {
    if (this.dismissTimer != null)
      this.dismissTimer.cancel(true);
    this.dismissTimer = new DismissTimer(Long.valueOf(System.currentTimeMillis()));
    this.dismissTimer.execute(new Void[0]);
  }

  protected class DismissTimer extends AsyncTask<Void, Void, Void>
  {
    private static final long dismissTime = 3000L;
    private Long lastTouchTime;

    public DismissTimer(Long arg2)
    {
      Object localObject;
      this.lastTouchTime = localObject;
    }

    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      while (true)
      {
        long l = 3000L - (System.currentTimeMillis() - getLastTouchTime().longValue());
        if (l <= 0L)
          return null;
        try
        {
          Thread.sleep(l);
        }
        catch (InterruptedException localInterruptedException)
        {
        }
      }
    }

    public Long getLastTouchTime()
    {
      synchronized (this.lastTouchTime)
      {
        Long localLong2 = this.lastTouchTime;
        return localLong2;
      }
    }

    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
      AbstractPaintTool.this.dismiss();
    }

    public void setLastTouchTime(Long paramLong)
    {
      synchronized (this.lastTouchTime)
      {
        this.lastTouchTime = paramLong;
        return;
      }
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.tool.AbstractPaintTool
 * JD-Core Version:    0.5.4
 */