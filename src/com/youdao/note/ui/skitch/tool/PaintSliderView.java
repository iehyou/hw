package com.youdao.note.ui.skitch.tool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.youdao.note.ui.skitch.ISkitch;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;

public class PaintSliderView extends AbstractPaintTool
  implements SkitchConsts.HandWrite
{
  private final Bitmap sliderBg = BitmapFactory.decodeResource(getResources(), 2130837609);
  private final Bitmap sliderBlock = BitmapFactory.decodeResource(getResources(), 2130837610);
  private final Bitmap sliderBlue = BitmapFactory.decodeResource(getResources(), 2130837611);
  private final Bitmap sliderBrown = BitmapFactory.decodeResource(getResources(), 2130837612);

  public PaintSliderView(Context paramContext)
  {
    super(paramContext);
  }

  public PaintSliderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public PaintSliderView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private float currentWidth()
  {
    return this.skitchCanvas.getPaintWidth();
  }

  private float getSliderValue()
  {
    float f = SLIDER_L_PAD + MIN_PAINT_WIDTH + SLIDER_L_PAD;
    return (getWidth() - SLIDER_R_PAD - MAX_PAINT_WIDTH - SLIDER_R_PAD - f) * ((currentWidth() - MIN_PAINT_WIDTH) / (MAX_PAINT_WIDTH - MIN_PAINT_WIDTH));
  }

  private void updateCurrentWidth(float paramFloat)
  {
    float f1 = SLIDER_L_PAD + MIN_PAINT_WIDTH + SLIDER_L_PAD;
    float f2 = getWidth() - SLIDER_R_PAD - MAX_PAINT_WIDTH - SLIDER_R_PAD - f1;
    float f3 = MIN_PAINT_WIDTH + (paramFloat - f1) / f2 * (MAX_PAINT_WIDTH - MIN_PAINT_WIDTH);
    if ((f3 >= MIN_PAINT_WIDTH) && (f3 <= MAX_PAINT_WIDTH))
      this.skitchCanvas.setPaintWidth(f3);
    invalidate();
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    Paint localPaint = new Paint();
    float f1 = getHeight() - SLIDER_BG_H;
    float f2 = getSliderValue();
    float f3 = SLIDER_L_PAD + MIN_PAINT_WIDTH + SLIDER_L_PAD;
    float f4 = f3 + f2 + this.sliderBlock.getWidth() / 2;
    float f5 = getWidth() - SLIDER_R_PAD - MAX_PAINT_WIDTH - SLIDER_R_PAD;
    float f6 = f1 + (getHeight() - f1) / 2.0F - SLIDER_H / 2.0F;
    float f7 = f6 + SLIDER_H;
    localPaint.setAntiAlias(true);
    localPaint.setStrokeWidth(3.0F);
    localPaint.setColor(-16777216);
    paramCanvas.drawBitmap(this.sliderBg, new Rect(0, 0, this.sliderBg.getWidth(), this.sliderBg.getHeight()), new RectF(0.0F, f1, getWidth(), getHeight()), null);
    paramCanvas.drawCircle(f3 / 2.0F, f1 + (getHeight() - f1) / 2.0F, MIN_PAINT_WIDTH / 2.0F, localPaint);
    paramCanvas.drawCircle(f5 + SLIDER_R_PAD + MAX_PAINT_WIDTH / 3.0F, f1 + (getHeight() - f1) / 2.0F, MAX_PAINT_WIDTH / 3.0F, localPaint);
    paramCanvas.drawCircle(f2 + (f3 + SLIDER_L_PAD) + this.sliderBlock.getWidth() / 2, f1 / 2.0F, currentWidth() / 2.0F, localPaint);
    if (f4 - f3 <= this.sliderBlue.getWidth())
    {
      paramCanvas.drawBitmap(this.sliderBlue, new Rect(0, 0, this.sliderBlue.getWidth(), this.sliderBlue.getHeight()), new RectF(f3, f6, f4, f7), null);
      label332: if (f5 - f4 > this.sliderBrown.getWidth())
        break label636;
      paramCanvas.drawBitmap(this.sliderBrown, new Rect((int)(f4 + (this.sliderBrown.getWidth() - f5)), 0, this.sliderBrown.getWidth(), this.sliderBrown.getHeight()), new RectF(f4, f6, f5, f7), null);
    }
    while (true)
    {
      paramCanvas.drawBitmap(this.sliderBlock, new Rect(0, 0, this.sliderBlock.getWidth(), this.sliderBlock.getHeight()), new RectF(f3 + f2, f1 + (getHeight() - f1) / 2.0F - this.sliderBlock.getHeight() / 2, f3 + f2 + this.sliderBlock.getWidth(), f1 + (getHeight() - f1) / 2.0F + this.sliderBlock.getHeight() / 2), null);
      return;
      paramCanvas.drawBitmap(this.sliderBlue, new Rect(0, 0, this.sliderBlue.getWidth(), this.sliderBlue.getHeight()), new RectF(f3, f6, f3 + this.sliderBlue.getWidth(), f7), null);
      paramCanvas.drawBitmap(this.sliderBlue, new Rect(-1 + this.sliderBlue.getWidth(), 0, this.sliderBlue.getWidth(), this.sliderBlue.getHeight()), new RectF(f3 + this.sliderBlue.getWidth(), f6, f4, f7), null);
      break label332:
      label636: paramCanvas.drawBitmap(this.sliderBrown, new Rect(0, 0, this.sliderBrown.getWidth(), this.sliderBrown.getHeight()), new RectF(f5 - this.sliderBrown.getWidth(), f6, f5, f7), null);
      paramCanvas.drawBitmap(this.sliderBrown, new Rect(0, 0, 1, this.sliderBrown.getHeight()), new RectF(f4, f6, f5 - this.sliderBrown.getWidth(), f7), null);
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = 1;
    switch (paramMotionEvent.getAction())
    {
    default:
      i = 0;
    case 1:
    case 0:
    case 2:
    }
    float f1;
    float f3;
    float f4;
    float f5;
    do
    {
      return i;
      this.dismissTimer.setLastTouchTime(Long.valueOf(System.currentTimeMillis()));
      f1 = getHeight() - SLIDER_BG_H;
      float f2 = SLIDER_L_PAD + MIN_PAINT_WIDTH + SLIDER_L_PAD;
      f3 = getWidth() - SLIDER_R_PAD - MAX_PAINT_WIDTH - SLIDER_R_PAD;
      f4 = paramMotionEvent.getX();
      f5 = paramMotionEvent.getY();
      if ((f4 >= f2) && (f4 <= f3 - this.sliderBlock.getWidth()) && (f5 >= f1) && (f5 <= getHeight()))
      {
        updateCurrentWidth(f4);
        return i;
      }
      if ((f4 >= f2) || (f5 < f1) || (f5 > getHeight()))
        continue;
      updateCurrentWidth(f2);
      return i;
    }
    while ((f4 <= f3) || (f5 < f1) || (f5 > getHeight()));
    updateCurrentWidth(f3 - this.sliderBlock.getWidth());
    return i;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.tool.PaintSliderView
 * JD-Core Version:    0.5.4
 */