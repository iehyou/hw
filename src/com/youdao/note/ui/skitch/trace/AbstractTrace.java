package com.youdao.note.ui.skitch.trace;

public abstract class AbstractTrace
  implements ITrace
{
  private float mCurrentX = 0.0F;
  private float mCurrentY = 0.0F;
  private float mLastX = 0.0F;
  private float mLastY = 0.0F;

  private void innerTrace(float paramFloat1, float paramFloat2)
  {
    this.mLastX = this.mCurrentX;
    this.mLastY = this.mCurrentY;
    this.mCurrentX = paramFloat1;
    this.mCurrentY = paramFloat2;
  }

  public void continueTrace(float paramFloat1, float paramFloat2)
  {
    innerTrace(paramFloat1, paramFloat2);
  }

  protected float getLastX()
  {
    return this.mLastX;
  }

  protected float getLastY()
  {
    return this.mLastY;
  }

  public void startTrace(float paramFloat1, float paramFloat2)
  {
    innerTrace(paramFloat1, paramFloat2);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.trace.AbstractTrace
 * JD-Core Version:    0.5.4
 */