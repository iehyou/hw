package com.youdao.note.ui.skitch.trace;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import com.youdao.note.utils.L;

public class PenTrace extends AbstractTrace
{
  private static final float DEFAULT_PEN_WIDTH = 20.0F;
  Paint mPaint = new Paint();
  Path mPath = new Path();

  public PenTrace()
  {
    this(20.0F);
  }

  public PenTrace(float paramFloat)
  {
    this.mPaint.setAntiAlias(true);
    this.mPaint.setColor(-16777216);
    this.mPaint.setStrokeWidth(paramFloat);
    this.mPaint.setDither(true);
    this.mPaint.setStyle(Paint.Style.STROKE);
    this.mPaint.setStrokeJoin(Paint.Join.ROUND);
    this.mPaint.setStrokeCap(Paint.Cap.ROUND);
    this.mPaint.setFilterBitmap(true);
  }

  public void continueTrace(float paramFloat1, float paramFloat2)
  {
    L.d(this, "pen tool continue trace called.");
    super.continueTrace(paramFloat1, paramFloat2);
    this.mPath.quadTo(getLastX(), getLastY(), (paramFloat1 + getLastX()) / 2.0F, (paramFloat2 + getLastY()) / 2.0F);
  }

  public PenTrace copy()
  {
    PenTrace localPenTrace = new PenTrace();
    localPenTrace.mPaint = new Paint(this.mPaint);
    localPenTrace.mPath = new Path(this.mPath);
    return localPenTrace;
  }

  public void drawTrace(Canvas paramCanvas, float paramFloat)
  {
    L.d(this, "pen on draw called.");
    paramCanvas.setDrawFilter(new PaintFlagsDrawFilter(1, 1));
    Matrix localMatrix = new Matrix();
    localMatrix.setScale(paramFloat, paramFloat);
    this.mPath.transform(localMatrix);
    float f = this.mPaint.getStrokeWidth();
    this.mPaint.setStrokeWidth(f * paramFloat);
    paramCanvas.drawPath(this.mPath, this.mPaint);
    this.mPaint.setStrokeWidth(f);
  }

  public void setColor(int paramInt)
  {
    this.mPaint.setColor(paramInt);
  }

  public void setWidth(float paramFloat)
  {
    this.mPaint.setStrokeWidth(paramFloat);
  }

  public void startTrace(float paramFloat1, float paramFloat2)
  {
    L.d(this, "pen tool start trace called.");
    super.startTrace(paramFloat1, paramFloat2);
    this.mPath.reset();
    this.mPath.moveTo(paramFloat1, paramFloat2);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.trace.PenTrace
 * JD-Core Version:    0.5.4
 */