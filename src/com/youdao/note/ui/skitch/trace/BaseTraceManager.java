package com.youdao.note.ui.skitch.trace;

import android.graphics.Canvas;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BaseTraceManager
  implements ITraceManager
{
  private static final float EMPTY_X = -1.0F;
  private float leftMost = -1.0F;
  private List<ITrace> mTraces = null;
  private float rightMost = -1.0F;

  public void addTrace(ITrace paramITrace)
  {
    if (this.mTraces == null)
      this.mTraces = new LinkedList();
    this.mTraces.add(paramITrace);
  }

  public void clear()
  {
    this.mTraces = null;
    this.rightMost = -1.0F;
    this.leftMost = -1.0F;
  }

  public BaseTraceManager copy()
  {
    BaseTraceManager localBaseTraceManager = new BaseTraceManager();
    if (this.mTraces != null)
    {
      localBaseTraceManager.mTraces = new LinkedList();
      Iterator localIterator = this.mTraces.iterator();
      while (localIterator.hasNext())
      {
        ITrace localITrace = (ITrace)localIterator.next();
        localBaseTraceManager.mTraces.add(localITrace.copy());
      }
    }
    localBaseTraceManager.leftMost = this.leftMost;
    localBaseTraceManager.rightMost = this.rightMost;
    return localBaseTraceManager;
  }

  public void drawTraces(Canvas paramCanvas, float paramFloat)
  {
    if (this.mTraces == null)
      return;
    Iterator localIterator = this.mTraces.iterator();
    while (true)
    {
      if (localIterator.hasNext());
      ((ITrace)localIterator.next()).drawTrace(paramCanvas, paramFloat);
    }
  }

  public float getLeftMost()
  {
    return this.leftMost;
  }

  public float getRightMost()
  {
    return this.rightMost;
  }

  public boolean isEmpty()
  {
    return (this.mTraces == null) || (this.mTraces.size() == 0) || (this.leftMost == this.rightMost);
  }

  public void onTouch(float paramFloat)
  {
    if ((this.leftMost == -1.0F) || (paramFloat < this.leftMost))
      this.leftMost = paramFloat;
    if ((this.rightMost != -1.0F) && (paramFloat <= this.rightMost))
      return;
    this.rightMost = paramFloat;
  }

  public void removeLastTrace()
  {
    if ((this.mTraces == null) || (this.mTraces.size() <= 0))
      return;
    this.mTraces.remove(-1 + this.mTraces.size());
  }

  public void removeTrace(ITrace paramITrace)
  {
    if ((this.mTraces == null) || (this.mTraces.size() <= 0))
      return;
    this.mTraces.remove(paramITrace);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.trace.BaseTraceManager
 * JD-Core Version:    0.5.4
 */