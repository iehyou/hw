package com.youdao.note.ui.skitch.trace;

import android.graphics.Canvas;

public abstract interface ITrace
{
  public abstract void continueTrace(float paramFloat1, float paramFloat2);

  public abstract ITrace copy();

  public abstract void drawTrace(Canvas paramCanvas, float paramFloat);

  public abstract void startTrace(float paramFloat1, float paramFloat2);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.trace.ITrace
 * JD-Core Version:    0.5.4
 */