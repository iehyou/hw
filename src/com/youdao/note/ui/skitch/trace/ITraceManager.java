package com.youdao.note.ui.skitch.trace;

import android.graphics.Canvas;

public abstract interface ITraceManager
{
  public abstract void addTrace(ITrace paramITrace);

  public abstract void clear();

  public abstract ITraceManager copy();

  public abstract void drawTraces(Canvas paramCanvas, float paramFloat);

  public abstract float getLeftMost();

  public abstract float getRightMost();

  public abstract boolean isEmpty();

  public abstract void onTouch(float paramFloat);

  public abstract void removeLastTrace();

  public abstract void removeTrace(ITrace paramITrace);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.trace.ITraceManager
 * JD-Core Version:    0.5.4
 */