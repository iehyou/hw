package com.youdao.note.ui.skitch;

public abstract interface ISkitchMeta
{
  public abstract ISkitchMeta copy();

  public abstract boolean isEmpty();

  public abstract boolean isNew();

  public abstract void onDestroy();

  public abstract void setIsNew(boolean paramBoolean);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.ISkitchMeta
 * JD-Core Version:    0.5.4
 */