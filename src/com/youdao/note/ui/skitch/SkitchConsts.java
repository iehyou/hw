package com.youdao.note.ui.skitch;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import com.youdao.note.YNoteApplication;
import com.youdao.note.ui.skitch.tool.ColorBoxView;

public abstract interface SkitchConsts
{
  public static final float DEFAULT_PAINT_WIDTH = YNoteApplication.getInstance().getResources().getDimension(2131296261);
  public static final float MAX_PAINT_WIDTH;
  public static final float MIN_PAINT_WIDTH = YNoteApplication.getInstance().getResources().getDimension(2131296262);
  public static final float SLIDER_BG_H;
  public static final float SLIDER_H;
  public static final float SLIDER_L_PAD;
  public static final float SLIDER_R_PAD;
  public static final float SLIDER_TOP_PAD;

  static
  {
    MAX_PAINT_WIDTH = YNoteApplication.getInstance().getResources().getDimension(2131296263);
    SLIDER_H = YNoteApplication.getInstance().getResources().getDimension(2131296264);
    SLIDER_L_PAD = YNoteApplication.getInstance().getResources().getDimension(2131296265);
    SLIDER_R_PAD = YNoteApplication.getInstance().getResources().getDimension(2131296266);
    SLIDER_TOP_PAD = YNoteApplication.getInstance().getResources().getDimension(2131296267);
    SLIDER_BG_H = YNoteApplication.getInstance().getResources().getDimension(2131296268);
  }

  public static abstract interface Doodle extends SkitchConsts
  {
    public static final int COLOR_COUNT = 6;
    public static final int DEFAULT_PAINT_COLOR;
    public static final float MARGIN_VERTICAL;
    public static final float PAD = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296277);

    static
    {
      MARGIN_VERTICAL = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296278);
      DEFAULT_PAINT_COLOR = ColorBoxView.rgb1;
    }
  }

  public static abstract interface HandWrite extends SkitchConsts
  {
    public static final Bitmap BLANK_CHARACTER;
    public static final int BLANK_WIDTH = 0;
    public static final float HEADER_W = 0.0F;
    public static final int LINE_COLOR = 0;
    public static final int LINE_HEIGHT = 0;
    public static final int LINE_STROKE = 0;
    public static final int MODE_GL = 1;
    public static final int MODE_NORMAL = 0;
    public static final long MODE_TIME_THRESHOLD = 60L;
    public static final int PADDING_BOTTOM;
    public static final int PADDING_TOP;
    public static final Bitmap RETURN_CHARACTER;
    public static final int SPACING;
    public static final int WORD_HEIGHT = YNoteApplication.getInstance().getHandwriteWordHeight();

    static
    {
      BLANK_WIDTH = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296271);
      PADDING_TOP = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296272);
      PADDING_BOTTOM = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296273);
      SPACING = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296274);
      LINE_HEIGHT = PADDING_TOP + WORD_HEIGHT + PADDING_BOTTOM;
      HEADER_W = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296275);
      LINE_COLOR = Color.parseColor("#CCCBCC");
      LINE_STROKE = YNoteApplication.getInstance().getResources().getDimensionPixelSize(2131296276);
      BLANK_CHARACTER = Bitmap.createBitmap(BLANK_WIDTH, WORD_HEIGHT, Bitmap.Config.ARGB_8888);
      RETURN_CHARACTER = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.SkitchConsts
 * JD-Core Version:    0.5.4
 */