package com.youdao.note.ui.skitch;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.youdao.note.ui.skitch.trace.ITrace;
import com.youdao.note.ui.skitch.trace.ITraceManager;

public abstract class AbstractCanvasView extends ImageView
{
  private ITrace curTrace;

  public AbstractCanvasView(Context paramContext)
  {
    super(paramContext);
  }

  public AbstractCanvasView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public AbstractCanvasView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void clearScreen()
  {
    this.curTrace = null;
    getTraceManager().clear();
    invalidate();
  }

  protected abstract ITrace createTraceTool();

  protected abstract ITraceManager getTraceManager();

  public void onDraw(Canvas paramCanvas)
  {
    getTraceManager().drawTraces(paramCanvas, 1.0F);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    getTraceManager().onTouch(paramMotionEvent.getX());
    switch (paramMotionEvent.getAction())
    {
    default:
    case 0:
    case 2:
    case 1:
    }
    while (true)
    {
      return true;
      this.curTrace = createTraceTool();
      getTraceManager().addTrace(this.curTrace);
      this.curTrace.startTrace(paramMotionEvent.getX(), paramMotionEvent.getY());
      invalidate();
      continue;
      if (this.curTrace == null)
      {
        this.curTrace = createTraceTool();
        getTraceManager().addTrace(this.curTrace);
        this.curTrace.startTrace(paramMotionEvent.getX(), paramMotionEvent.getY());
      }
      this.curTrace.continueTrace(paramMotionEvent.getX(), paramMotionEvent.getY());
      invalidate();
      continue;
      if (this.curTrace == null)
        continue;
      this.curTrace.continueTrace(paramMotionEvent.getX(), paramMotionEvent.getY());
      invalidate();
      this.curTrace = null;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.AbstractCanvasView
 * JD-Core Version:    0.5.4
 */