package com.youdao.note.ui.skitch;

import android.graphics.Bitmap;

public abstract interface ISkitch
{
  public abstract Bitmap getBitmap();

  public abstract float getPaintWidth();

  public abstract float getPaintWidthRatio();

  public abstract void initSkitchMeta(ISkitchMeta paramISkitchMeta);

  public abstract void setPaintWidth(float paramFloat);

  public abstract void trash();

  public abstract void undo();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.ISkitch
 * JD-Core Version:    0.5.4
 */