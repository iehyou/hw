package com.youdao.note.ui.skitch;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SkitchMetaManager
{
  private Map<String, ISkitchMeta> map = null;

  public void addOrUpdateSkitchMeta(String paramString, ISkitchMeta paramISkitchMeta)
  {
    if (this.map == null)
      this.map = new HashMap();
    this.map.put(paramString, paramISkitchMeta);
  }

  public boolean containsSkitch(String paramString)
  {
    if (this.map == null)
      return false;
    return this.map.containsKey(paramString);
  }

  public void destroy()
  {
    if (this.map == null)
      return;
    Iterator localIterator = this.map.values().iterator();
    while (localIterator.hasNext())
      ((ISkitchMeta)localIterator.next()).onDestroy();
    this.map.clear();
  }

  public ISkitchMeta getSkitchMeta(String paramString)
  {
    if (this.map == null)
      return null;
    return (ISkitchMeta)this.map.get(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.SkitchMetaManager
 * JD-Core Version:    0.5.4
 */