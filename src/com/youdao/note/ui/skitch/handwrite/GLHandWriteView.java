package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;
import com.youdao.note.utils.CompatUtils;
import com.youdao.note.utils.L;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11ExtensionPack;

public class GLHandWriteView extends GLSurfaceView
  implements IWrite, SkitchConsts.HandWrite
{
  private static final int BEGIN_COUNT = 3;
  private static final float BG_ALPHA = 0.6F;
  private static final float FG_ALPHA = 0.8F;
  private static final int STAND_WIDTH = 20;
  private int charCount = 0;
  private Bitmap character;
  private float mDensity;
  private IHandWrite mHandWriteCanvas = null;
  private HandWriteMonitor mMonitor;
  private HandWriteRender mRenderer = null;
  private float mStandWidth;
  private PerformanceMonitor monitor = new PerformanceMonitor(null);

  public GLHandWriteView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setRenderer(this.mRenderer);
    setRenderMode(0);
  }

  public void finishWrite()
  {
    this.mRenderer.erase();
    requestRender();
  }

  // ERROR //
  public void getCharacter(IWrite.IGetBitmap paramIGetBitmap)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:mRenderer	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;
    //   4: invokevirtual 96	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:finishWrite	()V
    //   7: aload_0
    //   8: iconst_1
    //   9: aload_0
    //   10: getfield 41	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:charCount	I
    //   13: iadd
    //   14: putfield 41	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:charCount	I
    //   17: aload_0
    //   18: invokevirtual 90	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:requestRender	()V
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: ldc2_w 97
    //   27: invokevirtual 104	java/lang/Object:wait	(J)V
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_0
    //   33: getfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   36: ifnull +133 -> 169
    //   39: aload_0
    //   40: new 106	java/lang/StringBuilder
    //   43: dup
    //   44: invokespecial 108	java/lang/StringBuilder:<init>	()V
    //   47: aload_0
    //   48: getfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   51: invokevirtual 114	android/graphics/Bitmap:getHeight	()I
    //   54: invokevirtual 118	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   57: ldc 120
    //   59: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: aload_0
    //   63: getfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   66: invokevirtual 126	android/graphics/Bitmap:getWidth	()I
    //   69: invokevirtual 118	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   72: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   75: invokestatic 136	com/youdao/note/utils/L:d	(Ljava/lang/Object;Ljava/lang/String;)V
    //   78: aload_1
    //   79: aload_0
    //   80: getfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   83: invokeinterface 142 2 0
    //   88: aload_0
    //   89: getfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   92: invokevirtual 145	android/graphics/Bitmap:recycle	()V
    //   95: aload_0
    //   96: aconst_null
    //   97: putfield 69	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:character	Landroid/graphics/Bitmap;
    //   100: invokestatic 151	com/youdao/note/YNoteApplication:getInstance	()Lcom/youdao/note/YNoteApplication;
    //   103: invokevirtual 155	com/youdao/note/YNoteApplication:isShowHandwriteModeTips	()Z
    //   106: ifeq +32 -> 138
    //   109: invokestatic 151	com/youdao/note/YNoteApplication:getInstance	()Lcom/youdao/note/YNoteApplication;
    //   112: invokevirtual 159	com/youdao/note/YNoteApplication:getLogRecorder	()Lcom/youdao/note/LogRecorder;
    //   115: astore 4
    //   117: aload_0
    //   118: getfield 41	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:charCount	I
    //   121: iconst_4
    //   122: if_icmple +56 -> 178
    //   125: invokestatic 151	com/youdao/note/YNoteApplication:getInstance	()Lcom/youdao/note/YNoteApplication;
    //   128: iconst_0
    //   129: invokevirtual 163	com/youdao/note/YNoteApplication:setShowHandwriteModeTips	(Z)V
    //   132: aload 4
    //   134: iconst_1
    //   135: invokevirtual 168	com/youdao/note/LogRecorder:setHandWriteMode	(I)V
    //   138: aload_0
    //   139: getfield 48	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:monitor	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    //   142: invokevirtual 171	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor:reset	()V
    //   145: aload_0
    //   146: getfield 37	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:mRenderer	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;
    //   149: invokevirtual 174	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:resetState	()V
    //   152: aload_0
    //   153: getfield 37	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:mRenderer	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;
    //   156: invokevirtual 87	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:erase	()V
    //   159: aload_0
    //   160: invokevirtual 90	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:requestRender	()V
    //   163: return
    //   164: astore_3
    //   165: aload_0
    //   166: monitorexit
    //   167: aload_3
    //   168: athrow
    //   169: aload_0
    //   170: ldc 176
    //   172: invokestatic 136	com/youdao/note/utils/L:d	(Ljava/lang/Object;Ljava/lang/String;)V
    //   175: goto -75 -> 100
    //   178: aload_0
    //   179: invokevirtual 180	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getCostTime	()J
    //   182: ldc2_w 181
    //   185: lcmp
    //   186: ifle +23 -> 209
    //   189: aload_0
    //   190: invokevirtual 186	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getContext	()Landroid/content/Context;
    //   193: checkcast 188	com/youdao/note/activity/resource/HandwritingActivity
    //   196: iconst_3
    //   197: invokevirtual 191	com/youdao/note/activity/resource/HandwritingActivity:showDialog	(I)V
    //   200: aload 4
    //   202: iconst_0
    //   203: invokevirtual 168	com/youdao/note/LogRecorder:setHandWriteMode	(I)V
    //   206: goto -68 -> 138
    //   209: aload 4
    //   211: iconst_1
    //   212: invokevirtual 168	com/youdao/note/LogRecorder:setHandWriteMode	(I)V
    //   215: goto -77 -> 138
    //   218: astore_2
    //   219: goto -189 -> 30
    //
    // Exception table:
    //   from	to	target	type
    //   23	30	164	finally
    //   30	32	164	finally
    //   165	167	164	finally
    //   23	30	218	java/lang/InterruptedException
  }

  public long getCostTime()
  {
    if (this.monitor == null)
      this.monitor = new PerformanceMonitor(null);
    return this.monitor.getAvgCost();
  }

  public void init(float paramFloat, int paramInt1, int paramInt2)
  {
    this.mDensity = paramFloat;
    this.mStandWidth = (20.0F * this.mDensity);
    this.mRenderer.resetState();
    requestRender();
  }

  public void onFinishWrite()
  {
  }

  public void onStartWrite()
  {
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    this.mMonitor.touch();
    switch (CompatUtils.getActionMasked(paramMotionEvent))
    {
    default:
    case 0:
    case 2:
    case 1:
    }
    while (true)
    {
      requestRender();
      return true;
      this.monitor.reset();
      this.mRenderer.add(paramMotionEvent);
      continue;
      this.mRenderer.add(paramMotionEvent);
      continue;
      this.mRenderer.add(paramMotionEvent);
    }
  }

  public void setHandWriteCanvas(IHandWrite paramIHandWrite)
  {
    this.mHandWriteCanvas = paramIHandWrite;
  }

  public void setTouchMonotor(HandWriteMonitor paramHandWriteMonitor)
  {
    this.mMonitor = paramHandWriteMonitor;
  }

  private class HandWriteRender
    implements GLSurfaceView.Renderer
  {
    private static final int EMPTY_X = -1;
    private static final int INTERVAL = 3;
    int[] b = null;
    int[] bt = null;
    private List<MotionEvent> eventList = new ArrayList(100);
    private float leftMost = -1.0F;
    private QuadraticBezierSpline2 mBezier = new QuadraticBezierSpline2();
    private NBrush mBrush = null;
    private PointTracker mDrawPoint = null;
    private boolean mFinshWrite = false;
    private int mFramebuffer;
    private int mFramebufferHeight;
    private int mFramebufferWidth;
    private GLHandWriteView mGLHandWriteView = null;
    private MotionEvent mLastEvent = null;
    private double mLastSpeed = 0.0D;
    private double mLastWidth;
    private boolean mNeedErase = false;
    private int mTargetTexture;
    private int mTouchCount;
    private float rightMost = -1.0F;

    public HandWriteRender(GLHandWriteView arg2)
    {
      Object localObject;
      this.mGLHandWriteView = localObject;
      erase();
    }

    private float beginWidth(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      return calcNewWidth(paramFloat1, paramFloat4, paramFloat2, 0.01F, 0.7F, 0.05F, 1.3F, 2.5F, 0.5F, 2.0F * (GLHandWriteView.this.mStandWidth * GLHandWriteView.this.mHandWriteCanvas.getPaintWidthRatio()), paramFloat3);
    }

    private float calcNewWidth(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9, float paramFloat10, float paramFloat11)
    {
      float f1 = paramFloat1 * 0.3F + paramFloat2 * 0.7F;
      float f2 = paramFloat4 * paramFloat3;
      if (paramFloat5 < f2)
        f2 = paramFloat5;
      float f3 = (float)Math.log(paramFloat7 / paramFloat9) / (paramFloat8 - paramFloat6);
      float f4 = paramFloat10 * (float)Math.exp((float)Math.log(paramFloat7) + f3 * paramFloat6 + f1 * -f3);
      float f6;
      if (Math.abs(f4 - paramFloat10) / paramFloat10 > f2)
      {
        if (f4 - paramFloat10 <= 0.0F)
          break label164;
        f6 = 1.0F;
        label107: f4 = paramFloat10 * (1.0F + f6 * f2);
      }
      float f5;
      if (Math.abs(f4 - paramFloat11) / paramFloat11 > f2)
      {
        if (f4 - paramFloat11 <= 0.0F)
          break label171;
        f5 = 1.0F;
      }
      while (true)
      {
        f4 = paramFloat11 * (1.0F + f5 * f2);
        return f4;
        label164: f6 = -1.0F;
        break label107:
        label171: f5 = -1.0F;
      }
    }

    private int createFrameBuffer(GL10 paramGL10, int paramInt1, int paramInt2, int paramInt3)
    {
      L.d(this, "width is " + paramInt1 + " height is " + paramInt2);
      GL11ExtensionPack localGL11ExtensionPack = (GL11ExtensionPack)paramGL10;
      int[] arrayOfInt1 = new int[1];
      localGL11ExtensionPack.glGenFramebuffersOES(1, arrayOfInt1, 0);
      int i = arrayOfInt1[0];
      localGL11ExtensionPack.glBindFramebufferOES(36160, i);
      int[] arrayOfInt2 = new int[1];
      localGL11ExtensionPack.glGenRenderbuffersOES(1, arrayOfInt2, 0);
      int j = arrayOfInt2[0];
      localGL11ExtensionPack.glBindRenderbufferOES(36161, j);
      localGL11ExtensionPack.glRenderbufferStorageOES(36161, 33189, paramInt1, paramInt2);
      localGL11ExtensionPack.glFramebufferRenderbufferOES(36160, 36096, 36161, j);
      localGL11ExtensionPack.glFramebufferTexture2DOES(36160, 36064, 3553, paramInt3, 0);
      int k = localGL11ExtensionPack.glCheckFramebufferStatusOES(36160);
      if (k != 36053)
        throw new RuntimeException("Framebuffer is not complete: " + Integer.toHexString(k));
      localGL11ExtensionPack.glBindFramebufferOES(36160, 0);
      return i;
    }

    private int createTargetTexture(GL10 paramGL10, int paramInt1, int paramInt2)
    {
      L.d(this, "in crete texture width " + paramInt1 + " height " + paramInt2);
      int[] arrayOfInt = new int[1];
      paramGL10.glGenTextures(1, arrayOfInt, 0);
      int i = arrayOfInt[0];
      paramGL10.glBindTexture(3553, i);
      paramGL10.glTexImage2D(3553, 0, 6408, paramInt1, paramInt2, 0, 6408, 5121, null);
      paramGL10.glTexParameterf(3553, 10241, 9729.0F);
      paramGL10.glTexParameterf(3553, 10240, 9729.0F);
      paramGL10.glTexParameterx(3553, 10242, 33071);
      paramGL10.glTexParameterx(3553, 10243, 33071);
      return i;
    }

    private void drawLine(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
    {
      int i = 1 + (int)(Math.hypot(paramFloat4 - paramFloat1, paramFloat5 - paramFloat2) * GLHandWriteView.this.mDensity / 2.0D);
      float f1 = (paramFloat4 - paramFloat1) / i;
      float f2 = (paramFloat5 - paramFloat2) / i;
      float f3 = (paramFloat6 - paramFloat3) / i;
      float f4 = paramFloat1;
      float f5 = paramFloat2;
      float f6 = paramFloat3;
      for (int j = 0; j < i; ++j)
      {
        this.mBrush.draw(paramGL10, f4, f5, f6, paramFloat7, paramFloat8);
        f4 += f1;
        f5 += f2;
        f6 += f3;
      }
    }

    private void drawPoint(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
    {
      paramGL10.glMatrixMode(5888);
      paramGL10.glLoadIdentity();
      this.mBrush.draw(paramGL10, paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5);
    }

    private void drawPointsOnOffscreen(GL10 paramGL10)
    {
      Iterator localIterator = this.eventList.iterator();
      if (localIterator.hasNext())
      {
        label10: MotionEvent localMotionEvent = (MotionEvent)localIterator.next();
        label109: float f1;
        float f2;
        if ((this.mDrawPoint == null) || (CompatUtils.getActionMasked(localMotionEvent) == 0))
        {
          this.mLastWidth = (0.8D * (2.0F * (GLHandWriteView.this.mStandWidth * GLHandWriteView.this.mHandWriteCanvas.getPaintWidthRatio())));
          this.mDrawPoint = new PointTracker();
          this.mDrawPoint.set(localMotionEvent.getX(), localMotionEvent.getY(), this.mLastWidth);
          this.mTouchCount = 0;
          if (CompatUtils.getActionMasked(localMotionEvent) == 1)
          {
            f1 = this.mDrawPoint.x;
            f2 = this.mDrawPoint.y;
            if (this.mTouchCount >= 2)
              break label424;
            drawPoint(paramGL10, f1, f2, endWidth(), 1.0F, 1.0F);
          }
        }
        while (true)
        {
          this.mDrawPoint = null;
          this.mLastSpeed = 0.0D;
          this.mLastEvent = localMotionEvent;
          this.mTouchCount = (1 + this.mTouchCount);
          break label10:
          float f7 = this.mDrawPoint.x;
          float f8 = this.mDrawPoint.y;
          float f9 = (localMotionEvent.getX() - f7) / GLHandWriteView.this.mDensity;
          float f10 = (localMotionEvent.getY() - f8) / GLHandWriteView.this.mDensity;
          float f11 = (float)Math.hypot(f9, f10);
          int j = 1 + (int)f11 / 3;
          double d1 = f11 / (float)(localMotionEvent.getEventTime() - this.mLastEvent.getEventTime());
          double d2;
          if (this.mTouchCount < 3)
          {
            d2 = beginWidth((float)d1, f11, (float)this.mLastWidth, (float)this.mLastSpeed);
            label308: if (this.mTouchCount >= 2)
              break label403;
            this.mLastSpeed = d1;
            this.mBezier.Init(f7, f8, this.mLastWidth, localMotionEvent.getX(), localMotionEvent.getY(), d2);
          }
          while (true)
          {
            this.mLastSpeed = ((d1 + this.mLastSpeed) / 2.0D);
            this.mLastWidth = d2;
            drawToPoint(paramGL10, j);
            break label109:
            d2 = movingWidth((float)d1, f11, (float)this.mLastWidth, (float)this.mLastSpeed);
            break label308:
            label403: this.mBezier.AddNode(localMotionEvent.getX(), localMotionEvent.getY(), (float)d2);
          }
          label424: float f3 = localMotionEvent.getX();
          float f4 = localMotionEvent.getY();
          float f5 = (f3 - f1) / GLHandWriteView.this.mDensity;
          float f6 = (f4 - f2) / GLHandWriteView.this.mDensity;
          int i = 1 + (int)((float)Math.hypot(f5, f6) * GLHandWriteView.this.mDensity) / 3;
          this.mBezier.AddNode(f3, f4, 0.0F);
          drawToPoint(paramGL10, i);
          this.mBezier.End();
          drawToPoint(paramGL10, i);
        }
      }
      resetState();
      L.d(this, "Draw offscreen ended");
    }

    private void drawToPoint(GL10 paramGL10, int paramInt)
    {
      this.mBezier.getPoint(this.mDrawPoint, 0.0D);
      float f1 = this.mDrawPoint.x;
      float f2 = this.mDrawPoint.y;
      float f3 = (float)this.mDrawPoint.width;
      double d1 = 1.0D / paramInt;
      double d2 = d1;
      while (d2 < 1.0D)
      {
        this.mBezier.getPoint(this.mDrawPoint, d2);
        drawLine(paramGL10, f1, f2, f3, this.mDrawPoint.x, this.mDrawPoint.y, (float)this.mDrawPoint.width, 0.6F, 0.8F);
        f1 = this.mDrawPoint.x;
        f2 = this.mDrawPoint.y;
        f3 = (float)this.mDrawPoint.width;
        d2 += d1;
      }
      this.mBezier.getPoint(this.mDrawPoint, 1.0D);
      drawLine(paramGL10, f1, f2, f3, this.mDrawPoint.x, this.mDrawPoint.y, (float)this.mDrawPoint.width, 0.6F, 0.8F);
    }

    private float endWidth()
    {
      return 1.2F * (2.0F * (GLHandWriteView.this.mStandWidth * GLHandWriteView.this.mHandWriteCanvas.getPaintWidthRatio()));
    }

    private int getColor(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
    {
      int i = 0;
      int j = 0;
      int k = 0;
      int l = 0;
      int i1 = 0;
      for (int i2 = paramInt3 - 1; i2 <= paramInt3 + 1; ++i2)
      {
        if ((i2 < 0) || (i2 >= paramInt2))
          continue;
        for (int i7 = paramInt4 - 1; i7 <= paramInt4 + 1; ++i7)
        {
          if ((i7 < 0) || (i7 >= paramInt1))
            continue;
          int i8 = this.b[(i7 + i2 * paramInt1)];
          i += (i8 & 0xFF);
          j += (0xFF & i8 >> 8);
          k += (0xFF & i8 >> 16);
          l += (0xFF & i8 >> 24);
          ++i1;
        }
      }
      int i3 = i / i1;
      int i4 = j / i1;
      int i5 = l / i1;
      int i6 = k / i1;
      return i3 | (i5 << 24 | i6 << 16 | i4 << 8);
    }

    private float movingWidth(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      return calcNewWidth(paramFloat1, paramFloat4, paramFloat2, 0.01F, 0.7F, 0.05F, 1.5F, 2.5F, 0.5F, 2.0F * (GLHandWriteView.this.mStandWidth * GLHandWriteView.this.mHandWriteCanvas.getPaintWidthRatio()), paramFloat3);
    }

    private int resize(int paramInt)
    {
      for (int i = 1; 1 << i < paramInt; ++i);
      return 1 << i;
    }

    public void add(MotionEvent paramMotionEvent)
    {
      monitorenter;
      try
      {
        this.eventList.add(MotionEvent.obtain(paramMotionEvent));
        float f = paramMotionEvent.getX();
        if ((this.leftMost == -1.0F) || (f < this.leftMost))
          this.leftMost = f;
        if ((this.rightMost == -1.0F) || (f > this.rightMost))
          this.rightMost = f;
        return;
      }
      finally
      {
        monitorexit;
      }
    }

    public void erase()
    {
      monitorenter;
      try
      {
        this.mNeedErase = true;
        monitorexit;
        return;
      }
      finally
      {
        localObject = finally;
        monitorexit;
        throw localObject;
      }
    }

    public void finishWrite()
    {
      monitorenter;
      try
      {
        this.mFinshWrite = true;
        monitorexit;
        return;
      }
      finally
      {
        localObject = finally;
        monitorexit;
        throw localObject;
      }
    }

    // ERROR //
    public void onDrawFrame(GL10 paramGL10)
    {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   6: invokestatic 376	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:access$100	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
      //   9: ifnonnull +23 -> 32
      //   12: aload_0
      //   13: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   16: new 378	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor
      //   19: dup
      //   20: aload_0
      //   21: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   24: aconst_null
      //   25: invokespecial 381	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor:<init>	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;)V
      //   28: invokestatic 385	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:access$102	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
      //   31: pop
      //   32: aload_0
      //   33: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   36: invokestatic 376	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:access$100	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
      //   39: invokestatic 390	java/lang/System:currentTimeMillis	()J
      //   42: invokevirtual 394	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor:recordTime	(J)V
      //   45: aload_1
      //   46: fconst_1
      //   47: fconst_1
      //   48: fconst_1
      //   49: fconst_0
      //   50: invokeinterface 398 5 0
      //   55: aload_1
      //   56: sipush 16384
      //   59: invokeinterface 401 2 0
      //   64: aload_1
      //   65: checkcast 160	javax/microedition/khronos/opengles/GL11ExtensionPack
      //   68: astore_3
      //   69: aload_3
      //   70: ldc 165
      //   72: aload_0
      //   73: getfield 403	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFramebuffer	I
      //   76: invokeinterface 169 3 0
      //   81: aload_1
      //   82: sipush 5889
      //   85: invokeinterface 255 2 0
      //   90: aload_1
      //   91: invokeinterface 258 1 0
      //   96: aload_1
      //   97: fconst_0
      //   98: aload_0
      //   99: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   102: invokevirtual 407	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getWidth	()I
      //   105: i2f
      //   106: aload_0
      //   107: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   110: invokevirtual 410	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getHeight	()I
      //   113: i2f
      //   114: fconst_0
      //   115: ldc 72
      //   117: fconst_1
      //   118: invokeinterface 414 7 0
      //   123: aload_1
      //   124: sipush 5888
      //   127: invokeinterface 255 2 0
      //   132: aload_0
      //   133: getfield 78	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mNeedErase	Z
      //   136: ifeq +312 -> 448
      //   139: aload_1
      //   140: fconst_1
      //   141: fconst_1
      //   142: fconst_1
      //   143: fconst_0
      //   144: invokeinterface 398 5 0
      //   149: aload_1
      //   150: sipush 16384
      //   153: invokeinterface 401 2 0
      //   158: aload_0
      //   159: iconst_0
      //   160: putfield 78	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mNeedErase	Z
      //   163: aload_3
      //   164: ldc 165
      //   166: iconst_0
      //   167: invokeinterface 169 3 0
      //   172: aload_1
      //   173: sipush 5889
      //   176: invokeinterface 255 2 0
      //   181: aload_1
      //   182: invokeinterface 258 1 0
      //   187: aload_1
      //   188: fconst_0
      //   189: aload_0
      //   190: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   193: invokevirtual 407	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getWidth	()I
      //   196: i2f
      //   197: aload_0
      //   198: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   201: invokevirtual 410	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getHeight	()I
      //   204: i2f
      //   205: fconst_0
      //   206: ldc 72
      //   208: fconst_1
      //   209: invokeinterface 414 7 0
      //   214: aload_1
      //   215: sipush 5888
      //   218: invokeinterface 255 2 0
      //   223: aload_0
      //   224: getfield 56	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mBrush	Lcom/youdao/note/ui/skitch/handwrite/NBrush;
      //   227: aload_1
      //   228: aload_0
      //   229: getfield 416	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mTargetTexture	I
      //   232: aload_0
      //   233: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   236: invokevirtual 407	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getWidth	()I
      //   239: aload_0
      //   240: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   243: invokevirtual 410	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getHeight	()I
      //   246: aload_0
      //   247: getfield 418	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFramebufferWidth	I
      //   250: aload_0
      //   251: getfield 420	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFramebufferHeight	I
      //   254: invokevirtual 424	com/youdao/note/ui/skitch/handwrite/NBrush:drawOnScreen	(Ljavax/microedition/khronos/opengles/GL10;IIIII)V
      //   257: aload_0
      //   258: getfield 80	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFinshWrite	Z
      //   261: ifeq +162 -> 423
      //   264: aload_0
      //   265: ldc_w 426
      //   268: invokestatic 158	com/youdao/note/utils/L:d	(Ljava/lang/Object;Ljava/lang/String;)V
      //   271: aload_0
      //   272: iconst_0
      //   273: iconst_0
      //   274: aload_0
      //   275: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   278: invokevirtual 407	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getWidth	()I
      //   281: aload_0
      //   282: getfield 58	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mGLHandWriteView	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   285: invokevirtual 410	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getHeight	()I
      //   288: aload_1
      //   289: invokevirtual 430	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:savePixels	(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;
      //   292: astore 7
      //   294: aload_0
      //   295: getfield 74	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:leftMost	F
      //   298: ldc_w 431
      //   301: fsub
      //   302: f2i
      //   303: istore 8
      //   305: iload 8
      //   307: iflt +180 -> 487
      //   310: aload_0
      //   311: getfield 74	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:leftMost	F
      //   314: ldc 72
      //   316: fcmpl
      //   317: ifne +6 -> 323
      //   320: goto +167 -> 487
      //   323: ldc_w 431
      //   326: aload_0
      //   327: getfield 76	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:rightMost	F
      //   330: fadd
      //   331: f2i
      //   332: istore 9
      //   334: iload 9
      //   336: aload 7
      //   338: invokevirtual 434	android/graphics/Bitmap:getWidth	()I
      //   341: if_icmpgt +13 -> 354
      //   344: aload_0
      //   345: getfield 76	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:rightMost	F
      //   348: ldc 72
      //   350: fcmpl
      //   351: ifne +10 -> 361
      //   354: aload 7
      //   356: invokevirtual 434	android/graphics/Bitmap:getWidth	()I
      //   359: istore 9
      //   361: aload_0
      //   362: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   365: aload 7
      //   367: iload 8
      //   369: iconst_0
      //   370: iload 9
      //   372: iload 8
      //   374: isub
      //   375: aload 7
      //   377: invokevirtual 435	android/graphics/Bitmap:getHeight	()I
      //   380: invokestatic 439	android/graphics/Bitmap:createBitmap	(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
      //   383: invokestatic 443	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:access$202	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
      //   386: pop
      //   387: aload_0
      //   388: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   391: astore 5
      //   393: aload 5
      //   395: monitorenter
      //   396: aload_0
      //   397: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   400: invokevirtual 446	java/lang/Object:notifyAll	()V
      //   403: aload 5
      //   405: monitorexit
      //   406: aload_0
      //   407: ldc 72
      //   409: putfield 76	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:rightMost	F
      //   412: aload_0
      //   413: ldc 72
      //   415: putfield 74	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:leftMost	F
      //   418: aload_0
      //   419: iconst_1
      //   420: putfield 78	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mNeedErase	Z
      //   423: aload_0
      //   424: getfield 80	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFinshWrite	Z
      //   427: ifeq +18 -> 445
      //   430: aload_0
      //   431: iconst_0
      //   432: putfield 80	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:mFinshWrite	Z
      //   435: aload_0
      //   436: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   439: invokestatic 450	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:access$300	(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;
      //   442: invokevirtual 455	com/youdao/note/ui/skitch/handwrite/HandWriteMonitor:finishGetCharacter	()V
      //   445: aload_0
      //   446: monitorexit
      //   447: return
      //   448: aload_0
      //   449: aload_1
      //   450: invokespecial 457	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:drawPointsOnOffscreen	(Ljavax/microedition/khronos/opengles/GL10;)V
      //   453: goto -290 -> 163
      //   456: astore_2
      //   457: aload_0
      //   458: monitorexit
      //   459: aload_2
      //   460: athrow
      //   461: astore 4
      //   463: aload_0
      //   464: getfield 46	com/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender:this$0	Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
      //   467: invokevirtual 461	com/youdao/note/ui/skitch/handwrite/GLHandWriteView:getContext	()Landroid/content/Context;
      //   470: ldc_w 462
      //   473: invokestatic 468	com/youdao/note/utils/UIUtilities:showToast	(Landroid/content/Context;I)V
      //   476: goto -89 -> 387
      //   479: astore 6
      //   481: aload 5
      //   483: monitorexit
      //   484: aload 6
      //   486: athrow
      //   487: iconst_0
      //   488: istore 8
      //   490: goto -167 -> 323
      //
      // Exception table:
      //   from	to	target	type
      //   2	32	456	finally
      //   32	163	456	finally
      //   163	271	456	finally
      //   271	305	456	finally
      //   310	320	456	finally
      //   323	354	456	finally
      //   354	361	456	finally
      //   361	387	456	finally
      //   387	396	456	finally
      //   406	423	456	finally
      //   423	445	456	finally
      //   448	453	456	finally
      //   463	476	456	finally
      //   484	487	456	finally
      //   271	305	461	java/lang/OutOfMemoryError
      //   310	320	461	java/lang/OutOfMemoryError
      //   323	354	461	java/lang/OutOfMemoryError
      //   354	361	461	java/lang/OutOfMemoryError
      //   361	387	461	java/lang/OutOfMemoryError
      //   396	406	479	finally
      //   481	484	479	finally
    }

    public void onSurfaceChanged(GL10 paramGL10, int paramInt1, int paramInt2)
    {
      L.d(this, "on surface changed " + paramInt1 + " * " + paramInt2);
      paramGL10.glViewport(0, 0, paramInt1, paramInt2);
    }

    public void onSurfaceCreated(GL10 paramGL10, EGLConfig paramEGLConfig)
    {
      String str = paramGL10.glGetString(7939);
      L.d(this, str);
      if (!str.contains("GL_OES_framebuffer_object"))
      {
        L.e(this, "Framebuffer is not supported!", null);
        return;
      }
      this.mFramebufferWidth = resize(this.mGLHandWriteView.getWidth());
      this.mFramebufferHeight = resize(this.mGLHandWriteView.getHeight());
      this.mTargetTexture = createTargetTexture(paramGL10, this.mFramebufferWidth, this.mFramebufferHeight);
      this.mFramebuffer = createFrameBuffer(paramGL10, this.mFramebufferWidth, this.mFramebufferHeight, this.mTargetTexture);
      this.mBrush.loadGLTexture(paramGL10, GLHandWriteView.this.getContext());
      paramGL10.glViewport(0, 0, this.mGLHandWriteView.getWidth(), this.mGLHandWriteView.getHeight());
      paramGL10.glMatrixMode(5889);
      paramGL10.glLoadIdentity();
      paramGL10.glOrthof(0.0F, this.mGLHandWriteView.getWidth(), this.mGLHandWriteView.getHeight(), 0.0F, -1.0F, 1.0F);
      paramGL10.glMatrixMode(5888);
      paramGL10.glDisable(3024);
      paramGL10.glEnable(3553);
      paramGL10.glEnable(3042);
      paramGL10.glTexEnvf(8960, 8704, 3042.0F);
      paramGL10.glBlendFunc(1, 771);
      paramGL10.glEnableClientState(32884);
      paramGL10.glEnableClientState(32888);
    }

    public void resetState()
    {
      monitorenter;
      try
      {
        this.eventList.clear();
        monitorexit;
        return;
      }
      finally
      {
        localObject = finally;
        monitorexit;
        throw localObject;
      }
    }

    protected Bitmap savePixels(int paramInt1, int paramInt2, int paramInt3, int paramInt4, GL10 paramGL10)
    {
      L.d(this, "In save pixcls block.");
      if ((this.b == null) && (this.bt == null))
      {
        this.b = new int[this.mGLHandWriteView.getWidth() * this.mGLHandWriteView.getHeight()];
        this.bt = new int[this.mGLHandWriteView.getWidth() * this.mGLHandWriteView.getHeight()];
      }
      L.d(this, "In save pixcls block2.");
      IntBuffer localIntBuffer = IntBuffer.wrap(this.b);
      L.d(this, "In save pixcls block3.");
      localIntBuffer.position(0);
      paramGL10.glReadPixels(paramInt1, paramInt2, paramInt3, paramInt4, 6408, 5121, localIntBuffer);
      L.d(this, "In save pixcls block4.");
      for (int i = 0; i < paramInt4; ++i)
        for (int j = 0; j < paramInt3; ++j)
          this.bt[(j + paramInt3 * (-1 + (paramInt4 - i)))] = this.b[(j + i * paramInt3)];
      return Bitmap.createBitmap(this.bt, paramInt3, paramInt4, Bitmap.Config.ARGB_8888);
    }
  }

  private class PerformanceMonitor
  {
    private long cost;
    private long lastTime = -1L;
    private long max = -9223372036854775808L;
    private long min = 9223372036854775807L;
    private long n;

    private PerformanceMonitor()
    {
    }

    public long getAvgCost()
    {
      monitorenter;
      long l2;
      try
      {
        long l1 = this.n;
        if (l1 <= 4L)
        {
          l2 = 0L;
          return l2;
        }
      }
      finally
      {
        monitorexit;
      }
    }

    public void recordTime(long paramLong)
    {
      monitorenter;
      while (true)
        try
        {
          if (this.lastTime == -1L)
          {
            this.lastTime = paramLong;
            this.cost = 0L;
            this.n = 0L;
            this.max = -9223372036854775808L;
            this.min = 9223372036854775807L;
            return;
          }
          long l = paramLong - this.lastTime;
          if (l > this.max)
            this.max = l;
          if (l < this.min)
            this.min = l;
          this.cost = (l + this.cost);
          this.n = (1L + this.n);
        }
        finally
        {
          monitorexit;
        }
    }

    public void reset()
    {
      monitorenter;
      try
      {
        this.lastTime = -1L;
        monitorexit;
        return;
      }
      finally
      {
        localObject = finally;
        monitorexit;
        throw localObject;
      }
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.GLHandWriteView
 * JD-Core Version:    0.5.4
 */