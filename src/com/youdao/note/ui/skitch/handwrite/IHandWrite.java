package com.youdao.note.ui.skitch.handwrite;

import android.graphics.Bitmap;
import com.youdao.note.ui.skitch.ISkitch;

public abstract interface IHandWrite extends ISkitch
{
  public abstract void addBlank();

  public abstract void addCharacter(Bitmap paramBitmap);

  public abstract void addReturn();

  public abstract void invalidateCanvas();

  public abstract void setIswritting(boolean paramBoolean);

  public abstract void startCursorDrawer();

  public abstract void stopCursorDrawer();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.IHandWrite
 * JD-Core Version:    0.5.4
 */