package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import com.youdao.note.YNoteApplication;
import com.youdao.note.tool.img.ImageProcess;
import com.youdao.note.ui.skitch.ISkitchMeta;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;
import com.youdao.note.utils.L;
import java.util.Iterator;
import java.util.List;

public class HandWritePageView extends View
  implements IHandWrite, SkitchConsts.HandWrite
{
  private int SCREEN_HEIGHT = getHeight();
  private int SCREEN_WIDTH = getWidth();
  private int cursorColor = -1;
  private CursorDrawer cursorDrawer;
  private boolean firstDraw = true;
  private HandwriteMeta handwriteMeta;
  private boolean isWritting = false;
  private double mDensity;
  private Paint paint = new Paint();

  public HandWritePageView(Context paramContext)
  {
    this(paramContext, null);
  }

  public HandWritePageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mDensity = (0.5D + getResources().getDisplayMetrics().density);
  }

  public HandWritePageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private Point calCursorPosition()
  {
    List localList = this.handwriteMeta.getWordWidthList();
    Point localPoint = new Point(SPACING, PADDING_TOP);
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Bitmap localBitmap = (Bitmap)localIterator.next();
      if (localBitmap == RETURN_CHARACTER)
        localPoint.set(SPACING, localPoint.y + LINE_HEIGHT);
      if (localPoint.x + SPACING + getCharWidth(localBitmap) > this.SCREEN_WIDTH)
        localPoint.set(SPACING + getCharWidth(localBitmap) + SPACING, localPoint.y + LINE_HEIGHT);
      localPoint.set(localPoint.x + SPACING + getCharWidth(localBitmap), localPoint.y);
    }
    return localPoint;
  }

  private int calScreenTopY(Point paramPoint)
  {
    if (paramPoint.y > this.SCREEN_HEIGHT - LINE_HEIGHT - PADDING_BOTTOM - WORD_HEIGHT)
      return paramPoint.y + LINE_HEIGHT + PADDING_BOTTOM + WORD_HEIGHT - this.SCREEN_HEIGHT + LINE_STROKE;
    return 0;
  }

  private void drawCursor(boolean paramBoolean)
  {
    if (paramBoolean);
    for (this.cursorColor = -16777216; ; this.cursorColor = -1)
    {
      if (!this.isWritting)
        postInvalidate();
      return;
    }
  }

  private void drawHandwrites(int paramInt1, int paramInt2, Canvas paramCanvas)
  {
    this.paint.setColor(-16777216);
    drawLines(paramInt1, paramInt2, paramCanvas);
    int i = LINE_HEIGHT - PADDING_BOTTOM;
    int j = SPACING;
    int k = 0;
    List localList = this.handwriteMeta.getWordWidthList();
    if ((i < paramInt1) && (k < localList.size()))
    {
      label42: Bitmap localBitmap3 = (Bitmap)localList.get(k);
      if (localBitmap3 == RETURN_CHARACTER)
      {
        i += LINE_HEIGHT;
        j = SPACING;
      }
      while (true)
      {
        ++k;
        break label42:
        if (j + SPACING + getCharWidth(localBitmap3) > this.SCREEN_WIDTH)
        {
          j = SPACING;
          i += LINE_HEIGHT;
        }
        j += SPACING + getCharWidth(localBitmap3);
      }
    }
    if ((i < paramInt1 + LINE_HEIGHT) && (k < localList.size()))
    {
      label155: Bitmap localBitmap2 = (Bitmap)localList.get(k);
      if (localBitmap2 == RETURN_CHARACTER)
      {
        i += LINE_HEIGHT;
        j = SPACING;
      }
      while (true)
      {
        ++k;
        break label155:
        if (j + SPACING + getCharWidth(localBitmap2) > this.SCREEN_WIDTH)
        {
          int i3 = SPACING;
          i += LINE_HEIGHT;
          paramCanvas.drawBitmap(localBitmap2, new Rect(0, 0, localBitmap2.getWidth(), localBitmap2.getHeight()), new Rect(i3, i - WORD_HEIGHT - paramInt1, i3 + getCharWidth(localBitmap2), i - paramInt1), this.paint);
          j = i3 + (getCharWidth(localBitmap2) + SPACING);
        }
        int i1 = i - paramInt1 - getCharHeight(localBitmap2);
        if (i1 < 0)
          i1 = 0;
        int i2 = localBitmap2.getHeight() - (int)((i - paramInt1 - i1) * this.mDensity);
        if (i2 < 0)
          i2 = 0;
        paramCanvas.drawBitmap(localBitmap2, new Rect(0, i2, localBitmap2.getWidth(), localBitmap2.getHeight()), new Rect(j, i1, j + getCharWidth(localBitmap2), i - paramInt1), this.paint);
        j += SPACING + getCharWidth(localBitmap2);
      }
    }
    if ((i > paramInt2) || (k >= localList.size()))
      label451: return;
    Bitmap localBitmap1 = (Bitmap)localList.get(k);
    if (localBitmap1 == RETURN_CHARACTER)
    {
      i += LINE_HEIGHT;
      j = SPACING;
    }
    while (true)
    {
      ++k;
      break label451:
      if (j + SPACING + getCharWidth(localBitmap1) > this.SCREEN_WIDTH)
      {
        int l = SPACING;
        i += LINE_HEIGHT;
        paramCanvas.drawBitmap(localBitmap1, new Rect(0, 0, localBitmap1.getWidth(), localBitmap1.getHeight()), new Rect(l, i - WORD_HEIGHT - paramInt1, l + getCharWidth(localBitmap1), i - paramInt1), this.paint);
        j = l + (getCharWidth(localBitmap1) + SPACING);
      }
      paramCanvas.drawBitmap(localBitmap1, new Rect(0, 0, localBitmap1.getWidth(), localBitmap1.getHeight()), new Rect(j, i - WORD_HEIGHT - paramInt1, j + getCharWidth(localBitmap1), i - paramInt1), this.paint);
      j += SPACING + getCharWidth(localBitmap1);
    }
  }

  private void drawLines(int paramInt1, int paramInt2, Canvas paramCanvas)
  {
    this.paint.setColor(LINE_COLOR);
    this.paint.setStrokeWidth(LINE_STROKE);
    this.paint.setAntiAlias(true);
    int i = LINE_HEIGHT - paramInt1 % LINE_HEIGHT;
    while (i < paramInt2)
    {
      paramCanvas.drawLine(SPACING, i, getWidth() - SPACING, i, this.paint);
      i += LINE_HEIGHT;
    }
    this.paint.setColor(-16777216);
  }

  private int getCharHeight(Bitmap paramBitmap)
  {
    return (int)(paramBitmap.getHeight() / this.mDensity);
  }

  private int getCharWidth(Bitmap paramBitmap)
  {
    return (int)(paramBitmap.getWidth() / this.mDensity);
  }

  private void innerAddCharacter(Bitmap paramBitmap)
  {
    this.handwriteMeta.getWordWidthList().add(paramBitmap);
    this.cursorColor = -16777216;
    if (this.cursorDrawer != null)
      this.cursorDrawer.delay();
    invalidateCanvas();
  }

  public void addBlank()
  {
    innerAddCharacter(BLANK_CHARACTER);
  }

  public void addCharacter(Bitmap paramBitmap)
  {
    Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, (int)(WORD_HEIGHT / paramBitmap.getHeight() * paramBitmap.getWidth() * this.mDensity), (int)(WORD_HEIGHT * this.mDensity), true);
    if (YNoteApplication.getInstance().getHandwriteMode() == 1)
      ImageProcess.smooth(paramBitmap, localBitmap);
    innerAddCharacter(localBitmap);
  }

  public void addReturn()
  {
    innerAddCharacter(RETURN_CHARACTER);
  }

  public Bitmap getBitmap()
  {
    int i = 1 + (calCursorPosition().y + WORD_HEIGHT + PADDING_BOTTOM + LINE_STROKE);
    if (i < this.SCREEN_HEIGHT)
      i = this.SCREEN_HEIGHT;
    Bitmap localBitmap = Bitmap.createBitmap(this.SCREEN_WIDTH, i, Bitmap.Config.ARGB_4444);
    drawHandwrites(0, i, new Canvas(localBitmap));
    return localBitmap;
  }

  public float getPaintWidth()
  {
    return this.handwriteMeta.getPaintWidth();
  }

  public float getPaintWidthRatio()
  {
    return getPaintWidth() / (MAX_PAINT_WIDTH - MIN_PAINT_WIDTH);
  }

  public void initSkitchMeta(ISkitchMeta paramISkitchMeta)
  {
    this.handwriteMeta = ((HandwriteMeta)paramISkitchMeta);
    startCursorDrawer();
    invalidateCanvas();
  }

  public void invalidateCanvas()
  {
    postInvalidate();
  }

  protected void onDraw(Canvas paramCanvas)
  {
    if (this.firstDraw)
    {
      this.SCREEN_WIDTH = getWidth();
      this.SCREEN_HEIGHT = getHeight();
      this.paint.setAntiAlias(true);
      this.paint.setFilterBitmap(true);
      this.paint.setDither(true);
      this.firstDraw = false;
    }
    Point localPoint = calCursorPosition();
    int i = calScreenTopY(localPoint);
    drawHandwrites(i, i + this.SCREEN_HEIGHT, paramCanvas);
    this.paint.setColor(this.cursorColor);
    paramCanvas.drawLine(localPoint.x, localPoint.y - i, localPoint.x, localPoint.y + WORD_HEIGHT - i, this.paint);
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.firstDraw = true;
    invalidate();
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  protected void onWindowVisibilityChanged(int paramInt)
  {
    super.onWindowVisibilityChanged(paramInt);
    if (paramInt == 0)
    {
      startCursorDrawer();
      return;
    }
    stopCursorDrawer();
  }

  public void setIswritting(boolean paramBoolean)
  {
    this.isWritting = paramBoolean;
  }

  public void setPaintWidth(float paramFloat)
  {
    this.handwriteMeta.setPaintWidth(paramFloat);
  }

  public void startCursorDrawer()
  {
    monitorenter;
    try
    {
      if (this.cursorDrawer == null)
        this.cursorDrawer = new CursorDrawer(null);
      this.cursorDrawer.startDrawing();
      L.d(this, "cursor drawer started");
      return;
    }
    finally
    {
      monitorexit;
    }
  }

  public void stopCursorDrawer()
  {
    monitorenter;
    try
    {
      if (this.cursorDrawer != null)
      {
        this.cursorDrawer.stopDrawing();
        this.cursorDrawer = null;
      }
      return;
    }
    finally
    {
      monitorexit;
    }
  }

  public void trash()
  {
    this.handwriteMeta.trash();
    invalidate();
  }

  public void undo()
  {
    List localList = this.handwriteMeta.getWordWidthList();
    int i = localList.size();
    if (i <= 0)
      return;
    Bitmap localBitmap = (Bitmap)localList.remove(i - 1);
    if ((localBitmap != RETURN_CHARACTER) && (localBitmap != BLANK_CHARACTER))
      localBitmap.recycle();
    this.cursorColor = -16777216;
    if (this.cursorDrawer != null)
      this.cursorDrawer.delay();
    invalidateCanvas();
  }

  private class CursorDrawer extends AsyncTask<Void, Boolean, Void>
  {
    private static final long INTERVAL = 500L;
    private volatile boolean delay = false;
    private volatile boolean running = false;

    private CursorDrawer()
    {
    }

    public void delay()
    {
      this.delay = true;
    }

    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      boolean bool = true;
      if (this.running)
      {
        if (!this.delay)
        {
          label2: Boolean[] arrayOfBoolean = new Boolean[1];
          arrayOfBoolean[0] = Boolean.valueOf(bool);
          publishProgress(arrayOfBoolean);
          if (bool)
            break label60;
        }
        for (bool = true; ; bool = false)
        {
          while (true)
          {
            this.delay = false;
            try
            {
              Thread.sleep(500L);
            }
            catch (InterruptedException localInterruptedException)
            {
            }
          }
          label60: break label2:
        }
      }
      return null;
    }

    protected void onProgressUpdate(Boolean[] paramArrayOfBoolean)
    {
      HandWritePageView.this.drawCursor(paramArrayOfBoolean[0].booleanValue());
    }

    public void startDrawing()
    {
      if (this.running)
        return;
      this.running = true;
      execute(new Void[0]);
    }

    public void stopDrawing()
    {
      if (!this.running)
        return;
      this.running = false;
      cancel(true);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.HandWritePageView
 * JD-Core Version:    0.5.4
 */