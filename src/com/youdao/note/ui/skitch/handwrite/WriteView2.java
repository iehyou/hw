package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;
import com.youdao.note.utils.CompatUtils;
import com.youdao.note.utils.L;

public class WriteView2 extends View
  implements IWrite, SkitchConsts.HandWrite
{
  private static final int MAX_WIDTH = 40;
  private static final double MAX_WIDTH_STEP = 0.1D;
  private static final int STAND_WIDTH = 20;
  private Rect dstRect = new Rect();
  private Paint mBackGroundPaint = new Paint(5);
  private QuadraticBezierSpline mBezier = new QuadraticBezierSpline();
  private Bitmap mBitmap;
  private Canvas mCanvas;
  private float mDensity;
  private Bitmap mDot;
  private Bitmap mDot2;
  private PointTracker mDrawPoint = new PointTracker();
  private Paint mForeGroundPaint = new Paint(5);
  private IHandWrite mHandWriteCanvas;
  private PointTracker mLastPoint;
  private double mLastSpeed = 0.0D;
  private float mMaxWidth;
  private HandWriteMonitor mMonitor;
  private Paint mPaint = new Paint(5);
  private float mStandWidth;
  private Matrix matrix = new Matrix();
  private Bitmap tmpBitmap;

  public WriteView2(Context paramContext)
  {
    this(paramContext, null);
  }

  public WriteView2(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public WriteView2(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet);
    L.d(this, "WriteView2 created");
  }

  private void drawPoint(PointTracker paramPointTracker)
  {
    this.dstRect.left = (int)(paramPointTracker.x - 0.8D * (paramPointTracker.width / 2.0D));
    this.dstRect.top = (int)(paramPointTracker.y - 0.8D * (paramPointTracker.width / 2.0D));
    this.dstRect.right = (int)(paramPointTracker.x + 0.8D * (paramPointTracker.width / 2.0D));
    this.dstRect.bottom = (int)(paramPointTracker.y + 0.8D * (paramPointTracker.width / 2.0D));
    this.mCanvas.drawBitmap(this.mDot2, null, this.dstRect, this.mForeGroundPaint);
    this.dstRect.left = (int)(paramPointTracker.x - paramPointTracker.width / 2.0D);
    this.dstRect.top = (int)(paramPointTracker.y - paramPointTracker.width / 2.0D);
    this.dstRect.right = (int)(paramPointTracker.x + paramPointTracker.width / 2.0D);
    this.dstRect.bottom = (int)(paramPointTracker.y + paramPointTracker.width / 2.0D);
    this.mCanvas.drawBitmap(this.mDot, null, this.dstRect, this.mBackGroundPaint);
  }

  private void drawToPoint(float paramFloat1, float paramFloat2, long paramLong)
  {
    if (this.mLastPoint == null)
    {
      PointTracker localPointTracker = new PointTracker(paramFloat1, paramFloat2);
      this.mLastPoint = localPointTracker;
      this.mLastPoint.time = paramLong;
      this.mLastPoint.width = (2.0F * (this.mStandWidth * this.mHandWriteCanvas.getPaintWidthRatio()));
    }
    float f1 = paramFloat1 - this.mLastPoint.x;
    float f2 = paramFloat2 - this.mLastPoint.y;
    double d1 = Math.sqrt(f1 * f1 + f2 * f2) / this.mDensity / this.mDensity;
    double d2;
    label122: double d5;
    label244: double d6;
    double d7;
    double d8;
    float f3;
    float f4;
    int j;
    if (paramLong == this.mLastPoint.time)
    {
      d2 = 1.0D;
      double d3 = 0.7D * this.mLastSpeed + 0.3D * d2;
      this.mLastSpeed = d3;
      int i = (int)d1;
      if (i == 0)
        i = 1;
      double d4 = 3.0D * (this.mStandWidth * this.mHandWriteCanvas.getPaintWidthRatio()) / (0.5D + d3);
      if (d4 > this.mMaxWidth)
        d4 = this.mMaxWidth;
      d5 = (d4 - this.mLastPoint.width) / i;
      if (Math.abs(d5) > 0.1D)
      {
        if (d5 <= 0.0D)
          break label435;
        d5 = 0.1D;
      }
      d6 = this.mLastPoint.width + d5 * i;
      d7 = f1 / i;
      d8 = f2 / i;
      Log.d("Write", "speed is " + d3 + "\n next width is " + d6 + "\n diff is " + i);
      this.mBezier.addPoint(paramFloat1, paramFloat2, d6);
      f3 = -1.0F;
      f4 = -1.0F;
      j = 0;
      label346: if (j > i)
        break label554;
      if (this.mDensity < 0.9D)
        break label443;
      this.mBezier.getPoint(this.mDrawPoint, 1.0D * j / i);
      label385: if ((this.mDrawPoint.x != f3) || (this.mDrawPoint.x != f4))
        break label517;
    }
    while (true)
    {
      ++j;
      break label346:
      d2 = d1 / (paramLong - this.mLastPoint.time);
      break label122:
      label435: d5 = -0.1D;
      break label244:
      label443: this.mDrawPoint.x = (int)(this.mLastPoint.x + d7 * j);
      this.mDrawPoint.y = (int)(this.mLastPoint.y + d8 * j);
      this.mDrawPoint.width = (int)(this.mLastPoint.width + d5 * j);
      break label385:
      label517: f3 = this.mDrawPoint.x;
      f4 = this.mDrawPoint.y;
      drawPoint(this.mDrawPoint);
      invalidate(this.dstRect);
    }
    label554: this.mLastPoint.x = this.mDrawPoint.x;
    this.mLastPoint.y = this.mDrawPoint.y;
    this.mLastPoint.time = paramLong;
    this.mLastPoint.width = d6;
  }

  public void finishWrite()
  {
  }

  public void getCharacter(IWrite.IGetBitmap paramIGetBitmap)
  {
    paramIGetBitmap.onBitmapObtained(Bitmap.createBitmap(this.mBitmap, 0, 0, this.mBitmap.getWidth(), this.mBitmap.getHeight(), this.matrix, true));
  }

  public void init(float paramFloat, int paramInt1, int paramInt2)
  {
    this.mDensity = paramFloat;
    this.mStandWidth = (20.0F * this.mDensity);
    this.mMaxWidth = (40.0F * this.mDensity);
    Log.d("Write", "density is " + this.mDensity);
    this.mBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    this.tmpBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    this.mCanvas = new Canvas(this.tmpBitmap);
    this.mCanvas.drawColor(-1);
    this.mCanvas.setBitmap(this.mBitmap);
    this.mCanvas.drawColor(0);
    this.mDot = BitmapFactory.decodeResource(getResources(), 2130837736);
    this.mDot2 = BitmapFactory.decodeResource(getResources(), 2130837735);
    this.mForeGroundPaint.setAlpha(200);
    this.mBackGroundPaint.setAlpha(40);
    this.mPaint.setColor(-16777216);
    float f = (float)(1.0D * WORD_HEIGHT / this.mBitmap.getHeight());
    this.matrix.postScale(f, f);
  }

  public void onDraw(Canvas paramCanvas)
  {
    paramCanvas.drawBitmap(this.mBitmap, 0.0F, 0.0F, this.mPaint);
    super.onDraw(paramCanvas);
  }

  public void onFinishWrite()
  {
    L.d(this, "OnFinishtWrite called.");
    this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
  }

  public void onStartWrite()
  {
    L.d(this, "OnStartWrite called.");
    this.mCanvas.drawColor(-1);
    invalidate();
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    L.d(this, "on Touch for writeview2 called.");
    this.mMonitor.touch();
    if (CompatUtils.getActionMasked(paramMotionEvent) == 0)
      this.mBezier.clear();
    drawToPoint(paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getEventTime());
    if (CompatUtils.getActionMasked(paramMotionEvent) == 1)
      drawToPoint(paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getEventTime());
    return true;
  }

  public void setHandWriteCanvas(IHandWrite paramIHandWrite)
  {
    this.mHandWriteCanvas = paramIHandWrite;
  }

  public void setTouchMonotor(HandWriteMonitor paramHandWriteMonitor)
  {
    this.mMonitor = paramHandWriteMonitor;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.WriteView2
 * JD-Core Version:    0.5.4
 */