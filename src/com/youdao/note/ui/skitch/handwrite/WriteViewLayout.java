package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.youdao.note.YNoteApplication;
import com.youdao.note.utils.L;

public class WriteViewLayout extends FrameLayout
  implements IWrite
{
  private IHandWrite mCanvas;
  private View mDoneView;
  private HandWriteMonitor mMonitor;
  private IWrite mWriteView;

  public WriteViewLayout(Context paramContext)
  {
    this(paramContext, null);
  }

  public WriteViewLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public WriteViewLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    LayoutInflater.from(paramContext).inflate(2130903117, this, true);
    if (YNoteApplication.getInstance().getHandwriteMode() == 1);
    for (this.mWriteView = ((IWrite)findViewById(2131165362)); ; this.mWriteView = ((IWrite)findViewById(2131165361)))
    {
      ((View)this.mWriteView).setVisibility(0);
      this.mDoneView = findViewById(2131165363);
      this.mDoneView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          WriteViewLayout.this.finishWrite();
        }
      });
      return;
    }
  }

  public void finishWrite()
  {
    if (!this.mMonitor.writeStarted())
      return;
    this.mMonitor.finishWrite();
  }

  public void getCharacter(IWrite.IGetBitmap paramIGetBitmap)
  {
    this.mWriteView.getCharacter(paramIGetBitmap);
  }

  public void init(float paramFloat, int paramInt1, int paramInt2)
  {
    this.mWriteView.init(paramFloat, paramInt1, paramInt2);
  }

  public void onFinishWrite()
  {
    this.mWriteView.onFinishWrite();
    L.d(this, "Before GONE");
    setVisibility(8);
    L.d(this, "After GONE");
    this.mCanvas.invalidateCanvas();
    this.mCanvas.startCursorDrawer();
  }

  public void onPause()
  {
    if (!this.mWriteView instanceof GLSurfaceView)
      return;
    ((GLSurfaceView)this.mWriteView).onPause();
  }

  public void onResume()
  {
    if (!this.mWriteView instanceof GLSurfaceView)
      return;
    ((GLSurfaceView)this.mWriteView).onResume();
  }

  public void onStartWrite()
  {
    this.mCanvas.stopCursorDrawer();
    this.mWriteView.onStartWrite();
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    L.d(this, "on Touch for writeviewlayout called.");
    this.mWriteView.onTouchEvent(paramMotionEvent);
    return true;
  }

  public void setHandWriteCanvas(IHandWrite paramIHandWrite)
  {
    this.mCanvas = paramIHandWrite;
    this.mMonitor = new HandWriteMonitor(paramIHandWrite, this);
    this.mWriteView.setHandWriteCanvas(paramIHandWrite);
    this.mWriteView.setTouchMonotor(this.mMonitor);
  }

  public void setTouchMonotor(HandWriteMonitor paramHandWriteMonitor)
  {
    this.mWriteView.setTouchMonotor(paramHandWriteMonitor);
  }

  public void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    if ((paramInt != 0) || (this.mDoneView.getVisibility() == 0))
      return;
    this.mDoneView.setVisibility(0);
  }

  public void switchToNormalMode()
  {
    findViewById(2131165362).setVisibility(8);
    findViewById(2131165361).setVisibility(0);
    this.mWriteView = ((IWrite)findViewById(2131165361));
    setHandWriteCanvas(this.mCanvas);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.WriteViewLayout
 * JD-Core Version:    0.5.4
 */