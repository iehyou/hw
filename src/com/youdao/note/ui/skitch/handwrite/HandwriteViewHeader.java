package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;

public class HandwriteViewHeader extends View
  implements SkitchConsts.HandWrite
{
  private final Bitmap topLine = BitmapFactory.decodeResource(getResources(), 2130837613);
  private final Bitmap topWidget = BitmapFactory.decodeResource(getResources(), 2130837614);

  public HandwriteViewHeader(Context paramContext)
  {
    super(paramContext);
  }

  public HandwriteViewHeader(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public HandwriteViewHeader(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = getWidth() / (int)HEADER_W;
    float f1 = getWidth() / i - this.topWidget.getWidth();
    Paint localPaint = new Paint();
    localPaint.setColor(-1);
    paramCanvas.drawPaint(localPaint);
    float f2 = f1 / 2.0F;
    while (f2 < getWidth())
    {
      paramCanvas.drawBitmap(this.topWidget, f2, 0.0F, null);
      f2 += getWidth() / i;
    }
    paramCanvas.drawBitmap(this.topLine, new Rect(0, 0, this.topLine.getWidth(), this.topLine.getHeight()), new RectF(SPACING, getHeight() - this.topLine.getHeight(), getWidth() - SPACING, getHeight()), null);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.HandwriteViewHeader
 * JD-Core Version:    0.5.4
 */