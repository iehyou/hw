package com.youdao.note.ui.skitch.handwrite;

public class QuadraticBezierSpline2
{
  private PointTracker mControl = new PointTracker();
  private PointTracker mDstKnot = new PointTracker();
  private PointTracker mNextControl = new PointTracker();
  private PointTracker mSrcKnot = new PointTracker();

  private float get(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return (float)(paramDouble1 + paramDouble4 * (2.0D * paramDouble2 - 2.0D * paramDouble1) + paramDouble4 * (paramDouble4 * (paramDouble3 + (paramDouble1 - 2.0D * paramDouble2))));
  }

  private float getWidth(double paramDouble)
  {
    return get(this.mSrcKnot.width, this.mControl.width, this.mDstKnot.width, paramDouble);
  }

  private float getX(double paramDouble)
  {
    return get(this.mSrcKnot.x, this.mControl.x, this.mDstKnot.x, paramDouble);
  }

  private float getY(double paramDouble)
  {
    return get(this.mSrcKnot.y, this.mControl.y, this.mDstKnot.y, paramDouble);
  }

  public void AddNode(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.mSrcKnot.set(this.mDstKnot.x, this.mDstKnot.y, this.mDstKnot.width);
    this.mControl.set(this.mNextControl.x, this.mNextControl.y, this.mNextControl.width);
    float f1 = (paramFloat1 + this.mNextControl.x) / 2.0F;
    float f2 = (paramFloat2 + this.mNextControl.y) / 2.0F;
    double d = (this.mNextControl.width + paramFloat3) / 2.0D;
    this.mDstKnot.set(f1, f2, d);
    this.mNextControl.set(paramFloat1, paramFloat2, paramFloat3);
  }

  public void End()
  {
    this.mSrcKnot.set(this.mDstKnot.x, this.mDstKnot.y, this.mDstKnot.width);
    float f1 = (this.mNextControl.x + this.mSrcKnot.x) / 2.0F;
    float f2 = (this.mNextControl.y + this.mSrcKnot.y) / 2.0F;
    double d = (this.mNextControl.width + this.mSrcKnot.width) / 2.0D;
    this.mControl.set(f1, f2, d);
    this.mDstKnot.set(this.mNextControl.x, this.mNextControl.y, this.mNextControl.width);
  }

  public void Init(float paramFloat1, float paramFloat2, double paramDouble1, float paramFloat3, float paramFloat4, double paramDouble2)
  {
    this.mSrcKnot.set(paramFloat1, paramFloat2, paramDouble1);
    float f1 = (paramFloat1 + paramFloat3) / 2.0F;
    float f2 = (paramFloat2 + paramFloat4) / 2.0F;
    double d1 = (paramDouble1 + paramDouble2) / 2.0D;
    this.mDstKnot.set(f1, f2, d1);
    float f3 = (f1 + paramFloat1) / 2.0F;
    float f4 = (f2 + paramFloat2) / 2.0F;
    double d2 = (d1 + paramDouble1) / 2.0D;
    this.mControl.set(f3, f4, d2);
    this.mNextControl.set(paramFloat3, paramFloat4, paramDouble2);
  }

  public void getPoint(PointTracker paramPointTracker, double paramDouble)
  {
    paramPointTracker.set(getX(paramDouble), getY(paramDouble), getWidth(paramDouble));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.QuadraticBezierSpline2
 * JD-Core Version:    0.5.4
 */