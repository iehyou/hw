package com.youdao.note.ui.skitch.handwrite;

import android.graphics.Bitmap;
import com.youdao.note.YNoteApplication;
import com.youdao.note.ui.skitch.ISkitchMeta;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HandwriteMeta
  implements ISkitchMeta, SkitchConsts.HandWrite
{
  private boolean isNew = true;
  private float paintWidth = YNoteApplication.getInstance().getHandWritePaintWidth();
  private List<Bitmap> wordWidthList = new ArrayList();

  public HandwriteMeta copy()
  {
    HandwriteMeta localHandwriteMeta = new HandwriteMeta();
    localHandwriteMeta.setIsNew(this.isNew);
    localHandwriteMeta.setPaintWidth(this.paintWidth);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.wordWidthList.iterator();
    while (localIterator.hasNext())
    {
      Bitmap localBitmap = (Bitmap)localIterator.next();
      localArrayList.add(localBitmap.copy(localBitmap.getConfig(), true));
    }
    localHandwriteMeta.setWordWidthList(localArrayList);
    return localHandwriteMeta;
  }

  public float getPaintWidth()
  {
    return this.paintWidth;
  }

  public List<Bitmap> getWordWidthList()
  {
    return this.wordWidthList;
  }

  public boolean isEmpty()
  {
    return (this.wordWidthList == null) || (this.wordWidthList.size() == 0);
  }

  public boolean isNew()
  {
    return this.isNew;
  }

  public void onDestroy()
  {
    Iterator localIterator = this.wordWidthList.iterator();
    while (localIterator.hasNext())
    {
      Bitmap localBitmap = (Bitmap)localIterator.next();
      if ((localBitmap == BLANK_CHARACTER) || (localBitmap == RETURN_CHARACTER))
        continue;
      localBitmap.recycle();
    }
  }

  public void setIsNew(boolean paramBoolean)
  {
    this.isNew = paramBoolean;
  }

  public void setPaintWidth(float paramFloat)
  {
    YNoteApplication.getInstance().setHandWritePaintWidth(paramFloat);
    this.paintWidth = paramFloat;
  }

  public void setWordWidthList(List<Bitmap> paramList)
  {
    this.wordWidthList = paramList;
  }

  public void trash()
  {
    onDestroy();
    this.wordWidthList = new ArrayList();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.HandwriteMeta
 * JD-Core Version:    0.5.4
 */