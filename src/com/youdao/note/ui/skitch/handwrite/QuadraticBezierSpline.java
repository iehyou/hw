package com.youdao.note.ui.skitch.handwrite;

public class QuadraticBezierSpline
{
  private PointTracker mControlPoint = new PointTracker();
  private PointTracker mFirstPoint = new PointTracker();
  private PointTracker mNextPoint = new PointTracker();
  private PointTracker mSecondPoint = new PointTracker();
  private boolean mStarted = false;

  private float get(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    return (float)(paramDouble1 + paramDouble4 * (2.0D * paramDouble2 - 2.0D * paramDouble1) + paramDouble4 * (paramDouble4 * (paramDouble3 + (paramDouble1 - 2.0D * paramDouble2))));
  }

  private float getWidth(double paramDouble)
  {
    return get(this.mFirstPoint.width, this.mControlPoint.width, this.mSecondPoint.width, paramDouble);
  }

  private float getX(double paramDouble)
  {
    return get(this.mFirstPoint.x, this.mControlPoint.x, this.mSecondPoint.x, paramDouble);
  }

  private float getY(double paramDouble)
  {
    return get(this.mFirstPoint.y, this.mControlPoint.y, this.mSecondPoint.y, paramDouble);
  }

  public void addPoint(float paramFloat1, float paramFloat2, double paramDouble)
  {
    if (!this.mStarted)
    {
      this.mFirstPoint.set(paramFloat1, paramFloat2, paramDouble);
      this.mSecondPoint.set(paramFloat1, paramFloat2, paramDouble);
      this.mControlPoint.set(paramFloat1, paramFloat2, paramDouble);
      this.mNextPoint.set(paramFloat1, paramFloat2, paramDouble);
      this.mStarted = true;
    }
    this.mFirstPoint.set(this.mControlPoint);
    this.mControlPoint.set(this.mSecondPoint);
    this.mSecondPoint.set((this.mControlPoint.x + this.mNextPoint.x) / 2.0F, (this.mControlPoint.y + this.mNextPoint.y) / 2.0F, (this.mControlPoint.width + this.mNextPoint.width) / 2.0D);
    this.mNextPoint.set(paramFloat1, paramFloat2, paramDouble);
  }

  public void clear()
  {
    this.mStarted = false;
  }

  public void getPoint(PointTracker paramPointTracker, double paramDouble)
  {
    if (!this.mStarted)
      return;
    paramPointTracker.set(getX(paramDouble), getY(paramDouble), getWidth(paramDouble));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.QuadraticBezierSpline
 * JD-Core Version:    0.5.4
 */