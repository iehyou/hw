package com.youdao.note.ui.skitch.handwrite;

import android.graphics.Bitmap;
import android.view.MotionEvent;

public abstract interface IWrite
{
  public abstract void finishWrite();

  public abstract void getCharacter(IGetBitmap paramIGetBitmap);

  public abstract void init(float paramFloat, int paramInt1, int paramInt2);

  public abstract void onFinishWrite();

  public abstract void onStartWrite();

  public abstract boolean onTouchEvent(MotionEvent paramMotionEvent);

  public abstract void setHandWriteCanvas(IHandWrite paramIHandWrite);

  public abstract void setTouchMonotor(HandWriteMonitor paramHandWriteMonitor);

  public static abstract interface IGetBitmap
  {
    public abstract void onBitmapObtained(Bitmap paramBitmap);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.IWrite
 * JD-Core Version:    0.5.4
 */