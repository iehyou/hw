package com.youdao.note.ui.skitch.handwrite;

public class PointTracker
{
  public long time;
  public double width;
  public float x;
  public float y;

  public PointTracker()
  {
  }

  public PointTracker(float paramFloat1, float paramFloat2)
  {
    this.x = paramFloat1;
    this.y = paramFloat2;
  }

  public void set(float paramFloat1, float paramFloat2, double paramDouble)
  {
    this.x = paramFloat1;
    this.y = paramFloat2;
    this.width = paramDouble;
  }

  public void set(PointTracker paramPointTracker)
  {
    this.x = paramPointTracker.x;
    this.y = paramPointTracker.y;
    this.width = paramPointTracker.width;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.PointTracker
 * JD-Core Version:    0.5.4
 */