package com.youdao.note.ui.skitch.handwrite;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

public class HandWriteMonitor
{
  private static final long DEFAULT_DELAY_TIME = 800L;
  private static final int MESSAGE_FINISH_GETCHARACTER = 2;
  private static final int MESSAGE_FINISH_WRITE = 1;
  private Handler handler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        return;
      case 1:
        HandWriteMonitor.this.innerFinishWrite();
        return;
      case 2:
      }
      HandWriteMonitor.this.innerFinishGetCharacter();
    }
  };
  private IWrite.IGetBitmap mBitmapGetter = new IWrite.IGetBitmap()
  {
    public void onBitmapObtained(Bitmap paramBitmap)
    {
      if ((paramBitmap == null) || (paramBitmap.isRecycled()))
        return;
      HandWriteMonitor.this.mCanvas.addCharacter(paramBitmap);
    }
  };
  private IHandWrite mCanvas = null;
  private long mDelayTime = 800L;
  private boolean mWriteStarted = false;
  private IWrite mWriter = null;

  public HandWriteMonitor(IHandWrite paramIHandWrite, IWrite paramIWrite)
  {
    this.mCanvas = paramIHandWrite;
    this.mWriter = paramIWrite;
  }

  private void innerFinishGetCharacter()
  {
    this.mWriteStarted = false;
  }

  private void innerFinishWrite()
  {
    this.mWriter.getCharacter(this.mBitmapGetter);
    this.mWriter.onFinishWrite();
  }

  public void finishGetCharacter()
  {
    this.handler.sendEmptyMessage(2);
  }

  public void finishWrite()
  {
    this.handler.removeMessages(1);
    innerFinishWrite();
  }

  public void setDelayTime(long paramLong)
  {
    this.mDelayTime = paramLong;
  }

  public void setHandWriteCanvas(IHandWrite paramIHandWrite)
  {
    this.mCanvas = paramIHandWrite;
  }

  public void touch()
  {
    if (!this.mWriteStarted)
    {
      this.mWriteStarted = true;
      this.mWriter.onStartWrite();
    }
    this.handler.removeMessages(1);
    this.handler.sendEmptyMessageDelayed(1, this.mDelayTime);
  }

  public boolean writeStarted()
  {
    return this.mWriteStarted;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.HandWriteMonitor
 * JD-Core Version:    0.5.4
 */