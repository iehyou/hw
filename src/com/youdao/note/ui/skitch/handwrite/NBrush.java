package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import com.youdao.note.utils.L;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class NBrush
{
  private float[] newTexture = new float[this.texture.length];
  float[] newVertics = new float[this.onScreenVertices.length];
  private FloatBuffer offScreenVertexBuffer;
  private float[] offScreenVertices = { 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F };
  private FloatBuffer onScreenVertexBuffer;
  private float[] onScreenVertices = { 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, 0.0F, 1.0F, 0.0F };
  private float[] texture = { 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F };
  private FloatBuffer textureBuffer;
  private int[] textures = new int[2];

  public NBrush()
  {
    ByteBuffer localByteBuffer1 = ByteBuffer.allocateDirect(4 * this.onScreenVertices.length);
    localByteBuffer1.order(ByteOrder.nativeOrder());
    this.onScreenVertexBuffer = localByteBuffer1.asFloatBuffer();
    this.onScreenVertexBuffer.put(this.onScreenVertices);
    this.onScreenVertexBuffer.position(0);
    ByteBuffer localByteBuffer2 = ByteBuffer.allocateDirect(4 * this.offScreenVertices.length);
    localByteBuffer2.order(ByteOrder.nativeOrder());
    this.offScreenVertexBuffer = localByteBuffer2.asFloatBuffer();
    this.offScreenVertexBuffer.put(this.offScreenVertices);
    this.offScreenVertexBuffer.position(0);
    ByteBuffer localByteBuffer3 = ByteBuffer.allocateDirect(4 * this.texture.length);
    localByteBuffer3.order(ByteOrder.nativeOrder());
    this.textureBuffer = localByteBuffer3.asFloatBuffer();
    this.textureBuffer.put(this.texture);
    this.textureBuffer.position(0);
  }

  private void buildMipmap(int paramInt, GL10 paramGL10, Bitmap paramBitmap)
  {
    int i = 0;
    int j = paramBitmap.getHeight();
    int k = paramBitmap.getWidth();
    paramGL10.glBindTexture(3553, this.textures[paramInt]);
    while (true)
    {
      if ((j >= 1) && (k >= 1))
      {
        GLUtils.texImage2D(3553, i, paramBitmap, 0);
        if ((j != 1) && (k != 1))
          break label65;
      }
      return;
      label65: ++i;
      j /= 2;
      k /= 2;
      Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, k, j, true);
      paramBitmap.recycle();
      paramBitmap = localBitmap;
    }
  }

  private void loadTextureFromResource(int paramInt1, int paramInt2, GL10 paramGL10, Context paramContext, boolean paramBoolean)
  {
    InputStream localInputStream = paramContext.getResources().openRawResource(paramInt2);
    Bitmap localBitmap;
    try
    {
      localBitmap = BitmapFactory.decodeStream(localInputStream);
    }
    finally
    {
      try
      {
        localInputStream.close();
        L.d(this, "bitmap size is " + localBitmap.getWidth() + " * " + localBitmap.getHeight());
        paramGL10.glBindTexture(3553, this.textures[paramInt1]);
        if (paramBoolean)
        {
          checkGLError(paramGL10);
          paramGL10.glTexParameterf(3553, 10241, 9987.0F);
          checkGLError(paramGL10);
          paramGL10.glTexParameterf(3553, 10240, 9729.0F);
          checkGLError(paramGL10);
          buildMipmap(paramInt1, paramGL10, localBitmap);
          label134: localBitmap.recycle();
          return;
          label147: localObject = finally;
        }
      }
      catch (IOException localIOException2)
      {
        try
        {
          localInputStream.close();
          throw localObject;
          paramGL10.glTexParameterf(3553, 10241, 9729.0F);
          paramGL10.glTexParameterf(3553, 10240, 9729.0F);
          GLUtils.texImage2D(3553, 0, localBitmap, 0);
          break label134:
          localIOException2 = localIOException2;
        }
        catch (IOException localIOException1)
        {
          break label147:
        }
      }
    }
  }

  void checkGLError(GL paramGL)
  {
    int i = ((GL10)paramGL).glGetError();
    if (i == 0)
      return;
    L.d(this, "GLError 0x" + Integer.toHexString(i));
    throw new RuntimeException("GLError 0x" + Integer.toHexString(i));
  }

  public void draw(GL10 paramGL10, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5)
  {
    int i = 0;
    if (i < this.offScreenVertices.length)
    {
      label3: this.newVertics[i] = (paramFloat3 * this.offScreenVertices[i]);
      if (i % 2 == 0)
      {
        float[] arrayOfFloat2 = this.newVertics;
        arrayOfFloat2[i] += paramFloat1 - paramFloat3 / 2.0F;
      }
      while (true)
      {
        ++i;
        break label3:
        float[] arrayOfFloat1 = this.newVertics;
        arrayOfFloat1[i] += paramFloat2 - paramFloat3 / 2.0F;
      }
    }
    this.offScreenVertexBuffer.put(this.newVertics);
    this.offScreenVertexBuffer.position(0);
    this.textureBuffer.put(this.texture);
    this.textureBuffer.position(0);
    paramGL10.glVertexPointer(2, 5126, 0, this.offScreenVertexBuffer);
    paramGL10.glTexCoordPointer(2, 5126, 0, this.textureBuffer);
    paramGL10.glColor4f(0.0F, 0.0F, 0.0F, paramFloat4);
    paramGL10.glBindTexture(3553, this.textures[0]);
    paramGL10.glTexParameterf(3553, 10241, 9729.0F);
    paramGL10.glTexParameterf(3553, 10240, 9729.0F);
    paramGL10.glTexEnvx(3553, 8704, 3042);
    paramGL10.glDrawArrays(5, 0, 4);
  }

  public void drawOnScreen(GL10 paramGL10, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    L.d(this, "Draw on Screen called.");
    int i = 0;
    if (i < this.onScreenVertices.length)
    {
      label9: this.newVertics[i] = this.onScreenVertices[i];
      if (i % 2 == 0)
      {
        float[] arrayOfFloat4 = this.newVertics;
        arrayOfFloat4[i] *= paramInt2;
      }
      while (true)
      {
        ++i;
        break label9:
        float[] arrayOfFloat3 = this.newVertics;
        arrayOfFloat3[i] *= paramInt3;
      }
    }
    this.onScreenVertexBuffer.put(this.newVertics);
    this.onScreenVertexBuffer.position(0);
    float f1 = (float)(1.0D * paramInt2 / paramInt4);
    float f2 = (float)(1.0D * paramInt3 / paramInt5);
    int j = 0;
    if (j < this.texture.length)
    {
      label135: this.newTexture[j] = this.texture[j];
      if (j % 2 == 0)
      {
        float[] arrayOfFloat2 = this.newTexture;
        arrayOfFloat2[j] = (f1 * arrayOfFloat2[j]);
      }
      while (true)
      {
        ++j;
        break label135:
        float[] arrayOfFloat1 = this.newTexture;
        arrayOfFloat1[j] = (f2 * arrayOfFloat1[j]);
      }
    }
    this.textureBuffer.put(this.newTexture);
    this.textureBuffer.position(0);
    paramGL10.glMatrixMode(5888);
    paramGL10.glLoadIdentity();
    paramGL10.glColor4f(0.0F, 0.0F, 0.0F, 1.0F);
    paramGL10.glActiveTexture(33984);
    paramGL10.glBindTexture(3553, paramInt1);
    paramGL10.glTexEnvx(3553, 8704, 7681);
    paramGL10.glVertexPointer(2, 5126, 0, this.onScreenVertexBuffer);
    paramGL10.glTexCoordPointer(2, 5126, 0, this.textureBuffer);
    paramGL10.glDrawArrays(5, 0, 4);
    L.d(this, "Draw on Screen ended.");
  }

  public void loadGLTexture(GL10 paramGL10, Context paramContext)
  {
    paramGL10.glGenTextures(1, this.textures, 0);
    loadTextureFromResource(0, 2130837736, paramGL10, paramContext, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.NBrush
 * JD-Core Version:    0.5.4
 */