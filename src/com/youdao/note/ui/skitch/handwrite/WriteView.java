package com.youdao.note.ui.skitch.handwrite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.youdao.note.activity.resource.HandwritingActivity;
import com.youdao.note.ui.skitch.AbstractCanvasView;
import com.youdao.note.ui.skitch.SkitchConsts.HandWrite;
import com.youdao.note.ui.skitch.trace.BaseTraceManager;
import com.youdao.note.ui.skitch.trace.ITrace;
import com.youdao.note.ui.skitch.trace.ITraceManager;
import com.youdao.note.ui.skitch.trace.PenTrace;

public class WriteView extends AbstractCanvasView
  implements IWrite, SkitchConsts.HandWrite
{
  private HandwritingActivity activity;
  private final Canvas mCanvas = new Canvas();
  private IHandWrite mHandWriteCanvas;
  private HandWriteMonitor mMonitor;
  private ITraceManager traceManager;

  public WriteView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }

  public WriteView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public WriteView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }

  protected ITrace createTraceTool()
  {
    PenTrace localPenTrace = new PenTrace();
    localPenTrace.setWidth(this.mHandWriteCanvas.getPaintWidth());
    localPenTrace.setColor(-16777216);
    return localPenTrace;
  }

  public void finishWrite()
  {
    if (!this.mMonitor.writeStarted())
      return;
    this.mMonitor.finishWrite();
  }

  public void getCharacter(IWrite.IGetBitmap paramIGetBitmap)
  {
    if (!this.traceManager.isEmpty())
    {
      float f = (float)(1.0D * WORD_HEIGHT / getHeight());
      Bitmap localBitmap = Bitmap.createBitmap(getWidth() * WORD_HEIGHT / getHeight(), WORD_HEIGHT, Bitmap.Config.ARGB_8888);
      this.mCanvas.setBitmap(localBitmap);
      this.traceManager.drawTraces(this.mCanvas, f);
      int i = -5 + (int)(f * this.traceManager.getLeftMost());
      int j = 5 + (int)(f * this.traceManager.getRightMost());
      if (i < 0)
        i = 0;
      if (j > localBitmap.getWidth())
        j = localBitmap.getWidth();
      paramIGetBitmap.onBitmapObtained(Bitmap.createBitmap(localBitmap, i, 0, j - i, localBitmap.getHeight()));
    }
    super.clearScreen();
  }

  protected ITraceManager getTraceManager()
  {
    return this.traceManager;
  }

  public void init(float paramFloat, int paramInt1, int paramInt2)
  {
  }

  public void init(Context paramContext)
  {
    this.traceManager = new BaseTraceManager();
    this.activity = ((HandwritingActivity)paramContext);
  }

  public void onFinishWrite()
  {
  }

  public void onStartWrite()
  {
    setBackgroundColor(-1);
    this.activity.findViewById(2131165363).setVisibility(0);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.mMonitor != null)
      this.mMonitor.touch();
    return super.onTouchEvent(paramMotionEvent);
  }

  public void setHandWriteCanvas(IHandWrite paramIHandWrite)
  {
    this.mHandWriteCanvas = paramIHandWrite;
  }

  public void setTouchMonotor(HandWriteMonitor paramHandWriteMonitor)
  {
    this.mMonitor = paramHandWriteMonitor;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.handwrite.WriteView
 * JD-Core Version:    0.5.4
 */