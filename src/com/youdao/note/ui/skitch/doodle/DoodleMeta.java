package com.youdao.note.ui.skitch.doodle;

import com.youdao.note.YNoteApplication;
import com.youdao.note.ui.skitch.ISkitchMeta;
import com.youdao.note.ui.skitch.SkitchConsts.Doodle;
import com.youdao.note.ui.skitch.trace.BaseTraceManager;
import com.youdao.note.ui.skitch.trace.ITraceManager;

public class DoodleMeta
  implements ISkitchMeta, SkitchConsts.Doodle
{
  private boolean isNew = true;
  private int paintColor = DEFAULT_PAINT_COLOR;
  private float paintwidth = YNoteApplication.getInstance().getDoodlePaintWidth();
  private ITraceManager traceManager = new BaseTraceManager();

  public DoodleMeta copy()
  {
    DoodleMeta localDoodleMeta = new DoodleMeta();
    localDoodleMeta.isNew = this.isNew;
    localDoodleMeta.paintColor = this.paintColor;
    localDoodleMeta.paintwidth = this.paintwidth;
    localDoodleMeta.traceManager = this.traceManager.copy();
    return localDoodleMeta;
  }

  public int getPaintColor()
  {
    return this.paintColor;
  }

  public float getPaintwidth()
  {
    return this.paintwidth;
  }

  public ITraceManager getTraceManager()
  {
    return this.traceManager;
  }

  public boolean isEmpty()
  {
    return (this.traceManager == null) || (this.traceManager.isEmpty());
  }

  public boolean isNew()
  {
    return this.isNew;
  }

  public void onDestroy()
  {
    if (this.traceManager == null)
      return;
    this.traceManager.clear();
  }

  public void setIsNew(boolean paramBoolean)
  {
    this.isNew = paramBoolean;
  }

  public void setPaintColor(int paramInt)
  {
    this.paintColor = paramInt;
  }

  public void setPaintwidth(float paramFloat)
  {
    YNoteApplication.getInstance().setDoodlePaintWidth(paramFloat);
    this.paintwidth = paramFloat;
  }

  public void setTraceManager(ITraceManager paramITraceManager)
  {
    this.traceManager = paramITraceManager;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.doodle.DoodleMeta
 * JD-Core Version:    0.5.4
 */