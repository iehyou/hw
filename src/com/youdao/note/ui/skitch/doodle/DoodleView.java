package com.youdao.note.ui.skitch.doodle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.youdao.note.ui.skitch.AbstractCanvasView;
import com.youdao.note.ui.skitch.ISkitchMeta;
import com.youdao.note.ui.skitch.SkitchConsts.Doodle;
import com.youdao.note.ui.skitch.trace.ITrace;
import com.youdao.note.ui.skitch.trace.ITraceManager;
import com.youdao.note.ui.skitch.trace.PenTrace;

public class DoodleView extends AbstractCanvasView
  implements IDoodle, SkitchConsts.Doodle
{
  private DoodleMeta doodleMeta;

  public DoodleView(Context paramContext)
  {
    super(paramContext);
  }

  public DoodleView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public DoodleView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected ITrace createTraceTool()
  {
    PenTrace localPenTrace = new PenTrace();
    localPenTrace.setWidth(this.doodleMeta.getPaintwidth());
    localPenTrace.setColor(this.doodleMeta.getPaintColor());
    return localPenTrace;
  }

  public Bitmap getBitmap()
  {
    Bitmap localBitmap1 = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
    localBitmap1.eraseColor(-1);
    Canvas localCanvas = new Canvas(localBitmap1);
    Bitmap localBitmap2 = BitmapFactory.decodeResource(getResources(), 2130837574);
    localCanvas.drawBitmap(localBitmap2, new Rect(0, 0, localBitmap2.getWidth(), localBitmap2.getHeight()), new Rect(0, 0, localBitmap1.getWidth(), localBitmap1.getHeight()), null);
    draw(localCanvas);
    return localBitmap1;
  }

  public int getPaintColor()
  {
    return this.doodleMeta.getPaintColor();
  }

  public float getPaintWidth()
  {
    return this.doodleMeta.getPaintwidth();
  }

  public float getPaintWidthRatio()
  {
    return getPaintWidth() / (MAX_PAINT_WIDTH - MIN_PAINT_WIDTH);
  }

  protected ITraceManager getTraceManager()
  {
    return this.doodleMeta.getTraceManager();
  }

  public void initSkitchMeta(ISkitchMeta paramISkitchMeta)
  {
    this.doodleMeta = ((DoodleMeta)paramISkitchMeta);
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void setPaintColor(int paramInt)
  {
    this.doodleMeta.setPaintColor(paramInt);
  }

  public void setPaintWidth(float paramFloat)
  {
    this.doodleMeta.setPaintwidth(paramFloat);
  }

  public void trash()
  {
    getTraceManager().clear();
    invalidate();
  }

  public void undo()
  {
    getTraceManager().removeLastTrace();
    invalidate();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.doodle.DoodleView
 * JD-Core Version:    0.5.4
 */