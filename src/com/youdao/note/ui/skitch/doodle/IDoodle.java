package com.youdao.note.ui.skitch.doodle;

import com.youdao.note.ui.skitch.ISkitch;

public abstract interface IDoodle extends ISkitch
{
  public abstract int getPaintColor();

  public abstract void setPaintColor(int paramInt);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.ui.skitch.doodle.IDoodle
 * JD-Core Version:    0.5.4
 */