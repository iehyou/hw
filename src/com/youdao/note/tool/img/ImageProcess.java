package com.youdao.note.tool.img;

import [I;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PointF;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.core.Point;

public class ImageProcess
{
  private static boolean loadLibSuccess = true;

  static
  {
    try
    {
      System.loadLibrary("ynote_lib_img");
      return;
    }
    catch (UnsatisfiedLinkError localUnsatisfiedLinkError)
    {
      loadLibSuccess = false;
    }
  }

  public static Mat BitmapToMat(Bitmap paramBitmap)
  {
    return new Mat(nBitmapToMat(paramBitmap));
  }

  public static boolean MatToBitmap(Mat paramMat, Bitmap paramBitmap)
  {
    return nMatToBitmap(paramMat.nativeObj, paramBitmap);
  }

  public static native void balancing(long paramLong1, long paramLong2, double paramDouble, boolean paramBoolean);

  public static native void blackEnhancing(long paramLong1, long paramLong2, double paramDouble, boolean paramBoolean);

  public static boolean coarseEquals(PointF paramPointF1, PointF paramPointF2)
  {
    return getDistance(paramPointF1, paramPointF2) < 0.5F;
  }

  public static boolean containsInfinitePoint(PointF[] paramArrayOfPointF)
  {
    for (int i = 0; i < paramArrayOfPointF.length; ++i)
      if ((paramArrayOfPointF[i].x == 3.4028235E+38F) || (paramArrayOfPointF[i].y == 3.4028235E+38F))
        return true;
    return false;
  }

  public static List<PointF> convertOpenCVPoint(List<Point> paramList)
  {
    if (paramList == null)
    {
      localArrayList = null;
      return localArrayList;
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (localIterator.hasNext());
      Point localPoint = (Point)localIterator.next();
      localArrayList.add(new PointF((float)localPoint.x, (float)localPoint.y));
    }
  }

  public static void copyPointF(PointF paramPointF1, PointF paramPointF2)
  {
    if ((paramPointF1 == null) || (paramPointF2 == null))
      return;
    paramPointF2.x = paramPointF1.x;
    paramPointF2.y = paramPointF1.y;
  }

  public static float crossProduct(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3)
  {
    return (paramPointF3.y - paramPointF1.y) * (paramPointF2.x - paramPointF1.x) - (paramPointF3.x - paramPointF1.x) * (paramPointF2.y - paramPointF1.y);
  }

  public static native void edgeEnhancing(long paramLong1, long paramLong2, double paramDouble, boolean paramBoolean);

  public static native void getAnimFrame(long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt1, int paramInt2);

  public static native int[] getDestImageSize(long paramLong1, long paramLong2);

  public static float getDistance(PointF paramPointF1, PointF paramPointF2)
  {
    return (float)Math.sqrt(Math.pow(paramPointF2.x - paramPointF1.x, 2.0D) + Math.pow(paramPointF2.y - paramPointF1.y, 2.0D));
  }

  private static PointF getLineIntersectionPoint(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3, PointF paramPointF4)
  {
    float f1 = paramPointF2.x - paramPointF1.x;
    float f2 = paramPointF2.y - paramPointF1.y;
    float f3 = paramPointF4.x - paramPointF3.x;
    float f4 = paramPointF4.y - paramPointF3.y;
    float f5 = paramPointF3.x - paramPointF1.x;
    float f6 = ((paramPointF3.y - paramPointF1.y) * (f1 * f3) + f2 * f3 * paramPointF1.x - f1 * f4 * paramPointF3.x) / (f2 * f3 - f1 * f4);
    if (Float.isNaN(f6))
      f6 = 3.4028235E+38F;
    float f7 = (f5 * (f2 * f4) + f1 * f4 * paramPointF1.y - f2 * f3 * paramPointF3.y) / (f1 * f4 - f2 * f3);
    if (Float.isNaN(f7))
      f7 = 3.4028235E+38F;
    return new PointF(f6, f7);
  }

  public static PointF getLineSegmentIntersectionPoint(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3, PointF paramPointF4)
  {
    PointF localPointF = getLineIntersectionPoint(paramPointF1, paramPointF2, paramPointF3, paramPointF4);
    if ((localPointF.x == 3.4028235E+38F) || (localPointF.y == 3.4028235E+38F))
      localPointF = null;
    do
    {
      return localPointF;
      if ((localPointF.x - paramPointF1.x) * (paramPointF2.x - localPointF.x) + (localPointF.y - paramPointF1.y) * (paramPointF2.y - localPointF.y) < 0.0F)
        return null;
    }
    while ((localPointF.x - paramPointF3.x) * (paramPointF4.x - localPointF.x) + (localPointF.y - paramPointF3.y) * (paramPointF4.y - localPointF.y) >= 0.0F);
    return null;
  }

  private static PointF getMidPoint(PointF paramPointF1, PointF paramPointF2)
  {
    return new PointF((paramPointF1.x + paramPointF2.x) / 2.0F, (paramPointF1.y + paramPointF2.y) / 2.0F);
  }

  public static boolean isClockwise(PointF[] paramArrayOfPointF)
  {
    if (paramArrayOfPointF == null)
      return false;
    if (paramArrayOfPointF.length < 3)
      return true;
    float f1 = 0.0F;
    int i = 0;
    if (i < paramArrayOfPointF.length)
    {
      label18: PointF localPointF1 = paramArrayOfPointF[i];
      PointF localPointF2 = paramArrayOfPointF[((i + 1) % paramArrayOfPointF.length)];
      PointF localPointF3 = paramArrayOfPointF[((i + 2) % paramArrayOfPointF.length)];
      float f2 = getDistance(localPointF1, localPointF2);
      float f3 = getDistance(localPointF2, localPointF3);
      float f4 = (float)Math.acos(((localPointF2.x - localPointF1.x) * (localPointF3.x - localPointF2.x) + (localPointF2.y - localPointF1.y) * (localPointF3.y - localPointF2.y)) / (f2 * f3));
      int j;
      if ((localPointF2.x - localPointF1.x) * (localPointF3.y - localPointF2.y) - (localPointF2.y - localPointF1.y) * (localPointF3.x - localPointF2.x) > 0.0F)
      {
        j = 1;
        label176: if (j == 0)
          break label198;
      }
      while (true)
      {
        f1 += f4;
        ++i;
        break label18:
        j = 0;
        break label176:
        label198: f4 = -f4;
      }
    }
    return f1 > 0.0F;
  }

  public static boolean isInRectangle(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3)
  {
    int i = 1;
    PointF[] arrayOfPointF = new PointF[3];
    arrayOfPointF[0] = paramPointF1;
    arrayOfPointF[i] = paramPointF2;
    arrayOfPointF[2] = paramPointF3;
    if (containsInfinitePoint(arrayOfPointF))
      return false;
    if (((paramPointF3.x - paramPointF1.x) * (paramPointF3.x - paramPointF2.x) <= 0.0F) && ((paramPointF3.y - paramPointF1.y) * (paramPointF3.y - paramPointF2.y) <= 0.0F));
    while (true)
    {
      return i;
      i = 0;
    }
  }

  public static boolean isInSameLine(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3)
  {
    return crossProduct(paramPointF1, paramPointF2, paramPointF3) < 0.5F;
  }

  public static boolean isIntersect(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3, PointF paramPointF4)
  {
    if (containsInfinitePoint(new PointF[] { paramPointF1, paramPointF2, paramPointF3, paramPointF4 }));
    do
      return false;
    while ((((crossProduct(paramPointF1, paramPointF2, paramPointF3) * crossProduct(paramPointF1, paramPointF2, paramPointF4) >= 0.0F) || (crossProduct(paramPointF3, paramPointF4, paramPointF1) * crossProduct(paramPointF3, paramPointF4, paramPointF2) >= 0.0F))) && (((crossProduct(paramPointF1, paramPointF2, paramPointF3) * crossProduct(paramPointF1, paramPointF2, paramPointF4) != 0.0F) || (crossProduct(paramPointF3, paramPointF4, paramPointF1) * crossProduct(paramPointF3, paramPointF4, paramPointF2) >= 0.0F))) && (((crossProduct(paramPointF1, paramPointF2, paramPointF3) * crossProduct(paramPointF1, paramPointF2, paramPointF4) >= 0.0F) || (crossProduct(paramPointF3, paramPointF4, paramPointF1) * crossProduct(paramPointF3, paramPointF4, paramPointF2) != 0.0F))));
    return true;
  }

  public static boolean isLoadImageLibSuccess()
  {
    return loadLibSuccess;
  }

  public static boolean isValidPointsOrder(PointF paramPointF1, PointF paramPointF2, PointF paramPointF3, PointF paramPointF4)
  {
    if (containsInfinitePoint(new PointF[] { paramPointF1, paramPointF2, paramPointF3, paramPointF4 }));
    PointF localPointF3;
    PointF localPointF4;
    do
    {
      PointF localPointF1;
      PointF localPointF2;
      do
      {
        do
          return false;
        while ((isIntersect(paramPointF1, paramPointF2, paramPointF3, paramPointF4)) || (isIntersect(paramPointF1, paramPointF4, paramPointF2, paramPointF3)));
        localPointF1 = getMidPoint(paramPointF1, paramPointF4);
        localPointF2 = getMidPoint(paramPointF2, paramPointF3);
      }
      while ((crossProduct(localPointF1, localPointF2, paramPointF1) * crossProduct(localPointF1, localPointF2, paramPointF2) < 0.0F) || (crossProduct(localPointF1, localPointF2, paramPointF4) * crossProduct(localPointF1, localPointF2, paramPointF3) < 0.0F));
      localPointF3 = getMidPoint(paramPointF1, paramPointF2);
      localPointF4 = getMidPoint(paramPointF4, paramPointF3);
    }
    while ((crossProduct(localPointF3, localPointF4, paramPointF1) * crossProduct(localPointF3, localPointF4, paramPointF4) < 0.0F) || (crossProduct(localPointF3, localPointF4, paramPointF2) * crossProduct(localPointF3, localPointF4, paramPointF3) < 0.0F));
    return true;
  }

  private static native long nBitmapToMat(Bitmap paramBitmap);

  private static native boolean nMatToBitmap(long paramLong, Bitmap paramBitmap);

  public static native void quadrangleLocation(long paramLong1, long paramLong2);

  public static native void rectify(long paramLong1, long paramLong2, long paramLong3, int[] paramArrayOfInt);

  public static PointF rotatePoint(PointF paramPointF1, PointF paramPointF2, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    float f1 = paramPointF1.x - paramPointF2.x;
    float f2 = paramPointF1.y - paramPointF2.y;
    float f3;
    float f4;
    if (paramBoolean)
    {
      f3 = -f2;
      f4 = f1;
    }
    while (true)
    {
      return new PointF(f3 * paramFloat2 / paramFloat1 + paramPointF2.x, f4 * paramFloat2 / paramFloat1 + paramPointF2.y);
      f3 = f2;
      f4 = -f1;
    }
  }

  public static native void scale(long paramLong1, long paramLong2);

  public static void scale(Bitmap paramBitmap1, Bitmap paramBitmap2)
  {
    Mat localMat1 = BitmapToMat(paramBitmap1);
    Mat localMat2 = BitmapToMat(paramBitmap2);
    scale(localMat1.nativeObj, localMat2.nativeObj);
    MatToBitmap(localMat2, paramBitmap2);
  }

  private static native void smooth(long paramLong1, long paramLong2);

  public static void smooth(Bitmap paramBitmap1, Bitmap paramBitmap2)
  {
    Mat localMat1 = BitmapToMat(paramBitmap1);
    Mat localMat2 = BitmapToMat(paramBitmap2);
    smooth(localMat1.nativeObj, localMat2.nativeObj);
    MatToBitmap(localMat2, paramBitmap2);
  }

  public static void sortVertices(List<PointF> paramList)
  {
    if ((paramList == null) || (paramList.size() != 4))
      return;
    int[][] arrayOfInt = { { 0, 1, 2, 3 }, { 0, 1, 3, 2 }, { 0, 2, 1, 3 }, { 0, 2, 3, 1 }, { 0, 3, 1, 2 }, { 0, 3, 2, 1 } };
    for (int i = 0; ; ++i)
    {
      if (i < arrayOfInt.length);
      [I local[I = arrayOfInt[i];
      if (!isValidPointsOrder((PointF)paramList.get(local[I[0]), (PointF)paramList.get(local[I[1]), (PointF)paramList.get(local[I[2]), (PointF)paramList.get(local[I[3])))
        continue;
      PointF[] arrayOfPointF1 = new PointF[4];
      arrayOfPointF1[0] = ((PointF)paramList.get(local[I[0]));
      arrayOfPointF1[1] = ((PointF)paramList.get(local[I[1]));
      arrayOfPointF1[2] = ((PointF)paramList.get(local[I[2]));
      arrayOfPointF1[3] = ((PointF)paramList.get(local[I[3]));
      if (!isClockwise(arrayOfPointF1))
        continue;
      PointF[] arrayOfPointF2 = new PointF[4];
      for (int j = 0; j < 4; ++j)
        arrayOfPointF2[j] = ((PointF)paramList.get(local[I[j]));
      for (int k = 0; ; ++k)
      {
        if (k < 4);
        paramList.set(k, arrayOfPointF2[k]);
      }
    }
  }

  public static Bitmap toGray(Bitmap paramBitmap)
  {
    int i = paramBitmap.getHeight();
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), i, Bitmap.Config.RGB_565);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    ColorMatrix localColorMatrix = new ColorMatrix();
    localColorMatrix.setSaturation(0.0F);
    localPaint.setColorFilter(new ColorMatrixColorFilter(localColorMatrix));
    localCanvas.drawBitmap(paramBitmap, 0.0F, 0.0F, localPaint);
    return localBitmap;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.tool.img.ImageProcess
 * JD-Core Version:    0.5.4
 */