package com.youdao.note.activity;

import android.content.Intent;
import android.os.Bundle;
import com.youdao.note.YNoteApplication;

public abstract class LockableActivity extends BaseActivity
{
  private static final String BUNDLE_LOCK_STATE = "unlocked";
  private static int sLockCount;
  private static boolean sUnlocked = false;
  private boolean mFinished = false;
  private boolean mNeedLock = false;

  static
  {
    sLockCount = 0;
  }

  public static void setUnLocked(boolean paramBoolean)
  {
    sUnlocked = paramBoolean;
  }

  protected void finishLockable()
  {
    finish();
    this.mFinished = true;
  }

  protected boolean needLock()
  {
    return this.mNeedLock;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
    case 14:
    }
    if (paramInt2 == 0)
    {
      this.mYNote.sendMainActivity(this, "com.youdao.note.action.FINISH");
      finishLockable();
      return;
    }
    sUnlocked = true;
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    sUnlocked = paramBundle.getBoolean("unlocked");
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    if ("com.youdao.note.action.FINISH".equals(getIntent().getAction()));
    do
      return;
    while ((sLockCount != 1) || (sUnlocked) || (!needLock()) || (!this.mYNote.isPinlockEnable()) || (!this.mYNote.isLogin()) || (this.mFinished));
    startActivityForResult(new Intent(this, PinlockActivity.class), 14);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putBoolean("unlocked", sUnlocked);
    super.onSaveInstanceState(paramBundle);
  }

  public void onStart()
  {
    super.onStart();
    sLockCount = 1 + sLockCount;
  }

  public void onStop()
  {
    super.onStop();
    sLockCount = -1 + sLockCount;
    if (sLockCount != 0)
      return;
    sUnlocked = false;
  }

  protected void setNeedLock(boolean paramBoolean)
  {
    this.mNeedLock = paramBoolean;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.LockableActivity
 * JD-Core Version:    0.5.4
 */