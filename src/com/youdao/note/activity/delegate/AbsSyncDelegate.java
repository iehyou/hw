package com.youdao.note.activity.delegate;

import android.app.Activity;
import android.content.res.Resources;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.RemoteErrorData;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.ResourceMissingException;
import com.youdao.note.exceptions.ServerException;
import com.youdao.note.exceptions.UnloginException;
import com.youdao.note.task.SyncManager;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import java.io.IOException;
import java.net.SocketException;

public abstract class AbsSyncDelegate extends BaseDelegate
{
  public AbsSyncDelegate(Activity paramActivity)
  {
    super(paramActivity);
  }

  protected void notifyError(BaseData paramBaseData)
  {
    Exception localException = ((RemoteErrorData)paramBaseData).getException();
    if (localException == null)
    {
      UIUtilities.showToast(this.context, 2131361842);
      return;
    }
    if (localException instanceof UnloginException)
    {
      UIUtilities.showToast(this.context, 2131361843);
      return;
    }
    if (localException instanceof ServerException)
    {
      ServerException localServerException = (ServerException)localException;
      L.d(this, "ServerError Code is " + localServerException.getResponseCode());
      if (localServerException.getErrorCode() == 210)
      {
        UIUtilities.showToast(this.context, 2131361839);
        return;
      }
      if (localServerException.getErrorCode() == 208)
      {
        UIUtilities.showToast(this.context, 2131361871);
        return;
      }
      UIUtilities.showToast(this.context, 2131361842);
      return;
    }
    if (localException instanceof ResourceMissingException)
    {
      ResourceMissingException localResourceMissingException = (ResourceMissingException)localException;
      Activity localActivity = this.context;
      String str = this.context.getResources().getString(2131361844);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = localResourceMissingException.getNoteTitle();
      UIUtilities.showToast(localActivity, String.format(str, arrayOfObject));
      return;
    }
    if (localException instanceof IOException)
    {
      if ((localException instanceof SocketException) && (this.mYNote.isUsingCMWAP()))
      {
        this.context.showDialog(117);
        return;
      }
      UIUtilities.showToast(this.context, 2131361953);
      return;
    }
    UIUtilities.showToast(this.context, 2131361951);
  }

  public void onResume()
  {
    super.onResume();
    if (this.mYNote.isLogin())
    {
      if (!this.mSyncManager.isSyncing())
        break label30;
      onSyncStatusChanged(true);
    }
    return;
    label30: onSyncStatusChanged(false);
  }

  protected abstract void onSyncStatusChanged(boolean paramBoolean);

  public void startSync()
  {
    this.mSyncManager.startSync(false);
  }

  public void syncIfNeed()
  {
    if ((!this.mYNote.isLogin()) || (this.mDataSource.getRootMeta() != null) || (this.mSyncManager.isSyncing()))
      return;
    startSync();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.delegate.AbsSyncDelegate
 * JD-Core Version:    0.5.4
 */