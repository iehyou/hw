package com.youdao.note.activity.delegate;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.UpdateCheckResult;
import com.youdao.note.task.TaskManager;
import com.youdao.note.utils.L;

public class CheckUpdateDelegate extends BaseDelegate
{
  public static final int DIALOG_CHECKUPDATE_FAILED = 4;
  public static final int DIALOG_CHECK_UPDATE = 2;
  public static final int DIALOG_NEWVERSION_FOUND = 5;
  public static final int DIALOG_NOUPDATE_FOUND = 6;
  private static final String UPDATE_RESULT = "check_update_result";
  private Activity mActivity;
  private TaskManager mTaskManager;
  private UpdateCheckResult mUpdateCheckResult;
  private YNoteApplication mYNote;

  public CheckUpdateDelegate(Activity paramActivity)
  {
    super(paramActivity);
    this.mActivity = paramActivity;
    this.mYNote = YNoteApplication.getInstance();
    this.mTaskManager = this.mYNote.getTaskManager();
  }

  public void checkUpdate()
  {
    this.mActivity.showDialog(2);
    this.mTaskManager.checkUpdate();
  }

  public Dialog createCheckUpdateFailedDialog()
  {
    return new AlertDialog.Builder(this.mActivity).setTitle(2131361834).setPositiveButton(2131361963, null).create();
  }

  public Dialog createCheckingUpdateDialog()
  {
    ProgressDialog localProgressDialog = new ProgressDialog(this.mActivity);
    localProgressDialog.setMessage(this.mActivity.getString(2131361857));
    localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramDialogInterface)
      {
        CheckUpdateDelegate.this.mTaskManager.stopAll();
      }
    });
    return localProgressDialog;
  }

  public Dialog createNewVersionFoundDialog()
  {
    if ((this.mUpdateCheckResult == null) || (!this.mUpdateCheckResult.isNewVersionFoun()))
      return null;
    L.d(this, "create new update found dialog.");
    return new AlertDialog.Builder(this.mActivity).setTitle(2131361833).setMessage(this.mUpdateCheckResult.getNewFeatures().replace(" ", "").replace("<br/>", "\n")).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        CheckUpdateDelegate.this.mYNote.sendUrl(CheckUpdateDelegate.this.mActivity, CheckUpdateDelegate.this.mUpdateCheckResult.getUpdateUrl());
        CheckUpdateDelegate.this.mActivity.removeDialog(2);
      }
    }).setNegativeButton(2131361962, null).create();
  }

  public Dialog createNoUpdateFoundDialog()
  {
    return new AlertDialog.Builder(this.mActivity).setTitle(2131361832).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        CheckUpdateDelegate.this.mActivity.removeDialog(2);
      }
    }).create();
  }

  public void onCheckUpdateResult(BaseData paramBaseData, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mUpdateCheckResult = ((UpdateCheckResult)paramBaseData);
      this.mActivity.removeDialog(2);
      if (this.mUpdateCheckResult.isNewVersionFoun())
      {
        this.mActivity.showDialog(5);
        return;
      }
      this.mActivity.showDialog(6);
      return;
    }
    this.mActivity.showDialog(4);
  }

  public void onRestoreState(Bundle paramBundle)
  {
    if (!paramBundle.containsKey("check_update_result"))
      return;
    this.mUpdateCheckResult = ((UpdateCheckResult)paramBundle.get("check_update_result"));
  }

  public void onSaveState(Bundle paramBundle)
  {
    if (this.mUpdateCheckResult == null)
      return;
    paramBundle.putSerializable("check_update_result", this.mUpdateCheckResult);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.delegate.CheckUpdateDelegate
 * JD-Core Version:    0.5.4
 */