package com.youdao.note.activity.delegate;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.LoginActivity;
import com.youdao.note.data.BaseData;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.SyncManager;
import com.youdao.note.task.TaskManager;

public class BaseDelegate
{
  protected Activity context;
  protected DataSource mDataSource;
  protected LogRecorder mLogRecorder;
  protected SyncManager mSyncManager;
  protected TaskManager mTaskManager;
  protected YNoteApplication mYNote;

  public BaseDelegate(Activity paramActivity)
  {
    this.context = paramActivity;
    this.mYNote = YNoteApplication.getInstance();
    this.mSyncManager = this.mYNote.getSyncManager();
    this.mTaskManager = this.mYNote.getTaskManager();
    this.mDataSource = this.mYNote.getDataSource();
    this.mLogRecorder = this.mYNote.getLogRecorder();
  }

  public boolean checkLogin()
  {
    if (!this.mYNote.isLogin())
    {
      sendLogin();
      return true;
    }
    return false;
  }

  protected View findViewById(int paramInt)
  {
    return this.context.findViewById(paramInt);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    return false;
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
  }

  public void onResume()
  {
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
  }

  public void sendLogin()
  {
    Intent localIntent = new Intent(this.context, LoginActivity.class);
    this.context.startActivityForResult(localIntent, 3);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.delegate.BaseDelegate
 * JD-Core Version:    0.5.4
 */