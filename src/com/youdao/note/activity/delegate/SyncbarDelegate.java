package com.youdao.note.activity.delegate;

import android.app.Activity;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.ProgressData;
import com.youdao.note.task.SyncManager;
import com.youdao.note.ui.SyncProgressBar;
import com.youdao.note.ui.SyncProgressBar.SyncAnimationListener;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;

public class SyncbarDelegate extends AbsSyncDelegate
{
  private SyncProgressBar mProgressBar = (SyncProgressBar)findViewById(2131165315);
  private MenuItem mSyncItem;
  private View mSyncView = findViewById(2131165312);
  private SyncListener syncListener;

  public SyncbarDelegate(Activity paramActivity)
  {
    super(paramActivity);
    if (this.mProgressBar == null)
      return;
    this.mProgressBar.setAnimationFinishListener(new SyncProgressBar.SyncAnimationListener(paramActivity)
    {
      public void onAnimationFinished()
      {
        UIUtilities.showToast(this.val$context, 2131361950);
      }
    });
  }

  private void disableSync()
  {
    if (this.mSyncView == null)
      return;
    this.mSyncView.setEnabled(false);
  }

  private void dismissSyncProgress(boolean paramBoolean)
  {
    if (this.mProgressBar == null)
      return;
    this.mProgressBar.finishSync(paramBoolean);
  }

  private void enableSync()
  {
    enableSyncMenuItem();
    if (this.mSyncView == null)
      return;
    this.mSyncView.setEnabled(true);
    this.mSyncView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        SyncbarDelegate.this.mLogRecorder.addIconSyncTimes();
        SyncbarDelegate.this.startSync();
      }
    });
  }

  private void enableSyncMenuItem()
  {
    if (this.mSyncItem == null)
      return;
    this.mSyncItem.setEnabled(true);
    this.mSyncItem.setIcon(2130837741);
    this.mSyncItem.setTitle(2131361936);
  }

  private void setOnSyncing()
  {
    if (this.mProgressBar == null)
      return;
    this.mProgressBar.setProgress(this.mSyncManager.getSyncProgress());
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = 0;
    if (i == 2131165364)
    {
      if (!paramMenuItem.getTitle().equals(this.context.getResources().getString(2131361936)))
        break label54;
      if (!checkLogin())
        startSync();
    }
    while (true)
    {
      j = 1;
      return j;
      label54: this.mSyncManager.stopSync();
      enableSync();
      dismissSyncProgress(false);
    }
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    super.onPrepareOptionsMenu(paramMenu);
    this.mSyncItem = paramMenu.findItem(2131165364);
    if (this.mSyncItem != null)
    {
      if (this.mYNote.isLogin())
        break label46;
      this.mSyncItem.setEnabled(false);
    }
    return;
    if (this.mSyncManager.isSyncing())
    {
      label46: this.mSyncItem.setIcon(17301560);
      this.mSyncItem.setTitle(2131361981);
      return;
    }
    enableSyncMenuItem();
  }

  public void onResume()
  {
    super.onResume();
    if (!this.mYNote.isLogin())
    {
      if (this.mSyncView != null)
        this.mSyncView.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramView)
          {
            SyncbarDelegate.this.sendLogin();
          }
        });
      if (this.mSyncItem != null)
        this.mSyncItem.setEnabled(false);
    }
    if (!this.mSyncManager.isSyncing())
      return;
    L.d(this, "set on syncing.");
    setOnSyncing();
  }

  protected void onSyncStatusChanged(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      L.d(this, "set onsyncing. ");
      disableSync();
      setOnSyncing();
    }
    do
    {
      return;
      enableSync();
    }
    while (this.mProgressBar == null);
    this.mProgressBar.setVisibility(8);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default:
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
      return;
    case 6:
      if (paramBoolean)
        dismissSyncProgress(true);
      while (true)
      {
        enableSync();
        return;
        dismissSyncProgress(false);
        notifyError(paramBaseData);
      }
    case 7:
    }
    ProgressData localProgressData = (ProgressData)paramBaseData;
    L.d(this, localProgressData.toString());
    int i = localProgressData.getProgress();
    this.mProgressBar.setProgress(i);
    L.d(this, "set progress bar to " + i);
  }

  public void setSyncListener(SyncListener paramSyncListener)
  {
    this.syncListener = paramSyncListener;
  }

  public void startSync()
  {
    if (this.syncListener != null)
      this.syncListener.beforeSync();
    disableSync();
    setOnSyncing();
    super.startSync();
  }

  public static abstract interface SyncListener
  {
    public abstract void beforeSync();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.delegate.SyncbarDelegate
 * JD-Core Version:    0.5.4
 */