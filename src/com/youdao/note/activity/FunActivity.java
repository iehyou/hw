package com.youdao.note.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FunActivity extends Activity
{
  private static final int CHAR_SIZE = 40;
  private static final int MESSAGE_NEXT = 1;
  private static final long mCheckInterval = 50L;
  private static final int[] mScale = { 20, 17, 15, 12, 10, 5, 2, 1 };
  private static final String mText = "其实这世上最恐怖的，不是伸手不见五指，而是在伸手不见五指的时候，你伸出了手，发现上面赫然长着六个指头。——《呐喊不起来》";
  private boolean mCanceled = false;
  private int mCharPerLine = 0;
  private List<TextView> mChars = new ArrayList("其实这世上最恐怖的，不是伸手不见五指，而是在伸手不见五指的时候，你伸出了手，发现上面赫然长着六个指头。——《呐喊不起来》".length());
  private ViewGroup mContainer = null;
  private int mCurrentCharIdx = 0;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if ((paramMessage.what != 1) || (FunActivity.this.mCurrentCharIdx >= "其实这世上最恐怖的，不是伸手不见五指，而是在伸手不见五指的时候，你伸出了手，发现上面赫然长着六个指头。——《呐喊不起来》".length()) || (FunActivity.this.mCanceled))
        return;
      TextView localTextView = new TextView(FunActivity.this);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
      localLayoutParams.setMargins(40 * (FunActivity.this.mCurrentCharIdx % FunActivity.this.mCharPerLine), 40 * (FunActivity.this.mCurrentCharIdx / FunActivity.this.mCharPerLine), 0, 0);
      FunActivity.this.mContainer.addView(localTextView);
      localTextView.setLayoutParams(localLayoutParams);
      localTextView.setTextSize(0, 40.0F);
      localTextView.setText("其实这世上最恐怖的，不是伸手不见五指，而是在伸手不见五指的时候，你伸出了手，发现上面赫然长着六个指头。——《呐喊不起来》".charAt(FunActivity.this.mCurrentCharIdx) + "");
      FunActivity.this.mChars.add(localTextView);
      Animation localAnimation = AnimationUtils.loadAnimation(FunActivity.this, 2130968586);
      Log.d("Char", "handled message");
      localAnimation.setAnimationListener(new Animation.AnimationListener(localTextView)
      {
        public void onAnimationEnd(Animation paramAnimation)
        {
          this.val$textView.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramView)
            {
              Animation localAnimation = AnimationUtils.loadAnimation(FunActivity.this, 2130968586);
              FunActivity.1.1.this.val$textView.startAnimation(localAnimation);
            }
          });
          Message localMessage = Message.obtain(FunActivity.this.mHandler, 1);
          FunActivity.access$008(FunActivity.this);
          try
          {
            Thread.sleep(50L);
            FunActivity.1.this.sendMessage(localMessage);
            return;
          }
          catch (InterruptedException localInterruptedException)
          {
            localInterruptedException.printStackTrace();
          }
        }

        public void onAnimationRepeat(Animation paramAnimation)
        {
        }

        public void onAnimationStart(Animation paramAnimation)
        {
        }
      });
      localTextView.startAnimation(localAnimation);
    }
  };
  private long mLastTouchTime = 0L;
  private int mWidth = 0;

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    super.onCreate(paramBundle);
    setContentView(2130903057);
    this.mWidth = getWindowManager().getDefaultDisplay().getWidth();
    this.mCharPerLine = (this.mWidth / 40);
    this.mContainer = ((ViewGroup)findViewById(2131165231));
    this.mHandler.sendMessage(Message.obtain(this.mHandler, 1));
    Log.d("Char", "message send");
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mCanceled = true;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
      this.mCanceled = true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 1)
    {
      Log.d("Char", "up");
      Iterator localIterator2 = this.mChars.iterator();
      while (true)
      {
        if (!localIterator2.hasNext())
          break label71;
        ((TextView)localIterator2.next()).setTextSize(0, 40.0F);
      }
    }
    if (System.currentTimeMillis() - this.mLastTouchTime < 50L)
      label71: return false;
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    Log.d("Char", f1 + " " + f2);
    int[] arrayOfInt = new int[2];
    Iterator localIterator1 = this.mChars.iterator();
    while (localIterator1.hasNext())
    {
      TextView localTextView = (TextView)localIterator1.next();
      localTextView.getLocationOnScreen(arrayOfInt);
      int i = (int)(Math.abs(f1 - arrayOfInt[0]) / 40.0F + Math.abs(f2 - arrayOfInt[1]) / 40.0F);
      if (i < mScale.length)
        localTextView.setTextSize(0, 40 + mScale[i]);
      localTextView.setTextSize(0, 40.0F);
    }
    this.mLastTouchTime = System.currentTimeMillis();
    return false;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.FunActivity
 * JD-Core Version:    0.5.4
 */