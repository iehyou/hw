package com.youdao.note.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;
import com.youdao.note.ui.AlphabetScrollbar;
import com.youdao.note.ui.AlphabetScrollbar.OnAlphabetListener;
import com.youdao.note.utils.UIUtilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailSelectionActivity extends LockableActivity
{
  private static final String BUNDLE_EMAILERS = "emailers";
  private static final int DIALOG_RECEIVERS_LIMIT = 1;
  private static final Pattern INDEX_PATTERN = Pattern.compile("^[a-zA-Z]+.*$");
  private static final String INDEX_SHARP = "#";
  private static final String PATTERN = "${email}<${name}>";
  private ArrayList<Emailer> emailers = new ArrayList();
  private ListView listView;
  private Toast toast;

  private void doSearch(String paramString)
  {
    String str = paramString.trim();
    if ((str.trim().length() != 0) && (this.emailers.size() > 0))
    {
      Cursor localCursor = managedQuery(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI, Uri.encode(str)), new String[] { "_id" }, null, null, null);
      ArrayList localArrayList = new ArrayList();
      if (localCursor.moveToNext())
      {
        long l = localCursor.getLong(0);
        Iterator localIterator = this.emailers.iterator();
        while (true)
        {
          if (localIterator.hasNext());
          Emailer localEmailer = (Emailer)localIterator.next();
          if (localEmailer.contactId != l)
            continue;
          localArrayList.add(localEmailer);
        }
      }
      initUI(localArrayList, true);
      return;
    }
    initUI(this.emailers, false);
  }

  private String getIndex(String paramString)
  {
    if (paramString == null)
      return "#";
    String str = paramString.trim();
    if ((str.length() == 0) || (!INDEX_PATTERN.matcher(str).matches()))
      return "#";
    return String.valueOf(str.charAt(0)).toUpperCase();
  }

  private int getSelectedCount()
  {
    int i = 0;
    Iterator localIterator = this.emailers.iterator();
    while (localIterator.hasNext())
    {
      if (!((Emailer)localIterator.next()).selected)
        continue;
      ++i;
    }
    return i;
  }

  private void hideSoftKeyboard()
  {
    ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.listView.getWindowToken(), 0);
  }

  private void initData()
  {
    Cursor localCursor = managedQuery(ContactsContract.Data.CONTENT_URI, new String[] { "_id", "data1", "display_name", "data2", "data3", "contact_id", "sort_key" }, "mimetype='vnd.android.cursor.item/email_v2'", null, "sort_key COLLATE LOCALIZED ASC");
    this.emailers.clear();
    while (localCursor.moveToNext())
    {
      Emailer localEmailer = new Emailer(null);
      Emailer.access$102(localEmailer, localCursor.getString(1));
      Emailer.access$202(localEmailer, localCursor.getString(2));
      int i = localCursor.getInt(3);
      String str = localCursor.getString(4);
      Emailer.access$302(localEmailer, ContactsContract.CommonDataKinds.Email.getTypeLabel(getResources(), i, str).toString());
      Emailer.access$402(localEmailer, localCursor.getLong(5));
      Emailer.access$502(localEmailer, getIndex(localCursor.getString(6)));
      this.emailers.add(localEmailer);
    }
  }

  private void initListener()
  {
    findViewById(2131165294).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        MailSelectionActivity.this.selectionDone();
      }
    });
    findViewById(2131165295).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        MailSelectionActivity.this.onBackPressed();
      }
    });
    AutoCompleteTextView localAutoCompleteTextView = (AutoCompleteTextView)findViewById(2131165287);
    ImageButton localImageButton = (ImageButton)findViewById(2131165288);
    localAutoCompleteTextView.addTextChangedListener(new TextWatcher(localAutoCompleteTextView, localImageButton)
    {
      public void afterTextChanged(Editable paramEditable)
      {
        if (TextUtils.isEmpty(this.val$searchBar.getText().toString()))
          UIUtilities.makeViewGone(this.val$clearButton);
        do
          return;
        while (this.val$clearButton.getVisibility() != 8);
        UIUtilities.makeViewVisiable(this.val$clearButton);
      }

      public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
      {
      }

      public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
      {
        MailSelectionActivity.this.doSearch(paramCharSequence.toString());
      }
    });
    localImageButton.setOnClickListener(new View.OnClickListener(localAutoCompleteTextView)
    {
      public void onClick(View paramView)
      {
        this.val$searchBar.setText("");
        MailSelectionActivity.this.doSearch("");
      }
    });
    ((AlphabetScrollbar)findViewById(2131165291)).setOnAlphabetListener(new AlphabetScrollbar.OnAlphabetListener()
    {
      public void onAlphabetSelected(String paramString)
      {
        MailSelectionActivity.this.hideSoftKeyboard();
        if (paramString == null)
          return;
        ((TextView)MailSelectionActivity.this.toast.getView()).setText(paramString);
        MailSelectionActivity.this.toast.show();
        MailSelectionActivity.this.scrollTo(paramString);
      }
    });
    this.listView.setOnScrollListener(new AbsListView.OnScrollListener()
    {
      public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
      {
        AlphabetScrollbar localAlphabetScrollbar = (AlphabetScrollbar)MailSelectionActivity.this.findViewById(2131165291);
        MailSelectionActivity.EmailerListAdapter localEmailerListAdapter;
        if (localAlphabetScrollbar.getVisibility() == 0)
        {
          localEmailerListAdapter = (MailSelectionActivity.EmailerListAdapter)MailSelectionActivity.this.listView.getAdapter();
          if (localEmailerListAdapter != null)
            break label43;
        }
        return;
        label43: String str = localEmailerListAdapter.getAlphabetByPosition(paramInt1);
        Message localMessage = new Message();
        localMessage.what = 1;
        localMessage.getData().putString("alphabet", str);
        localAlphabetScrollbar.handler.sendMessage(localMessage);
      }

      public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
      {
      }
    });
  }

  private void initUI(ArrayList<Emailer> paramArrayList, boolean paramBoolean)
  {
    View localView1 = findViewById(2131165289);
    AlphabetScrollbar localAlphabetScrollbar = (AlphabetScrollbar)findViewById(2131165291);
    this.listView.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
      {
        MailSelectionActivity.this.hideSoftKeyboard();
        MailSelectionActivity.this.toast.cancel();
        return false;
      }
    });
    View localView2 = findViewById(2131165292);
    if ((paramArrayList == null) || (paramArrayList.size() == 0))
    {
      UIUtilities.makeViewGone(localView1);
      UIUtilities.makeViewVisiable(localView2);
    }
    while (paramBoolean)
    {
      UIUtilities.makeViewGone(localAlphabetScrollbar);
      return;
      UIUtilities.makeViewGone(localView2);
      UIUtilities.makeViewVisiable(localView1);
      EmailerListAdapter localEmailerListAdapter = new EmailerListAdapter(paramArrayList, this, paramBoolean);
      this.listView.setAdapter(localEmailerListAdapter);
    }
    UIUtilities.makeViewVisiable(localAlphabetScrollbar);
  }

  private void scrollTo(String paramString)
  {
    int i = ((EmailerListAdapter)this.listView.getAdapter()).getPositionByAlphabet(paramString);
    if (i == -1)
      return;
    this.listView.setSelectionFromTop(i, 0);
  }

  private void selectionDone()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 1;
    int j = 0;
    Iterator localIterator = this.emailers.iterator();
    while (localIterator.hasNext())
    {
      Emailer localEmailer = (Emailer)localIterator.next();
      if (!localEmailer.selected)
        continue;
      if (i == 0)
        localStringBuilder.append(",");
      localStringBuilder.append("${email}<${name}>".replace("${email}", localEmailer.address).replace("${name}", localEmailer.name));
      ++j;
      i = 0;
    }
    if (j > 3)
    {
      showDialog(1);
      return;
    }
    Intent localIntent = new Intent();
    localIntent.putExtra("emails", localStringBuilder.toString());
    setResult(-1, localIntent);
    finish();
  }

  private void updateButtonText()
  {
    int i = getSelectedCount();
    Button localButton = (Button)findViewById(2131165294);
    if (i == 0)
    {
      localButton.setText(2131362033);
      return;
    }
    localButton.setText(getString(2131362033) + "(" + i + ")");
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setRequestedOrientation(1);
    super.onCreate(paramBundle);
    setContentView(2130903079);
    this.listView = ((ListView)findViewById(2131165290));
    this.toast = new Toast(this);
    this.toast.setView(LayoutInflater.from(this).inflate(2130903082, null));
    this.toast.setDuration(0);
    this.toast.setGravity(16, 0, 0);
    initData();
    initListener();
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    if (paramInt == 1)
      return new AlertDialog.Builder(this).setTitle(2131362023).setMessage(2131362026).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailSelectionActivity.this.removeDialog(1);
        }
      }).create();
    return super.onCreateDialog(paramInt, paramBundle);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    this.emailers = ((ArrayList)paramBundle.getSerializable("emailers"));
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    initUI(this.emailers, false);
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.emailers != null)
      paramBundle.putSerializable("emailers", this.emailers);
    super.onSaveInstanceState(paramBundle);
  }

  private static class Emailer
    implements Serializable
  {
    private static final long serialVersionUID = -6995500959811527670L;
    private String address;
    private long contactId;
    private String index;
    private String name;
    private boolean selected = false;
    private String type;
  }

  private class EmailerListAdapter extends BaseAdapter
    implements SectionIndexer, CompoundButton.OnCheckedChangeListener
  {
    private Map<String, Integer> alphabetPosMap;
    private boolean isSearch;
    private List<MailSelectionActivity.ListItem> itemList;
    protected LayoutInflater mInflater;
    private List<Pair<Integer, Integer>> sections;
    private int typeCount;

    public EmailerListAdapter(Context paramBoolean, boolean arg3)
    {
      int i;
      this.isSearch = i;
      if (i != 0);
      for (int j = 1; ; j = 2)
      {
        this.typeCount = j;
        Object localObject;
        this.mInflater = ((LayoutInflater)localObject.getSystemService("layout_inflater"));
        this.itemList = new ArrayList();
        this.sections = new ArrayList();
        this.alphabetPosMap = new HashMap();
        String str = "";
        int k = 0;
        int l = 0;
        Iterator localIterator = paramBoolean.iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            return;
          MailSelectionActivity.Emailer localEmailer = (MailSelectionActivity.Emailer)localIterator.next();
          if ((i == 0) && (!localEmailer.index.equals(str)))
          {
            str = localEmailer.index;
            MailSelectionActivity.ListItem localListItem2 = new MailSelectionActivity.ListItem(MailSelectionActivity.this, null);
            MailSelectionActivity.ListItem.access$1402(localListItem2, str);
            MailSelectionActivity.ListItem.access$1502(localListItem2, 0);
            this.itemList.add(localListItem2);
            Pair localPair = new Pair(Integer.valueOf(l), Integer.valueOf(k));
            this.sections.add(localPair);
            this.alphabetPosMap.put(str, Integer.valueOf(k));
            ++k;
            ++l;
          }
          MailSelectionActivity.ListItem localListItem1 = new MailSelectionActivity.ListItem(MailSelectionActivity.this, null);
          MailSelectionActivity.ListItem.access$1502(localListItem1, 1);
          MailSelectionActivity.ListItem.access$1602(localListItem1, localEmailer);
          this.itemList.add(localListItem1);
          ++k;
        }
      }
    }

    private void checkBoxChanged(CompoundButton paramCompoundButton, boolean paramBoolean, int paramInt)
    {
      MailSelectionActivity.Emailer localEmailer = MailSelectionActivity.ListItem.access$1600(getItem(paramInt));
      if ((paramBoolean) && (MailSelectionActivity.this.getSelectedCount() >= 3))
      {
        MailSelectionActivity.this.showDialog(1);
        paramCompoundButton.setChecked(false);
        return;
      }
      MailSelectionActivity.Emailer.access$1202(localEmailer, paramBoolean);
      MailSelectionActivity.this.updateButtonText();
      MailSelectionActivity.this.hideSoftKeyboard();
    }

    public String getAlphabetByPosition(int paramInt)
    {
      int i = getSectionForPosition(paramInt);
      Iterator localIterator = this.sections.iterator();
      Pair localPair;
      while (localIterator.hasNext())
      {
        localPair = (Pair)localIterator.next();
        if (((Integer)localPair.first).equals(Integer.valueOf(i)))
          return MailSelectionActivity.ListItem.access$1400(getItem(((Integer)localPair.second).intValue()));
      }
      return null;
    }

    public int getCount()
    {
      return this.itemList.size();
    }

    public MailSelectionActivity.ListItem getItem(int paramInt)
    {
      return (MailSelectionActivity.ListItem)this.itemList.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public int getItemViewType(int paramInt)
    {
      return MailSelectionActivity.ListItem.access$1500(getItem(paramInt));
    }

    public int getPositionByAlphabet(String paramString)
    {
      Integer localInteger = (Integer)this.alphabetPosMap.get(paramString);
      if (localInteger == null)
        return -1;
      return localInteger.intValue();
    }

    public int getPositionForSection(int paramInt)
    {
      Iterator localIterator = this.sections.iterator();
      Pair localPair;
      while (localIterator.hasNext())
      {
        localPair = (Pair)localIterator.next();
        if (((Integer)localPair.first).equals(Integer.valueOf(paramInt)))
          return ((Integer)localPair.second).intValue();
      }
      return 0;
    }

    public int getSectionForPosition(int paramInt)
    {
      int i = 0;
      Iterator localIterator = this.sections.iterator();
      while (localIterator.hasNext())
      {
        Pair localPair = (Pair)localIterator.next();
        if (paramInt < ((Integer)localPair.second).intValue())
          break;
        i = ((Integer)localPair.first).intValue();
      }
      return i;
    }

    public String[] getSections()
    {
      if (this.isSearch)
      {
        arrayOfString = new String[0];
        return arrayOfString;
      }
      String[] arrayOfString = new String[this.sections.size()];
      int i = 0;
      Iterator localIterator = this.sections.iterator();
      while (true)
      {
        if (localIterator.hasNext());
        Pair localPair = (Pair)localIterator.next();
        int j = i + 1;
        arrayOfString[i] = MailSelectionActivity.ListItem.access$1400(getItem(getPositionForSection(((Integer)localPair.first).intValue())));
        i = j;
      }
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (getItemViewType(paramInt) == 1)
      {
        if (paramView != null);
        for (View localView2 = paramView; ; localView2 = this.mInflater.inflate(2130903080, paramViewGroup, false))
        {
          CheckBox localCheckBox = (CheckBox)localView2.findViewById(2131165296);
          localCheckBox.setOnCheckedChangeListener(this);
          MailSelectionActivity.Emailer localEmailer = MailSelectionActivity.ListItem.access$1600(getItem(paramInt));
          ((TextView)localView2.findViewById(2131165298)).setText(localEmailer.name);
          ((TextView)localView2.findViewById(2131165299)).setText(localEmailer.type + ": " + localEmailer.address);
          localCheckBox.setChecked(localEmailer.selected);
          localCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(paramInt)
          {
            public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
            {
              MailSelectionActivity.EmailerListAdapter.this.checkBoxChanged(paramCompoundButton, paramBoolean, this.val$position);
            }
          });
          localView2.setOnClickListener(new View.OnClickListener(localCheckBox)
          {
            public void onClick(View paramView)
            {
              this.val$cb.performClick();
            }
          });
          return localView2;
        }
      }
      if (paramView != null);
      for (View localView1 = paramView; ; localView1 = this.mInflater.inflate(2130903081, paramViewGroup, false))
      {
        ((TextView)localView1.findViewById(2131165300)).setText(MailSelectionActivity.ListItem.access$1400(getItem(paramInt)));
        return localView1;
      }
    }

    public int getViewTypeCount()
    {
      return this.typeCount;
    }

    public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
    {
    }
  }

  private class ListItem
  {
    public static final int TYPE_EMAILER = 1;
    public static final int TYPE_SECTION;
    private MailSelectionActivity.Emailer emailer;
    private String index;
    private int type;

    private ListItem()
    {
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.MailSelectionActivity
 * JD-Core Version:    0.5.4
 */