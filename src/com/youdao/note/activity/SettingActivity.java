package com.youdao.note.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.delegate.CheckUpdateDelegate;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.UserMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.SyncManager;
import com.youdao.note.task.TaskManager;
import com.youdao.note.task.TaskManager.DataUpdateListener;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.L;
import com.youdao.note.utils.StringUtils;
import com.youdao.note.utils.UnitUtils;

public class SettingActivity extends PreferenceActivity
  implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener, Consts, TaskManager.DataUpdateListener
{
  private static final int DIALOG_CLEARING = 3;
  private static final int DIALOG_CLEAR_CONFIRM = 1;
  private static final int DIALOG_NETWORK_ERROR = 7;
  private static final int DIALOG_OPENGL2_NOT_AVAILABLE = 9;
  private static final int FANCY_HANDWRITE = 0;
  private static final int NORMAL_HANDWRITE = 1;
  private ListPreference handwritePref;
  private String lastSyncKey = null;
  private String mAutoSyncKey = null;
  private String mCheckUpdateKey = null;
  private String mClearDataKey = null;
  private DataSource mDataSource;
  private String mHandwriteModeKey = null;
  private String mImageQualityKey = null;
  private String mLoginKey;
  private String mPinlockKey;
  private String mSetPinlockKey;
  private SyncManager mSyncManager;
  private TaskManager mTaskManager;
  private int mTrigerCount = 0;
  private CheckUpdateDelegate mUpdateDelegate;
  private String mUsedSpaceKey = null;
  private YNoteApplication mYNote;
  private String userNameKey = null;
  private Preference userNamePreference = null;

  private String getSpaceSummary(UserMeta paramUserMeta)
  {
    double d = 100.0D * paramUserMeta.getUsedSpace() / paramUserMeta.getQuotaSpace();
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Double.valueOf(d);
    arrayOfObject[1] = Double.valueOf(UnitUtils.getSizeInMegaByte(paramUserMeta.getUsedSpace()));
    arrayOfObject[2] = Double.valueOf(UnitUtils.getSizeInMegaByte(paramUserMeta.getQuotaSpace()));
    return String.format("%.2f%% %.2fM/%.0fM", arrayOfObject);
  }

  private void initPreference()
  {
    findPreference(this.mCheckUpdateKey).setOnPreferenceClickListener(this);
    this.handwritePref = ((ListPreference)findPreference(this.mHandwriteModeKey));
    if (!this.mYNote.isOpengGL2Available())
      this.handwritePref.setValueIndex(1);
    updateHandwritePref();
    if (this.mYNote.isLogin())
    {
      Preference localPreference1 = findPreference(this.mClearDataKey);
      if (localPreference1 != null)
        localPreference1.setOnPreferenceClickListener(this);
      Preference localPreference2 = findPreference(this.mPinlockKey);
      Preference localPreference3 = findPreference(this.mSetPinlockKey);
      localPreference3.setOnPreferenceClickListener(this);
      localPreference2.setOnPreferenceChangeListener(this);
      localPreference3.setEnabled(this.mYNote.isPinlockEnable());
      this.userNamePreference = findPreference(this.userNameKey);
      this.userNamePreference.setSummary(this.mYNote.getUserName());
      this.userNamePreference.setOnPreferenceClickListener(this);
      UserMeta localUserMeta = this.mDataSource.getUserMeta();
      findPreference(this.lastSyncKey).setSummary(StringUtils.getPrettyTimeDetail(localUserMeta.getLastSynceTime()));
      findPreference(this.mUsedSpaceKey).setSummary(getSpaceSummary(localUserMeta));
      ListPreference localListPreference1 = (ListPreference)findPreference(this.mAutoSyncKey);
      localListPreference1.setOnPreferenceChangeListener(this);
      localListPreference1.setSummary(localListPreference1.getEntry());
      ListPreference localListPreference2 = (ListPreference)findPreference(this.mImageQualityKey);
      localListPreference2.setOnPreferenceChangeListener(this);
      localListPreference2.setSummary(localListPreference2.getEntry());
      return;
    }
    findPreference(this.mLoginKey).setOnPreferenceClickListener(this);
  }

  private void updateHandwritePref()
  {
    int i = this.handwritePref.findIndexOfValue(this.handwritePref.getValue());
    this.handwritePref.setSummary(getResources().getStringArray(2131099654)[i]);
    this.handwritePref.setOnPreferenceChangeListener(this);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 3:
    case 15:
    case 16:
      do
      {
        do
        {
          return;
          if (paramInt2 != -1)
            continue;
          finish();
          return;
        }
        while (paramInt2 != 0);
        L.d(this, "canceled.");
        return;
        if (paramInt2 == 0)
        {
          ((CheckBoxPreference)findPreference(this.mPinlockKey)).setChecked(false);
          this.mYNote.setPinlockEnable(false);
          onContentChanged();
          return;
        }
        findPreference(this.mSetPinlockKey).setEnabled(true);
        LockableActivity.setUnLocked(true);
        return;
      }
      while (paramInt2 != -1);
      LockableActivity.setUnLocked(true);
      return;
    case 17:
    }
    if (paramInt2 == -1)
    {
      this.mYNote.setPinlockEnable(false);
      findPreference(this.mSetPinlockKey).setEnabled(false);
      return;
    }
    this.mYNote.setPinlockEnable(true);
    ((CheckBoxPreference)findPreference(this.mPinlockKey)).setChecked(true);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mUpdateDelegate = new CheckUpdateDelegate(this);
    L.d(this, "OnCreate called.");
    this.mYNote = YNoteApplication.getInstance();
    this.mDataSource = this.mYNote.getDataSource();
    this.mSyncManager = this.mYNote.getSyncManager();
    this.mTaskManager = this.mYNote.getTaskManager();
    this.mTaskManager.registDataListener(this);
    Resources localResources = getResources();
    this.mCheckUpdateKey = localResources.getString(2131361804);
    this.userNameKey = localResources.getString(2131361799);
    this.lastSyncKey = localResources.getString(2131361800);
    this.mUsedSpaceKey = localResources.getString(2131361801);
    this.mAutoSyncKey = localResources.getString(2131361802);
    this.mClearDataKey = localResources.getString(2131361808);
    this.mLoginKey = localResources.getString(2131361807);
    this.mImageQualityKey = localResources.getString(2131361805);
    this.mHandwriteModeKey = localResources.getString(2131361806);
    this.mPinlockKey = localResources.getString(2131361797);
    this.mSetPinlockKey = localResources.getString(2131361798);
    if (this.mYNote.isLogin())
      addPreferencesFromResource(2131034118);
    while (true)
    {
      initPreference();
      if ("com.youdao.note.action.CHECK_UPDATE".equals(getIntent().getAction()))
      {
        getIntent().setAction(null);
        this.mUpdateDelegate.checkUpdate();
      }
      return;
      addPreferencesFromResource(2131034119);
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 8:
    default:
      return null;
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131361875).setIcon(17301543).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          SettingActivity.this.showDialog(3);
          new AsyncTask()
          {
            protected Void doInBackground(Void[] paramArrayOfVoid)
            {
              SettingActivity.this.mYNote.clearData();
              SettingActivity.this.mYNote.logOut();
              return null;
            }

            public void onPostExecute(Void paramVoid)
            {
              SettingActivity.this.mYNote.sendMainActivity(SettingActivity.this, "com.youdao.note.action.login");
            }
          }
          .execute(EmptyInstance.EMPTY_VOIDS);
        }
      }).setNegativeButton(2131361962, null).create();
    case 3:
      ProgressDialog localProgressDialog = new ProgressDialog(this);
      localProgressDialog.requestWindowFeature(1);
      localProgressDialog.setMessage(getResources().getString(2131361876));
      return localProgressDialog;
    case 7:
      return new AlertDialog.Builder(this).setTitle(2131361953).setPositiveButton(2131361963, null).create();
    case 9:
      return new AlertDialog.Builder(this).setTitle(2131361809).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          SettingActivity.this.handwritePref.setValueIndex(1);
          SettingActivity.this.updateHandwritePref();
        }
      }).create();
    case 2:
      return this.mUpdateDelegate.createCheckingUpdateDialog();
    case 4:
      return this.mUpdateDelegate.createCheckUpdateFailedDialog();
    case 5:
      return this.mUpdateDelegate.createNewVersionFoundDialog();
    case 6:
    }
    return this.mUpdateDelegate.createNoUpdateFoundDialog();
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mTaskManager.unregistDataListener(this);
  }

  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    if (this.mAutoSyncKey.equals(paramPreference.getKey()))
    {
      int k = Integer.parseInt((String)paramObject);
      this.mSyncManager.resetTime(k);
      ListPreference localListPreference2 = (ListPreference)findPreference(this.mAutoSyncKey);
      int l = localListPreference2.findIndexOfValue((String)paramObject);
      localListPreference2.setSummary(localListPreference2.getEntries()[l]);
    }
    do
    {
      return true;
      if (this.mImageQualityKey.equals(paramPreference.getKey()))
      {
        String str2 = (String)paramObject;
        ListPreference localListPreference1 = (ListPreference)findPreference(this.mImageQualityKey);
        int j = localListPreference1.findIndexOfValue(str2);
        localListPreference1.setSummary(localListPreference1.getEntries()[j]);
        return true;
      }
      if (!this.mHandwriteModeKey.equals(paramPreference.getKey()))
        continue;
      String str1 = (String)paramObject;
      int i = this.handwritePref.findIndexOfValue(str1);
      if ((i == 0) && (!this.mYNote.isOpengGL2Available()))
      {
        showDialog(9);
        return true;
      }
      this.handwritePref.setSummary(getResources().getStringArray(2131099654)[i]);
      return true;
    }
    while (!this.mPinlockKey.equals(paramPreference.getKey()));
    boolean bool = ((Boolean)paramObject).booleanValue();
    Intent localIntent = new Intent(this, PinlockActivity.class);
    if (bool)
    {
      localIntent.setAction("com.youdao.note.action.SETUP_PINLOCK");
      startActivityForResult(localIntent, 15);
      return true;
    }
    startActivityForResult(localIntent, 17);
    return true;
  }

  public boolean onPreferenceClick(Preference paramPreference)
  {
    L.d(this, "pre clicked " + paramPreference.getKey());
    if (this.mCheckUpdateKey.equals(paramPreference.getKey()))
    {
      this.mUpdateDelegate.checkUpdate();
      this.mTrigerCount = (1 + this.mTrigerCount);
      if ((this.mTrigerCount > 5) && (this.userNamePreference != null))
        this.userNamePreference.setEnabled(true);
      L.d(this, "Check update.");
      return true;
    }
    if (this.mLoginKey.equals(paramPreference.getKey()))
    {
      this.mYNote.sendMainActivity(this, "com.youdao.note.action.login");
      return true;
    }
    if (this.mClearDataKey.equals(paramPreference.getKey()))
      showDialog(1);
    while (true)
    {
      return false;
      if (this.userNameKey.equals(paramPreference.getKey()))
      {
        this.mTrigerCount = (1 + this.mTrigerCount);
        if (this.mTrigerCount <= 50)
          continue;
        startActivity(new Intent(this, FunActivity.class));
        this.userNamePreference.setEnabled(false);
        this.mTrigerCount = -100;
      }
      if (!this.mSetPinlockKey.equals(paramPreference.getKey()))
        continue;
      Intent localIntent = new Intent(this, PinlockActivity.class);
      localIntent.setAction("com.youdao.note.action.UPDATE_PINLOCK");
      startActivityForResult(localIntent, 16);
    }
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    this.mUpdateDelegate.onRestoreState(paramBundle);
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    L.d(this, "onResume called.");
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.mUpdateDelegate.onSaveState(paramBundle);
    super.onSaveInstanceState(paramBundle);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    if (paramInt != 21)
      return;
    this.mUpdateDelegate.onCheckUpdateResult(paramBaseData, paramBoolean);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.SettingActivity
 * JD-Core Version:    0.5.4
 */