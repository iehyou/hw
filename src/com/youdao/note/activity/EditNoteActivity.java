package com.youdao.note.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.youdao.note.LogRecorder;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.task.TaskManager;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;

public class EditNoteActivity extends BaseEditActivity
  implements TextView.OnEditorActionListener
{
  private static final String JS_GETBODY = "javascript:window.Htmlout.onBodyFetched(document.body.innerHTML, %s)";
  private final Handler handler = new Handler();
  private EditText mEditTextView = null;
  private View mEditView = null;
  private View saveButton = null;

  private void dismissFooterBar()
  {
    findViewById(2131165229).setVisibility(8);
  }

  private void editDone()
  {
    String str = this.mEditTextView.getText().toString().replace("\"", "\\\"").replace("\n", "\\n");
    this.mWebView.loadUrl("javascript:editDone(\"" + str + "\")");
    hideEditView();
    showFooterBar();
  }

  private void hideEditView()
  {
    this.mEditView.setVisibility(8);
    ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.mEditTextView.getWindowToken(), 0);
  }

  private void showFooterBar()
  {
    findViewById(2131165229).setVisibility(0);
  }

  protected void initControls()
  {
    super.initControls();
    this.saveButton = findViewById(2131165230);
    this.saveButton.setOnClickListener(this);
    findViewById(2131165204).setOnClickListener(this);
    findViewById(2131165205).setOnClickListener(this);
  }

  protected void loadNote(Note paramNote)
  {
    super.loadNote(paramNote);
    if (!this.loadDismissed)
      dismissDialog(201);
    if (!TextUtils.isEmpty(this.mNote.getBody()))
      return;
    this.mNote = paramNote;
    this.mWebView.loadDataWithBaseURL("file:///android_asset/", this.mNote.getEditableHtml(), "text/html", "utf-8", null);
  }

  protected void obtainNote(boolean paramBoolean)
  {
    String str = this.mTitleView.getText().toString();
    L.d(this, "modify title is " + str);
    this.mNote.getNoteMeta().setTitle(str);
    WebView localWebView = this.mWebView;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Boolean.valueOf(paramBoolean);
    localWebView.loadUrl(String.format("javascript:window.Htmlout.onBodyFetched(document.body.innerHTML, %s)", arrayOfObject));
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165204)
    {
      editDone();
      return;
    }
    if (paramView.getId() == 2131165205)
    {
      hideEditView();
      showFooterBar();
      return;
    }
    if (paramView.getId() == 2131165230)
    {
      editDone();
      obtainNote(true);
      return;
    }
    super.onClick(paramView);
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903055);
    super.onCreate(paramBundle);
    this.mEditTextView = ((EditText)findViewById(2131165234));
    this.mEditTextView.setOnEditorActionListener(this);
    L.d(this, "new webview is " + this.mWebView);
    this.mWebView.addJavascriptInterface(new MyJavaScriptInteface(), "Htmlout");
    this.mWebView.setWebViewClient(new YNoteWebViewClient(null));
    this.mWebView.requestFocus();
    this.mEditView = findViewById(2131165233);
    L.d(this, "onCreate called.");
    String str = this.mNote.getNoteBook();
    this.oriNoteBookId = str;
    this.mNoteBookId = str;
    if (Build.VERSION.SDK_INT > 7)
      return;
    UIUtilities.showToast(this, 2131361815);
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 201:
    }
    ProgressDialog localProgressDialog = new ProgressDialog(this);
    localProgressDialog.setMessage(getResources().getText(2131361945));
    localProgressDialog.setIndeterminate(true);
    localProgressDialog.setCancelable(true);
    localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramDialogInterface)
      {
        EditNoteActivity.this.mTaskManager.stopAll();
        EditNoteActivity.this.cancleEdit();
      }
    });
    return localProgressDialog;
  }

  public void onDestroy()
  {
    super.onDestroy();
    L.d(this, "OnDestroy called.");
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 6)
    {
      editDone();
      return true;
    }
    return false;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.mEditView.getVisibility() == 0))
    {
      hideEditView();
      showFooterBar();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onPause()
  {
    super.onPause();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
  }

  class MyJavaScriptInteface
  {
    MyJavaScriptInteface()
    {
    }

    public void editContent(String paramString)
    {
      EditNoteActivity.this.handler.post(new Runnable(paramString)
      {
        private void showEditView(String paramString)
        {
          EditNoteActivity.this.mEditTextView.setText(paramString);
          if (EditNoteActivity.this.mEditView.getVisibility() == 8)
            EditNoteActivity.this.mEditView.setVisibility(0);
          EditNoteActivity.this.mEditTextView.clearFocus();
          EditNoteActivity.this.dismissFooterBar();
        }

        public void run()
        {
          showEditView(this.val$content);
        }
      });
    }

    public void onBodyFetched(String paramString, boolean paramBoolean)
    {
      L.d(this, paramString);
      EditNoteActivity.this.handler.post(new Runnable(paramString, paramBoolean)
      {
        public void run()
        {
          L.d(EditNoteActivity.this, "body fecthed.");
          L.d(EditNoteActivity.this, "body is " + this.val$body);
          EditNoteActivity.this.mNote.setBody(this.val$body);
          if (!this.val$saveOnObtained)
            return;
          EditNoteActivity.this.saveNote();
          EditNoteActivity.this.mLogRecorder.addEditNoteTimes();
        }
      });
    }
  }

  private class YNoteWebViewClient extends WebViewClient
  {
    private YNoteWebViewClient()
    {
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      L.d(EditNoteActivity.this, "load url " + paramString);
      return true;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.EditNoteActivity
 * JD-Core Version:    0.5.4
 */