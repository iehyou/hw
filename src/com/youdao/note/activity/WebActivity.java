package com.youdao.note.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.youdao.note.YNoteApplication;
import com.youdao.note.utils.L;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebActivity extends Activity
  implements ActivityConsts
{
  public static final int FEED_BACK = 2;
  private static final String FEED_BACK_URL = "http://m.note.youdao.com/soft-manager/feedback?soft=note";
  public static final int HELP = 1;
  private static final String HELP_URL = "http://note.youdao.com/help.html";
  private static final String LOGIN_PAGE = "http://note.elibom.youdao.com/noteproxy/login";
  private static final Pattern LOGIN_REG = Pattern.compile("http://note\\.elibom\\.youdao\\.com/noteproxy/login\\?.*username=([^&]*)&password=(.*)");
  private static final String LOGIN_SUCCEED = "http://note.elibom.youdao.com/noteproxy/list";
  private static final int MESSAGE_FINISH = 1;
  public static final int REGIST = 3;
  private static final String REGIST_URL = "http://note.elibom.youdao.com/noteproxy/register?action=Register";
  private int mContentType = 3;
  private int mFeedBackLoadTimes = 0;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if (paramMessage.what != 1)
        return;
      WebActivity.this.finish();
    }
  };
  private String mPassWord = null;
  private String mUserName = null;
  private WebView mWebView = null;
  private YNoteApplication mYNote = null;

  private void loadHtml()
  {
    switch (this.mContentType)
    {
    default:
      return;
    case 1:
      this.mWebView.loadUrl("http://note.youdao.com/help.html");
      this.mWebView.setWebViewClient(new PageLoadWebClient(null));
      return;
    case 2:
      this.mWebView.loadUrl("http://m.note.youdao.com/soft-manager/feedback?soft=note&keyfrom=" + this.mYNote.getKeyFrom());
      this.mWebView.setWebViewClient(new PageLoadWebClient(null));
      return;
    case 3:
    }
    this.mWebView.loadUrl("http://note.elibom.youdao.com/noteproxy/register?action=Register&keyfrom=" + this.mYNote.getKeyFrom());
    this.mWebView.setWebViewClient(new RegistWebClient(null));
  }

  private void setTitle()
  {
    switch (this.mContentType)
    {
    default:
      return;
    case 1:
      setTitle(2131361898);
      return;
    case 2:
      setTitle(2131361899);
      return;
    case 3:
    }
    setTitle(2131361897);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    ViewGroup localViewGroup = (ViewGroup)findViewById(2131165231);
    if (localViewGroup == null)
      return;
    localViewGroup.requestLayout();
  }

  protected void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(5);
    super.onCreate(paramBundle);
    setContentView(2130903116);
    this.mYNote = YNoteApplication.getInstance();
    this.mContentType = getIntent().getIntExtra("web_conetnt", 3);
    this.mWebView = ((WebView)findViewById(2131165360));
    setProgressBarIndeterminateVisibility(true);
    setTitle();
    loadHtml();
  }

  private class PageLoadWebClient extends WebViewClient
  {
    private PageLoadWebClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      L.d(WebActivity.this, "Page loaded " + paramString);
      if (paramString.startsWith("http://m.note.youdao.com/soft-manager/feedback?soft=note"))
        WebActivity.access$208(WebActivity.this);
      if (WebActivity.this.mFeedBackLoadTimes > 1)
        WebActivity.this.mHandler.sendEmptyMessage(1);
      WebActivity.this.setProgressBarIndeterminateVisibility(false);
    }
  }

  private class RegistWebClient extends WebActivity.PageLoadWebClient
  {
    private RegistWebClient()
    {
      super(WebActivity.this, null);
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      if (paramString.startsWith("http://note.elibom.youdao.com/noteproxy/login"))
      {
        Matcher localMatcher = WebActivity.LOGIN_REG.matcher(paramString);
        if (localMatcher.find())
        {
          WebActivity.access$502(WebActivity.this, localMatcher.group(1));
          WebActivity.access$602(WebActivity.this, localMatcher.group(2));
          L.d(WebActivity.this, "Got user name and password " + WebActivity.this.mUserName + " " + WebActivity.this.mPassWord);
        }
      }
      if ((paramString.startsWith("http://note.elibom.youdao.com/noteproxy/list")) || (paramString.startsWith("http://note.elibom.youdao.com/noteproxy/login")))
      {
        L.d(WebActivity.this, "finsh regist.");
        Intent localIntent = new Intent();
        localIntent.putExtra("username", WebActivity.this.mUserName);
        localIntent.putExtra("password", WebActivity.this.mPassWord);
        WebActivity.this.setResult(-1, localIntent);
        WebActivity.this.finish();
      }
      L.d(WebActivity.this, "load url " + paramString);
      WebActivity.this.mWebView.loadUrl(paramString);
      return true;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.WebActivity
 * JD-Core Version:    0.5.4
 */