package com.youdao.note.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.youdao.note.LogRecorder;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.LocalErrorData;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.TempFile;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.ServerException;
import com.youdao.note.task.TaskManager;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.StringUtils;
import com.youdao.note.utils.UIUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailShareActivity extends LockableActivity
{
  private static final String BUNDLE_NOTE_ID = "bundle_noteId";
  private static final int DIALOG_INVALID_MAILS = 1;
  private static final int DIALOG_MAILS_EMPTY = 2;
  private static final int DIALOG_RECEIVERS_LIMIT = 7;
  private static final int DIALOG_SENDING = 5;
  private static final int DIALOG_SEND_FAILED = 6;
  private static final int DIALOG_TITLE_EMPTY = 3;
  private static final int DIALOG_TRASH = 4;
  private static final Pattern PATTERN = Pattern.compile("^([^<]+)<[^>]*>$");
  private static final int REQUEST_PICK_CONTACTS = 1;
  private String noteId;
  private String receivers = "";

  private void checkEmails()
  {
    String[] arrayOfString = ((TextView)findViewById(2131165303)).getText().toString().split("[,，]");
    int i = 0;
    int j = 0;
    ArrayList localArrayList = new ArrayList();
    int k = 0;
    if (k < arrayOfString.length)
    {
      label37: arrayOfString[k] = arrayOfString[k].trim();
      String str3 = arrayOfString[k];
      if (str3.length() != 0)
      {
        i = 1;
        if (!StringUtils.isEmail(str3))
        {
          Matcher localMatcher = PATTERN.matcher(str3);
          if (!localMatcher.find())
            break label142;
          String str4 = localMatcher.group(1);
          arrayOfString[k] = str4;
          if (!StringUtils.isEmail(str4))
          {
            arrayOfString[k] = null;
            j = 1;
            localArrayList.add(str4);
          }
        }
      }
      while (true)
      {
        ++k;
        break label37:
        label142: arrayOfString[k] = null;
        j = 1;
        localArrayList.add(str3);
      }
    }
    StringBuilder localStringBuilder1 = new StringBuilder();
    int l = 1;
    int i1 = 0;
    int i2 = arrayOfString.length;
    int i3 = 0;
    if (i3 < i2)
    {
      label184: String str2 = arrayOfString[i3];
      if (str2 == null);
      while (true)
      {
        ++i3;
        break label184:
        if (l == 0)
          localStringBuilder1.append(",");
        localStringBuilder1.append(str2);
        ++i1;
        l = 0;
      }
    }
    this.receivers = localStringBuilder1.toString();
    if (j != 0)
    {
      StringBuilder localStringBuilder2 = new StringBuilder();
      int i4 = 1;
      Iterator localIterator = localArrayList.iterator();
      while (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        if (i4 == 0)
          localStringBuilder2.append(",");
        localStringBuilder2.append("\"");
        localStringBuilder2.append(str1);
        localStringBuilder2.append("\"");
        i4 = 0;
      }
      Bundle localBundle = new Bundle();
      localBundle.putString("invalidMails", localStringBuilder2.toString());
      showDialog(1, localBundle);
      return;
    }
    if (i == 0)
    {
      showDialog(2);
      return;
    }
    if (i1 > 3)
    {
      showDialog(7);
      return;
    }
    doSendMail();
  }

  private void doSendMail()
  {
    showDialog(5);
    NoteMeta localNoteMeta = this.mDataSource.getNoteMetaById(this.noteId);
    String str1 = ((EditText)findViewById(2131165306)).getText().toString();
    String str2 = ((EditText)findViewById(2131165308)).getText().toString();
    this.mTaskManager.mailShare(localNoteMeta, this.receivers, str1, str2);
    this.mLogRecorder.addSendMailTimes();
  }

  private void hideSoftKeyboard()
  {
    ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(findViewById(2131165302).getWindowToken(), 0);
  }

  private void initListener()
  {
    findViewById(2131165305).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        Intent localIntent = new Intent(MailShareActivity.this, MailSelectionActivity.class);
        MailShareActivity.this.startActivityForResult(localIntent, 1);
      }
    });
    findViewById(2131165310).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        String str = ((EditText)MailShareActivity.this.findViewById(2131165306)).getText().toString();
        if ((str == null) || (str.trim().length() == 0))
        {
          MailShareActivity.this.showDialog(3);
          return;
        }
        MailShareActivity.this.checkEmails();
      }
    });
    findViewById(2131165302).setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
      {
        MailShareActivity.this.hideSoftKeyboard();
        return false;
      }
    });
  }

  private void initUI()
  {
    NoteMeta localNoteMeta = this.mDataSource.getNoteMetaById(this.noteId);
    String str = this.mDataSource.getNote(localNoteMeta).getTitle();
    ((EditText)findViewById(2131165306)).setText(str);
    ((ImageView)findViewById(2131165309)).setImageBitmap(ImageUtils.bytes2bitmap(TempFile.newNoteSnapshot(this.noteId).getContentBytes()));
  }

  private void onMailShareFailed(Exception paramException)
  {
    removeDialog(5);
    if ((paramException == null) || (!paramException instanceof ServerException))
    {
      showDialog(6);
      return;
    }
    ServerException localServerException = (ServerException)paramException;
    if (localServerException.getErrorCode() == 233)
    {
      UIUtilities.showToast(this, 2131362018);
      return;
    }
    if ((localServerException.getResponseCode() == 302) || (localServerException.getErrorCode() == 232))
    {
      UIUtilities.showToast(this, 2131362021);
      return;
    }
    if (localServerException.getErrorCode() == 230)
    {
      UIUtilities.showToast(this, 2131362019);
      return;
    }
    UIUtilities.showToast(this, 2131362017);
  }

  private void onMailShareSucceed()
  {
    removeDialog(5);
    Toast.makeText(this, 2131362020, 0).show();
    finish();
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 != 1) || (paramInt2 != -1))
      return;
    EditText localEditText = (EditText)findViewById(2131165303);
    String str1 = localEditText.getText().toString().trim();
    if (str1.length() != 0)
      str1 = str1 + ",";
    String str2 = paramIntent.getExtras().getString("emails");
    localEditText.setText(str1 + str2);
    localEditText.setSelection(localEditText.getText().length());
  }

  public void onBackPressed()
  {
    showDialog(4);
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setRequestedOrientation(1);
    super.onCreate(paramBundle);
    setContentView(2130903083);
    this.noteId = ((String)getIntent().getSerializableExtra("noteid"));
    initUI();
    initListener();
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt, paramBundle);
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131362023).setMessage("").setCancelable(true).setPositiveButton(2131362022, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.doSendMail();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.removeDialog(1);
        }
      }).create();
    case 2:
      return new AlertDialog.Builder(this).setTitle(2131362023).setMessage(2131362024).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.removeDialog(2);
        }
      }).create();
    case 3:
      return new AlertDialog.Builder(this).setTitle(2131362023).setMessage(2131362025).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.removeDialog(3);
        }
      }).create();
    case 7:
      return new AlertDialog.Builder(this).setTitle(2131362023).setMessage(2131362026).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.removeDialog(7);
        }
      }).create();
    case 4:
      return new AlertDialog.Builder(this).setTitle(2131362014).setMessage(2131362015).setCancelable(true).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.finish();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          MailShareActivity.this.removeDialog(4);
        }
      }).create();
    case 5:
      ProgressDialog localProgressDialog = new ProgressDialog(this);
      localProgressDialog.setMessage(getString(2131362013));
      localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramDialogInterface)
        {
          MailShareActivity.this.mTaskManager.stopAll();
        }
      });
      return localProgressDialog;
    case 6:
    }
    return new AlertDialog.Builder(this).setTitle(2131362008).setMessage(2131362016).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        MailShareActivity.this.removeDialog(6);
      }
    }).create();
  }

  public void onDestroy()
  {
    super.onDestroy();
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default:
      super.onPrepareDialog(paramInt, paramDialog, paramBundle);
      return;
    case 1:
    }
    String str = getResources().getString(2131362027).replace("${invalid_mails}", paramBundle.getString("invalidMails"));
    ((AlertDialog)paramDialog).setMessage(str);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.noteId = paramBundle.getString("bundle_noteId");
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("bundle_noteId", this.noteId);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    if (paramInt == 17)
    {
      if (!paramBoolean)
        break label22;
      onMailShareSucceed();
    }
    while (true)
    {
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
      return;
      if ((paramBaseData != null) && (paramBaseData instanceof LocalErrorData))
      {
        label22: LocalErrorData localLocalErrorData = (LocalErrorData)paramBaseData;
        if (localLocalErrorData.getException() instanceof IOException)
        {
          UIUtilities.showToast(this, 2131361953);
          removeDialog(5);
        }
        onMailShareFailed(localLocalErrorData.getException());
      }
      onMailShareFailed(null);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.MailShareActivity
 * JD-Core Version:    0.5.4
 */