package com.youdao.note.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.net.Uri;
import android.widget.Toast;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.task.local.AbsAddResourceTask;
import com.youdao.note.task.local.AddImageResourceTask;
import com.youdao.note.task.local.AddOtherResourceTask;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;

public class ResourceAdder
  implements DialogInterface.OnCancelListener
{
  private static ResourceAdder mInstance = null;
  private Activity mActivity;
  private ResourceAddCallback mCallback;
  private ProgressDialog mDialog;
  private AbsAddResourceTask<?> mTask;

  private ResourceAdder(Activity paramActivity, ResourceAddCallback paramResourceAddCallback)
  {
    this.mActivity = paramActivity;
    this.mCallback = paramResourceAddCallback;
  }

  public static ResourceAdder getInstance(Activity paramActivity, ResourceAddCallback paramResourceAddCallback)
  {
    if (mInstance == null)
      mInstance = new ResourceAdder(paramActivity, paramResourceAddCallback);
    while (true)
    {
      mInstance.mDialog = null;
      return mInstance;
      mInstance.mActivity = paramActivity;
      mInstance.mCallback = paramResourceAddCallback;
    }
  }

  public void addImageResource(Uri paramUri, int paramInt)
  {
    addImageResource(new Uri[] { paramUri }, paramInt, null);
  }

  public void addImageResource(Uri paramUri, int paramInt, ImageResourceMeta paramImageResourceMeta)
  {
    addImageResource(new Uri[] { paramUri }, paramInt, paramImageResourceMeta);
  }

  public void addImageResource(Uri[] paramArrayOfUri)
  {
    addImageResource(paramArrayOfUri, 1, null);
  }

  public void addImageResource(Uri[] paramArrayOfUri, int paramInt, ImageResourceMeta paramImageResourceMeta)
  {
    this.mActivity.showDialog(116);
    this.mTask = new AddImageResourceTask(paramArrayOfUri, paramInt, paramImageResourceMeta)
    {
      protected void onFailed(Exception paramException)
      {
        ResourceAdder.this.onFailed(paramException);
      }

      protected void onFinishAll(ImageResourceMeta paramImageResourceMeta)
      {
        ResourceAdder.this.onFinishAll(paramImageResourceMeta);
      }

      protected void onFinishOne(ImageResourceMeta paramImageResourceMeta)
      {
        ResourceAdder.this.onFinishOne(paramImageResourceMeta);
      }

      protected void onResouceTooLarge(String paramString, long paramLong)
      {
        Toast.makeText(ResourceAdder.this.mActivity, 2131362036, 1).show();
      }
    };
    this.mTask.execute(EmptyInstance.EMPTY_VOIDS);
  }

  protected void addImageResources(Uri[] paramArrayOfUri)
  {
    addImageResource(paramArrayOfUri);
  }

  public void addOtherResource(Uri paramUri, int paramInt)
  {
    this.mActivity.showDialog(116);
    if (paramInt == 0)
      throw new IllegalArgumentException("You should use the addImageResource()");
    this.mTask = new AddOtherResourceTask(paramUri, paramInt)
    {
      protected void onFailed(Exception paramException)
      {
        ResourceAdder.this.onFailed(paramException);
      }

      protected void onFinishAll(BaseResourceMeta paramBaseResourceMeta)
      {
        ResourceAdder.this.onFinishAll(paramBaseResourceMeta);
      }

      protected void onFinishOne(BaseResourceMeta paramBaseResourceMeta)
      {
        ResourceAdder.this.onFinishOne(paramBaseResourceMeta);
      }

      protected void onResouceTooLarge(String paramString, long paramLong)
      {
        Toast.makeText(ResourceAdder.this.mActivity, 2131362036, 1).show();
      }
    };
    this.mTask.execute(EmptyInstance.EMPTY_VOIDS);
  }

  protected void addResourceFromUri(Uri paramUri)
  {
    if (paramUri != null)
    {
      if (!FileUtils.isImage(FileUtils.getFileNameFromUri(paramUri)))
        break label21;
      addImageResource(paramUri, 1);
    }
    return;
    if (FileUtils.isAudio(paramUri))
    {
      label21: addOtherResource(paramUri, 4);
      return;
    }
    addOtherResource(paramUri, 1);
  }

  public ProgressDialog createDialog()
  {
    if (this.mDialog != null)
      return this.mDialog;
    this.mDialog = new ProgressDialog(this.mActivity);
    this.mDialog.setIndeterminate(true);
    this.mDialog.setCanceledOnTouchOutside(false);
    this.mDialog.setOnCancelListener(this);
    this.mDialog.setMessage(this.mActivity.getText(2131361946));
    return this.mDialog;
  }

  public void destroy()
  {
    if (this.mActivity != null)
      this.mActivity.removeDialog(116);
    if (this.mTask != null)
      this.mTask.cancel(true);
    this.mActivity = null;
    this.mCallback = null;
  }

  public void onCancel(DialogInterface paramDialogInterface)
  {
    if (this.mTask != null)
      this.mTask.stop(true);
    if (this.mCallback == null)
      return;
    this.mCallback.onCancle();
  }

  public void onFailed(Exception paramException)
  {
    L.e(this, "", paramException);
    this.mActivity.removeDialog(116);
    Toast.makeText(this.mActivity, 2131362035, 1).show();
    if (this.mCallback == null)
      return;
    this.mCallback.onFailed(paramException);
  }

  public void onFinishAll(IResourceMeta paramIResourceMeta)
  {
    this.mActivity.removeDialog(116);
    if (this.mCallback == null)
      return;
    this.mCallback.onFinishAll(paramIResourceMeta);
  }

  public void onFinishOne(IResourceMeta paramIResourceMeta)
  {
    if (this.mCallback == null)
      return;
    this.mCallback.onFinishOne(paramIResourceMeta);
  }

  public static abstract interface ResourceAddCallback
  {
    public abstract void onCancle();

    public abstract void onFailed(Exception paramException);

    public abstract void onFinishAll(IResourceMeta paramIResourceMeta);

    public abstract void onFinishOne(IResourceMeta paramIResourceMeta);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.ResourceAdder
 * JD-Core Version:    0.5.4
 */