package com.youdao.note.activity;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.delegate.SyncbarDelegate;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.adapter.SectionAdapter;
import com.youdao.note.data.adapter.SectionItem;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.ui.DialogFactory;
import com.youdao.note.ui.DialogFactory.NoteDeletedCallback;
import com.youdao.note.utils.Consts.DATA_TYPE;
import com.youdao.note.utils.L;
import java.io.Serializable;

public class NoteListActivity extends BaseListFileActivity
  implements Consts.DATA_TYPE, DialogInterface.OnClickListener, ActivityConsts
{
  private static final String LONGCLICKED_NOTEMETA = "longclicked_notemeta";
  private View mEmptyNotebooks = null;
  private View mEmptyNotes = null;
  private TextView mEmptyTip = null;
  private NoteMeta mLongClickedNoteMeta = null;
  private NoteBook mNoteBook = null;
  private String mNoteBookId = null;
  private SyncbarDelegate syncbarDelegate;

  private void initControllers()
  {
    this.mEmptyNotes = findViewById(2131165332);
    this.mEmptyNotebooks = findViewById(2131165333);
    this.mEmptyTip = ((TextView)findViewById(2131165334));
    findViewById(2131165335).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        NoteListActivity.this.sendNewNote();
      }
    });
    this.mNoteBookId = getIntent().getStringExtra("noteBook");
    if (this.mNoteBookId == null)
      return;
    this.mNoteBook = this.mDataSource.getNoteBookMetaById(this.mNoteBookId);
    L.d(this, "note book id is " + this.mNoteBookId);
  }

  private void prepareEmptyView()
  {
    if (this.mNoteBookId == null)
    {
      this.mEmptyNotes.setVisibility(0);
      this.mEmptyNotebooks.setVisibility(8);
      this.mEmptyTip.setText(2131361965);
      return;
    }
    this.mEmptyNotes.setVisibility(8);
    this.mEmptyNotebooks.setVisibility(0);
    this.mEmptyTip.setText(2131361980);
  }

  private void updateAdapter(Cursor paramCursor)
  {
    monitorenter;
    try
    {
      startManagingCursor(paramCursor);
      Resources localResources = getResources();
      LayoutInflater localLayoutInflater = getLayoutInflater();
      if (this.mNoteBookId == null);
      SectionAdapter localSectionAdapter;
      for (String str1 = localResources.getString(2131361881); ; str1 = this.mNoteBook.getTitle().replaceAll("%", "%%") + localResources.getString(2131361880))
      {
        boolean bool = this.mYNote.isLogin();
        String str2 = null;
        if (bool)
          str2 = localResources.getString(2131361859);
        localSectionAdapter = new SectionAdapter(localLayoutInflater, paramCursor, str1, str2);
        if (paramCursor.getCount() != 0)
          break;
        prepareEmptyView();
        setListAdapter(null);
        return;
      }
      setListAdapter(localSectionAdapter);
    }
    finally
    {
      monitorexit;
    }
  }

  private void updateData()
  {
    if ((this.mYNote.isLogin()) || (!this.mYNote.isEverLogin()))
      if (this.mNoteBookId != null)
        break label73;
    for (Cursor localCursor = this.mDataSource.listAllNotes(); ; localCursor = this.mDataSource.listNoteByNotebook(this.mNoteBookId))
    {
      L.d(this, "total notes count is " + localCursor.getCount());
      if (localCursor != null)
        updateAdapter(localCursor);
      return;
      label73: if (this.mDataSource.getNoteBookMetaById(this.mNoteBookId) != null)
        continue;
      finish();
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    case 0:
    }
    do
      return;
    while (paramInt2 != 1);
    updateData();
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903093);
    super.onCreate(paramBundle);
    this.syncbarDelegate = new SyncbarDelegate(this);
    L.d(this, "onCreate called.");
    initControllers();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 106:
      return DialogFactory.getNoteLongClickMenu(this, this.mLongClickedNoteMeta);
    case 100:
      return DialogFactory.getDeleteConfirmDialog(this, this.mLongClickedNoteMeta, new DialogFactory.NoteDeletedCallback()
      {
        public void onNoteDeleted()
        {
          NoteListActivity.this.updateData();
        }
      });
    case 105:
    }
    this.mLogRecorder.addNoteDetailTimes();
    return DialogFactory.getNoteDetailDialog(this, this.mLongClickedNoteMeta.getNoteId());
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    new MenuInflater(this).inflate(2131034114, paramMenu);
    return true;
  }

  public void onIntentSend(Intent paramIntent)
  {
    if ((!"com.youdao.note.action.CREATE_TEXT".equals(paramIntent.getAction())) || (this.mNoteBookId == null))
      return;
    paramIntent.putExtra("noteBook", this.mNoteBookId);
  }

  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    SectionItem localSectionItem = (SectionItem)this.mAdapter.getItem(paramInt);
    if (localSectionItem.getType() == 1)
    {
      this.mLongClickedNoteMeta = localSectionItem.getNoteMeta();
      removeDialog(105);
      removeDialog(104);
      removeDialog(106);
      removeDialog(100);
      showDialog(106);
      return true;
    }
    return false;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.syncbarDelegate.onOptionsItemSelected(paramMenuItem))
      return true;
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.syncbarDelegate.onPrepareOptionsMenu(paramMenu);
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    Serializable localSerializable = paramBundle.getSerializable("longclicked_notemeta");
    if (localSerializable != null)
      this.mLongClickedNoteMeta = ((NoteMeta)localSerializable);
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    updateData();
    this.syncbarDelegate.onResume();
    L.d(this, "onResume called.");
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.mLongClickedNoteMeta != null)
      paramBundle.putSerializable("longclicked_notemeta", this.mLongClickedNoteMeta);
    super.onSaveInstanceState(paramBundle);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    this.syncbarDelegate.onUpdate(paramInt, paramBaseData, paramBoolean);
    switch (paramInt)
    {
    default:
    case 1:
    case 6:
    }
    while (true)
    {
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
      return;
      if (!paramBoolean)
        continue;
      updateData();
      continue;
      if (!paramBoolean)
        continue;
      updateData();
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.NoteListActivity
 * JD-Core Version:    0.5.4
 */