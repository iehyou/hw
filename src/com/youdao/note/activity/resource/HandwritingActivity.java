package com.youdao.note.activity.resource;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.ui.skitch.handwrite.HandWritePageView;
import com.youdao.note.ui.skitch.handwrite.HandwriteMeta;
import com.youdao.note.ui.skitch.handwrite.WriteViewLayout;
import com.youdao.note.ui.skitch.tool.PaintSliderView;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;

public class HandwritingActivity extends BaseResourceNoteActivity
{
  private static final int DIALOG_BACK = 2;
  public static final int DIALOG_MODE_TIPS = 3;
  private static final int DIALOG_TRASH = 1;
  private HandwriteMeta handwriteMeta;
  private Handler mHandler = new Handler();
  private HandwriteMeta oldHandwriteMeta;
  private HandWritePageView pageView = null;
  private WriteViewLayout writeViewLayout = null;

  private void dismissHandwriteGuide()
  {
    View localView = findViewById(2131165253);
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
    localAlphaAnimation.setDuration(500L);
    localView.setAnimation(localAlphaAnimation);
    localView.setVisibility(8);
  }

  private void giveupHandwrite()
  {
    YNoteApplication localYNoteApplication = YNoteApplication.getInstance();
    if (this.oldHandwriteMeta != null)
      localYNoteApplication.getSkitchManager().addOrUpdateSkitchMeta(this.resourceMeta.getResourceId(), this.oldHandwriteMeta);
    cancleAndBack();
  }

  private void initViewListener()
  {
    findViewById(2131165246).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        HandwritingActivity.this.pageView.addBlank();
      }
    });
    findViewById(2131165248).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        HandwritingActivity.this.pageView.undo();
      }
    });
    findViewById(2131165247).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        HandwritingActivity.this.pageView.addReturn();
      }
    });
    findViewById(2131165249).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        PaintSliderView localPaintSliderView = (PaintSliderView)HandwritingActivity.this.findViewById(2131165243);
        if (localPaintSliderView.getVisibility() == 0)
        {
          localPaintSliderView.dismiss();
          return;
        }
        localPaintSliderView.show();
      }
    });
    findViewById(2131165252).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        try
        {
          Bitmap localBitmap1 = HandwritingActivity.this.pageView.getBitmap();
          View localView = HandwritingActivity.this.findViewById(2131165241);
          Bitmap localBitmap2 = Bitmap.createBitmap(localView.getWidth(), localView.getHeight(), Bitmap.Config.ARGB_8888);
          localView.draw(new Canvas(localBitmap2));
          Bitmap localBitmap3 = Bitmap.createBitmap(localBitmap1.getWidth(), localBitmap1.getHeight() + localBitmap2.getHeight(), Bitmap.Config.ARGB_8888);
          localBitmap3.eraseColor(-1);
          Canvas localCanvas = new Canvas(localBitmap3);
          Paint localPaint = new Paint();
          localPaint.setColor(-16777216);
          localPaint.setAntiAlias(true);
          localCanvas.drawBitmap(localBitmap2, new Rect(0, 0, localBitmap2.getWidth(), localBitmap2.getHeight()), new Rect(0, 0, localBitmap3.getWidth(), localBitmap2.getHeight()), localPaint);
          localCanvas.drawBitmap(localBitmap1, new Rect(0, 0, localBitmap1.getWidth(), localBitmap1.getHeight()), new Rect(0, localBitmap2.getHeight(), localBitmap3.getWidth(), localBitmap3.getHeight()), localPaint);
          HandwritingActivity.this.saveHandwriteResource(localBitmap3);
          localBitmap2.recycle();
          localBitmap1.recycle();
          localBitmap3.recycle();
          YNoteApplication.getInstance().getSkitchManager().addOrUpdateSkitchMeta(HandwritingActivity.this.resourceMeta.getResourceId(), HandwritingActivity.this.handwriteMeta);
          HandwritingActivity.this.finishAndBack();
          return;
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
          UIUtilities.showToast(HandwritingActivity.this, 2131361994);
          HandwritingActivity.this.finish();
        }
      }
    });
    findViewById(2131165250).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if ((HandwritingActivity.this.handwriteMeta == null) || (HandwritingActivity.this.handwriteMeta.isEmpty()))
          return;
        HandwritingActivity.this.showDialog(1);
      }
    });
    findViewById(2131165254).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        HandwritingActivity.this.dismissHandwriteGuide();
      }
    });
    findViewById(2131165255).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        HandwritingActivity.this.mYNote.setShowHandwriteGuide(false);
        HandwritingActivity.this.dismissHandwriteGuide();
      }
    });
  }

  public void onBackPressed()
  {
    if (findViewById(2131165253).getVisibility() == 8)
    {
      showDialog(2);
      return;
    }
    giveupHandwrite();
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903062);
    super.onCreate(paramBundle);
    this.pageView = ((HandWritePageView)findViewById(2131165242));
    Intent localIntent = getIntent();
    if (localIntent.getAction().equals("com.youdao.note.action.EDIT_HANDWRITE"))
      this.handwriteMeta = ((HandwriteMeta)this.mYNote.getSkitchManager().getSkitchMeta(this.resourceMeta.getResourceId()));
    try
    {
      this.oldHandwriteMeta = this.handwriteMeta.copy();
      label82: this.handwriteMeta.setIsNew(false);
      label90: this.pageView.initSkitchMeta(this.handwriteMeta);
      this.writeViewLayout = ((WriteViewLayout)findViewById(2131165244));
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      this.writeViewLayout.init(localDisplayMetrics.density, localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels);
      this.writeViewLayout.setHandWriteCanvas(this.pageView);
      PaintSliderView localPaintSliderView = (PaintSliderView)findViewById(2131165243);
      localPaintSliderView.setSkitchCanvas(this.pageView);
      localPaintSliderView.setVisibility(4);
      if ((!this.mYNote.isShowHandwriteGuide()) || (!localIntent.getAction().equals("com.youdao.note.action.NEW_HANDWRITE")))
        break label288;
      findViewById(2131165253).setVisibility(0);
      initViewListener();
      this.mHandler.postDelayed(new Runnable()
      {
        public void run()
        {
          HandwritingActivity.this.writeViewLayout.setVisibility(8);
        }
      }
      , 500L);
      label288: return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      UIUtilities.showToast(this, 2131361994);
      finish();
      break label82:
      this.handwriteMeta = new HandwriteMeta();
      this.oldHandwriteMeta = null;
      break label90:
      findViewById(2131165253).setVisibility(8);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131361996).setMessage(2131361997).setCancelable(true).setPositiveButton(2131361998, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          HandwritingActivity.this.pageView.trash();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          HandwritingActivity.this.dismissDialog(1);
        }
      }).create();
    case 2:
      return new AlertDialog.Builder(this).setTitle(2131361999).setCancelable(true).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          HandwritingActivity.this.giveupHandwrite();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          HandwritingActivity.this.dismissDialog(2);
        }
      }).create();
    case 3:
    }
    return new AlertDialog.Builder(this).setTitle(2131362043).setMessage(2131362044).setPositiveButton(2131362047, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        HandwritingActivity.this.writeViewLayout.switchToNormalMode();
        HandwritingActivity.this.writeViewLayout.postInvalidate();
        YNoteApplication.getInstance().setHandwriteMode(0);
        YNoteApplication.getInstance().setShowHandwriteModeTips(false);
        UIUtilities.showToast(HandwritingActivity.this, 2131362046);
      }
    }).setNegativeButton(2131362048, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        HandwritingActivity.this.dismissDialog(3);
        UIUtilities.showToast(HandwritingActivity.this, 2131362045);
        YNoteApplication.getInstance().setShowHandwriteModeTips(false);
      }
    }).create();
  }

  protected void onPause()
  {
    super.onPause();
    this.writeViewLayout.onPause();
    this.writeViewLayout.setVisibility(8);
  }

  public void onResume()
  {
    super.onResume();
    this.writeViewLayout.onResume();
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    L.d(this, "HandWriteActivity on touch called.");
    HandWritePageView localHandWritePageView = this.pageView;
    int[] arrayOfInt = new int[2];
    localHandWritePageView.getLocationOnScreen(arrayOfInt);
    int i = arrayOfInt[1] + localHandWritePageView.getHeight();
    if (paramMotionEvent.getY() < i - 20)
    {
      this.writeViewLayout.setVisibility(0);
      this.writeViewLayout.onTouchEvent(paramMotionEvent);
    }
    return true;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.resource.HandwritingActivity
 * JD-Core Version:    0.5.4
 */