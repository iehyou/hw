package com.youdao.note.activity.resource;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.LockableActivity;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.DoodleResource;
import com.youdao.note.data.resource.DoodleResourceMeta;
import com.youdao.note.data.resource.HandwriteResource;
import com.youdao.note.data.resource.HandwriteResourceMeta;
import com.youdao.note.data.resource.ImageResource;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.BitmapMemCache;
import com.youdao.note.utils.ImageUtils;
import java.io.IOException;
import java.io.Serializable;

public class BaseResourceNoteActivity extends LockableActivity
{
  private static final String BUNDLE_RESOURCE_META = "resourceMeta";
  protected BaseResourceMeta resourceMeta = null;

  protected void cancleAndBack()
  {
    setResult(0, new Intent());
    finish();
  }

  protected void finishAndBack()
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("resourceMeta", this.resourceMeta);
    setResult(-1, localIntent);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.resourceMeta = ((BaseResourceMeta)getIntent().getSerializableExtra("resourceMeta"));
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    Serializable localSerializable = paramBundle.getSerializable("resourceMeta");
    if (localSerializable == null)
      return;
    this.resourceMeta = ((BaseResourceMeta)localSerializable);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.resourceMeta == null)
      return;
    paramBundle.putSerializable("resourceMeta", this.resourceMeta);
  }

  protected void saveDoodleResource(Bitmap paramBitmap)
  {
    if ((this.resourceMeta == null) || (!this.resourceMeta instanceof DoodleResourceMeta));
    for (DoodleResource localDoodleResource = ResourceUtils.genEmptyDoodleResource(); ; localDoodleResource = (DoodleResource)this.mDataSource.getResource(this.resourceMeta))
    {
      localDoodleResource.setContentBytes(ImageUtils.bitmap2bytes(paramBitmap, Bitmap.CompressFormat.JPEG, 100));
      ImageUtils.persistImage(localDoodleResource, paramBitmap, 100, false);
      this.resourceMeta = localDoodleResource.getMeta();
      BitmapMemCache.getInstance().removeBitmap(this.mDataSource.getThumbnailPath(this.resourceMeta));
      return;
    }
  }

  protected void saveHandwriteResource(Bitmap paramBitmap)
  {
    if ((this.resourceMeta == null) || (!this.resourceMeta instanceof HandwriteResourceMeta));
    for (HandwriteResource localHandwriteResource = ResourceUtils.genEmptyHandwriteResource(); ; localHandwriteResource = (HandwriteResource)this.mDataSource.getResource(this.resourceMeta))
    {
      localHandwriteResource.setContentBytes(ImageUtils.bitmap2bytes(paramBitmap, Bitmap.CompressFormat.JPEG, 100));
      ImageUtils.persistImage(localHandwriteResource, paramBitmap, 100, false);
      this.resourceMeta = localHandwriteResource.getMeta();
      BitmapMemCache.getInstance().removeBitmap(this.mDataSource.getThumbnailPath(this.resourceMeta));
      return;
    }
  }

  protected void saveImageResource(Bitmap paramBitmap, int paramInt)
    throws IOException
  {
    if ((this.resourceMeta == null) || (!this.resourceMeta instanceof ImageResourceMeta));
    for (ImageResource localImageResource = ResourceUtils.genEmptyImageResource(); ; localImageResource = (ImageResource)this.mDataSource.getResource(this.resourceMeta))
    {
      ((ImageResourceMeta)localImageResource.getMeta()).setPicFrom(paramInt);
      ImageUtils.persistImage(localImageResource, paramBitmap, this.mYNote.getImageQuality(), true);
      this.resourceMeta = localImageResource.getMeta();
      return;
    }
  }

  protected void saveImageResource(Uri paramUri, int paramInt)
    throws IOException
  {
    if ((this.resourceMeta == null) || (!this.resourceMeta instanceof ImageResourceMeta));
    for (ImageResource localImageResource = ResourceUtils.genEmptyImageResource(); ; localImageResource = (ImageResource)this.mDataSource.getResource(this.resourceMeta))
    {
      ((ImageResourceMeta)localImageResource.getMeta()).setPicFrom(paramInt);
      ImageUtils.persistImage(localImageResource, paramUri, this.mYNote.getImageQuality(), true);
      this.resourceMeta = localImageResource.getMeta();
      return;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.resource.BaseResourceNoteActivity
 * JD-Core Version:    0.5.4
 */