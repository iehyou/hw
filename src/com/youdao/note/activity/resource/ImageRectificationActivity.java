package com.youdao.note.activity.resource;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.ui.ImageRectificationView;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.IOException;

public class ImageRectificationActivity extends BaseResourceNoteActivity
{
  public static final int DIALOG_ID_AUTO_DETECT = 0;
  public static final int DIALOG_ID_COMPLETE = 1;
  private AnimationDrawable animation;
  private RelativeLayout confirmView;
  private Handler correlationConfirmHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      ImageRectificationActivity.this.setContentView(2130903067);
      ((ImageView)ImageRectificationActivity.this.findViewById(2131165262)).setImageBitmap(ImageRectificationActivity.this.image);
      ImageRectificationActivity.this.findViewById(2131165263).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          ImageRectificationActivity.this.setContentView(ImageRectificationActivity.this.mainView);
          if (ImageRectificationActivity.this.confirmView == null)
            return;
          ImageRectificationActivity.access$502(ImageRectificationActivity.this, null);
        }
      });
      ImageRectificationActivity.this.findViewById(2131165264).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          Intent localIntent = ImageRectificationActivity.this.getIntent();
          localIntent.putExtra("resourceMeta", ImageRectificationActivity.this.resourceMeta);
          ImageRectificationActivity.this.setResult(-1, localIntent);
          ImageRectificationActivity.this.finish();
        }
      });
    }
  };
  private Handler firstAnimHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      Bundle localBundle = paramMessage.getData();
      float f1 = localBundle.getFloat("endRatio");
      float f2 = localBundle.getFloat("x");
      float f3 = localBundle.getFloat("y");
      ImageRectificationActivity.access$102(ImageRectificationActivity.this, ImageRectificationActivity.this.findViewById(2131165202).getHeight());
      Bitmap localBitmap = ((BitmapDrawable)ImageRectificationActivity.this.animation.getFrame(0)).getBitmap();
      int i = localBitmap.getWidth();
      int j = localBitmap.getHeight();
      ImageRectificationActivity.access$302(ImageRectificationActivity.this, f1 * i);
      ImageRectificationActivity.access$402(ImageRectificationActivity.this, f1 * j);
      ImageRectificationActivity.access$502(ImageRectificationActivity.this, new RelativeLayout(ImageRectificationActivity.this));
      ImageRectificationActivity.this.confirmView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
      ImageRectificationActivity.this.setContentView(ImageRectificationActivity.this.confirmView);
      View localView = new View(ImageRectificationActivity.this);
      localView.setBackgroundDrawable(ImageRectificationActivity.this.animation);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(i, j);
      localLayoutParams.setMargins((int)f2, (int)f3 + ImageRectificationActivity.this.titleHeight, 0, 0);
      localView.setLayoutParams(localLayoutParams);
      ImageRectificationActivity.this.confirmView.addView(localView);
      ImageRectificationActivity.this.animation.setOneShot(true);
      ImageRectificationActivity.this.animation.start();
      int k = 0;
      int l = ImageRectificationActivity.this.animation.getNumberOfFrames();
      for (int i1 = 0; i1 < l; ++i1)
        k += ImageRectificationActivity.this.animation.getDuration(i1);
      Handler localHandler = new Handler();
      1 local1 = new Runnable(f3, j)
      {
        public void run()
        {
          int i = (int)(ImageRectificationActivity.this.titleHeight + this.val$imageY + this.val$height / 2 - ImageRectificationActivity.this.confirmView.getHeight() / 2);
          Message localMessage = new Message();
          localMessage.arg1 = i;
          ImageRectificationActivity.this.secondAnimHandler.sendMessage(localMessage);
        }
      };
      localHandler.postDelayed(local1, k);
    }
  };
  private float firstAnimationEndHeight;
  private float firstAnimationEndWidth;
  private Bitmap image;
  private View mainView;
  private Handler secondAnimHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      int i = paramMessage.arg1;
      float f1 = ImageRectificationActivity.this.confirmView.getWidth();
      float f2 = ImageRectificationActivity.this.confirmView.getHeight();
      ImageRectificationActivity.this.confirmView.removeAllViews();
      ImageView localImageView = new ImageView(ImageRectificationActivity.this);
      localImageView.setImageBitmap(ImageRectificationActivity.this.image);
      localImageView.setScaleType(ImageView.ScaleType.CENTER);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(ImageRectificationActivity.this.confirmView.getWidth(), ImageRectificationActivity.this.confirmView.getHeight() + i * 2);
      localLayoutParams.setMargins(0, i / 2, 0, 0);
      localImageView.setLayoutParams(localLayoutParams);
      ImageRectificationActivity.this.confirmView.addView(localImageView);
      int j = ImageRectificationActivity.this.image.getWidth();
      int k = ImageRectificationActivity.this.image.getHeight();
      float f3 = Math.max(Math.max(1.0F, j / f1), k / f2);
      float f4 = f3 * (ImageRectificationActivity.this.firstAnimationEndWidth / j);
      float f5 = f3 * (ImageRectificationActivity.this.firstAnimationEndHeight / k);
      float f6 = Math.min(f3 * Math.min(f1 / j, f2 / k), 1.0F);
      ScaleAnimation localScaleAnimation = new ScaleAnimation(f4, f6, f5, f6, 1, 0.5F, 1, 0.5F);
      localScaleAnimation.setDuration(800L);
      ImageRectificationActivity.this.confirmView.startAnimation(localScaleAnimation);
      new Handler().postDelayed(new Runnable()
      {
        public void run()
        {
          ImageRectificationActivity.this.correlationConfirmHandler.sendEmptyMessage(0);
        }
      }
      , 800L);
      int l = ImageRectificationActivity.this.animation.getNumberOfFrames();
      for (int i1 = 0; i1 < l; ++i1)
        ((BitmapDrawable)ImageRectificationActivity.this.animation.getFrame(i1)).getBitmap().recycle();
      ImageRectificationActivity.access$202(ImageRectificationActivity.this, null);
    }
  };
  private int titleHeight;
  private ImageRectificationView view;

  public void completeRectification(Bitmap paramBitmap, AnimationDrawable paramAnimationDrawable, float paramFloat, PointF paramPointF)
  {
    this.image = paramBitmap;
    ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)this.resourceMeta;
    try
    {
      saveImageResource(paramBitmap, localImageResourceMeta.getPicFrom());
      this.animation = paramAnimationDrawable;
      Message localMessage = new Message();
      Bundle localBundle = new Bundle();
      localBundle.putFloat("endRatio", paramFloat);
      localBundle.putFloat("x", paramPointF.x);
      localBundle.putFloat("y", paramPointF.y);
      localMessage.setData(localBundle);
      this.firstAnimHandler.sendMessage(localMessage);
      return;
    }
    catch (IOException localIOException)
    {
      L.e(this, "save image failed!", localIOException);
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 != 11) || (paramInt2 != -1) || (paramIntent == null))
      return;
    this.resourceMeta = ((BaseResourceMeta)paramIntent.getSerializableExtra("resourceMeta"));
    Intent localIntent = getIntent();
    localIntent.putExtra("resourceMeta", this.resourceMeta);
    setResult(-1, localIntent);
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    try
    {
      L.d(this, "WhiteBoardActivity create");
      requestWindowFeature(1);
      setRequestedOrientation(1);
      super.onCreate(paramBundle);
      this.mainView = LayoutInflater.from(this).inflate(2130903066, null);
      setContentView(this.mainView);
      label147: this.view = ((ImageRectificationView)findViewById(2131165261));
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      try
      {
        File localFile = ((ImageResourceMeta)this.resourceMeta).getTempFile();
        if (localFile.exists())
        {
          this.image = FileUtils.getBitmapFromUri(Uri.fromFile(localFile));
          if (this.image.getConfig() != Bitmap.Config.ARGB_8888)
          {
            Bitmap localBitmap = this.image.copy(Bitmap.Config.ARGB_8888, true);
            this.image.recycle();
            this.image = localBitmap;
          }
          this.view.setImage(this.image);
          this.view.setBackgroundColor(0);
          findViewById(2131165257).setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramView)
            {
              ImageRectificationActivity.this.view.menuHandler.sendEmptyMessage(1);
            }
          });
          findViewById(2131165258).setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramView)
            {
              ImageRectificationActivity.this.view.menuHandler.sendEmptyMessage(2);
            }
          });
          findViewById(2131165259).setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramView)
            {
              ImageRectificationActivity.this.view.menuHandler.sendEmptyMessage(3);
            }
          });
          findViewById(2131165260).setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramView)
            {
              ImageRectificationActivity.this.view.menuHandler.sendEmptyMessage(4);
            }
          });
          System.gc();
          return;
        }
        this.image = FileUtils.getBitmapFromUri(Uri.fromFile(new File(this.mDataSource.getResourcePath(this.resourceMeta))));
      }
      catch (IOException localIOException)
      {
        L.e(this.resourceMeta, "get resource by meta failed", localIOException);
        break label147:
        localOutOfMemoryError = localOutOfMemoryError;
        System.gc();
        Toast.makeText(this, 2131361994, 0).show();
        if (this.image != null)
        {
          this.image.recycle();
          this.image = null;
        }
        finish();
      }
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 0:
      ProgressDialog localProgressDialog2 = new ProgressDialog(this);
      localProgressDialog2.setMessage("正在检测，请稍候...");
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(false);
      return localProgressDialog2;
    case 1:
    }
    ProgressDialog localProgressDialog1 = new ProgressDialog(this);
    localProgressDialog1.setMessage("正在处理，请稍候...");
    localProgressDialog1.setIndeterminate(true);
    localProgressDialog1.setCancelable(false);
    return localProgressDialog1;
  }

  public void onDestroy()
  {
    this.view.menuHandler.sendEmptyMessage(5);
    L.d(this, "WhiteboardActivity destroyed");
    super.onDestroy();
    this.image.recycle();
    this.view.onDestroy();
    System.gc();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.confirmView != null) && (this.confirmView.getVisibility() == 0))
    {
      setContentView(this.mainView);
      this.confirmView = null;
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.resource.ImageRectificationActivity
 * JD-Core Version:    0.5.4
 */