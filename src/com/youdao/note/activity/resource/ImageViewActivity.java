package com.youdao.note.activity.resource;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.ResourceAdder;
import com.youdao.note.activity.ResourceAdder.ResourceAddCallback;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ImageResource;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import java.io.File;
import java.io.FileNotFoundException;

public class ImageViewActivity extends BaseResourceNoteActivity
{
  private static final String BUNDLE_CREATE_OR_EDIT = "create_or_edit";
  private static final String BUNDLE_URI_CAMERA = "uri4camera";
  private static final String BUNDLE_URI_VIEW = "uri4view";
  private static final int DIALOG_DELETE_RESOURCE_CONFIRM = 1;
  public static final String IMAGE_EXTRA_NAME = "image";
  private boolean createOrEdit;
  protected LogRecorder mLogRecorder = null;
  private ResourceAdder mResourceAdder;
  protected YNoteApplication mYNote = null;
  private Uri uri4camera;
  private Uri uri4view;

  private void complete()
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("resourceMeta", this.resourceMeta);
    setResult(-1, localIntent);
    finish();
  }

  private void updateUI()
  {
    try
    {
      if (this.resourceMeta == null)
        return;
      ImageView localImageView = (ImageView)findViewById(2131165265);
      if (this.uri4view == null)
        updateUri();
      if (this.uri4view == null)
        return;
      boolean bool = FileUtils.exist(this.uri4view);
      if (!bool)
        return;
      try
      {
        localImageView.setImageBitmap(FileUtils.getBitmapFromUri(this.uri4view));
        return;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        L.e(this, "load image view failed!", localFileNotFoundException);
        return;
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      System.gc();
      Toast.makeText(this, 2131361994, 0).show();
      finish();
    }
  }

  private void updateUri()
  {
    if (this.resourceMeta != null)
    {
      this.uri4view = Uri.fromFile(new File(this.mDataSource.getResourcePath(this.resourceMeta)));
      return;
    }
    this.uri4view = null;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 11:
      do
        return;
      while ((paramInt2 != -1) || (paramIntent == null));
      try
      {
        this.resourceMeta = ((BaseResourceMeta)paramIntent.getSerializableExtra("resourceMeta"));
        updateUri();
        complete();
        return;
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        System.gc();
        Toast.makeText(this, 2131361994, 0).show();
        finish();
        return;
      }
    case 5:
      L.d(this, "On activity result called.");
      if (this.uri4camera == null)
      {
        UIUtilities.showToast(this, 2131361919);
        finish();
        return;
      }
      if (!FileUtils.exist(this.uri4camera))
      {
        UIUtilities.showToast(this, 2131361919);
        finish();
        return;
      }
      boolean bool2 = this.resourceMeta instanceof ImageResourceMeta;
      ImageResourceMeta localImageResourceMeta2 = null;
      if (bool2)
        localImageResourceMeta2 = (ImageResourceMeta)this.resourceMeta;
      this.mResourceAdder.addImageResource(this.uri4camera, 2, localImageResourceMeta2);
      this.uri4camera = null;
      return;
    case 6:
    }
    if ((paramIntent == null) || (paramIntent.getData() == null))
    {
      UIUtilities.showToast(this, 2131361919);
      finish();
      return;
    }
    if (!FileUtils.exist(paramIntent.getData()))
    {
      UIUtilities.showToast(this, 2131361919);
      finish();
      return;
    }
    boolean bool1 = this.resourceMeta instanceof ImageResourceMeta;
    ImageResourceMeta localImageResourceMeta1 = null;
    if (bool1)
      localImageResourceMeta1 = (ImageResourceMeta)this.resourceMeta;
    this.mResourceAdder.addImageResource(paramIntent.getData(), 1, localImageResourceMeta1);
  }

  public void onBackPressed()
  {
    if ((this.resourceMeta != null) && (this.createOrEdit))
    {
      if (this.resourceMeta instanceof ImageResourceMeta)
        ((ImageResourceMeta)this.resourceMeta).getTempFile().delete();
      this.mDataSource.deleteResource(this.resourceMeta);
    }
    cancleAndBack();
  }

  public void onCancel(DialogInterface paramDialogInterface)
  {
    cancleAndBack();
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    super.onCreate(paramBundle);
    this.mResourceAdder = ResourceAdder.getInstance(this, new ResourceAdder.ResourceAddCallback()
    {
      public void onCancle()
      {
        ImageViewActivity.this.onBackPressed();
      }

      public void onFailed(Exception paramException)
      {
        ImageViewActivity.this.onBackPressed();
      }

      public void onFinishAll(IResourceMeta paramIResourceMeta)
      {
      }

      public void onFinishOne(IResourceMeta paramIResourceMeta)
      {
        ImageViewActivity.this.removeDialog(116);
        ImageViewActivity.this.resourceMeta = ((BaseResourceMeta)paramIResourceMeta);
        ImageViewActivity.this.updateUri();
        ImageViewActivity.this.updateUI();
      }
    });
    this.mYNote = YNoteApplication.getInstance();
    this.mLogRecorder = this.mYNote.getLogRecorder();
    String str = getIntent().getAction();
    if ("android.intent.action.GET_CONTENT".equals(str))
    {
      Intent localIntent1 = new Intent("android.intent.action.GET_CONTENT");
      localIntent1.addCategory("android.intent.category.OPENABLE");
      localIntent1.setType("image/*");
      startActivityForResult(localIntent1, 6);
      getIntent().setAction("com.youdao.note.action.CREATE_RESOURCE");
      str = "com.youdao.note.action.CREATE_RESOURCE";
      label112: if (!"com.youdao.note.action.CREATE_RESOURCE".equals(str))
        break label263;
      this.createOrEdit = true;
      setContentView(2130903070);
      findViewById(2131165264).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          ImageViewActivity.this.complete();
        }
      });
    }
    while (true)
    {
      findViewById(2131165266).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          if (ImageViewActivity.this.mYNote.isLoadImageLibSuccess())
          {
            ImageViewActivity.this.mLogRecorder.addImageRectificationTimes();
            Intent localIntent = new Intent(ImageViewActivity.this, ImageRectificationActivity.class);
            localIntent.putExtra("resourceMeta", ImageViewActivity.this.resourceMeta);
            ImageViewActivity.this.startActivityForResult(localIntent, 11);
            ((ImageView)ImageViewActivity.this.findViewById(2131165265)).setImageDrawable(null);
            System.gc();
            return;
          }
          Toast.makeText(ImageViewActivity.this, 2131362038, 0).show();
        }
      });
      return;
      if ("android.media.action.IMAGE_CAPTURE".equals(str));
      Intent localIntent2 = new Intent("android.media.action.IMAGE_CAPTURE");
      ImageResource localImageResource = ResourceUtils.genEmptyImageResource();
      this.resourceMeta = localImageResource.getMeta();
      this.uri4camera = Uri.fromFile(((ImageResourceMeta)localImageResource.getMeta()).getTempFile());
      localIntent2.putExtra("output", this.uri4camera);
      startActivityForResult(localIntent2, 5);
      getIntent().setAction("com.youdao.note.action.CREATE_RESOURCE");
      str = "com.youdao.note.action.CREATE_RESOURCE";
      break label112:
      label263: this.createOrEdit = false;
      setContentView(2130903071);
      findViewById(2131165268).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          ImageViewActivity.this.showDialog(1);
        }
      });
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131361846).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          ImageViewActivity.this.setResult(1, ImageViewActivity.this.getIntent());
          ImageViewActivity.this.finish();
        }
      }).setNegativeButton(2131361962, null).create();
    case 116:
    }
    return this.mResourceAdder.createDialog();
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mResourceAdder.destroy();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    String str1 = paramBundle.getString("uri4camera");
    if (str1 != null)
      this.uri4camera = Uri.parse((String)str1);
    String str2 = paramBundle.getString("uri4view");
    if (str2 != null)
      this.uri4view = Uri.parse((String)str2);
    this.createOrEdit = paramBundle.getBoolean("create_or_edit", true);
  }

  public void onResume()
  {
    L.d(this, "OnResume called.");
    super.onResume();
    updateUI();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.uri4camera != null)
      paramBundle.putString("uri4camera", this.uri4camera.toString());
    if (this.uri4view != null)
      paramBundle.putString("uri4view", this.uri4view.toString());
    paramBundle.putBoolean("create_or_edit", this.createOrEdit);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    super.onUpdate(paramInt, paramBaseData, paramBoolean);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.resource.ImageViewActivity
 * JD-Core Version:    0.5.4
 */