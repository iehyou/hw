package com.youdao.note.activity.resource;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.ui.skitch.doodle.DoodleMeta;
import com.youdao.note.ui.skitch.doodle.DoodleView;
import com.youdao.note.ui.skitch.doodle.IDoodle;
import com.youdao.note.ui.skitch.tool.ColorBoxView;
import com.youdao.note.ui.skitch.tool.PaintSliderView;

public class DoodleActivity extends BaseResourceNoteActivity
{
  private static final int DIALOG_BACK = 2;
  private static final int DIALOG_TRASH = 1;
  private DoodleMeta doodleMeta;
  private IDoodle mDoodleView = null;
  private DoodleMeta oldDoodleMeta;

  private void giveupDoodle()
  {
    YNoteApplication localYNoteApplication = YNoteApplication.getInstance();
    if (this.oldDoodleMeta != null)
      localYNoteApplication.getSkitchManager().addOrUpdateSkitchMeta(this.resourceMeta.getResourceId(), this.oldDoodleMeta);
    cancleAndBack();
  }

  private void initViewListener()
  {
    findViewById(2131165213).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        DoodleActivity.this.mDoodleView.undo();
      }
    });
    findViewById(2131165216).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if ((DoodleActivity.this.doodleMeta == null) || (DoodleActivity.this.doodleMeta.isEmpty()))
          return;
        DoodleActivity.this.showDialog(1);
      }
    });
    findViewById(2131165214).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ColorBoxView localColorBoxView = (ColorBoxView)DoodleActivity.this.findViewById(2131165211);
        PaintSliderView localPaintSliderView = (PaintSliderView)DoodleActivity.this.findViewById(2131165210);
        if (localPaintSliderView.getVisibility() == 0)
        {
          localPaintSliderView.dismiss();
          return;
        }
        if (localColorBoxView.getVisibility() == 0)
          localColorBoxView.dismiss();
        localPaintSliderView.show();
      }
    });
    findViewById(2131165215).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        ColorBoxView localColorBoxView = (ColorBoxView)DoodleActivity.this.findViewById(2131165211);
        PaintSliderView localPaintSliderView = (PaintSliderView)DoodleActivity.this.findViewById(2131165210);
        if (localColorBoxView.getVisibility() == 0)
        {
          localColorBoxView.dismiss();
          return;
        }
        if (localPaintSliderView.getVisibility() == 0)
          localPaintSliderView.dismiss();
        localColorBoxView.show();
      }
    });
    findViewById(2131165218).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        Bitmap localBitmap = DoodleActivity.this.mDoodleView.getBitmap();
        DoodleActivity.this.saveDoodleResource(localBitmap);
        localBitmap.recycle();
        YNoteApplication.getInstance().getSkitchManager().addOrUpdateSkitchMeta(DoodleActivity.this.resourceMeta.getResourceId(), DoodleActivity.this.doodleMeta);
        DoodleActivity.this.finishAndBack();
      }
    });
  }

  public void onBackPressed()
  {
    showDialog(2);
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    super.onCreate(paramBundle);
    setContentView(2130903048);
    this.mDoodleView = ((DoodleView)findViewById(2131165209));
    Intent localIntent = getIntent();
    YNoteApplication localYNoteApplication = YNoteApplication.getInstance();
    if (localIntent.getAction().equals("com.youdao.note.action.EDIT_DOODLE"))
    {
      this.doodleMeta = ((DoodleMeta)localYNoteApplication.getSkitchManager().getSkitchMeta(this.resourceMeta.getResourceId()));
      this.oldDoodleMeta = this.doodleMeta.copy();
      this.doodleMeta.setIsNew(false);
    }
    while (true)
    {
      this.mDoodleView.initSkitchMeta(this.doodleMeta);
      PaintSliderView localPaintSliderView = (PaintSliderView)findViewById(2131165210);
      localPaintSliderView.setSkitchCanvas(this.mDoodleView);
      localPaintSliderView.setVisibility(8);
      ColorBoxView localColorBoxView = (ColorBoxView)findViewById(2131165211);
      localColorBoxView.setSkitchCanvas(this.mDoodleView);
      localColorBoxView.setVisibility(8);
      initViewListener();
      return;
      this.doodleMeta = new DoodleMeta();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
      return new AlertDialog.Builder(this).setTitle(2131362002).setMessage(2131362003).setCancelable(true).setPositiveButton(2131362004, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          DoodleActivity.this.mDoodleView.trash();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          DoodleActivity.this.dismissDialog(1);
        }
      }).create();
    case 2:
    }
    return new AlertDialog.Builder(this).setTitle(2131362005).setCancelable(true).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        DoodleActivity.this.giveupDoodle();
      }
    }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        DoodleActivity.this.dismissDialog(2);
      }
    }).create();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.resource.DoodleActivity
 * JD-Core Version:    0.5.4
 */