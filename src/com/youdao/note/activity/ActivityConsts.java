package com.youdao.note.activity;

public abstract interface ActivityConsts
{
  public static abstract interface ACTION
  {
    public static final String ACCESS_DENIED = "com.youdao.note.action.ACCESS_DENIED";
    public static final String CHECK_UPDATE = "com.youdao.note.action.CHECK_UPDATE";
    public static final String CORRELATION_CONFIRM = "com.youdao.note.action.CORRELATION_CONFIRM";
    public static final String CREATE_AUDIO = "com.youdao.note.action.CREATE_AUDIO";
    public static final String CREATE_GALLARY = "com.youdao.note.action.CREATE_GALLARY";
    public static final String CREATE_HANDWRITE = "com.youdao.note.action.CREATE_HANDWRITE";
    public static final String CREATE_PREFIX = "com.youdao.note.action.CREATE_";
    public static final String CREATE_RECORD = "com.youdao.note.action.CREATE_RECORD";
    public static final String CREATE_RESOURCE = "com.youdao.note.action.CREATE_RESOURCE";
    public static final String CREATE_SNAPSHOT = "com.youdao.note.action.CREATE_SNAPSHOT";
    public static final String CREATE_TEXT = "com.youdao.note.action.CREATE_TEXT";
    public static final String DOODLE = "com.youdao.note.action.DOODLE";
    public static final String EDIT_DOODLE = "com.youdao.note.action.EDIT_DOODLE";
    public static final String EDIT_HANDWRITE = "com.youdao.note.action.EDIT_HANDWRITE";
    public static final String FINISH = "com.youdao.note.action.FINISH";
    public static final String LOGIN = "com.youdao.note.action.login";
    public static final String NEW_HANDWRITE = "com.youdao.note.action.NEW_HANDWRITE";
    public static final String PREFIX = "com.youdao.note.action.";
    public static final String RECORD_PAUSE = "com.youdao.note.action.RECORD_PAUSE";
    public static final String RECORD_RECORD = "com.youdao.note.action.RECORD_RECORD";
    public static final String RECORD_STOP = "com.youdao.note.action.RECORD_STOP";
    public static final String SETUP_PINLOCK = "com.youdao.note.action.SETUP_PINLOCK";
    public static final String UPDATE_PINLOCK = "com.youdao.note.action.UPDATE_PINLOCK";
    public static final String VIEW_RESOURCE = "com.youdao.note.action.VIEW_RESOURCE";
  }

  public static abstract interface INTENT_EXTRA
  {
    public static final String ENTRY_FROM = "entry_from";
    public static final String EXTRA_EMAILS = "emails";
    public static final String EXTRA_LIST_TYPE = "listType";
    public static final String EXTRA_LOGIN_ERROR = "loginError";
    public static final String EXTRA_NOTE = "note";
    public static final String EXTRA_NOTEBOOK = "noteBook";
    public static final String EXTRA_NOTEMETA = "noteMeta";
    public static final String EXTRA_RESOURCE_META = "resourceMeta";
    public static final String EXTRA_RESOURCE_META_LIST = "resourceMetaList";
    public static final String EXTRA_TITLE = "title";
    public static final String NOTEID = "noteid";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String WEB_CONTENT = "web_conetnt";
  }

  public static abstract interface LIST_TYPE
  {
    public static final int NOTE_BOOK = 1;
    public static final int NOTE_META = 2;
  }

  public static abstract interface REQUEST_CODE
  {
    public static final int CLEAR_PINLOCK = 17;
    public static final int DELETE = 1;
    public static final int DOODLE = 12;
    public static final int EDIT = 2;
    public static final int EDIT_DOODLE = 20;
    public static final int EDIT_HANDWRITE = 19;
    public static final int HANDWRITING = 13;
    public static final int IMAGE_RECTIFICATION = 11;
    public static final int LOGIN = 3;
    public static final int NOTEBOOK = 4;
    public static final int NOTEMETA = 10;
    public static final int PICKPHOTO = 6;
    public static final int REGIST = 9;
    public static final int SETUP_PINLOCK = 15;
    public static final int SNAPSHOT = 5;
    public static final int UNLOCK = 14;
    public static final int UPDATE_PINLOCK = 16;
    public static final int VIEW_DETAIL = 0;
    public static final int VIEW_IMAGE = 8;
    public static final int VIEW_RESOURCE = 18;
    public static final int VIEW_RESOURCE_LIST = 7;
  }

  public static abstract interface RESULT_CODE
  {
    public static final int DELETE = 1;
    public static final int UNLOCK_FAILED = 2;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.ActivityConsts
 * JD-Core Version:    0.5.4
 */