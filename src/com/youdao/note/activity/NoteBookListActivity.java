package com.youdao.note.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.delegate.SyncbarDelegate;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.DataFactory;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.adapter.NoteBookListAdapter;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;

public class NoteBookListActivity extends BaseListFileActivity
{
  private static final String BUNDLE_LONGCLICKED_NOTEBOOKID = "longclicked_notebook";
  private static final int MENU_DELETENOTEBOOK = 1;
  private static final int MENU_RENAMENOTEBOOK;
  private boolean fromRenameDialog;
  private String mLongclickedNotebookId = null;
  private EditText newBookEditText;
  private String newBookText;
  private View newBookView;
  private String renameBookText;
  private View renameBookView;
  private EditText renameEditText;
  private NoteBook selectedNoteBook;
  private boolean showing_DIALOG_NOTEBOOK_EXIST = false;
  private SyncbarDelegate syncbarDelegate;

  private boolean checkNotebookNameExist(String paramString)
  {
    return checkNotebookNameExist(paramString, null);
  }

  private boolean checkNotebookNameExist(String paramString1, String paramString2)
  {
    NoteBook localNoteBook = this.mDataSource.getNoteBookMetaByTitle(paramString1);
    if ((localNoteBook != null) && (!localNoteBook.getNoteBookId().equals(paramString2)))
    {
      showDialog(113);
      return true;
    }
    return false;
  }

  private NoteBook getSelectedNoteMeta()
  {
    return this.mDataSource.getNoteBookMetaById(this.mLongclickedNotebookId);
  }

  private void update()
  {
    if (!this.mYNote.isLogin())
    {
      findViewById(16908298).setVisibility(8);
      L.d(this, "set require login layout visiable");
      findViewById(2131165350).setVisibility(0);
      return;
    }
    setListAdapter(new NoteBookListAdapter(this, this.mDataSource.listAllNoteBooksAsList()));
    findViewById(2131165350).setVisibility(8);
    this.mListView.setSelection(this.mListPost);
  }

  protected void initComponent()
  {
    super.initComponent();
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903098);
    this.renameBookView = LayoutInflater.from(this).inflate(2130903088, null);
    this.renameEditText = ((EditText)this.renameBookView.findViewById(2131165317));
    this.newBookView = LayoutInflater.from(this).inflate(2130903088, null);
    this.newBookEditText = ((EditText)this.newBookView.findViewById(2131165317));
    this.syncbarDelegate = new SyncbarDelegate(this);
    super.onCreate(paramBundle);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 114:
    default:
      return super.onCreateDialog(paramInt);
    case 110:
      NoteBook localNoteBook2 = this.mDataSource.getNoteBookMetaById(this.mLongclickedNotebookId);
      if (this.mYNote.getDefaultNoteBook().equals(localNoteBook2.getNoteBookId()));
      for (int i = 2131099658; ; i = 2131099657)
        return new AlertDialog.Builder(this).setTitle(localNoteBook2.getTitle()).setItems(i, new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramDialogInterface, int paramInt)
          {
            switch (paramInt)
            {
            default:
              return;
            case 1:
              NoteBookListActivity.this.showDialog(115);
              return;
            case 0:
            }
            NoteBookListActivity.access$002(NoteBookListActivity.this, NoteBookListActivity.this.mDataSource.getNoteBookMetaById(NoteBookListActivity.this.mLongclickedNotebookId));
            NoteBookListActivity.access$202(NoteBookListActivity.this, NoteBookListActivity.this.selectedNoteBook.getTitle());
            NoteBookListActivity.this.showDialog(111);
          }
        }).create();
    case 115:
      NoteBook localNoteBook1 = this.mDataSource.getNoteBookMetaById(this.mLongclickedNotebookId);
      return new AlertDialog.Builder(this).setTitle(getResources().getString(2131361868).replace("${name}", localNoteBook1.getTitle())).setMessage(getResources().getString(2131361869).replace("${name}", localNoteBook1.getTitle())).setIcon(17301543).setCancelable(true).setPositiveButton(2131361958, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          NoteBookListActivity.this.mDataSource.markDeleteNotebook(NoteBookListActivity.this.getSelectedNoteMeta());
          NoteBookListActivity.this.update();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          NoteBookListActivity.this.dismissDialog(115);
        }
      }).create();
    case 111:
      return new AlertDialog.Builder(this).setTitle(2131361870).setView(this.renameBookView).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          NoteBookListActivity.access$202(NoteBookListActivity.this, NoteBookListActivity.this.renameEditText.getText().toString().trim());
          if (TextUtils.isEmpty(NoteBookListActivity.this.renameBookText))
            UIUtilities.showToast(NoteBookListActivity.this, 2131361812);
          do
            return;
          while (NoteBookListActivity.this.checkNotebookNameExist(NoteBookListActivity.this.renameBookText, NoteBookListActivity.this.selectedNoteBook.getNoteBookId()) != 0);
          NoteBookListActivity.this.selectedNoteBook.setTitle(NoteBookListActivity.this.renameEditText.getText().toString());
          NoteBookListActivity.this.selectedNoteBook.setDirty(true);
          NoteBookListActivity.this.mDataSource.insertOrUpdateNoteBookMeta(NoteBookListActivity.this.selectedNoteBook);
          NoteBookListActivity.this.update();
        }
      }).setNegativeButton(2131361962, null).create();
    case 112:
      return new AlertDialog.Builder(this).setTitle(2131361867).setView(this.newBookView).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          NoteBookListActivity.access$702(NoteBookListActivity.this, NoteBookListActivity.this.newBookEditText.getText().toString().trim());
          if (TextUtils.isEmpty(NoteBookListActivity.this.newBookText))
            UIUtilities.showToast(NoteBookListActivity.this, 2131361812);
          do
            return;
          while (NoteBookListActivity.this.checkNotebookNameExist(NoteBookListActivity.this.newBookText) != 0);
          NoteBook localNoteBook = DataFactory.newNotebookMeta(NoteBookListActivity.this.newBookText);
          NoteBookListActivity.this.mDataSource.insertOrUpdateNoteBookMeta(localNoteBook);
          NoteBookListActivity.this.update();
        }
      }).setNegativeButton(2131361962, null).create();
    case 113:
    }
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).setIcon(17301543).setTitle(2131361906).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        NoteBookListActivity.access$1002(NoteBookListActivity.this, false);
        NoteBookListActivity.this.dismissDialog(113);
        if (NoteBookListActivity.this.fromRenameDialog)
        {
          NoteBookListActivity.this.showDialog(111);
          return;
        }
        NoteBookListActivity.this.showDialog(112);
      }
    }).create();
    localAlertDialog.setOnKeyListener(new DialogInterface.OnKeyListener()
    {
      public boolean onKey(DialogInterface paramDialogInterface, int paramInt, KeyEvent paramKeyEvent)
      {
        if ((paramInt == 4) && (NoteBookListActivity.this.showing_DIALOG_NOTEBOOK_EXIST))
        {
          NoteBookListActivity.this.dismissDialog(113);
          if (NoteBookListActivity.this.fromRenameDialog)
            NoteBookListActivity.this.showDialog(111);
          while (true)
          {
            return true;
            NoteBookListActivity.this.showDialog(112);
          }
        }
        return false;
      }
    });
    return localAlertDialog;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    new MenuInflater(this).inflate(2131034115, paramMenu);
    return true;
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    L.d(this, "on item click called.");
    NoteBook localNoteBook = (NoteBook)this.mAdapter.getItem(paramInt);
    Intent localIntent = new Intent(this, NoteListActivity.class);
    localIntent.putExtra("noteBook", localNoteBook.getNoteBookId());
    startActivityForResult(localIntent, 4);
  }

  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.mLongclickedNotebookId = ((NoteBook)this.mAdapter.getItem(paramInt)).getNoteBookId();
    removeDialog(110);
    showDialog(110);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.syncbarDelegate.onOptionsItemSelected(paramMenuItem))
      return true;
    if (paramMenuItem.getItemId() == 2131165376)
    {
      this.newBookText = "";
      showDialog(112);
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    super.onPrepareDialog(paramInt, paramDialog);
    switch (paramInt)
    {
    case 114:
    default:
      return;
    case 111:
      this.fromRenameDialog = true;
      this.renameEditText.setText(this.renameBookText);
      this.renameEditText.selectAll();
      return;
    case 112:
      this.fromRenameDialog = false;
      this.newBookEditText.setText(this.newBookText);
      return;
    case 113:
      this.showing_DIALOG_NOTEBOOK_EXIST = true;
      return;
    case 115:
    }
    NoteBook localNoteBook = this.mDataSource.getNoteBookMetaById(this.mLongclickedNotebookId);
    AlertDialog localAlertDialog = (AlertDialog)paramDialog;
    localAlertDialog.setTitle(getResources().getString(2131361868).replace("${name}", localNoteBook.getTitle()));
    localAlertDialog.setMessage(getResources().getString(2131361869).replace("${name}", localNoteBook.getTitle()));
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.syncbarDelegate.onPrepareOptionsMenu(paramMenu);
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    if (paramBundle.containsKey("longclicked_notebook"))
      this.mLongclickedNotebookId = paramBundle.getString("longclicked_notebook");
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    this.syncbarDelegate.onResume();
    super.onResume();
    L.d(this, "on Resume called.");
    update();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.mLongclickedNotebookId != null)
      paramBundle.putString("longclicked_notebook", this.mLongclickedNotebookId);
    super.onSaveInstanceState(paramBundle);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    this.syncbarDelegate.onUpdate(paramInt, paramBaseData, paramBoolean);
    switch (paramInt)
    {
    default:
    case 6:
    }
    while (true)
    {
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
      return;
      if (!paramBoolean)
        continue;
      update();
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.NoteBookListActivity
 * JD-Core Version:    0.5.4
 */