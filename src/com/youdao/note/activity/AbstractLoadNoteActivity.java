package com.youdao.note.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.RemoteErrorData;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.DownloadResourceException;
import com.youdao.note.task.TaskManager;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import java.io.IOException;

public abstract class AbstractLoadNoteActivity extends LockableActivity
{
  private static final String BUNDLE_NOTE_ID = "bundle_noteId";
  protected static final int DIALOG_LOADING = 201;
  protected boolean loadDismissed = true;
  protected Note mNote = null;
  private String mNoteId = null;
  protected TextView mTitleView = null;
  protected WebView mWebView = null;

  protected void failedLoadNote(RemoteErrorData paramRemoteErrorData)
  {
    Exception localException = paramRemoteErrorData.getException();
    boolean bool = this.loadDismissed;
    if (!this.loadDismissed)
    {
      dismissDialog(201);
      this.loadDismissed = true;
    }
    if (localException instanceof IOException)
      UIUtilities.showToast(this, 2131361953);
    while (true)
    {
      if (!bool)
        finish();
      return;
      if (this instanceof BaseEditActivity)
        UIUtilities.showToast(this, 2131361845);
      UIUtilities.showToast(this, 2131361852);
    }
  }

  protected abstract void loadNote(Note paramNote);

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    View localView = findViewById(2131165232);
    if ((localView != null) && (localView instanceof WebView))
      this.mWebView = ((WebView)localView);
    this.mTitleView = ((TextView)findViewById(2131165321));
    this.loadDismissed = true;
    this.mNoteId = getIntent().getStringExtra("noteid");
    if (this.mNoteId == null)
      return;
    NoteMeta localNoteMeta = this.mDataSource.getNoteMetaById(this.mNoteId);
    L.d(this, "Enter NoteDetail " + localNoteMeta);
    String str = localNoteMeta.getTitle();
    this.mTitleView.setText(str);
    this.mNote = new Note(localNoteMeta, null);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 201:
    }
    ProgressDialog localProgressDialog = new ProgressDialog(this);
    localProgressDialog.setMessage(getResources().getText(2131361945));
    localProgressDialog.setIndeterminate(true);
    localProgressDialog.setCancelable(true);
    localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramDialogInterface)
      {
        AbstractLoadNoteActivity.this.mTaskManager.stopAll();
        AbstractLoadNoteActivity.this.mWebView.stopLoading();
        AbstractLoadNoteActivity.this.finish();
      }
    });
    return localProgressDialog;
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.mNoteId = paramBundle.getString("bundle_noteId");
  }

  public void onResume()
  {
    super.onResume();
    if (this.mNoteId != null)
    {
      Note localNote = this.mDataSource.getNote(this.mNote.getNoteMeta());
      if (localNote == null)
        break label36;
      loadNote(localNote);
    }
    return;
    label36: showLoadingDialog();
    this.mTaskManager.pullNote(this.mNote.getNoteMeta(), true);
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("bundle_noteId", this.mNoteId);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default:
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
    case 14:
    case 2:
    case 5:
    }
    label175: DownloadResourceException localDownloadResourceException;
    do
    {
      Thumbnail localThumbnail;
      do
      {
        do
        {
          do
            return;
          while ((paramBoolean) || (!showError()));
          failedLoadNote((RemoteErrorData)paramBaseData);
          return;
          if (!paramBoolean)
            continue;
          loadNote((Note)paramBaseData);
          return;
        }
        while (!showError());
        failedLoadNote((RemoteErrorData)paramBaseData);
        return;
        if (!paramBoolean)
          break label175;
        L.d(this, "Got notified of thumbnail.");
        localThumbnail = (Thumbnail)paramBaseData;
      }
      while (this.mWebView == null);
      this.mWebView.loadUrl("javascript:e=document.getElementById(\"" + localThumbnail.getImageMeta().getResourceId() + "\");e.src=\"" + this.mDataSource.getThumbnailPath(localThumbnail.getImageMeta()) + "\"");
      return;
      RemoteErrorData localRemoteErrorData = (RemoteErrorData)paramBaseData;
      boolean bool = localRemoteErrorData.getException() instanceof DownloadResourceException;
      localDownloadResourceException = null;
      if (!bool)
        continue;
      localDownloadResourceException = (DownloadResourceException)localRemoteErrorData.getException();
    }
    while ((localDownloadResourceException.getCause() == null) || (!localDownloadResourceException.getCause() instanceof IOException) || (!"No space left on device".equals(((IOException)localDownloadResourceException.getCause()).getMessage())));
    L.d(this, "No space left on device.");
    UIUtilities.showToast(this, 2131361944);
  }

  protected boolean showError()
  {
    return true;
  }

  protected void showLoadingDialog()
  {
    showDialog(201);
    this.loadDismissed = false;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.AbstractLoadNoteActivity
 * JD-Core Version:    0.5.4
 */