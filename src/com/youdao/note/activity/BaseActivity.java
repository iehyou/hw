package com.youdao.note.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.delegate.BaseDelegate;
import com.youdao.note.data.BaseData;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.SyncManager;
import com.youdao.note.task.TaskManager;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.L;

public class BaseActivity extends Activity
  implements ActivityConsts, View.OnClickListener, DialogInterface.OnClickListener, TaskManager.DataUpdateListener, Consts, DialogInterface.OnCancelListener
{
  protected static final int DIALOG_ABOUT = 108;
  public static final int DIALOG_CMWAP = 117;
  public static final int DIALOG_DELETE_CONFIRM = 100;
  protected static final int DIALOG_DELETE_NOTEBOOK = 115;
  public static final int DIALOG_DETAIL = 105;
  public static final int DIALOG_ITEM = 106;
  protected static final int DIALOG_LICENSE = 109;
  protected static final int DIALOG_LOADING_RESOURCE = 116;
  public static final int DIALOG_NEED_LOGIN = 107;
  protected static final int DIALOG_NEW_FEATURES = 114;
  protected static final int DIALOG_NEW_NOTEBOOK = 112;
  public static final int DIALOG_NOTEBOOKS = 104;
  protected static final int DIALOG_NOTEBOOK_EXIST = 113;
  protected static final int DIALOG_NOTEBOOK_MENU = 110;
  protected static final int DIALOG_RENAME_NOTEBOOK = 111;
  private BaseDelegate baseDelegate;
  protected DataSource mDataSource;
  protected LogRecorder mLogRecorder;
  protected SyncManager mSyncManager;
  protected TaskManager mTaskManager;
  protected YNoteApplication mYNote;

  private void sendToSearch()
  {
    EditText localEditText = (EditText)findViewById(2131165287);
    Intent localIntent = new Intent(this, SearchActivity.class);
    if (localEditText != null)
    {
      if (localEditText != null)
        localIntent.putExtra("query", localEditText.getText().toString());
      localIntent.putExtra("noteBook", "/");
    }
    startActivity(localIntent);
  }

  protected boolean checkLogin()
  {
    return this.baseDelegate.checkLogin();
  }

  protected void hideKeyboard(IBinder paramIBinder)
  {
    ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(paramIBinder, 0);
  }

  protected void initComponent()
  {
    setOnClickListener(findViewById(2131165311), new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        BaseActivity.this.sendNewNote();
      }
    });
    setOnClickListener(findViewById(2131165314), new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if (BaseActivity.this.getClass() == NoteListActivity.class)
          BaseActivity.this.finish();
        Intent localIntent = new Intent(BaseActivity.this, NoteBookListActivity.class);
        BaseActivity.this.startActivity(localIntent);
      }
    });
    setOnClickListener(findViewById(2131165313), new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if (BaseActivity.this.getClass() == NoteBookListActivity.class)
          BaseActivity.this.finish();
        Intent localIntent = new Intent(BaseActivity.this, NoteListActivity.class);
        BaseActivity.this.startActivity(localIntent);
      }
    });
    View localView1 = findViewById(2131165379);
    L.d(this, " " + localView1);
    setOnClickListener(localView1, new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        BaseActivity.this.sendToSearch();
      }
    });
    View localView2 = findViewById(2131165187);
    if (localView2 != null)
    {
      if (!(this instanceof MainActivity))
        break label139;
      localView2.setEnabled(false);
    }
    while (true)
    {
      setOnClickListener(findViewById(2131165286), new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          BaseActivity.this.sendLogin();
        }
      });
      return;
      label139: setOnClickListener(localView2, new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          BaseActivity.this.mYNote.sendMainActivity(BaseActivity.this, null);
        }
      });
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    case 3:
    }
    do
    {
      do
      {
        return;
        if (paramInt2 != -1)
          break label56;
        L.d(this, "login succeed.");
      }
      while (this instanceof MainActivity);
      this.mYNote.sendMainActivity(this, null);
      return;
      label56: if (paramInt2 != 0)
        break label81;
      L.d(this, "canceled.");
    }
    while (!this.mYNote.isEverLogin());
    finish();
    return;
    label81: L.d(this, "login failed.");
  }

  public void onCancel(DialogInterface paramDialogInterface)
  {
  }

  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramInt != 108)
      return;
    dismissDialog(108);
  }

  public void onClick(View paramView)
  {
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    ViewGroup localViewGroup = (ViewGroup)findViewById(2131165231);
    if (localViewGroup == null)
      return;
    localViewGroup.requestLayout();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mYNote = YNoteApplication.getInstance();
    this.mSyncManager = this.mYNote.getSyncManager();
    this.mTaskManager = this.mYNote.getTaskManager();
    this.mDataSource = this.mYNote.getDataSource();
    this.mLogRecorder = this.mYNote.getLogRecorder();
    this.baseDelegate = new BaseDelegate(this);
    initComponent();
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt, paramBundle);
    case 117:
    }
    return new AlertDialog.Builder(this).setTitle(2131362039).setMessage(2131362040).setPositiveButton(2131362041, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        Intent localIntent = new Intent("android.settings.APN_SETTINGS");
        BaseActivity.this.startActivity(localIntent);
      }
    }).setNegativeButton(2131362042, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        BaseActivity.this.dismissDialog(117);
      }
    }).create();
  }

  public void onDestroy()
  {
    super.onDestroy();
  }

  protected void onIntentSend(Intent paramIntent)
  {
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 2131165367)
    {
      Intent localIntent1 = new Intent(this, WebActivity.class);
      localIntent1.putExtra("web_conetnt", 2);
      startActivity(localIntent1);
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165368)
    {
      this.mYNote.logOut();
      this.mYNote.sendMainActivity(this, "com.youdao.note.action.login");
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165365)
    {
      startActivity(new Intent(this, SettingActivity.class));
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165370)
      showDialog(108);
    do
      return false;
    while (paramMenuItem.getItemId() != 2131165366);
    Intent localIntent2 = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
    String str1 = getResources().getString(2131361883);
    String str2 = getResources().getString(2131361882);
    localIntent2.setType("text/plain");
    localIntent2.putExtra("android.intent.extra.EMAIL", "");
    localIntent2.putExtra("android.intent.extra.SUBJECT", str2);
    localIntent2.putExtra("android.intent.extra.TEXT", str1);
    localIntent2.putExtra("web_conetnt", 2);
    startActivity(localIntent2);
    return true;
  }

  protected void onPause()
  {
    this.mTaskManager.unregistDataListener(this);
    this.mTaskManager.stopAll();
    super.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    MenuItem localMenuItem = paramMenu.findItem(2131165368);
    if (localMenuItem != null)
    {
      if (!this.mYNote.isLogin())
        break label46;
      localMenuItem.setIcon(2130837661);
      localMenuItem.setTitle(2131361935);
    }
    while (true)
    {
      return true;
      label46: localMenuItem.setTitle(2131361896);
      localMenuItem.setIcon(2130837653);
    }
  }

  protected void onResume()
  {
    this.mTaskManager.registDataListener(this);
    super.onResume();
  }

  public boolean onSearchRequested()
  {
    sendToSearch();
    return false;
  }

  public void onStart()
  {
    super.onStart();
    L.d(this, "onStart called.");
    if ((((this.mYNote == null) || (this.mYNote.isLogin()) || (!this.mYNote.isEverLogin()))) && (((!"com.youdao.note.action.login".equals(getIntent().getAction())) || (this instanceof LoginActivity))))
      return;
    Log.d("Lognin", super.getClass() + " ");
    sendLogin();
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
  }

  protected void sendLogin()
  {
    this.baseDelegate.sendLogin();
  }

  protected void sendNewNote()
  {
    sendNewNote("com.youdao.note.action.CREATE_TEXT");
  }

  protected void sendNewNote(String paramString)
  {
    Intent localIntent = new Intent(this, NewNoteActivity.class);
    localIntent.setAction(paramString);
    localIntent.putExtra("entry_from", "AppAddTimes");
    startActivityForResult(localIntent, 2);
  }

  protected void setOnClickListener(View paramView, View.OnClickListener paramOnClickListener)
  {
    if (paramView == null)
      return;
    paramView.setOnClickListener(paramOnClickListener);
  }

  public void startActivityForResult(Intent paramIntent, int paramInt)
  {
    onIntentSend(paramIntent);
    super.startActivityForResult(paramIntent, paramInt);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.BaseActivity
 * JD-Core Version:    0.5.4
 */