package com.youdao.note.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.resource.DoodleActivity;
import com.youdao.note.activity.resource.HandwritingActivity;
import com.youdao.note.activity.resource.ImageViewActivity;
import com.youdao.note.data.adapter.BaseListAdapter;
import com.youdao.note.data.resource.AbstractImageResource;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.ui.skitch.ISkitchMeta;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.ui.skitch.doodle.DoodleMeta;
import com.youdao.note.ui.skitch.handwrite.HandwriteMeta;
import com.youdao.note.utils.BitmapMemCache;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import com.youdao.note.utils.UnitUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResourceListActivity extends ListActivity
  implements ActivityConsts
{
  private static final String BUNDLE_DELETED_IDX = "delete_idx";
  private static final String BUNDLE_RESOURCE_META_LIST = "resource_list";
  private static final int DIALOG_DELETE_RESOURCE_CONFIRM = 1;
  private int mClickedResouceIdx = -1;
  protected DataSource mDataSource = null;
  protected YNoteApplication mYNote = null;
  private int resPos;
  private ArrayList<BaseResourceMeta> resourceMetaList = null;

  private void backEdiitActivityWithAudio(BaseResourceMeta paramBaseResourceMeta)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("resourceMeta", paramBaseResourceMeta);
    backEditActivity(localIntent);
  }

  private void backEditActivity()
  {
    backEditActivity(new Intent());
  }

  private void backEditActivity(Intent paramIntent)
  {
    paramIntent.putExtra("resourceMetaList", this.resourceMetaList);
    L.d(this, "ready to back");
    Iterator localIterator = this.resourceMetaList.iterator();
    while (localIterator.hasNext())
      L.d(this, ((BaseResourceMeta)localIterator.next()).getResourceId());
    setResult(-1, paramIntent);
    finish();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 8)
      if ((paramInt2 == -1) && (paramIntent != null))
      {
        BaseResourceMeta localBaseResourceMeta3 = (BaseResourceMeta)paramIntent.getSerializableExtra("resourceMeta");
        AbstractImageResource localAbstractImageResource2 = (AbstractImageResource)this.mDataSource.getResource(localBaseResourceMeta3);
        byte[] arrayOfByte = localAbstractImageResource2.getContentBytes();
        Bitmap localBitmap = BitmapFactory.decodeByteArray(arrayOfByte, 0, arrayOfByte.length);
        ImageUtils.persistImage(localAbstractImageResource2, localBitmap, this.mYNote.getImageQuality(), true);
        localBitmap.recycle();
        this.resourceMetaList.set(this.resPos, localBaseResourceMeta3);
        onContentChanged();
      }
    do
    {
      do
        return;
      while (paramInt2 != 1);
      BaseResourceMeta localBaseResourceMeta2 = (BaseResourceMeta)this.resourceMetaList.get(this.resPos);
      this.resourceMetaList.remove(this.resPos);
      L.d(this, "to be removed " + localBaseResourceMeta2.getResourceId());
      this.mDataSource.deleteResource(localBaseResourceMeta2);
      if (this.resourceMetaList.size() == 0)
      {
        backEditActivity();
        return;
      }
      onContentChanged();
      return;
    }
    while (((paramInt1 != 19) && (paramInt1 != 20)) || (paramInt2 != -1) || (paramIntent == null));
    BaseResourceMeta localBaseResourceMeta1 = (BaseResourceMeta)paramIntent.getSerializableExtra("resourceMeta");
    AbstractImageResource localAbstractImageResource1 = (AbstractImageResource)this.mDataSource.getResource(localBaseResourceMeta1);
    this.resourceMetaList.set(this.resPos, localAbstractImageResource1.getMeta());
    onContentChanged();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    this.mYNote = YNoteApplication.getInstance();
    this.mDataSource = this.mYNote.getDataSource();
    setContentView(2130903108);
    this.resourceMetaList = ((ArrayList)getIntent().getSerializableExtra("resourceMetaList"));
    if (this.resourceMetaList.size() == 0)
      finish();
    L.d(this, "Got resource " + this.resourceMetaList.size());
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
    }
    return new AlertDialog.Builder(this).setTitle(2131361846).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramDialogInterface, int paramInt)
      {
        BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)ResourceListActivity.this.resourceMetaList.get(ResourceListActivity.this.mClickedResouceIdx);
        ResourceListActivity.this.resourceMetaList.remove(ResourceListActivity.this.mClickedResouceIdx);
        L.d(ResourceListActivity.this, "to be removed " + localBaseResourceMeta.getResourceId());
        ResourceListActivity.this.mDataSource.deleteResource(localBaseResourceMeta);
        if (ResourceListActivity.this.resourceMetaList.size() == 0)
        {
          ResourceListActivity.this.backEditActivity();
          return;
        }
        ResourceListActivity.this.onContentChanged();
      }
    }).setNegativeButton(2131361962, null).create();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      backEditActivity();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    this.resPos = paramInt;
    BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)this.resourceMetaList.get(this.resPos);
    ISkitchMeta localISkitchMeta = YNoteApplication.getInstance().getSkitchManager().getSkitchMeta(localBaseResourceMeta.getResourceId());
    if (localISkitchMeta == null)
      if (localBaseResourceMeta.getType() == 0)
      {
        Intent localIntent3 = new Intent(this, ImageViewActivity.class);
        localIntent3.setAction("com.youdao.note.action.VIEW_RESOURCE");
        localIntent3.putExtra("resourceMeta", localBaseResourceMeta);
        startActivityForResult(localIntent3, 8);
      }
    do
    {
      return;
      if (FileUtils.isPlayable(localBaseResourceMeta.getFileName()))
      {
        backEdiitActivityWithAudio(localBaseResourceMeta);
        return;
      }
      Uri localUri = Uri.fromFile(new File(this.mDataSource.getResourcePath(localBaseResourceMeta)));
      Intent localIntent4 = new Intent();
      localIntent4.setAction("android.intent.action.VIEW");
      localIntent4.setDataAndType(localUri, MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(localUri.toString())));
      try
      {
        startActivity(localIntent4);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        UIUtilities.showToast(this, 2131361923);
        return;
      }
      if (!localISkitchMeta instanceof HandwriteMeta)
        continue;
      Intent localIntent1 = new Intent(this, HandwritingActivity.class);
      localIntent1.setAction("com.youdao.note.action.EDIT_HANDWRITE");
      localIntent1.putExtra("resourceMeta", localBaseResourceMeta);
      startActivityForResult(localIntent1, 19);
      return;
    }
    while (!localISkitchMeta instanceof DoodleMeta);
    Intent localIntent2 = new Intent(this, DoodleActivity.class);
    localIntent2.setAction("com.youdao.note.action.EDIT_DOODLE");
    localIntent2.putExtra("resourceMeta", localBaseResourceMeta);
    startActivityForResult(localIntent2, 20);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    this.mClickedResouceIdx = paramBundle.getInt("delete_idx");
    this.resourceMetaList = ((ArrayList)paramBundle.getSerializable("resource_list"));
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    setListAdapter(new ResourceListAdapter(this, 2130903107, this.resourceMetaList));
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putInt("delete_idx", this.mClickedResouceIdx);
    paramBundle.putSerializable("resource_list", this.resourceMetaList);
    super.onSaveInstanceState(paramBundle);
  }

  class ResourceListAdapter extends BaseListAdapter<BaseResourceMeta>
  {
    private DataSource mDataSource = null;

    public ResourceListAdapter(int paramList, List<BaseResourceMeta> arg3)
    {
      super(paramList, i, localList);
    }

    private void fillView(int paramInt, View paramView)
    {
      BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)getItem(paramInt);
      ImageView localImageView = (ImageView)paramView.findViewById(2131165352);
      ((TextView)paramView.findViewById(2131165353)).setText(UnitUtils.getSize(localBaseResourceMeta.getLength()));
      TextView localTextView = (TextView)paramView.findViewById(2131165354);
      if (ResourceUtils.hasThumbnail(localBaseResourceMeta))
      {
        localImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        localImageView.setImageBitmap(BitmapMemCache.getInstance().getBitmap(this.mDataSource.getThumbnailPath(localBaseResourceMeta), localImageView.getWidth(), localImageView.getHeight()));
        AbstractImageResourceMeta localAbstractImageResourceMeta = (AbstractImageResourceMeta)localBaseResourceMeta;
        localTextView.setText(localAbstractImageResourceMeta.getWidth() + "×" + localAbstractImageResourceMeta.getHeight());
      }
      while (true)
      {
        paramView.findViewById(2131165268).setOnClickListener(new View.OnClickListener(paramInt)
        {
          public void onClick(View paramView)
          {
            ResourceListActivity.access$002(ResourceListActivity.this, this.val$position);
            ResourceListActivity.this.showDialog(1);
          }
        });
        return;
        localImageView.setImageResource(FileUtils.getIconResouceId(localBaseResourceMeta.getFileName()));
        localImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        localTextView.setText(localBaseResourceMeta.getFileName());
      }
    }

    protected View createViewFromResource(int paramInt1, ViewGroup paramViewGroup, int paramInt2)
    {
      View localView = this.mInflater.inflate(paramInt2, null);
      fillView(paramInt1, localView);
      return localView;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView != null)
      {
        fillView(paramInt, paramView);
        return paramView;
      }
      return createViewFromResource(paramInt, paramViewGroup, this.mResourceId);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.ResourceListActivity
 * JD-Core Version:    0.5.4
 */