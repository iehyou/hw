package com.youdao.note.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.LoginResult;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.RemoteErrorData;
import com.youdao.note.data.UserMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.LoginException;
import com.youdao.note.task.TaskManager;
import com.youdao.note.ui.DialogFactory;
import com.youdao.note.ui.ResizeableLayout;
import com.youdao.note.ui.ResizeableLayout.SizeChangeListener;
import com.youdao.note.utils.L;
import com.youdao.note.utils.MD5Digester;
import com.youdao.note.utils.UIUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class LoginActivity extends BaseActivity
  implements TextView.OnEditorActionListener
{
  private static final String BUNDLE_OFFLIE_RESOURCES = "offline_resources";
  private static final String BUNDLE_OFFLINE_NOTES = "offline_notes";
  private static final int LOGINING_DIALOG = 1;
  private static final String TAG = LoginActivity.class.getName();
  private EditText account = null;
  private Button mHelpButton = null;
  private ResizeableLayout mLayout = null;
  private Button mLoginButton = null;
  private View mLoginLogo = null;
  private LoginResult mLoginResult = null;
  private Button mRegistButton = null;
  private View mRegistHelpView = null;
  private Note[] offlineNotes = new Note[0];
  private EditText password = null;
  private ArrayList<ArrayList<BaseResourceMeta>> resourceLists = new ArrayList(0);

  private void clearLoginStatuse()
  {
    this.mYNote.setPersistCookie("");
    this.mYNote.setUserName("");
    this.mYNote.setPassword("");
  }

  private boolean isValidUserName(String paramString)
  {
    if (TextUtils.isEmpty(paramString));
    do
      return false;
    while (paramString.contains(" "));
    return true;
  }

  private void login()
  {
    String str1 = this.account.getText().toString();
    String str2 = this.password.getText().toString();
    if ((TextUtils.isEmpty(str1)) || (TextUtils.isEmpty(str2)))
    {
      UIUtilities.showToast(this, 2131361826);
      return;
    }
    startLogin(str1, MD5Digester.digest(str2));
  }

  private void loginFailed(BaseData paramBaseData)
  {
    Exception localException = ((RemoteErrorData)paramBaseData).getException();
    if (localException instanceof LoginException)
    {
      LoginException localLoginException = (LoginException)localException;
      clearLoginStatuse();
      if (localLoginException.getLoginResultCode() == 460)
      {
        this.password.requestFocus();
        this.password.selectAll();
        UIUtilities.showToast(this, 2131361982);
      }
      do
      {
        return;
        if (localLoginException.getLoginResultCode() != 420)
          continue;
        this.account.requestFocus();
        UIUtilities.showToast(this, 2131361983);
        return;
      }
      while (localLoginException.getLoginResultCode() != 412);
      UIUtilities.showToast(this, 2131361813);
      return;
    }
    UIUtilities.showToast(getApplicationContext(), 2131361795);
  }

  private void startLogin(String paramString1, String paramString2)
  {
    if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)));
    String str2;
    String str3;
    do
    {
      return;
      String str1 = paramString1.trim().toLowerCase();
      if (!str1.contains("@"))
        str1 = str1 + "@163.com";
      str2 = str1.trim();
      str3 = paramString2.trim();
      L.d(this, "password is " + paramString2);
      if (isValidUserName(str2))
        continue;
      UIUtilities.showToast(this, 2131361850);
      return;
    }
    while (TextUtils.isEmpty(str3));
    if (this.account.isFocused())
      hideKeyboard(this.account.getWindowToken());
    while (true)
    {
      this.mTaskManager.login(str2, str3);
      showDialog(1);
      return;
      if (!this.password.isFocused())
        continue;
      hideKeyboard(this.password.getWindowToken());
    }
  }

  public void bindEvents()
  {
    this.mLoginButton.setOnClickListener(this);
    this.mRegistButton.setOnClickListener(this);
    this.mHelpButton.setOnClickListener(this);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 9:
    }
    do
    {
      return;
      L.d(this, "Regist succeed.");
    }
    while (paramIntent == null);
    startLogin(paramIntent.getStringExtra("username"), paramIntent.getStringExtra("password"));
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165280)
      login();
    do
    {
      return;
      if (paramView.getId() != 2131165283)
        continue;
      Intent localIntent1 = new Intent(this, WebActivity.class);
      localIntent1.putExtra("web_conetnt", 3);
      startActivityForResult(localIntent1, 9);
      return;
    }
    while (paramView.getId() != 2131165284);
    Intent localIntent2 = new Intent(this, WebActivity.class);
    localIntent2.putExtra("web_conetnt", 1);
    startActivity(localIntent2);
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903077);
    super.onCreate(paramBundle);
    this.mLoginButton = ((Button)findViewById(2131165280));
    this.account = ((EditText)findViewById(2131165278));
    if (this.mYNote.getLastLoginUserName() != null)
      this.account.setText(this.mYNote.getLastLoginUserName());
    this.account.setOnClickListener(this);
    this.password = ((EditText)findViewById(2131165279));
    this.password.setOnClickListener(this);
    this.password.setOnEditorActionListener(this);
    this.mRegistButton = ((Button)findViewById(2131165283));
    this.mHelpButton = ((Button)findViewById(2131165284));
    this.mLoginLogo = findViewById(2131165281);
    this.mRegistHelpView = findViewById(2131165282);
    View localView1 = findViewById(2131165285);
    View localView2 = findViewById(2131165276);
    if (localView2 != null)
    {
      this.mLayout = ((ResizeableLayout)localView2);
      this.mLayout.setSizeChangeListener(new ResizeableLayout.SizeChangeListener(localView1)
      {
        public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
          L.d(this, "on size changed called." + paramInt2 + " " + paramInt4);
          if (paramInt2 < paramInt4)
          {
            LoginActivity.this.mLoginLogo.setVisibility(8);
            LoginActivity.this.mRegistHelpView.setVisibility(8);
            this.val$mCopyRight.setVisibility(8);
          }
          do
            return;
          while (paramInt2 - paramInt4 <= 0.3D * paramInt4);
          UIUtilities.makeViewVisiable(LoginActivity.this.mLoginLogo);
          UIUtilities.makeViewVisiable(LoginActivity.this.mRegistHelpView);
          UIUtilities.makeViewVisiable(this.val$mCopyRight);
        }
      });
    }
    bindEvents();
    this.mYNote = YNoteApplication.getInstance();
    this.mTaskManager = this.mYNote.getTaskManager();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
    }
    return DialogFactory.getCancleableProgressDialog(null, 2131361895, this, 0, new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramDialogInterface)
      {
        LoginActivity.this.mTaskManager.stopAll();
        LoginActivity.this.clearLoginStatuse();
      }
    });
  }

  public void onDestroy()
  {
    super.onDestroy();
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    L.d(this, "actionId is " + paramInt);
    if (paramInt == 1)
    {
      login();
      return true;
    }
    return false;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (4 == paramInt)
    {
      Log.d(TAG, "key code " + paramInt + " found. ");
      setResult(0, new Intent());
      finish();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    if (paramBundle.getSerializable("offline_notes") != null)
      this.offlineNotes = ((Note[])(Note[])paramBundle.getSerializable("offline_notes"));
    if (paramBundle.getSerializable("offline_resources") != null)
      this.resourceLists = ((ArrayList)paramBundle.getSerializable("offline_resources"));
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    Log.d(TAG, "onResume called.");
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putSerializable("offline_notes", this.offlineNotes);
    paramBundle.putSerializable("offline_resources", this.resourceLists);
    super.onSaveInstanceState(paramBundle);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    switch (paramInt)
    {
    case 12:
    default:
      return;
    case 11:
      if (paramBoolean)
      {
        this.mLoginResult = ((LoginResult)paramBaseData);
        if (!this.mYNote.isEverLogin())
        {
          NoteMeta[] arrayOfNoteMeta = this.mDataSource.listAllNotesAsArray();
          this.offlineNotes = new Note[arrayOfNoteMeta.length];
          this.resourceLists = new ArrayList(arrayOfNoteMeta.length);
          for (int i1 = 0; i1 < arrayOfNoteMeta.length; ++i1)
          {
            this.offlineNotes[i1] = this.mDataSource.getNote(arrayOfNoteMeta[i1]);
            this.resourceLists.add(this.mDataSource.getResourceMetasByNoteId(arrayOfNoteMeta[i1].getNoteId()));
          }
          L.d(this, "Found " + arrayOfNoteMeta.length + " offline notes.");
        }
        this.mYNote.setPersistCookie(this.mLoginResult.getPersistCookie());
        this.mYNote.setUserName(this.mLoginResult.getUserName());
        this.mYNote.setPassword(this.mLoginResult.getPassWord());
        this.mTaskManager.pullUserMeta(true);
        return;
      }
      removeDialog(1);
      loginFailed(paramBaseData);
      return;
    case 13:
    }
    removeDialog(1);
    UserMeta localUserMeta;
    int k;
    Note localNote;
    int l;
    if (paramBoolean)
    {
      L.d(this, "isEverlogin is " + this.mYNote.isEverLogin());
      localUserMeta = (UserMeta)paramBaseData;
      if (!this.mYNote.isEverLogin())
      {
        this.mYNote.getLogRecorder().report(LogRecorder.LOGIN_STATUS_FISTLOGIN);
        this.mYNote.setEverLogin();
        Note[] arrayOfNote = this.offlineNotes;
        int i = arrayOfNote.length;
        int j = 0;
        for (k = 0; ; k = l)
        {
          if (j >= i)
            break label531;
          localNote = arrayOfNote[j];
          if (localNote != null)
            break;
          l = k;
          ++j;
        }
        localNote.getNoteMeta().setNoteBook(localUserMeta.getDefaultNoteBook());
        localNote.getNoteMeta().setServerNoteBook(localUserMeta.getDefaultNoteBook());
        L.d(this, "default note book is " + localUserMeta.getDefaultNoteBook());
      }
    }
    try
    {
      this.mDataSource.insertOrUpdateNote(localNote);
      ArrayList localArrayList = this.resourceLists;
      l = k + 1;
      Iterator localIterator = ((ArrayList)localArrayList.get(k)).iterator();
      while (true)
      {
        if (localIterator.hasNext());
        BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
        L.d(this, "add resource " + localBaseResourceMeta.getResourceId());
        AbstractResource localAbstractResource = this.mDataSource.getResource(localBaseResourceMeta);
        this.mDataSource.insertOrUpdateResource(localAbstractResource);
      }
      label531: this.mDataSource.insertOrUpdateUserMeta(this.mLoginResult.getUserName(), localUserMeta);
      Cursor localCursor = this.mDataSource.listAllNotes();
      L.d(this, "After login total notes count is " + localCursor.getCount());
      localCursor.close();
      setResult(-1);
      finish();
      return;
      loginFailed(paramBaseData);
      return;
    }
    catch (IOException localIOException)
    {
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.LoginActivity
 * JD-Core Version:    0.5.4
 */