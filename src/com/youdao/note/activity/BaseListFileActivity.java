package com.youdao.note.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.adapter.SectionAdapter;
import com.youdao.note.data.adapter.SectionItem;
import com.youdao.note.ui.ViewInflatter;
import com.youdao.note.utils.L;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BaseListFileActivity extends LockableActivity
  implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, AbsListView.OnScrollListener
{
  protected ListAdapter mAdapter = null;
  protected View mEmptyView = null;
  private View mHeadView = null;
  protected int mListPost = 0;
  protected ListView mListView = null;
  protected TextView notesTitle = null;

  private void updateFixedHeader(SectionItem paramSectionItem)
  {
    if (this.mHeadView.getVisibility() != 0)
      this.mHeadView.setVisibility(0);
    if ((paramSectionItem.getNoteMeta().needSync()) && (this.mAdapter instanceof SectionAdapter) && (((SectionAdapter)this.mAdapter).getUnsyncedTitle() != null));
    for (String str = getResources().getString(2131361859); ; str = ViewInflatter.labelFormator.format(new Date(paramSectionItem.getNoteMeta().getModifyTime())))
    {
      ((TextView)this.mHeadView.findViewById(2131165207)).setText(str);
      ((TextView)this.mHeadView.findViewById(2131165208)).setText(Integer.toString(((SectionAdapter)this.mAdapter).getSectionCount(str)));
      return;
    }
  }

  protected boolean needLock()
  {
    return true;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mYNote = ((YNoteApplication)getApplication());
    this.notesTitle = ((TextView)findViewById(2131165274));
    this.mListView = ((ListView)findViewById(16908298));
    this.mEmptyView = findViewById(16908292);
    this.mHeadView = findViewById(2131165275);
    if (this.mHeadView == null)
      return;
    this.mHeadView.setVisibility(8);
  }

  public void onDestroy()
  {
    super.onDestroy();
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    L.d(this, "on item click called.");
    SectionItem localSectionItem;
    if ((this.mAdapter != null) && (this.mAdapter.getItem(paramInt) instanceof SectionItem))
    {
      localSectionItem = (SectionItem)this.mAdapter.getItem(paramInt);
      if (localSectionItem.getType() != 0)
        break label59;
      L.d(this, "Label clicked.");
    }
    label59: 
    do
      return;
    while (localSectionItem.getType() != 1);
    NoteMeta localNoteMeta = localSectionItem.getNoteMeta();
    Intent localIntent = new Intent(this, NoteDetailActivity.class);
    localIntent.putExtra("noteid", localNoteMeta.getNoteId());
    L.d(this, "noteMeta is " + localNoteMeta.toString());
    startActivityForResult(localIntent, 0);
    this.mLogRecorder.addViewDetailTimes();
  }

  public boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    return false;
  }

  public void onResume()
  {
    super.onResume();
    L.d(this, "set list post to " + this.mListPost);
  }

  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    Object localObject;
    if (this.mAdapter != null)
    {
      localObject = this.mAdapter.getItem(paramInt1);
      if ((localObject != null) && (localObject instanceof SectionItem))
        break label33;
    }
    label33: SectionItem localSectionItem;
    do
    {
      return;
      localSectionItem = (SectionItem)localObject;
      if (localSectionItem.getType() == 0)
      {
        L.d(this, "ddfadfasdf");
        updateFixedHeader((SectionItem)this.mAdapter.getItem(paramInt1 + 1));
        return;
      }
      if (localSectionItem.getType() != 1)
        continue;
      updateFixedHeader(localSectionItem);
      return;
    }
    while (localSectionItem.getType() != 2);
    this.mHeadView.setVisibility(8);
  }

  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
    if (paramInt != 0)
      return;
    this.mListPost = this.mListView.getFirstVisiblePosition();
  }

  public void setListAdapter(ListAdapter paramListAdapter)
  {
    if ((paramListAdapter == null) || (paramListAdapter.getCount() == 0))
    {
      this.mListView.setVisibility(8);
      if (this.mEmptyView != null)
        this.mEmptyView.setVisibility(0);
    }
    do
    {
      return;
      this.mAdapter = paramListAdapter;
      this.mListView.setAdapter(paramListAdapter);
      this.mListView.setVisibility(0);
      this.mListView.setOnItemLongClickListener(this);
      this.mListView.setOnItemClickListener(this);
      if (this.mHeadView != null)
        this.mListView.setOnScrollListener(this);
      L.d(this, "set empty gone.");
    }
    while (this.mEmptyView == null);
    this.mEmptyView.setVisibility(8);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.BaseListFileActivity
 * JD-Core Version:    0.5.4
 */