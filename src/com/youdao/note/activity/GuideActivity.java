package com.youdao.note.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import com.youdao.note.YNoteApplication;
import com.youdao.note.ui.viewflow.FlowIndicator;
import com.youdao.note.ui.viewflow.ViewFlow;
import com.youdao.note.ui.viewflow.ViewFlow.ViewSwitchListener;

public class GuideActivity extends BaseActivity
  implements ViewFlow.ViewSwitchListener
{
  private static final int VIEW_COUNT = 2;
  private Button mLeftButton;
  private Button mRightButton;
  private ViewFlow mViewFlow;

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903059);
    super.onCreate(paramBundle);
    this.mViewFlow = ((ViewFlow)findViewById(2131165235));
    this.mViewFlow.setAdapter(new GuideAdapter());
    FlowIndicator localFlowIndicator = (FlowIndicator)findViewById(2131165236);
    this.mViewFlow.setFlowIndicator(localFlowIndicator);
    this.mViewFlow.setOnViewSwitchListener(this);
    this.mYNote.setNotFirst();
  }

  public void onSwitched(View paramView, int paramInt)
  {
  }

  private class GuideAdapter extends BaseAdapter
  {
    private View view1 = LayoutInflater.from(GuideActivity.this).inflate(2130903060, null);
    private View view2 = LayoutInflater.from(GuideActivity.this).inflate(2130903061, null);

    public GuideAdapter()
    {
      GuideActivity.access$002(GuideActivity.this, (Button)this.view2.findViewById(2131165237));
      if (GuideActivity.this.mLeftButton != null)
        GuideActivity.this.mLeftButton.setOnClickListener(new View.OnClickListener(GuideActivity.this)
        {
          public void onClick(View paramView)
          {
            GuideActivity.this.finish();
            GuideActivity.this.sendNewNote("com.youdao.note.action.CREATE_HANDWRITE");
          }
        });
      GuideActivity.access$102(GuideActivity.this, (Button)this.view2.findViewById(2131165238));
      if (GuideActivity.this.mRightButton == null)
        return;
      GuideActivity.this.mRightButton.setOnClickListener(new View.OnClickListener(GuideActivity.this)
      {
        public void onClick(View paramView)
        {
          GuideActivity.this.finish();
        }
      });
    }

    public int getCount()
    {
      return 2;
    }

    public Object getItem(int paramInt)
    {
      return null;
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramInt == 0)
        return this.view1;
      return this.view2;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.GuideActivity
 * JD-Core Version:    0.5.4
 */