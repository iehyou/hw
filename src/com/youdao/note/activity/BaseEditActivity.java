package com.youdao.note.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.resource.DoodleActivity;
import com.youdao.note.activity.resource.HandwritingActivity;
import com.youdao.note.activity.resource.ImageViewActivity;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.Snippet;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.AudioResourceMeta;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.DoodleResourceMeta;
import com.youdao.note.data.resource.HandwriteResourceMeta;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.ui.DialogFactory;
import com.youdao.note.ui.DialogFactory.NotebookChooseCallback;
import com.youdao.note.ui.EditFooterBar;
import com.youdao.note.ui.EditFooterBar.EditActionListener;
import com.youdao.note.ui.ResourceButton;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.HTMLUtils;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.htmlcleaner.TagNode;

public abstract class BaseEditActivity extends AbstractLoadNoteActivity
  implements EditFooterBar.EditActionListener
{
  private static final int DIALOG_CANCEL_CONFIRM = 2;
  protected static final int DIALOG_SAVING = 1;
  private static final int SAVE_RESULT_DEVICE_FULL = 2;
  private static final int SAVE_RESULT_FAILED = 1;
  private static final int SAVE_RESULT_SUCCEED = 0;
  private static final String THUMBNAIL_TEMPLATE = "<img border=\"0\" src=\"%s\" filelength=\"%d\" id=\"%s\"  class=\"android-resource-img\" onclick=\"window.View.viewResource('%s')\" /></br><span>&nbsp;</br></span>";
  protected EditFooterBar mFooter;
  protected String mNoteBookId;
  private TextView mNoteBookTitle;
  protected ResourceButton mResourceButton;
  protected ArrayList<BaseResourceMeta> mResourceMetaList = new ArrayList(2);
  protected String oriNoteBookId;
  ProgressDialog savingDialog = null;
  private AsyncTask<Void, Void, Integer> savingTask = null;

  private void appendResources()
  {
    if (this.mResourceMetaList.size() <= 0)
      return;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mNote.getBody());
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    int i1 = 0;
    Iterator localIterator = this.mResourceMetaList.iterator();
    while (localIterator.hasNext())
    {
      label53: BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
      switch (localBaseResourceMeta.getType())
      {
      case 1:
      default:
      case 0:
      case 2:
      case 3:
      case 4:
      }
      while (localBaseResourceMeta instanceof AbstractImageResourceMeta)
      {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = this.mDataSource.getThumbnailPath(localBaseResourceMeta);
        arrayOfObject[1] = Long.valueOf(localBaseResourceMeta.getLength());
        arrayOfObject[2] = localBaseResourceMeta.getResourceId();
        arrayOfObject[3] = localBaseResourceMeta.getResourceId();
        localStringBuilder.append(String.format("<img border=\"0\" src=\"%s\" filelength=\"%d\" id=\"%s\"  class=\"android-resource-img\" onclick=\"window.View.viewResource('%s')\" /></br><span>&nbsp;</br></span>", arrayOfObject));
        break label53:
        ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)localBaseResourceMeta;
        if (localImageResourceMeta.getPicFrom() == 2)
          ++i;
        if (localImageResourceMeta.getPicFrom() != 1)
          continue;
        ++l;
        continue;
        ++j;
        continue;
        ++k;
        continue;
        ++i1;
      }
      TagNode localTagNode = HTMLUtils.createResourceNode(this.mDataSource, localBaseResourceMeta);
      try
      {
        localStringBuilder.append(HTMLUtils.serilizeHtml(localTagNode));
        this.mNote.getNoteMeta().setHasAttachment();
      }
      catch (IOException localIOException)
      {
        L.e(this, "Failed to append resource " + localBaseResourceMeta.getFileName(), localIOException);
      }
    }
    this.mNote.setBody(localStringBuilder.toString());
    this.mLogRecorder.addCameraTimes(i);
    this.mLogRecorder.addPhotoTimes(l);
    this.mLogRecorder.addPaintTimes(j);
    this.mLogRecorder.addArtTimes(k);
    this.mLogRecorder.addRecordTimes(i1);
    L.d(this, "new body is " + this.mNote.getBody());
  }

  private void generateSnippet(NoteMeta paramNoteMeta)
  {
    ArrayList localArrayList = this.mDataSource.getResourceMetasByNoteId(paramNoteMeta.getNoteId());
    if (localArrayList.size() <= 0)
      return;
    int i = -1;
    long l1 = 6144L;
    for (int j = 0; j < localArrayList.size(); ++j)
    {
      if (!FileUtils.isImage(((BaseResourceMeta)localArrayList.get(j)).getFileName()))
        continue;
      long l2 = this.mDataSource.getResourceLength((BaseResourceMeta)localArrayList.get(j));
      if (l2 <= l1)
        continue;
      i = j;
      l1 = l2;
    }
    if (i <= -1)
      return;
    AbstractImageResourceMeta localAbstractImageResourceMeta = (AbstractImageResourceMeta)localArrayList.get(i);
    paramNoteMeta.setSnippetFID(localAbstractImageResourceMeta.getResourceId());
    Snippet localSnippet = this.mDataSource.getSnippet(paramNoteMeta);
    Thumbnail localThumbnail = this.mDataSource.getThumbnail(localAbstractImageResourceMeta);
    if (localSnippet == null)
      return;
    Bitmap localBitmap = ImageUtils.cropImage(BitmapFactory.decodeByteArray(localThumbnail.getContentBytes(), 0, localThumbnail.getContentBytes().length), 90);
    L.d(this, "size is " + localBitmap.getWidth() + " " + localBitmap.getHeight());
    localSnippet.setContentBytes(ImageUtils.bitmap2bytes(localBitmap, Bitmap.CompressFormat.JPEG, 100));
    this.mDataSource.writeSnippet(localSnippet);
  }

  private void sentToResourceManageActivity()
  {
    Intent localIntent = new Intent(getApplicationContext(), ResourceListActivity.class);
    localIntent.putExtra("resourceMetaList", this.mResourceMetaList);
    localIntent.putExtra("noteMeta", this.mNote.getNoteMeta());
    startActivityForResult(localIntent, 7);
  }

  private void updateResources()
  {
    this.mResourceButton.updateResourceCount(this.mResourceMetaList.size());
  }

  protected void addResource(BaseResourceMeta paramBaseResourceMeta)
  {
    paramBaseResourceMeta.setNoteId(this.mNote.getNoteId());
    this.mResourceMetaList.add(paramBaseResourceMeta);
    updateResources();
  }

  public void cancleEdit()
  {
    Iterator localIterator = this.mResourceMetaList.iterator();
    while (localIterator.hasNext())
    {
      BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
      this.mDataSource.deleteResource(localBaseResourceMeta);
    }
    setResult(0);
    finish();
  }

  protected void finishEdit(int paramInt)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("note", this.mNote);
    setResult(-1, localIntent);
    if (paramInt == 0)
      UIUtilities.showToast(this, 2131361941);
    while (true)
    {
      YNoteApplication.getInstance().getSkitchManager().destroy();
      Iterator localIterator = this.mResourceMetaList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          break label188;
        BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
        if (!localBaseResourceMeta instanceof ImageResourceMeta)
          continue;
        ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)localBaseResourceMeta;
        if ((localImageResourceMeta.getPicFrom() != 2) && (localImageResourceMeta.getPicFrom() != 1))
          continue;
        File localFile = localImageResourceMeta.getTempFile();
        if (localFile.delete())
          continue;
        L.e(this, "delete tmp file:" + localFile.getAbsolutePath() + " failed!", null);
      }
      if (paramInt == 2)
        UIUtilities.showToast(this, 2131361943);
      UIUtilities.showToast(this, 2131361942);
    }
    label188: finish();
  }

  protected void initControls()
  {
    this.mFooter = ((EditFooterBar)findViewById(2131165229));
    this.mFooter.setActionListener(this);
    this.mResourceButton = ((ResourceButton)findViewById(2131165220));
    this.mResourceButton.setOnClickListener(this);
    this.mNoteBookTitle = ((TextView)findViewById(2131165318));
    findViewById(2131165325).setOnClickListener(this);
  }

  protected void loadNote(Note paramNote)
  {
    updateNoteBookTitle(paramNote.getNoteBook());
  }

  protected abstract void obtainNote(boolean paramBoolean);

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    L.d(this, "OnActivityResult called.");
    switch (paramInt1)
    {
    case 8:
    case 9:
    case 10:
    case 11:
    default:
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    case 13:
    case 12:
    case 5:
    case 6:
    case 7:
    }
    while (true)
    {
      getIntent().setAction("com.youdao.note.action.CREATE_TEXT");
      return;
      this.mFooter.recoverPos();
      if ((paramInt2 != -1) || (paramIntent == null))
        continue;
      addResource((HandwriteResourceMeta)paramIntent.getSerializableExtra("resourceMeta"));
      continue;
      this.mFooter.recoverPos();
      if ((paramInt2 != -1) || (paramIntent == null))
        continue;
      addResource((DoodleResourceMeta)paramIntent.getSerializableExtra("resourceMeta"));
      continue;
      this.mFooter.recoverPos();
      if ((paramInt2 != -1) || (paramIntent == null))
        continue;
      ImageResourceMeta localImageResourceMeta2 = (ImageResourceMeta)paramIntent.getSerializableExtra("resourceMeta");
      localImageResourceMeta2.setPicFrom(2);
      addResource(localImageResourceMeta2);
      continue;
      this.mFooter.recoverPos();
      if ((paramInt2 != -1) || (paramIntent == null))
        continue;
      ImageResourceMeta localImageResourceMeta1 = (ImageResourceMeta)paramIntent.getSerializableExtra("resourceMeta");
      localImageResourceMeta1.setPicFrom(1);
      addResource(localImageResourceMeta1);
      continue;
      if ((paramInt2 != -1) || (paramIntent == null))
        continue;
      this.mResourceMetaList = ((ArrayList)paramIntent.getSerializableExtra("resourceMetaList"));
      updateResources();
      Serializable localSerializable = paramIntent.getSerializableExtra("resourceMeta");
      if (localSerializable == null)
        continue;
      BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localSerializable;
      try
      {
        this.mFooter.play(this.mDataSource.getResourcePath(localBaseResourceMeta));
      }
      catch (Exception localException)
      {
        L.e(this, "Failed to play resource " + localBaseResourceMeta.getFileName(), localException);
      }
    }
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == 2131165325)
      if (!this.mYNote.isLogin())
        showDialog(107);
    do
    {
      return;
      showDialog(104);
      return;
    }
    while (paramView.getId() != 2131165220);
    sentToResourceManageActivity();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initControls();
    updateResources();
    setVolumeControlStream(3);
    this.mTitleView.requestFocus();
    this.mTitleView.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramEditable)
      {
        BaseEditActivity.this.mNote.setTitle(paramEditable.toString());
      }

      public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
      {
      }

      public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
      {
      }
    });
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog;
    switch (paramInt)
    {
    default:
      localDialog = super.onCreateDialog(paramInt);
    case 107:
    case 1:
    case 2:
    case 104:
    }
    Note localNote;
    do
    {
      return localDialog;
      return new AlertDialog.Builder(this).setTitle(2131361847).setPositiveButton(2131361963, null).create();
      this.savingDialog = new ProgressDialog(this);
      this.savingDialog.setMessage(getText(2131361940));
      this.savingDialog.setIndeterminate(true);
      this.savingDialog.setCancelable(true);
      this.savingDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramDialogInterface)
        {
          if (BaseEditActivity.this.savingTask == null)
            return;
          BaseEditActivity.this.savingTask.cancel(true);
        }
      });
      return this.savingDialog;
      return new AlertDialog.Builder(this).setTitle(2131361873).setCancelable(true).setPositiveButton(2131361963, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          BaseEditActivity.this.cancleEdit();
        }
      }).setNegativeButton(2131361962, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramDialogInterface, int paramInt)
        {
          BaseEditActivity.this.dismissDialog(2);
        }
      }).create();
      localNote = this.mNote;
      localDialog = null;
    }
    while (localNote == null);
    return DialogFactory.getNoteBookPickDialog(this, this.mNoteBookId, new DialogFactory.NotebookChooseCallback()
    {
      public void onNoteMoved(String paramString1, String paramString2)
      {
        BaseEditActivity.this.mNoteBookId = paramString2;
        BaseEditActivity.this.updateNoteBookTitle(BaseEditActivity.this.mNoteBookId);
      }
    });
  }

  public void onDestroy()
  {
    Iterator localIterator = this.mResourceMetaList.iterator();
    while (localIterator.hasNext())
    {
      BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
      if (!localBaseResourceMeta instanceof ImageResourceMeta)
        continue;
      ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)localBaseResourceMeta;
      if ((localImageResourceMeta.getPicFrom() != 2) && (localImageResourceMeta.getPicFrom() != 1))
        continue;
      File localFile = localImageResourceMeta.getTempFile();
      if (localFile.delete())
        continue;
      L.e(this, "delete tmp file:" + localFile.getAbsolutePath() + " failed!", null);
    }
    this.mFooter.destroy();
    super.onDestroy();
  }

  public void onDoodle()
  {
    Intent localIntent = new Intent(this, DoodleActivity.class);
    localIntent.setAction("com.youdao.note.action.DOODLE");
    startActivityForResult(localIntent, 12);
  }

  public void onError(int paramInt)
  {
    L.e(this, "Action error, error code is " + paramInt, null);
  }

  public void onHandwrite()
  {
    Intent localIntent = new Intent(this, HandwritingActivity.class);
    localIntent.setAction("com.youdao.note.action.NEW_HANDWRITE");
    startActivityForResult(localIntent, 13);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      showDialog(2);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    if (!"statusBar".equals(paramIntent.getStringExtra("entry_from")))
      return;
    this.mLogRecorder.addRecordFromBarTimes();
  }

  public void onPause()
  {
    super.onPause();
    this.mFooter.pause();
  }

  public void onPickPhoto()
  {
    Intent localIntent = new Intent(this, ImageViewActivity.class);
    localIntent.setAction("android.intent.action.GET_CONTENT");
    startActivityForResult(localIntent, 6);
  }

  public void onRecord()
  {
    this.mFooter.startRecord();
  }

  public void onRecordFinished(AudioResourceMeta paramAudioResourceMeta)
  {
    UIUtilities.showToast(this, 2131361925);
    addResource(paramAudioResourceMeta);
  }

  public boolean onSearchRequested()
  {
    this.mYNote.sendToSearch(this);
    return false;
  }

  public void onTakePhoto()
  {
    Intent localIntent = new Intent(this, ImageViewActivity.class);
    localIntent.setAction("android.media.action.IMAGE_CAPTURE");
    startActivityForResult(localIntent, 5);
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    super.onUpdate(paramInt, paramBaseData, paramBoolean);
  }

  protected void saveNote()
  {
    showDialog(1);
    this.savingTask = new AsyncTask()
    {
      protected Integer doInBackground(Void[] paramArrayOfVoid)
      {
        boolean bool1 = true;
        NoteMeta localNoteMeta = BaseEditActivity.this.mNote.getNoteMeta();
        BaseEditActivity.this.appendResources();
        localNoteMeta.setSummary(HTMLUtils.extractSummary(BaseEditActivity.this.mNote.getBody(), 100));
        localNoteMeta.setNoteBook(BaseEditActivity.this.mNoteBookId);
        if (localNoteMeta.getVersion() <= 0)
          localNoteMeta.setServerNoteBook(BaseEditActivity.this.mNoteBookId);
        localNoteMeta.setDirty(true);
        localNoteMeta.setModifyTime(System.currentTimeMillis());
        localNoteMeta.setLength(BaseEditActivity.this.mNote.getBody().getBytes().length);
        if ("youdao_note_debug_19880222".equals(localNoteMeta.getTitle()))
          BaseEditActivity.this.mNote.setBody(BaseEditActivity.this.mLogRecorder.dumpStatistics());
        L.d(this, "edited note meta is " + localNoteMeta);
        Iterator localIterator = BaseEditActivity.this.mResourceMetaList.iterator();
        while (localIterator.hasNext())
        {
          BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
          if (!BaseEditActivity.this.mDataSource.existResource(localBaseResourceMeta))
            continue;
          AbstractResource localAbstractResource = BaseEditActivity.this.mDataSource.getResource(localBaseResourceMeta);
          if (BaseEditActivity.this.mDataSource.insertOrUpdateResource(localAbstractResource))
            continue;
          bool1 = false;
        }
        BaseEditActivity.this.generateSnippet(localNoteMeta);
        L.d(this, "note content is " + BaseEditActivity.this.mNote.getHtml());
        if (TextUtils.isEmpty(BaseEditActivity.this.mNote.getBody()))
          BaseEditActivity.this.mNote.setBody("&nbsp;");
        try
        {
          boolean bool2 = BaseEditActivity.this.mDataSource.insertOrUpdateNote(BaseEditActivity.this.mNote, BaseEditActivity.this.oriNoteBookId);
          if ((bool1 & bool2))
            return Integer.valueOf(0);
        }
        catch (IOException localIOException)
        {
          if ("No space left on device".equals(localIOException.getMessage()))
          {
            L.d(this, "No space left on device.");
            return Integer.valueOf(2);
          }
          return Integer.valueOf(1);
        }
        catch (Exception localException)
        {
          return Integer.valueOf(1);
        }
        return Integer.valueOf(1);
      }

      public void onPostExecute(Integer paramInteger)
      {
        if ((BaseEditActivity.this.savingDialog != null) && (BaseEditActivity.this.savingDialog.isShowing()))
          BaseEditActivity.this.dismissDialog(1);
        BaseEditActivity.this.finishEdit(paramInteger.intValue());
      }
    };
    this.savingTask.execute(EmptyInstance.EMPTY_VOIDS);
  }

  protected void updateNoteBookTitle(String paramString)
  {
    NoteBook localNoteBook = this.mDataSource.getNoteBookMetaById(paramString);
    if (localNoteBook != null)
    {
      this.mNoteBookTitle.setText(localNoteBook.getTitle());
      return;
    }
    this.mNoteBookTitle.setText(2131361838);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.BaseEditActivity
 * JD-Core Version:    0.5.4
 */