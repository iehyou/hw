package com.youdao.note.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.ui.EditFooterBar;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.utils.HTMLUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.PreferenceKeys;
import java.util.ArrayList;

public class NewNoteActivity extends BaseEditActivity
  implements PreferenceKeys
{
  private String entryFrom = null;
  private EditText mContentView = null;
  private ResourceAdder mResourceAdder;
  private String typeFrom = null;

  private int checkIdx(int paramInt1, int paramInt2)
  {
    if ((paramInt1 < paramInt2) && (paramInt1 != -1))
      return paramInt1;
    return paramInt2;
  }

  private void dispatchIntent(Intent paramIntent)
  {
    if (needLogin());
    String str;
    do
    {
      return;
      str = paramIntent.getAction();
      if ("com.youdao.note.action.CREATE_TEXT".equals(str))
      {
        this.typeFrom = "TextNoteTimes";
        return;
      }
      if ("com.youdao.note.action.CREATE_SNAPSHOT".equals(str))
      {
        onTakePhoto();
        this.typeFrom = "CameraNoteTimes";
        return;
      }
      if ("com.youdao.note.action.CREATE_GALLARY".equals(str))
      {
        onPickPhoto();
        return;
      }
      if ("com.youdao.note.action.CREATE_HANDWRITE".equals(str))
      {
        onHandwrite();
        return;
      }
      if ("android.intent.action.SEND".equals(str))
      {
        handleIntent(paramIntent);
        return;
      }
      if (!"android.intent.action.SEND_MULTIPLE".equals(str))
        continue;
      handleIntent(paramIntent);
      return;
    }
    while (!"com.youdao.note.action.CREATE_RECORD".equals(str));
    onRecord();
    this.typeFrom = "RecordNoteTimes";
  }

  private void handleIntent(Intent paramIntent)
  {
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle == null);
    do
      return;
    while (localBundle.isEmpty());
    String str1 = paramIntent.getAction();
    while (true)
    {
      String str3;
      try
      {
        String str2 = (String)localBundle.get("android.intent.extra.TITLE");
        if (!TextUtils.isEmpty(str2))
          this.mTitleView.setText(str2);
        do
        {
          String str4 = (String)localBundle.get("android.intent.extra.TEXT");
          if (!TextUtils.isEmpty(str4))
            this.mContentView.setText(str4);
          if (!"android.intent.action.SEND".equals(str1))
            break label180;
          Uri localUri = (Uri)localBundle.getParcelable("android.intent.extra.STREAM");
          L.d(this, "uri is " + localUri);
          this.mResourceAdder.addResourceFromUri(localUri);
          return;
          str3 = (String)localBundle.get("android.intent.extra.SUBJECT");
        }
        while (TextUtils.isEmpty(str3));
      }
      finally
      {
        L.d(this, "Finish extract text notes.");
      }
      label180: if (!"android.intent.action.SEND_MULTIPLE".equals(str1))
        continue;
      Uri[] arrayOfUri = (Uri[])localBundle.getParcelableArrayList("android.intent.extra.STREAM").toArray(new Uri[0]);
      this.mResourceAdder.addImageResource(arrayOfUri);
    }
  }

  private void setNoteBook()
  {
    String str = getIntent().getStringExtra("noteBook");
    if (str != null)
      this.mNote.setNoteBookId(str);
    while (true)
    {
      updateNoteBookTitle(this.mNote.getNoteBook());
      return;
      this.mNote.setNoteBookId(this.mYNote.getDefaultNoteBook());
    }
  }

  protected void initControls()
  {
    super.initControls();
    findViewById(2131165230).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        NewNoteActivity.this.obtainNote(true);
        NewNoteActivity.this.mYNote.getSkitchManager().destroy();
      }
    });
  }

  protected boolean needLogin()
  {
    return (this.mYNote.isEverLogin()) && (!this.mYNote.isLogin());
  }

  protected void obtainNote(boolean paramBoolean)
  {
    String str1 = this.mTitleView.getText().toString();
    String str2 = this.mContentView.getText().toString();
    L.d(this, "content is " + str2);
    NoteMeta localNoteMeta = this.mNote.getNoteMeta();
    int i3;
    if (TextUtils.isEmpty(str1))
    {
      L.d(this, "title is empty");
      if (TextUtils.isEmpty(str2))
        break label235;
      L.d(this, "content is not empty");
      int i = str2.length();
      int j = checkIdx(str2.indexOf("\n"), i);
      int k = checkIdx(str2.indexOf('。'), j);
      int l = checkIdx(str2.indexOf(" "), k);
      int i1 = checkIdx(str2.indexOf(65281), l);
      int i2 = checkIdx(str2.indexOf('!'), i1);
      i3 = checkIdx(str2.indexOf('?'), i2);
    }
    for (str1 = str2.substring(0, checkIdx(str2.indexOf(65311), i3)); ; str1 = getResources().getString(2131361855))
      while (true)
      {
        localNoteMeta.setTitle(str1);
        this.mNote.setBody(HTMLUtils.escAndEncodeHTML(str2));
        if (paramBoolean)
        {
          saveNote();
          this.mLogRecorder.addNote(this.typeFrom, this.entryFrom);
        }
        return;
        label235: L.d(this, this.typeFrom + " " + this.mResourceMetaList.size());
        if (this.mResourceMetaList.size() <= 0)
          continue;
        switch (((BaseResourceMeta)this.mResourceMetaList.get(0)).getType())
        {
        case 1:
        default:
          break;
        case 0:
          ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)this.mResourceMetaList.get(0);
          if (localImageResourceMeta.getPicFrom() == 2)
            str1 = getResources().getString(2131361853);
          if (localImageResourceMeta.getPicFrom() != 1)
            continue;
          str1 = getResources().getString(2131361854);
          break;
        case 2:
          str1 = getResources().getString(2131361856);
        case 3:
        }
      }
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903086);
    super.onCreate(paramBundle);
    this.mResourceAdder = ResourceAdder.getInstance(this, new ResourceAdder.ResourceAddCallback()
    {
      public void onCancle()
      {
      }

      public void onFailed(Exception paramException)
      {
        L.e(this, "", paramException);
      }

      public void onFinishAll(IResourceMeta paramIResourceMeta)
      {
      }

      public void onFinishOne(IResourceMeta paramIResourceMeta)
      {
        NewNoteActivity.this.addResource((BaseResourceMeta)paramIResourceMeta);
      }
    });
    this.mNote = new Note(false);
    setNoteBook();
    String str = this.mNote.getNoteBook();
    this.oriNoteBookId = str;
    this.mNoteBookId = str;
    Intent localIntent = getIntent();
    this.entryFrom = localIntent.getStringExtra("entry_from");
    if (this.entryFrom == null)
      this.entryFrom = "WidgetAddTimes";
    this.mContentView = ((EditText)findViewById(2131165232));
    dispatchIntent(localIntent);
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt, paramBundle);
    case 116:
    }
    return this.mResourceAdder.createDialog();
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mResourceAdder.destroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (TextUtils.isEmpty(this.mTitleView.getText().toString())) && (TextUtils.isEmpty(this.mContentView.getText().toString())) && (this.mResourceMetaList.size() == 0) && (!this.mFooter.isRecording()))
    {
      finish();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onNewIntent(Intent paramIntent)
  {
    L.d(this, "on new intent called.");
    dispatchIntent(paramIntent);
    super.onNewIntent(paramIntent);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.NewNoteActivity
 * JD-Core Version:    0.5.4
 */