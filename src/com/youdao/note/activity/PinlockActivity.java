package com.youdao.note.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.youdao.note.YNoteApplication;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.MD5Digester;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class PinlockActivity extends BaseActivity
{
  private ILocker mLocker = null;

  private void finishWithResult(boolean paramBoolean)
  {
    if (paramBoolean)
      setResult(-1);
    while (true)
    {
      finish();
      return;
      setResult(0);
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    super.onCreate(paramBundle);
    if ("com.youdao.note.action.SETUP_PINLOCK".equals(getIntent().getAction()));
    for (this.mLocker = new SetupLocker(null); ; this.mLocker = new UnLocker(null))
      while (true)
      {
        this.mLocker.initContentView();
        return;
        if (!"com.youdao.note.action.UPDATE_PINLOCK".equals(getIntent().getAction()))
          break;
        this.mLocker = new UpdateLocker(null);
      }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt >= 7) && (paramInt <= 16))
      this.mLocker.onInput(paramInt - 7);
    while (true)
    {
      return super.onKeyDown(paramInt, paramKeyEvent);
      if (paramInt == 67)
        this.mLocker.onInput(-2);
      if (paramInt != 66)
        continue;
    }
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    this.mLocker.restoreState(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.mLocker.saveState(paramBundle);
  }

  private abstract class BaseLocker
    implements PinlockActivity.ILocker
  {
    protected static final int BLUE = 2;
    private static final String BUNDLE_CODE_INPUT_TIMES = "code_times";
    private static final String BUNDLE_CODE_STACK = "code_stack";
    private static final String BUNDLE_CURRENT_CODES = "current_codes";
    private static final int MESSAGE_INPUT_COMPLETE = 1;
    private static final int PINCODE_SIZE = 4;
    protected static final int RED = 1;
    private Handler handler = new Handler()
    {
      public void handleMessage(Message paramMessage)
      {
        if ((paramMessage.what != 1) || (PinlockActivity.BaseLocker.this.mCodeStack.size() != 4))
          return;
        PinlockActivity.BaseLocker.this.onLockCodeCompleted();
      }
    };
    private ArrayList<TextView> mCodeArray = new ArrayList(4);
    private View mCodeBox = null;
    private int mCodeInputTimes = 0;
    protected Stack<Integer> mCodeStack = new Stack();
    private int mCurrentCodes = -1;
    private View mKeyboard = null;
    private TextView mLabel = null;
    private View mPinLabelBg = null;
    private int mPreviousCodes = -1;
    private TextView mSubLabel = null;

    private BaseLocker()
    {
    }

    public void clear()
    {
      this.mCodeInputTimes = 0;
      Iterator localIterator = this.mCodeArray.iterator();
      while (localIterator.hasNext())
        ((TextView)localIterator.next()).setText("");
      this.mCodeStack.clear();
    }

    public int getCodeInputTimes()
    {
      return this.mCodeInputTimes;
    }

    protected int getCurrentCodes()
    {
      return this.mCurrentCodes;
    }

    protected int getPreviousCodes()
    {
      return this.mPreviousCodes;
    }

    protected void hideCodeBox()
    {
      this.mCodeBox.setVisibility(8);
    }

    protected void hideKeyboard()
    {
      this.mKeyboard.setVisibility(8);
    }

    public void initContentView()
    {
      PinlockActivity.this.setContentView(2130903102);
      this.mLabel = ((TextView)PinlockActivity.this.findViewById(2131165274));
      this.mSubLabel = ((TextView)PinlockActivity.this.findViewById(2131165342));
      this.mCodeBox = PinlockActivity.this.findViewById(2131165344);
      this.mKeyboard = PinlockActivity.this.findViewById(2131165345);
      this.mPinLabelBg = PinlockActivity.this.findViewById(2131165341);
      TableLayout localTableLayout = (TableLayout)PinlockActivity.this.findViewById(2131165345);
      for (int i = 0; i < localTableLayout.getChildCount(); ++i)
      {
        TableRow localTableRow = (TableRow)localTableLayout.getChildAt(i);
        int k = 0;
        label116: if (k >= localTableRow.getChildCount())
          continue;
        View localView = localTableRow.getChildAt(k);
        if (i == 3)
          if (k != 0);
        for (int l = -1; ; l = 1 + (k + i * 3))
          while (true)
          {
            localView.setOnClickListener(new View.OnClickListener(l)
            {
              public void onClick(View paramView)
              {
                PinlockActivity.BaseLocker.this.onInput(this.val$tmpCode);
              }
            });
            ++k;
            break label116:
            if (k == 1)
              l = 0;
            l = 0;
            if (k != 2)
              continue;
            l = -2;
          }
      }
      LinearLayout localLinearLayout = (LinearLayout)PinlockActivity.this.findViewById(2131165344);
      for (int j = 0; j < localLinearLayout.getChildCount(); ++j)
      {
        TextView localTextView = (TextView)localLinearLayout.getChildAt(j);
        if (j < this.mCodeStack.size())
          localTextView.setText("*");
        this.mCodeArray.add(localTextView);
      }
    }

    public void onInput(int paramInt)
    {
      L.d(PinlockActivity.this, "OnInput called. " + paramInt);
      if (paramInt == -1)
      {
        PinlockActivity.this.setResult(0);
        PinlockActivity.this.finish();
      }
      do
      {
        do
        {
          return;
          if (paramInt != -2)
            break label96;
        }
        while (this.mCodeStack.size() <= 0);
        ((TextView)this.mCodeArray.get(-1 + this.mCodeStack.size())).setText("");
        this.mCodeStack.pop();
        return;
        label96: if (this.mCodeStack.size() >= 4)
          continue;
        this.mCodeStack.push(Integer.valueOf(paramInt));
        ((TextView)this.mCodeArray.get(-1 + this.mCodeStack.size())).setText("*");
      }
      while (this.mCodeStack.size() != 4);
      this.handler.sendEmptyMessageDelayed(1, 500L);
    }

    protected void onLockCodeCompleted()
    {
      this.mPreviousCodes = this.mCurrentCodes;
      this.mCurrentCodes = 0;
      Iterator localIterator1 = this.mCodeStack.iterator();
      while (localIterator1.hasNext())
      {
        Integer localInteger = (Integer)localIterator1.next();
        this.mCurrentCodes += 10 * this.mCurrentCodes + localInteger.intValue();
      }
      Iterator localIterator2 = this.mCodeArray.iterator();
      while (localIterator2.hasNext())
        ((TextView)localIterator2.next()).setText("");
      this.mCodeStack.clear();
      this.mCodeInputTimes = (1 + this.mCodeInputTimes);
    }

    public void restoreState(Bundle paramBundle)
    {
      this.mCodeInputTimes = paramBundle.getInt("code_times", 0);
      this.mCurrentCodes = paramBundle.getInt("current_codes");
      this.mCodeStack = ((Stack)paramBundle.getSerializable("code_stack"));
    }

    public void saveState(Bundle paramBundle)
    {
      paramBundle.putInt("code_times", this.mCodeInputTimes);
      paramBundle.putInt("current_codes", this.mCurrentCodes);
      paramBundle.putSerializable("code_stack", this.mCodeStack);
    }

    protected void setLabel(int paramInt)
    {
      this.mLabel.setText(paramInt);
    }

    protected void setLabelBgColor(int paramInt)
    {
      if (paramInt == 1)
      {
        this.mPinLabelBg.setBackgroundResource(2130837697);
        return;
      }
      this.mPinLabelBg.setBackgroundResource(2130837696);
    }

    protected void setSubLabel(int paramInt)
    {
      this.mSubLabel.setText(paramInt);
      if (this.mSubLabel.getVisibility() == 0)
        return;
      this.mSubLabel.setVisibility(0);
    }
  }

  private static abstract interface ILocker
  {
    public static final int CANCEL = -1;
    public static final int DELETE = -2;

    public abstract void initContentView();

    public abstract void onInput(int paramInt);

    public abstract void restoreState(Bundle paramBundle);

    public abstract void saveState(Bundle paramBundle);
  }

  private class SetupLocker extends PinlockActivity.BaseLocker
  {
    private TextView mCodeHint = null;

    private SetupLocker()
    {
      super(PinlockActivity.this, null);
    }

    private void showInputAgain()
    {
      setLabel(2131361825);
      this.mCodeHint.setVisibility(8);
      setLabelBgColor(2);
    }

    public void initContentView()
    {
      super.initContentView();
      setLabel(2131361828);
      this.mCodeHint = ((TextView)PinlockActivity.this.findViewById(2131165343));
      setLabelBgColor(2);
      if (getCodeInputTimes() != 1)
        return;
      showInputAgain();
    }

    public void onLockCodeCompleted()
    {
      super.onLockCodeCompleted();
      if (getCodeInputTimes() == 1)
        showInputAgain();
      do
        return;
      while (getCodeInputTimes() != 2);
      if (getCurrentCodes() == getPreviousCodes())
      {
        PinlockActivity.this.mYNote.setPinLock(getCurrentCodes());
        PinlockActivity.this.finishWithResult(true);
        return;
      }
      clear();
      this.mCodeHint.setVisibility(0);
      this.mCodeHint.setText(2131361824);
      setLabelBgColor(1);
    }
  }

  private class UnLocker extends PinlockActivity.BaseLocker
  {
    private static final String APP_LOCKED_KEY = "locked";
    private static final int MAX_RETRY_TIMES = 3;
    private SharedPreferences mLockPreference = PinlockActivity.this.getSharedPreferences("lock", 0);
    private View mPasswordLayout = null;

    private UnLocker()
    {
      super(PinlockActivity.this, null);
    }

    private void showInputAgain()
    {
      setLabel(2131361822);
      setSubLabel(2131361823);
      setLabelBgColor(1);
    }

    private void switch2PasswordView()
    {
      View localView = PinlockActivity.this.findViewById(2131165281);
      if (localView != null)
        localView.setVisibility(8);
      ((TextView)PinlockActivity.this.findViewById(2131165340)).setText(PinlockActivity.this.mYNote.getUserName());
      setLabelBgColor(1);
      hideCodeBox();
      setLabel(2131361820);
      setSubLabel(2131361821);
      this.mPasswordLayout.setVisibility(0);
      hideKeyboard();
    }

    private boolean verifyPassword(String paramString)
    {
      return MD5Digester.digest(paramString).equals(PinlockActivity.this.mYNote.getPassword());
    }

    private boolean verifyPinCode(int paramInt)
    {
      return IdUtils.genPinCode(paramInt).equals(PinlockActivity.this.mYNote.getPinLock());
    }

    private void veryfyPassword()
    {
      TextView localTextView = (TextView)PinlockActivity.this.findViewById(2131165279);
      if (verifyPassword(localTextView.getText().toString()))
      {
        PinlockActivity.this.mYNote.setPinlockEnable(false);
        this.mLockPreference.edit().putBoolean("locked", false).commit();
        PinlockActivity.this.finishWithResult(true);
        return;
      }
      setLabel(2131361822);
      localTextView.setText("");
      setLabelBgColor(1);
    }

    public void initContentView()
    {
      super.initContentView();
      setLabel(2131361827);
      this.mPasswordLayout = PinlockActivity.this.findViewById(2131165339);
      PinlockActivity.this.findViewById(2131165204).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          PinlockActivity.UnLocker.this.veryfyPassword();
        }
      });
      if (this.mLockPreference.getBoolean("locked", false))
        switch2PasswordView();
      do
        return;
      while (getCodeInputTimes() <= 0);
      showInputAgain();
    }

    public void onLockCodeCompleted()
    {
      super.onLockCodeCompleted();
      if (getCodeInputTimes() > 3)
      {
        this.mLockPreference.edit().putBoolean("locked", true).commit();
        switch2PasswordView();
        return;
      }
      if (verifyPinCode(getCurrentCodes()))
      {
        onLockCodeVerified();
        return;
      }
      showInputAgain();
    }

    protected void onLockCodeVerified()
    {
      PinlockActivity.this.finishWithResult(true);
    }
  }

  private class UpdateLocker
    implements PinlockActivity.ILocker
  {
    private static final String BUNDLE_CODE_VERIFIED = "code_verified";
    private boolean mCodeVerified = false;
    private PinlockActivity.ILocker mSetupLocker = new PinlockActivity.SetupLocker(PinlockActivity.this, null);
    private PinlockActivity.ILocker mUnlocker = new PinlockActivity.UnLocker()
    {
      public void onLockCodeVerified()
      {
        L.d(PinlockActivity.this, "Code verified.");
        PinlockActivity.UpdateLocker.access$602(PinlockActivity.UpdateLocker.this, true);
        PinlockActivity.UpdateLocker.this.mSetupLocker.initContentView();
      }
    };

    private UpdateLocker()
    {
    }

    public void initContentView()
    {
      if (this.mCodeVerified)
      {
        this.mSetupLocker.initContentView();
        return;
      }
      this.mUnlocker.initContentView();
    }

    public void onInput(int paramInt)
    {
      if (this.mCodeVerified)
      {
        this.mSetupLocker.onInput(paramInt);
        return;
      }
      this.mUnlocker.onInput(paramInt);
    }

    public void restoreState(Bundle paramBundle)
    {
      this.mCodeVerified = paramBundle.getBoolean("code_verified", false);
      if (this.mCodeVerified)
      {
        this.mSetupLocker.restoreState(paramBundle);
        return;
      }
      this.mUnlocker.restoreState(paramBundle);
    }

    public void saveState(Bundle paramBundle)
    {
      if (this.mCodeVerified)
        this.mSetupLocker.saveState(paramBundle);
      while (true)
      {
        paramBundle.putBoolean("code_verified", this.mCodeVerified);
        return;
        this.mUnlocker.saveState(paramBundle);
      }
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.PinlockActivity
 * JD-Core Version:    0.5.4
 */