package com.youdao.note.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.ProgressData;
import com.youdao.note.data.RemoteErrorData;
import com.youdao.note.data.TempFile;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.TempFileCache;
import com.youdao.note.exceptions.DownloadResourceException;
import com.youdao.note.task.TaskManager;
import com.youdao.note.ui.DialogFactory;
import com.youdao.note.ui.DialogFactory.NoteDeletedCallback;
import com.youdao.note.ui.audio.AudioPlayerBar;
import com.youdao.note.ui.audio.AudioPlayerBar.AudioPlayListener;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UIUtilities;
import com.youdao.note.utils.UnitUtils;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.List;

public class NoteDetailActivity extends AbstractLoadNoteActivity
  implements AudioPlayerBar.AudioPlayListener
{
  private static final int DOWNLOAD_DIALOG = 4;
  private static final String JS_GETBODY = "javascript:window.View.onBodyFetched(document.body.innerHTML)";
  private static final String JS_UPDATE_PROGRESS_TEXT = "javascript:updateResourceDownloadProgress(\"%s\", \"%s\")";
  private static final String JS_UPDATE_RESOURCE_BUTTON = "javascript:updateResourceButtonImage(\"%s\", \"%s\")";
  private static final int NO_APP_DIALOG = 3;
  private static final int NO_RESOURCE_FOUND_DIALOG = 5;
  private final int guide_right = 155;
  private final int guide_top = 150;
  private final Handler handler = new Handler();
  private int lastWidth = -1;
  private boolean mFirstLoad = true;
  private boolean mImgSelected = false;
  private Intent mIntent = null;
  private View mNoteLoding = null;
  private AudioPlayerBar mPlayer;
  private ProgressDialog mProgressDialog = null;
  private boolean mPullAuto = false;
  private MenuItem mRefreshItem = null;
  private BaseResourceMeta resourceMeta;

  private boolean checkIntent()
  {
    if (getPackageManager().queryIntentActivities(this.mIntent, 65536).isEmpty())
    {
      UIUtilities.showToast(this, 2131361923);
      return false;
    }
    return true;
  }

  private void enableRefresh()
  {
    UIUtilities.makeViewGone(this.mNoteLoding);
    if (this.mRefreshItem == null)
      return;
    this.mRefreshItem.setEnabled(true);
  }

  private void initListener()
  {
    this.mPlayer = ((AudioPlayerBar)findViewById(2131165323));
    this.mPlayer.setAudioPlayListener(this);
    ((ImageView)findViewById(2131165331)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        if (!NoteDetailActivity.this.mYNote.isLogin())
        {
          Toast.makeText(NoteDetailActivity.this, 2131362031, 0).show();
          return;
        }
        if (NoteDetailActivity.this.mNote.isDirty())
        {
          Toast.makeText(NoteDetailActivity.this, 2131362032, 0).show();
          return;
        }
        NoteDetailActivity.this.mailShare();
      }
    });
    ((ImageView)findViewById(2131165330)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramView)
      {
        NoteDetailActivity.this.editCurrentNote();
      }
    });
  }

  private void initUI()
  {
    this.mWebView.setDrawingCacheEnabled(true);
    this.mWebView.addJavascriptInterface(new MyJavaScriptInteface(), "View");
    this.mNoteLoding = findViewById(2131165322);
    if (this.mYNote.isShowMailGuide())
    {
      View localView1 = findViewById(2131165324);
      localView1.setVisibility(0);
      View localView2 = findViewById(2131165320);
      1 local1 = new ViewTreeObserver.OnGlobalLayoutListener(localView1, localView2)
      {
        public void onGlobalLayout()
        {
          if ((!NoteDetailActivity.this.mYNote.isShowMailGuide()) || (this.val$guide.getVisibility() != 0) || (this.val$frameLayout.getWidth() == NoteDetailActivity.this.lastWidth))
            return;
          NoteDetailActivity.this.showMailGuide();
          NoteDetailActivity.access$002(NoteDetailActivity.this, this.val$frameLayout.getWidth());
        }
      };
      localView2.getViewTreeObserver().addOnGlobalLayoutListener(local1);
      findViewById(2131165324).setOnTouchListener(new View.OnTouchListener(localView2, local1, localView1)
      {
        public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
        {
          this.val$frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this.val$listener);
          this.val$guide.setVisibility(8);
          AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
          localAlphaAnimation.setDuration(500L);
          this.val$guide.setAnimation(localAlphaAnimation);
          this.val$guide.setVisibility(8);
          NoteDetailActivity.this.mYNote.setShowMailGuide(false);
          return true;
        }
      });
      return;
    }
    findViewById(2131165324).setVisibility(8);
  }

  private void mailShare()
  {
    Bitmap localBitmap = this.mWebView.getDrawingCache();
    TempFile localTempFile = TempFile.newNoteSnapshot(this.mNote.getNoteId());
    localTempFile.setContentBytes(ImageUtils.bitmap2bytes(localBitmap));
    try
    {
      this.mDataSource.getTempFileCache().updateTempFile(localTempFile);
      Intent localIntent = new Intent(this, MailShareActivity.class);
      localIntent.putExtra("noteid", this.mNote.getNoteId());
      startActivity(localIntent);
      this.mLogRecorder.addShareMailTimes();
      return;
    }
    catch (IOException localIOException)
    {
      L.e(this, "", localIOException);
    }
  }

  private void refreshNote()
  {
    this.mPullAuto = true;
    this.mTaskManager.pullNoteIfNeed(this.mNote.getNoteMeta());
    if (this.loadDismissed)
    {
      this.mNoteLoding.setVisibility(0);
      return;
    }
    this.mNoteLoding.setVisibility(8);
  }

  private void sendToViewResource()
  {
    setNeedLock(false);
    if (!checkIntent())
      return;
    Uri localUri = this.mIntent.getData();
    if (FileUtils.isPlayable(localUri.toString()))
      this.mPlayer.setVisibility(0);
    try
    {
      this.mPlayer.stop();
      this.mPlayer.play(localUri);
      return;
    }
    catch (Exception localException)
    {
      this.mPlayer.setVisibility(8);
      L.e(this, "Failed to play uri " + localUri.toString(), localException);
      startActivity(this.mIntent);
    }
  }

  private void showMailGuide()
  {
    View localView1 = findViewById(2131165320);
    View localView2 = findViewById(2131165331);
    ImageView localImageView = (ImageView)findViewById(2131165324);
    int[] arrayOfInt = new int[2];
    localView2.getLocationInWindow(arrayOfInt);
    int i = localView1.getWidth();
    int j = localView1.getHeight();
    int k = 155 - (i - arrayOfInt[0] - localView2.getWidth() / 2);
    int l = 150 - localView2.getHeight() / 2;
    Bitmap localBitmap1;
    try
    {
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      if (localDisplayMetrics.densityDpi == 120);
      for (String str = "mail_share_guide_ldpi.png"; ; str = "mail_share_guide_xdpi.png")
      {
        while (true)
        {
          Bitmap localBitmap3 = BitmapFactory.decodeStream(getAssets().open(str));
          localBitmap1 = localBitmap3;
          label135: Bitmap localBitmap2 = Bitmap.createBitmap(localImageView.getWidth(), localImageView.getHeight(), Bitmap.Config.ARGB_8888);
          Canvas localCanvas = new Canvas();
          Paint localPaint = new Paint();
          localCanvas.setBitmap(localBitmap2);
          Rect localRect1 = new Rect();
          Rect localRect2 = new Rect();
          int i1 = i + k - localBitmap1.getWidth();
          int i2 = 0 - l;
          int i3 = i1 + localBitmap1.getWidth();
          int i4 = i2 + localBitmap1.getHeight();
          localRect1.left = (0 - i1);
          localRect1.right = (i - i1);
          localRect1.top = (0 - i2);
          localRect1.bottom = (j - i2);
          localRect2.left = (i1 - 0);
          localRect2.right = (i3 - 0);
          localRect2.top = (i2 - 0);
          localRect2.bottom = (i4 - 0);
          if (localRect1.left < 0)
            localRect1.left = 0;
          if (localRect1.right < 0)
            localRect1.right = 0;
          if (localRect1.top < 0)
            localRect1.top = 0;
          if (localRect1.bottom < 0)
            localRect1.bottom = 0;
          if (localRect2.left < 0)
            localRect2.left = 0;
          if (localRect2.right < 0)
            localRect2.right = 0;
          if (localRect2.top < 0)
            localRect2.top = 0;
          if (localRect2.bottom < 0)
            localRect2.bottom = 0;
          if (localRect1.left > localBitmap1.getWidth())
            localRect1.left = localBitmap1.getWidth();
          if (localRect1.right > localBitmap1.getWidth())
            localRect1.right = localBitmap1.getWidth();
          if (localRect1.top > localBitmap1.getHeight())
            localRect1.top = localBitmap1.getHeight();
          if (localRect1.bottom > localBitmap1.getHeight())
            localRect1.bottom = localBitmap1.getHeight();
          if (localRect2.left > localBitmap2.getWidth())
            localRect2.left = localBitmap2.getWidth();
          if (localRect2.right > localBitmap2.getWidth())
            localRect2.right = localBitmap2.getWidth();
          if (localRect2.top > localBitmap2.getHeight())
            localRect2.top = localBitmap2.getHeight();
          if (localRect2.bottom > localBitmap2.getHeight())
            localRect2.bottom = localBitmap2.getHeight();
          localCanvas.drawBitmap(localBitmap1, localRect1, localRect2, null);
          localPaint.setColor(localBitmap1.getPixel(5, 5));
          if (localRect2.left > 0)
            localCanvas.drawRect(new Rect(0, 0, localRect2.left, localBitmap2.getHeight()), localPaint);
          if (localRect2.top > 0)
            localCanvas.drawRect(new Rect(localRect2.left, 0, localRect2.right, localRect2.top), localPaint);
          if (localRect2.right < localBitmap2.getWidth())
            localCanvas.drawRect(new Rect(localRect2.right, 0, localBitmap2.getWidth(), localBitmap2.getHeight()), localPaint);
          if (localRect2.bottom > 0)
            localCanvas.drawRect(new Rect(localRect2.left, localRect2.bottom, localRect2.right, localBitmap2.getHeight()), localPaint);
          localImageView.setImageBitmap(localBitmap2);
          localImageView.postInvalidate();
          return;
          if (localDisplayMetrics.densityDpi != 160)
            break;
          str = "mail_share_guide_mdpi.png";
        }
        if (localDisplayMetrics.densityDpi != 320)
          break;
      }
      str = "mail_share_guide_hdpi.png";
    }
    catch (IOException localIOException)
    {
      L.e(this, "", localIOException);
      localBitmap1 = null;
      break label135:
    }
  }

  private void updateDownloadProgress(String paramString1, String paramString2)
  {
    this.mWebView.loadUrl(String.format("javascript:updateResourceDownloadProgress(\"%s\", \"%s\")", new Object[] { paramString1, paramString2 }));
  }

  private void updateNoteContent()
  {
    this.mWebView.loadUrl("javascript:window.View.onBodyFetched(document.body.innerHTML)");
  }

  private void updateResourceButtonImage(String paramString1, String paramString2)
  {
    this.mWebView.loadUrl(String.format("javascript:updateResourceButtonImage(\"%s\", \"%s\")", new Object[] { paramString1, paramString2 }));
    updateNoteContent();
  }

  void editCurrentNote()
  {
    this.mYNote.sendEditNote(this, this.mNote.getNoteId());
  }

  protected void loadNote(Note paramNote)
  {
    synchronized (this.mNote)
    {
      if (!paramNote.getNoteId().equals(this.mNote.getNoteId()))
        return;
      if (!this.loadDismissed)
      {
        dismissDialog(201);
        this.loadDismissed = true;
      }
      if (!this.mNote.getNoteBook().equals(paramNote.getNoteBook()))
        removeDialog(105);
      if ((!this.mNote.getTitle().equals(paramNote.getTitle())) || (this.mFirstLoad))
        this.mTitleView.setText(paramNote.getTitle());
      if ((!this.mNote.getHtml().equals(paramNote.getHtml())) || (this.mFirstLoad))
      {
        L.d(this, "reload html is " + this.mNote.getHtml());
        this.mWebView.loadDataWithBaseURL("file:///android_asset/", paramNote.getHtml(), "text/html", "utf-8", null);
      }
      this.mNote = paramNote;
      this.mFirstLoad = false;
      return;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
    case 2:
    }
    if (-1 == paramInt2)
    {
      Note localNote = (Note)paramIntent.getSerializableExtra("note");
      L.d(this, "Note edit ok. notebook is " + localNote.getNoteBook());
      loadNote(localNote);
      return;
    }
    L.d(this, "Note edit cancled.");
  }

  public void onClose()
  {
    UIUtilities.makeViewGone(this.mPlayer);
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    UIUtilities.makeViewGone(this.mPlayer);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  public void onCreate(Bundle paramBundle)
  {
    setVolumeControlStream(3);
    requestWindowFeature(1);
    setContentView(2130903090);
    super.onCreate(paramBundle);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 105:
      this.mLogRecorder.addNoteDetailTimes();
      return DialogFactory.getNoteDetailDialog(this, this.mNote.getNoteId());
    case 3:
      return new AlertDialog.Builder(this).setMessage(2131361923).setPositiveButton(2131361963, null).create();
    case 4:
      this.mProgressDialog = new ProgressDialog(this);
      this.mProgressDialog.setMessage(this.resourceMeta.getFileName());
      this.mProgressDialog.setIndeterminate(false);
      this.mProgressDialog.setMax(100);
      this.mProgressDialog.setProgressStyle(1);
      this.mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramDialogInterface)
        {
          NoteDetailActivity.this.mTaskManager.stopAll();
        }
      });
      return this.mProgressDialog;
    case 5:
      return new AlertDialog.Builder(this).setMessage(2131361851).setPositiveButton(2131361963, null).create();
    case 100:
    }
    return DialogFactory.getDeleteConfirmDialog(this, this.mNote.getNoteMeta(), new DialogFactory.NoteDeletedCallback()
    {
      public void onNoteDeleted()
      {
        NoteDetailActivity.this.setResult(1);
        NoteDetailActivity.this.finish();
      }
    });
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    new MenuInflater(this).inflate(2131034113, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mPlayer.stop();
    this.mDataSource.getTempFileCache().clean("-note-snapshot.jpg");
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    return false;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == 2131165373)
    {
      this.mPullAuto = false;
      this.mNoteLoding.setVisibility(0);
      this.mTaskManager.pullNote(this.mNote.getNoteMeta(), true);
    }
    do
    {
      return true;
      if (paramMenuItem.getItemId() == 2131165374)
      {
        showDialog(105);
        return true;
      }
      if (paramMenuItem.getItemId() == 2131165268)
      {
        showDialog(100);
        return true;
      }
      if (paramMenuItem.getItemId() != 2131165372)
        continue;
      try
      {
        WebView.class.getMethod("emulateShiftHeld", new Class[0]).invoke(this.mWebView, new Object[0]);
        UIUtilities.showToast(this, 2131361817);
        this.mLogRecorder.addContentCopyTimes();
        return true;
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        UIUtilities.showToast(this, 2131361815);
        return true;
      }
      catch (Exception localException)
      {
        UIUtilities.showToast(this, 2131361816);
        return true;
      }
    }
    while (paramMenuItem.getItemId() != 2131165375);
    editCurrentNote();
    return true;
  }

  public void onPause()
  {
    this.mTaskManager.stopAll();
    super.onPause();
    this.mPlayer.pause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.mRefreshItem = paramMenu.findItem(2131165373);
    if ((this.mNoteLoding.getVisibility() == 0) || (this.mNote.getNoteMeta().needSync()))
      this.mRefreshItem.setEnabled(false);
    while (true)
    {
      return super.onPrepareOptionsMenu(paramMenu);
      this.mRefreshItem.setEnabled(true);
    }
  }

  public void onResume()
  {
    super.onResume();
    initUI();
    initListener();
    if ((!this.mNote.getNoteMeta().isDirty()) && (this.loadDismissed))
      refreshNote();
    while (true)
    {
      this.mWebView.loadUrl("javascript:recover()");
      setNeedLock(true);
      return;
      this.mNoteLoding.setVisibility(8);
    }
  }

  public boolean onSearchRequested()
  {
    this.mYNote.sendToSearch(this);
    return false;
  }

  public void onSeekComplete(MediaPlayer paramMediaPlayer)
  {
  }

  public void onStart()
  {
    super.onStart();
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default:
      super.onUpdate(paramInt, paramBaseData, paramBoolean);
    case 14:
    case 2:
    case 8:
    case 9:
    }
    DownloadResourceException localDownloadResourceException;
    do
    {
      ProgressData localProgressData;
      do
      {
        do
        {
          return;
          enableRefresh();
          super.onUpdate(paramInt, paramBaseData, paramBoolean);
          return;
          enableRefresh();
          super.onUpdate(paramInt, paramBaseData, paramBoolean);
          return;
        }
        while (!paramBoolean);
        localProgressData = (ProgressData)paramBaseData;
        L.d(this, "progress updated called.");
        updateDownloadProgress(localProgressData.getId(), UnitUtils.getSize(localProgressData.getTotalSize(), localProgressData.getProgress()));
      }
      while (this.mProgressDialog == null);
      this.mProgressDialog.setProgress(localProgressData.getProgress());
      return;
      if (paramBoolean)
      {
        if (this.mImgSelected)
        {
          removeDialog(4);
          sendToViewResource();
          return;
        }
        updateResourceButtonImage(((ProgressData)paramBaseData).getId(), "file:///android_asset/arrow.png");
        return;
      }
      RemoteErrorData localRemoteErrorData = (RemoteErrorData)paramBaseData;
      boolean bool = localRemoteErrorData.getException() instanceof DownloadResourceException;
      localDownloadResourceException = null;
      if (bool)
        localDownloadResourceException = (DownloadResourceException)localRemoteErrorData.getException();
      if ((this.mProgressDialog != null) && (this.mProgressDialog.isShowing()))
        dismissDialog(4);
      if ((localDownloadResourceException.getCause() != null) && (localDownloadResourceException.getCause() instanceof IOException) && ("No space left on device".equals(((IOException)localDownloadResourceException.getCause()).getMessage())))
      {
        L.d(this, "No space left on device.");
        UIUtilities.showToast(this, 2131361944);
        updateResourceButtonImage(localDownloadResourceException.getResourceId(), "file:///android_asset/download.png");
        return;
      }
      UIUtilities.showToast(this, 2131361922);
    }
    while (localDownloadResourceException == null);
    updateResourceButtonImage(localDownloadResourceException.getResourceId(), "file:///android_asset/arrow.png");
  }

  protected boolean showError()
  {
    return !this.mPullAuto;
  }

  class MyJavaScriptInteface
  {
    MyJavaScriptInteface()
    {
    }

    public void log(String paramString)
    {
      NoteDetailActivity.this.handler.post(new Runnable(paramString)
      {
        public void run()
        {
          L.d(this, this.val$log);
        }
      });
    }

    public void onBodyFetched(String paramString)
    {
      NoteDetailActivity.this.handler.post(new Runnable(paramString)
      {
        public void run()
        {
          L.d(this, "Note content updated.");
          NoteDetailActivity.this.mNote.setBody(this.val$noteBody);
          try
          {
            NoteDetailActivity.this.mDataSource.updateNoteCache(NoteDetailActivity.this.mNote);
            return;
          }
          catch (IOException localIOException)
          {
          }
        }
      });
    }

    public void viewResource(String paramString)
    {
      NoteDetailActivity.this.handler.post(new Runnable(paramString)
      {
        private void openResource(BaseResourceMeta paramBaseResourceMeta)
          throws IOException
        {
          if (FileUtils.isImage(paramBaseResourceMeta.genRelativePath()))
          {
            NoteDetailActivity.access$502(NoteDetailActivity.this, true);
            NoteDetailActivity.access$602(NoteDetailActivity.this, paramBaseResourceMeta);
            NoteDetailActivity.this.mLogRecorder.addViewPicTimes();
            if (NoteDetailActivity.this.mDataSource.existResource(paramBaseResourceMeta))
            {
              NoteDetailActivity.this.sendToViewResource();
              return;
            }
            NoteDetailActivity.this.removeDialog(4);
            NoteDetailActivity.this.showDialog(4);
          }
          while (true)
          {
            NoteDetailActivity.this.mTaskManager.pullResource(paramBaseResourceMeta);
            return;
            NoteDetailActivity.access$502(NoteDetailActivity.this, false);
            NoteDetailActivity.this.mLogRecorder.addViewAttachTimes();
            if (NoteDetailActivity.this.mDataSource.existResource(paramBaseResourceMeta))
            {
              NoteDetailActivity.this.sendToViewResource();
              NoteDetailActivity.this.updateResourceButtonImage(paramBaseResourceMeta.getResourceId(), "file:///android_asset/arrow.png");
              NoteDetailActivity.this.updateDownloadProgress(paramBaseResourceMeta.getResourceId(), UnitUtils.getSize(paramBaseResourceMeta.getLength()));
              return;
            }
            NoteDetailActivity.this.updateResourceButtonImage(paramBaseResourceMeta.getResourceId(), "file:///android_asset/juhua.gif");
          }
        }

        public void run()
        {
          try
          {
            L.d(NoteDetailActivity.this, "resourceId is " + this.val$resourceId);
            BaseResourceMeta localBaseResourceMeta = NoteDetailActivity.this.mDataSource.getResourceMeta(this.val$resourceId);
            if (localBaseResourceMeta != null)
            {
              Uri localUri = Uri.fromFile(new File(NoteDetailActivity.this.mDataSource.getResourcePath(localBaseResourceMeta)));
              NoteDetailActivity.access$402(NoteDetailActivity.this, new Intent());
              NoteDetailActivity.this.mIntent.setAction("android.intent.action.VIEW");
              String str1 = FileUtils.getFileExtension(URLDecoder.decode(localUri.toString()));
              String str2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str1);
              NoteDetailActivity.this.mIntent.setDataAndType(localUri, str2);
              openResource(localBaseResourceMeta);
              return;
            }
            NoteDetailActivity.this.showDialog(5);
            return;
          }
          catch (Exception localException)
          {
            L.e(this, "no application.", localException);
            NoteDetailActivity.this.showDialog(3);
          }
        }
      });
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.NoteDetailActivity
 * JD-Core Version:    0.5.4
 */