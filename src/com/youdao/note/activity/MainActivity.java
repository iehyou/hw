package com.youdao.note.activity;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.activity.delegate.CheckUpdateDelegate;
import com.youdao.note.activity.delegate.SyncbarDelegate;
import com.youdao.note.activity.delegate.SyncbarDelegate.SyncListener;
import com.youdao.note.data.BaseData;
import com.youdao.note.ui.DialogFactory;
import com.youdao.note.ui.YNoteDialog;
import com.youdao.note.utils.L;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MainActivity extends LockableActivity
{
  private static final String BUNDLE_SHOW_LICENSE = "license";
  private CheckUpdateDelegate mUpdateDelegate;
  private Integer[] magicNumbers;
  private Queue<Integer> queue;
  private long resumedTime;
  private boolean showingLicense;
  private SyncbarDelegate syncbarDelegate;

  public MainActivity()
  {
    Integer[] arrayOfInteger = new Integer[10];
    arrayOfInteger[0] = Integer.valueOf(18);
    arrayOfInteger[1] = Integer.valueOf(42);
    arrayOfInteger[2] = Integer.valueOf(37);
    arrayOfInteger[3] = Integer.valueOf(31);
    arrayOfInteger[4] = Integer.valueOf(36);
    arrayOfInteger[5] = Integer.valueOf(43);
    arrayOfInteger[6] = Integer.valueOf(40);
    arrayOfInteger[7] = Integer.valueOf(29);
    arrayOfInteger[8] = Integer.valueOf(47);
    arrayOfInteger[9] = Integer.valueOf(18);
    this.magicNumbers = arrayOfInteger;
    this.resumedTime = 0L;
    this.showingLicense = false;
    this.queue = new LinkedList();
  }

  private void checkNewFeatureGuide()
  {
    if (!this.mYNote.isFirstTime())
      return;
    startActivity(new Intent(this, GuideActivity.class));
  }

  private void checkReport()
  {
    if ((this.mLogRecorder.isEverReport()) || (System.currentTimeMillis() - this.resumedTime <= 3000L))
      return;
    this.mLogRecorder.report(LogRecorder.LOGIN_STATUS_FIRSTLAUNCH);
  }

  protected boolean needLock()
  {
    return true;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 3:
    }
    while (true)
    {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
      if (paramInt2 != -1)
        break;
      L.d(this, "login succeed.");
      this.syncbarDelegate.syncIfNeed();
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    requestWindowFeature(1);
    setContentView(2130903084);
    super.onCreate(paramBundle);
    this.syncbarDelegate = new SyncbarDelegate(this);
    this.syncbarDelegate.setSyncListener(new SyncbarDelegate.SyncListener()
    {
      public void beforeSync()
      {
        MainActivity.this.checkReport();
      }
    });
    if ("com.youdao.note.action.FINISH".equals(getIntent().getAction()))
      finishLockable();
    this.mUpdateDelegate = new CheckUpdateDelegate(this);
    if (this.mLogRecorder.isSizeSet())
      return;
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.mLogRecorder.setScreenParameters(localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, localDisplayMetrics.densityDpi);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 2:
      return this.mUpdateDelegate.createCheckingUpdateDialog();
    case 4:
      return this.mUpdateDelegate.createCheckUpdateFailedDialog();
    case 5:
      return this.mUpdateDelegate.createNewVersionFoundDialog();
    case 6:
      return this.mUpdateDelegate.createNoUpdateFoundDialog();
    case 109:
      YNoteDialog localYNoteDialog = new YNoteDialog(this, 2131361849);
      View localView = getLayoutInflater().inflate(2130903072, null);
      localYNoteDialog.setView(localView);
      localYNoteDialog.setTitle(2131361993);
      localView.findViewById(2131165271).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          MainActivity.this.mYNote.acceptLicense();
          MainActivity.this.removeDialog(109);
          MainActivity.this.checkNewFeatureGuide();
          MainActivity.access$202(MainActivity.this, false);
        }
      });
      localView.findViewById(2131165272).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramView)
        {
          MainActivity.this.finish();
        }
      });
      localYNoteDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramDialogInterface)
        {
          MainActivity.this.finish();
        }
      });
      this.showingLicense = true;
      return localYNoteDialog;
    case 108:
      return DialogFactory.getAboutDialog(this);
    case 114:
    }
    return DialogFactory.getNewFeaturesDialog(this);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    new MenuInflater(this).inflate(2131034112, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
  }

  protected void onIntentSend(Intent paramIntent)
  {
    String str;
    if (paramIntent.getComponent() != null)
    {
      str = paramIntent.getComponent().getClassName();
      if (!NewNoteActivity.class.getName().equals(str))
        break label41;
      this.mLogRecorder.addIconNewTimes();
    }
    while (true)
    {
      checkReport();
      return;
      if (NoteListActivity.class.getName().equals(str))
        label41: this.mLogRecorder.addIconAllTimes();
      if (!NoteBookListActivity.class.getName().equals(str))
        continue;
      this.mLogRecorder.addIconNotebookTimes();
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    L.d(this, paramInt + " keycode");
    this.queue.add(Integer.valueOf(paramInt));
    while (this.queue.size() > this.magicNumbers.length)
      this.queue.poll();
    if ((this.queue.size() == this.magicNumbers.length) && (Arrays.equals(this.queue.toArray(new Integer[0]), this.magicNumbers)))
    {
      YNoteApplication localYNoteApplication = this.mYNote;
      boolean bool1 = this.mYNote.isDebug();
      boolean bool2 = false;
      if (!bool1)
        bool2 = true;
      localYNoteApplication.setDebug(bool2);
      this.mYNote.clearData();
      sendLogin();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onNewIntent(Intent paramIntent)
  {
    L.d(this, "Main on new intent called.");
    if ((!this.mYNote.isLogin()) && ("com.youdao.note.action.login".equals(paramIntent.getAction())))
      sendLogin();
    do
      return;
    while (!"com.youdao.note.action.FINISH".equals(paramIntent.getAction()));
    finishLockable();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.syncbarDelegate.onOptionsItemSelected(paramMenuItem))
      return true;
    if (paramMenuItem.getItemId() == 2131165367)
    {
      Intent localIntent1 = new Intent(this, WebActivity.class);
      localIntent1.putExtra("web_conetnt", 2);
      startActivity(localIntent1);
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165368)
    {
      this.mYNote.logOut();
      startActivityForResult(new Intent(this, LoginActivity.class), 3);
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165370)
    {
      showDialog(108);
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165370)
    {
      Intent localIntent2 = new Intent("android.intent.action.SEND", Uri.parse("mailto:"));
      String str1 = getResources().getString(2131361883);
      String str2 = getResources().getString(2131361882);
      localIntent2.setType("text/plain");
      localIntent2.putExtra("android.intent.extra.EMAIL", "");
      localIntent2.putExtra("android.intent.extra.SUBJECT", str2);
      localIntent2.putExtra("android.intent.extra.TEXT", str1);
      localIntent2.putExtra("web_conetnt", 2);
      startActivity(localIntent2);
      return true;
    }
    if (paramMenuItem.getItemId() == 2131165371)
    {
      this.mYNote.stopAllTask();
      finish();
      Process.killProcess(Process.myPid());
    }
    if (paramMenuItem.getItemId() == 2131165369)
      this.mUpdateDelegate.checkUpdate();
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.syncbarDelegate.onPrepareOptionsMenu(paramMenu);
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    this.showingLicense = paramBundle.getBoolean("license");
    this.mUpdateDelegate.onRestoreState(paramBundle);
    super.onRestoreInstanceState(paramBundle);
  }

  public void onResume()
  {
    this.syncbarDelegate.onResume();
    super.onResume();
    if (!this.mYNote.licenseAccepted())
      if (!this.showingLicense)
        showDialog(109);
    while (true)
    {
      this.syncbarDelegate.syncIfNeed();
      L.d(this, "onResume called.");
      this.resumedTime = System.currentTimeMillis();
      return;
      checkNewFeatureGuide();
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putBoolean("license", this.showingLicense);
    this.mUpdateDelegate.onSaveState(paramBundle);
    super.onSaveInstanceState(paramBundle);
  }

  public void onStart()
  {
    super.onStart();
  }

  public void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    this.syncbarDelegate.onUpdate(paramInt, paramBaseData, paramBoolean);
    if (paramInt == 21)
    {
      this.mUpdateDelegate.onCheckUpdateResult(paramBaseData, paramBoolean);
      return;
    }
    super.onUpdate(paramInt, paramBaseData, paramBoolean);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.activity.MainActivity
 * JD-Core Version:    0.5.4
 */