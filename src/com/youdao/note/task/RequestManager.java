package com.youdao.note.task;

import com.youdao.note.utils.L;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class RequestManager
{
  private Set<IManageableTask> runningTask = new HashSet();

  public void manageTask(IManageableTask paramIManageableTask)
  {
    monitorenter;
    try
    {
      this.runningTask.add(paramIManageableTask);
      paramIManageableTask.setRequestManager(this);
      L.d(this, "One task added, total task size is " + this.runningTask.size());
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void removeTask(IManageableTask paramIManageableTask)
  {
    monitorenter;
    try
    {
      this.runningTask.remove(paramIManageableTask);
      L.d(this, "One task removed, total task size is " + this.runningTask.size());
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void stopAll()
  {
    monitorenter;
    IManageableTask localIManageableTask;
    try
    {
      Iterator localIterator = this.runningTask.iterator();
      if (!localIterator.hasNext())
        break label68;
      localIManageableTask = (IManageableTask)localIterator.next();
    }
    finally
    {
      monitorexit;
    }
    label68: this.runningTask.clear();
    L.d(this, "All request task stoped.");
    monitorexit;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.RequestManager
 * JD-Core Version:    0.5.4
 */