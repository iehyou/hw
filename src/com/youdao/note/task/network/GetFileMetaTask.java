package com.youdao.note.task.network;

import com.youdao.note.data.PathCollection;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.L;
import org.json.JSONException;

public class GetFileMetaTask extends BaseApiRequestTask<PathCollection>
{
  public GetFileMetaTask()
  {
    this("/", -1, 0, false);
  }

  public GetFileMetaTask(String paramString, int paramInt)
  {
    this(paramString, paramInt, -1, false);
    L.d(this, "baseVersion is " + paramInt);
  }

  public GetFileMetaTask(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super("filemeta", "get", arrayOfObject, true);
  }

  protected PathCollection handleResponse(String paramString)
    throws JSONException
  {
    return PathCollection.fromJsonString(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetFileMetaTask
 * JD-Core Version:    0.5.4
 */