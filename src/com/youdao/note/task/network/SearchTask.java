package com.youdao.note.task.network;

import com.youdao.note.data.ListNoteMetas;
import com.youdao.note.task.BaseApiRequestTask;
import org.json.JSONException;

public class SearchTask extends BaseApiRequestTask<ListNoteMetas>
{
  public SearchTask(String paramString)
  {
    this(paramString, "/", 0, -1L, -1L, -1L, -1L, 0, 1000, true);
  }

  public SearchTask(String paramString1, String paramString2, int paramInt1, long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    super("search", "get", arrayOfObject);
  }

  public ListNoteMetas handleResponse(String paramString)
    throws JSONException
  {
    return ListNoteMetas.fromJsonString(paramString, true, true);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.SearchTask
 * JD-Core Version:    0.5.4
 */