package com.youdao.note.task.network;

import com.youdao.note.task.BaseApiRequestTask;
import org.json.JSONException;
import org.json.JSONObject;

public class MoveNoteTask extends BaseApiRequestTask<Integer>
{
  public MoveNoteTask(String paramString1, String paramString2)
  {
    super("file", "mv", arrayOfString);
  }

  public Integer handleResponse(String paramString)
  {
    try
    {
      Integer localInteger = Integer.valueOf(new JSONObject(paramString).getInt("v"));
      return localInteger;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return Integer.valueOf(-1);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.MoveNoteTask
 * JD-Core Version:    0.5.4
 */