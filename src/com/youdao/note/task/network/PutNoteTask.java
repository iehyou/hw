package com.youdao.note.task.network;

import com.youdao.note.data.Note;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.HTMLUtils;
import com.youdao.note.utils.L;
import java.util.Map;
import org.json.JSONObject;

public class PutNoteTask extends BaseApiRequestTask<Integer>
{
  public PutNoteTask(Note paramNote, Map<String, BaseResourceMeta> paramMap)
  {
    this(paramNote.getEntryPath(), paramNote.getSource(), paramNote.getAuthor(), paramNote.getTitle(), HTMLUtils.convertToServerHtml(paramNote.getBody()));
  }

  public PutNoteTask(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    super("file", "putfile", new Object[] { "src", paramString2, "au", paramString3, "tl", paramString4, "p", paramString1, "bs", paramString5 });
  }

  protected Integer handleResponse(String paramString)
    throws Exception
  {
    L.d(this, "PutNoteTask " + paramString);
    return Integer.valueOf(new JSONObject(paramString).getInt("v"));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.PutNoteTask
 * JD-Core Version:    0.5.4
 */