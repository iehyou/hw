package com.youdao.note.task.network;

import com.youdao.note.data.ListNoteMetas;
import com.youdao.note.task.BaseApiRequestTask;
import org.json.JSONException;

public class GetTrashTask extends BaseApiRequestTask<ListNoteMetas>
{
  public GetTrashTask(int paramInt1, int paramInt2, int paramInt3)
  {
    super("trash", "get", arrayOfObject);
  }

  public ListNoteMetas handleResponse(String paramString)
    throws JSONException
  {
    return ListNoteMetas.fromJsonString(paramString, true, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetTrashTask
 * JD-Core Version:    0.5.4
 */