package com.youdao.note.task.network;

import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.DownloadResourceException;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

public class GetResourceTask extends BaseApiRequestTask<File>
  implements Consts
{
  private static final long DEFAULT_CONTENT_LENGTH = 1024L;
  private static final int PROGRES_UPSATE_THREAD = 5;
  private static final long UPDATE_TIME_THREAD = 1000L;
  private int mLastProgress = -5;
  private long mLastUpdateTime = -1000L;
  private IResourceMeta mMeta;
  private String mPath;
  private File outFile;

  public GetResourceTask(String paramString, IResourceMeta paramIResourceMeta, int paramInt1, int paramInt2)
  {
    super("file", "getres", arrayOfObject);
    this.mPath = paramString;
    this.mMeta = paramIResourceMeta;
    this.outFile = new File(DataSource.getTmpResourcePath(paramString));
  }

  protected HttpPost getPostMethod()
    throws DownloadResourceException
  {
    try
    {
      HttpPost localHttpPost = super.getPostMethod();
      if (this.outFile.exists())
        localHttpPost.addHeader("Range", "bytes=" + this.outFile.length() + "-");
      return localHttpPost;
    }
    catch (Exception localException)
    {
      throw new DownloadResourceException(this.mMeta.getResourceId(), localException);
    }
  }

  protected File handleResponse(HttpResponse paramHttpResponse)
    throws DownloadResourceException
  {
    while (true)
    {
      FileOutputStream localFileOutputStream;
      InputStream localInputStream;
      int j;
      try
      {
        if (!this.outFile.exists())
          FileUtils.createNewFile(this.outFile);
        long l = paramHttpResponse.getEntity().getContentLength();
        if (l < 0L)
          l = 1024L;
        localFileOutputStream = new FileOutputStream(this.outFile, true);
        localInputStream = paramHttpResponse.getEntity().getContent();
        try
        {
          byte[] arrayOfByte = new byte[''];
          int i = localInputStream.read(arrayOfByte);
          if (i == -1)
            break label238;
          localFileOutputStream.write(arrayOfByte, 0, i);
          boolean bool = isCancelled();
          if (bool)
            return null;
          j = (int)(100L * this.outFile.length() / l);
          if (j < 5 + this.mLastProgress)
            break label274;
          if (System.currentTimeMillis() < 1000L + this.mLastUpdateTime)
            break label274;
          Integer[] arrayOfInteger = new Integer[1];
          arrayOfInteger[0] = Integer.valueOf(j);
          publishProgress(arrayOfInteger);
        }
        finally
        {
          localFileOutputStream.close();
          localInputStream.close();
        }
      }
      catch (Exception localException)
      {
        throw new DownloadResourceException(this.mMeta.getResourceId(), localException);
      }
      label238: localFileOutputStream.close();
      localInputStream.close();
      File localFile = new File(this.mPath);
      this.outFile.renameTo(localFile);
      return localFile;
      label274: if (j < 100)
        continue;
    }
  }

  protected boolean needGzip()
  {
    return false;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetResourceTask
 * JD-Core Version:    0.5.4
 */