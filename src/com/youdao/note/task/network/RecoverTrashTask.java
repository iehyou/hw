package com.youdao.note.task.network;

import com.youdao.note.task.BaseApiRequestTask;

public class RecoverTrashTask extends BaseApiRequestTask<Void>
{
  public RecoverTrashTask(String paramString)
  {
    super("trash", "recover", new Object[] { "ps", paramString });
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.RecoverTrashTask
 * JD-Core Version:    0.5.4
 */