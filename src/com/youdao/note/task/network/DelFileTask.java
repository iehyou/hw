package com.youdao.note.task.network;

import android.text.TextUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.BaseApiRequestTask;

public class DelFileTask extends BaseApiRequestTask<Boolean>
{
  private String[] mPaths = null;

  public DelFileTask(String paramString)
  {
    super("filemeta", "rm", new String[] { "ps", paramString }, true);
    this.mPaths = paramString.split(",");
  }

  public DelFileTask(String[] paramArrayOfString)
  {
    this(TextUtils.join(",", paramArrayOfString));
    this.mPaths = paramArrayOfString;
  }

  protected Boolean handleResponse(String paramString)
    throws Exception
  {
    boolean bool = true;
    for (String str : this.mPaths)
      bool &= getDataSource().deleteNote(str);
    return Boolean.valueOf(bool);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.DelFileTask
 * JD-Core Version:    0.5.4
 */