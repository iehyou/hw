package com.youdao.note.task.network;

import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.HTMLUtils;
import com.youdao.note.utils.L;

public class GetNoteTask extends BaseApiRequestTask<Note>
{
  private NoteMeta mNoteMeta = null;

  public GetNoteTask(NoteMeta paramNoteMeta)
  {
    this(paramNoteMeta.getEntryPath());
    this.mNoteMeta = paramNoteMeta;
  }

  private GetNoteTask(String paramString)
  {
    this(paramString, -1, 0, 0);
  }

  public GetNoteTask(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    super("file", "get", arrayOfObject, true);
  }

  protected Note handleResponse(String paramString)
    throws Exception
  {
    Note localNote = new Note(this.mNoteMeta, null);
    try
    {
      localNote.setBody(HTMLUtils.convertToLocalHtml(localNote.getNoteId(), getDataSource(), paramString));
      getDataSource().insertOrUpdateNote(localNote);
      return localNote;
    }
    catch (Exception localException)
    {
      L.e(this, "Failed to convert html to local version.", localException);
      localNote.setBody(paramString);
    }
    return localNote;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetNoteTask
 * JD-Core Version:    0.5.4
 */