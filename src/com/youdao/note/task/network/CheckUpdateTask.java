package com.youdao.note.task.network;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.UpdateCheckResult;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.L;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CheckUpdateTask extends BaseApiRequestTask<UpdateCheckResult>
{
  private static final String TAG_DOWNLOADURL = "downloadurl";
  private static final String TAG_FEATURES = "release_note";
  private static final String TAG_VERSION = "latestversion";
  private static YNoteApplication mYNote = YNoteApplication.getInstance();

  public CheckUpdateTask()
    throws PackageManager.NameNotFoundException
  {
    super(mYNote.getUpdateUrl(), false);
  }

  private String getValue(NodeList paramNodeList)
  {
    return paramNodeList.item(0).getChildNodes().item(0).getNodeValue().trim();
  }

  protected UpdateCheckResult handleResponse(InputStream paramInputStream)
    throws Exception
  {
    Document localDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(paramInputStream);
    NodeList localNodeList1 = localDocument.getElementsByTagName("latestversion");
    if (localNodeList1.getLength() == 1)
    {
      String str1 = getValue(localNodeList1);
      NodeList localNodeList2 = localDocument.getElementsByTagName("downloadurl");
      if (localNodeList2.getLength() == 1)
      {
        String str2 = getValue(localNodeList2);
        String str3 = mYNote.getPackageManager().getPackageInfo(mYNote.getPackageName(), 0).versionName;
        L.d(this, str1 + "  " + str2);
        if (!str1.equalsIgnoreCase(str3))
          return new UpdateCheckResult(true, str2, getValue(localDocument.getElementsByTagName("release_note")).trim());
      }
    }
    return new UpdateCheckResult();
  }

  protected void onFailed(Exception paramException)
  {
  }

  protected void onSucceed(UpdateCheckResult paramUpdateCheckResult)
  {
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.CheckUpdateTask
 * JD-Core Version:    0.5.4
 */