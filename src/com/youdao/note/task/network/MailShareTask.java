package com.youdao.note.task.network;

import com.youdao.note.data.NoteMeta;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.L;

public class MailShareTask extends BaseApiRequestTask<Boolean>
{
  public MailShareTask(NoteMeta paramNoteMeta, String paramString1, String paramString2, String paramString3)
  {
    super("share", "mail", arrayOfObject);
  }

  protected Boolean handleResponse(String paramString)
    throws Exception
  {
    L.d(this, "mail share response is " + paramString);
    return Boolean.valueOf(true);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.MailShareTask
 * JD-Core Version:    0.5.4
 */