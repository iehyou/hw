package com.youdao.note.task.network;

import com.youdao.note.data.PathCollection;
import com.youdao.note.task.BaseApiRequestTask;
import org.json.JSONException;

@Deprecated
public class ListFileTask extends BaseApiRequestTask<PathCollection>
{
  public ListFileTask()
  {
    this("/", -1);
  }

  public ListFileTask(int paramInt)
  {
    this("/", paramInt);
  }

  public ListFileTask(String paramString, int paramInt)
  {
    this(paramString, -1, 0, -1L, -1L, -1L, -1L, 0, 1000);
  }

  public ListFileTask(String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt3, int paramInt4)
  {
    super("list", "get", arrayOfObject);
  }

  public PathCollection handleResponse(String paramString)
    throws JSONException
  {
    return PathCollection.fromJsonString(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.ListFileTask
 * JD-Core Version:    0.5.4
 */