package com.youdao.note.task.network;

import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.IOUtils;
import java.io.InputStream;

public class GetSnippetTask extends BaseApiRequestTask<byte[]>
{
  public GetSnippetTask(String paramString, int paramInt)
  {
    super("file", "thm", arrayOfObject);
  }

  protected byte[] handleResponse(InputStream paramInputStream)
    throws Exception
  {
    return IOUtils.toByteArray(paramInputStream);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetSnippetTask
 * JD-Core Version:    0.5.4
 */