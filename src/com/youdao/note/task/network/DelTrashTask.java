package com.youdao.note.task.network;

import com.youdao.note.task.BaseApiRequestTask;

public class DelTrashTask extends BaseApiRequestTask<Void>
{
  public DelTrashTask()
  {
    super("trash", "clear", null);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.DelTrashTask
 * JD-Core Version:    0.5.4
 */