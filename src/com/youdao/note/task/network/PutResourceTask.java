package com.youdao.note.task.network;

import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.YNoteUtils;
import java.io.File;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartUploadListener;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

public class PutResourceTask extends BaseApiRequestTask<JSONObject>
{
  private MultipartUploadListener listener;
  private String mFid;
  private AbstractResource<? extends IResourceMeta> mResource;

  public PutResourceTask(AbstractResource<? extends IResourceMeta> paramAbstractResource, String paramString, MultipartUploadListener paramMultipartUploadListener)
  {
    super("file", "putres", null);
    this.mResource = paramAbstractResource;
    this.mFid = paramString;
    this.listener = paramMultipartUploadListener;
  }

  protected HttpPost getPostMethod()
    throws Exception
  {
    HttpPost localHttpPost = super.getPostMethod();
    MultipartEntity localMultipartEntity = new MultipartEntity(this.listener);
    localMultipartEntity.addPart("fid", new StringBody(this.mFid));
    localMultipartEntity.addPart("keyfrom", new StringBody(YNoteApplication.getInstance().getKeyFrom()));
    localMultipartEntity.addPart("bs", new FileBody(new File(this.mResource.getAbslutePath()), this.mResource.getFileName(), "application/octet-stream", null));
    localHttpPost.setEntity(localMultipartEntity);
    return localHttpPost;
  }

  public JSONObject handleResponse(String paramString)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject(paramString);
    BaseResourceMeta localBaseResourceMeta = YNoteUtils.extractResource(localJSONObject.getString("url"), 1);
    if (localBaseResourceMeta != null)
      localJSONObject.put("version", localBaseResourceMeta.getVersion());
    return localJSONObject;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.PutResourceTask
 * JD-Core Version:    0.5.4
 */