package com.youdao.note.task.network;

import com.youdao.note.data.UserMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.BaseApiRequestTask;
import org.json.JSONException;

public class GetUserMetaTask extends BaseApiRequestTask<UserMeta>
{
  public GetUserMetaTask()
  {
    super("user", "get", null);
  }

  public UserMeta handleResponse(String paramString)
    throws JSONException
  {
    UserMeta localUserMeta = UserMeta.fromJsonString(paramString);
    String str = getUserName();
    getDataSource().insertOrUpdateUserMeta(str, localUserMeta);
    return localUserMeta;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.GetUserMetaTask
 * JD-Core Version:    0.5.4
 */