package com.youdao.note.task.network;

import android.text.TextUtils;
import com.youdao.note.data.LoginResult;
import com.youdao.note.exceptions.LoginException;
import com.youdao.note.task.AbstractAsyncRequestTask;
import com.youdao.note.utils.IOUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.WebUtils;
import org.apache.http.HttpResponse;

public abstract class LoginTask extends AbstractAsyncRequestTask<LoginResult>
{
  private static final String PERSIST_COOKIE = "&persist_cookie=";
  private String mPasswdMd5;
  private String mUserName;

  public LoginTask(String paramString1, String paramString2, String paramString3)
  {
    super(String.format("http://reg.163.com/services/userlogin?username=%s&password=%s&type=1&userip=%s&product=note&savelogin=1&passtype=0&back_url=http://www.youdao.com", arrayOfObject), false);
    this.mUserName = paramString1;
    this.mPasswdMd5 = paramString2;
  }

  protected LoginResult handleResponse(HttpResponse paramHttpResponse)
    throws Exception
  {
    L.d(this, "response is " + paramHttpResponse);
    String str1 = new String(IOUtils.toByteArray(getResponseAsInputStream(paramHttpResponse)), "utf-8");
    if (str1.startsWith("460"))
      throw new LoginException(460);
    if (str1.startsWith("420"))
      throw new LoginException(420);
    if (str1.startsWith("412"))
      throw new LoginException(412);
    for (String str2 : str1.split("\n"))
    {
      if ((TextUtils.isEmpty(str2)) || (!str2.startsWith("cookie=")))
        continue;
      String str3 = str2.substring(str2.indexOf("&persist_cookie=") + "&persist_cookie=".length());
      String str4 = WebUtils.getSessionCookie(paramHttpResponse);
      return new LoginResult(this.mUserName, this.mPasswdMd5, str3, str4);
    }
    throw new LoginException(-1);
  }

  protected void onFailed(Exception paramException)
  {
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.network.LoginTask
 * JD-Core Version:    0.5.4
 */