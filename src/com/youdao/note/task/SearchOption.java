package com.youdao.note.task;

public class SearchOption
{
  private String mKeyWord = null;
  private String mNoteBook = null;

  public String getKeyWord()
  {
    return this.mKeyWord;
  }

  public String getNoteBook()
  {
    return this.mNoteBook;
  }

  public void setKeyWord(String paramString)
  {
    this.mKeyWord = paramString;
  }

  public void setNoteBook(String paramString)
  {
    this.mNoteBook = paramString;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.SearchOption
 * JD-Core Version:    0.5.4
 */