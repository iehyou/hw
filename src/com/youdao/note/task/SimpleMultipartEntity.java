package com.youdao.note.task;

import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.IResourceMeta;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

public class SimpleMultipartEntity
  implements HttpEntity
{
  private static final char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  private String boundary = null;
  boolean isSetFirst = false;
  boolean isSetLast = false;
  ByteArrayOutputStream out = new ByteArrayOutputStream();

  public SimpleMultipartEntity()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    Random localRandom = new Random();
    for (int i = 0; i < 30; ++i)
      localStringBuffer.append(MULTIPART_CHARS[localRandom.nextInt(MULTIPART_CHARS.length)]);
    this.boundary = localStringBuffer.toString();
  }

  public void addPart(String paramString, AbstractResource<? extends IResourceMeta> paramAbstractResource)
  {
    try
    {
      addPart(paramString, paramAbstractResource.getFileName(), new FileInputStream(paramAbstractResource.getAbslutePath()));
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }

  public void addPart(String paramString, File paramFile)
  {
    try
    {
      addPart(paramString, paramFile.getName(), new FileInputStream(paramFile));
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }

  public void addPart(String paramString1, String paramString2)
  {
    writeFirstBoundaryIfNeeds();
    try
    {
      this.out.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"\r\n\r\n").getBytes());
      this.out.write(paramString2.getBytes());
      this.out.write(("\r\n--" + this.boundary + "\r\n").getBytes());
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public void addPart(String paramString1, String paramString2, InputStream paramInputStream)
  {
    addPart(paramString1, paramString2, paramInputStream, "application/octet-stream");
  }

  public void addPart(String paramString1, String paramString2, InputStream paramInputStream, String paramString3)
  {
    writeFirstBoundaryIfNeeds();
    byte[] arrayOfByte;
    int i;
    try
    {
      String str = "Content-Type: " + paramString3 + "\r\n";
      this.out.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"; filename=\"" + paramString2 + "\"\r\n").getBytes());
      this.out.write(str.getBytes());
      this.out.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
      arrayOfByte = new byte[4096];
      i = paramInputStream.read(arrayOfByte);
      label146: if (i == -1)
        break label146;
    }
    catch (IOException localIOException2)
    {
      try
      {
        paramInputStream.close();
        return;
        this.out.flush();
        try
        {
          paramInputStream.close();
          return;
        }
        catch (IOException localIOException4)
        {
          localIOException4.printStackTrace();
          return;
        }
      }
      catch (IOException localIOException3)
      {
        localIOException3.printStackTrace();
        return;
      }
    }
    finally
    {
      try
      {
        paramInputStream.close();
        throw localObject;
      }
      catch (IOException localIOException1)
      {
        localIOException1.printStackTrace();
      }
    }
  }

  public void consumeContent()
    throws IOException, UnsupportedOperationException
  {
    if (!isStreaming())
      return;
    throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
  }

  public InputStream getContent()
    throws IOException, UnsupportedOperationException
  {
    return new ByteArrayInputStream(this.out.toByteArray());
  }

  public Header getContentEncoding()
  {
    return null;
  }

  public long getContentLength()
  {
    writeLastBoundaryIfNeeds();
    return this.out.size();
  }

  public Header getContentType()
  {
    return new BasicHeader("Content-Type", "multipart/form-data; boundary=" + this.boundary);
  }

  public boolean isChunked()
  {
    return false;
  }

  public boolean isRepeatable()
  {
    return false;
  }

  public boolean isStreaming()
  {
    return false;
  }

  public void writeFirstBoundaryIfNeeds()
  {
    if (!this.isSetFirst);
    try
    {
      this.out.write(("--" + this.boundary + "\r\n").getBytes());
      this.isSetFirst = true;
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public void writeLastBoundaryIfNeeds()
  {
    if (this.isSetLast)
      return;
    try
    {
      this.out.write(("\r\n--" + this.boundary + "--\r\n").getBytes());
      this.isSetLast = true;
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public void writeTo(OutputStream paramOutputStream)
    throws IOException
  {
    paramOutputStream.write(this.out.toByteArray());
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.SimpleMultipartEntity
 * JD-Core Version:    0.5.4
 */