package com.youdao.note.task;

import android.os.AsyncTask.Status;
import android.os.Looper;
import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.Consts.DATA_TYPE;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.L;
import java.util.Timer;
import java.util.TimerTask;

public class SyncManager
  implements Consts.DATA_TYPE
{
  private static final long DELAY = 60000L;
  private static final long SYNCER_CHECK_INTERVAL = 120000L;
  private static Syncer syncer = null;
  private Object lock = new Object();
  private DataSource mDataSource = null;
  private TaskManager mTaskManager = null;
  private Timer timer = null;

  public SyncManager(TaskManager paramTaskManager, DataSource paramDataSource)
  {
    this.mTaskManager = paramTaskManager;
    this.mDataSource = paramDataSource;
    resetTime(YNoteApplication.getInstance().getAutoSyncPeriod());
  }

  public int getSyncProgress()
  {
    synchronized (this.lock)
    {
      if (syncer != null)
      {
        int i = syncer.getProgress();
        return i;
      }
      return 0;
    }
  }

  public boolean isSyncing()
  {
    synchronized (this.lock)
    {
      if (syncer == null)
        return false;
      AsyncTask.Status localStatus1 = syncer.getStatus();
      AsyncTask.Status localStatus2 = AsyncTask.Status.FINISHED;
      int i = 0;
      if (localStatus1 != localStatus2)
        i = 1;
      return i;
    }
  }

  public void resetTime(int paramInt)
  {
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer = null;
    }
    if (paramInt == -1)
      return;
    this.timer = new Timer("YNoteSyncerTimer", false);
    this.timer.schedule(new TimerTask()
    {
      private boolean loop = false;

      public void run()
      {
        if (!this.loop)
        {
          Looper.prepare();
          this.loop = true;
        }
        try
        {
          SyncManager.this.startSync(true);
          return;
        }
        catch (Exception localException)
        {
          L.e(this, "Failed to sync", localException);
        }
      }
    }
    , 60000L, 60000L * paramInt);
    this.timer.schedule(new TimerTask()
    {
      public void run()
      {
        synchronized (SyncManager.this.lock)
        {
          if ((SyncManager.syncer != null) && (SyncManager.syncer.isRunInBackGround()) && (SyncManager.syncer.getStartTime() < System.currentTimeMillis() - 120000L))
          {
            SyncManager.syncer.cancel(true);
            SyncManager.access$102(null);
            L.d(this, "calcel a background syncer task.");
          }
          return;
        }
      }
    }
    , 60000L, 120000L);
    L.d(this, "sync period reset to " + paramInt + " minutes");
  }

  public boolean startSync(boolean paramBoolean)
  {
    synchronized (this.lock)
    {
      if ((syncer != null) && (AsyncTask.Status.FINISHED != syncer.getStatus()))
      {
        L.d(this, "syncer is running or pendding, return false.");
        L.d(this, "syncer status " + syncer.getStatus());
        syncer.cancel(true);
      }
      L.d(this, "Start to sync.");
      syncer = new Syncer(this.mTaskManager, this.mDataSource, paramBoolean);
      syncer.setOnSyncFinishListener(new Syncer.SyncFinishListener()
      {
        public void onSyncFinished()
        {
          SyncManager.access$102(null);
        }
      });
      syncer.execute(EmptyInstance.EMPTY_VOIDS);
      return true;
    }
  }

  public void stopAutoSync()
  {
    synchronized (this.lock)
    {
      if (syncer != null)
        syncer.cancel(true);
      if (this.timer != null)
        this.timer.cancel();
      return;
    }
  }

  public void stopSync()
  {
    synchronized (this.lock)
    {
      if (syncer != null)
      {
        syncer.cancel(true);
        syncer = null;
        this.mTaskManager.stopAll();
      }
      return;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.SyncManager
 * JD-Core Version:    0.5.4
 */