package com.youdao.note.task.local;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.ImageResource;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.L;

public abstract class AddImageResourceTask extends AbsAddResourceTask<ImageResourceMeta>
{
  private ImageResourceMeta meta;
  private int picFrom;

  public AddImageResourceTask(Uri paramUri, int paramInt)
  {
    this(new Uri[] { paramUri }, paramInt, null);
  }

  public AddImageResourceTask(Uri[] paramArrayOfUri)
  {
    this(paramArrayOfUri, 1, null);
  }

  public AddImageResourceTask(Uri[] paramArrayOfUri, int paramInt, ImageResourceMeta paramImageResourceMeta)
  {
    super(paramArrayOfUri);
    this.meta = paramImageResourceMeta;
    this.picFrom = paramInt;
  }

  protected ImageResourceMeta doAddResource(Uri paramUri)
    throws Exception
  {
    if (this.meta == null);
    for (ImageResource localImageResource = ResourceUtils.genEmptyImageResource(FileUtils.getFileNameFromUri(paramUri)); ; localImageResource = this.datasource.getImageResource(this.meta))
    {
      Bitmap localBitmap = FileUtils.getBitmapFromUri(paramUri);
      localImageResource.setContentBytes(ImageUtils.bitmap2bytes(localBitmap, Bitmap.CompressFormat.JPEG, 100));
      if (localBitmap == null)
        break;
      ((ImageResourceMeta)localImageResource.getMeta()).setPicFrom(this.picFrom);
      ImageUtils.persistImage(localImageResource, localBitmap, this.ynote.getImageQuality(), true);
      if (this.picFrom == 1)
        ((ImageResourceMeta)localImageResource.getMeta()).copyTempPicture(paramUri);
      return (ImageResourceMeta)localImageResource.getMeta();
    }
    L.d(this, "Got null for image");
    return null;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.local.AddImageResourceTask
 * JD-Core Version:    0.5.4
 */