package com.youdao.note.task.local;

import android.net.Uri;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.FileUtils;
import java.io.File;

public abstract class AddOtherResourceTask extends AbsAddResourceTask<BaseResourceMeta>
{
  private int type;

  public AddOtherResourceTask(Uri paramUri, int paramInt)
  {
    super(paramUri);
    this.type = paramInt;
  }

  protected BaseResourceMeta doAddResource(Uri paramUri)
    throws Exception
  {
    BaseResourceMeta localBaseResourceMeta = ResourceUtils.genEmptyResourceMeta(this.type);
    if (localBaseResourceMeta instanceof ImageResourceMeta)
      throw new RuntimeException("You should use AddImageResourceTask");
    localBaseResourceMeta.setFileName(FileUtils.getFileNameFromUri(paramUri));
    String str = this.datasource.getResourcePath(localBaseResourceMeta);
    FileUtils.copyFile(paramUri, str);
    localBaseResourceMeta.setLength(new File(str).length());
    return localBaseResourceMeta;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.local.AddOtherResourceTask
 * JD-Core Version:    0.5.4
 */