package com.youdao.note.task.local;

import android.net.Uri;
import android.os.Handler;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.LocalTask;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;

public abstract class AbsAddResourceTask<T extends BaseResourceMeta> extends LocalTask<T, T>
{
  protected DataSource datasource = this.ynote.getDataSource();
  private Handler mHandler = new Handler();
  protected Uri[] uriArray;
  protected YNoteApplication ynote = YNoteApplication.getInstance();

  public AbsAddResourceTask(Uri paramUri)
  {
    this(new Uri[] { paramUri });
  }

  public AbsAddResourceTask(Uri[] paramArrayOfUri)
  {
    this.uriArray = paramArrayOfUri;
  }

  protected abstract T doAddResource(Uri paramUri)
    throws Exception;

  protected T onExecute()
    throws Exception
  {
    BaseResourceMeta localBaseResourceMeta = null;
    Uri[] arrayOfUri = this.uriArray;
    int i = arrayOfUri.length;
    int j = 0;
    if (j < i)
    {
      label13: Uri localUri = arrayOfUri[j];
      if (!isCancelled())
      {
        long l = FileUtils.getFileSize(localUri);
        L.d(this, "file size is " + l + "  max size is " + this.ynote.getMaxResourceSize());
        if (l <= this.ynote.getMaxResourceSize())
          break label124;
        String str = FileUtils.getFileNameFromUri(localUri);
        this.mHandler.post(new Runnable(str, l)
        {
          public void run()
          {
            AbsAddResourceTask.this.onResouceTooLarge(this.val$fileName, this.val$fileSize);
          }
        });
      }
      while (true)
      {
        ++j;
        break label13:
        label124: localBaseResourceMeta = doAddResource(localUri);
        publishProgress(new BaseResourceMeta[] { localBaseResourceMeta });
      }
    }
    return localBaseResourceMeta;
  }

  protected abstract void onFinishAll(T paramT);

  protected abstract void onFinishOne(T paramT);

  protected void onProgressUpdate(T[] paramArrayOfT)
  {
    if ((paramArrayOfT == null) || (paramArrayOfT.length <= 0))
      return;
    onFinishOne(paramArrayOfT[0]);
  }

  protected abstract void onResouceTooLarge(String paramString, long paramLong);

  protected void onSucceed(T paramT)
  {
    onFinishAll(paramT);
    L.d(this, "succeed called.");
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.local.AbsAddResourceTask
 * JD-Core Version:    0.5.4
 */