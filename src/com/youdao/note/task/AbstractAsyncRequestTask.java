package com.youdao.note.task;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.text.TextUtils;
import com.youdao.note.YNoteApplication;
import com.youdao.note.exceptions.NetworkNotAvaliableException;
import com.youdao.note.exceptions.ServerException;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.Consts.APIS;
import com.youdao.note.utils.Consts.Request;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.IOUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.WebUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpParams;

public abstract class AbstractAsyncRequestTask<T> extends AsyncTaskWithExecuteResult<String, Integer, T>
  implements Consts.Request, Consts.APIS, Consts, IManageableTask, EmptyInstance
{
  private static final String[] APN_PROJECTION;
  private static final Uri APN_URI = Uri.parse("content://telephony/carriers/preferapn");
  private static HttpClient mClient;
  protected RequestManager mManager = null;
  private HttpPost mPost = null;
  protected String mRequestUrl = null;
  private int mStatusCode = -1;
  protected boolean needAuth = true;

  static
  {
    APN_PROJECTION = new String[] { "apn", "proxy", "port" };
    mClient = AndroidHttpClient.newInstance("ynote-android");
  }

  public AbstractAsyncRequestTask(String paramString)
  {
    this(paramString, true);
  }

  public AbstractAsyncRequestTask(String paramString, boolean paramBoolean)
  {
    this.mRequestUrl = paramString;
    this.needAuth = paramBoolean;
  }

  private void addCookie(HttpPost paramHttpPost)
  {
    String str1 = YNoteApplication.getInstance().getPersistCookie();
    String str2 = YNoteApplication.getInstance().getSessionCookie();
    L.d(this, "persist cookie is " + str1 + "\n session cookie is " + str2);
    paramHttpPost.addHeader("cookie", str1 + ";" + str2);
  }

  private void checkSessionCookie(HttpResponse paramHttpResponse)
  {
    String str = WebUtils.getSessionCookie(paramHttpResponse);
    if (TextUtils.isEmpty(str))
      return;
    YNoteApplication.getInstance().setSessionCookie(str);
  }

  private void dump(HttpPost paramHttpPost)
  {
    UrlEncodedFormEntity localUrlEncodedFormEntity;
    if (paramHttpPost.getEntity() instanceof UrlEncodedFormEntity)
      localUrlEncodedFormEntity = (UrlEncodedFormEntity)paramHttpPost.getEntity();
    try
    {
      L.d(this, IOUtils.toString(localUrlEncodedFormEntity.getContent()));
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  private void setUpApn(HttpClient paramHttpClient)
  {
    String[] arrayOfString = getApn();
    if (arrayOfString != null)
    {
      String str1 = arrayOfString[0];
      String str2 = arrayOfString[1];
      if (!TextUtils.isEmpty(str1))
      {
        int i = 80;
        if (!TextUtils.isEmpty(str2))
          i = Integer.parseInt(str2);
        addProxy(paramHttpClient, str1, i);
        return;
      }
    }
    YNoteApplication.getInstance().setIsCmwap(false);
    paramHttpClient.getParams().setParameter("http.route.default-proxy", null);
  }

  public void addProxy(HttpClient paramHttpClient, String paramString, int paramInt)
  {
    L.d(this, "host " + paramString + " port " + paramInt);
    if ("10.0.0.172".equals(paramString.trim()))
      YNoteApplication.getInstance().setIsCmwap(true);
    while (true)
    {
      HttpHost localHttpHost = new HttpHost(paramString, paramInt);
      paramHttpClient.getParams().setParameter("http.route.default-proxy", localHttpHost);
      return;
      YNoteApplication.getInstance().setIsCmwap(false);
    }
  }

  public void execute()
  {
    super.execute(EMPTY_STRINGS);
  }

  protected T failed(int paramInt, HttpResponse paramHttpResponse, Exception paramException)
  {
    L.e(this, "Returned status code is " + paramInt, paramException);
    if (paramHttpResponse == null);
    try
    {
      L.e(this, "PostMetod is null.", null);
      onFailed(paramException);
      return null;
      L.d(this, "response " + paramHttpResponse.getStatusLine().getStatusCode());
      L.d(this, "return content is " + IOUtils.toString(getResponseAsInputStream(paramHttpResponse)));
    }
    catch (Exception localException)
    {
      L.e(this, "Failed to log response.", paramException);
    }
  }

  protected String[] getApn()
  {
    Cursor localCursor = YNoteApplication.getInstance().getContentResolver().query(APN_URI, APN_PROJECTION, null, null, null);
    if (localCursor != null);
    try
    {
      if (localCursor.moveToFirst())
      {
        String str1 = localCursor.getString(1);
        String str2 = localCursor.getString(2);
        if (TextUtils.isEmpty(str2))
          str2 = "80";
        if (!TextUtils.isEmpty(str1))
        {
          String[] arrayOfString = { str1, str2 };
          return arrayOfString;
        }
      }
      return null;
    }
    finally
    {
      localCursor.close();
    }
  }

  protected HttpPost getPostMethod()
    throws Exception
  {
    L.d(this, "Request url is " + this.mRequestUrl);
    HttpPost localHttpPost = new HttpPost(this.mRequestUrl);
    if (this.needAuth)
      addCookie(localHttpPost);
    if (needGzip())
      localHttpPost.addHeader("Accept-Encoding", "gzip");
    localHttpPost.addHeader("Accept-Charset", "GBK,utf-8;q=0.7,*;q=0.3");
    return localHttpPost;
  }

  protected InputStream getResponseAsInputStream(HttpResponse paramHttpResponse)
    throws IOException
  {
    Object localObject = paramHttpResponse.getEntity().getContent();
    if (isGzip(paramHttpResponse.getFirstHeader("Content-Encoding")))
    {
      L.d(this, "Use gzip!");
      localObject = new GZIPInputStream((InputStream)localObject);
    }
    return (InputStream)localObject;
  }

  protected T handleResponse(InputStream paramInputStream)
    throws Exception
  {
    String str = new String(IOUtils.toByteArray(paramInputStream), "utf-8");
    L.d(this, "Got response " + str);
    return handleResponse(str);
  }

  protected T handleResponse(String paramString)
    throws Exception
  {
    return null;
  }

  protected T handleResponse(HttpResponse paramHttpResponse)
    throws Exception
  {
    return handleResponse(getResponseAsInputStream(paramHttpResponse));
  }

  protected T innerRun(String[] paramArrayOfString)
    throws Exception
  {
    HttpResponse localHttpResponse = null;
    try
    {
      boolean bool;
      throw new NetworkNotAvaliableException("Network not avaliable", this.mRequestUrl);
    }
    finally
    {
      if (localHttpResponse != null)
        localHttpResponse.getEntity().consumeContent();
      if (this.mManager != null)
        this.mManager.removeTask(this);
    }
    this.mPost = getPostMethod();
    HttpPost localHttpPost = this.mPost;
    Object localObject3;
    if (localHttpPost == null)
    {
      if (0 != 0)
        null.getEntity().consumeContent();
      RequestManager localRequestManager3 = this.mManager;
      localObject3 = null;
      if (localRequestManager3 != null)
        this.mManager.removeTask(this);
    }
    do
    {
      RequestManager localRequestManager2;
      do
      {
        return localObject3;
        L.d(this, "Start to execute task " + this.mRequestUrl);
        if (!isCancelled())
          break label204;
        L.d(this, "Task has been cancelled.");
        if (0 != 0)
          null.getEntity().consumeContent();
        localRequestManager2 = this.mManager;
        localObject3 = null;
      }
      while (localRequestManager2 == null);
      this.mManager.removeTask(this);
      return null;
      label204: dump(this.mPost);
      NetworkInfo localNetworkInfo = YNoteApplication.getInstance().getActiveNetworkInfo();
      localHttpResponse = null;
      if (localNetworkInfo != null)
      {
        int i = localNetworkInfo.getType();
        localHttpResponse = null;
        if (i != 1)
          setUpApn(mClient);
      }
      while (true)
      {
        localHttpResponse = mClient.execute(this.mPost);
        this.mStatusCode = localHttpResponse.getStatusLine().getStatusCode();
        if (!isCancelled())
          break;
        L.d(this, "Task has been cancelled.");
        if (localHttpResponse != null)
          localHttpResponse.getEntity().consumeContent();
        RequestManager localRequestManager1 = this.mManager;
        localObject3 = null;
        if (localRequestManager1 != null);
        this.mManager.removeTask(this);
        return null;
        YNoteApplication.getInstance().setIsCmwap(false);
        mClient.getParams().setParameter("http.route.default-proxy", null);
      }
      if ((200 > this.mStatusCode) || (this.mStatusCode >= 300))
        break label426;
      checkSessionCookie(localHttpResponse);
      Object localObject2 = handleResponse(localHttpResponse);
      localObject3 = localObject2;
      if (localHttpResponse == null)
        continue;
      localHttpResponse.getEntity().consumeContent();
    }
    while (this.mManager == null);
    this.mManager.removeTask(this);
    return localObject3;
    label426: throw new ServerException(this.mStatusCode, IOUtils.toString(getResponseAsInputStream(localHttpResponse)));
  }

  protected boolean isGzip(Header paramHeader)
  {
    return (paramHeader != null) && (paramHeader.getValue().equalsIgnoreCase("gzip"));
  }

  protected boolean needGzip()
  {
    return true;
  }

  public void setRequestManager(RequestManager paramRequestManager)
  {
    this.mManager = paramRequestManager;
  }

  public boolean stop(boolean paramBoolean)
  {
    if (this.mPost != null)
      this.mPost.abort();
    return super.cancel(paramBoolean);
  }

  public T syncExecute()
  {
    Object localObject = doInBackground(EMPTY_STRINGS);
    onPostExecute(localObject);
    return localObject;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.AbstractAsyncRequestTask
 * JD-Core Version:    0.5.4
 */