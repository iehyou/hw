package com.youdao.note.task;

import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.BaseData;
import com.youdao.note.data.ListNoteMetas;
import com.youdao.note.data.LocalErrorData;
import com.youdao.note.data.LoginResult;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.PathCollection;
import com.youdao.note.data.ProgressData;
import com.youdao.note.data.RemoteErrorData;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.UpdateCheckResult;
import com.youdao.note.data.UserMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.ServerException;
import com.youdao.note.task.network.CheckUpdateTask;
import com.youdao.note.task.network.DelFileTask;
import com.youdao.note.task.network.GetFileMetaTask;
import com.youdao.note.task.network.GetNoteTask;
import com.youdao.note.task.network.GetResourceTask;
import com.youdao.note.task.network.GetUserMetaTask;
import com.youdao.note.task.network.LoginTask;
import com.youdao.note.task.network.MailShareTask;
import com.youdao.note.task.network.MetaUpdateTask;
import com.youdao.note.task.network.MoveNoteTask;
import com.youdao.note.task.network.PutNoteTask;
import com.youdao.note.task.network.PutResourceTask;
import com.youdao.note.task.network.SearchTask;
import com.youdao.note.utils.Consts.APIS;
import com.youdao.note.utils.Consts.DATA_TYPE;
import com.youdao.note.utils.IpUtils;
import com.youdao.note.utils.L;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.http.entity.mime.MultipartUploadListener;
import org.json.JSONException;
import org.json.JSONObject;

public class TaskManager
  implements Consts.DATA_TYPE, Consts.APIS
{
  private static TaskManager mInstance = null;
  private Map<String, Boolean> downloadingResourceMap = new HashMap();
  private List<DataUpdateListener> listeners = new LinkedList();
  private DataSource mDataSource = null;
  private RequestManager mManager = null;
  private YNoteApplication yNote = null;

  public TaskManager(DataSource paramDataSource)
  {
    this.mDataSource = paramDataSource;
    this.yNote = YNoteApplication.getInstance();
    this.mManager = new RequestManager();
    mInstance = this;
  }

  private void checkError(Exception paramException)
  {
    if ((!paramException instanceof ServerException) || (((ServerException)paramException).getErrorCode() != 207))
      return;
    YNoteApplication.getInstance().sendBroadcast(new Intent("com.youdao.note.action.ACCESS_DENIED"));
  }

  private <Params, Progress, Result> void checkTaskResult(boolean paramBoolean, AsyncTaskWithExecuteResult<Params, Progress, Result> paramAsyncTaskWithExecuteResult)
    throws Exception
  {
    if ((paramAsyncTaskWithExecuteResult.isSucceed()) || (paramBoolean))
      return;
    throw paramAsyncTaskWithExecuteResult.getException();
  }

  private <T> boolean executeTask(boolean paramBoolean, AbstractAsyncRequestTask<T> paramAbstractAsyncRequestTask)
  {
    if (!paramBoolean)
    {
      paramAbstractAsyncRequestTask.syncExecute();
      return paramAbstractAsyncRequestTask.isSucceed();
    }
    paramAbstractAsyncRequestTask.execute();
    this.mManager.manageTask(paramAbstractAsyncRequestTask);
    return true;
  }

  private <T> boolean executeTask(boolean paramBoolean, LocalTask<?, T> paramLocalTask)
  {
    if (!paramBoolean)
    {
      paramLocalTask.syncExecute();
      return paramLocalTask.isSucceed();
    }
    paramLocalTask.execute(new Void[0]);
    this.mManager.manageTask(paramLocalTask);
    return true;
  }

  public static TaskManager getInstance()
  {
    return mInstance;
  }

  private boolean isServerVersionNewer(NoteBook paramNoteBook1, NoteBook paramNoteBook2)
  {
    return (paramNoteBook2 == null) || (paramNoteBook2.getVersion() < paramNoteBook1.getVersion());
  }

  public void checkUpdate()
  {
    try
    {
      executeTask(true, new CheckUpdateTask()
      {
        protected void onFailed(Exception paramException)
        {
          TaskManager.this.updateResult(21, new RemoteErrorData(paramException), false);
        }

        public void onSucceed(UpdateCheckResult paramUpdateCheckResult)
        {
          TaskManager.this.updateResult(21, paramUpdateCheckResult, true);
        }
      });
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      L.e(this, "Faild to check version update.", localNameNotFoundException);
      updateResult(21, new RemoteErrorData(localNameNotFoundException), false);
    }
  }

  public void delNote(String paramString, boolean paramBoolean)
    throws Exception
  {
    5 local5 = new DelFileTask(paramString, paramString)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        TaskManager.this.updateResult(12, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(Boolean paramBoolean)
      {
        TaskManager.this.mDataSource.deleteNote(this.val$entryPath);
        TaskManager.this.updateResult(12, null, true);
      }
    };
    executeTask(paramBoolean, local5);
    checkTaskResult(paramBoolean, local5);
  }

  public void login(String paramString1, String paramString2)
  {
    executeTask(true, new LoginTask(paramString1, paramString2, IpUtils.getLocalIpAddress())
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.updateResult(11, new RemoteErrorData(paramException), false);
      }

      protected void onSucceed(LoginResult paramLoginResult)
      {
        TaskManager.this.updateResult(11, paramLoginResult, true);
      }
    });
  }

  public void mailShare(NoteMeta paramNoteMeta, String paramString1, String paramString2, String paramString3)
  {
    executeTask(true, new MailShareTask(paramNoteMeta, paramString1, paramString2, paramString3)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.updateResult(17, new LocalErrorData(paramException), false);
        L.e(this, "", paramException);
        super.onFailed(paramException);
      }

      protected void onSucceed(Boolean paramBoolean)
      {
        TaskManager.this.updateResult(17, null, true);
      }
    });
  }

  public boolean moveNote(NoteMeta paramNoteMeta, boolean paramBoolean)
    throws Exception
  {
    L.d(this, "Try to move " + paramNoteMeta.getTitle());
    15 local15 = new MoveNoteTask(paramNoteMeta.getEntryPath(), paramNoteMeta.getNoteBook(), paramNoteMeta)
    {
      public void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        if ((!paramException instanceof ServerException) || (((ServerException)paramException).getErrorCode() != 209))
          return;
        clearError();
        TaskManager.this.mDataSource.deleteNote(this.val$noteMeta);
      }

      public void onSucceed(Integer paramInteger)
      {
        this.val$noteMeta.setVersion(paramInteger.intValue());
        this.val$noteMeta.setServerNoteBook(this.val$noteMeta.getNoteBook());
        TaskManager.this.mDataSource.insertOrUpdateNoteMeta(this.val$noteMeta);
      }
    };
    executeTask(paramBoolean, local15);
    checkTaskResult(paramBoolean, local15);
    return local15.isSucceed();
  }

  public void pullNote(NoteMeta paramNoteMeta, boolean paramBoolean)
  {
    executeTask(paramBoolean, new GetNoteTask(paramNoteMeta, paramBoolean)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        if (!this.val$async)
          return;
        TaskManager.this.updateResult(2, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(Note paramNote)
      {
        if (!this.val$async)
          return;
        TaskManager.this.updateResult(2, paramNote, true);
      }
    });
  }

  public void pullNoteIfNeed(NoteMeta paramNoteMeta)
  {
    if (this.mDataSource.getNote(paramNoteMeta) == null)
      this.mDataSource.removeNoteContentVersion(paramNoteMeta.getNoteId());
    executeTask(true, new GetFileMetaTask(paramNoteMeta.getEntryPath(), -1, paramNoteMeta)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        TaskManager.this.updateResult(14, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(PathCollection paramPathCollection)
      {
        if (paramPathCollection.size() > 0)
        {
          NoteMeta localNoteMeta = paramPathCollection.getFirstNoteMeta();
          int i = TaskManager.this.mDataSource.getNoteContentVersion(this.val$noteMeta.getNoteId());
          L.d(TaskManager.this, "Local content version is " + i);
          if (localNoteMeta.getVersion() <= i)
            break label83;
          L.d(TaskManager.this, "Start to pull note.");
          TaskManager.this.pullNote(localNoteMeta, true);
        }
        return;
        label83: TaskManager.this.updateResult(14, null, true);
        L.d(TaskManager.this, "Local content is the newest version.");
      }
    });
  }

  public void pullNotes(boolean paramBoolean)
  {
    executeTask(paramBoolean, new GetUserMetaTask(paramBoolean)
    {
      public void onFailed(Exception paramException)
      {
        TaskManager.this.updateResult(5, new RemoteErrorData(paramException), false);
        TaskManager.this.checkError(paramException);
      }

      public void onSucceed(UserMeta paramUserMeta)
      {
        String str = TaskManager.this.yNote.getUserName();
        NoteBook localNoteBook;
        if (TaskManager.this.mDataSource.insertOrUpdateUserMeta(str, paramUserMeta))
        {
          localNoteBook = TaskManager.this.mDataSource.getRootMeta();
          L.d(TaskManager.this, "local root version is " + localNoteBook);
          if (localNoteBook != null)
            break label100;
        }
        for (int i = -1; ; i = localNoteBook.getVersion())
        {
          1 local1 = new GetFileMetaTask("/", i, paramUserMeta)
          {
            boolean resolveResult = true;

            protected PathCollection handleResponse(String paramString)
              throws JSONException
            {
              PathCollection localPathCollection = super.handleResponse(paramString);
              this.resolveResult = NoteResolver.getInstance(TaskManager.this.mDataSource).resolveNotes(this.val$userMeta, localPathCollection);
              return localPathCollection;
            }

            public void onSucceed(PathCollection paramPathCollection)
            {
              if (!TaskManager.11.this.val$async)
                return;
              TaskManager.this.updateResult(1, null, this.resolveResult);
            }
          };
          TaskManager.this.executeTask(this.val$async, local1);
          label100: return;
        }
      }
    });
  }

  public boolean pullNotesIfNecessary(boolean paramBoolean)
    throws Exception
  {
    12 local12 = new CheckNeedPullTask(paramBoolean)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        L.e(TaskManager.this, "check need pull failed. async is " + this.val$async, paramException);
        if (!this.val$async)
          return;
        TaskManager.this.updateResult(20, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(PathCollection paramPathCollection)
      {
        NoteBook localNoteBook1 = paramPathCollection.getRoot();
        if (localNoteBook1 == null)
          return;
        NoteBook localNoteBook2 = TaskManager.this.mDataSource.getRootMeta();
        L.d(this, localNoteBook2 + "");
        L.d(this, localNoteBook1.toString());
        this.mNeedPull = TaskManager.this.isServerVersionNewer(localNoteBook1, localNoteBook2);
        if (this.mNeedPull)
        {
          L.d(TaskManager.this, "Server version has newer version, start to sync.");
          TaskManager.this.pullNotes(this.val$async);
          return;
        }
        L.d(TaskManager.this, "Data base is the neweast version.");
      }
    };
    executeTask(paramBoolean, local12);
    checkTaskResult(paramBoolean, local12);
    return local12.isNeedPull();
  }

  public void pullResource(BaseResourceMeta paramBaseResourceMeta)
  {
    if (this.downloadingResourceMap.get(paramBaseResourceMeta.getResourceId()) != null)
      return;
    this.downloadingResourceMap.put(paramBaseResourceMeta.getResourceId(), Boolean.valueOf(true));
    executeTask(true, new GetResourceTask(this.mDataSource.getResourcePath(paramBaseResourceMeta), paramBaseResourceMeta, 0, 0, paramBaseResourceMeta)
    {
      ProgressData progress = new ProgressData(this.val$meta.getResourceId(), 0, this.val$meta.getLength());

      protected void onFailed(Exception paramException)
      {
        TaskManager.this.updateResult(9, new RemoteErrorData(paramException), false);
      }

      public void onPostExecute(File paramFile)
      {
        super.onPostExecute(paramFile);
        TaskManager.this.downloadingResourceMap.remove(this.val$meta.getResourceId());
      }

      protected void onProgressUpdate(Integer[] paramArrayOfInteger)
      {
        this.progress.setProgress(paramArrayOfInteger[0].intValue());
        TaskManager.this.updateResult(8, this.progress, true);
      }

      public void onSucceed(File paramFile)
      {
        TaskManager.this.updateResult(9, this.progress, true);
      }
    });
  }

  public void pullThumbnail(Thumbnail paramThumbnail)
  {
    executeTask(true, new GetResourceTask(this.mDataSource.getThumbnailPath(paramThumbnail.getImageMeta()), paramThumbnail.getImageMeta(), 800, 800, paramThumbnail)
    {
      public void onFailed(Exception paramException)
      {
        TaskManager.this.updateResult(5, new RemoteErrorData(paramException), false);
        TaskManager.this.checkError(paramException);
      }

      public void onSucceed(File paramFile)
      {
        TaskManager.this.updateResult(5, this.val$thumbnail, true);
      }
    });
  }

  public void pullUserMeta(boolean paramBoolean)
  {
    executeTask(paramBoolean, new GetUserMetaTask()
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        TaskManager.this.updateResult(13, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(UserMeta paramUserMeta)
      {
        TaskManager.this.updateResult(13, paramUserMeta, true);
      }
    });
  }

  public boolean pushNote(Note paramNote, Map<String, BaseResourceMeta> paramMap)
    throws Exception
  {
    if (paramNote == null)
      return true;
    14 local14 = new PutNoteTask(paramNote, paramMap, paramNote.getNoteMeta(), paramNote)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        if (paramException instanceof ServerException)
        {
          if (((ServerException)paramException).getErrorCode() == 225)
          {
            TaskManager.this.mDataSource.deleteNote(this.val$note.getEntryPath());
            clearError();
            TaskManager.this.updateResult(16, null, true);
          }
          return;
        }
        TaskManager.this.updateResult(16, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(Integer paramInteger)
      {
        this.val$noteMeta.setVersion(paramInteger.intValue());
        this.val$noteMeta.setDirty(false);
        TaskManager.this.mDataSource.insertOrUpdateNoteMeta(this.val$noteMeta);
      }
    };
    executeTask(false, local14);
    checkTaskResult(false, local14);
    return local14.isSucceed();
  }

  public boolean pushResource(BaseResourceMeta paramBaseResourceMeta, MultipartUploadListener paramMultipartUploadListener)
    throws Exception
  {
    L.d(this, "try to push resource " + paramBaseResourceMeta.getResourceId());
    13 local13 = new PutResourceTask(this.mDataSource.getResource(paramBaseResourceMeta), paramBaseResourceMeta.getResourceId(), paramMultipartUploadListener, paramBaseResourceMeta)
    {
      public void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
      }

      public void onSucceed(JSONObject paramJSONObject)
      {
        try
        {
          this.val$meta.setVersion(paramJSONObject.getInt("version"));
          this.val$meta.setLength(paramJSONObject.getLong("sz"));
          if (paramJSONObject.has("src"))
            this.val$meta.setSrc(paramJSONObject.getString("src"));
          TaskManager.this.mDataSource.insertOrUpdateResourceMeta(this.val$meta);
          return;
        }
        catch (JSONException localJSONException)
        {
          L.e(this, "Server return data error", localJSONException);
        }
      }
    };
    L.d(this, "put resource result is " + local13.isSucceed());
    L.d(this, "exception is " + local13.getException());
    executeTask(false, local13);
    checkTaskResult(false, local13);
    return local13.isSucceed();
  }

  public void registDataListener(DataUpdateListener paramDataUpdateListener)
  {
    synchronized (this.listeners)
    {
      this.listeners.add(paramDataUpdateListener);
      return;
    }
  }

  public void searchNotes(String paramString)
  {
    executeTask(true, new SearchTask(paramString)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        TaskManager.this.updateResult(10, new RemoteErrorData(paramException), false);
      }

      protected void onSucceed(ListNoteMetas paramListNoteMetas)
      {
        TaskManager.this.updateResult(10, paramListNoteMetas, true);
      }
    });
  }

  public void stopAll()
  {
    this.mManager.stopAll();
    this.downloadingResourceMap.clear();
  }

  public void unregistDataListener(DataUpdateListener paramDataUpdateListener)
  {
    synchronized (this.listeners)
    {
      this.listeners.remove(paramDataUpdateListener);
      return;
    }
  }

  public void updateMeta(NoteBook paramNoteBook, boolean paramBoolean)
    throws Exception
  {
    4 local4 = new MetaUpdateTask(paramNoteBook, paramNoteBook)
    {
      protected void onFailed(Exception paramException)
      {
        TaskManager.this.checkError(paramException);
        if ((this.val$noteBook.isDeleted()) && (paramException instanceof ServerException))
        {
          TaskManager.this.mDataSource.deleteNoteBook(this.val$noteBook);
          clearError();
          TaskManager.this.updateResult(16, null, true);
          return;
        }
        TaskManager.this.updateResult(16, new RemoteErrorData(paramException), false);
      }

      public void onSucceed(Integer paramInteger)
      {
        if (this.val$noteBook.isDeleted())
          TaskManager.this.mDataSource.deleteNoteBook(this.val$noteBook);
        TaskManager.this.updateResult(16, null, true);
      }
    };
    executeTask(paramBoolean, local4);
    checkTaskResult(paramBoolean, local4);
  }

  public void updateResult(int paramInt, BaseData paramBaseData, boolean paramBoolean)
  {
    synchronized (this.listeners)
    {
      Iterator localIterator = this.listeners.iterator();
      if (localIterator.hasNext())
        ((DataUpdateListener)localIterator.next()).onUpdate(paramInt, paramBaseData, paramBoolean);
    }
    monitorexit;
  }

  private class CheckNeedPullTask extends GetFileMetaTask
  {
    protected boolean mNeedPull = true;

    private CheckNeedPullTask()
    {
    }

    public boolean isNeedPull()
    {
      return this.mNeedPull;
    }
  }

  public static abstract interface DataUpdateListener
  {
    public abstract void onUpdate(int paramInt, BaseData paramBaseData, boolean paramBoolean);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.TaskManager
 * JD-Core Version:    0.5.4
 */