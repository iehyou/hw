package com.youdao.note.task;

import com.youdao.note.utils.L;

public abstract class LocalTask<Progress, Result> extends AsyncTaskWithExecuteResult<Void, Progress, Result>
  implements IManageableTask
{
  protected RequestManager mManager;

  protected Result innerRun(Void[] paramArrayOfVoid)
    throws Exception
  {
    try
    {
      L.d(this, "innerRunn called.");
      Object localObject2 = onExecute();
      return localObject2;
    }
    finally
    {
      if (this.mManager != null)
        this.mManager.removeTask(this);
    }
  }

  protected abstract Result onExecute()
    throws Exception;

  public void setRequestManager(RequestManager paramRequestManager)
  {
    this.mManager = paramRequestManager;
  }

  public boolean stop(boolean paramBoolean)
  {
    return super.cancel(paramBoolean);
  }

  public Result syncExecute()
  {
    Object localObject = doInBackground(new Void[0]);
    onPostExecute(localObject);
    return localObject;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.LocalTask
 * JD-Core Version:    0.5.4
 */