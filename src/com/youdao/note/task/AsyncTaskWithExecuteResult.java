package com.youdao.note.task;

import android.os.AsyncTask;
import com.youdao.note.utils.L;

public abstract class AsyncTaskWithExecuteResult<Params, Progress, Result> extends AsyncTask<Params, Progress, Result>
{
  private Exception mException = null;
  private Object mExtraInfo = null;
  private boolean mSucceed = true;

  protected void clearError()
  {
    this.mException = null;
    this.mSucceed = true;
  }

  protected Result doInBackground(Params[] paramArrayOfParams)
  {
    try
    {
      Object localObject = innerRun(paramArrayOfParams);
      return localObject;
    }
    catch (Exception localException)
    {
      L.e(this, "Got error in AsyncTaskWithExecuteResult", localException);
      this.mException = localException;
      this.mSucceed = false;
    }
    return null;
  }

  public Exception getException()
  {
    return this.mException;
  }

  protected Object getExtraErrorInfo()
  {
    return this.mExtraInfo;
  }

  protected abstract Result innerRun(Params[] paramArrayOfParams)
    throws Exception;

  public boolean isSucceed()
  {
    return this.mSucceed;
  }

  protected abstract void onFailed(Exception paramException);

  protected void onPostExecute(Result paramResult)
  {
    L.d(this, "got result is " + paramResult);
    if ((paramResult == null) || (!this.mSucceed))
    {
      onFailed(this.mException);
      return;
    }
    L.d(this, "call on succeed.");
    onSucceed(paramResult);
  }

  protected abstract void onSucceed(Result paramResult);

  public void setExecuteResult(boolean paramBoolean)
  {
    this.mSucceed = paramBoolean;
  }

  protected void setExtracErrorInfo(Object paramObject)
  {
    this.mExtraInfo = paramObject;
  }

  protected abstract Result syncExecute();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.AsyncTaskWithExecuteResult
 * JD-Core Version:    0.5.4
 */