package com.youdao.note.task;

public abstract interface IManageableTask
{
  public abstract void setRequestManager(RequestManager paramRequestManager);

  public abstract boolean stop(boolean paramBoolean);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.IManageableTask
 * JD-Core Version:    0.5.4
 */