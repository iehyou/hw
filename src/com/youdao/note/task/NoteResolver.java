package com.youdao.note.task;

import com.youdao.note.data.Note;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.PathCollection;
import com.youdao.note.data.UserMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.NoteBookNotExistException;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.L;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NoteResolver
{
  private static NoteResolver instance = null;
  private DataSource mDataSource = null;

  private NoteResolver(DataSource paramDataSource)
  {
    this.mDataSource = paramDataSource;
  }

  private void checkConfilct(NoteMeta paramNoteMeta)
  {
    NoteMeta localNoteMeta = this.mDataSource.getNoteMetaById(paramNoteMeta.getNoteId());
    L.d(this, "Server note is " + paramNoteMeta);
    L.d(this, "Found local note is " + localNoteMeta);
    int i;
    if ((localNoteMeta != null) && (localNoteMeta.needSync()))
    {
      i = this.mDataSource.getNoteContentVersion(paramNoteMeta.getNoteId());
      if (!localNoteMeta.isDeleted());
    }
    label244: NoteBook localNoteBook;
    UserMeta localUserMeta;
    do
    {
      return;
      if ((i < 0) || (paramNoteMeta.getVersion() == i))
      {
        paramNoteMeta.setDirty(true);
        return;
      }
      L.d(this, "fount conflict note " + localNoteMeta.getTitle());
      if (this.mDataSource.reGenerateNoteMeta(localNoteMeta))
      {
        Iterator localIterator = this.mDataSource.getResourceMetasByNoteId(paramNoteMeta.getNoteId()).iterator();
        while (true)
        {
          if (!localIterator.hasNext())
            break label244;
          BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
          localBaseResourceMeta.setNoteId(localNoteMeta.getNoteId());
          AbstractResource localAbstractResource = this.mDataSource.getResource(localBaseResourceMeta);
          this.mDataSource.insertOrUpdateResource(localAbstractResource);
        }
        L.d(this, "not fount conflict note " + paramNoteMeta.getTitle());
      }
      localNoteBook = this.mDataSource.getNoteBookMetaById(paramNoteMeta.getNoteBook());
      localUserMeta = this.mDataSource.getUserMeta();
    }
    while ((localNoteBook == null) || (!localNoteBook.isDeleted()));
    paramNoteMeta.setNoteBook(localUserMeta.getDefaultNoteBook());
    paramNoteMeta.setDirty(true);
  }

  private boolean createNoteBooks(List<NoteBook> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      if (!resolveNoteBook((NoteBook)localIterator.next()))
        return false;
    return true;
  }

  private boolean deleteNoteBooks(List<NoteBook> paramList)
  {
    Iterator localIterator1 = paramList.iterator();
    while (localIterator1.hasNext())
    {
      NoteBook localNoteBook1 = (NoteBook)localIterator1.next();
      NoteBook localNoteBook2 = this.mDataSource.getNoteBookMetaById(localNoteBook1.getNoteBookId());
      if (localNoteBook2 != null)
      {
        if (localNoteBook2.isDirty())
        {
          String str1 = IdUtils.genNotebookId();
          NoteBook localNoteBook3 = this.mDataSource.getNoteBookMetaById(localNoteBook1.getNoteBookId());
          localNoteBook3.setNoteBookId(str1);
          this.mDataSource.insertOrUpdateNoteBookMeta(localNoteBook3);
          Iterator localIterator3 = this.mDataSource.listNotesByNotebookAsList(localNoteBook1.getNoteBookId()).iterator();
          while (true)
          {
            if (!localIterator3.hasNext())
              break label413;
            NoteMeta localNoteMeta2 = (NoteMeta)localIterator3.next();
            String str2 = localNoteMeta2.getNoteId();
            Note localNote2;
            if (localNoteMeta2.needSync())
            {
              localNote2 = this.mDataSource.getNote(localNoteMeta2);
              localNote2.setNoteId(IdUtils.genNoteId());
              localNote2.setNoteBookId(str1);
              localNote2.setServerNoteBook(str1);
            }
            try
            {
              this.mDataSource.insertOrUpdateNote(localNote2);
              label185: Iterator localIterator4 = this.mDataSource.getResourceMetasByNoteId(localNoteMeta2.getNoteId()).iterator();
              if (!localIterator4.hasNext())
                break label268;
              BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)localIterator4.next();
              localBaseResourceMeta.setNoteId(localNote2.getNoteId());
              AbstractResource localAbstractResource = this.mDataSource.getResource(localBaseResourceMeta);
              label268: this.mDataSource.insertOrUpdateResource(localAbstractResource);
            }
            catch (IOException localIOException)
            {
              localIOException.printStackTrace();
              break label185:
              this.mDataSource.deleteNoteById(str2);
            }
          }
        }
        Iterator localIterator2 = this.mDataSource.listNotesByNotebookAsList(localNoteBook1.getNoteBookId()).iterator();
        while (localIterator2.hasNext())
        {
          NoteMeta localNoteMeta1 = (NoteMeta)localIterator2.next();
          if (localNoteMeta1.needSync())
          {
            Note localNote1 = this.mDataSource.getNote(localNoteMeta1);
            this.mDataSource.deleteNote(localNoteMeta1);
            localNote1.setNoteBookId(this.mDataSource.getDefaultNoteBookId());
            localNote1.setServerNoteBook(this.mDataSource.getDefaultNoteBookId());
            localNote1.setNoteId(IdUtils.genNoteId());
            try
            {
              this.mDataSource.insertOrUpdateNote(localNote1);
            }
            catch (Exception localException)
            {
            }
          }
          this.mDataSource.deleteNote(localNoteMeta1);
        }
      }
      label413: this.mDataSource.deleteNoteBook(localNoteBook1);
    }
    return true;
  }

  private boolean deleteNotes(List<NoteMeta> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      NoteMeta localNoteMeta = (NoteMeta)localIterator.next();
      checkConfilct(localNoteMeta);
      if (!this.mDataSource.deleteNote(localNoteMeta))
        return false;
    }
    return true;
  }

  public static NoteResolver getInstance(DataSource paramDataSource)
  {
    monitorenter;
    try
    {
      if ((instance == null) || (instance.mDataSource != paramDataSource))
        instance = new NoteResolver(paramDataSource);
      NoteResolver localNoteResolver = instance;
      return localNoteResolver;
    }
    finally
    {
      monitorexit;
    }
  }

  private boolean resolveNoteBook(NoteBook paramNoteBook)
  {
    NoteBook localNoteBook = this.mDataSource.getNoteBookMetaByTitle(paramNoteBook.getTitle());
    if (localNoteBook == null);
    for (boolean bool = false; !this.mDataSource.insertOrUpdateNoteBookMeta(paramNoteBook); bool = localNoteBook.getNoteBookId().equals(paramNoteBook.getNoteBookId()))
      return false;
    if ((localNoteBook != null) && (!bool))
      try
      {
        if (this.mDataSource.moveNotesBetweenNoteBook(localNoteBook.getNoteBookId(), paramNoteBook.getNoteBookId()));
        if (localNoteBook.getVersion() >= 0)
          this.mDataSource.markDeleteNotebook(localNoteBook);
        else
          this.mDataSource.deleteNoteBook(localNoteBook);
      }
      catch (NoteBookNotExistException localNoteBookNotExistException)
      {
        L.e(this, "Shouldn't got exception here.", localNoteBookNotExistException);
      }
    return true;
  }

  private boolean updateDefaultNoteBook(NoteBook paramNoteBook)
  {
    if (paramNoteBook != null)
      return this.mDataSource.insertOrUpdateNoteBookMeta(paramNoteBook);
    return true;
  }

  private boolean updateNoteBooks(List<NoteBook> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      if (!resolveNoteBook((NoteBook)localIterator.next()))
        return false;
    return true;
  }

  private boolean updateNotes(List<NoteMeta> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      NoteMeta localNoteMeta = (NoteMeta)localIterator.next();
      L.d(this, "Update note " + localNoteMeta.getTitle());
      checkConfilct(localNoteMeta);
      if (!this.mDataSource.insertOrUpdateNoteMeta(localNoteMeta))
        return false;
    }
    return true;
  }

  public boolean resolveNotes(UserMeta paramUserMeta, PathCollection paramPathCollection)
  {
    monitorenter;
    while (true)
    {
      NoteBook localNoteBook1;
      Object localObject2;
      ArrayList localArrayList1;
      ArrayList localArrayList2;
      ArrayList localArrayList3;
      ArrayList localArrayList4;
      ArrayList localArrayList5;
      NoteMeta localNoteMeta;
      try
      {
        int i = paramPathCollection.size();
        if (i == 0)
        {
          j = 1;
          return j;
        }
        L.d(this, "start to resolve notes, total " + paramPathCollection.size());
        localNoteBook1 = paramPathCollection.getRoot();
        localObject2 = null;
        localArrayList1 = new ArrayList();
        localArrayList2 = new ArrayList();
        localArrayList3 = new ArrayList();
        localArrayList4 = new ArrayList();
        localArrayList5 = new ArrayList();
        Iterator localIterator1 = paramPathCollection.getNoteMetas().iterator();
        if (!localIterator1.hasNext())
          break label174;
        localNoteMeta = (NoteMeta)localIterator1.next();
        if (!localNoteMeta.isDeleted())
          break label161;
      }
      finally
      {
        monitorexit;
      }
      label161: localArrayList4.add(localNoteMeta);
      continue;
      label174: Iterator localIterator2 = paramPathCollection.getNoteBooks().iterator();
      while (localIterator2.hasNext())
      {
        NoteBook localNoteBook2 = (NoteBook)localIterator2.next();
        if (localNoteBook2.getNoteBookId().equals(paramUserMeta.getDefaultNoteBook()))
          localObject2 = localNoteBook2;
        if (localNoteBook2.isDeleted())
          localArrayList3.add(localNoteBook2);
        if (!this.mDataSource.exsitNoteBook(localNoteBook2.getNoteBookId()))
          localArrayList2.add(localNoteBook2);
        localArrayList5.add(localNoteBook2);
      }
      L.d(this, "notes dispatched. " + localArrayList3.size() + " notebooks deleted.");
      L.d(this, localArrayList2.size() + " notebooks added.");
      L.d(this, localArrayList1.size() + " notes deleted.");
      L.d(this, localArrayList4.size() + " updated.");
      if ((updateDefaultNoteBook(localObject2)) && (createNoteBooks(localArrayList2)))
      {
        L.d(this, "notebooks added.");
        if (deleteNoteBooks(localArrayList3))
        {
          L.d(this, "notebooks deleted.");
          if ((updateNoteBooks(localArrayList5)) && (deleteNotes(localArrayList1)))
          {
            L.d(this, "notemetas deleted.");
            if (updateNotes(localArrayList4))
            {
              L.d(this, "notemetas updated.");
              L.d(this, "try to update root note meta" + localNoteBook1);
              boolean bool = this.mDataSource.updateRootNoteBookMeta(localNoteBook1);
              j = bool;
            }
          }
        }
      }
      int j = 0;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.NoteResolver
 * JD-Core Version:    0.5.4
 */