package com.youdao.note.task;

import com.youdao.note.LogRecorder;
import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.utils.WebUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

public class BaseApiRequestTask<T> extends AbstractAsyncRequestTask<T>
{
  private Object[] mArgs = null;

  public BaseApiRequestTask(String paramString1, String paramString2, Object[] paramArrayOfObject)
  {
    this(paramString1, paramString2, paramArrayOfObject, true);
  }

  public BaseApiRequestTask(String paramString1, String paramString2, Object[] paramArrayOfObject, boolean paramBoolean)
  {
    this(WebUtils.constructRequestUrl("yws/mapi/" + paramString1, paramString2, null), paramBoolean);
    this.mArgs = paramArrayOfObject;
  }

  public BaseApiRequestTask(String paramString, boolean paramBoolean)
  {
    super(paramString, paramBoolean);
  }

  public DataSource getDataSource()
  {
    if (YNoteApplication.getInstance() != null)
      return YNoteApplication.getInstance().getDataSource();
    return null;
  }

  protected HttpPost getPostMethod()
    throws Exception
  {
    HttpPost localHttpPost = super.getPostMethod();
    ArrayList localArrayList = new ArrayList();
    if (this.mArgs != null)
      for (int i = 0; i < this.mArgs.length; i += 2)
      {
        if (this.mArgs[(i + 1)] == null)
          continue;
        localArrayList.add(new BasicNameValuePair(this.mArgs[i].toString(), this.mArgs[(i + 1)].toString()));
      }
    YNoteApplication.getInstance().getLogRecorder().addGeneralParameter(localArrayList);
    localHttpPost.setEntity(new UrlEncodedFormEntity(localArrayList, "utf-8"));
    return localHttpPost;
  }

  public String getUserName()
  {
    return YNoteApplication.getInstance().getUserName();
  }

  protected void onFailed(Exception paramException)
  {
  }

  protected void onSucceed(T paramT)
  {
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.task.BaseApiRequestTask
 * JD-Core Version:    0.5.4
 */