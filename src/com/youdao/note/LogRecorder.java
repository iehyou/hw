package com.youdao.note;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.youdao.note.task.BaseApiRequestTask;
import com.youdao.note.utils.Consts.APIS;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.L;
import com.youdao.note.utils.PreferenceKeys.STAT;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

public class LogRecorder
  implements PreferenceKeys.STAT, EmptyInstance
{
  private static final long DELAY = 5000L;
  public static int LOGIN_STATUS_FIRSTLAUNCH = 0;
  public static int LOGIN_STATUS_FISTLOGIN = 0;
  public static int LOGIN_STATUS_LOGIN = 0;
  public static int LOGIN_STATUS_UNLOGIN = 0;
  private static final long REPORT_INTERVAL = 86400000L;
  private static final String STAT_NAME = "YNote-stat";
  private static final String VENDOR_KEY = "vendor";
  private boolean bSizeSet = false;
  private Object lock = new Object();
  private int mDensityDpi;
  private int mHeight;
  private SharedPreferences mPreference = null;
  private int mWidth;
  private YNoteApplication mYNote = null;
  private ReportTask reporter = null;
  private Timer timer = null;
  private String vendor = null;

  static
  {
    LOGIN_STATUS_LOGIN = 1;
    LOGIN_STATUS_FIRSTLAUNCH = 2;
    LOGIN_STATUS_FISTLOGIN = 3;
  }

  public LogRecorder(Context paramContext)
  {
    this.mYNote = ((YNoteApplication)paramContext);
    this.mPreference = paramContext.getSharedPreferences("YNote-stat", 0);
    try
    {
      this.vendor = this.mYNote.getPackageManager().getApplicationInfo(this.mYNote.getPackageName(), 128).metaData.getString("vendor");
      if ("yz".equals(this.vendor))
      {
        this.vendor = ("yz_" + Build.MANUFACTURER + "_" + getModel());
        L.i(this, "Update vendor to " + this.vendor);
      }
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      L.e(this, "Failed to get vendor from manfiest.", localNameNotFoundException);
      this.vendor = "undefined";
    }
  }

  private void dumpKeyValue(StringBuilder paramStringBuilder, String paramString1, String paramString2)
  {
    paramStringBuilder.append(paramString1 + "=" + paramString2 + "<br/>");
  }

  private String getIMEI()
  {
    String str = this.mPreference.getString("imei", "");
    if (TextUtils.isEmpty(str))
    {
      str = ((TelephonyManager)this.mYNote.getSystemService("phone")).getDeviceId();
      this.mPreference.edit().putString("imei", str);
      L.d(this, "Set imei to " + str);
    }
    return str;
  }

  private long getLastReportTime()
  {
    return this.mPreference.getLong("last_report_time", 0L);
  }

  private String getMid()
  {
    return Build.VERSION.CODENAME + " " + Build.VERSION.RELEASE;
  }

  private String getModel()
  {
    return Build.MODEL;
  }

  private boolean inc(String paramString, int paramInt)
  {
    return this.mPreference.edit().putInt(paramString, paramInt + this.mPreference.getInt(paramString, 0)).commit();
  }

  private boolean incKeys(int paramInt, String[] paramArrayOfString)
  {
    Object localObject1 = this.lock;
    monitorenter;
    boolean bool = true;
    try
    {
      int i = paramArrayOfString.length;
      for (int j = 0; j < i; ++j)
        bool &= inc(paramArrayOfString[j], paramInt);
      return bool;
    }
    finally
    {
      monitorexit;
    }
  }

  private boolean incKeys(String[] paramArrayOfString)
  {
    return incKeys(1, paramArrayOfString);
  }

  private void report()
  {
    YNoteApplication localYNoteApplication = YNoteApplication.getInstance();
    int i = LOGIN_STATUS_UNLOGIN;
    if (localYNoteApplication.isLogin())
      i = LOGIN_STATUS_LOGIN;
    report(i);
  }

  private boolean updateReportTime()
  {
    return this.mPreference.edit().putLong("last_report_time", System.currentTimeMillis()).commit();
  }

  public boolean addArtTimes(int paramInt)
  {
    return incKeys(paramInt, new String[] { "AddArtTimes", "AttachNum" });
  }

  public boolean addCameraNoteTimes()
  {
    L.d(this, "Update view cameraote times.");
    return incKeys(1, new String[] { "CameraNoteTimes" });
  }

  public boolean addCameraTimes(int paramInt)
  {
    L.d(this, "Update add camera times by " + paramInt);
    return incKeys(paramInt, new String[] { "AddCameraTimes", "AttachNum" });
  }

  public boolean addContentCopyTimes()
  {
    return incKeys(new String[] { "ContentCopyTimes" });
  }

  public boolean addEditNoteTimes()
  {
    L.d(this, "update edit note times");
    return incKeys(new String[] { "EditNoteTimes" });
  }

  public void addGeneralParameter(List<NameValuePair> paramList)
  {
    paramList.add(new BasicNameValuePair("imei", getIMEI()));
    paramList.add(new BasicNameValuePair("mid", getMid()));
    paramList.add(new BasicNameValuePair("phoneVersion", "android"));
    paramList.add(new BasicNameValuePair("model", getModel()));
    paramList.add(new BasicNameValuePair("vendor", this.vendor));
    paramList.add(new BasicNameValuePair("keyfrom", this.mYNote.getKeyFrom()));
    paramList.add(new BasicNameValuePair("dpi", Integer.toString(this.mDensityDpi)));
    paramList.add(new BasicNameValuePair("resolution", this.mWidth + "*" + this.mHeight));
    paramList.add(new BasicNameValuePair("size", 1.0D * this.mWidth / this.mDensityDpi + "*" + 1.0D * this.mHeight / this.mDensityDpi));
  }

  public boolean addIconAllTimes()
  {
    return incKeys(new String[] { "IconAllTimes" });
  }

  public boolean addIconNewTimes()
  {
    return incKeys(new String[] { "IconNewTimes" });
  }

  public boolean addIconNotebookTimes()
  {
    return incKeys(new String[] { "IconNotebookTimes" });
  }

  public boolean addIconSyncTimes()
  {
    return incKeys(new String[] { "IconSyncTimes" });
  }

  public boolean addImageRectificationTimes()
  {
    L.d(this, "update image rectification times");
    return incKeys(new String[] { "WbCorrectTimes" });
  }

  public boolean addNote(String paramString1, String paramString2)
  {
    if (paramString2.equals("AppAddTimes"))
    {
      L.d(this, "update add note times,  " + paramString2);
      return incKeys(new String[] { paramString2, "AddNoteTimes" });
    }
    L.d(this, "update add note times, " + paramString1 + " " + paramString2);
    return incKeys(new String[] { paramString1, paramString2, "AddNoteTimes" });
  }

  public boolean addNoteDetailTimes()
  {
    return incKeys(new String[] { "NoteDetailTimes" });
  }

  public boolean addPaintTimes(int paramInt)
  {
    return incKeys(paramInt, new String[] { "AddPaintTimes", "AttachNum" });
  }

  public boolean addPhotoTimes(int paramInt)
  {
    L.d(this, "Update add photo times by " + paramInt);
    return incKeys(paramInt, new String[] { "AddPhotoTimes", "AttachNum" });
  }

  public boolean addRecordFromBarTimes()
  {
    L.d(this, "Update record from bar times.");
    return incKeys(1, new String[] { "RecordFromBar" });
  }

  public boolean addRecordTimes(int paramInt)
  {
    return incKeys(paramInt, new String[] { "AddRecordTimes" });
  }

  public boolean addSearchTimes()
  {
    return incKeys(new String[] { "SearchNoteTimes" });
  }

  public boolean addSendMailTimes()
  {
    return incKeys(new String[] { "SendMailTimes" });
  }

  public boolean addShareMailTimes()
  {
    return incKeys(new String[] { "EditMailTimes" });
  }

  public boolean addTextNoteTimes()
  {
    L.d(this, "Update view textNote times.");
    return incKeys(1, new String[] { "TextNoteTimes" });
  }

  public boolean addViewAttachTimes()
  {
    L.d(this, "Update view attach times.");
    return incKeys(new String[] { "ViewAttachTimes" });
  }

  public boolean addViewDetailTimes()
  {
    L.d(this, "update view note times");
    return incKeys(new String[] { "ViewNoteTimes" });
  }

  public boolean addViewPicTimes()
  {
    L.d(this, "Update view image times.");
    return incKeys(new String[] { "ViewPicTimes" });
  }

  public boolean addWbCorrectTimes()
  {
    return incKeys(new String[] { "WbCorrectTimes" });
  }

  public void clearErrorLog()
  {
    this.mPreference.edit().putString("error", null).commit();
  }

  public String dumpStatistics()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    dumpKeyValue(localStringBuilder, "imei", getIMEI());
    dumpKeyValue(localStringBuilder, "mid", getMid());
    dumpKeyValue(localStringBuilder, "phoneVersion", "android");
    dumpKeyValue(localStringBuilder, "model", getModel());
    dumpKeyValue(localStringBuilder, "vendor", this.vendor);
    dumpKeyValue(localStringBuilder, "keyfrom", this.mYNote.getKeyFrom());
    dumpKeyValue(localStringBuilder, "dpi", Integer.toString(this.mDensityDpi));
    dumpKeyValue(localStringBuilder, "resolution", this.mWidth + "*" + this.mHeight);
    String str1;
    if (this.mYNote.isPinlockEnable())
    {
      str1 = "1";
      label147: dumpKeyValue(localStringBuilder, "PassCodeStatus", str1);
      if (this.mYNote.getAutoSyncPeriod() <= 0)
        break label325;
    }
    for (String str2 = "1"; ; str2 = "0")
    {
      dumpKeyValue(localStringBuilder, "AutoSyncStatus", str2);
      dumpKeyValue(localStringBuilder, "size", 1.0D * this.mWidth / this.mDensityDpi + "*" + 1.0D * this.mHeight / this.mDensityDpi);
      dumpKeyValue(localStringBuilder, "error", getError());
      Iterator localIterator = this.mPreference.getAll().entrySet().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          break label332;
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        dumpKeyValue(localStringBuilder, (String)localEntry.getKey(), localEntry.getValue().toString());
      }
      str1 = "0";
      label325: break label147:
    }
    label332: return localStringBuilder.toString();
  }

  public String getError()
  {
    return this.mPreference.getString("error", null);
  }

  public boolean isEverReport()
  {
    while (true)
    {
      synchronized (this.lock)
      {
        if (this.mPreference.getLong("last_report_time", 0L) <= 0L)
          break label35;
        i = 1;
        return i;
      }
      label35: int i = 0;
    }
  }

  public boolean isSizeSet()
  {
    return this.bSizeSet;
  }

  public void report(int paramInt)
  {
    this.mPreference.edit().putInt("LoginStatus", paramInt).commit();
    this.reporter = new ReportTask();
    this.reporter.execute();
  }

  public void reportIfNecessary()
  {
    if ((!isEverReport()) || (System.currentTimeMillis() - getLastReportTime() <= 86400000L))
      return;
    report();
  }

  public void setError(String paramString)
  {
    this.mPreference.edit().putString("error", paramString).commit();
  }

  public void setHandWriteMode(int paramInt)
  {
    if (paramInt == 1)
    {
      this.mPreference.edit().putString("machine", "FineNWrite").commit();
      return;
    }
    this.mPreference.edit().putString("machine", "BadNWrite").commit();
  }

  public void setScreenParameters(int paramInt1, int paramInt2, int paramInt3)
  {
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    this.mDensityDpi = paramInt3;
    this.bSizeSet = true;
  }

  public void start()
  {
    this.timer = new Timer();
    this.timer.schedule(new TimerTask()
    {
      public void run()
      {
        LogRecorder.this.reportIfNecessary();
      }
    }
    , 5000L, 86400000L);
  }

  public void stop()
  {
    this.timer.cancel();
    if (this.reporter == null)
      return;
    this.reporter.stop(true);
  }

  private class ReportTask extends BaseApiRequestTask<Boolean>
    implements Consts.APIS
  {
    public ReportTask()
    {
      super("ilogrpt", "put", null);
    }

    protected HttpPost getPostMethod()
      throws Exception
    {
      HttpPost localHttpPost = super.getPostMethod();
      ArrayList localArrayList = new ArrayList();
      synchronized (LogRecorder.this.lock)
      {
        Map localMap = LogRecorder.this.mPreference.getAll();
        L.d(LogRecorder.this, "Total parameter size is " + localMap.size());
        Iterator localIterator = localMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)localIterator.next();
          localArrayList.add(new BasicNameValuePair((String)localEntry.getKey(), localEntry.getValue().toString()));
        }
      }
      monitorexit;
      label181: String str1;
      if (LogRecorder.this.getLastReportTime() == 0L)
      {
        localArrayList.add(new BasicNameValuePair("StatDuration", "0"));
        if (!LogRecorder.this.mYNote.isPinlockEnable())
          break label345;
        str1 = "1";
        label198: localArrayList.add(new BasicNameValuePair("PassCodeStatus", str1));
        if (LogRecorder.this.mYNote.getAutoSyncPeriod() <= 0)
          break label352;
      }
      for (String str2 = "1"; ; str2 = "0")
      {
        localArrayList.add(new BasicNameValuePair("AutoSyncStatus", str2));
        if (LogRecorder.this.getError() != null)
          localArrayList.add(new BasicNameValuePair("error", LogRecorder.this.getError()));
        LogRecorder.this.addGeneralParameter(localArrayList);
        localHttpPost.setEntity(new UrlEncodedFormEntity(localArrayList, "utf-8"));
        return localHttpPost;
        localArrayList.add(new BasicNameValuePair("StatDuration", Long.toString((System.currentTimeMillis() - LogRecorder.this.getLastReportTime()) / 3600000L)));
        break label181:
        label345: str1 = "0";
        label352: break label198:
      }
    }

    protected Boolean handleResponse(String paramString)
      throws Exception
    {
      L.d(LogRecorder.this, "Got reponse for log report." + paramString);
      return Boolean.valueOf(true);
    }

    protected void onSucceed(Boolean paramBoolean)
    {
      LogRecorder.this.mPreference.edit().clear().commit();
      LogRecorder.this.mPreference.edit().putInt("AddCameraTimes", 0).putInt("ViewPicTimes", 0).putInt("NoteDetailTimes", 0).putInt("TextNoteTimes", 0).putInt("WbCorrectTimes", 0).putInt("EditNoteTimes", 0).putInt("SearchNoteTimes", 0).putInt("ViewNoteTimes", 0).putInt("CameraNoteTimes", 0).putInt("AddNoteTimes", 0).putInt("AddPhotoTimes", 0).putInt("ViewAttachTimes", 0).putInt("AttachNum", 0).putInt("WidgetAddTimes", 0).putInt("AppAddTimes", 0).putInt("WbCorrectTimes", 0).putInt("AddArtTimes", 0).putInt("AddPaintTimes", 0).putInt("IconNewTimes", 0).putInt("IconSyncTimes", 0).putInt("IconAllTimes", 0).putInt("IconNotebookTimes", 0).putInt("ContentCopyTimes", 0).putInt("RecordNoteTimes", 0).putInt("AddRecordTimes", 0).putInt("RecordFromBar", 0).commit();
      L.d(LogRecorder.this, "Succeed");
      LogRecorder.this.updateReportTime();
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.LogRecorder
 * JD-Core Version:    0.5.4
 */