package com.youdao.note.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.youdao.note.YNoteApplication;
import com.youdao.note.exceptions.RecordBusyException;
import com.youdao.note.ui.audio.YNoteRecorder;
import com.youdao.note.ui.audio.YNoteRecorder.YNoteRecordListener;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UnitUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class RecordService extends Service
{
  public static final String ACTION_START_RECORD = "com.youdao.note.START_RECORD";
  public static final String ACTION_STOP_RECORD = "com.youdao.note.STOP_RECORD";
  private static final long DURATION_UPDATE_INTERVAL = 1000L;
  private static final int MESSAGE_UPDATE_DURATION = 1;
  public static final int RECORDING = 1;
  private static final int SAMPLE_RATE = 8000;
  private final IBinder mBinder = new RecorderBinder();
  private Context mContext;
  private RecordDurationListener mDurationListener;
  private File mFile;
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramMessage)
    {
      if (paramMessage.what != 1)
        return;
      if ((RecordService.this.mRecorder != null) && (RecordService.this.mRecorder.isRecording()))
        sendEmptyMessageDelayed(1, 1000L);
      RecordService.access$114(RecordService.this, 1000L);
      if (RecordService.this.mDurationListener == null)
        return;
      RecordService.this.mDurationListener.onDurationChanged(RecordService.this.mTotalTimeMs);
      RecordService.this.updateNotification(false);
    }
  };
  private long mNotificationTime = -1L;
  private YNoteRecorder mRecorder;
  private long mTotalTimeMs = 0L;

  private void dismissNotification()
  {
    ((NotificationManager)getSystemService("notification")).cancel(1);
    this.mNotificationTime = -1L;
  }

  private void updateNotification()
  {
    updateNotification(true);
  }

  public int getRecordStatus()
  {
    if (this.mRecorder == null)
      return 1;
    return this.mRecorder.getRecordState();
  }

  public void init(Context paramContext, File paramFile, RecordDurationListener paramRecordDurationListener)
    throws IOException
  {
    if (!paramFile.exists())
      throw new FileNotFoundException();
    if (getRecordStatus() != 1)
      throw new RecordBusyException();
    this.mTotalTimeMs = 0L;
    this.mContext = paramContext;
    this.mFile = paramFile;
    this.mDurationListener = paramRecordDurationListener;
    YNoteApplication.getInstance().setIsRecording(true);
  }

  public IBinder onBind(Intent paramIntent)
  {
    L.d(this, "onBind called.");
    return this.mBinder;
  }

  public void pauseRecord()
  {
    L.d(this, "audio record paused.");
    if (this.mRecorder != null)
      this.mRecorder.pause();
    this.mHandler.removeMessages(1);
    updateNotification();
  }

  public void startRecord(YNoteRecorder.YNoteRecordListener paramYNoteRecordListener)
  {
    L.d(this, "audio record started.");
    try
    {
      if (this.mRecorder == null)
        this.mRecorder = new YNoteRecorder(this.mFile, 8000);
      this.mRecorder.setVolumeChangeListener(paramYNoteRecordListener);
      this.mHandler.sendEmptyMessageDelayed(1, 1000L);
      this.mRecorder.start();
      updateNotification();
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }

  public void stopRecord()
  {
    L.d(this, "audio record stoped.");
    if (this.mRecorder != null)
    {
      this.mRecorder.stopRecord();
      this.mRecorder = null;
    }
    this.mHandler.removeMessages(1);
    YNoteApplication.getInstance().setIsRecording(false);
  }

  public void updateNotification(boolean paramBoolean)
  {
    if ((this.mRecorder == null) || (this.mRecorder.getRecordState() == 1))
    {
      L.d(this, "notification dismissed.");
      dismissNotification();
      return;
    }
    if (this.mNotificationTime < 0L)
      this.mNotificationTime = System.currentTimeMillis();
    Intent localIntent = new Intent(this, this.mContext.getClass());
    localIntent.putExtra("entry_from", "statusBar");
    PendingIntent localPendingIntent = PendingIntent.getActivity(this, 0, localIntent, 134217728);
    int i;
    label112: String str;
    label143: Notification localNotification;
    if (this.mRecorder.getRecordState() == 2)
      if (this.mTotalTimeMs / 1000L % 2L == 0L)
      {
        i = 2130837738;
        if (!paramBoolean)
          break label229;
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = UnitUtils.getDuration(this.mTotalTimeMs);
        str = getString(2131361864, arrayOfObject2);
        localNotification = new Notification(i, str, this.mNotificationTime);
        localNotification.setLatestEventInfo(this, getText(2131361861), UnitUtils.getDuration(this.mTotalTimeMs), localPendingIntent);
        localNotification.ledARGB = Color.argb(255, 255, 0, 0);
        localNotification.ledOnMS = 1000;
        localNotification.ledOffMS = 2000;
      }
    while (true)
    {
      startForeground(1, localNotification);
      return;
      i = 2130837739;
      break label112:
      label229: str = null;
      break label143:
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = UnitUtils.getDuration(this.mTotalTimeMs);
      localNotification = new Notification(2130837737, getString(2131361865, arrayOfObject1), this.mNotificationTime);
      localNotification.setLatestEventInfo(this, getText(2131361862), UnitUtils.getDuration(this.mTotalTimeMs), localPendingIntent);
    }
  }

  public static abstract interface RecordDurationListener
  {
    public abstract void onDurationChanged(long paramLong);
  }

  public class RecorderBinder extends Binder
  {
    public RecorderBinder()
    {
    }

    public RecordService getRecordService()
    {
      return RecordService.this;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.service.RecordService
 * JD-Core Version:    0.5.4
 */