package com.youdao.note;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.youdao.note.activity.ActivityConsts;
import com.youdao.note.activity.EditNoteActivity;
import com.youdao.note.activity.MainActivity;
import com.youdao.note.activity.SearchActivity;
import com.youdao.note.data.LoginResult;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.UserMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.exceptions.LoginException;
import com.youdao.note.task.SyncManager;
import com.youdao.note.task.TaskManager;
import com.youdao.note.task.network.LoginTask;
import com.youdao.note.tool.img.ImageProcess;
import com.youdao.note.ui.audio.YNoteRecorder;
import com.youdao.note.ui.skitch.SkitchConsts;
import com.youdao.note.ui.skitch.SkitchMetaManager;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.IpUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.PreferenceKeys;
import com.youdao.note.utils.UIUtilities;
import java.io.File;

public class YNoteApplication extends Application
  implements PreferenceKeys, EmptyInstance, ActivityConsts, Consts
{
  private static final String DEBUG_KEY = "is_debug";
  private static final boolean RESETDB = false;
  private static final String TAG = YNoteApplication.class.getSimpleName();
  public static final String UNLOGIN_NOTEBOOK = "default_notebook";
  public static final String UNLOGIN_USER = "unlogin@unlogin";
  private static YNoteApplication mYNoteInstance = null;
  private boolean isCmwap = false;
  private boolean isRecording = false;
  private DataSource mDataSource = null;
  private LogRecorder mLogRecorder = null;
  private SharedPreferences mSharedPreferences = null;
  private SyncManager mSyncManager = null;
  private TaskManager mTaskManager = null;
  private BroadcastReceiver receiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if (!"com.youdao.note.action.ACCESS_DENIED".equals(paramIntent.getAction()))
        return;
      new LoginTask(YNoteApplication.this.getUserName(), YNoteApplication.this.getPassword(), IpUtils.getLocalIpAddress())
      {
        protected void onFailed(Exception paramException)
        {
          YNoteApplication.this.logOut();
        }

        protected void onSucceed(LoginResult paramLoginResult)
        {
          YNoteApplication.this.setCookie(paramLoginResult);
        }
      }
      .execute();
    }
  };
  private SkitchMetaManager skitchManager = new SkitchMetaManager();

  public static YNoteApplication getInstance()
  {
    return mYNoteInstance;
  }

  public void acceptLicense()
  {
    this.mSharedPreferences.edit().putBoolean("license_accepted", true).commit();
  }

  public void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(paramContext);
  }

  public void clearData()
  {
    boolean bool = isDebug();
    this.mSharedPreferences.edit().clear().commit();
    setDebug(bool);
    Log.d(TAG, "clear cache.");
    this.mDataSource.clearCache();
    Log.d(TAG, "reset data base.");
    this.mDataSource.resetDataBase();
  }

  public NetworkInfo getActiveNetworkInfo()
  {
    return ((ConnectivityManager)getSystemService("connectivity")).getActiveNetworkInfo();
  }

  public int getAutoSyncPeriod()
  {
    String str1 = getResources().getString(2131361802);
    String str2 = this.mSharedPreferences.getString(str1, "-1");
    if (TextUtils.isEmpty(str2))
      return -1;
    return Integer.parseInt(str2);
  }

  public DataSource getDataSource()
  {
    return this.mDataSource;
  }

  public String getDefaultNoteBook()
  {
    if (isLogin())
      return this.mDataSource.getUserMeta().getDefaultNoteBook();
    return "default_notebook";
  }

  public float getDoodlePaintWidth()
  {
    return this.mSharedPreferences.getFloat("doodle_paint_width", SkitchConsts.DEFAULT_PAINT_WIDTH);
  }

  public float getHandWritePaintWidth()
  {
    return this.mSharedPreferences.getFloat("handwrite_paint_width", SkitchConsts.DEFAULT_PAINT_WIDTH);
  }

  public int getHandwriteMode()
  {
    if (!isOpengGL2Available())
      setHandwriteMode(0);
    String str = getResources().getString(2131361806);
    return Integer.parseInt(this.mSharedPreferences.getString(str, "1"));
  }

  public int getHandwriteWordHeight()
  {
    int i = getResources().getDimensionPixelSize(2131296270);
    int j = getResources().getDimensionPixelSize(2131296269);
    if (j < 70)
      j = 70;
    if (i > j)
      return j;
    return i;
  }

  public String getHost()
  {
    if (isDebug())
      return "note.corp.youdao.com/";
    return "note.youdao.com/";
  }

  public int getIgnoreVersion()
  {
    return this.mSharedPreferences.getInt("ignore_version", 0);
  }

  public int getImageQuality()
  {
    String str = getResources().getString(2131361805);
    return Integer.parseInt(this.mSharedPreferences.getString(str, Integer.toString(2)));
  }

  public String getKeyFrom()
  {
    return "note." + getPackageVersionName() + ".android";
  }

  public long getLastCheckUpdateTime()
  {
    return this.mSharedPreferences.getLong("last_chcekupdate", 0L);
  }

  public String getLastLoginUserName()
  {
    return this.mSharedPreferences.getString("last_username", null);
  }

  public LogRecorder getLogRecorder()
  {
    return this.mLogRecorder;
  }

  public long getMaxResourceSize()
  {
    return 25165824L;
  }

  public String getPackageVersionName()
  {
    try
    {
      String str = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
      return str;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return "unknowVersionName";
  }

  public String getPassword()
  {
    return this.mSharedPreferences.getString("password", "");
  }

  public String getPersistCookie()
  {
    return this.mSharedPreferences.getString("cookie", "");
  }

  public String getPinLock()
  {
    return this.mSharedPreferences.getString("pinlock", "");
  }

  public String getSessionCookie()
  {
    return this.mSharedPreferences.getString("session_cookie", "");
  }

  public SkitchMetaManager getSkitchManager()
  {
    return this.skitchManager;
  }

  public File getStoreDir()
  {
    if ("mounted".equalsIgnoreCase(Environment.getExternalStorageState()))
      return Environment.getExternalStorageDirectory();
    return getApplicationContext().getFilesDir();
  }

  public SyncManager getSyncManager()
  {
    return this.mSyncManager;
  }

  public TaskManager getTaskManager()
  {
    return this.mTaskManager;
  }

  public String getUpdateUrl()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (isDebug());
    for (String str = "http://ynote.elibom.youdao.com/noteproxy/update"; ; str = "http://m.note.youdao.com/noteproxy/update")
      return str + "?keyfrom=" + getKeyFrom();
  }

  public String getUserName()
  {
    String str = this.mSharedPreferences.getString(USERNAME, "");
    if (TextUtils.isEmpty(str))
      str = "unlogin@unlogin";
    return str;
  }

  public String getYNoteDBName()
  {
    return getUserName().replace(".", "_") + "_ynote.db";
  }

  public String getYNoteSearchHistryDBName()
  {
    return getUserName().replace(".", "_") + "_search_history.db";
  }

  public boolean isDebug()
  {
    return this.mSharedPreferences.getBoolean("is_debug", false);
  }

  public boolean isEverLogin()
  {
    return this.mSharedPreferences.getBoolean("ever_login", false);
  }

  public boolean isFirstTime()
  {
    return !this.mSharedPreferences.getString("last_use_version", "").equals(getPackageVersionName());
  }

  public boolean isLoadImageLibSuccess()
  {
    return ImageProcess.isLoadImageLibSuccess();
  }

  public boolean isLoadRecortLibSuccess()
  {
    return YNoteRecorder.isLoadLibSuccess();
  }

  public boolean isLogin()
  {
    L.d(this, "cookies is " + getPersistCookie());
    return (!TextUtils.isEmpty(getPersistCookie())) && (getPersistCookie().length() > 1 + "NTES_PASSPORT".length()) && (this.mDataSource.getUserMeta() != null);
  }

  public boolean isNetworkAvailable()
  {
    NetworkInfo localNetworkInfo = getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.isAvailable());
  }

  public boolean isOpengGL2Available()
  {
    return ((ActivityManager)getSystemService("activity")).getDeviceConfigurationInfo().reqGlEsVersion >= 65537;
  }

  public boolean isPinlockEnable()
  {
    String str = getResources().getString(2131361797);
    return this.mSharedPreferences.getBoolean(str, false);
  }

  public boolean isRecording()
  {
    return this.isRecording;
  }

  public boolean isShowHandwriteGuide()
  {
    return this.mSharedPreferences.getBoolean("show_handwrite_guide", true);
  }

  public boolean isShowHandwriteModeTips()
  {
    return this.mSharedPreferences.getBoolean("show_handwrite_mode_tips", true);
  }

  public boolean isShowMailGuide()
  {
    return this.mSharedPreferences.getBoolean("show_mail_guide", true);
  }

  public boolean isUsingCMWAP()
  {
    return this.isCmwap;
  }

  public boolean isWifiAvailable()
  {
    NetworkInfo localNetworkInfo = getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.isAvailable()) && (localNetworkInfo.getType() == 1);
  }

  public boolean licenseAccepted()
  {
    return this.mSharedPreferences.getBoolean("license_accepted", false);
  }

  public void logOut()
  {
    this.mSyncManager.stopAutoSync();
    this.mTaskManager.stopAll();
    setPersistCookie("");
    setPinlockEnable(false);
  }

  public void markDeleteNote(Activity paramActivity, NoteMeta paramNoteMeta)
  {
    this.mDataSource.markDeleteNote(paramNoteMeta);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramNoteMeta.getTitle();
    UIUtilities.showFormattedToast(paramActivity, 2131361960, arrayOfObject);
  }

  public void onCreate()
  {
    super.onCreate();
    this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    mYNoteInstance = this;
    this.mDataSource = new DataSource(this);
    this.mTaskManager = new TaskManager(this.mDataSource);
    this.mSyncManager = new SyncManager(this.mTaskManager, this.mDataSource);
    if (getAutoSyncPeriod() <= 0)
      this.mSyncManager.stopAutoSync();
    while (true)
    {
      this.mLogRecorder = new LogRecorder(this);
      this.mLogRecorder.reportIfNecessary();
      this.mLogRecorder.start();
      L.setLogger(this.mLogRecorder);
      try
      {
        L.setDebug(getPackageManager().getApplicationInfo(getPackageName(), 128).metaData.getBoolean("debug"));
        if (isLogin())
          label135: new LoginTask(getUserName(), getPassword(), IpUtils.getLocalIpAddress())
          {
            public void onFailed(Exception paramException)
            {
              if (!paramException instanceof LoginException)
                return;
              YNoteApplication.this.logOut();
            }

            public void onSucceed(LoginResult paramLoginResult)
            {
              YNoteApplication.this.setCookie(paramLoginResult);
            }
          }
          .execute();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("com.youdao.note.action.ACCESS_DENIED");
        registerReceiver(this.receiver, localIntentFilter);
        return;
        this.mSyncManager.resetTime(getAutoSyncPeriod());
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        localNameNotFoundException.printStackTrace();
        break label135:
      }
    }
  }

  public void sendEditNote(Activity paramActivity, String paramString)
  {
    Intent localIntent = new Intent(paramActivity, EditNoteActivity.class);
    localIntent.setAction("android.intent.action.EDIT");
    localIntent.putExtra("noteid", paramString);
    paramActivity.startActivityForResult(localIntent, 2);
  }

  public void sendMainActivity(Activity paramActivity, String paramString)
  {
    if (this.isRecording)
      paramActivity.finish();
    do
    {
      return;
      Intent localIntent = new Intent(paramActivity, MainActivity.class);
      localIntent.setFlags(67108864);
      if (paramString != null)
        localIntent.setAction(paramString);
      paramActivity.startActivity(localIntent);
    }
    while (paramActivity instanceof MainActivity);
    paramActivity.finish();
  }

  public void sendToSearch(Activity paramActivity)
  {
    paramActivity.startActivity(new Intent(paramActivity, SearchActivity.class));
  }

  public void sendUrl(Activity paramActivity, String paramString)
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.VIEW");
    localIntent.setData(Uri.parse(paramString));
    paramActivity.startActivity(localIntent);
  }

  public boolean setCookie(LoginResult paramLoginResult)
  {
    return (setPersistCookie(paramLoginResult.getPersistCookie())) && (setSessionCookie(paramLoginResult.getSessionCookie()));
  }

  public void setDebug(boolean paramBoolean)
  {
    this.mSharedPreferences.edit().putBoolean("is_debug", paramBoolean).commit();
  }

  public void setDoodlePaintWidth(float paramFloat)
  {
    this.mSharedPreferences.edit().putFloat("doodle_paint_width", paramFloat).commit();
  }

  public boolean setEverLogin()
  {
    return this.mSharedPreferences.edit().putBoolean("ever_login", true).commit();
  }

  public void setHandWritePaintWidth(float paramFloat)
  {
    this.mSharedPreferences.edit().putFloat("handwrite_paint_width", paramFloat).commit();
  }

  public void setHandwriteMode(int paramInt)
  {
    String str = getResources().getString(2131361806);
    this.mSharedPreferences.edit().putString(str, String.valueOf(paramInt)).commit();
  }

  public boolean setIgnoreVersion(int paramInt)
  {
    return this.mSharedPreferences.edit().putInt("ignore_version", paramInt).commit();
  }

  public void setIsCmwap(boolean paramBoolean)
  {
    this.isCmwap = paramBoolean;
  }

  public void setIsRecording(boolean paramBoolean)
  {
    this.isRecording = paramBoolean;
  }

  public void setNotFirst()
  {
    String str = getPackageVersionName();
    this.mSharedPreferences.edit().putString("last_use_version", str).commit();
  }

  public boolean setPassword(String paramString)
  {
    return this.mSharedPreferences.edit().putString("password", paramString).commit();
  }

  public boolean setPersistCookie(String paramString)
  {
    return this.mSharedPreferences.edit().putString("cookie", "NTES_PASSPORT=" + paramString).commit();
  }

  public boolean setPinLock(int paramInt)
  {
    return this.mSharedPreferences.edit().putString("pinlock", IdUtils.genPinCode(paramInt)).commit();
  }

  public boolean setPinlockEnable(boolean paramBoolean)
  {
    String str = getResources().getString(2131361797);
    return this.mSharedPreferences.edit().putBoolean(str, paramBoolean).commit();
  }

  public boolean setSessionCookie(String paramString)
  {
    return this.mSharedPreferences.edit().putString("session_cookie", "NTES_SESS=" + paramString).commit();
  }

  public void setShowHandwriteGuide(boolean paramBoolean)
  {
    this.mSharedPreferences.edit().putBoolean("show_handwrite_guide", paramBoolean).commit();
  }

  public void setShowHandwriteModeTips(boolean paramBoolean)
  {
    this.mSharedPreferences.edit().putBoolean("show_handwrite_mode_tips", paramBoolean).commit();
  }

  public void setShowMailGuide(boolean paramBoolean)
  {
    this.mSharedPreferences.edit().putBoolean("show_mail_guide", paramBoolean).commit();
  }

  public void setSkitchManager(SkitchMetaManager paramSkitchMetaManager)
  {
    this.skitchManager = paramSkitchMetaManager;
  }

  public boolean setUserName(String paramString)
  {
    if (paramString == null)
      paramString = "";
    String str = paramString.toLowerCase().trim();
    if (this.mSharedPreferences.edit().putString(USERNAME, str).commit())
    {
      this.mDataSource.switchUser();
      this.mSharedPreferences.edit().putString("last_username", str).commit();
      return true;
    }
    return false;
  }

  public void stopAllTask()
  {
    this.mSyncManager.stopAutoSync();
    this.mTaskManager.stopAll();
    this.mLogRecorder.stop();
  }

  public void stopAutoSync()
  {
    String str = getResources().getString(2131361802);
    this.mSharedPreferences.edit().putString(str, "-1").commit();
    this.mSyncManager.stopAutoSync();
  }

  public boolean syncOnlyUnderWifi()
  {
    return this.mSharedPreferences.getBoolean(getResources().getString(2131361803), false);
  }

  public boolean updateCheckUpdateTime()
  {
    return this.mSharedPreferences.edit().putLong("last_chcekupdate", System.currentTimeMillis()).commit();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.YNoteApplication
 * JD-Core Version:    0.5.4
 */