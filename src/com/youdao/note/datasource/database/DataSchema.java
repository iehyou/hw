package com.youdao.note.datasource.database;

import android.provider.BaseColumns;

public abstract interface DataSchema
{
  public static final int DATABASE_VERSION = 2;
  public static final int DATABASE_VERSION_1 = 1;
  public static final int DATABASE_VERSION_2 = 2;
  public static final String DROP_TABLE = "drop table if exists ";

  public static abstract interface BASE_NOTE_META_TABLE extends BaseColumns
  {
    public static final String CREATE_TIME = "create_time";
    public static final String IS_DELETED = "is_deleted";
    public static final String IS_DIRTY = "is_dirty";
    public static final String LAST_SYNC_TIME = "last_sync_time";
    public static final String MODIFY_TIME = "modify_time";
    public static final String TITLE = "title";
    public static final String VERSION = "version";
  }

  public static abstract interface CACHE_TABLE extends BaseColumns
  {
    public static final String CREATE_CACHE_TABLE_SQL = "create table cache ( _id varchar(128) primary key not null, touch_time varchar(32) not null, length varchar(32), item_type integer not null);";
    public static final String CREATE_CACHE_TYPE_INDEX_SQL = "create index cache_type_index on cache ( item_type ) ";
    public static final String DROP_CACHE_TABLE_SQL = "drop table if exists cache";
    public static final String ITEM_ID = "_id";
    public static final String ITEM_TYPE = "item_type";
    public static final String LENGTH = "length";
    public static final String TABLE_NAME = "cache";
    public static final String TOUCH_TIME = "touch_time";
    public static final String TYPE_INDEX_NAME = "cache_type_index";
    public static final int TYPE_NOTE = 1;
    public static final int TYPE_RESOURCE = 2;
  }

  public static abstract interface NOTE_BOOKS_TABLE extends DataSchema.BASE_NOTE_META_TABLE
  {
    public static final String COLUMN_DEF_PROPS = "props text not null default ''";
    public static final String COLUMN_DEF_TYPE = "type integer not null default 0";
    public static final String CREATE_NOTEBOOK_TABLE_SQL = "create table note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default '' );";
    public static final String DROP_NOTE_BOOKS_TABLE_SQL = "drop table if exists note_books";
    public static final String NOTEBOOK = "_id";
    public static final String NOTE_NUMBER = "note_number";
    public static final String PROPS = "props";
    public static final String TABLE_NAME = "note_books";
    public static final String TYPE = "type";
  }

  public static abstract interface NOTE_CONTENT_VERSION_TABLE extends BaseColumns
  {
    public static final String CONTENT_VERSION = "content_version";
    public static final String CREATE_NOTES_CONTENT_TABLE_SQL = "create table content_version ( note_id varchar(128) primary key, content_version integer not null);";
    public static final String DROP_NOTES_CONTENT_TABLE_SQL = "drop table if exists content_version";
    public static final String NOTEID = "note_id";
    public static final String TABLE_NAME = "content_version";
  }

  public static abstract interface NOTE_META_TABLE extends DataSchema.BASE_NOTE_META_TABLE
  {
    public static final String AUTHOR = "author";
    public static final String CREATE_NOTEBOOK_INDEX_SQL = "CREATE INDEX notes_on_notebook ON note_meta (notebook);";
    public static final String CREATE_NOTE_META_TABLE_SQL = "create table note_meta ( _id varchar(128) primary key, server_notebook varchar(128), notebook varchar(128), create_time varchar(32), modify_time varchar(32), author varchar(64), props text not null, title varchar(512), version integer not null, last_sync_time varchar(32), source varchar(128), is_dirty boolean not null, is_deleted boolean not null, length integer not null);";
    public static final String DROP_NOTE_META_TABLE_SQL = "drop table if exists note_meta";
    public static final String LENGTH = "length";
    public static final String NOTEBOOK = "notebook";
    public static final String NOTEBOOK_INDEX_NAME = "notes_on_notebook";
    public static final String NOTEID = "_id";
    public static final String PROPS = "props";
    public static final String SERVER_NOTEBOOK = "server_notebook";
    public static final String SOURCE = "source";
    public static final String TABLE_NAME = "note_meta";
  }

  public static abstract interface RESOURCE_META_TABLE extends BaseColumns
  {
    public static final String COLUMN_DEF_PROPS = "props text not null default ''";
    public static final String COLUMN_DEF_TYPE = "type integer not null default 0";
    public static final String CREATE_RESOURCE_INDEX_SQL = "create index resource_index on resource_meta ( _id ) ";
    public static final String CREATE_RESOURCE_META_TABLE_SQL = "create table resource_meta ( _id varchar(128) not null primary key, filename varchar(256) not null, length integer not null, version integer not null, is_dirty boolean not null, noteid varchar(128) not null, type integer not null default 0,props text not null default '' );";
    public static final String CREATE_RESOURCE_NOTE_ID_INDEX_SQL = "create index resource_noteid_index on resource_meta ( noteid)";
    public static final String DROP_RESOURCE_META_TABLE_SQL = "drop table if exists resource_meta";
    public static final String FILE_LENGTH = "length";
    public static final String FILE_NAME = "filename";
    public static final String IS_DIRTY = "is_dirty";
    public static final String NOTEID = "noteid";
    public static final String PROPS = "props";
    public static final String RESOURCE_ID = "_id";
    public static final String RESOURCE_ID_INDEX_NAME = "resource_index";
    public static final String RESOURCE_NOTE_ID_INDEX_NAME = "resource_noteid_index";
    public static final String TABLE_NAME = "resource_meta";
    public static final String TYPE = "type";
    public static final String VERSION = "version";
  }

  public static abstract interface ROOT_NOTEBOOK_TABLE extends DataSchema.BASE_NOTE_META_TABLE
  {
    public static final String COLUMN_DEF_PROPS = "props text not null default ''";
    public static final String COLUMN_DEF_TYPE = "type integer not null default 0";
    public static final String CREATE_ROOT_NOTEBOOK_TABLE_SQL = "create table root_note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default '' );";
    public static final String DROP_ROOT_NOTE_BOOKS_TABLE_SQL = "drop table if exists root_note_books";
    public static final String NOTEBOOK = "_id";
    public static final String NOTE_NUMBER = "note_number";
    public static final String PROPS = "props";
    public static final String TABLE_NAME = "root_note_books";
    public static final String TYPE = "type";
  }

  public static abstract interface SEARCH_HISTORY_TABLE extends BaseColumns
  {
    public static final String CREATE_SEARCH_HISTORY_TABLE_SQL = "create table search_history ( _id integer primary key autoincrement, query text not null unique, timestamp integer not null);";
    public static final String DROP_SEARCH_HISTORY_TABLE_SQL = "drop table if exists search_history";
    public static final String QUERY = "query";
    public static final String TABLE_NAME = "search_history";
    public static final String TIMESTAMP = "timestamp";
  }

  public static abstract interface USER_TABLE extends BaseColumns
  {
    public static final String COLUMN_DEF_PROPS = "props text not null default ''";
    public static final String CREATE_USER_TABLE_SQL = "create table user ( _id varchar(128) not null primary key, used_space integer, quota_space integer, default_notebook varchar(128), last_push_time text not null, last_sync_time text, props text not null default '' );";
    public static final String DEFAULT_NOTEBOOK = "default_notebook";
    public static final String DROP_USER_TABLE_SQL = "drop table if exists user";
    public static final String LAST_PUSH_TIME = "last_push_time";
    public static final String LAST_SYNC_TIME = "last_sync_time";
    public static final String PROPS = "props";
    public static final String QUOTA_SPACE = "quota_space";
    public static final String TABLE_NAME = "user";
    public static final String USED_SPACE = "used_space";
    public static final String USER_NAME = "_id";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.database.DataSchema
 * JD-Core Version:    0.5.4
 */