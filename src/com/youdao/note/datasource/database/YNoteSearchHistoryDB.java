package com.youdao.note.datasource.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.youdao.note.YNoteApplication;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.L;

public class YNoteSearchHistoryDB extends SQLiteOpenHelper
  implements DataSchema
{
  private static final int MAX_SEARCH_HISTORY = 256;
  private SQLiteDatabase db = null;

  public YNoteSearchHistoryDB(Context paramContext)
  {
    this(paramContext, YNoteApplication.getInstance().getYNoteSearchHistryDBName());
  }

  public YNoteSearchHistoryDB(Context paramContext, String paramString)
  {
    super(paramContext, paramString, null, 1);
  }

  private void innerResetDataBase()
  {
    this.db.execSQL("drop table if exists search_history");
    onCreate(this.db);
  }

  private void truncate()
  {
    Cursor localCursor = getAllQuery();
    SQLiteDatabase localSQLiteDatabase;
    String[] arrayOfString;
    try
    {
      CursorHelper localCursorHelper = new CursorHelper(localCursor);
      if (localCursor.getCount() <= 256)
        break label94;
      localCursor.move(256);
      if (!localCursor.moveToNext())
        break label94;
      localSQLiteDatabase = this.db;
      arrayOfString = new String[1];
      arrayOfString[0] = Integer.toString(localCursorHelper.getInt("_id"));
    }
    finally
    {
      localCursor.close();
    }
    label94: localCursor.close();
  }

  public void clearHistory()
  {
    this.db.delete("search_history", null, null);
  }

  public Cursor getAllQuery()
  {
    return this.db.query("search_history", null, null, null, null, null, "timestamp desc");
  }

  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("create table search_history ( _id integer primary key autoincrement, query text not null unique, timestamp integer not null);");
    L.d(this, "YNoteSearchHistory table created.");
  }

  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    innerResetDataBase();
  }

  public void resetDataBase()
  {
    innerResetDataBase();
  }

  public void saveQuery(String paramString)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("query", paramString);
    localContentValues.put("timestamp", Long.valueOf(System.currentTimeMillis() / 1000L));
    DBUtils.replaceValues(this.db, "search_history", localContentValues);
    truncate();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.database.YNoteSearchHistoryDB
 * JD-Core Version:    0.5.4
 */