package com.youdao.note.datasource.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBUtils
{
  public static boolean replaceValues(SQLiteDatabase paramSQLiteDatabase, String paramString, ContentValues paramContentValues)
  {
    try
    {
      paramSQLiteDatabase.replaceOrThrow(paramString, null, paramContentValues);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DBUtils", "insert " + paramContentValues + " into table " + paramString + " faile", localException);
    }
    return false;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.database.DBUtils
 * JD-Core Version:    0.5.4
 */