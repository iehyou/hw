package com.youdao.note.datasource.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.UserMeta;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.L;
import java.util.ArrayList;

public class YNoteDB extends SQLiteOpenHelper
  implements DataSchema
{
  private static final String TAG = YNoteDB.class.getSimpleName();
  private SQLiteDatabase db = null;

  public YNoteDB(Context paramContext)
  {
    super(paramContext, YNoteApplication.getInstance().getYNoteDBName(), null, 2);
    L.d(this, "YNote database reset and db open is " + this.db.isOpen() + " " + YNoteApplication.getInstance().getYNoteDBName());
  }

  public YNoteDB(Context paramContext, String paramString)
  {
    super(paramContext, paramString, null, 1);
  }

  private String escape(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramString.length());
    char[] arrayOfChar = paramString.toCharArray();
    int i = arrayOfChar.length;
    int j = 0;
    if (j < i)
    {
      label24: char c = arrayOfChar[j];
      switch (c)
      {
      default:
      case '!':
      case '%':
      case '&':
      case '(':
      case ')':
      case '/':
      case '[':
      case ']':
      case '_':
      case '\'':
      }
      while (true)
      {
        localStringBuilder.append(c);
        while (true)
        {
          ++j;
          break label24:
          localStringBuilder.append('!').append(c);
        }
        localStringBuilder.append("''").append(c);
      }
    }
    return localStringBuilder.toString();
  }

  private NoteBook getNoteBookFromCursor(Cursor paramCursor)
  {
    paramCursor.moveToFirst();
    try
    {
      NoteBook localNoteBook = NoteBook.fromCursor(paramCursor);
      return localNoteBook;
    }
    finally
    {
      paramCursor.close();
    }
  }

  private NoteMeta getNoteMeta(Cursor paramCursor)
  {
    try
    {
      if (paramCursor.moveToFirst())
      {
        NoteMeta localNoteMeta = NoteMeta.fromCursor(paramCursor);
        return localNoteMeta;
      }
      return null;
    }
    finally
    {
      paramCursor.close();
    }
  }

  private ArrayList<BaseResourceMeta> getResourcesFromCursor(Cursor paramCursor)
  {
    ArrayList localArrayList = new ArrayList(paramCursor.getCount());
    while (true)
      try
      {
        if (!paramCursor.moveToNext())
          break label35;
        label35: localArrayList.add(ResourceUtils.fromCursor(paramCursor));
      }
      finally
      {
        paramCursor.close();
      }
    return localArrayList;
  }

  private void innerResetDB()
  {
    this.db.execSQL("drop table if exists note_meta");
    this.db.execSQL("drop table if exists note_books");
    this.db.execSQL("drop table if exists resource_meta");
    this.db.execSQL("drop table if exists user");
    this.db.execSQL("drop table if exists root_note_books");
    this.db.execSQL("drop table if exists cache");
    this.db.execSQL("drop table if exists content_version");
    onCreate(this.db);
  }

  public void deleteCacheItem(String paramString)
  {
    this.db.delete("cache", "_id = ?", new String[] { paramString });
  }

  public boolean deleteNoteBook(NoteMeta paramNoteMeta)
  {
    return deleteNoteBook(paramNoteMeta.getNoteBook());
  }

  public boolean deleteNoteBook(String paramString)
  {
    this.db.delete("note_books", "_id = ?", new String[] { paramString });
    return true;
  }

  public boolean deleteNoteMeta(NoteMeta paramNoteMeta)
  {
    L.d(this, "delete " + paramNoteMeta + " from database.");
    SQLiteDatabase localSQLiteDatabase = this.db;
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramNoteMeta.getNoteId();
    localSQLiteDatabase.delete("note_meta", "_id = ?", arrayOfString);
    return true;
  }

  public boolean deleteResource(IResourceMeta paramIResourceMeta)
  {
    return deleteResourceById(paramIResourceMeta.getResourceId());
  }

  public boolean deleteResourceById(String paramString)
  {
    this.db.delete("resource_meta", "_id = ?", new String[] { paramString });
    return true;
  }

  public boolean deleteResourceByNote(String paramString)
  {
    this.db.delete("resource_meta", "noteid = ?", new String[] { paramString });
    return true;
  }

  public Cursor getAllNoteBookMetasCursor()
  {
    Cursor localCursor = this.db.query("note_books", null, "is_deleted = ?", new String[] { "0" }, null, null, "modify_time desc", null);
    L.d(this, "Total " + localCursor.getCount() + " note books fetched from database.");
    return localCursor;
  }

  public Cursor getAllNoteMetasCursor()
  {
    Cursor localCursor = this.db.query("note_meta", null, "is_deleted = ?", new String[] { "0" }, null, null, "modify_time desc", null);
    Log.d(TAG, "Total " + localCursor.getCount() + " fetched from database.");
    return localCursor;
  }

  public Cursor getCacheItem(int paramInt)
  {
    SQLiteDatabase localSQLiteDatabase = this.db;
    String[] arrayOfString = new String[1];
    arrayOfString[0] = Integer.toString(paramInt);
    return localSQLiteDatabase.query("cache", null, "item_type = ?", arrayOfString, null, null, "touch_time asc", null);
  }

  public int getCachedNoteCount()
  {
    SQLiteStatement localSQLiteStatement = this.db.compileStatement("select count(*) from cache where item_type = 1");
    try
    {
      long l = localSQLiteStatement.simpleQueryForLong();
      int i = (int)l;
      return i;
    }
    finally
    {
      localSQLiteStatement.close();
    }
  }

  public long getCachedResourceSize()
  {
    SQLiteStatement localSQLiteStatement = this.db.compileStatement("select sum(length ) from cache where item_type = 2");
    try
    {
      long l = localSQLiteStatement.simpleQueryForLong();
      return l;
    }
    finally
    {
      localSQLiteStatement.close();
    }
  }

  public ArrayList<NoteMeta> getDirtyAndMovedNoteMetas()
  {
    Cursor localCursor = this.db.query("note_meta", null, "is_dirty = 1 or server_notebook != notebook", null, null, null, null);
    ArrayList localArrayList = new ArrayList(localCursor.getCount());
    while (true)
      try
      {
        if (!localCursor.moveToNext())
          break label53;
        label53: localArrayList.add(NoteMeta.fromCursor(localCursor));
      }
      finally
      {
        localCursor.close();
      }
    return localArrayList;
  }

  public Cursor getDirtyNotebookMetas()
  {
    return this.db.query("note_books", null, "is_dirty = 1", null, null, null, null);
  }

  public ArrayList<BaseResourceMeta> getDirtyResourcesOf(String paramString)
  {
    return getResourcesFromCursor(this.db.query("resource_meta", null, "is_dirty = 1 and noteid = ?", new String[] { paramString }, null, null, null, null));
  }

  public int getNoteBookCount(String paramString)
  {
    SQLiteStatement localSQLiteStatement = this.db.compileStatement("select count(*) from note_meta where notebook = \"" + paramString + "\" and " + "is_deleted" + " = 0");
    try
    {
      long l = localSQLiteStatement.simpleQueryForLong();
      int i = (int)l;
      return i;
    }
    finally
    {
      localSQLiteStatement.close();
    }
  }

  public NoteBook getNoteBookMetaById(String paramString)
  {
    return getNoteBookFromCursor(this.db.query("note_books", null, "_id = ?", new String[] { paramString }, null, null, null, null));
  }

  public NoteBook getNoteBookMetaByTitle(String paramString)
  {
    return getNoteBookFromCursor(this.db.query("note_books", null, "title = ?", new String[] { paramString }, null, null, null));
  }

  public Cursor getNoteByNoteBook(String paramString)
  {
    return this.db.query("note_meta", null, "notebook = ? and is_deleted = ?", new String[] { paramString, "0" }, null, null, "modify_time desc", null);
  }

  public int getNoteContentVersion(String paramString)
  {
    Cursor localCursor = this.db.query("content_version", null, "note_id = ?", new String[] { paramString }, null, null, null, null);
    CursorHelper localCursorHelper = new CursorHelper(localCursor);
    try
    {
      if (localCursor.moveToFirst())
      {
        int i = localCursorHelper.getInt("content_version");
        return i;
      }
      return -1;
    }
    finally
    {
      localCursor.close();
    }
  }

  public NoteMeta getNoteMetaById(String paramString)
  {
    return getNoteMeta(this.db.query("note_meta", null, "_id = ? and is_deleted = 0", new String[] { paramString }, null, null, null, null));
  }

  public NoteMeta getNoteMetaByTitle(String paramString)
  {
    return getNoteMeta(this.db.query("note_meta", null, "title = ? and is_deleted = 0", new String[] { paramString }, null, null, null, null));
  }

  public BaseResourceMeta getResourceById(String paramString)
  {
    Cursor localCursor = this.db.query("resource_meta", null, "_id = ?", new String[] { paramString }, null, null, null, null);
    try
    {
      if (localCursor.moveToFirst())
      {
        BaseResourceMeta localBaseResourceMeta = ResourceUtils.fromCursor(localCursor);
        return localBaseResourceMeta;
      }
      return null;
    }
    finally
    {
      localCursor.close();
    }
  }

  public ArrayList<BaseResourceMeta> getResourcesByNoteId(String paramString)
  {
    return getResourcesFromCursor(this.db.query("resource_meta", null, "noteid = ?", new String[] { paramString }, null, null, null, null));
  }

  public NoteBook getRootMeta()
  {
    Cursor localCursor = this.db.query("root_note_books", null, null, null, null, null, null);
    try
    {
      if ((localCursor.getCount() > 0) && (localCursor.moveToFirst()))
      {
        NoteBook localNoteBook = NoteBook.fromCursor(localCursor);
        return localNoteBook;
      }
      return null;
    }
    finally
    {
      localCursor.close();
    }
  }

  public UserMeta getUserMeta(String paramString)
  {
    L.d(this, "user name is " + paramString);
    Cursor localCursor = this.db.query("user", null, "_id = ?", new String[] { paramString }, null, null, null, null);
    L.d(this, "cursor count is " + localCursor.getCount());
    try
    {
      if (localCursor.moveToFirst())
      {
        UserMeta localUserMeta = UserMeta.fromCursor(localCursor);
        return localUserMeta;
      }
      return null;
    }
    finally
    {
      localCursor.close();
    }
  }

  public boolean insertOrUpdateNoteBookMeta(NoteBook paramNoteBook)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramNoteBook.getNoteBookId());
    localContentValues.put("create_time", Long.valueOf(paramNoteBook.getCreateTime()));
    localContentValues.put("modify_time", Long.valueOf(paramNoteBook.getModifyTime()));
    localContentValues.put("version", Integer.valueOf(paramNoteBook.getVersion()));
    localContentValues.put("last_sync_time", Long.valueOf(paramNoteBook.getLastSyncTime()));
    localContentValues.put("title", paramNoteBook.getTitle());
    localContentValues.put("is_dirty", Boolean.valueOf(paramNoteBook.isDirty()));
    localContentValues.put("is_deleted", Boolean.valueOf(paramNoteBook.isDeleted()));
    localContentValues.put("note_number", Integer.valueOf(paramNoteBook.getNoteNumber()));
    return DBUtils.replaceValues(this.db, "note_books", localContentValues);
  }

  public boolean insertOrUpdateNoteMeta(NoteMeta paramNoteMeta)
  {
    L.d(this, "insert one noteMeta " + paramNoteMeta);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("notebook", paramNoteMeta.getNoteBook());
    localContentValues.put("_id", paramNoteMeta.getNoteId());
    localContentValues.put("author", paramNoteMeta.getAuthor());
    localContentValues.put("title", paramNoteMeta.getTitle());
    localContentValues.put("create_time", Long.valueOf(paramNoteMeta.getCreateTime()));
    localContentValues.put("modify_time", Long.valueOf(paramNoteMeta.getModifyTime()));
    localContentValues.put("server_notebook", paramNoteMeta.getServerNoteBook());
    localContentValues.put("version", Integer.valueOf(paramNoteMeta.getVersion()));
    localContentValues.put("length", Long.valueOf(paramNoteMeta.getLength()));
    localContentValues.put("last_sync_time", Long.valueOf(paramNoteMeta.getLastSyncTime()));
    localContentValues.put("props", paramNoteMeta.getPropsAsStr());
    localContentValues.put("is_dirty", Boolean.valueOf(paramNoteMeta.isDirty()));
    localContentValues.put("is_deleted", Boolean.valueOf(paramNoteMeta.isDeleted()));
    localContentValues.put("source", paramNoteMeta.getSourceUrl());
    return DBUtils.replaceValues(this.db, "note_meta", localContentValues);
  }

  public boolean insertOrUpdateResource(BaseResourceMeta paramBaseResourceMeta)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramBaseResourceMeta.getResourceId());
    localContentValues.put("filename", paramBaseResourceMeta.getFileName());
    localContentValues.put("noteid", paramBaseResourceMeta.getNoteId());
    localContentValues.put("version", Integer.valueOf(paramBaseResourceMeta.getVersion()));
    localContentValues.put("length", Long.valueOf(paramBaseResourceMeta.getLength()));
    localContentValues.put("is_dirty", Boolean.valueOf(paramBaseResourceMeta.isDirty()));
    localContentValues.put("type", Integer.valueOf(paramBaseResourceMeta.getType()));
    localContentValues.put("props", paramBaseResourceMeta.getPropsAsStr());
    return DBUtils.replaceValues(this.db, "resource_meta", localContentValues);
  }

  public boolean insertOrUpdateUserMeta(String paramString, UserMeta paramUserMeta)
  {
    L.d(this, "User Meta info updated " + paramUserMeta + ", userName is " + paramString);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramString);
    localContentValues.put("used_space", Long.valueOf(paramUserMeta.getUsedSpace()));
    localContentValues.put("quota_space", Long.valueOf(paramUserMeta.getQuotaSpace()));
    localContentValues.put("default_notebook", paramUserMeta.getDefaultNoteBook());
    localContentValues.put("last_push_time", Long.valueOf(paramUserMeta.getLastPushTime()));
    if (paramUserMeta.getLastSynceTime() > 0L)
      localContentValues.put("last_sync_time", Long.valueOf(paramUserMeta.getLastSynceTime()));
    DBUtils.replaceValues(this.db, "user", localContentValues);
    UserMeta localUserMeta = getUserMeta(paramString);
    L.d(this, "haa user meta is " + localUserMeta);
    return true;
  }

  public boolean markDeleteNote(NoteMeta paramNoteMeta)
  {
    paramNoteMeta.setDeleted(true);
    paramNoteMeta.setDirty(true);
    return insertOrUpdateNoteMeta(paramNoteMeta);
  }

  public boolean markDeleteNoteBook(NoteBook paramNoteBook)
  {
    paramNoteBook.setDeleted(true);
    paramNoteBook.setDirty(true);
    return insertOrUpdateNoteBookMeta(paramNoteBook);
  }

  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("create table note_meta ( _id varchar(128) primary key, server_notebook varchar(128), notebook varchar(128), create_time varchar(32), modify_time varchar(32), author varchar(64), props text not null, title varchar(512), version integer not null, last_sync_time varchar(32), source varchar(128), is_dirty boolean not null, is_deleted boolean not null, length integer not null);");
    paramSQLiteDatabase.execSQL("CREATE INDEX notes_on_notebook ON note_meta (notebook);");
    paramSQLiteDatabase.execSQL("create table note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default '' );");
    paramSQLiteDatabase.execSQL("create table resource_meta ( _id varchar(128) not null primary key, filename varchar(256) not null, length integer not null, version integer not null, is_dirty boolean not null, noteid varchar(128) not null, type integer not null default 0,props text not null default '' );");
    paramSQLiteDatabase.execSQL("create index resource_noteid_index on resource_meta ( noteid)");
    paramSQLiteDatabase.execSQL("create index resource_index on resource_meta ( _id ) ");
    paramSQLiteDatabase.execSQL("create table user ( _id varchar(128) not null primary key, used_space integer, quota_space integer, default_notebook varchar(128), last_push_time text not null, last_sync_time text, props text not null default '' );");
    paramSQLiteDatabase.execSQL("create table root_note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default '' );");
    paramSQLiteDatabase.execSQL("create table cache ( _id varchar(128) primary key not null, touch_time varchar(32) not null, length varchar(32), item_type integer not null);");
    paramSQLiteDatabase.execSQL("create index cache_type_index on cache ( item_type ) ");
    paramSQLiteDatabase.execSQL("create table content_version ( note_id varchar(128) primary key, content_version integer not null);");
  }

  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    if ((paramInt1 != 1) || (paramInt2 != 2))
      return;
    paramSQLiteDatabase.execSQL("alter table resources rename to resource_meta");
    paramSQLiteDatabase.execSQL("alter table notes rename to note_meta");
    paramSQLiteDatabase.execSQL("alter table resource_meta add type integer not null default 0");
    paramSQLiteDatabase.execSQL("alter table resource_meta add props text not null default ''");
    paramSQLiteDatabase.execSQL("alter table user add props text not null default ''");
    paramSQLiteDatabase.execSQL("alter table note_books add type integer not null default 0");
    paramSQLiteDatabase.execSQL("alter table note_books add props text not null default ''");
    paramSQLiteDatabase.execSQL("alter table root_note_books add type integer not null default 0");
    paramSQLiteDatabase.execSQL("alter table root_note_books add props text not null default ''");
  }

  public boolean removeNoteContentVersion(String paramString)
  {
    this.db.delete("content_version", "note_id = ?", new String[] { paramString });
    return true;
  }

  public void resetDataBase()
  {
    innerResetDB();
  }

  public Cursor searchNotes(String paramString)
  {
    String str = escape(paramString);
    L.d(this, "escaped keyword is " + str);
    return this.db.query("note_meta", null, "title like '%" + str + "%' escape '!' and " + "is_deleted" + " = 0", null, null, null, "modify_time desc");
  }

  public boolean touchCacheItem(String paramString, int paramInt)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramString);
    localContentValues.put("item_type", Integer.valueOf(paramInt));
    localContentValues.put("touch_time", Long.valueOf(System.currentTimeMillis()));
    return DBUtils.replaceValues(this.db, "cache", localContentValues);
  }

  public boolean touchCacheItem(String paramString, int paramInt, long paramLong)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramString);
    localContentValues.put("item_type", Integer.valueOf(paramInt));
    localContentValues.put("length", Long.valueOf(paramLong));
    localContentValues.put("touch_time", Long.valueOf(System.currentTimeMillis()));
    return DBUtils.replaceValues(this.db, "cache", localContentValues);
  }

  public void updateNoteContentVersion(String paramString, int paramInt)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("note_id", paramString);
    localContentValues.put("content_version", Integer.valueOf(paramInt));
    DBUtils.replaceValues(this.db, "content_version", localContentValues);
  }

  public boolean updateRootNoteBookMeta(NoteBook paramNoteBook)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", paramNoteBook.getNoteBookId());
    localContentValues.put("create_time", Long.valueOf(paramNoteBook.getCreateTime()));
    localContentValues.put("modify_time", Long.valueOf(paramNoteBook.getModifyTime()));
    localContentValues.put("version", Integer.valueOf(paramNoteBook.getVersion()));
    localContentValues.put("last_sync_time", Long.valueOf(paramNoteBook.getLastSyncTime()));
    localContentValues.put("title", paramNoteBook.getTitle());
    localContentValues.put("is_dirty", Boolean.valueOf(paramNoteBook.isDirty()));
    localContentValues.put("note_number", Integer.valueOf(paramNoteBook.getNoteNumber()));
    localContentValues.put("is_deleted", Boolean.valueOf(paramNoteBook.isDeleted()));
    return DBUtils.replaceValues(this.db, "root_note_books", localContentValues);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.database.YNoteDB
 * JD-Core Version:    0.5.4
 */