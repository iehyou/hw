package com.youdao.note.datasource.localcache;

import android.content.Context;

public abstract class BaseResourceCache extends AbstractLocalCache
{
  public BaseResourceCache(Context paramContext)
  {
    super(paramContext);
  }

  protected String getDataName()
  {
    return "Resource";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.BaseResourceCache
 * JD-Core Version:    0.5.4
 */