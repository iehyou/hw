package com.youdao.note.datasource.localcache;

import android.content.Context;
import android.util.Log;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.ICacheable;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.IOException;

public abstract class AbstractLocalCache
{
  private static final String ROOT_STORE = "YoudaoNote";
  private static final String TAG = AbstractLocalCache.class.getName();
  private YNoteApplication mYNote = null;

  static
  {
    File localFile = new File(YNoteApplication.getInstance().getStoreDir(), "YoudaoNote/.nomedia");
    if (!localFile.exists());
    try
    {
      localFile.createNewFile();
      return;
    }
    catch (IOException localIOException)
    {
      L.e(AbstractLocalCache.class, "Failed to create ignore file.", localIOException);
    }
  }

  public AbstractLocalCache(Context paramContext)
  {
    this.mYNote = ((YNoteApplication)paramContext);
  }

  public boolean clearCache()
  {
    return FileUtils.deleteDirectory(getDataPath());
  }

  public boolean deleteCacheItem(String paramString)
    throws IOException
  {
    return FileUtils.deleteFile(getAbsolutePath(paramString));
  }

  public boolean exist(String paramString)
  {
    String str = getAbsolutePath(paramString);
    boolean bool = FileUtils.exist(str);
    Log.d(TAG, str + " exist is " + bool);
    return bool;
  }

  public String getAbsolutePath(String paramString)
  {
    return getDataPath() + paramString;
  }

  public byte[] getCacheItemAsBytes(String paramString)
    throws IOException
  {
    return FileUtils.readFromFileAsBytes(getAbsolutePath(paramString));
  }

  protected String getCacheItemAsString(String paramString)
    throws IOException
  {
    return FileUtils.readFromFileAsStr(getAbsolutePath(paramString));
  }

  protected abstract String getDataName();

  public String getDataPath()
  {
    return getYNoteCachePath() + File.separatorChar + getDataName() + File.separatorChar;
  }

  protected String getPathCreateIfNotExist(String paramString)
  {
    File localFile = new File(paramString);
    if (!localFile.isDirectory())
      localFile.mkdirs();
    return localFile.getPath();
  }

  protected String getYNoteCachePath()
  {
    File localFile = this.mYNote.getStoreDir();
    return getPathCreateIfNotExist(localFile.getPath() + "/YoudaoNote");
  }

  public boolean updateCacheItem(ICacheable paramICacheable)
    throws IOException
  {
    String str = getAbsolutePath(paramICacheable.getRelativePath());
    FileUtils.saveToFile(str, paramICacheable.getContentBytes());
    Log.d(TAG, "Succeed to update cache item " + str);
    return true;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.AbstractLocalCache
 * JD-Core Version:    0.5.4
 */