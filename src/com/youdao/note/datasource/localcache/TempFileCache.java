package com.youdao.note.datasource.localcache;

import android.content.Context;
import com.youdao.note.data.TempFile;
import java.io.File;
import java.io.IOException;

public class TempFileCache extends AbstractLocalCache
{
  public static final String SUFFIX_NOTE_SNAPSHOT = "-note-snapshot.jpg";

  public TempFileCache(Context paramContext)
  {
    super(paramContext);
  }

  public void clean()
  {
    File[] arrayOfFile = new File(getDataPath()).listFiles();
    if (arrayOfFile == null)
      return;
    int i = arrayOfFile.length;
    for (int j = 0; ; ++j)
    {
      if (j < i);
      arrayOfFile[j].delete();
    }
  }

  public void clean(String paramString)
  {
    File[] arrayOfFile = new File(getDataPath()).listFiles();
    if (arrayOfFile == null)
      return;
    int i = arrayOfFile.length;
    for (int j = 0; ; ++j)
    {
      if (j < i);
      File localFile = arrayOfFile[j];
      if (!localFile.getName().endsWith(paramString))
        continue;
      localFile.delete();
    }
  }

  protected String getDataName()
  {
    return "temp";
  }

  public TempFile getTempFile(String paramString)
    throws IOException
  {
    return new TempFile(paramString);
  }

  public void updateTempFile(TempFile paramTempFile)
    throws IOException
  {
    super.updateCacheItem(paramTempFile);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.TempFileCache
 * JD-Core Version:    0.5.4
 */