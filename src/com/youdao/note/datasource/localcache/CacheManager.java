package com.youdao.note.datasource.localcache;

import android.database.Cursor;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.Snippet;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.database.YNoteDB;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.L;
import com.youdao.note.utils.UnitUtils;
import java.util.Iterator;
import java.util.List;

public class CacheManager
{
  public static final int MAX_CACHE_NOTES_NUMBER = 500;
  public static long MAX_CACHE_RESOURCE_SIZE = 209715200L;
  private DataSource dataSource = YNoteApplication.getInstance().getDataSource();
  private Object lock = new Object();
  private NotesCleaner mNotesCleaner = null;
  private ResourcesCleaner mResourcesCleaner = null;

  private void checkNotesCount()
  {
    int i = this.dataSource.getDb().getCachedNoteCount();
    L.d(this, "Total cached notes size is " + i);
    if (i <= 500)
      return;
    synchronized (this.lock)
    {
      if ((this.mNotesCleaner == null) || (!this.mNotesCleaner.isAlive()))
      {
        this.mNotesCleaner = new NotesCleaner(null);
        this.mNotesCleaner.setDaemon(true);
        this.mNotesCleaner.start();
      }
      return;
    }
  }

  private void checkResourcesSize()
  {
    long l = this.dataSource.getDb().getCachedResourceSize();
    L.d(this, "Total cached resources size is " + UnitUtils.getSizeInMegaByte(l) + " M");
    if (l <= MAX_CACHE_RESOURCE_SIZE)
      return;
    synchronized (this.lock)
    {
      if ((this.mResourcesCleaner == null) || (!this.mResourcesCleaner.isAlive()))
      {
        this.mResourcesCleaner = new ResourcesCleaner();
        this.mResourcesCleaner.setDaemon(true);
        this.mResourcesCleaner.start();
      }
      return;
    }
  }

  public boolean isCleaning()
  {
    return ((this.mNotesCleaner != null) && (this.mNotesCleaner.isAlive())) || ((this.mResourcesCleaner != null) && (this.mResourcesCleaner.isAlive()));
  }

  public void touchCacheItem(String paramString, int paramInt)
  {
    this.dataSource.getDb().touchCacheItem(paramString, paramInt);
  }

  public void touchCacheItem(String paramString, int paramInt, long paramLong)
  {
    this.dataSource.getDb().touchCacheItem(paramString, paramInt, paramLong);
    if (paramInt == 1)
      checkNotesCount();
    do
      return;
    while (paramInt != 2);
    checkResourcesSize();
  }

  private class NotesCleaner extends Thread
  {
    private NotesCleaner()
    {
    }

    public void run()
    {
      L.d(CacheManager.this, "Start to clean notes.");
      Cursor localCursor = CacheManager.this.dataSource.getDb().getCacheItem(1);
      String str;
      NoteMeta localNoteMeta;
      try
      {
        CursorHelper localCursorHelper = new CursorHelper(localCursor);
        do
        {
          if (CacheManager.this.dataSource.getDb().getCachedNoteCount() <= 500)
            break label315;
          if (!localCursorHelper.moveToNext())
            break label188;
          str = localCursorHelper.getString("_id");
          localNoteMeta = CacheManager.this.dataSource.getDb().getNoteMetaById(str);
        }
        while (localNoteMeta.isDirty());
        Iterator localIterator = CacheManager.this.dataSource.getDb().getResourcesByNoteId(str).iterator();
        BaseResourceMeta localBaseResourceMeta;
        do
        {
          if (!localIterator.hasNext())
            break label210;
          localBaseResourceMeta = (BaseResourceMeta)localIterator.next();
          CacheManager.this.dataSource.getResourceCache(localBaseResourceMeta.getType()).deleteCacheItem(localBaseResourceMeta.genRelativePath());
        }
        while (!ResourceUtils.hasThumbnail(localBaseResourceMeta));
        label315: label188: label210: CacheManager.this.dataSource.getThumbnailCache().deleteCacheItem(localBaseResourceMeta.genRelativePath());
      }
      catch (Exception localException)
      {
        L.e(CacheManager.this, "Failed to clean cache notes.", localException);
        return;
        CacheManager.this.dataSource.getNoteCache().deleteNote(localNoteMeta);
        Snippet localSnippet = CacheManager.this.dataSource.getSnippet(localNoteMeta);
        if (localSnippet != null)
          CacheManager.this.dataSource.deleteSnippet(localSnippet.getRelativePath());
        CacheManager.this.dataSource.getDb().deleteCacheItem(str);
        L.d(CacheManager.this, "Clean one note " + str);
      }
      finally
      {
        localCursor.close();
      }
    }
  }

  public class ResourcesCleaner extends Thread
  {
    public ResourcesCleaner()
    {
    }

    public void run()
    {
      L.d(CacheManager.this, "Start to clearn resources");
      Cursor localCursor = CacheManager.this.dataSource.getDb().getCacheItem(2);
      String str;
      try
      {
        CursorHelper localCursorHelper = new CursorHelper(localCursor);
        BaseResourceMeta localBaseResourceMeta;
        do
        {
          if ((CacheManager.this.dataSource.getDb().getCachedResourceSize() <= CacheManager.MAX_CACHE_RESOURCE_SIZE) || (!localCursorHelper.moveToNext()))
            break label209;
          str = localCursorHelper.getString("_id");
          localBaseResourceMeta = CacheManager.this.dataSource.getDb().getResourceById(str);
        }
        while (localBaseResourceMeta.isDirty());
        CacheManager.this.dataSource.getResourceCache(localBaseResourceMeta.getType()).deleteCacheItem(localBaseResourceMeta.genRelativePath());
        if (ResourceUtils.hasThumbnail(localBaseResourceMeta))
          CacheManager.this.dataSource.getThumbnailCache().deleteCacheItem(localBaseResourceMeta.genRelativePath());
        label209: CacheManager.this.dataSource.getDb().deleteCacheItem(str);
      }
      catch (Exception localException)
      {
        L.e(CacheManager.this, "Failed to clean resources.", localException);
        return;
        return;
      }
      finally
      {
        localCursor.close();
      }
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.CacheManager
 * JD-Core Version:    0.5.4
 */