package com.youdao.note.datasource.localcache;

import android.content.Context;

public class SnippetCache extends AbstractLocalCache
{
  public static final String SNIPPET_FILENAME = "snippet.png";

  public SnippetCache(Context paramContext)
  {
    super(paramContext);
  }

  protected String getDataName()
  {
    return "Snippet";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.SnippetCache
 * JD-Core Version:    0.5.4
 */