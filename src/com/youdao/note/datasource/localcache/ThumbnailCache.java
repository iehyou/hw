package com.youdao.note.datasource.localcache;

import android.content.Context;

public class ThumbnailCache extends AbstractLocalCache
{
  public ThumbnailCache(Context paramContext)
  {
    super(paramContext);
  }

  protected String getDataName()
  {
    return "Thumbnail";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.ThumbnailCache
 * JD-Core Version:    0.5.4
 */