package com.youdao.note.datasource.localcache;

import android.content.Context;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteMeta;
import java.io.IOException;

public class NoteCache extends AbstractLocalCache
{
  public NoteCache(Context paramContext)
  {
    super(paramContext);
  }

  public boolean deleteNote(Note paramNote)
    throws IOException
  {
    return super.deleteCacheItem(paramNote.getRelativePath());
  }

  public boolean deleteNote(NoteMeta paramNoteMeta)
    throws IOException
  {
    return super.deleteCacheItem(paramNoteMeta.genRelativePath());
  }

  protected String getDataName()
  {
    return "Note";
  }

  public String getNoteContent(Note paramNote)
    throws IOException
  {
    return super.getCacheItemAsString(paramNote.getRelativePath());
  }

  public boolean updateNote(Note paramNote)
    throws IOException
  {
    return super.updateCacheItem(paramNote);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.localcache.NoteCache
 * JD-Core Version:    0.5.4
 */