package com.youdao.note.datasource;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.util.Log;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.ListNoteMetas;
import com.youdao.note.data.Note;
import com.youdao.note.data.NoteBook;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.data.Snippet;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.UserMeta;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.AudioResource;
import com.youdao.note.data.resource.AudioResourceMeta;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.DoodleResource;
import com.youdao.note.data.resource.DoodleResourceMeta;
import com.youdao.note.data.resource.GeneralResource;
import com.youdao.note.data.resource.GeneralResourceMeta;
import com.youdao.note.data.resource.HandwriteResource;
import com.youdao.note.data.resource.HandwriteResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ImageResource;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.data.resource.ResourceMetaSwitcher;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.database.YNoteDB;
import com.youdao.note.datasource.database.YNoteSearchHistoryDB;
import com.youdao.note.datasource.localcache.AudioCache;
import com.youdao.note.datasource.localcache.BaseResourceCache;
import com.youdao.note.datasource.localcache.CacheManager;
import com.youdao.note.datasource.localcache.DoodleCache;
import com.youdao.note.datasource.localcache.GeneralResourceCache;
import com.youdao.note.datasource.localcache.HandwriteCache;
import com.youdao.note.datasource.localcache.ImageCache;
import com.youdao.note.datasource.localcache.NoteCache;
import com.youdao.note.datasource.localcache.SnippetCache;
import com.youdao.note.datasource.localcache.TempFileCache;
import com.youdao.note.datasource.localcache.ThumbnailCache;
import com.youdao.note.exceptions.NoteBookNotExistException;
import com.youdao.note.utils.Consts;
import com.youdao.note.utils.Consts.HTML;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.FileUtils;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataSource
  implements Consts.HTML, Consts
{
  private static final String TAG = DataSource.class.getSimpleName();
  private AudioCache audioCache = null;
  private DoodleCache doodleCache = null;
  private GeneralResourceCache generalResourceCache = null;
  private HandwriteCache handwriteCache = null;
  private ImageCache imageCache = null;
  private CacheManager mCacheManager = null;
  private NoteCache mNoteCache = null;
  private YNoteApplication mYNote;
  private YNoteDB mYNoteDb = null;
  private YNoteSearchHistoryDB mYNoteSearchDb = null;
  private SnippetCache snippetCache = null;
  private TempFileCache tempFileCache = null;
  private ThumbnailCache thumbnailCache = null;

  public DataSource(Context paramContext)
  {
    this.mYNote = ((YNoteApplication)paramContext);
  }

  private boolean adjustNoteBookCount(NoteBook paramNoteBook)
  {
    if (paramNoteBook == null)
      return true;
    paramNoteBook.setNoteNumber(getDb().getNoteBookCount(paramNoteBook.getNoteBookId()));
    return insertOrUpdateNoteBookMeta(paramNoteBook);
  }

  private void adjustNotebookCount(NoteMeta paramNoteMeta, String paramString)
  {
    if (paramString != null)
      adjustNoteBookCount(paramString);
    adjustNoteBookCount(paramNoteMeta.getNoteBook());
  }

  private NoteMeta[] cursor2Array(Cursor paramCursor, boolean paramBoolean)
  {
    L.d(this, "Notes size is " + paramCursor.getCount());
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    NoteMeta[] arrayOfNoteMeta = new NoteMeta[paramCursor.getCount()];
    int j;
    for (int i = 0; localCursorHelper.moveToNext(); i = j)
    {
      j = i + 1;
      arrayOfNoteMeta[i] = NoteMeta.fromCursorHelper(localCursorHelper);
    }
    return arrayOfNoteMeta;
  }

  private NoteBook[] cursor2NoteBookArray(Cursor paramCursor)
  {
    L.d(this, "Notes size is " + paramCursor.getCount());
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    NoteBook[] arrayOfNoteBook = new NoteBook[paramCursor.getCount()];
    int j;
    for (int i = 0; localCursorHelper.moveToNext(); i = j)
    {
      j = i + 1;
      arrayOfNoteBook[i] = NoteBook.fromCursorHelper(localCursorHelper);
    }
    return arrayOfNoteBook;
  }

  private List<NoteBook> cursor2NoteBookList(Cursor paramCursor)
  {
    L.d(this, "Notes size is " + paramCursor.getCount());
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    ArrayList localArrayList = new ArrayList(paramCursor.getCount());
    while (localCursorHelper.moveToNext())
      localArrayList.add(NoteBook.fromCursorHelper(localCursorHelper));
    return localArrayList;
  }

  private List<NoteMeta> cursor2NoteMetaList(Cursor paramCursor)
  {
    L.d(this, "Notes size is " + paramCursor.getCount());
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    ArrayList localArrayList = new ArrayList(paramCursor.getCount());
    while (localCursorHelper.moveToNext())
      localArrayList.add(NoteMeta.fromCursorHelper(localCursorHelper));
    return localArrayList;
  }

  private boolean deleteNotesInNotebook(String paramString)
  {
    boolean bool1 = true;
    Cursor localCursor = getDb().getNoteByNoteBook(paramString);
    try
    {
      while (localCursor.moveToNext())
      {
        boolean bool2 = deleteNoteById(NoteMeta.fromCursor(localCursor).getNoteId());
        bool1 &= bool2;
      }
      return bool1;
    }
    finally
    {
      localCursor.close();
    }
  }

  private YNoteSearchHistoryDB getSearchDb()
  {
    if (this.mYNoteSearchDb == null)
      monitorenter;
    try
    {
      if (this.mYNoteSearchDb == null)
        this.mYNoteSearchDb = new YNoteSearchHistoryDB(this.mYNote);
      return this.mYNoteSearchDb;
    }
    finally
    {
      monitorexit;
    }
  }

  public static String getTmpResourcePath(String paramString)
  {
    return paramString + ".tmp";
  }

  private boolean markDeleteNotesInNotebook(String paramString)
  {
    boolean bool1 = true;
    Cursor localCursor = getDb().getNoteByNoteBook(paramString);
    try
    {
      while (localCursor.moveToNext())
      {
        boolean bool2 = markDeleteNote(NoteMeta.fromCursor(localCursor));
        bool1 &= bool2;
      }
      return bool1;
    }
    finally
    {
      localCursor.close();
    }
  }

  public boolean adjustNoteBookCount(String paramString)
  {
    if (paramString != null)
      return adjustNoteBookCount(getNoteBookMetaById(paramString));
    return true;
  }

  public boolean clearCache()
  {
    return true & getNoteCache().clearCache() & getImageCache().clearCache() & getThumbnailCache().clearCache() & getSnippetCache().clearCache() & getHandwriteCache().clearCache() & getDoodleCache().clearCache();
  }

  public void clearResourcesInNote(String paramString)
  {
    Iterator localIterator = getResourceMetasByNoteId(paramString).iterator();
    while (localIterator.hasNext())
      deleteResource((BaseResourceMeta)localIterator.next());
  }

  public void clearSearchHistory()
  {
    getSearchDb().clearHistory();
  }

  public void close()
  {
    if (this.mYNoteDb != null)
      this.mYNoteDb.close();
    if (this.mYNoteSearchDb == null)
      return;
    this.mYNoteSearchDb.close();
  }

  public boolean deleteNote(NoteMeta paramNoteMeta)
  {
    return deleteNoteById(paramNoteMeta.getNoteId());
  }

  public boolean deleteNote(String paramString)
  {
    return deleteNoteById(com.youdao.note.utils.YNoteUtils.splitEntryPath(paramString)[1]);
  }

  public boolean deleteNoteBook(NoteBook paramNoteBook)
  {
    return deleteNoteBook(paramNoteBook.getNoteBookId());
  }

  public boolean deleteNoteBook(String paramString)
  {
    deleteNotesInNotebook(paramString);
    return getDb().deleteNoteBook(paramString);
  }

  public boolean deleteNoteById(String paramString)
  {
    Iterator localIterator = getResourceMetasByNoteId(paramString).iterator();
    while (localIterator.hasNext())
      deleteResource((BaseResourceMeta)localIterator.next());
    NoteMeta localNoteMeta = getDb().getNoteMetaById(paramString);
    if (localNoteMeta == null)
      return true;
    Snippet localSnippet = getSnippet(localNoteMeta);
    if (localSnippet != null)
      deleteSnippet(localSnippet.getRelativePath());
    try
    {
      getNoteCache().deleteNote(localNoteMeta);
      this.mYNoteDb.removeNoteContentVersion(localNoteMeta.getNoteId());
      if ((!getDb().deleteNoteMeta(localNoteMeta)) || (!adjustNoteBookCount(localNoteMeta.getNoteBook())));
      return false;
    }
    catch (IOException localIOException)
    {
      L.w(this, "Failed to delete cache for note " + localNoteMeta);
    }
  }

  public void deleteResource(BaseResourceMeta paramBaseResourceMeta)
  {
    try
    {
      getResourceCache(paramBaseResourceMeta.getType()).deleteCacheItem(paramBaseResourceMeta.genRelativePath());
      getResourceCache(paramBaseResourceMeta.getType()).deleteCacheItem(getTmpResourcePath(paramBaseResourceMeta.genRelativePath()));
      if (ResourceUtils.hasThumbnail(paramBaseResourceMeta))
        getThumbnailCache().deleteCacheItem(paramBaseResourceMeta.genRelativePath());
      this.mYNoteDb.deleteResource(paramBaseResourceMeta);
      return;
    }
    catch (IOException localIOException)
    {
      L.e(this, "delete resource failed", localIOException);
    }
  }

  public boolean deleteSnippet(String paramString)
  {
    try
    {
      boolean bool = getSnippetCache().deleteCacheItem(paramString);
      return bool;
    }
    catch (IOException localIOException)
    {
      L.e(this, "Failed to delete snippet " + paramString, localIOException);
    }
    return false;
  }

  public boolean existNoteMeta(String paramString)
  {
    return getDb().getNoteMetaById(paramString) != null;
  }

  public boolean existResource(BaseResourceMeta paramBaseResourceMeta)
  {
    return getResourceCache(paramBaseResourceMeta.getType()).exist(paramBaseResourceMeta.genRelativePath());
  }

  public boolean existThumbnail(String paramString)
  {
    return getThumbnailCache().exist(paramString);
  }

  public boolean exsitNoteBook(String paramString)
  {
    return getDb().getNoteBookMetaById(paramString) != null;
  }

  public Cursor getAllSearchHistory()
  {
    return getSearchDb().getAllQuery();
  }

  public List<String> getAllSearchHistoryList()
  {
    Cursor localCursor = getAllSearchHistory();
    ArrayList localArrayList;
    CursorHelper localCursorHelper;
    try
    {
      localArrayList = new ArrayList(localCursor.getCount());
      localCursorHelper = new CursorHelper(localCursor);
      if (!localCursor.moveToNext())
        break label65;
    }
    finally
    {
      localCursor.close();
    }
    label65: localCursor.close();
    return localArrayList;
  }

  public AudioCache getAudioCache()
  {
    if (this.audioCache == null)
      monitorenter;
    try
    {
      if (this.audioCache == null)
        this.audioCache = new AudioCache(this.mYNote);
      return this.audioCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public String getAvableNoteTitle(String paramString)
  {
    return this.mYNote.getResources().getString(2131361841) + paramString;
  }

  public CacheManager getCacheManager()
  {
    if (this.mCacheManager == null)
      monitorenter;
    try
    {
      if (this.mCacheManager == null)
        this.mCacheManager = new CacheManager();
      return this.mCacheManager;
    }
    finally
    {
      monitorexit;
    }
  }

  public YNoteDB getDb()
  {
    if (this.mYNoteDb == null)
      monitorenter;
    try
    {
      if (this.mYNoteDb == null)
        this.mYNoteDb = new YNoteDB(this.mYNote);
      return this.mYNoteDb;
    }
    finally
    {
      monitorexit;
    }
  }

  public String getDefaultNoteBookId()
  {
    return this.mYNote.getDefaultNoteBook();
  }

  public List<NoteMeta> getDirtyAndMovedNoteMetas()
  {
    return getDb().getDirtyAndMovedNoteMetas();
  }

  public List<NoteBook> getDirtyNoteBookMetas()
  {
    Cursor localCursor = getDb().getDirtyNotebookMetas();
    try
    {
      List localList = cursor2NoteBookList(localCursor);
      return localList;
    }
    finally
    {
      localCursor.close();
    }
  }

  public List<BaseResourceMeta> getDirtyResourcesOf(NoteMeta paramNoteMeta)
  {
    return getDb().getDirtyResourcesOf(paramNoteMeta.getNoteId());
  }

  public DoodleCache getDoodleCache()
  {
    if (this.doodleCache == null)
      monitorenter;
    try
    {
      if (this.doodleCache == null)
        this.doodleCache = new DoodleCache(this.mYNote);
      return this.doodleCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public GeneralResourceCache getGeneralResourceCache()
  {
    if (this.generalResourceCache == null)
      monitorenter;
    try
    {
      if (this.generalResourceCache == null)
        this.generalResourceCache = new GeneralResourceCache(this.mYNote);
      return this.generalResourceCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public HandwriteCache getHandwriteCache()
  {
    if (this.handwriteCache == null)
      monitorenter;
    try
    {
      if (this.handwriteCache == null)
        this.handwriteCache = new HandwriteCache(this.mYNote);
      return this.handwriteCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public ImageCache getImageCache()
  {
    if (this.imageCache == null)
      monitorenter;
    try
    {
      if (this.imageCache == null)
        this.imageCache = new ImageCache(this.mYNote);
      return this.imageCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public ImageResource getImageResource(ImageResourceMeta paramImageResourceMeta)
  {
    return new ImageResource(paramImageResourceMeta);
  }

  public ImageResource getImageResource(String paramString)
  {
    getCacheManager().touchCacheItem(paramString, 2);
    ImageResourceMeta localImageResourceMeta = new ImageResourceMeta(getDb().getResourceById(paramString));
    ImageResource localImageResource = new ImageResource();
    localImageResource.setMeta(localImageResourceMeta);
    return localImageResource;
  }

  public Note getNote(NoteMeta paramNoteMeta)
  {
    Note localNote = new Note(paramNoteMeta, null);
    try
    {
      String str = getNoteCache().getNoteContent(localNote);
      if (str != null)
      {
        getCacheManager().touchCacheItem(paramNoteMeta.getNoteId(), 1);
        localNote.setBody(str);
        return localNote;
      }
      this.mYNoteDb.removeNoteContentVersion(paramNoteMeta.getNoteId());
      return null;
    }
    catch (IOException localIOException)
    {
      L.e(this, "Failed to load note content from cache.", localIOException);
    }
    return null;
  }

  public NoteBook getNoteBookMetaById(String paramString)
  {
    return getDb().getNoteBookMetaById(paramString);
  }

  public NoteBook getNoteBookMetaByTitle(String paramString)
  {
    return getDb().getNoteBookMetaByTitle(paramString);
  }

  public NoteCache getNoteCache()
  {
    if (this.mNoteCache == null)
      monitorenter;
    try
    {
      if (this.mNoteCache == null)
        this.mNoteCache = new NoteCache(this.mYNote);
      return this.mNoteCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public int getNoteContentVersion(String paramString)
  {
    return this.mYNoteDb.getNoteContentVersion(paramString);
  }

  public NoteMeta getNoteMetaById(String paramString)
  {
    return getDb().getNoteMetaById(paramString);
  }

  public NoteMeta getNoteMetaByTitle(String paramString)
  {
    return getDb().getNoteMetaByTitle(paramString);
  }

  public AbstractResource<? extends IResourceMeta> getResource(BaseResourceMeta paramBaseResourceMeta)
  {
    return (AbstractResource)new ResourceMetaSwitcher(paramBaseResourceMeta, paramBaseResourceMeta)
    {
      protected AbstractResource<? extends IResourceMeta> onAudioType()
      {
        if (this.val$baseMeta instanceof AudioResourceMeta);
        for (AudioResourceMeta localAudioResourceMeta = (AudioResourceMeta)this.val$baseMeta; ; localAudioResourceMeta = new AudioResourceMeta(this.val$baseMeta))
          return new AudioResource(localAudioResourceMeta);
      }

      protected AbstractResource<? extends IResourceMeta> onDoodleType()
      {
        if (this.val$baseMeta instanceof DoodleResourceMeta);
        for (DoodleResourceMeta localDoodleResourceMeta = (DoodleResourceMeta)this.val$baseMeta; ; localDoodleResourceMeta = new DoodleResourceMeta(this.val$baseMeta))
          return new DoodleResource(localDoodleResourceMeta);
      }

      protected AbstractResource<? extends IResourceMeta> onGeneralType()
      {
        if (this.val$baseMeta instanceof GeneralResourceMeta);
        for (GeneralResourceMeta localGeneralResourceMeta = (GeneralResourceMeta)this.val$baseMeta; ; localGeneralResourceMeta = new GeneralResourceMeta(this.val$baseMeta))
          return new GeneralResource(localGeneralResourceMeta);
      }

      protected AbstractResource<? extends IResourceMeta> onHandwriteType()
      {
        if (this.val$baseMeta instanceof HandwriteResourceMeta);
        for (HandwriteResourceMeta localHandwriteResourceMeta = (HandwriteResourceMeta)this.val$baseMeta; ; localHandwriteResourceMeta = new HandwriteResourceMeta(this.val$baseMeta))
          return new HandwriteResource(localHandwriteResourceMeta);
      }

      protected AbstractResource<? extends IResourceMeta> onImageType()
      {
        if (this.val$baseMeta instanceof ImageResourceMeta);
        for (ImageResourceMeta localImageResourceMeta = (ImageResourceMeta)this.val$baseMeta; ; localImageResourceMeta = new ImageResourceMeta(this.val$baseMeta))
          return new ImageResource(localImageResourceMeta);
      }
    }
    .doSwitch();
  }

  public BaseResourceCache getResourceCache(int paramInt)
  {
    return (BaseResourceCache)new ResourceMetaSwitcher(paramInt)
    {
      protected BaseResourceCache onAudioType()
      {
        return DataSource.this.getAudioCache();
      }

      protected BaseResourceCache onDoodleType()
      {
        return DataSource.this.getDoodleCache();
      }

      protected BaseResourceCache onGeneralType()
      {
        return DataSource.this.getGeneralResourceCache();
      }

      protected BaseResourceCache onHandwriteType()
      {
        return DataSource.this.getHandwriteCache();
      }

      protected BaseResourceCache onImageType()
      {
        return DataSource.this.getImageCache();
      }
    }
    .doSwitch();
  }

  public long getResourceLength(BaseResourceMeta paramBaseResourceMeta)
  {
    return new File(getResourceCache(paramBaseResourceMeta.getType()).getAbsolutePath(paramBaseResourceMeta.genRelativePath())).length();
  }

  public BaseResourceMeta getResourceMeta(String paramString)
  {
    return getDb().getResourceById(paramString);
  }

  public ArrayList<BaseResourceMeta> getResourceMetasByNoteId(String paramString)
  {
    return getDb().getResourcesByNoteId(paramString);
  }

  public String getResourcePath(IResourceMeta paramIResourceMeta)
  {
    getCacheManager().touchCacheItem(paramIResourceMeta.getResourceId(), 2);
    return getResourceCache(paramIResourceMeta.getType()).getAbsolutePath(paramIResourceMeta.genRelativePath());
  }

  public NoteBook getRootMeta()
  {
    return getDb().getRootMeta();
  }

  public Snippet getSnippet(NoteMeta paramNoteMeta)
  {
    Snippet localSnippet = new Snippet(paramNoteMeta);
    if (paramNoteMeta.hasSnippet())
      return localSnippet;
    if (localSnippet.exist())
      deleteSnippet(localSnippet.getRelativePath());
    return null;
  }

  public SnippetCache getSnippetCache()
  {
    if (this.snippetCache == null)
      monitorenter;
    try
    {
      if (this.snippetCache == null)
        this.snippetCache = new SnippetCache(this.mYNote);
      return this.snippetCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public TempFileCache getTempFileCache()
  {
    if (this.tempFileCache == null)
      monitorenter;
    try
    {
      if (this.tempFileCache == null)
        this.tempFileCache = new TempFileCache(this.mYNote);
      return this.tempFileCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public Thumbnail getThumbnail(AbstractImageResourceMeta paramAbstractImageResourceMeta)
  {
    return new Thumbnail(paramAbstractImageResourceMeta);
  }

  public ThumbnailCache getThumbnailCache()
  {
    if (this.thumbnailCache == null)
      monitorenter;
    try
    {
      if (this.thumbnailCache == null)
        this.thumbnailCache = new ThumbnailCache(this.mYNote);
      return this.thumbnailCache;
    }
    finally
    {
      monitorexit;
    }
  }

  public String getThumbnailPath(IResourceMeta paramIResourceMeta)
  {
    if (ResourceUtils.hasThumbnail(paramIResourceMeta))
      return getThumbnailCache().getAbsolutePath(paramIResourceMeta.genRelativePath());
    return FileUtils.getResourceIconPath(paramIResourceMeta.getFileName());
  }

  public UserMeta getUserMeta()
  {
    return getDb().getUserMeta(this.mYNote.getUserName());
  }

  public boolean insertOrUpdateNote(Note paramNote)
    throws IOException
  {
    if (insertOrUpdateNoteMeta(paramNote.getNoteMeta()))
      return updateNoteCache(paramNote);
    return false;
  }

  public boolean insertOrUpdateNote(Note paramNote, String paramString)
    throws IOException
  {
    NoteMeta localNoteMeta = getNoteMetaById(paramNote.getNoteId());
    if (localNoteMeta != null)
      getNoteCache().deleteNote(localNoteMeta);
    if (insertOrUpdateNote(paramNote))
    {
      adjustNotebookCount(paramNote.getNoteMeta(), paramString);
      return true;
    }
    return false;
  }

  public boolean insertOrUpdateNoteBookMeta(NoteBook paramNoteBook)
  {
    return getDb().insertOrUpdateNoteBookMeta(paramNoteBook);
  }

  public boolean insertOrUpdateNoteMeta(NoteMeta paramNoteMeta)
  {
    NoteMeta localNoteMeta = getDb().getNoteMetaById(paramNoteMeta.getNoteId());
    if ((localNoteMeta != null) && (localNoteMeta.getSnippetUrl() != null) && (!localNoteMeta.getSnippetUrl().equals(paramNoteMeta.getSnippetUrl())))
    {
      Snippet localSnippet = getSnippet(localNoteMeta);
      if (localSnippet != null)
        deleteSnippet(localSnippet.getRelativePath());
    }
    return getDb().insertOrUpdateNoteMeta(paramNoteMeta);
  }

  public boolean insertOrUpdateNoteMeta(NoteMeta paramNoteMeta, String paramString)
  {
    NoteMeta localNoteMeta = getNoteMetaById(paramNoteMeta.getNoteId());
    Note localNote = getNote(localNoteMeta);
    if (localNote != null);
    try
    {
      localNote.setNoteMeta(paramNoteMeta);
      if (!getNoteCache().updateNote(localNote))
        return false;
      getNoteCache().deleteNote(localNoteMeta);
      if (!insertOrUpdateNoteMeta(paramNoteMeta))
        break label77;
      adjustNotebookCount(paramNoteMeta, paramString);
      return false;
    }
    catch (IOException localIOException)
    {
      L.e(this, "Failed to move note content.", localIOException);
    }
    label77: return false;
  }

  public boolean insertOrUpdateResource(AbstractResource<? extends IResourceMeta> paramAbstractResource)
  {
    if (insertOrUpdateResourceMeta(paramAbstractResource.getMeta()))
      return updateResourceCache(paramAbstractResource);
    return false;
  }

  public boolean insertOrUpdateResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    return getDb().insertOrUpdateResource(paramBaseResourceMeta);
  }

  public boolean insertOrUpdateUserMeta(String paramString, UserMeta paramUserMeta)
  {
    return getDb().insertOrUpdateUserMeta(paramString, paramUserMeta);
  }

  public Cursor listAllNoteBooks()
  {
    return getDb().getAllNoteBookMetasCursor();
  }

  public NoteBook[] listAllNoteBooksAsArray()
  {
    Cursor localCursor = listAllNoteBooks();
    try
    {
      NoteBook[] arrayOfNoteBook = cursor2NoteBookArray(localCursor);
      return arrayOfNoteBook;
    }
    finally
    {
      localCursor.close();
    }
  }

  public List<NoteBook> listAllNoteBooksAsList()
  {
    Cursor localCursor = listAllNoteBooks();
    try
    {
      List localList = cursor2NoteBookList(localCursor);
      return localList;
    }
    finally
    {
      localCursor.close();
    }
  }

  public Cursor listAllNotes()
  {
    return getDb().getAllNoteMetasCursor();
  }

  public NoteMeta[] listAllNotesAsArray()
  {
    Cursor localCursor = listAllNotes();
    try
    {
      NoteMeta[] arrayOfNoteMeta = cursor2Array(localCursor, false);
      return arrayOfNoteMeta;
    }
    finally
    {
      localCursor.close();
    }
  }

  public Cursor listNoteByNotebook(String paramString)
  {
    return getDb().getNoteByNoteBook(paramString);
  }

  public List<NoteMeta> listNotesByNotebookAsList(String paramString)
  {
    Cursor localCursor = listNoteByNotebook(paramString);
    try
    {
      List localList = cursor2NoteMetaList(localCursor);
      return localList;
    }
    finally
    {
      localCursor.close();
    }
  }

  public boolean markDeleteNote(NoteMeta paramNoteMeta)
  {
    if (getDb().markDeleteNote(paramNoteMeta))
      adjustNoteBookCount(paramNoteMeta.getNoteBook());
    return false;
  }

  public boolean markDeleteNotebook(NoteBook paramNoteBook)
  {
    if (getDb().markDeleteNoteBook(paramNoteBook))
      return markDeleteNotesInNotebook(paramNoteBook.getNoteBookId());
    return false;
  }

  public boolean moveNotesBetweenNoteBook(String paramString1, String paramString2)
    throws NoteBookNotExistException
  {
    if (getNoteBookMetaById(paramString2) == null)
      throw new NoteBookNotExistException(paramString2 + " not exist");
    Cursor localCursor = getDb().getNoteByNoteBook(paramString1);
    while (true)
    {
      NoteMeta localNoteMeta;
      Note localNote;
      try
      {
        if (!localCursor.moveToNext())
          break label194;
        localNoteMeta = NoteMeta.fromCursor(localCursor);
        localNote = getNote(localNoteMeta);
        if (localNoteMeta.getVersion() == 0)
          localNoteMeta.setServerNoteBook(paramString2);
      }
      finally
      {
        localCursor.close();
      }
    }
    label194: localCursor.close();
    adjustNoteBookCount(paramString1);
    adjustNoteBookCount(paramString2);
    return true;
  }

  public boolean reGenerateNoteMeta(NoteMeta paramNoteMeta)
  {
    Note localNote = getNote(paramNoteMeta);
    paramNoteMeta.setNoteId(IdUtils.genNoteId());
    paramNoteMeta.setServerNoteBook(paramNoteMeta.getNoteBook());
    paramNoteMeta.setDirty(true);
    paramNoteMeta.setTitle(getAvableNoteTitle(paramNoteMeta.getTitle()));
    getDb().insertOrUpdateNoteMeta(paramNoteMeta);
    try
    {
      boolean bool = getNoteCache().updateNote(localNote);
      return bool;
    }
    catch (IOException localIOException)
    {
      L.e(this, "regenerate note cache failed.", localIOException);
    }
    return false;
  }

  public boolean removeNoteContentVersion(String paramString)
  {
    return this.mYNoteDb.removeNoteContentVersion(paramString);
  }

  public boolean resetDataBase()
  {
    try
    {
      getDb().resetDataBase();
      getSearchDb().resetDataBase();
      return true;
    }
    catch (Exception localException)
    {
      Log.e(TAG, "Failed to reset data base.", localException);
    }
    return false;
  }

  public void saveSearchQuery(String paramString)
  {
    getSearchDb().saveQuery(paramString);
  }

  public ListNoteMetas searchNotes(String paramString)
  {
    Cursor localCursor = getDb().searchNotes(paramString);
    try
    {
      ListNoteMetas localListNoteMetas = new ListNoteMetas();
      localListNoteMetas.setFileMetas(cursor2Array(localCursor, false));
      localListNoteMetas.setTotalNumber(localCursor.getCount());
      return localListNoteMetas;
    }
    finally
    {
      localCursor.close();
    }
  }

  public void switchUser()
  {
    if (this.mYNoteDb != null)
      this.mYNoteDb.close();
    if (this.mYNoteSearchDb != null)
      this.mYNoteSearchDb.close();
    this.mYNoteDb = new YNoteDB(this.mYNote);
    this.mYNoteSearchDb = new YNoteSearchHistoryDB(this.mYNote);
  }

  public boolean updateNoteBook(NoteBook paramNoteBook)
  {
    return getDb().insertOrUpdateNoteBookMeta(paramNoteBook);
  }

  public boolean updateNoteCache(Note paramNote)
    throws IOException
  {
    boolean bool = getNoteCache().updateNote(paramNote);
    if (bool)
    {
      getDb().updateNoteContentVersion(paramNote.getNoteId(), paramNote.getNoteMeta().getVersion());
      getCacheManager().touchCacheItem(paramNote.getNoteId(), 1, paramNote.getContentBytes().length);
    }
    return bool;
  }

  public boolean updateNoteFromCache(Note paramNote)
  {
    try
    {
      paramNote.setBody(getNoteCache().getNoteContent(paramNote));
      L.d(this, "update note from cache " + paramNote.getBody());
      return true;
    }
    catch (Exception localException)
    {
      L.e(this, "Failed to update note from cache." + paramNote, localException);
    }
    return false;
  }

  public boolean updateResourceCache(AbstractResource<? extends IResourceMeta> paramAbstractResource)
  {
    if (!paramAbstractResource.isDataEmpty())
      try
      {
        boolean bool = getResourceCache(paramAbstractResource.getMeta().getType()).updateCacheItem(paramAbstractResource);
        L.d(this, "Update resrouce cache succeed. " + paramAbstractResource.getMeta().getResourceId());
        if (bool)
          getCacheManager().touchCacheItem(paramAbstractResource.getMeta().getResourceId(), 2, paramAbstractResource.getLength());
        return bool;
      }
      catch (IOException localIOException)
      {
        L.e(this, "Failed to update resource cache.", localIOException);
        return false;
      }
    return true;
  }

  public boolean updateRootNoteBookMeta(NoteBook paramNoteBook)
  {
    boolean bool = getDb().updateRootNoteBookMeta(paramNoteBook);
    L.d(this, "Update root noet book meta ret is " + bool);
    return bool;
  }

  public boolean writeSnippet(Snippet paramSnippet)
  {
    try
    {
      boolean bool = getSnippetCache().updateCacheItem(paramSnippet);
      return bool;
    }
    catch (Exception localException)
    {
      L.e(this, "Failed to update snippet cache.", localException);
    }
    return false;
  }

  public boolean writeThumbnail(Thumbnail paramThumbnail)
  {
    try
    {
      boolean bool = getThumbnailCache().updateCacheItem(paramThumbnail);
      return bool;
    }
    catch (IOException localIOException)
    {
      L.e(this, "Failed to update thumbnail cache.", localIOException);
    }
    return false;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.datasource.DataSource
 * JD-Core Version:    0.5.4
 */