package com.youdao.note.provider;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.youdao.note.activity.ActivityConsts;
import com.youdao.note.activity.MainActivity;
import com.youdao.note.activity.NewNoteActivity;
import com.youdao.note.activity.SearchActivity;
import com.youdao.note.utils.PreferenceKeys;

public class YNoteWidgetProvider extends AppWidgetProvider
  implements ActivityConsts, PreferenceKeys
{
  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; ++j)
    {
      int k = paramArrayOfInt[j];
      RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903041);
      localRemoteViews.setOnClickPendingIntent(2131165188, PendingIntent.getActivity(paramContext, 0, new Intent(paramContext, SearchActivity.class), 0));
      Intent localIntent = new Intent(paramContext, NewNoteActivity.class);
      localIntent.putExtra("entry_from", "WidgetAddTimes");
      localIntent.setAction("com.youdao.note.action.CREATE_TEXT");
      localRemoteViews.setOnClickPendingIntent(2131165189, PendingIntent.getActivity(paramContext, 0, localIntent, 0));
      localIntent.setAction("com.youdao.note.action.CREATE_SNAPSHOT");
      localRemoteViews.setOnClickPendingIntent(2131165190, PendingIntent.getActivity(paramContext, 0, localIntent, 0));
      localIntent.setAction("com.youdao.note.action.CREATE_RECORD");
      localRemoteViews.setOnClickPendingIntent(2131165191, PendingIntent.getActivity(paramContext, 0, localIntent, 0));
      localRemoteViews.setOnClickPendingIntent(2131165187, PendingIntent.getActivity(paramContext, 0, new Intent(paramContext, MainActivity.class), 0));
      paramAppWidgetManager.updateAppWidget(k, localRemoteViews);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.provider.YNoteWidgetProvider
 * JD-Core Version:    0.5.4
 */