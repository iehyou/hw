package com.youdao.note.provider;

import android.content.SearchRecentSuggestionsProvider;

public class SearchSuggestProvider extends SearchRecentSuggestionsProvider
{
  public static final String AUTHORITY = SearchSuggestProvider.class.getName().toLowerCase();
  public static final int MODE = 1;

  public SearchSuggestProvider()
  {
    setupSuggestions(AUTHORITY, 1);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.provider.SearchSuggestProvider
 * JD-Core Version:    0.5.4
 */