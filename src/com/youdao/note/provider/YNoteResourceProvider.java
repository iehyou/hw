package com.youdao.note.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.database.YNoteDB;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

@Deprecated
public class YNoteResourceProvider extends ContentProvider
{
  public static final String AUTHORITY = YNoteResourceProvider.class.getName().toLowerCase();
  public static Uri CONTENT_URI = null;
  private static final int FILE_NAME_IDX = 2;
  private static final int RESOURCE_ID_IDX = 1;
  private static final int VERSION_ID = 0;
  private static final int VERSION_RESOURCEID_FILENAME = 1;
  private static final UriMatcher sUriMatcher = new UriMatcher(-1);

  static
  {
    CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    Log.d("YNoteResourceProvider", "YNoteResource provider autority is " + CONTENT_URI.getAuthority());
    sUriMatcher.addURI(AUTHORITY, "/#/*/*", 1);
  }

  public static BaseResourceMeta buildResource(Uri paramUri)
  {
    List localList = paramUri.getPathSegments();
    BaseResourceMeta localBaseResourceMeta = ResourceUtils.newResourceMeta(1);
    if (localList.size() == 3)
    {
      localBaseResourceMeta.setFileName((String)localList.get(2));
      localBaseResourceMeta.setResourceId((String)localList.get(1));
      localBaseResourceMeta.setVersion(Integer.parseInt((String)localList.get(0)));
    }
    return localBaseResourceMeta;
  }

  public static Uri buildUri(BaseResourceMeta paramBaseResourceMeta)
  {
    Uri localUri = CONTENT_URI.buildUpon().appendPath(Integer.toString(paramBaseResourceMeta.getVersion())).appendPath(paramBaseResourceMeta.getResourceId()).appendPath(paramBaseResourceMeta.getFileName()).build();
    Log.d("YNoteResourceProvider", "build uri " + localUri.toString());
    return localUri;
  }

  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }

  public String getType(Uri paramUri)
  {
    L.d(this, "ynote resource provider get type");
    BaseResourceMeta localBaseResourceMeta = buildResource(paramUri);
    String str1 = YNoteApplication.getInstance().getDataSource().getDb().getResourceById(localBaseResourceMeta.getResourceId()).getFileName();
    if (!TextUtils.isEmpty(str1))
    {
      int i = str1.lastIndexOf('.');
      if (i >= 0)
      {
        String str2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str1.substring(i + 1));
        L.d(this, "type is " + str2);
        return str2;
      }
    }
    return "unknow";
  }

  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    return null;
  }

  public boolean onCreate()
  {
    L.d(this, "***************Ynote resource provider created.");
    return true;
  }

  public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
    throws FileNotFoundException
  {
    L.d(this, "come to ynote resource provider " + paramUri.getPath());
    BaseResourceMeta localBaseResourceMeta = buildResource(paramUri);
    return ParcelFileDescriptor.open(new File(YNoteApplication.getInstance().getDataSource().getResourcePath(localBaseResourceMeta)), 268435456);
  }

  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    L.d(this, "ynote resource provider query");
    return null;
  }

  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.provider.YNoteResourceProvider
 * JD-Core Version:    0.5.4
 */