package com.youdao.note.data.resource;

public class DoodleResourceMeta extends AbstractImageResourceMeta
{
  private static final long serialVersionUID = 7037286255581361058L;

  public DoodleResourceMeta()
  {
    setType(2);
  }

  public DoodleResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    setType(2);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.DoodleResourceMeta
 * JD-Core Version:    0.5.4
 */