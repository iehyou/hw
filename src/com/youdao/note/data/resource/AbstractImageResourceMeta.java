package com.youdao.note.data.resource;

import android.graphics.BitmapFactory.Options;
import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.ImageCache;
import com.youdao.note.utils.ImageUtils;
import com.youdao.note.utils.L;
import java.io.FileNotFoundException;
import java.util.Map;

public class AbstractImageResourceMeta extends BaseResourceMeta
{
  private static final String KEY_HEIGHT = "height";
  private static final String KEY_WIDTH = "width";
  private static final long serialVersionUID = 7684984004059218457L;

  public AbstractImageResourceMeta()
  {
  }

  public AbstractImageResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
  }

  private void loadWidthAndHeight()
  {
    try
    {
      BitmapFactory.Options localOptions = ImageUtils.getBitmapOption(YNoteApplication.getInstance().getDataSource().getImageCache().getAbsolutePath(genRelativePath()));
      this.transientValues.put("width", Integer.valueOf(localOptions.outWidth));
      this.transientValues.put("height", Integer.valueOf(localOptions.outHeight));
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      L.e(this, "", localFileNotFoundException);
    }
  }

  public int getHeight()
  {
    Integer localInteger = (Integer)this.transientValues.get("height");
    if (localInteger == null)
    {
      loadWidthAndHeight();
      localInteger = (Integer)this.transientValues.get("height");
    }
    return localInteger.intValue();
  }

  public int getWidth()
  {
    Integer localInteger = (Integer)this.transientValues.get("width");
    if (localInteger == null)
    {
      loadWidthAndHeight();
      localInteger = (Integer)this.transientValues.get("width");
    }
    return localInteger.intValue();
  }

  public void setHeight(int paramInt)
  {
    this.transientValues.put("height", Integer.valueOf(paramInt));
  }

  public void setWidth(int paramInt)
  {
    this.transientValues.put("width", Integer.valueOf(paramInt));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.AbstractImageResourceMeta
 * JD-Core Version:    0.5.4
 */