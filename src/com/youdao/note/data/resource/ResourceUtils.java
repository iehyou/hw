package com.youdao.note.data.resource;

import android.database.Cursor;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.StringUtils;
import com.youdao.note.utils.YNoteUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class ResourceUtils
{
  public static BaseResourceMeta fromCursor(Cursor paramCursor)
  {
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    BaseResourceMeta localBaseResourceMeta = newResourceMeta(localCursorHelper.getInt("type"));
    localBaseResourceMeta.setFileName(localCursorHelper.getString("filename"));
    localBaseResourceMeta.setNoteId(localCursorHelper.getString("noteid"));
    localBaseResourceMeta.setVersion(localCursorHelper.getInt("version"));
    localBaseResourceMeta.setResourceId(localCursorHelper.getString("_id"));
    localBaseResourceMeta.setLength(localCursorHelper.getInt("length"));
    localBaseResourceMeta.setDirty(localCursorHelper.getBoolean("is_dirty"));
    try
    {
      localBaseResourceMeta.extProp = YNoteUtils.json2Map(new JSONObject(localCursorHelper.getString("props")));
      return localBaseResourceMeta;
    }
    catch (JSONException localJSONException)
    {
      L.e(ResourceUtils.class, "Resource init failed.", localJSONException);
    }
    return localBaseResourceMeta;
  }

  public static AudioResource genEmptyAudioResource()
  {
    return new AudioResource((AudioResourceMeta)genEmptyResourceMeta(4));
  }

  public static DoodleResource genEmptyDoodleResource()
  {
    return new DoodleResource((DoodleResourceMeta)genEmptyResourceMeta(2));
  }

  public static GeneralResource genEmptyGeneralResource()
  {
    return new GeneralResource((GeneralResourceMeta)genEmptyResourceMeta(1));
  }

  public static HandwriteResource genEmptyHandwriteResource()
  {
    return new HandwriteResource((HandwriteResourceMeta)genEmptyResourceMeta(3));
  }

  public static ImageResource genEmptyImageResource()
  {
    return new ImageResource((ImageResourceMeta)genEmptyResourceMeta(0));
  }

  public static ImageResource genEmptyImageResource(String paramString)
  {
    return new ImageResource((ImageResourceMeta)genEmptyResourceMeta(0, paramString));
  }

  public static BaseResourceMeta genEmptyResourceMeta(int paramInt)
  {
    BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)new ResourceMetaSwitcher(paramInt)
    {
      protected BaseResourceMeta onAudioType()
      {
        AudioResourceMeta localAudioResourceMeta = new AudioResourceMeta();
        localAudioResourceMeta.setResourceId(IdUtils.genResourceId("mp3"));
        localAudioResourceMeta.setFileName(StringUtils.currentTimeStr() + ".mp3");
        return localAudioResourceMeta;
      }

      protected BaseResourceMeta onDoodleType()
      {
        DoodleResourceMeta localDoodleResourceMeta = new DoodleResourceMeta();
        localDoodleResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localDoodleResourceMeta.setFileName(localDoodleResourceMeta.getResourceId() + ".jpg");
        return localDoodleResourceMeta;
      }

      protected BaseResourceMeta onGeneralType()
      {
        GeneralResourceMeta localGeneralResourceMeta = new GeneralResourceMeta();
        localGeneralResourceMeta.setResourceId(IdUtils.genResourceId(""));
        localGeneralResourceMeta.setFileName(localGeneralResourceMeta.getResourceId());
        return localGeneralResourceMeta;
      }

      protected BaseResourceMeta onHandwriteType()
      {
        HandwriteResourceMeta localHandwriteResourceMeta = new HandwriteResourceMeta();
        localHandwriteResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localHandwriteResourceMeta.setFileName(localHandwriteResourceMeta.getResourceId() + ".jpg");
        return localHandwriteResourceMeta;
      }

      protected BaseResourceMeta onImageType()
      {
        ImageResourceMeta localImageResourceMeta = new ImageResourceMeta();
        localImageResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localImageResourceMeta.setFileName(localImageResourceMeta.getResourceId() + ".jpg");
        return localImageResourceMeta;
      }
    }
    .doSwitch();
    localBaseResourceMeta.setDirty(true);
    localBaseResourceMeta.setVersion(-1);
    return localBaseResourceMeta;
  }

  public static BaseResourceMeta genEmptyResourceMeta(int paramInt, String paramString)
  {
    if (paramString == null)
      return genEmptyResourceMeta(paramInt);
    BaseResourceMeta localBaseResourceMeta = (BaseResourceMeta)new ResourceMetaSwitcher(paramInt, paramString)
    {
      protected BaseResourceMeta onAudioType()
      {
        AudioResourceMeta localAudioResourceMeta = new AudioResourceMeta();
        localAudioResourceMeta.setResourceId(IdUtils.genResourceId("mp3"));
        localAudioResourceMeta.setFileName(this.val$fileName);
        return localAudioResourceMeta;
      }

      protected BaseResourceMeta onDoodleType()
      {
        DoodleResourceMeta localDoodleResourceMeta = new DoodleResourceMeta();
        localDoodleResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localDoodleResourceMeta.setFileName(this.val$fileName);
        return localDoodleResourceMeta;
      }

      protected BaseResourceMeta onGeneralType()
      {
        GeneralResourceMeta localGeneralResourceMeta = new GeneralResourceMeta();
        localGeneralResourceMeta.setResourceId(IdUtils.genResourceId(""));
        localGeneralResourceMeta.setFileName(this.val$fileName);
        return localGeneralResourceMeta;
      }

      protected BaseResourceMeta onHandwriteType()
      {
        HandwriteResourceMeta localHandwriteResourceMeta = new HandwriteResourceMeta();
        localHandwriteResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localHandwriteResourceMeta.setFileName(this.val$fileName);
        return localHandwriteResourceMeta;
      }

      protected BaseResourceMeta onImageType()
      {
        ImageResourceMeta localImageResourceMeta = new ImageResourceMeta();
        localImageResourceMeta.setResourceId(IdUtils.genResourceId("jpg"));
        localImageResourceMeta.setFileName(this.val$fileName);
        return localImageResourceMeta;
      }
    }
    .doSwitch();
    localBaseResourceMeta.setDirty(true);
    localBaseResourceMeta.setVersion(-1);
    return localBaseResourceMeta;
  }

  public static boolean hasThumbnail(IResourceMeta paramIResourceMeta)
  {
    return paramIResourceMeta instanceof AbstractImageResourceMeta;
  }

  public static BaseResourceMeta newResourceMeta(int paramInt)
  {
    return (BaseResourceMeta)new ResourceMetaSwitcher(paramInt)
    {
      protected BaseResourceMeta onAudioType()
      {
        return new AudioResourceMeta();
      }

      protected BaseResourceMeta onDoodleType()
      {
        return new DoodleResourceMeta();
      }

      protected BaseResourceMeta onGeneralType()
      {
        return new GeneralResourceMeta();
      }

      protected BaseResourceMeta onHandwriteType()
      {
        return new HandwriteResourceMeta();
      }

      protected BaseResourceMeta onImageType()
      {
        return new ImageResourceMeta();
      }
    }
    .doSwitch();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.ResourceUtils
 * JD-Core Version:    0.5.4
 */