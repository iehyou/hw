package com.youdao.note.data.resource;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class HandwriteResource extends AbstractImageResource<HandwriteResourceMeta>
{
  private static final long serialVersionUID = 588851630494371575L;

  public HandwriteResource()
  {
  }

  public HandwriteResource(HandwriteResourceMeta paramHandwriteResourceMeta)
  {
    super(paramHandwriteResourceMeta);
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getHandwriteCache();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.HandwriteResource
 * JD-Core Version:    0.5.4
 */