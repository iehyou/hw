package com.youdao.note.data.resource;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class DoodleResource extends AbstractImageResource<DoodleResourceMeta>
{
  private static final long serialVersionUID = 1662562501089461218L;

  public DoodleResource()
  {
  }

  public DoodleResource(DoodleResourceMeta paramDoodleResourceMeta)
  {
    super(paramDoodleResourceMeta);
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getDoodleCache();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.DoodleResource
 * JD-Core Version:    0.5.4
 */