package com.youdao.note.data.resource;

public abstract class AbstractImageResource<T extends AbstractImageResourceMeta> extends AbstractResource<T>
{
  private static final long serialVersionUID = 5549041408791458424L;

  public AbstractImageResource()
  {
  }

  public AbstractImageResource(T paramT)
  {
    super(paramT);
  }

  public int getHeight()
  {
    return ((AbstractImageResourceMeta)this.meta).getHeight();
  }

  public int getWidth()
  {
    return ((AbstractImageResourceMeta)this.meta).getWidth();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.AbstractImageResource
 * JD-Core Version:    0.5.4
 */