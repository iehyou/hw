package com.youdao.note.data.resource;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class AudioResource extends AbstractResource<AudioResourceMeta>
{
  private static final long serialVersionUID = -8310654654413214317L;

  public AudioResource()
  {
  }

  public AudioResource(AudioResourceMeta paramAudioResourceMeta)
  {
    super(paramAudioResourceMeta);
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getAudioCache();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.AudioResource
 * JD-Core Version:    0.5.4
 */