package com.youdao.note.data.resource;

public class AudioResourceMeta extends BaseResourceMeta
{
  private static final long serialVersionUID = 451832390010587300L;

  public AudioResourceMeta()
  {
    setType(4);
  }

  public AudioResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    setType(4);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.AudioResourceMeta
 * JD-Core Version:    0.5.4
 */