package com.youdao.note.data.resource;

public class HandwriteResourceMeta extends AbstractImageResourceMeta
{
  private static final long serialVersionUID = 6589437101165493029L;

  public HandwriteResourceMeta()
  {
    setType(3);
  }

  public HandwriteResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    setType(3);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.HandwriteResourceMeta
 * JD-Core Version:    0.5.4
 */