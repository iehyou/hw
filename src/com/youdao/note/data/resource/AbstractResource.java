package com.youdao.note.data.resource;

import com.youdao.note.data.AbstractLocalCacheData;

public abstract class AbstractResource<T extends BaseResourceMeta> extends AbstractLocalCacheData
  implements IResource
{
  private static final long serialVersionUID = 3025005428020490001L;
  protected T meta;

  public AbstractResource()
  {
  }

  public AbstractResource(T paramT)
  {
    this.meta = paramT;
  }

  public String getFileName()
  {
    return this.meta.getFileName();
  }

  public T getMeta()
  {
    return this.meta;
  }

  public String getRelativePath()
  {
    return this.meta.genRelativePath();
  }

  public String getResourceId()
  {
    return this.meta.getResourceId();
  }

  public void setContentBytes(byte[] paramArrayOfByte)
  {
    this.datas = paramArrayOfByte;
    this.meta.setLength(paramArrayOfByte.length);
  }

  public void setMeta(T paramT)
  {
    this.meta = paramT;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.AbstractResource
 * JD-Core Version:    0.5.4
 */