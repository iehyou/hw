package com.youdao.note.data.resource;

import com.youdao.note.data.BaseMetaData;
import com.youdao.note.utils.L;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class BaseResourceMeta extends BaseMetaData
  implements IResourceMeta
{
  private static final String ICON_SRC = "icon_src";
  private static final long serialVersionUID = 6220265382105342566L;
  protected Map<String, Object> extProp = new HashMap(1);
  protected String fileName;
  protected String mNoteId;
  protected String mResourceId;
  protected Map<String, Object> transientValues = new HashMap();
  private int type;

  protected BaseResourceMeta()
  {
  }

  protected BaseResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    this.mNoteId = paramBaseResourceMeta.mNoteId;
    this.mResourceId = paramBaseResourceMeta.mResourceId;
    this.fileName = paramBaseResourceMeta.fileName;
    this.type = paramBaseResourceMeta.getType();
    this.transientValues = new HashMap(this.transientValues);
    this.extProp = paramBaseResourceMeta.extProp;
  }

  public String genRelativePath()
  {
    return getResourceId() + "-" + getFileName();
  }

  public String getFileName()
  {
    return this.fileName;
  }

  public String getNoteId()
  {
    return this.mNoteId;
  }

  public String getPropsAsStr()
  {
    if (this.extProp == null)
      return "{}";
    return new JSONObject(this.extProp).toString();
  }

  public String getResourceId()
  {
    return this.mResourceId;
  }

  public String getSrc()
  {
    Object localObject = this.extProp.get("icon_src");
    if (localObject != null)
    {
      L.d(this, "got src" + localObject);
      return (String)localObject;
    }
    L.d(this, "got src nothing");
    return "";
  }

  public int getType()
  {
    return this.type;
  }

  @Deprecated
  public boolean isDeleted()
  {
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public void setDeleted(boolean paramBoolean)
  {
    throw new UnsupportedOperationException();
  }

  public void setFileName(String paramString)
  {
    this.fileName = paramString;
  }

  public void setNoteId(String paramString)
  {
    this.mNoteId = paramString;
  }

  public void setResourceId(String paramString)
  {
    this.mResourceId = paramString;
  }

  public void setSrc(String paramString)
  {
    L.d(this, "set src " + paramString);
    this.extProp.put("icon_src", paramString);
  }

  public void setType(int paramInt)
  {
    this.type = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.BaseResourceMeta
 * JD-Core Version:    0.5.4
 */