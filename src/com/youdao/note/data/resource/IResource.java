package com.youdao.note.data.resource;

import com.youdao.note.data.ICacheable;
import java.io.Serializable;

public abstract interface IResource extends ICacheable, Serializable
{
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.IResource
 * JD-Core Version:    0.5.4
 */