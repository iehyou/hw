package com.youdao.note.data.resource;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class GeneralResource extends AbstractResource<GeneralResourceMeta>
{
  private static final long serialVersionUID = -4031407029816554163L;

  public GeneralResource()
  {
  }

  public GeneralResource(GeneralResourceMeta paramGeneralResourceMeta)
  {
    super(paramGeneralResourceMeta);
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getGeneralResourceCache();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.GeneralResource
 * JD-Core Version:    0.5.4
 */