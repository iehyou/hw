package com.youdao.note.data.resource;

public class ResourceConsts
{
  public static final int TYPE_AUDIO = 4;
  public static final int TYPE_DOODLE = 2;
  public static final int TYPE_GENERAL = 1;
  public static final int TYPE_HANDWRITE = 3;
  public static final int TYPE_IMAGE;
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.ResourceConsts
 * JD-Core Version:    0.5.4
 */