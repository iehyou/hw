package com.youdao.note.data.resource;

import android.net.Uri;
import com.youdao.note.YNoteApplication;
import com.youdao.note.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ImageResourceMeta extends AbstractImageResourceMeta
{
  public static final int FROM_CAMERA = 2;
  public static final int FROM_ORIGIN = 0;
  public static final int FROM_PHOTO = 1;
  private static final String KEY_PICFROM = "picFrom";
  private static final long serialVersionUID = -7126086004970813649L;

  public ImageResourceMeta()
  {
    setType(0);
  }

  public ImageResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    setType(0);
  }

  public void copyTempPicture(Uri paramUri)
    throws IOException
  {
    FileUtils.copyFile(paramUri, getTempFile().getAbsolutePath());
  }

  public int getPicFrom()
  {
    Integer localInteger = (Integer)this.transientValues.get("picFrom");
    if (localInteger == null)
      return 0;
    return localInteger.intValue();
  }

  public File getTempFile()
  {
    return new File(YNoteApplication.getInstance().getStoreDir(), genRelativePath());
  }

  public void setPicFrom(int paramInt)
  {
    this.transientValues.put("picFrom", Integer.valueOf(paramInt));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.ImageResourceMeta
 * JD-Core Version:    0.5.4
 */