package com.youdao.note.data.resource;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class ImageResource extends AbstractImageResource<ImageResourceMeta>
{
  private static final long serialVersionUID = 4236723922684253059L;

  public ImageResource()
  {
  }

  public ImageResource(ImageResourceMeta paramImageResourceMeta)
  {
    super(paramImageResourceMeta);
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getImageCache();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.ImageResource
 * JD-Core Version:    0.5.4
 */