package com.youdao.note.data.resource;

public class GeneralResourceMeta extends BaseResourceMeta
{
  private static final long serialVersionUID = -1399872752898377480L;

  public GeneralResourceMeta()
  {
    setType(1);
  }

  public GeneralResourceMeta(BaseResourceMeta paramBaseResourceMeta)
  {
    super(paramBaseResourceMeta);
    setType(1);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.GeneralResourceMeta
 * JD-Core Version:    0.5.4
 */