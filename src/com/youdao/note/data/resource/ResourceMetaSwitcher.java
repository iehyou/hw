package com.youdao.note.data.resource;

import com.youdao.note.utils.L;

public abstract class ResourceMetaSwitcher<T>
{
  private int type;

  public ResourceMetaSwitcher(int paramInt)
  {
    this.type = paramInt;
  }

  public ResourceMetaSwitcher(BaseResourceMeta paramBaseResourceMeta)
  {
    this(paramBaseResourceMeta.getType());
  }

  public T doSwitch()
  {
    switch (this.type)
    {
    default:
      L.e(this, "unknow type", null);
      return null;
    case 2:
      return onDoodleType();
    case 3:
      return onHandwriteType();
    case 0:
      return onImageType();
    case 1:
      return onGeneralType();
    case 4:
    }
    return onAudioType();
  }

  protected abstract T onAudioType();

  protected abstract T onDoodleType();

  protected abstract T onGeneralType();

  protected abstract T onHandwriteType();

  protected abstract T onImageType();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.ResourceMetaSwitcher
 * JD-Core Version:    0.5.4
 */