package com.youdao.note.data.resource;

import com.youdao.note.data.IMeta;

public abstract interface IResourceMeta extends IMeta
{
  public abstract String genRelativePath();

  public abstract String getFileName();

  public abstract String getNoteId();

  public abstract String getResourceId();

  public abstract int getType();

  public abstract void setFileName(String paramString);

  public abstract void setNoteId(String paramString);

  public abstract void setResourceId(String paramString);

  public abstract void setType(int paramInt);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.resource.IResourceMeta
 * JD-Core Version:    0.5.4
 */