package com.youdao.note.data;

public abstract interface ICacheable
{
  public abstract String getAbslutePath();

  public abstract byte[] getContentBytes();

  public abstract String getRelativePath();

  public abstract boolean isDataEmpty();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.ICacheable
 * JD-Core Version:    0.5.4
 */