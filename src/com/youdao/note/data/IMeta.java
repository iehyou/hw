package com.youdao.note.data;

import java.io.Serializable;

public abstract interface IMeta extends Serializable
{
  public abstract long getLength();

  public abstract int getVersion();

  public abstract boolean isDeleted();

  public abstract boolean isDirty();

  public abstract void setDeleted(boolean paramBoolean);

  public abstract void setDirty(boolean paramBoolean);

  public abstract void setLength(long paramLong);

  public abstract void setVersion(int paramInt);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.IMeta
 * JD-Core Version:    0.5.4
 */