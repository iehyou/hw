package com.youdao.note.data;

public class BaseMetaData extends BaseData
  implements IMeta
{
  private static final long serialVersionUID = 2728337280687691440L;
  private boolean isDeleted = false;
  private boolean isDirty = false;
  private long mLength = 0L;
  private int version = 0;

  public BaseMetaData()
  {
  }

  public BaseMetaData(BaseMetaData paramBaseMetaData)
  {
    this.isDeleted = paramBaseMetaData.isDeleted;
    this.isDirty = paramBaseMetaData.isDirty;
    this.version = paramBaseMetaData.version;
    this.mLength = paramBaseMetaData.mLength;
  }

  public long getLength()
  {
    return this.mLength;
  }

  public int getVersion()
  {
    return this.version;
  }

  public boolean isDeleted()
  {
    return this.isDeleted;
  }

  public boolean isDirty()
  {
    return this.isDirty;
  }

  public void setDeleted(boolean paramBoolean)
  {
    this.isDeleted = paramBoolean;
  }

  public void setDirty(boolean paramBoolean)
  {
    this.isDirty = paramBoolean;
  }

  public void setLength(long paramLong)
  {
    this.mLength = paramLong;
  }

  public void setVersion(int paramInt)
  {
    this.version = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.BaseMetaData
 * JD-Core Version:    0.5.4
 */