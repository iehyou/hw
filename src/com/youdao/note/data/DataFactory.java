package com.youdao.note.data;

import com.youdao.note.utils.IdUtils;

public class DataFactory
{
  public static NoteBook newNotebookMeta(String paramString)
  {
    NoteBook localNoteBook = new NoteBook();
    localNoteBook.setNoteBookId(IdUtils.genNotebookId());
    localNoteBook.setDirty(true);
    localNoteBook.setModifyTime(System.currentTimeMillis());
    localNoteBook.setTitle(paramString);
    localNoteBook.setVersion(-1);
    return localNoteBook;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.DataFactory
 * JD-Core Version:    0.5.4
 */