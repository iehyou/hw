package com.youdao.note.data;

public class ProgressData extends BaseData
{
  private String mId;
  private int mProgress;
  private long mTotalSize;

  public ProgressData(int paramInt)
  {
    this("", paramInt, 0L);
  }

  public ProgressData(String paramString, int paramInt, long paramLong)
  {
    this.mProgress = paramInt;
    this.mId = paramString;
    this.mTotalSize = paramLong;
  }

  public String getId()
  {
    monitorenter;
    try
    {
      String str = this.mId;
      monitorexit;
      return str;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public int getProgress()
  {
    monitorenter;
    try
    {
      int i = this.mProgress;
      monitorexit;
      return i;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public long getTotalSize()
  {
    monitorenter;
    try
    {
      long l = this.mTotalSize;
      monitorexit;
      return l;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void setProgress(int paramInt)
  {
    monitorenter;
    try
    {
      this.mProgress = paramInt;
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.ProgressData
 * JD-Core Version:    0.5.4
 */