package com.youdao.note.data;

public abstract interface IEntryPath
{
  public abstract String getEntryPath();

  public abstract int getVersion();
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.IEntryPath
 * JD-Core Version:    0.5.4
 */