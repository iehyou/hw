package com.youdao.note.data;

import android.database.Cursor;
import android.text.TextUtils;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.IdUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class NoteBook extends BaseMetaData
  implements EmptyInstance
{
  private static final long serialVersionUID = -2776996429757575868L;
  private long createTime = 0L;
  private long mLastSyncTime = -1L;
  private String mNoteBookId;
  private String mTitle = "";
  private long modifyTime = 0L;
  private int noteNumber = 0;

  public NoteBook()
  {
    setDirty(true);
    this.createTime = System.currentTimeMillis();
    this.mNoteBookId = IdUtils.genNoteId();
  }

  public static NoteBook fromCursor(Cursor paramCursor)
  {
    if (paramCursor.getCount() <= 0)
      return null;
    return fromCursorHelper(new CursorHelper(paramCursor));
  }

  public static NoteBook fromCursorHelper(CursorHelper paramCursorHelper)
  {
    NoteBook localNoteBook = new NoteBook();
    localNoteBook.setCreateTime(paramCursorHelper.getLong("create_time"));
    localNoteBook.setModifyTime(paramCursorHelper.getLong("modify_time"));
    localNoteBook.setVersion(paramCursorHelper.getInt("version"));
    localNoteBook.setLastSyncTime(paramCursorHelper.getLong("last_sync_time"));
    localNoteBook.setTitle(paramCursorHelper.getString("title"));
    localNoteBook.setDirty(paramCursorHelper.getBoolean("is_dirty"));
    localNoteBook.setDeleted(paramCursorHelper.getBoolean("is_deleted"));
    localNoteBook.setNoteBookId(paramCursorHelper.getString("_id"));
    localNoteBook.setNoteNumber(paramCursorHelper.getInt("note_number"));
    return localNoteBook;
  }

  public static NoteBook fromJsonObject(JSONObject paramJSONObject)
    throws JSONException
  {
    NoteBook localNoteBook = new NoteBook();
    localNoteBook.setVersion(paramJSONObject.getInt("v"));
    localNoteBook.mTitle = paramJSONObject.getString("tl");
    localNoteBook.setDeleted(toBool(paramJSONObject, "del"));
    localNoteBook.createTime = (1000L * paramJSONObject.getLong("ct"));
    localNoteBook.modifyTime = (1000L * paramJSONObject.getLong("mt"));
    localNoteBook.setNoteBookId(paramJSONObject.getString("p").substring(1));
    localNoteBook.setLength(paramJSONObject.getInt("sz"));
    localNoteBook.noteNumber = paramJSONObject.getInt("nn");
    localNoteBook.setDirty(false);
    return localNoteBook;
  }

  public long getCreateTime()
  {
    return this.createTime;
  }

  public String getEntryPath()
  {
    return "/" + this.mNoteBookId;
  }

  public long getLastSyncTime()
  {
    return this.mLastSyncTime;
  }

  public long getModifyTime()
  {
    return this.modifyTime;
  }

  public String getNoteBookId()
  {
    return this.mNoteBookId;
  }

  public int getNoteNumber()
  {
    return this.noteNumber;
  }

  public String getTitle()
  {
    return this.mTitle;
  }

  public boolean isRoot()
  {
    return TextUtils.isEmpty(this.mNoteBookId);
  }

  public void setCreateTime(long paramLong)
  {
    this.createTime = paramLong;
  }

  public void setLastSyncTime(long paramLong)
  {
    this.mLastSyncTime = paramLong;
  }

  public void setModifyTime(long paramLong)
  {
    this.modifyTime = paramLong;
  }

  public void setNoteBookId(String paramString)
  {
    this.mNoteBookId = paramString;
  }

  public void setNoteNumber(int paramInt)
  {
    this.noteNumber = paramInt;
  }

  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.NoteBook
 * JD-Core Version:    0.5.4
 */