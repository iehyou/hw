package com.youdao.note.data;

public class LocalErrorData extends BaseData
{
  private Exception mException = null;

  public LocalErrorData(Exception paramException)
  {
    this.mException = paramException;
  }

  public Exception getException()
  {
    return this.mException;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.LocalErrorData
 * JD-Core Version:    0.5.4
 */