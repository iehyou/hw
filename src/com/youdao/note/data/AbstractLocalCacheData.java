package com.youdao.note.data;

import com.youdao.note.datasource.localcache.AbstractLocalCache;
import com.youdao.note.utils.L;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public abstract class AbstractLocalCacheData extends BaseData
  implements ICacheable, Serializable
{
  private static final long serialVersionUID = 2719674629615963136L;
  protected byte[] datas;

  public boolean exist()
  {
    return getLocalCache().exist(getRelativePath());
  }

  public String getAbslutePath()
  {
    return getLocalCache().getAbsolutePath(getRelativePath());
  }

  public byte[] getContentBytes()
  {
    if (this.datas == null);
    try
    {
      this.datas = getLocalCache().getCacheItemAsBytes(getRelativePath());
      return this.datas;
    }
    catch (IOException localIOException)
    {
      L.e(this, "getContentBytes failed!", localIOException);
    }
  }

  public long getLength()
  {
    if (this.datas != null)
      return this.datas.length;
    return new File(getAbslutePath()).length();
  }

  protected abstract AbstractLocalCache getLocalCache();

  public boolean isDataEmpty()
  {
    return this.datas == null;
  }

  public boolean isDataLoaded()
  {
    return this.datas != null;
  }

  public void releaseData()
  {
    this.datas = null;
  }

  public void setContentBytes(byte[] paramArrayOfByte)
  {
    this.datas = paramArrayOfByte;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.AbstractLocalCacheData
 * JD-Core Version:    0.5.4
 */