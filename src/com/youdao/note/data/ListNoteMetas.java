package com.youdao.note.data;

import com.youdao.note.utils.EmptyInstance;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListNoteMetas extends BaseData
  implements Serializable
{
  private static final long serialVersionUID = 2068437119991346435L;
  private NoteMeta[] fileMetas = null;
  private int totalNumber = 0;

  public static ListNoteMetas fromJsonString(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    throws JSONException
  {
    JSONArray localJSONArray1 = new JSONArray(paramString);
    ListNoteMetas localListNoteMetas = new ListNoteMetas();
    if (paramBoolean1)
      localListNoteMetas.totalNumber = localJSONArray1.getInt(0);
    for (JSONArray localJSONArray2 = (JSONArray)localJSONArray1.get(1); ; localJSONArray2 = localJSONArray1)
    {
      localListNoteMetas.fileMetas = new NoteMeta[localJSONArray2.length()];
      for (int i = 0; ; ++i)
      {
        if (i >= localJSONArray2.length())
          break label102;
        localListNoteMetas.fileMetas[i] = NoteMeta.fromJsonObject((JSONObject)localJSONArray2.get(i), paramBoolean2);
      }
    }
    label102: return localListNoteMetas;
  }

  public NoteMeta[] getFileMetas()
  {
    if (this.fileMetas == null)
      return EmptyInstance.EMPTY_NOTEMETAS;
    return this.fileMetas;
  }

  public int getTotalNumber()
  {
    return this.totalNumber;
  }

  public void setFileMetas(NoteMeta[] paramArrayOfNoteMeta)
  {
    this.fileMetas = paramArrayOfNoteMeta;
  }

  public void setTotalNumber(int paramInt)
  {
    this.totalNumber = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.ListNoteMetas
 * JD-Core Version:    0.5.4
 */