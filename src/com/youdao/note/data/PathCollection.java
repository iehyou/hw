package com.youdao.note.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PathCollection extends BaseData
  implements Serializable
{
  private static final long serialVersionUID = 6735811092192543435L;
  private final List<NoteBook> mNoteBooks = new ArrayList();
  private final List<NoteMeta> mNoteMetas = new ArrayList();
  private NoteBook root;

  public static PathCollection fromJsonString(String paramString)
    throws JSONException
  {
    JSONArray localJSONArray = new JSONArray(paramString);
    PathCollection localPathCollection = new PathCollection();
    int i = 0;
    if (i < localJSONArray.length())
    {
      label19: JSONObject localJSONObject = localJSONArray.getJSONObject(i);
      if (toBool(localJSONObject, "dr"))
        localPathCollection.addNoteBook(NoteBook.fromJsonObject(localJSONObject));
      while (true)
      {
        ++i;
        break label19:
        localPathCollection.addNoteMeta(NoteMeta.fromJsonObject(localJSONObject));
      }
    }
    return localPathCollection;
  }

  public void addNoteBook(NoteBook paramNoteBook)
  {
    if (paramNoteBook.isRoot())
    {
      this.root = paramNoteBook;
      return;
    }
    this.mNoteBooks.add(paramNoteBook);
  }

  public void addNoteMeta(NoteMeta paramNoteMeta)
  {
    this.mNoteMetas.add(paramNoteMeta);
  }

  public NoteMeta getFirstNoteMeta()
  {
    if (this.mNoteMetas.size() > 0)
      return (NoteMeta)this.mNoteMetas.get(0);
    return null;
  }

  public List<NoteBook> getNoteBooks()
  {
    return this.mNoteBooks;
  }

  public List<NoteMeta> getNoteMetas()
  {
    return this.mNoteMetas;
  }

  public NoteBook getRoot()
  {
    return this.root;
  }

  public int size()
  {
    int i = this.mNoteBooks.size() + this.mNoteMetas.size();
    if (this.root == null);
    for (int j = 0; ; j = 1)
      return j + i;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.PathCollection
 * JD-Core Version:    0.5.4
 */