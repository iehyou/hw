package com.youdao.note.data;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;
import com.youdao.note.utils.Consts.HTML;
import com.youdao.note.utils.EmptyInstance;

public class Note extends AbstractLocalCacheData
  implements Consts.HTML, EmptyInstance, IEntryPath
{
  private static final long serialVersionUID = -3441305378360806369L;
  private String mHtmlBody = "";
  private NoteMeta mNoteMeta = null;

  public Note(NoteMeta paramNoteMeta, String paramString)
  {
    this.mNoteMeta = paramNoteMeta;
    setBody(paramString);
  }

  public Note(String paramString)
  {
    this(paramString, null);
  }

  public Note(String paramString1, String paramString2)
  {
    this.mNoteMeta = new NoteMeta(paramString1);
    setBody(paramString2);
  }

  public Note(boolean paramBoolean)
  {
    this.mNoteMeta = new NoteMeta(paramBoolean);
    this.mHtmlBody = "";
  }

  public String getAuthor()
  {
    return this.mNoteMeta.getAuthor();
  }

  public String getBody()
  {
    return this.mHtmlBody;
  }

  public byte[] getContentBytes()
  {
    return getBody().getBytes();
  }

  public String getEditableHtml()
  {
    return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"editor2.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>" + this.mHtmlBody + "</body></html>";
  }

  public String getEntryPath()
  {
    return this.mNoteMeta.getEntryPath();
  }

  public String getHtml()
  {
    return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"detail.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>" + this.mHtmlBody + "</body></html>";
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getNoteCache();
  }

  public long getModifyTime()
  {
    return this.mNoteMeta.getModifyTime();
  }

  public String getNoteBook()
  {
    return this.mNoteMeta.getNoteBook();
  }

  public String getNoteId()
  {
    return this.mNoteMeta.getNoteId();
  }

  public NoteMeta getNoteMeta()
  {
    return this.mNoteMeta;
  }

  public String getRelativePath()
  {
    return this.mNoteMeta.genRelativePath();
  }

  public String getSource()
  {
    return this.mNoteMeta.getSourceUrl();
  }

  public String getTitle()
  {
    return this.mNoteMeta.getTitle();
  }

  public int getVersion()
  {
    return this.mNoteMeta.getVersion();
  }

  public boolean isDirty()
  {
    return this.mNoteMeta.isDirty();
  }

  public void setBody(String paramString)
  {
    if (paramString == null)
    {
      this.mHtmlBody = "";
      return;
    }
    this.mHtmlBody = paramString;
  }

  public void setDirty(boolean paramBoolean)
  {
    this.mNoteMeta.setDirty(paramBoolean);
  }

  public void setNoteBookId(String paramString)
  {
    this.mNoteMeta.setNoteBook(paramString);
  }

  public void setNoteId(String paramString)
  {
    this.mNoteMeta.setNoteId(paramString);
  }

  public void setNoteMeta(NoteMeta paramNoteMeta)
  {
    this.mNoteMeta = paramNoteMeta;
  }

  public void setServerNoteBook(String paramString)
  {
    this.mNoteMeta.setServerNoteBook(paramString);
  }

  public void setTitle(String paramString)
  {
    this.mNoteMeta.setTitle(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.Note
 * JD-Core Version:    0.5.4
 */