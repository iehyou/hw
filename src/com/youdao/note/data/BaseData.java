package com.youdao.note.data;

import com.youdao.note.utils.Consts.DATA_NAME;
import com.youdao.note.utils.L;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseData
  implements Consts.DATA_NAME
{
  private boolean innerEquals(String paramString, Object paramObject1, Object paramObject2)
  {
    if ((paramObject1 != null) && (paramObject1.getClass().isArray()))
      if (Array.getLength(paramObject1) == Array.getLength(paramObject2));
    do
    {
      return false;
      for (int i = 0; ; ++i)
      {
        if (i >= Array.getLength(paramObject1))
          break label73;
        if (!objectEqual(Array.get(paramObject1, i), Array.get(paramObject2, i)));
      }
    }
    while (!objectEqual(paramObject1, paramObject2));
    label73: return true;
  }

  private boolean objectEqual(Object paramObject1, Object paramObject2)
  {
    if ((paramObject1 == null) && (paramObject2 != null))
    {
      L.d(this, paramObject1 + " " + paramObject2);
      return false;
    }
    return paramObject1.equals(paramObject2);
  }

  public static boolean toBool(JSONObject paramJSONObject, String paramString)
    throws JSONException
  {
    Object localObject = paramJSONObject.get(paramString);
    if (localObject == null);
    do
    {
      return false;
      if (localObject instanceof Boolean)
        return ((Boolean)localObject).booleanValue();
    }
    while (((Number)localObject).intValue() == 0);
    return true;
  }

  public boolean equals(Object paramObject)
  {
    L.d(this, "Enter equals.");
    if (this == paramObject);
    Method[] arrayOfMethod;
    int i;
    int j;
    do
    {
      return true;
      if ((this == null) || (paramObject == null))
        return false;
      arrayOfMethod = super.getClass().getMethods();
      L.d(this, "total method number is " + arrayOfMethod.length);
      i = arrayOfMethod.length;
      label61: j = 0;
    }
    while (j >= i);
    Method localMethod = arrayOfMethod[j];
    if (localMethod.getTypeParameters().length > 0);
    String str;
    do
    {
      ++j;
      break label61:
      str = localMethod.getName();
    }
    while ((!str.startsWith("get")) && (!str.startsWith("is")));
    try
    {
      Object localObject1 = localMethod.invoke(this, new Object[0]);
      Object localObject2 = localMethod.invoke(paramObject, new Object[0]);
      L.d(this, str + " invoke resule " + localObject1 + " : " + localObject2);
      boolean bool = innerEquals(localMethod.getName(), localObject1, localObject2);
      if (!bool);
      return false;
    }
    catch (Exception localException)
    {
      L.e(this, str, localException);
    }
    return false;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Method[] arrayOfMethod = super.getClass().getMethods();
    int i = arrayOfMethod.length;
    int j = 0;
    label22: Method localMethod;
    String str1;
    String str2;
    if (j < i)
    {
      localMethod = arrayOfMethod[j];
      str1 = localMethod.getName();
      boolean bool = str1.startsWith("get");
      str2 = null;
      if (bool)
        str2 = "get";
      if (str1.startsWith("is"))
        str2 = "is";
      if (str2 == null);
    }
    try
    {
      localStringBuilder.append("\t" + str1.substring(str2.length()) + "=" + localMethod.invoke(this, new Object[0]));
      ++j;
      break label22:
      localStringBuilder.append("\taddress=" + super.toString());
      return localStringBuilder.toString();
    }
    catch (Exception localException)
    {
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.BaseData
 * JD-Core Version:    0.5.4
 */