package com.youdao.note.data;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class TempFile extends AbstractLocalCacheData
{
  private static final long serialVersionUID = -2856461444644464955L;
  private String relativePath;

  public TempFile(String paramString)
  {
    this.relativePath = paramString;
  }

  public static TempFile newNoteSnapshot(String paramString)
  {
    return new TempFile(paramString + "-note-snapshot.jpg");
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getTempFileCache();
  }

  public String getRelativePath()
  {
    return this.relativePath;
  }

  public void setContentBytes(byte[] paramArrayOfByte)
  {
    this.datas = paramArrayOfByte;
  }

  public void setFileName(String paramString)
  {
    this.relativePath = paramString;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.TempFile
 * JD-Core Version:    0.5.4
 */