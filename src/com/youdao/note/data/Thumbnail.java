package com.youdao.note.data;

import com.youdao.note.YNoteApplication;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;

public class Thumbnail extends AbstractLocalCacheData
{
  private static final long serialVersionUID = -8566065805593351592L;
  private AbstractImageResourceMeta imageMeta;

  public Thumbnail(AbstractImageResourceMeta paramAbstractImageResourceMeta)
  {
    this.imageMeta = paramAbstractImageResourceMeta;
  }

  public AbstractImageResourceMeta getImageMeta()
  {
    return this.imageMeta;
  }

  public long getLength()
  {
    return this.imageMeta.getLength();
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getThumbnailCache();
  }

  public String getRelativePath()
  {
    return this.imageMeta.genRelativePath();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.Thumbnail
 * JD-Core Version:    0.5.4
 */