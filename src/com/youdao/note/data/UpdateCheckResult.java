package com.youdao.note.data;

import java.io.Serializable;

public class UpdateCheckResult extends BaseData
  implements Serializable
{
  private static final long serialVersionUID = -2001198779568002199L;
  private String mNewFeatures;
  private boolean mNewVersionFound;
  private String mUpdateUrl;

  public UpdateCheckResult()
  {
    this.mNewVersionFound = false;
  }

  public UpdateCheckResult(boolean paramBoolean, String paramString1, String paramString2)
  {
    this.mNewVersionFound = paramBoolean;
    this.mUpdateUrl = paramString1;
    this.mNewFeatures = paramString2;
  }

  public String getNewFeatures()
  {
    return this.mNewFeatures;
  }

  public String getUpdateUrl()
  {
    return this.mUpdateUrl;
  }

  public boolean isNewVersionFoun()
  {
    return this.mNewVersionFound;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.UpdateCheckResult
 * JD-Core Version:    0.5.4
 */