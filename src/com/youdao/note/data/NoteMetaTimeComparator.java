package com.youdao.note.data;

import java.util.Comparator;

class NoteMetaTimeComparator
  implements Comparator<NoteMeta>
{
  public int compare(NoteMeta paramNoteMeta1, NoteMeta paramNoteMeta2)
  {
    if (paramNoteMeta1.getModifyTime() == paramNoteMeta2.getModifyTime())
      return 0;
    if (paramNoteMeta1.getModifyTime() - paramNoteMeta2.getModifyTime() < 0L)
      return 1;
    return -1;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.NoteMetaTimeComparator
 * JD-Core Version:    0.5.4
 */