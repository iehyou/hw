package com.youdao.note.data;

import android.database.Cursor;
import android.text.TextUtils;
import com.youdao.note.utils.CursorHelper;
import com.youdao.note.utils.EmptyInstance;
import com.youdao.note.utils.IdUtils;
import com.youdao.note.utils.L;
import com.youdao.note.utils.YNoteUtils;
import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class NoteMeta extends BaseMetaData
  implements Serializable, EmptyInstance, IEntryPath
{
  public static final int NOTE_TYPE_ATTACH = 2;
  public static final int NOTE_TYPE_THUMBNAIL = 4;
  public static final String PROP_SNIPPET = "thmurl";
  public static final String PROP_SUMMARY_NAME = "dg";
  public static final String PROP_TP = "tp";
  public static final Comparator<NoteMeta> TIME_REVERSE_COMPARATOR = new NoteMetaTimeComparator();
  private static final long serialVersionUID = -3897572357774934849L;
  private String author = "";
  private long createTime = 0L;
  private Map<String, Object> extProp = new HashMap(1);
  private long mLastSyncTime = -1L;
  private String mNoteBook = null;
  private String mNoteId = null;
  private String mServerNoteBook = null;
  private String mTitle = "";
  private long modifyTime = 0L;
  private int noteNumber = 0;
  private String sourceUrl = "";
  private String[] tags = null;

  private NoteMeta()
  {
  }

  public NoteMeta(String paramString)
  {
    String[] arrayOfString = YNoteUtils.splitEntryPath(paramString);
    this.mNoteBook = arrayOfString[0];
    this.mNoteId = arrayOfString[1];
  }

  public NoteMeta(boolean paramBoolean)
  {
    setDirty(true);
    this.createTime = System.currentTimeMillis();
    this.mNoteId = IdUtils.genNoteId();
  }

  public static NoteMeta fromCursor(Cursor paramCursor)
  {
    if (paramCursor.getCount() <= 0)
      return null;
    return fromCursorHelper(new CursorHelper(paramCursor));
  }

  public static NoteMeta fromCursorHelper(CursorHelper paramCursorHelper)
  {
    NoteMeta localNoteMeta = new NoteMeta();
    localNoteMeta.createTime = paramCursorHelper.getLong("create_time");
    localNoteMeta.modifyTime = paramCursorHelper.getLong("modify_time");
    localNoteMeta.setVersion(paramCursorHelper.getInt("version"));
    localNoteMeta.mLastSyncTime = paramCursorHelper.getLong("last_sync_time");
    localNoteMeta.mTitle = paramCursorHelper.getString("title");
    localNoteMeta.setDirty(paramCursorHelper.getBoolean("is_dirty"));
    localNoteMeta.setDeleted(paramCursorHelper.getBoolean("is_deleted"));
    localNoteMeta.mNoteId = paramCursorHelper.getString("_id");
    localNoteMeta.mNoteBook = paramCursorHelper.getString("notebook");
    localNoteMeta.mServerNoteBook = paramCursorHelper.getString("server_notebook");
    localNoteMeta.author = paramCursorHelper.getString("author");
    localNoteMeta.sourceUrl = paramCursorHelper.getString("source");
    localNoteMeta.setLength(paramCursorHelper.getInt("length"));
    try
    {
      localNoteMeta.extProp = YNoteUtils.json2Map(new JSONObject(paramCursorHelper.getString("props")));
      return localNoteMeta;
    }
    catch (JSONException localJSONException)
    {
      localNoteMeta.extProp = new HashMap(3);
      L.w(localNoteMeta, "Convert data from database error, just create a empty props.");
    }
    return localNoteMeta;
  }

  public static NoteMeta fromJsonObject(JSONObject paramJSONObject)
    throws JSONException
  {
    return fromJsonObject(paramJSONObject, false);
  }

  public static NoteMeta fromJsonObject(JSONObject paramJSONObject, boolean paramBoolean)
    throws JSONException
  {
    NoteMeta localNoteMeta = new NoteMeta();
    localNoteMeta.setEntryPath(paramJSONObject.getString("p"));
    localNoteMeta.setVersion(paramJSONObject.getInt("v"));
    if (!paramBoolean)
    {
      localNoteMeta.mTitle = paramJSONObject.getString("tl");
      localNoteMeta.setDeleted(toBool(paramJSONObject, "del"));
      localNoteMeta.createTime = (1000L * paramJSONObject.getLong("ct"));
      localNoteMeta.modifyTime = (1000L * paramJSONObject.getLong("mt"));
      localNoteMeta.extProp = YNoteUtils.json2Map(paramJSONObject.getJSONObject("pp"));
      if ((0x4 & localNoteMeta.getNoteMetaType()) == 0)
        localNoteMeta.setSnippetUrl(null);
      String str = paramJSONObject.getString("tg");
      if (str != null)
        localNoteMeta.setTags(str.split(","));
      localNoteMeta.setLength(paramJSONObject.getInt("sz"));
      localNoteMeta.noteNumber = paramJSONObject.getInt("nn");
      localNoteMeta.author = paramJSONObject.getString("au");
      localNoteMeta.sourceUrl = paramJSONObject.getString("su");
    }
    return localNoteMeta;
  }

  public static NoteMeta fromJsonString(String paramString)
    throws JSONException
  {
    return fromJsonObject(new JSONObject(paramString));
  }

  public static NoteMeta fromJsonString(String paramString, boolean paramBoolean)
    throws JSONException
  {
    return fromJsonObject(new JSONObject(paramString), paramBoolean);
  }

  private String safeString(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      paramString = "";
    return paramString;
  }

  public String genRelativePath()
  {
    return getNoteBook() + File.separatorChar + getNoteId();
  }

  public String getAuthor()
  {
    return this.author;
  }

  public long getCreateTime()
  {
    return this.createTime;
  }

  public String getEntryPath()
  {
    return "/" + this.mServerNoteBook + "/" + this.mNoteId;
  }

  public long getLastSyncTime()
  {
    return this.mLastSyncTime;
  }

  public long getModifyTime()
  {
    return this.modifyTime;
  }

  public String getNoteBook()
  {
    return this.mNoteBook;
  }

  public String getNoteId()
  {
    return this.mNoteId;
  }

  public int getNoteMetaType()
  {
    if (this.extProp.get("tp") != null)
      return Integer.parseInt((String)this.extProp.get("tp"));
    return 0;
  }

  public int getNoteNumber()
  {
    return this.noteNumber;
  }

  public String getPropsAsStr()
  {
    if (this.extProp == null)
      return "{}";
    return new JSONObject(this.extProp).toString();
  }

  public String getServerNoteBook()
  {
    return this.mServerNoteBook;
  }

  public String getSnippetUrl()
  {
    return safeString((String)this.extProp.get("thmurl"));
  }

  public String getSourceUrl()
  {
    return this.sourceUrl;
  }

  public String getSummary()
  {
    return safeString((String)this.extProp.get("dg"));
  }

  public String[] getTags()
  {
    return this.tags;
  }

  public String getTitle()
  {
    if (TextUtils.isEmpty(this.mTitle))
      return "无主题";
    return this.mTitle;
  }

  public boolean hasAttachment()
  {
    return (0x2 & getNoteMetaType()) > 0;
  }

  public boolean hasSnippet()
  {
    return !TextUtils.isEmpty(getSnippetUrl());
  }

  public boolean isMoved()
  {
    return !this.mNoteBook.equals(this.mServerNoteBook);
  }

  public boolean needSync()
  {
    return (isDirty()) || (isMoved());
  }

  public void setAuthor(String paramString)
  {
    this.author = paramString;
  }

  public void setCreateTime(long paramLong)
  {
    this.createTime = paramLong;
  }

  public void setEntryPath(String paramString)
  {
    if (paramString == null)
      return;
    String[] arrayOfString = YNoteUtils.splitEntryPath(paramString);
    this.mServerNoteBook = arrayOfString[0];
    this.mNoteBook = this.mServerNoteBook;
    this.mNoteId = arrayOfString[1];
  }

  public void setHasAttachment()
  {
    this.extProp.put("tp", Integer.toString(0x2 | getNoteMetaType()));
  }

  public void setModifyTime(long paramLong)
  {
    this.modifyTime = paramLong;
  }

  public void setNoteBook(String paramString)
  {
    this.mNoteBook = paramString;
    if (this.mServerNoteBook != null)
      return;
    this.mServerNoteBook = paramString;
  }

  public void setNoteId(String paramString)
  {
    this.mNoteId = paramString;
  }

  public void setNoteNumber(int paramInt)
  {
    this.noteNumber = paramInt;
  }

  public void setServerNoteBook(String paramString)
  {
    this.mServerNoteBook = paramString;
  }

  public void setSnippetFID(String paramString)
  {
    this.extProp.put("thmurl", "fid=" + paramString + " v=0");
  }

  public void setSnippetUrl(String paramString)
  {
    if (paramString == null)
    {
      this.extProp.put("thmurl", "");
      return;
    }
    this.extProp.put("thmurl", paramString);
  }

  public void setSourceUrl(String paramString)
  {
    this.sourceUrl = paramString;
  }

  public void setSummary(String paramString)
  {
    this.extProp.put("dg", paramString);
  }

  public void setTags(String[] paramArrayOfString)
  {
    this.tags = paramArrayOfString;
  }

  public void setTitle(String paramString)
  {
    this.mTitle = paramString;
  }

  public void setmLastSyncTime(long paramLong)
  {
    this.mLastSyncTime = paramLong;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.NoteMeta
 * JD-Core Version:    0.5.4
 */