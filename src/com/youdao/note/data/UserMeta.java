package com.youdao.note.data;

import android.database.Cursor;
import com.youdao.note.utils.CursorHelper;
import org.json.JSONException;
import org.json.JSONObject;

public class UserMeta extends BaseData
{
  private long lastLoginTime = 0L;
  private String mDefaultNotebook = null;
  private long mLastPushTime = 0L;
  private long mLastSynceTime = 0L;
  private long mQuotaSpace = 0L;
  private long mUsedSpace = 0L;

  public static UserMeta fromCursor(Cursor paramCursor)
  {
    CursorHelper localCursorHelper = new CursorHelper(paramCursor);
    UserMeta localUserMeta = new UserMeta();
    localUserMeta.mUsedSpace = localCursorHelper.getInt("used_space");
    localUserMeta.mQuotaSpace = localCursorHelper.getLong("quota_space");
    localUserMeta.mDefaultNotebook = localCursorHelper.getString("default_notebook");
    localUserMeta.mLastPushTime = localCursorHelper.getLong("last_push_time");
    localUserMeta.mLastSynceTime = localCursorHelper.getLong("last_sync_time");
    return localUserMeta;
  }

  public static UserMeta fromJsonString(String paramString)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject(paramString);
    UserMeta localUserMeta = new UserMeta();
    localUserMeta.mDefaultNotebook = localJSONObject.getString("df");
    localUserMeta.lastLoginTime = localJSONObject.getLong("ll");
    localUserMeta.mLastPushTime = localJSONObject.getLong("lp");
    localUserMeta.mUsedSpace = localJSONObject.getLong("u");
    localUserMeta.mQuotaSpace = localJSONObject.getLong("q");
    return localUserMeta;
  }

  public String getDefaultNoteBook()
  {
    return this.mDefaultNotebook;
  }

  public long getLastLoginTime()
  {
    return this.lastLoginTime;
  }

  public long getLastPushTime()
  {
    return this.mLastPushTime;
  }

  public long getLastSynceTime()
  {
    return this.mLastSynceTime;
  }

  public long getQuotaSpace()
  {
    return this.mQuotaSpace;
  }

  public long getUsedSpace()
  {
    return this.mUsedSpace;
  }

  public void setDefaultNoteBook(String paramString)
  {
    this.mDefaultNotebook = paramString;
  }

  public void setLastLoginTime(long paramLong)
  {
    this.lastLoginTime = paramLong;
  }

  public void setLastPushTime(long paramLong)
  {
    this.mLastPushTime = paramLong;
  }

  public void setLastSynceTime(long paramLong)
  {
    this.mLastSynceTime = paramLong;
  }

  public void setQuotaSpace(int paramInt)
  {
    this.mQuotaSpace = paramInt;
  }

  public void setUsedSpace(int paramInt)
  {
    this.mUsedSpace = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.UserMeta
 * JD-Core Version:    0.5.4
 */