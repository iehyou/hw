package com.youdao.note.data;

import com.youdao.note.YNoteApplication;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.datasource.localcache.AbstractLocalCache;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Snippet extends AbstractLocalCacheData
{
  private static final Pattern FID_PATTERN = Pattern.compile("fid=([\\w\\d]+)");
  public static final String SNIPPET_FILENAME = "snippet.png";
  private static final Pattern VERSION_PATTERN = Pattern.compile("v=(\\d+)");
  private static final long serialVersionUID = 9137388046039579966L;
  private NoteMeta noteMeta;

  public Snippet(NoteMeta paramNoteMeta)
  {
    this.noteMeta = paramNoteMeta;
  }

  protected AbstractLocalCache getLocalCache()
  {
    return YNoteApplication.getInstance().getDataSource().getSnippetCache();
  }

  public NoteMeta getNoteMeta()
  {
    return this.noteMeta;
  }

  public String getRelativePath()
  {
    return parseFID() + "-" + "snippet.png";
  }

  public String parseFID()
  {
    Matcher localMatcher = FID_PATTERN.matcher(this.noteMeta.getSnippetUrl());
    if (localMatcher.find())
      return localMatcher.group(1);
    return null;
  }

  public int parseVERSION()
  {
    Matcher localMatcher = VERSION_PATTERN.matcher(this.noteMeta.getSnippetUrl());
    if (localMatcher.find())
      return Integer.parseInt(localMatcher.group(1));
    return -1;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.Snippet
 * JD-Core Version:    0.5.4
 */