package com.youdao.note.data.adapter;

import android.database.Cursor;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.youdao.note.data.NoteMeta;
import com.youdao.note.ui.AsyncImageView;
import com.youdao.note.utils.StringUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SectionAdapter extends BaseAdapter
{
  private SimpleDateFormat labelFormator = new SimpleDateFormat("yyyy-MM");
  private LayoutInflater mInflater = null;
  private String mTitleFormator = null;
  private int mTotalCount = 0;
  private String mUnSyncedTitle = null;
  private Map<String, Integer> sectionCount = new HashMap();
  private SectionItem[] sectionItems = new SectionItem[0];

  public SectionAdapter(LayoutInflater paramLayoutInflater, Cursor paramCursor, String paramString1, String paramString2)
  {
    this.mInflater = paramLayoutInflater;
    this.mUnSyncedTitle = paramString2;
    this.mTitleFormator = paramString1;
    resetSections(paramCursor);
  }

  public SectionAdapter(LayoutInflater paramLayoutInflater, String paramString1, String paramString2)
  {
    this.mInflater = paramLayoutInflater;
    this.mUnSyncedTitle = paramString2;
    this.mTitleFormator = paramString1;
  }

  public SectionAdapter(LayoutInflater paramLayoutInflater, NoteMeta[] paramArrayOfNoteMeta, String paramString1, String paramString2)
  {
    this.mInflater = paramLayoutInflater;
    this.mUnSyncedTitle = paramString2;
    this.mTitleFormator = paramString1;
    resetSections(paramArrayOfNoteMeta);
  }

  private int addNoteMeta(int paramInt, Date paramDate, List<SectionItem> paramList, NoteMeta paramNoteMeta)
  {
    paramDate.setTime(paramNoteMeta.getModifyTime());
    String str = this.labelFormator.format(paramDate);
    if ((paramInt == -1) || (paramDate.getMonth() != paramInt))
    {
      paramList.add(new SectionItem(str));
      paramInt = paramDate.getMonth();
    }
    paramList.add(new SectionItem(paramNoteMeta));
    incSectionCount(str);
    return paramInt;
  }

  private void incSectionCount(String paramString)
  {
    Integer localInteger = (Integer)this.sectionCount.get(paramString);
    Map localMap = this.sectionCount;
    if (localInteger == null);
    for (int i = 1; ; i = 1 + localInteger.intValue())
    {
      localMap.put(paramString, Integer.valueOf(i));
      return;
    }
  }

  public boolean areAllItemsEnabled()
  {
    return true;
  }

  public int getCount()
  {
    return this.sectionItems.length;
  }

  public Object getItem(int paramInt)
  {
    return this.sectionItems[paramInt];
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    return this.sectionItems[paramInt].getType();
  }

  public int getSectionCount(String paramString)
  {
    Integer localInteger = (Integer)this.sectionCount.get(paramString);
    if (localInteger == null)
      return 0;
    return localInteger.intValue();
  }

  public String getUnsyncedTitle()
  {
    return this.mUnSyncedTitle;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    SectionItem localSectionItem1 = (SectionItem)getItem(paramInt);
    View localView = paramView;
    SectionItem localSectionItem2 = null;
    if (localView != null)
      localSectionItem2 = (SectionItem)localView.getTag();
    if (getItemViewType(paramInt) == 2)
      if ((localView == null) || ((localSectionItem2 != null) && (localSectionItem2.getType() != 2)))
      {
        localView = this.mInflater.inflate(2130903073, null);
        TextView localTextView = (TextView)localView.findViewById(2131165274);
        String str = this.mTitleFormator;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(this.mTotalCount);
        localTextView.setText(String.format(str, arrayOfObject));
      }
    while (true)
    {
      label118: localView.setTag(localSectionItem1);
      return localView;
      if (getItemViewType(paramInt) != 0)
        break;
      if ((localView == null) || ((localSectionItem2 != null) && (localSectionItem2.getType() != 0)))
        localView = this.mInflater.inflate(2130903047, null);
      ((TextView)localView.findViewById(2131165207)).setText(localSectionItem1.getLabel());
      ((TextView)localView.findViewById(2131165208)).setText(Integer.toString(((Integer)this.sectionCount.get(localSectionItem1.getLabel())).intValue()));
    }
    NoteMeta localNoteMeta1 = localSectionItem1.getNoteMeta();
    NoteMeta localNoteMeta2 = null;
    if (localSectionItem2 != null)
      localNoteMeta2 = localSectionItem2.getNoteMeta();
    if (localNoteMeta2 == null)
      localView = this.mInflater.inflate(2130903096, null);
    ((TextView)localView.findViewById(2131165321)).setText(localNoteMeta1.getTitle());
    AsyncImageView localAsyncImageView = (AsyncImageView)localView.findViewById(2131165309);
    if (localNoteMeta1.hasSnippet())
    {
      localAsyncImageView.loadSnippet(localNoteMeta1);
      label307: if (!localNoteMeta1.hasAttachment())
        break label394;
      localView.findViewById(2131165336).setVisibility(0);
    }
    while (true)
    {
      ((TextView)localView.findViewById(2131165337)).setText(Html.fromHtml("<font color='grey'>" + StringUtils.getPrettyTime2(localNoteMeta1.getModifyTime()) + "</font>  " + localNoteMeta1.getSummary()));
      break label118:
      localAsyncImageView.setVisibility(8);
      break label307:
      label394: localView.findViewById(2131165336).setVisibility(8);
    }
  }

  public int getViewTypeCount()
  {
    return 3;
  }

  public boolean hasStableIds()
  {
    return true;
  }

  public boolean isEmpty()
  {
    return false;
  }

  public boolean isEnabled(int paramInt)
  {
    return true;
  }

  public void resetSections(Cursor paramCursor)
  {
    int i = -1;
    this.mTotalCount = paramCursor.getCount();
    Date localDate = new Date();
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    localArrayList1.add(new SectionItem(this.mTitleFormator, this.mTotalCount));
    localArrayList1.add(new SectionItem(this.mUnSyncedTitle));
    while (paramCursor.moveToNext())
    {
      NoteMeta localNoteMeta = NoteMeta.fromCursor(paramCursor);
      if ((this.mUnSyncedTitle != null) && (localNoteMeta.needSync()))
      {
        localArrayList1.add(new SectionItem(localNoteMeta));
        incSectionCount(this.mUnSyncedTitle);
      }
      i = addNoteMeta(i, localDate, localArrayList2, localNoteMeta);
    }
    if (localArrayList1.size() == 2)
      localArrayList1.remove(1);
    localArrayList1.addAll(localArrayList2);
    this.sectionItems = ((SectionItem[])localArrayList1.toArray(new SectionItem[0]));
  }

  public void resetSections(NoteMeta[] paramArrayOfNoteMeta)
  {
    int i = -1;
    this.mTotalCount = paramArrayOfNoteMeta.length;
    Date localDate = new Date();
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new SectionItem(this.mTitleFormator, this.mTotalCount));
    int j = paramArrayOfNoteMeta.length;
    for (int k = 0; k < j; ++k)
      i = addNoteMeta(i, localDate, localArrayList, paramArrayOfNoteMeta[k]);
    this.sectionItems = ((SectionItem[])localArrayList.toArray(new SectionItem[0]));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.adapter.SectionAdapter
 * JD-Core Version:    0.5.4
 */