package com.youdao.note.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.youdao.note.data.NoteBook;
import java.util.List;

public class NoteBookListAdapter extends BaseListAdapter<NoteBook>
{
  public NoteBookListAdapter(Context paramContext, List<NoteBook> paramList)
  {
    super(paramContext, 2130903089, paramList);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (localView == null)
      localView = this.mInflater.inflate(2130903089, null);
    NoteBook localNoteBook = (NoteBook)getItem(paramInt);
    ((TextView)localView.findViewById(2131165318)).setText(localNoteBook.getTitle());
    ((TextView)localView.findViewById(2131165319)).setText(Integer.toString(localNoteBook.getNoteNumber()));
    return localView;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.adapter.NoteBookListAdapter
 * JD-Core Version:    0.5.4
 */