package com.youdao.note.data.adapter;

public abstract interface ItemType
{
  public static final int ITEM = 1;
  public static final int LABEL = 0;
  public static final int TITLE = 2;
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.adapter.ItemType
 * JD-Core Version:    0.5.4
 */