package com.youdao.note.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import java.util.List;

public abstract class BaseListAdapter<T> extends BaseAdapter
{
  protected Context mContext;
  protected List<T> mDatas;
  protected LayoutInflater mInflater;
  protected int mResourceId = 0;

  public BaseListAdapter(Context paramContext, int paramInt, List<T> paramList)
  {
    this.mContext = paramContext;
    this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.mResourceId = paramInt;
    this.mDatas = paramList;
  }

  public BaseListAdapter(Context paramContext, List<T> paramList)
  {
    this(paramContext, 0, paramList);
  }

  public View createFooterView(int paramInt, ListView paramListView)
  {
    View localView = this.mInflater.inflate(paramInt, paramListView, false);
    paramListView.addFooterView(localView);
    return localView;
  }

  public View createHeaderView(int paramInt, ListView paramListView)
  {
    View localView = this.mInflater.inflate(paramInt, paramListView, false);
    paramListView.addHeaderView(localView);
    return localView;
  }

  protected View createView(int paramInt, ViewGroup paramViewGroup)
  {
    return null;
  }

  protected View createViewFromResource(int paramInt1, ViewGroup paramViewGroup, int paramInt2)
  {
    return this.mInflater.inflate(paramInt2, paramViewGroup, false);
  }

  public int getCount()
  {
    return this.mDatas.size();
  }

  public T getItem(int paramInt)
  {
    if ((this.mDatas != null) && (paramInt >= 0) && (paramInt < this.mDatas.size()))
      return this.mDatas.get(paramInt);
    return null;
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (this.mResourceId == 0)
      return createView(paramInt, paramViewGroup);
    return createViewFromResource(paramInt, paramViewGroup, this.mResourceId);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.adapter.BaseListAdapter
 * JD-Core Version:    0.5.4
 */