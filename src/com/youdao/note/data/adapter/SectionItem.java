package com.youdao.note.data.adapter;

import com.youdao.note.data.NoteMeta;

public class SectionItem
{
  private int mItemType = 1;
  private String mLabel = "";
  private NoteMeta mNoteMeta = null;

  public SectionItem(NoteMeta paramNoteMeta)
  {
    this.mItemType = 1;
    this.mNoteMeta = paramNoteMeta;
  }

  public SectionItem(String paramString)
  {
    this.mItemType = 0;
    this.mLabel = paramString;
  }

  public SectionItem(String paramString, int paramInt)
  {
    this.mItemType = 2;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    this.mLabel = String.format(paramString, arrayOfObject);
  }

  public String getLabel()
  {
    return this.mLabel;
  }

  public NoteMeta getNoteMeta()
  {
    return this.mNoteMeta;
  }

  public int getType()
  {
    return this.mItemType;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.adapter.SectionItem
 * JD-Core Version:    0.5.4
 */