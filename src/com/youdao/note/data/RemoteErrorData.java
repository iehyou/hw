package com.youdao.note.data;

public class RemoteErrorData extends BaseData
{
  public static final int ERROR_NEWORK = 1;
  private int mErrorType = 0;
  private Exception mException = null;

  public RemoteErrorData(int paramInt)
  {
    this.mErrorType = paramInt;
  }

  public RemoteErrorData(Exception paramException)
  {
    this.mException = paramException;
  }

  public int getErrorType()
  {
    return this.mErrorType;
  }

  public Exception getException()
  {
    return this.mException;
  }

  public void setErrorType(int paramInt)
  {
    this.mErrorType = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.RemoteErrorData
 * JD-Core Version:    0.5.4
 */