package com.youdao.note.data;

public class LoginResult extends BaseData
{
  public static final int CODE_PASSWORD_ERROR = 460;
  public static final int CODE_SUCCEED = 200;
  public static final int CODE_TOO_MANY_ERROR = 412;
  public static final int CODE_UNKNOW_REASON = -1;
  public static final int CODE_USER_NOT_EXIST = 420;
  private String mPassWord;
  private String mPersistCookie;
  private int mResultCode = 200;
  private String mSessionCookie;
  private String mUserName;

  public LoginResult(int paramInt)
  {
    setResultCode(paramInt);
  }

  public LoginResult(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.mPersistCookie = paramString3;
    this.mSessionCookie = paramString4;
    this.mUserName = paramString1;
    this.mPassWord = paramString2;
  }

  public String getPassWord()
  {
    return this.mPassWord;
  }

  public String getPersistCookie()
  {
    return this.mPersistCookie;
  }

  public int getResultCode()
  {
    return this.mResultCode;
  }

  public String getSessionCookie()
  {
    return this.mSessionCookie;
  }

  public String getUserName()
  {
    return this.mUserName;
  }

  public void setResultCode(int paramInt)
  {
    this.mResultCode = paramInt;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.data.LoginResult
 * JD-Core Version:    0.5.4
 */