package com.youdao.note.exceptions;

import java.io.IOException;

public class NetworkNotAvaliableException extends IOException
{
  private static final long serialVersionUID = 1L;
  private String mRequestUrl = null;

  public NetworkNotAvaliableException(String paramString1, String paramString2)
  {
    super(paramString1);
    this.mRequestUrl = paramString2;
  }

  public String toString()
  {
    return getMessage() + ", request url is " + this.mRequestUrl;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.NetworkNotAvaliableException
 * JD-Core Version:    0.5.4
 */