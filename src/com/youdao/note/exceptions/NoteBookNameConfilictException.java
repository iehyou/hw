package com.youdao.note.exceptions;

public class NoteBookNameConfilictException extends Exception
{
  private static final long serialVersionUID = 9115423469054552900L;

  public NoteBookNameConfilictException(String paramString)
  {
    super(paramString);
  }

  public NoteBookNameConfilictException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.NoteBookNameConfilictException
 * JD-Core Version:    0.5.4
 */