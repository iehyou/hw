package com.youdao.note.exceptions;

import com.youdao.note.utils.L;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerException extends Exception
{
  public static final int ACCESS_DENIED = 207;
  public static final int MAIL_BAN = 232;
  public static final int MAIL_BIG = 233;
  public static final int MAIL_RECV_LIMIT = 230;
  public static final int NOTE_ALREADY_DELETED = 304;
  public static final int PARENT_NOT_EXIST = 225;
  public static final int RESOURCE_EXIST = 231;
  public static final int RESOURCE_NOT_EXIST = 209;
  public static final int SERVICE_INVALID = 208;
  public static final int UNKNOWN_EXCEPTION = 205;
  public static final int USER_SPACE_FULL = 210;
  private static final long serialVersionUID = 3887281607964854777L;
  private int mErrorCode = 500;
  private String mErrorInfo = null;
  private int mResponseCode = 200;

  public ServerException(int paramInt, String paramString)
  {
    this.mResponseCode = paramInt;
    this.mErrorInfo = paramString;
    try
    {
      this.mErrorCode = new JSONObject(paramString).getInt("error");
      return;
    }
    catch (JSONException localJSONException)
    {
      L.e(this, "Failed to get error code", localJSONException);
    }
  }

  public int getErrorCode()
  {
    return this.mErrorCode;
  }

  public int getResponseCode()
  {
    return this.mResponseCode;
  }

  public String toString()
  {
    return "Response code is " + this.mResponseCode + "\n" + this.mErrorInfo + "\n" + super.toString();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.ServerException
 * JD-Core Version:    0.5.4
 */