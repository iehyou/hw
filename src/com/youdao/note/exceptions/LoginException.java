package com.youdao.note.exceptions;

public class LoginException extends Exception
{
  public static final int CODE_PASSWORD_ERROR = 460;
  public static final int CODE_SUCCEED = 200;
  public static final int CODE_USER_NOT_EXIST = 420;
  private static final long serialVersionUID = -34028876207454223L;
  private int mLoginResultCode = 0;

  public LoginException(int paramInt)
  {
    this.mLoginResultCode = paramInt;
  }

  public int getLoginResultCode()
  {
    return this.mLoginResultCode;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.LoginException
 * JD-Core Version:    0.5.4
 */