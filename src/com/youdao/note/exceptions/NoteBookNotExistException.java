package com.youdao.note.exceptions;

public class NoteBookNotExistException extends Exception
{
  private static final long serialVersionUID = 1046219252090859816L;

  public NoteBookNotExistException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.NoteBookNotExistException
 * JD-Core Version:    0.5.4
 */