package com.youdao.note.exceptions;

public class ResourceMissingException extends Exception
{
  private static final long serialVersionUID = -966273931426622929L;
  private String mNoteTitle = null;

  public ResourceMissingException(String paramString, Exception paramException)
  {
    super(paramException);
    this.mNoteTitle = paramString;
  }

  public String getNoteTitle()
  {
    return this.mNoteTitle;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.ResourceMissingException
 * JD-Core Version:    0.5.4
 */