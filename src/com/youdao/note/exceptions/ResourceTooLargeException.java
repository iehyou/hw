package com.youdao.note.exceptions;

public class ResourceTooLargeException extends Exception
{
  private static final long serialVersionUID = -5493575124702236849L;
  private String fileName;
  private long fileSize;

  public ResourceTooLargeException(String paramString, long paramLong)
  {
    this.fileName = paramString;
    this.fileSize = paramLong;
  }

  public String getFileName()
  {
    return this.fileName;
  }

  public long getFileSize()
  {
    return this.fileSize;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.ResourceTooLargeException
 * JD-Core Version:    0.5.4
 */