package com.youdao.note.exceptions;

import com.youdao.note.utils.L;

public class DownloadResourceException extends Exception
{
  private static final long serialVersionUID = 6305347315179756958L;
  private String mResourceId;

  public DownloadResourceException(String paramString)
  {
    this.mResourceId = paramString;
  }

  public DownloadResourceException(String paramString, Throwable paramThrowable)
  {
    super(paramThrowable);
    L.d(this, paramThrowable.getClass().toString());
    this.mResourceId = paramString;
  }

  public String getResourceId()
  {
    return this.mResourceId;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.DownloadResourceException
 * JD-Core Version:    0.5.4
 */