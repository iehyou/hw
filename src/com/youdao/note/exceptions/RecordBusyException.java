package com.youdao.note.exceptions;

import java.io.IOException;

public class RecordBusyException extends IOException
{
  private static final long serialVersionUID = 3461793064516472309L;
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.exceptions.RecordBusyException
 * JD-Core Version:    0.5.4
 */