package com.youdao.note.utils;

import com.youdao.note.data.BaseData;
import com.youdao.note.data.NoteMeta;

public abstract interface EmptyInstance
{
  public static final byte[] EMPTY_BYTES = new byte[0];
  public static final BaseData EMPTY_DATA;
  public static final NoteMeta[] EMPTY_NOTEMETAS;
  public static final Object EMPTY_OBJECT;
  public static final String EMPTY_STRING = "";
  public static final String[] EMPTY_STRINGS = new String[0];
  public static final String[] EMPTY_STRINGS2 = { "", "" };
  public static final String EMPTY_TITLE = "无主题";
  public static final Void[] EMPTY_VOIDS = new Void[0];

  static
  {
    EMPTY_NOTEMETAS = new NoteMeta[0];
    EMPTY_DATA = new BaseData();
    EMPTY_OBJECT = new Object();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.EmptyInstance
 * JD-Core Version:    0.5.4
 */