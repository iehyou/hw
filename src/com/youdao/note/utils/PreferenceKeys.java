package com.youdao.note.utils;

import android.content.res.Resources;
import com.youdao.note.YNoteApplication;

public abstract interface PreferenceKeys
{
  public static final String DOODLE_PAINT_WIDTH = "doodle_paint_width";
  public static final String EVER_LOGIN = "ever_login";
  public static final String HANDWRITE_PAINT_WIDTH = "handwrite_paint_width";
  public static final String IGNORE_VERSION = "ignore_version";
  public static final String LAST_CHECK_UPDATE = "last_chcekupdate";
  public static final String LAST_LOGIN_USERNAME = "last_username";
  public static final String LAST_USE_VERSION = "last_use_version";
  public static final String LICENSE_ACCEPTED = "license_accepted";
  public static final String PACKAGE_DOWNLOAD_URL = "download_url";
  public static final String PASSWORD = "password";
  public static final String PERSIST_COOKIE = "cookie";
  public static final String PIN_LOCK = "pinlock";
  public static final String SERVER_PACKAGE_VERSION = "server_version";
  public static final String SESSION_COOKIE = "session_cookie";
  public static final String SHOW_HANDWRITE_GUIDE = "show_handwrite_guide";
  public static final String SHOW_HANDWRITE_MODE_TIPS = "show_handwrite_mode_tips";
  public static final String SHOW_MAIL_GUIDE = "show_mail_guide";
  public static final String UPDATE_IGNORED = "update_ignored";
  public static final String USERNAME = YNoteApplication.getInstance().getResources().getString(2131361799);

  public static abstract interface STAT
  {
    public static final String ADD_ART_TIMES = "AddArtTimes";
    public static final String ADD_CAMERA_TIMES = "AddCameraTimes";
    public static final String ADD_NOTE_TIMES = "AddNoteTimes";
    public static final String ADD_PAINT_TIMES = "AddPaintTimes";
    public static final String ADD_PHOTO_TIMES = "AddPhotoTimes";
    public static final String ADD_RECORD_TIMES = "AddRecordTimes";
    public static final String APP_ADD_TIMES = "AppAddTimes";
    public static final String ATTACH_NUM = "AttachNum";
    public static final String AUTO_SYNC_STATUS = "AutoSyncStatus";
    public static final String CAMERA_NOTE_TIMES = "CameraNoteTimes";
    public static final String CONTENT_COPY_TIMES = "ContentCopyTimes";
    public static final String DPI = "dpi";
    public static final String EDIT_NOTE_TIMES = "EditNoteTimes";
    public static final String ENTRY_STATUS_BAR = "statusBar";
    public static final String ERROR = "error";
    public static final String EVER_REPORT = "ever_report";
    public static final String HANDWRITE_MODE = "machine";
    public static final String HANDWRITE_MODE_BAD = "BadNWrite";
    public static final String HANDWRITE_MODE_FINE = "FineNWrite";
    public static final String ICON_ALL_TIMES = "IconAllTimes";
    public static final String ICON_NEW_TIMES = "IconNewTimes";
    public static final String ICON_NOTEBOOK_TIMES = "IconNotebookTimes";
    public static final String ICON_SYNC_TIMES = "IconSyncTimes";
    public static final String IMAGE_RECTIFICATION_TIMES = "WbCorrectTimes";
    public static final String IMEI = "imei";
    public static final String KEYFROM = "keyfrom";
    public static final String LAST_REPORT_TIME = "last_report_time";
    public static final String LOGIN_STATUS = "LoginStatus";
    public static final String MAIL_SEND_TIMES = "SendMailTimes";
    public static final String MAIL_SHARE_TIMES = "EditMailTimes";
    public static final String MID = "mid";
    public static final String MODEL = "model";
    public static final String NOTE_DETAIL_TIMES = "NoteDetailTimes";
    public static final String PASS_CODE_STATUS = "PassCodeStatus";
    public static final String PHONE_VERSION = "phoneVersion";
    public static final String RECORD_FROM_BAR = "RecordFromBar";
    public static final String RECORD_NOTE_TIMES = "RecordNoteTimes";
    public static final String RESOLUTION = "resolution";
    public static final String SEARCH_NOTE_TIMES = "SearchNoteTimes";
    public static final String SIZE = "size";
    public static final String STAT_DURATION = "StatDuration";
    public static final String TEXT_NOTE_TIMES = "TextNoteTimes";
    public static final String VENDOR = "vendor";
    public static final String VIEW_ATTACH_TIMES = "ViewAttachTimes";
    public static final String VIEW_NOTE_TIMES = "ViewNoteTimes";
    public static final String VIEW_PIC_TIMES = "ViewPicTimes";
    public static final String WB_CORRECT_TIMES = "WbCorrectTimes";
    public static final String WIDGET_ADD_TIMES = "WidgetAddTimes";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.PreferenceKeys
 * JD-Core Version:    0.5.4
 */