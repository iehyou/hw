package com.youdao.note.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.youdao.note.YNoteApplication;

public class UIUtilities
{
  public static void clickButton(Button paramButton)
  {
    paramButton.post(new Runnable(paramButton)
    {
      public void run()
      {
        this.val$button.setPressed(true);
      }
    });
    paramButton.postDelayed(new Runnable(paramButton)
    {
      public void run()
      {
        this.val$button.setPressed(false);
        this.val$button.performClick();
      }
    }
    , 300L);
  }

  public static void makeViewGone(View paramView)
  {
    if (paramView.getVisibility() == 8)
      return;
    Animation localAnimation = AnimationUtils.loadAnimation(YNoteApplication.getInstance(), 2130968577);
    localAnimation.setAnimationListener(new Animation.AnimationListener(paramView)
    {
      public void onAnimationEnd(Animation paramAnimation)
      {
        this.val$view.setVisibility(8);
      }

      public void onAnimationRepeat(Animation paramAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnimation)
      {
      }
    });
    paramView.startAnimation(localAnimation);
  }

  public static void makeViewVisiable(View paramView)
  {
    if (paramView.getVisibility() == 0)
      return;
    Animation localAnimation = AnimationUtils.loadAnimation(YNoteApplication.getInstance(), 2130968576);
    localAnimation.setAnimationListener(new Animation.AnimationListener(paramView)
    {
      public void onAnimationEnd(Animation paramAnimation)
      {
      }

      public void onAnimationRepeat(Animation paramAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnimation)
      {
        this.val$view.setVisibility(0);
      }
    });
    paramView.startAnimation(localAnimation);
  }

  public static void showFormattedToast(Context paramContext, int paramInt, Object[] paramArrayOfObject)
  {
    Toast.makeText(paramContext, String.format(paramContext.getText(paramInt).toString(), paramArrayOfObject), 1).show();
  }

  public static void showImageToast(Context paramContext, int paramInt, Drawable paramDrawable)
  {
    View localView = LayoutInflater.from(paramContext).inflate(2130903099, null);
    ((TextView)localView.findViewById(2131165338)).setText(paramInt);
    Toast localToast = new Toast(paramContext);
    localToast.setDuration(1);
    localToast.setView(localView);
    localToast.show();
  }

  public static void showToast(Context paramContext, int paramInt)
  {
    showToast(paramContext, paramInt, false);
  }

  public static void showToast(Context paramContext, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 1; ; i = 0)
    {
      Toast.makeText(paramContext, paramInt, i).show();
      return;
    }
  }

  public static void showToast(Context paramContext, String paramString)
  {
    showToast(paramContext, paramString, false);
  }

  public static void showToast(Context paramContext, String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 1; ; i = 0)
    {
      Toast.makeText(paramContext, paramString, i).show();
      return;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.UIUtilities
 * JD-Core Version:    0.5.4
 */