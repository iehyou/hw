package com.youdao.note.utils;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.resource.AbstractImageResource;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageUtils
{
  public static final int HIGH_SIZE = 1200;
  public static final int LOW_SIZE = 640;
  public static final int NOMAL_SIZE = 800;
  public static final int QUALITY_HIGH = 3;
  public static final int QUALITY_LOW = 1;
  public static final int QUALITY_NORMAL = 2;
  public static final int QUALITY_ORIGINAL = 4;
  private static final int THUMBNAIL_SIZE = 480;

  public static byte[] bitmap2bytes(Bitmap paramBitmap)
  {
    return bitmap2bytes(paramBitmap, Bitmap.CompressFormat.JPEG, 75);
  }

  public static byte[] bitmap2bytes(Bitmap paramBitmap, Bitmap.CompressFormat paramCompressFormat, int paramInt)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramBitmap.compress(paramCompressFormat, paramInt, localByteArrayOutputStream);
    return localByteArrayOutputStream.toByteArray();
  }

  public static Bitmap bytes2bitmap(byte[] paramArrayOfByte)
  {
    Bitmap localBitmap1 = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
    Bitmap localBitmap2 = Bitmap.createScaledBitmap(localBitmap1, localBitmap1.getWidth(), localBitmap1.getHeight(), false);
    localBitmap1.recycle();
    return localBitmap2;
  }

  public static int bytesPerPixel(Bitmap.Config paramConfig)
  {
    if (paramConfig == Bitmap.Config.ALPHA_8)
      return 1;
    if (paramConfig == Bitmap.Config.RGB_565)
      return 2;
    return 4;
  }

  public static Bitmap compressImage(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    Matrix localMatrix = new Matrix();
    int i;
    label16: float f2;
    float f3;
    if (paramInt1 > paramInt2)
    {
      i = paramInt1;
      if ((paramBitmap.getHeight() > i) || (paramBitmap.getWidth() > i))
      {
        float f1 = paramInt1 / paramBitmap.getHeight();
        f2 = paramInt2 / paramBitmap.getWidth();
        if (f1 >= f2)
          break label108;
        f3 = f1;
      }
    }
    while (true)
    {
      localMatrix.postScale(f3, f3);
      int j = paramBitmap.getWidth();
      int k = paramBitmap.getHeight();
      paramBitmap = Bitmap.createBitmap(paramBitmap, 0, 0, j, k, localMatrix, true);
      return paramBitmap;
      i = paramInt2;
      break label16:
      label108: f3 = f2;
    }
  }

  public static Bitmap compressImage(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return compressImage(BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length), paramInt1, paramInt2);
  }

  public static Bitmap cropImage(Bitmap paramBitmap, int paramInt)
  {
    int i = paramBitmap.getHeight();
    int j = paramBitmap.getWidth();
    if (i < j);
    for (int k = i; ; k = j)
    {
      int l = i / 2;
      int i1 = j / 2;
      float f = (float)(1.0D * paramInt / k);
      Matrix localMatrix = new Matrix();
      localMatrix.postScale(f, f);
      Log.d("ImageUtils", "size is " + (i1 - k / 2) + " " + (l - k / 2) + " " + k + " " + k);
      return Bitmap.createBitmap(paramBitmap, i1 - k / 2, l - k / 2, k, k, localMatrix, true);
    }
  }

  public static BitmapFactory.Options getBitmapOption(Uri paramUri)
    throws FileNotFoundException
  {
    return getBitmapOption(FileUtils.openFileDescriptor(paramUri, "r"));
  }

  public static BitmapFactory.Options getBitmapOption(FileDescriptor paramFileDescriptor)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeFileDescriptor(paramFileDescriptor, null, localOptions);
    return localOptions;
  }

  public static BitmapFactory.Options getBitmapOption(String paramString)
    throws FileNotFoundException
  {
    FileDescriptor localFileDescriptor = FileUtils.openFileDescriptor(paramString, "r");
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeFileDescriptor(localFileDescriptor, null, localOptions);
    return localOptions;
  }

  public static Rect getImageDimensions(String paramString)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    try
    {
      BitmapFactory.decodeFile(paramString, localOptions);
      Rect localRect = new Rect(0, 0, localOptions.outWidth, localOptions.outHeight);
      return localRect;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public static <T extends AbstractImageResourceMeta> void persistImage(AbstractImageResource<T> paramAbstractImageResource, Bitmap paramBitmap, int paramInt, boolean paramBoolean)
  {
    saveThumbnail(paramAbstractImageResource, paramBitmap, paramBoolean);
    saveImageResource(paramAbstractImageResource, paramBitmap, paramInt);
  }

  public static <T extends AbstractImageResourceMeta> void persistImage(AbstractImageResource<T> paramAbstractImageResource, Uri paramUri, int paramInt, boolean paramBoolean)
    throws IOException
  {
    Bitmap localBitmap = FileUtils.getBitmapFromUri(paramUri);
    saveThumbnail(paramAbstractImageResource, localBitmap, paramBoolean);
    if (paramInt == 4)
    {
      DataSource localDataSource = YNoteApplication.getInstance().getDataSource();
      AbstractImageResource localAbstractImageResource = (AbstractImageResource)localDataSource.getResource(paramAbstractImageResource.getMeta());
      FileUtils.copyFile(paramUri, localDataSource.getResourcePath(localAbstractImageResource.getMeta()));
      setResourceSize(localAbstractImageResource, paramUri);
      paramAbstractImageResource.setMeta(localAbstractImageResource.getMeta());
      paramAbstractImageResource.releaseData();
      return;
    }
    saveImageResource(paramAbstractImageResource, localBitmap, paramInt);
  }

  private static void resizeImg(Bitmap paramBitmap, AbstractImageResource<? extends AbstractImageResourceMeta> paramAbstractImageResource1, int paramInt, AbstractImageResource<? extends AbstractImageResourceMeta> paramAbstractImageResource2)
  {
    Bitmap localBitmap = compressImage(paramBitmap, paramInt, paramInt);
    paramAbstractImageResource1.setContentBytes(bitmap2bytes(localBitmap, Bitmap.CompressFormat.JPEG, 100));
    setResourceSize(paramAbstractImageResource2, localBitmap);
    ((AbstractImageResourceMeta)paramAbstractImageResource2.getMeta()).setLength(paramAbstractImageResource1.getLength());
  }

  private static <T extends AbstractImageResourceMeta> void saveImageResource(AbstractImageResource<T> paramAbstractImageResource, Bitmap paramBitmap, int paramInt)
  {
    DataSource localDataSource = YNoteApplication.getInstance().getDataSource();
    AbstractImageResource localAbstractImageResource = (AbstractImageResource)localDataSource.getResource(paramAbstractImageResource.getMeta());
    switch (paramInt)
    {
    default:
      localAbstractImageResource.setContentBytes(bitmap2bytes(paramBitmap, Bitmap.CompressFormat.JPEG, 100));
      setResourceSize(paramAbstractImageResource, paramBitmap);
      ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setLength(localAbstractImageResource.getLength());
    case 3:
    case 2:
    case 1:
    }
    while (true)
    {
      localDataSource.updateResourceCache(localAbstractImageResource);
      paramAbstractImageResource.setMeta(localAbstractImageResource.getMeta());
      localAbstractImageResource.releaseData();
      paramAbstractImageResource.releaseData();
      return;
      resizeImg(paramBitmap, localAbstractImageResource, 1200, paramAbstractImageResource);
      continue;
      resizeImg(paramBitmap, localAbstractImageResource, 800, paramAbstractImageResource);
      continue;
      resizeImg(paramBitmap, localAbstractImageResource, 640, paramAbstractImageResource);
    }
  }

  private static void saveThumbnail(AbstractImageResource<? extends AbstractImageResourceMeta> paramAbstractImageResource, Bitmap paramBitmap, boolean paramBoolean)
  {
    Thumbnail localThumbnail = new Thumbnail((AbstractImageResourceMeta)paramAbstractImageResource.getMeta());
    if (paramBoolean)
      localThumbnail.setContentBytes(bitmap2bytes(compressImage(paramBitmap, 480, 480), Bitmap.CompressFormat.JPEG, 100));
    while (true)
    {
      YNoteApplication.getInstance().getDataSource().writeThumbnail(localThumbnail);
      return;
      localThumbnail.setContentBytes(paramAbstractImageResource.getContentBytes());
    }
  }

  private static void setResourceSize(AbstractImageResource<? extends AbstractImageResourceMeta> paramAbstractImageResource, Bitmap paramBitmap)
  {
    ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setHeight(paramBitmap.getHeight());
    ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setWidth(paramBitmap.getWidth());
  }

  private static void setResourceSize(AbstractImageResource<? extends AbstractImageResourceMeta> paramAbstractImageResource, Uri paramUri)
    throws FileNotFoundException
  {
    ParcelFileDescriptor localParcelFileDescriptor = YNoteApplication.getInstance().getContentResolver().openFileDescriptor(paramUri, "r");
    FileDescriptor localFileDescriptor = localParcelFileDescriptor.getFileDescriptor();
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeFileDescriptor(localFileDescriptor, null, localOptions);
    ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setHeight(localOptions.outHeight);
    ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setWidth(localOptions.outWidth);
    ((AbstractImageResourceMeta)paramAbstractImageResource.getMeta()).setLength(localParcelFileDescriptor.getStatSize());
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.ImageUtils
 * JD-Core Version:    0.5.4
 */