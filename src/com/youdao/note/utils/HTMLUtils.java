package com.youdao.note.utils;

import android.text.TextUtils;
import android.util.Log;
import com.youdao.note.YNoteApplication;
import com.youdao.note.data.Thumbnail;
import com.youdao.note.data.resource.AbstractImageResourceMeta;
import com.youdao.note.data.resource.AbstractResource;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.GeneralResource;
import com.youdao.note.data.resource.GeneralResourceMeta;
import com.youdao.note.data.resource.IResourceMeta;
import com.youdao.note.data.resource.ImageResource;
import com.youdao.note.data.resource.ImageResourceMeta;
import com.youdao.note.datasource.DataSource;
import com.youdao.note.task.TaskManager;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.HtmlNode;
import org.htmlcleaner.SimpleHtmlSerializer;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.TagNodeVisitor;
import org.xmlpull.v1.XmlPullParserException;

public class HTMLUtils
  implements Consts.APIS
{
  public static final String ANDROID_RESOURCE_ATT = "rborder";
  public static final String ANDROID_RESOURCE_IMG = "android-resource-img";
  private static final String ATTR_FILENAME = "filename";
  private static final String ATTR_LENGTH = "filelength";
  private static final String ATTR_PATH = "path";
  private static final String ATTR_SRC = "src";
  private static final String ATTR_TITLE = "title";
  private static final String BAK_HEIGHT = "bak-height";
  private static final String BAK_SRC = "bak-src";
  private static final String BAK_STYLE = "bak-style";
  private static final String BAK_WIDTH = "bak-width";
  public static final String BR = "<br/>";
  public static final String EMPTY_FOOTER = "<br/><span>&nbsp;</span>";
  private static final String TAG = HTMLUtils.class.getSimpleName();
  private static final String UNUSED_HTML_FOOT = "</body></html>";
  private static final String UNUSED_HTML_HEAD = "<html><head></head><body>";
  private static HtmlCleaner sCleaner = new HtmlCleaner();

  static
  {
    CleanerProperties localCleanerProperties = sCleaner.getProperties();
    localCleanerProperties.setAllowHtmlInsideAttributes(true);
    localCleanerProperties.setAllowMultiWordAttributes(true);
    localCleanerProperties.setRecognizeUnicodeChars(true);
    localCleanerProperties.setOmitComments(true);
  }

  public static String convertToLocalHtml(String paramString1, DataSource paramDataSource, String paramString2)
    throws XmlPullParserException, IOException
  {
    monitorenter;
    TagNode localTagNode;
    HashMap localHashMap;
    BaseResourceMeta localBaseResourceMeta;
    try
    {
      localTagNode = sCleaner.clean(paramString2);
      Log.d(TAG, "Before convert to local html is " + paramString2);
      localHashMap = new HashMap();
      Iterator localIterator1 = paramDataSource.getResourceMetasByNoteId(paramString1).iterator();
      if (!localIterator1.hasNext())
        break label103;
      localBaseResourceMeta = (BaseResourceMeta)localIterator1.next();
    }
    finally
    {
      monitorexit;
    }
    label103: localTagNode.traverse(new TagNodeVisitor(localHashMap, paramString1, paramDataSource)
    {
      private void convertAttachResrouce(TagNode paramTagNode, GeneralResourceMeta paramGeneralResourceMeta)
      {
        L.d(HTMLUtils.class, "Convert attatch resource called.");
        TagNode localTagNode = paramTagNode.getParent();
        int i = localTagNode.getChildIndex(paramTagNode);
        localTagNode.removeChild(paramTagNode);
        localTagNode.insertChild(i, HTMLUtils.createResourceNode(this.val$dataSource, paramTagNode, paramGeneralResourceMeta));
      }

      private void convertImageResource(TagNode paramTagNode, Thumbnail paramThumbnail)
      {
        if (!this.val$dataSource.existThumbnail(paramThumbnail.getRelativePath()))
          TaskManager.getInstance().pullThumbnail(paramThumbnail);
        L.d(HTMLUtils.class, "resource length is " + paramThumbnail.getLength());
        paramTagNode.setAttribute("class", "android-resource-img");
        paramTagNode.setAttribute("onClick", "window.View.viewResource('" + paramThumbnail.getImageMeta().getResourceId() + "')");
      }

      private AbstractResource<? extends IResourceMeta> getResource(String paramString1, String paramString2, BaseResourceMeta paramBaseResourceMeta)
      {
        if (paramString2 != null)
          return new GeneralResource((GeneralResourceMeta)YNoteUtils.extractResource(paramString2, 1));
        ImageResourceMeta localImageResourceMeta = new ImageResourceMeta(paramBaseResourceMeta);
        localImageResourceMeta.setFileName(paramBaseResourceMeta.getResourceId() + ".jpg");
        return new ImageResource(localImageResourceMeta);
      }

      public boolean visit(TagNode paramTagNode, HtmlNode paramHtmlNode)
      {
        TagNode localTagNode;
        String str1;
        String str2;
        String str3;
        int i;
        label56: String str4;
        int j;
        if (paramHtmlNode instanceof TagNode)
        {
          localTagNode = (TagNode)paramHtmlNode;
          if ("img".equals(localTagNode.getName()))
          {
            str1 = localTagNode.getAttributeByName("src");
            str2 = localTagNode.getAttributeByName("path");
            str3 = localTagNode.getAttributeByName("filename");
            if (str2 == null)
              break label368;
            i = 1;
            if (TextUtils.isEmpty(str3))
              str3 = localTagNode.getAttributeByName("title");
            str4 = localTagNode.getAttributeByName("filelength");
            j = 0;
            if (str4 == null);
          }
        }
        try
        {
          int l = Integer.parseInt(str4);
          j = l;
          label99: BaseResourceMeta localBaseResourceMeta1 = YNoteUtils.extractResource(str1, i);
          if (localBaseResourceMeta1 != null)
          {
            AbstractResource localAbstractResource = getResource(str1, str2, localBaseResourceMeta1);
            this.val$resourceMap.remove(localAbstractResource.getResourceId());
            BaseResourceMeta localBaseResourceMeta2 = localAbstractResource.getMeta();
            localBaseResourceMeta2.setLength(j);
            localBaseResourceMeta2.setNoteId(this.val$noteId);
            if (!TextUtils.isEmpty(str3))
              localBaseResourceMeta2.setFileName(str3);
            BaseResourceMeta localBaseResourceMeta3 = this.val$dataSource.getResourceMeta(localAbstractResource.getResourceId());
            if ((localBaseResourceMeta3 != null) && (localAbstractResource.getMeta().getVersion() > localBaseResourceMeta3.getVersion()))
            {
              L.d(this, "try to delete reousrce.");
              this.val$dataSource.deleteResource(localAbstractResource.getMeta());
            }
            this.val$dataSource.insertOrUpdateResource(localAbstractResource);
            localTagNode.setAttribute("bak-src", localTagNode.getAttributeByName("src"));
            localTagNode.setAttribute("bak-style", localTagNode.getAttributeByName("style"));
            localTagNode.removeAttribute("style");
            localTagNode.setAttribute("bak-width", localTagNode.getAttributeByName("width"));
            localTagNode.removeAttribute("width");
            localTagNode.setAttribute("bak-height", localTagNode.getAttributeByName("height"));
            localTagNode.removeAttribute("height");
            localTagNode.setAttribute("id", localAbstractResource.getMeta().getResourceId());
            localTagNode.setAttribute("src", this.val$dataSource.getThumbnailPath(localBaseResourceMeta2));
            if (str2 != null)
              convertAttachResrouce(localTagNode, (GeneralResourceMeta)((GeneralResource)localAbstractResource).getMeta());
            while (true)
            {
              return true;
              label368: i = 0;
              break label56:
              convertImageResource(localTagNode, new Thumbnail((AbstractImageResourceMeta)((ImageResource)localAbstractResource).getMeta()));
            }
          }
          Log.d(HTMLUtils.TAG, "got resource null.");
        }
        catch (NumberFormatException localNumberFormatException)
        {
          int k = 0;
          break label99:
        }
      }
    });
    Iterator localIterator2 = localHashMap.entrySet().iterator();
    while (localIterator2.hasNext())
      paramDataSource.deleteResource((BaseResourceMeta)((Map.Entry)localIterator2.next()).getValue());
    String str = serilizeHtml(localTagNode);
    monitorexit;
    return str;
  }

  public static String convertToServerHtml(String paramString)
  {
    monitorenter;
    TagNode localTagNode;
    try
    {
      localTagNode.traverse(new TagNodeVisitor(YNoteApplication.getInstance().getDataSource())
      {
        private void recoverSrc(TagNode paramTagNode)
        {
          String str1 = paramTagNode.getAttributeByName("bak-src");
          String str2 = paramTagNode.getAttributeByName("bak-style");
          paramTagNode.removeAttribute("style");
          if (!TextUtils.isEmpty(str2))
            paramTagNode.setAttribute("style", str2);
          paramTagNode.removeAttribute("bak-style");
          String str3 = paramTagNode.getAttributeByName("bak-width");
          if (!TextUtils.isEmpty(str3))
            paramTagNode.setAttribute("width", str3);
          paramTagNode.removeAttribute("bak-width");
          String str4 = paramTagNode.getAttributeByName("bak-height");
          if (!TextUtils.isEmpty(str4))
            paramTagNode.setAttribute("height", str4);
          paramTagNode.removeAttribute("bak-height");
          Log.d(HTMLUtils.TAG, "bakSrc is " + str1);
          if (!TextUtils.isEmpty(str1))
          {
            paramTagNode.setAttribute("src", str1);
            paramTagNode.removeAttribute("bak-src");
          }
          while (true)
          {
            paramTagNode.removeAttribute("id");
            return;
            String str5 = paramTagNode.getAttributeByName("id");
            BaseResourceMeta localBaseResourceMeta = this.val$dataSource.getResourceMeta(str5);
            if (localBaseResourceMeta != null)
            {
              StringBuilder localStringBuilder = new StringBuilder();
              localStringBuilder.append("http://").append(YNoteApplication.getInstance().getHost()).append("yws/res/").append(localBaseResourceMeta.getVersion()).append("/").append(str5);
              if (localBaseResourceMeta instanceof AbstractImageResourceMeta)
                paramTagNode.setAttribute("src", localStringBuilder.toString());
              paramTagNode.setAttribute("path", localStringBuilder.toString());
              paramTagNode.setAttribute("src", localBaseResourceMeta.getSrc());
              paramTagNode.setAttribute("filename", localBaseResourceMeta.getFileName());
              paramTagNode.setAttribute("filelength", Long.toString(localBaseResourceMeta.getLength()));
            }
            L.w(this, "Failed to find resource " + str5);
          }
        }

        private void resetNode(TagNode paramTagNode)
        {
          paramTagNode.removeAttribute("onClick");
          paramTagNode.removeAttribute("class");
          paramTagNode.removeAttribute("bak-src");
        }

        public boolean visit(TagNode paramTagNode, HtmlNode paramHtmlNode)
        {
          TagNode localTagNode1;
          if (paramHtmlNode instanceof TagNode)
          {
            localTagNode1 = (TagNode)paramHtmlNode;
            if ((paramTagNode == null) || (!paramTagNode.getName().equals("table")) || (!"rborder".equals(paramTagNode.getAttributeByName("class"))))
              break label137;
            List localList1 = localTagNode1.getChildTagList();
            if (localList1.size() > 0)
            {
              List localList2 = ((TagNode)localList1.get(0)).getChildTagList();
              if (localList2.size() > 0)
              {
                List localList3 = ((TagNode)localList2.get(0)).getChildTagList();
                if (localList3.size() > 0)
                {
                  TagNode localTagNode2 = (TagNode)localList3.get(0);
                  recoverSrc(localTagNode2);
                  HTMLUtils.access$100(paramTagNode, localTagNode2);
                }
              }
            }
          }
          while (true)
          {
            return true;
            label137: if ((!localTagNode1.getName().equals("img")) || (!"android-resource-img".equals(localTagNode1.getAttributeByName("class"))))
              continue;
            recoverSrc(localTagNode1);
            resetNode(localTagNode1);
          }
        }
      });
    }
    finally
    {
      String str1;
      try
      {
        String str2 = serilizeHtml(localTagNode);
        str1 = str2;
        label38: monitorexit;
        return str1;
      }
      catch (Exception localException)
      {
        str1 = paramString;
        break label38:
        localObject = finally;
        monitorexit;
        throw localObject;
      }
    }
  }

  public static TagNode createResourceNode(DataSource paramDataSource, BaseResourceMeta paramBaseResourceMeta)
  {
    TagNode localTagNode = new TagNode("img");
    localTagNode.setAttribute("id", paramBaseResourceMeta.getResourceId());
    return createResourceNode(paramDataSource, localTagNode, paramBaseResourceMeta);
  }

  public static TagNode createResourceNode(DataSource paramDataSource, TagNode paramTagNode, BaseResourceMeta paramBaseResourceMeta)
  {
    TagNode localTagNode1 = new TagNode("table");
    localTagNode1.setAttribute("contenteditable", "false");
    TagNode localTagNode2 = new TagNode("tr");
    TagNode localTagNode3 = new TagNode("td");
    localTagNode3.setAttribute("width", "35px");
    localTagNode3.setAttribute("rowspan", "2");
    paramTagNode.setAttribute("src", FileUtils.getResourceIconPath(paramBaseResourceMeta.getFileName()));
    paramTagNode.setAttribute("class", "attachicon");
    localTagNode3.addChild(paramTagNode);
    localTagNode2.addChild(localTagNode3);
    TagNode localTagNode4 = new TagNode("td");
    localTagNode4.setAttribute("width", "230px");
    localTagNode4.addChild(new ContentNode(paramBaseResourceMeta.getFileName()));
    localTagNode2.addChild(localTagNode4);
    TagNode localTagNode5 = new TagNode("td");
    localTagNode5.setAttribute("width", "20px");
    localTagNode5.setAttribute("rowspan", "2");
    TagNode localTagNode6 = new TagNode("img");
    localTagNode6.setAttribute("class", "arrow");
    label208: TagNode localTagNode7;
    TagNode localTagNode8;
    File localFile;
    if (paramDataSource.existResource(paramBaseResourceMeta))
    {
      localTagNode6.setAttribute("src", "file:///android_asset/arrow.png");
      localTagNode5.addChild(localTagNode6);
      localTagNode2.addChild(localTagNode5);
      localTagNode1.addChild(localTagNode2);
      localTagNode7 = new TagNode("tr");
      localTagNode8 = new TagNode("td");
      localTagNode8.setAttribute("style", "color:gray");
      localFile = new File(DataSource.getTmpResourcePath(paramDataSource.getResourcePath(paramBaseResourceMeta)));
      if (!localFile.exists())
        break label399;
    }
    for (ContentNode localContentNode = new ContentNode(UnitUtils.getSize(paramBaseResourceMeta.getLength(), (int)(100.0D * localFile.length() / paramBaseResourceMeta.getLength()))); ; localContentNode = new ContentNode(UnitUtils.getSize(paramBaseResourceMeta.getLength())))
    {
      localTagNode8.addChild(localContentNode);
      localTagNode7.addChild(localTagNode8);
      localTagNode1.addChild(localTagNode7);
      localTagNode1.setAttribute("onClick", "window.View.viewResource('" + paramBaseResourceMeta.getResourceId() + "')");
      localTagNode1.setAttribute("class", "rborder");
      return localTagNode1;
      localTagNode6.setAttribute("src", "file:///android_asset/download.png");
      label399: break label208:
    }
  }

  public static String escAndEncodeHTML(String paramString)
  {
    if (paramString == null)
      return "";
    StringBuilder localStringBuilder = new StringBuilder(paramString.length());
    int i = paramString.length();
    int j = 0;
    if (j < i)
    {
      label27: char c = paramString.charAt(j);
      if (c == '"')
        localStringBuilder.append("&quot;");
      while (true)
      {
        ++j;
        break label27:
        if (c == '&')
          localStringBuilder.append("&amp;");
        if (c == '<')
          localStringBuilder.append("&lt;");
        if (c == '>')
          localStringBuilder.append("&gt;");
        if (c == ' ')
          localStringBuilder.append("&nbsp;");
        if (c == '\n')
          localStringBuilder.append("<br/>");
        localStringBuilder.append(c);
      }
    }
    return localStringBuilder.toString();
  }

  public static String extractSummary(String paramString, int paramInt)
  {
    monitorenter;
    try
    {
      StringBuilder localStringBuilder = new StringBuilder(paramInt);
      sCleaner.clean(paramString).traverse(new TagNodeVisitor(localStringBuilder, paramInt)
      {
        public boolean visit(TagNode paramTagNode, HtmlNode paramHtmlNode)
        {
          String str;
          if (paramHtmlNode instanceof ContentNode)
          {
            str = HTMLUtils.revertHTMLText(((ContentNode)paramHtmlNode).toString()).trim();
            this.val$ret.append(" ");
            if (str.length() >= this.val$maxSize - this.val$ret.length())
              break label61;
            this.val$ret.append(str);
          }
          return true;
          label61: this.val$ret.append(str.substring(0, this.val$maxSize - this.val$ret.length()));
          return false;
        }
      });
      String str = localStringBuilder.toString();
      monitorexit;
      return str;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  private static void replaceNode(TagNode paramTagNode1, TagNode paramTagNode2)
  {
    TagNode localTagNode = paramTagNode1.getParent();
    localTagNode.insertChild(localTagNode.getChildIndex(paramTagNode1), paramTagNode2);
    localTagNode.removeChild(paramTagNode1);
  }

  public static String revertHTMLText(String paramString)
  {
    return paramString.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quot;", "\"").replaceAll("&amp;", "&").replaceAll("&nbsp;", " ");
  }

  public static String serilizeHtml(TagNode paramTagNode)
    throws IOException
  {
    monitorenter;
    try
    {
      SimpleHtmlSerializer localSimpleHtmlSerializer = new SimpleHtmlSerializer(sCleaner.getProperties());
      StringWriter localStringWriter = new StringWriter();
      paramTagNode.serialize(localSimpleHtmlSerializer, localStringWriter);
      String str = localStringWriter.toString();
      if ((str.startsWith("<html><head></head><body>")) && (str.endsWith("</body></html>")))
        str = str.substring("<html><head></head><body>".length(), str.length() - "</body></html>".length());
      if (!str.endsWith("<br/><span>&nbsp;</span>"))
        str = str + "<br/><span>&nbsp;</span>";
      L.d(HTMLUtils.class, "converted string is " + str);
      return str;
    }
    finally
    {
      monitorexit;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.HTMLUtils
 * JD-Core Version:    0.5.4
 */