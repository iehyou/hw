package com.youdao.note.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class IpUtils
{
  public static String getLocalIpAddress()
  {
    try
    {
      Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
      if (localEnumeration1.hasMoreElements())
      {
        Enumeration localEnumeration2 = ((NetworkInterface)localEnumeration1.nextElement()).getInetAddresses();
        String str;
        int i;
        do
        {
          InetAddress localInetAddress;
          do
          {
            if (localEnumeration2.hasMoreElements());
            localInetAddress = (InetAddress)localEnumeration2.nextElement();
          }
          while (localInetAddress.isLoopbackAddress());
          str = localInetAddress.getHostAddress().toString();
          i = localInetAddress.getAddress().length;
        }
        while (i != 4);
        return str;
      }
    }
    catch (SocketException localSocketException)
    {
    }
    return "127.0.0.1";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.IpUtils
 * JD-Core Version:    0.5.4
 */