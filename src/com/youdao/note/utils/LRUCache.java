package com.youdao.note.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class LRUCache<K, V>
{
  private static final float factor = 0.75F;
  private int cacheSize;
  private CacheResizeListener<K, V> mCacheReizeListener = null;
  private Map<K, V> map;

  public LRUCache(int paramInt)
  {
    this.cacheSize = paramInt;
    this.map = new LinkedHashMap(1 + (int)Math.ceil(paramInt / 0.75F), 0.75F, true)
    {
      private static final long serialVersionUID = -6265505777659103537L;

      protected boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
      {
        if (size() > LRUCache.this.cacheSize);
        for (int i = 1; ; i = 0)
        {
          if (LRUCache.this.mCacheReizeListener != null)
            LRUCache.this.mCacheReizeListener.onEntryToDel(paramEntry);
          return i;
        }
      }
    };
  }

  public void clear()
  {
    monitorenter;
    try
    {
      this.map.clear();
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public V get(K paramK)
  {
    monitorenter;
    try
    {
      Object localObject2 = this.map.get(paramK);
      monitorexit;
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      monitorexit;
      throw localObject1;
    }
  }

  public Collection<Map.Entry<K, V>> getAll()
  {
    monitorenter;
    try
    {
      ArrayList localArrayList = new ArrayList(this.map.entrySet());
      monitorexit;
      return localArrayList;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void put(K paramK, V paramV)
  {
    monitorenter;
    try
    {
      this.map.put(paramK, paramV);
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void remove(K paramK)
  {
    monitorenter;
    try
    {
      this.map.remove(paramK);
      monitorexit;
      return;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public void setCacheReizeListener(CacheResizeListener<K, V> paramCacheResizeListener)
  {
    this.mCacheReizeListener = paramCacheResizeListener;
  }

  public int usedEntries()
  {
    monitorenter;
    try
    {
      int i = this.map.size();
      monitorexit;
      return i;
    }
    finally
    {
      localObject = finally;
      monitorexit;
      throw localObject;
    }
  }

  public static abstract interface CacheResizeListener<K, V>
  {
    public abstract void onEntryToDel(Map.Entry<K, V> paramEntry);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.LRUCache
 * JD-Core Version:    0.5.4
 */