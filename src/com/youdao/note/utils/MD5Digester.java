package com.youdao.note.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Digester
{
  public static String digest(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      localMessageDigest.update(paramString.getBytes());
      byte[] arrayOfByte = localMessageDigest.digest();
      StringBuilder localStringBuilder = new StringBuilder();
      for (int i = 0; i < arrayOfByte.length; ++i)
      {
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(0xFF & arrayOfByte[i]);
        localStringBuilder.append(String.format("%02x", arrayOfObject));
      }
      String str = localStringBuilder.toString();
      return str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
    }
    return paramString;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.MD5Digester
 * JD-Core Version:    0.5.4
 */