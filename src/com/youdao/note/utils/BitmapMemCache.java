package com.youdao.note.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import java.lang.ref.SoftReference;

public class BitmapMemCache
{
  private static BitmapMemCache sInstance;
  private LRUCache<String, SoftReference<Bitmap>> cache = new LRUCache(40);

  private Bitmap decodeBitmap(String paramString, int paramInt1, int paramInt2)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.outHeight = paramInt2;
    localOptions.outWidth = paramInt1;
    return BitmapFactory.decodeFile(paramString, localOptions);
  }

  public static BitmapMemCache getInstance()
  {
    if (sInstance == null)
      monitorenter;
    try
    {
      if (sInstance == null)
        sInstance = new BitmapMemCache();
      return sInstance;
    }
    finally
    {
      monitorexit;
    }
  }

  public Bitmap getBitmap(Uri paramUri, int paramInt1, int paramInt2)
  {
    return getBitmap(paramUri.toString(), paramInt1, paramInt2);
  }

  public Bitmap getBitmap(String paramString, int paramInt1, int paramInt2)
  {
    SoftReference localSoftReference1 = (SoftReference)this.cache.get(paramString);
    if ((localSoftReference1 == null) || (localSoftReference1.get() == null))
    {
      Bitmap localBitmap = decodeBitmap(paramString, paramInt1, paramInt2);
      SoftReference localSoftReference2 = new SoftReference(localBitmap);
      this.cache.put(paramString, localSoftReference2);
      return localBitmap;
    }
    return (Bitmap)localSoftReference1.get();
  }

  public void removeBitmap(String paramString)
  {
    this.cache.remove(paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.BitmapMemCache
 * JD-Core Version:    0.5.4
 */