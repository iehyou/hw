package com.youdao.note.utils;

import android.util.Log;
import com.youdao.note.LogRecorder;
import java.io.PrintWriter;
import java.io.StringWriter;

public class L
{
  private static boolean sDebug = true;
  private static LogRecorder sLogger;

  public static void d(Object paramObject, String paramString)
  {
    if (!sDebug)
      return;
    Log.d(getTagName(paramObject), paramString);
  }

  public static void e(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if (paramThrowable != null)
    {
      StringWriter localStringWriter = new StringWriter();
      paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
      sLogger.setError(localStringWriter.toString());
    }
    if (!sDebug)
      return;
    Log.e(getTagName(paramObject), paramString, paramThrowable);
  }

  private static String getTagName(Object paramObject)
  {
    if (paramObject instanceof Class)
      return ((Class)paramObject).getSimpleName();
    return getTagName(paramObject.getClass());
  }

  public static void i(Object paramObject, String paramString)
  {
    Log.i(getTagName(paramObject), paramString);
  }

  public static void setDebug(boolean paramBoolean)
  {
    sDebug = paramBoolean;
  }

  public static void setLogger(LogRecorder paramLogRecorder)
  {
    sLogger = paramLogRecorder;
  }

  public static void w(Object paramObject, String paramString)
  {
    if (!sDebug)
      return;
    Log.w(getTagName(paramObject), paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.L
 * JD-Core Version:    0.5.4
 */