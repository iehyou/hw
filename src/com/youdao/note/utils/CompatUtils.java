package com.youdao.note.utils;

import android.view.MotionEvent;

public class CompatUtils
{
  public static int getActionMasked(MotionEvent paramMotionEvent)
  {
    return 0xFF & paramMotionEvent.getAction();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.CompatUtils
 * JD-Core Version:    0.5.4
 */