package com.youdao.note.utils;

import com.youdao.note.YNoteApplication;
import java.net.URLEncoder;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class WebUtils
  implements Consts
{
  private static final int INITIAL_CAPACITY = 64;

  public static void appendParams(StringBuilder paramStringBuilder, Object paramObject1, Object paramObject2)
  {
    appendParams(paramStringBuilder, (String)paramObject1, paramObject2);
  }

  public static void appendParams(StringBuilder paramStringBuilder, String paramString, Object paramObject)
  {
    if (paramObject instanceof String);
    for (String str = (String)paramObject; ; str = paramObject + "")
      while (true)
      {
        paramStringBuilder.append(paramString).append("=").append(URLEncoder.encode(str));
        return;
        if (!paramObject instanceof Number)
          break;
        str = ((Number)paramObject).toString();
      }
  }

  public static String constructRequestUrl(String paramString1, String paramString2, Object[] paramArrayOfObject)
  {
    StringBuilder localStringBuilder = new StringBuilder(64);
    localStringBuilder.append("http://").append(YNoteApplication.getInstance().getHost());
    localStringBuilder.append(paramString1).append("?");
    appendParams(localStringBuilder, "method", paramString2);
    for (int i = 0; (paramArrayOfObject != null) && (i < paramArrayOfObject.length); i += 2)
    {
      if (paramArrayOfObject[(i + 1)] == null)
        continue;
      localStringBuilder.append("&").append(paramArrayOfObject[i].toString()).append("=").append(paramArrayOfObject[(i + 1)].toString());
    }
    return localStringBuilder.toString();
  }

  public static String getSessionCookie(HttpResponse paramHttpResponse)
  {
    for (Header localHeader : paramHttpResponse.getHeaders("Set-Cookie"))
    {
      L.d(WebUtils.class, localHeader.getName() + ":" + localHeader.getValue());
      if (!localHeader.getValue().startsWith("NTES_SESS"))
        continue;
      int k = localHeader.getValue().indexOf("=");
      int l = localHeader.getValue().indexOf(";");
      return localHeader.getValue().substring(k + 1, l);
    }
    return "";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.WebUtils
 * JD-Core Version:    0.5.4
 */