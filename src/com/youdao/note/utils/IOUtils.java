package com.youdao.note.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class IOUtils
{
  private static final int DEFAULT_BUFFER_SIZE = 4096;
  public static final char DIR_SEPARATOR = '\000';
  public static final char DIR_SEPARATOR_UNIX = '/';
  public static final char DIR_SEPARATOR_WINDOWS = '\\';
  public static final String LINE_SEPARATOR;
  public static final String LINE_SEPARATOR_UNIX = "\n";
  public static final String LINE_SEPARATOR_WINDOWS = "\r\n";

  static
  {
    StringWriter localStringWriter = new StringWriter(4);
    new PrintWriter(localStringWriter).println();
    LINE_SEPARATOR = localStringWriter.toString();
  }

  public static void closeQuietly(InputStream paramInputStream)
  {
    if (paramInputStream != null);
    try
    {
      paramInputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public static void closeQuietly(OutputStream paramOutputStream)
  {
    if (paramOutputStream != null);
    try
    {
      paramOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public static void closeQuietly(Reader paramReader)
  {
    if (paramReader != null);
    try
    {
      paramReader.close();
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public static void closeQuietly(Writer paramWriter)
  {
    if (paramWriter != null);
    try
    {
      paramWriter.close();
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public static boolean contentEquals(InputStream paramInputStream1, InputStream paramInputStream2)
    throws IOException
  {
    if (!paramInputStream1 instanceof BufferedInputStream)
      paramInputStream1 = new BufferedInputStream(paramInputStream1);
    if (!paramInputStream2 instanceof BufferedInputStream)
      paramInputStream2 = new BufferedInputStream(paramInputStream2);
    int i = paramInputStream1.read();
    if (-1 != i)
      label37: if (i == paramInputStream2.read());
    do
    {
      return false;
      i = paramInputStream1.read();
      break label37:
    }
    while (paramInputStream2.read() != -1);
    return true;
  }

  public static boolean contentEquals(Reader paramReader1, Reader paramReader2)
    throws IOException
  {
    if (!paramReader1 instanceof BufferedReader)
      paramReader1 = new BufferedReader(paramReader1);
    if (!paramReader2 instanceof BufferedReader)
      paramReader2 = new BufferedReader(paramReader2);
    int i = paramReader1.read();
    if (-1 != i)
      label37: if (i == paramReader2.read());
    do
    {
      return false;
      i = paramReader1.read();
      break label37:
    }
    while (paramReader2.read() != -1);
    return true;
  }

  public static int copy(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    long l = copyLarge(paramInputStream, paramOutputStream);
    if (l > 2147483647L)
      return -1;
    return (int)l;
  }

  public static int copy(Reader paramReader, Writer paramWriter)
    throws IOException
  {
    long l = copyLarge(paramReader, paramWriter);
    if (l > 2147483647L)
      return -1;
    return (int)l;
  }

  public static void copy(InputStream paramInputStream, Writer paramWriter)
    throws IOException
  {
    copy(new InputStreamReader(paramInputStream), paramWriter);
  }

  public static void copy(InputStream paramInputStream, Writer paramWriter, String paramString)
    throws IOException
  {
    if (paramString == null)
    {
      copy(paramInputStream, paramWriter);
      return;
    }
    copy(new InputStreamReader(paramInputStream, paramString), paramWriter);
  }

  public static void copy(Reader paramReader, OutputStream paramOutputStream)
    throws IOException
  {
    OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(paramOutputStream);
    copy(paramReader, localOutputStreamWriter);
    localOutputStreamWriter.flush();
  }

  public static void copy(Reader paramReader, OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    if (paramString == null)
    {
      copy(paramReader, paramOutputStream);
      return;
    }
    OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(paramOutputStream, paramString);
    copy(paramReader, localOutputStreamWriter);
    localOutputStreamWriter.flush();
  }

  public static long copyLarge(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte[4096];
    long l = 0L;
    while (true)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (-1 == i)
        break;
      paramOutputStream.write(arrayOfByte, 0, i);
      l += i;
    }
    return l;
  }

  public static long copyLarge(Reader paramReader, Writer paramWriter)
    throws IOException
  {
    char[] arrayOfChar = new char[4096];
    long l = 0L;
    while (true)
    {
      int i = paramReader.read(arrayOfChar);
      if (-1 == i)
        break;
      paramWriter.write(arrayOfChar, 0, i);
      l += i;
    }
    return l;
  }

  public static byte[] toByteArray(InputStream paramInputStream)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    copy(paramInputStream, localByteArrayOutputStream);
    return localByteArrayOutputStream.toByteArray();
  }

  public static byte[] toByteArray(Reader paramReader)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    copy(paramReader, localByteArrayOutputStream);
    return localByteArrayOutputStream.toByteArray();
  }

  public static byte[] toByteArray(Reader paramReader, String paramString)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    copy(paramReader, localByteArrayOutputStream, paramString);
    return localByteArrayOutputStream.toByteArray();
  }

  public static byte[] toByteArray(String paramString)
    throws IOException
  {
    return paramString.getBytes();
  }

  public static char[] toCharArray(InputStream paramInputStream)
    throws IOException
  {
    CharArrayWriter localCharArrayWriter = new CharArrayWriter();
    copy(paramInputStream, localCharArrayWriter);
    return localCharArrayWriter.toCharArray();
  }

  public static char[] toCharArray(InputStream paramInputStream, String paramString)
    throws IOException
  {
    CharArrayWriter localCharArrayWriter = new CharArrayWriter();
    copy(paramInputStream, localCharArrayWriter, paramString);
    return localCharArrayWriter.toCharArray();
  }

  public static char[] toCharArray(Reader paramReader)
    throws IOException
  {
    CharArrayWriter localCharArrayWriter = new CharArrayWriter();
    copy(paramReader, localCharArrayWriter);
    return localCharArrayWriter.toCharArray();
  }

  public static InputStream toInputStream(String paramString)
  {
    return new ByteArrayInputStream(paramString.getBytes());
  }

  public static InputStream toInputStream(String paramString1, String paramString2)
    throws IOException
  {
    if (paramString2 != null);
    for (byte[] arrayOfByte = paramString1.getBytes(paramString2); ; arrayOfByte = paramString1.getBytes())
      return new ByteArrayInputStream(arrayOfByte);
  }

  public static String toString(InputStream paramInputStream)
    throws IOException
  {
    StringWriter localStringWriter = new StringWriter();
    copy(paramInputStream, localStringWriter);
    return localStringWriter.toString();
  }

  public static String toString(InputStream paramInputStream, String paramString)
    throws IOException
  {
    StringWriter localStringWriter = new StringWriter();
    copy(paramInputStream, localStringWriter, paramString);
    return localStringWriter.toString();
  }

  public static String toString(Reader paramReader)
    throws IOException
  {
    StringWriter localStringWriter = new StringWriter();
    copy(paramReader, localStringWriter);
    return localStringWriter.toString();
  }

  public static String toString(byte[] paramArrayOfByte)
    throws IOException
  {
    return new String(paramArrayOfByte);
  }

  public static String toString(byte[] paramArrayOfByte, String paramString)
    throws IOException
  {
    if (paramString == null)
      return new String(paramArrayOfByte);
    return new String(paramArrayOfByte, paramString);
  }

  public static void write(String paramString, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramString == null)
      return;
    paramOutputStream.write(paramString.getBytes());
  }

  public static void write(String paramString1, OutputStream paramOutputStream, String paramString2)
    throws IOException
  {
    if (paramString1 != null)
    {
      if (paramString2 != null)
        break label14;
      write(paramString1, paramOutputStream);
    }
    return;
    label14: paramOutputStream.write(paramString1.getBytes(paramString2));
  }

  public static void write(String paramString, Writer paramWriter)
    throws IOException
  {
    if (paramString == null)
      return;
    paramWriter.write(paramString);
  }

  public static void write(StringBuffer paramStringBuffer, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramStringBuffer == null)
      return;
    paramOutputStream.write(paramStringBuffer.toString().getBytes());
  }

  public static void write(StringBuffer paramStringBuffer, OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    if (paramStringBuffer != null)
    {
      if (paramString != null)
        break label14;
      write(paramStringBuffer, paramOutputStream);
    }
    return;
    label14: paramOutputStream.write(paramStringBuffer.toString().getBytes(paramString));
  }

  public static void write(StringBuffer paramStringBuffer, Writer paramWriter)
    throws IOException
  {
    if (paramStringBuffer == null)
      return;
    paramWriter.write(paramStringBuffer.toString());
  }

  public static void write(byte[] paramArrayOfByte, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramArrayOfByte == null)
      return;
    paramOutputStream.write(paramArrayOfByte);
  }

  public static void write(byte[] paramArrayOfByte, Writer paramWriter)
    throws IOException
  {
    if (paramArrayOfByte == null)
      return;
    paramWriter.write(new String(paramArrayOfByte));
  }

  public static void write(byte[] paramArrayOfByte, Writer paramWriter, String paramString)
    throws IOException
  {
    if (paramArrayOfByte != null)
    {
      if (paramString != null)
        break label14;
      write(paramArrayOfByte, paramWriter);
    }
    return;
    label14: paramWriter.write(new String(paramArrayOfByte, paramString));
  }

  public static void write(char[] paramArrayOfChar, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramArrayOfChar == null)
      return;
    paramOutputStream.write(new String(paramArrayOfChar).getBytes());
  }

  public static void write(char[] paramArrayOfChar, OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    if (paramArrayOfChar != null)
    {
      if (paramString != null)
        break label14;
      write(paramArrayOfChar, paramOutputStream);
    }
    return;
    label14: paramOutputStream.write(new String(paramArrayOfChar).getBytes(paramString));
  }

  public static void write(char[] paramArrayOfChar, Writer paramWriter)
    throws IOException
  {
    if (paramArrayOfChar == null)
      return;
    paramWriter.write(paramArrayOfChar);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.IOUtils
 * JD-Core Version:    0.5.4
 */