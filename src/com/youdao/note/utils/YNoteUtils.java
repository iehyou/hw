package com.youdao.note.utils;

import android.text.TextUtils;
import com.youdao.note.data.resource.BaseResourceMeta;
import com.youdao.note.data.resource.ResourceUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class YNoteUtils
  implements EmptyInstance, Consts
{
  private static final char PATH_SEP = '/';
  private static final Pattern RESOURCE_PATTERN = Pattern.compile("http://([^/]+/)*res/(\\d+)/([\\w\\d]+)");

  public static BaseResourceMeta extractResource(String paramString, int paramInt)
  {
    if (TextUtils.isEmpty(paramString))
      return null;
    Matcher localMatcher = RESOURCE_PATTERN.matcher(paramString);
    try
    {
      if (!localMatcher.find())
        break label98;
      BaseResourceMeta localBaseResourceMeta = ResourceUtils.newResourceMeta(paramInt);
      int i = localMatcher.groupCount();
      localBaseResourceMeta.setVersion(Integer.parseInt(localMatcher.group(i - 1)));
      localBaseResourceMeta.setResourceId(localMatcher.group(i));
      localBaseResourceMeta.setDirty(false);
      return localBaseResourceMeta;
    }
    catch (Exception localException)
    {
      L.e(YNoteUtils.class, "Failed to parse resource " + paramString, localException);
    }
    label98: return null;
  }

  public static Map<String, Object> json2Map(JSONObject paramJSONObject)
    throws JSONException
  {
    HashMap localHashMap = new HashMap(2 * paramJSONObject.length());
    Iterator localIterator = paramJSONObject.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localHashMap.put(str, paramJSONObject.get(str));
    }
    return localHashMap;
  }

  public static String[] splitEntryPath(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      throw new IllegalArgumentException("Not valid entry path = " + paramString);
    int i = paramString.lastIndexOf('/');
    if (paramString.trim().equals("/"))
      return EMPTY_STRINGS2;
    if (i == 0)
    {
      String[] arrayOfString2 = new String[2];
      arrayOfString2[0] = paramString.substring(1);
      arrayOfString2[1] = "";
      return arrayOfString2;
    }
    String[] arrayOfString1 = new String[2];
    arrayOfString1[0] = paramString.substring(1, i);
    arrayOfString1[1] = paramString.substring(i + 1);
    return arrayOfString1;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.YNoteUtils
 * JD-Core Version:    0.5.4
 */