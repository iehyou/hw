package com.youdao.note.utils;

public abstract interface Consts
{
  public static final String ARROW_ASSET = "file:///android_asset/arrow.png";
  public static final String ASSET_PREFIX = "file:///android_asset/";
  public static final String CMWAP_HOST = "10.0.0.172";
  public static final long DEBUG_MAX_RESOURCE_SIZE = 102400L;
  public static final String DOWNLOAD_ASSET = "file:///android_asset/download.png";
  public static final String JUHUA_ASSET = "file:///android_asset/juhua.gif";
  public static final long MAX_RESOURCE_SIZE = 25165824L;
  public static final String ROOT_NOTEBOOK = "/";
  public static final String SUMMARY_PREFIX = "                       ";

  public static abstract interface APIS
  {
    public static final int DEFAULT_BASE_VERSION = -1;
    public static final int DEFAULT_BEGIN = 0;
    public static final int DEFAULT_DEPTH = 0;
    public static final int DEFAULT_DIGEST_LENGTH = 100;
    public static final int DEFAULT_HEIGHT = 0;
    public static final int DEFAULT_LENGTH = 1000;
    public static final int DEFAULT_LIST_MODE = 0;
    public static final String DEFAULT_PATH = "/";
    public static final boolean DEFAULT_THIN = false;
    public static final long DEFAULT_THRH_SIZE = 6144L;
    public static final long DEFAULT_TIME_PARAMETER = -1L;
    public static final int DEFAULT_VERSION = -1;
    public static final int DEFAULT_WIDTH = 0;
    public static final int EMAIL_RECEIVER_LIMIT = 3;
    public static final String HOST = "note.youdao.com/";
    public static final String HOST_DEBUG = "note.corp.youdao.com/";
    public static final String HTTP = "http://";
    public static final String LOGIN_URL = "http://reg.163.com/services/userlogin?username=%s&password=%s&type=1&userip=%s&product=note&savelogin=1&passtype=0&back_url=http://www.youdao.com";
    public static final String METHOD_CLEAR = "clear";
    public static final String METHOD_GET = "get";
    public static final String METHOD_GETRES = "getres";
    public static final String METHOD_MOVE = "mv";
    public static final String METHOD_PUT = "put";
    public static final String METHOD_PUTFILE = "putfile";
    public static final String METHOD_PUTRES = "putres";
    public static final String METHOD_RECOVER = "recover";
    public static final String METHOD_RM = "rm";
    public static final String METHOD_SENDMAIL = "mail";
    public static final String METHOD_THM = "thm";
    public static final String METHOD_UPDATE = "update";
    public static final String NAME_AUTHOR = "au";
    public static final String NAME_BASE_VERSION = "bv";
    public static final String NAME_BEGIN = "b";
    public static final String NAME_BYTE_STREAM = "bs";
    public static final String NAME_CREATE_TIME_BEGIN = "cb";
    public static final String NAME_CRERTE_TIME_END = "ce";
    public static final String NAME_DEL = "del";
    public static final String NAME_DEPTH = "dp";
    public static final String NAME_DEST_PATH = "dst";
    public static final String NAME_FILE_ID = "fid";
    public static final String NAME_HEIGHT = "h";
    public static final String NAME_ISDIR = "r";
    public static final String NAME_KEY_WORD = "kw";
    public static final String NAME_LENGTH = "l";
    public static final String NAME_LIST_MODE = "m";
    public static final String NAME_MAIL_SHARE_MESSAGE = "msg";
    public static final String NAME_MAIL_SHARE_RECEIVERS = "to";
    public static final String NAME_MAIL_SHARE_SUBJECT = "subject";
    public static final String NAME_METHOD_PARAM = "method";
    public static final String NAME_MODIFY_TIME_END = "me";
    public static final String NAME_MODITY_TIME_BEGIN = "mb";
    public static final String NAME_NEED_SUMMARY = "dg";
    public static final String NAME_NOTE_BOOK = "nb";
    public static final String NAME_PATH = "p";
    public static final String NAME_PATH_LIST = "ps";
    public static final String NAME_RESOURECE_ID = "fid";
    public static final String NAME_SOURCE = "src";
    public static final String NAME_SUMMARY_LEN = "dn";
    public static final String NAME_THIN = "th";
    public static final String NAME_THM = "thm";
    public static final String NAME_THRH = "thr";
    public static final String NAME_TITLE = "tl";
    public static final String NAME_VERSION = "v";
    public static final String NAME_WIDTH = "w";
    public static final String PATH_FILE = "file";
    public static final String PATH_FILEMETA = "filemeta";
    public static final String PATH_LIST = "list";
    public static final String PATH_LOG = "ilogrpt";
    public static final String PATH_PREFIX = "yws/mapi/";
    public static final String PATH_SEARCH = "search";
    public static final String PATH_SHARE = "share";
    public static final String PATH_TRASH = "trash";
    public static final String PATH_USER = "user";
    public static final String RES_PREFIX = "yws/res/";
  }

  public static abstract interface DATA_NAME
  {
    public static final String AUTHOR = "au";
    public static final String CREATE_TIME = "ct";
    public static final String DEFAULT_NOTE_BOOK = "df";
    public static final String ENTRY_PATH = "p";
    public static final String EXT_PROP = "pp";
    public static final String IS_DEL = "del";
    public static final String IS_DIR = "dr";
    public static final String LAST_LOGIN_TIME = "ll";
    public static final String LAST_PUSH_TIME = "lp";
    public static final String LENGTH = "sz";
    public static final String MODIFY_TIME = "mt";
    public static final String NOTE_NUMBER = "nn";
    public static final String QUTA_SPACE = "q";
    public static final String SOURCE_URL = "su";
    public static final String TAGS = "tg";
    public static final String TITLE = "tl";
    public static final String USED_SPACE = "u";
    public static final String VERSION = "v";
  }

  public static abstract interface DATA_TYPE
  {
    public static final int ADD_RESOURCE = 18;
    public static final int ADD_RESOURCE_FINISH = 19;
    public static final int AUTO_NOTE_REFRESH = 15;
    public static final int CHECK_NEED_UPDATE = 20;
    public static final int CHECK_VERSION_UPDATE = 21;
    public static final int DATA_TYPE_LIST = 1;
    public static final int DATA_TYPE_NOTE = 2;
    public static final int DATA_TYPE_NOTE_BOOK = 3;
    public static final int DATA_TYPE_THUMBNAIL = 5;
    public static final int DATA_TYPE_USER = 4;
    public static final int DEL_RESULT = 12;
    public static final int DOWNLOAD_PROGRESS = 8;
    public static final int DOWNLOAD_RESULT = 9;
    public static final int FILEMETA_UPDATE = 16;
    public static final int LOGIN_RESULT = 11;
    public static final int MAIL_SHARE = 17;
    public static final int NOTE_META = 14;
    public static final int SEARCH_RESULT = 10;
    public static final int SYNC_PROGRESS = 7;
    public static final int SYNC_RESULT = 6;
    public static final int USERMETA_RESULT = 13;
  }

  public static abstract interface HTML
  {
    public static final String EDITABLE_HTML_HEAD = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"editor2.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>";
    public static final String HTML_ENCODING = "utf-8";
    public static final String HTML_FOOTER = "</body></html>";
    public static final String HTML_HEAD = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"detail.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>";
    public static final String HTML_TYPE = "text/html";
  }

  public static abstract interface Request
  {
    public static final String COOKIE_FILED = "cookie";
    public static final String COOKIE_PREFIX = "cookie=";
    public static final String NTES_PASSPORT = "NTES_PASSPORT";
    public static final String NTES_SESSION = "NTES_SESS";
    public static final String SESSION_COOKIE_FILED = "X-Set-NTES-SESS";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.Consts
 * JD-Core Version:    0.5.4
 */