package com.youdao.note.utils;

public class IdUtils
{
  private static final String salt = "note.youdao.com/android/";

  public static String genNoteId()
  {
    return MD5Digester.digest("note.youdao.com/android/note/" + System.currentTimeMillis());
  }

  public static String genNotebookId()
  {
    return MD5Digester.digest("note.youdao.com/android/notebook/" + System.currentTimeMillis());
  }

  public static String genPinCode(int paramInt)
  {
    return MD5Digester.digest("note.youdao.com/android/pincode/" + paramInt);
  }

  public static String genResourceId(String paramString)
  {
    return MD5Digester.digest("note.youdao.com/android/resource/" + paramString + "/" + System.currentTimeMillis());
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.IdUtils
 * JD-Core Version:    0.5.4
 */