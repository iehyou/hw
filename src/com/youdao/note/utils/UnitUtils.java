package com.youdao.note.utils;

public class UnitUtils
{
  public static final long BYTE = 1L;
  public static final long DAY = 86400000L;
  public static final long GBYTE = 1073741824L;
  public static final long HOUR = 3600000L;
  public static final long KBYTE = 1024L;
  public static final long MBYTE = 1048576L;
  public static final long MINUTE = 60000L;
  public static final long SECOND = 1000L;

  public static String getDuration(long paramLong)
  {
    if (paramLong / 86400000L > 0L)
      return "Baby, STOP!";
    int i = (int)(paramLong / 3600000L);
    int j = (int)(paramLong % 3600000L / 60000L);
    int k = (int)(paramLong % 60000L / 1000L);
    if (i == 0)
      return toStr(j) + ":" + toStr(k);
    return toStr(i) + ":" + toStr(j) + ":" + toStr(k);
  }

  public static String getSize(long paramLong)
  {
    if (paramLong < 1024L)
      return paramLong + "B";
    if (paramLong < 1048576L)
      return paramLong / 1024L + "KB";
    if (paramLong < 1073741824L)
      return paramLong / 1048576L + "MB";
    return paramLong / 1073741824L + "GB";
  }

  public static String getSize(long paramLong, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder().append(getSize(paramLong));
    if (paramInt >= 100);
    for (String str = ""; ; str = "  --" + paramInt + "%")
      return str;
  }

  public static double getSizeInMegaByte(long paramLong)
  {
    return 1.0D * paramLong / 1048576.0D;
  }

  private static String toStr(int paramInt)
  {
    if (paramInt / 10 == 0)
      return "0" + paramInt;
    return Integer.toString(paramInt);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.UnitUtils
 * JD-Core Version:    0.5.4
 */