package com.youdao.note.utils;

import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils
{
  private static final SimpleDateFormat currentTimeFormator;
  public static final SimpleDateFormat formator;
  public static final SimpleDateFormat formator2;
  private static final SimpleDateFormat formatorDetail = new SimpleDateFormat("yyyy-MM-dd HH:mm");
  private static final Pattern rfc2822;

  static
  {
    formator = new SimpleDateFormat("yyyy-MM");
    formator2 = new SimpleDateFormat("yyyy-MM-dd");
    currentTimeFormator = new SimpleDateFormat("yyyyMMdd-HHMMss");
    rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
  }

  public static String currentTimeStr()
  {
    synchronized (currentTimeFormator)
    {
      String str = currentTimeFormator.format(new Date(System.currentTimeMillis()));
      return str;
    }
  }

  public static String getPrettyTime(long paramLong)
  {
    synchronized (formatorDetail)
    {
      String str = formatorDetail.format(new Date(paramLong));
      return str;
    }
  }

  public static String getPrettyTime2(long paramLong)
  {
    synchronized (formator2)
    {
      String str = formator2.format(new Date(paramLong));
      return str;
    }
  }

  public static String getPrettyTimeDetail(long paramLong)
  {
    synchronized (formatorDetail)
    {
      String str = formatorDetail.format(new Date(paramLong));
      return str;
    }
  }

  public static boolean isBlank(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      return true;
    for (int i = 0; ; ++i)
    {
      if (i < paramString.length());
      if (paramString.charAt(i) != ' ')
        return false;
    }
  }

  public static boolean isEmail(String paramString)
  {
    if (TextUtils.isEmpty(paramString));
    do
      return false;
    while (!rfc2822.matcher(paramString.toLowerCase()).matches());
    return true;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.StringUtils
 * JD-Core Version:    0.5.4
 */