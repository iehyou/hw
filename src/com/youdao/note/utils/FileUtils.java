package com.youdao.note.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.youdao.note.YNoteApplication;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FileUtils
  implements Consts
{
  private static final String DEFAULT_FILE_ICON = "else.png";
  private static Set<String> audioSuffix;
  private static Set<String> compressSuffix;
  private static Map<String, Integer> drawableMap;
  private static Set<String> flashSuffix;
  private static Map<String, String> iconMap = null;
  private static Set<String> imageSuffix;
  private static Set<String> pageSuffix;
  private static final Set<String> sPlayAble;
  private static Set<String> videoSuffix;

  static
  {
    drawableMap = null;
    sPlayAble = new HashSet();
    drawableMap = new HashMap();
    imageSuffix = new HashSet();
    imageSuffix.add("jpg");
    imageSuffix.add("jpeg");
    imageSuffix.add("bmp");
    imageSuffix.add("gif");
    imageSuffix.add("psd");
    imageSuffix.add("ia");
    imageSuffix.add("png");
    sPlayAble.add("mp3");
    sPlayAble.add("amr");
    sPlayAble.add("3gpp");
    sPlayAble.add("flac");
    drawableMap.put("mp3", Integer.valueOf(2130837515));
    drawableMap.put("aac", Integer.valueOf(2130837515));
    drawableMap.put("ogg", Integer.valueOf(2130837515));
    drawableMap.put("wav", Integer.valueOf(2130837515));
    drawableMap.put("amr", Integer.valueOf(2130837515));
    drawableMap.put("3gpp", Integer.valueOf(2130837515));
    drawableMap.put("flac", Integer.valueOf(2130837515));
    drawableMap.put("m4a", Integer.valueOf(2130837515));
    drawableMap.put("ape", Integer.valueOf(2130837515));
    audioSuffix = new HashSet();
    audioSuffix.add("mp3");
    audioSuffix.add("aac");
    audioSuffix.add("ogg");
    audioSuffix.add("wav");
    audioSuffix.add("flac");
    audioSuffix.add("m4a");
    audioSuffix.add("ape");
    audioSuffix.add("amr");
    videoSuffix = new HashSet();
    videoSuffix.add("rm");
    videoSuffix.add("rmvb");
    videoSuffix.add("avi");
    videoSuffix.add("mkv");
    videoSuffix.add("mpg");
    videoSuffix.add("mpeg");
    videoSuffix.add("wmv");
    videoSuffix.add("ts");
    videoSuffix.add("m4v");
    videoSuffix.add("mp4");
    videoSuffix.add("wma");
    videoSuffix.add("3gp");
    drawableMap.put("rm", Integer.valueOf(2130837756));
    drawableMap.put("rmvb", Integer.valueOf(2130837756));
    drawableMap.put("avi", Integer.valueOf(2130837756));
    drawableMap.put("mkv", Integer.valueOf(2130837756));
    drawableMap.put("mpg", Integer.valueOf(2130837756));
    drawableMap.put("mpeg", Integer.valueOf(2130837756));
    drawableMap.put("wmv", Integer.valueOf(2130837756));
    drawableMap.put("ts", Integer.valueOf(2130837756));
    drawableMap.put("m4v", Integer.valueOf(2130837756));
    drawableMap.put("mp4", Integer.valueOf(2130837756));
    drawableMap.put("wma", Integer.valueOf(2130837756));
    drawableMap.put("3gp", Integer.valueOf(2130837756));
    flashSuffix = new HashSet(2);
    flashSuffix.add("swf");
    flashSuffix.add("flv");
    drawableMap.put("swf", Integer.valueOf(2130837586));
    drawableMap.put("flv", Integer.valueOf(2130837586));
    compressSuffix = new HashSet(3);
    compressSuffix.add("zip");
    compressSuffix.add("rar");
    compressSuffix.add("7zp");
    drawableMap.put("zip", Integer.valueOf(2130837704));
    drawableMap.put("rar", Integer.valueOf(2130837704));
    drawableMap.put("7zp", Integer.valueOf(2130837704));
    pageSuffix = new HashSet();
    pageSuffix.add("html");
    pageSuffix.add("htm");
    pageSuffix.add("mht");
    drawableMap.put("html", Integer.valueOf(2130837529));
    drawableMap.put("htm", Integer.valueOf(2130837529));
    drawableMap.put("mht", Integer.valueOf(2130837529));
    iconMap = new HashMap(8);
    iconMap.put("pdf", "pdf64.png");
    iconMap.put("doc", "word.png");
    iconMap.put("docx", "word.png");
    iconMap.put("ppt", "powerpoint.png");
    iconMap.put("pptx", "powerpoint.png");
    iconMap.put("txt", "txt.png");
    iconMap.put("xls", "excel.png");
    iconMap.put("xlsx", "excel.png");
    drawableMap.put("pdf", Integer.valueOf(2130837694));
    drawableMap.put("doc", Integer.valueOf(2130837771));
    drawableMap.put("docx", Integer.valueOf(2130837771));
    drawableMap.put("ppt", Integer.valueOf(2130837700));
    drawableMap.put("pptx", Integer.valueOf(2130837700));
    drawableMap.put("txt", Integer.valueOf(2130837752));
    drawableMap.put("xls", Integer.valueOf(2130837584));
    drawableMap.put("xlsx", Integer.valueOf(2130837584));
    addIcons(audioSuffix, "audio.png");
    addIcons(videoSuffix, "video.png");
    addIcons(flashSuffix, "swf.png");
    addIcons(compressSuffix, "rar.png");
    addIcons(pageSuffix, "html.png");
  }

  private static void addIcons(Set<String> paramSet, String paramString)
  {
    Iterator localIterator = paramSet.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      iconMap.put(str, paramString);
    }
  }

  // ERROR //
  public static void copyFile(Uri paramUri, String paramString)
    throws IOException
  {
    // Byte code:
    //   0: aload_1
    //   1: aload_0
    //   2: invokevirtual 223	android/net/Uri:getPath	()Ljava/lang/String;
    //   5: invokevirtual 226	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   8: ifeq +11 -> 19
    //   11: ldc 2
    //   13: ldc 228
    //   15: invokestatic 234	com/youdao/note/utils/L:d	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: return
    //   19: invokestatic 240	com/youdao/note/YNoteApplication:getInstance	()Lcom/youdao/note/YNoteApplication;
    //   22: invokevirtual 244	com/youdao/note/YNoteApplication:getContentResolver	()Landroid/content/ContentResolver;
    //   25: aload_0
    //   26: ldc 246
    //   28: invokevirtual 252	android/content/ContentResolver:openFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    //   31: invokevirtual 258	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
    //   34: astore_2
    //   35: aconst_null
    //   36: astore_3
    //   37: aconst_null
    //   38: astore 4
    //   40: new 260	java/io/FileInputStream
    //   43: dup
    //   44: aload_2
    //   45: invokespecial 263	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
    //   48: astore 5
    //   50: new 265	java/io/File
    //   53: dup
    //   54: aload_1
    //   55: invokespecial 268	java/io/File:<init>	(Ljava/lang/String;)V
    //   58: astore 6
    //   60: aload 6
    //   62: invokevirtual 271	java/io/File:exists	()Z
    //   65: ifne +9 -> 74
    //   68: aload 6
    //   70: invokestatic 275	com/youdao/note/utils/FileUtils:createNewFile	(Ljava/io/File;)Z
    //   73: pop
    //   74: new 277	java/io/FileOutputStream
    //   77: dup
    //   78: aload 6
    //   80: invokespecial 280	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   83: astore 14
    //   85: aload 5
    //   87: invokevirtual 284	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   90: astore_3
    //   91: aload 14
    //   93: invokevirtual 285	java/io/FileOutputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   96: astore 4
    //   98: sipush 1024
    //   101: invokestatic 291	java/nio/ByteBuffer:allocate	(I)Ljava/nio/ByteBuffer;
    //   104: astore 15
    //   106: aload 15
    //   108: invokevirtual 295	java/nio/ByteBuffer:clear	()Ljava/nio/Buffer;
    //   111: pop
    //   112: aload_3
    //   113: aload 15
    //   115: invokevirtual 301	java/nio/channels/FileChannel:read	(Ljava/nio/ByteBuffer;)I
    //   118: istore 17
    //   120: iload 17
    //   122: iconst_m1
    //   123: if_icmpne +45 -> 168
    //   126: aload_3
    //   127: ifnull +7 -> 134
    //   130: aload_3
    //   131: invokevirtual 304	java/nio/channels/FileChannel:close	()V
    //   134: aload 4
    //   136: ifnull +8 -> 144
    //   139: aload 4
    //   141: invokevirtual 304	java/nio/channels/FileChannel:close	()V
    //   144: aload 5
    //   146: ifnull +8 -> 154
    //   149: aload 5
    //   151: invokevirtual 305	java/io/FileInputStream:close	()V
    //   154: aload 14
    //   156: ifnull -138 -> 18
    //   159: aload 14
    //   161: invokevirtual 306	java/io/FileOutputStream:close	()V
    //   164: return
    //   165: astore 20
    //   167: return
    //   168: aload 15
    //   170: invokevirtual 309	java/nio/ByteBuffer:flip	()Ljava/nio/Buffer;
    //   173: pop
    //   174: aload 4
    //   176: aload 15
    //   178: invokevirtual 312	java/nio/channels/FileChannel:write	(Ljava/nio/ByteBuffer;)I
    //   181: pop
    //   182: goto -76 -> 106
    //   185: astore 7
    //   187: aload 14
    //   189: astore 9
    //   191: aload 5
    //   193: astore 8
    //   195: aload_3
    //   196: ifnull +7 -> 203
    //   199: aload_3
    //   200: invokevirtual 304	java/nio/channels/FileChannel:close	()V
    //   203: aload 4
    //   205: ifnull +8 -> 213
    //   208: aload 4
    //   210: invokevirtual 304	java/nio/channels/FileChannel:close	()V
    //   213: aload 8
    //   215: ifnull +8 -> 223
    //   218: aload 8
    //   220: invokevirtual 305	java/io/FileInputStream:close	()V
    //   223: aload 9
    //   225: ifnull +8 -> 233
    //   228: aload 9
    //   230: invokevirtual 306	java/io/FileOutputStream:close	()V
    //   233: aload 7
    //   235: athrow
    //   236: astore 23
    //   238: goto -104 -> 134
    //   241: astore 22
    //   243: goto -99 -> 144
    //   246: astore 21
    //   248: goto -94 -> 154
    //   251: astore 13
    //   253: goto -50 -> 203
    //   256: astore 12
    //   258: goto -45 -> 213
    //   261: astore 11
    //   263: goto -40 -> 223
    //   266: astore 10
    //   268: goto -35 -> 233
    //   271: astore 7
    //   273: aconst_null
    //   274: astore 8
    //   276: aconst_null
    //   277: astore 9
    //   279: aconst_null
    //   280: astore_3
    //   281: aconst_null
    //   282: astore 4
    //   284: goto -89 -> 195
    //   287: astore 7
    //   289: aload 5
    //   291: astore 8
    //   293: aconst_null
    //   294: astore 9
    //   296: aconst_null
    //   297: astore_3
    //   298: aconst_null
    //   299: astore 4
    //   301: goto -106 -> 195
    //
    // Exception table:
    //   from	to	target	type
    //   159	164	165	java/io/IOException
    //   85	106	185	finally
    //   106	120	185	finally
    //   168	182	185	finally
    //   130	134	236	java/io/IOException
    //   139	144	241	java/io/IOException
    //   149	154	246	java/io/IOException
    //   199	203	251	java/io/IOException
    //   208	213	256	java/io/IOException
    //   218	223	261	java/io/IOException
    //   228	233	266	java/io/IOException
    //   40	50	271	finally
    //   50	74	287	finally
    //   74	85	287	finally
  }

  public static boolean createNewFile(File paramFile)
    throws IOException
  {
    File localFile = paramFile.getParentFile();
    if ((localFile != null) && (!localFile.isDirectory()))
    {
      localFile.delete();
      localFile.mkdirs();
    }
    return paramFile.createNewFile();
  }

  public static boolean deleteDirectory(File paramFile)
  {
    if (paramFile.exists())
    {
      File[] arrayOfFile = paramFile.listFiles();
      if (arrayOfFile != null)
      {
        int i = arrayOfFile.length;
        for (int j = 0; j < i; ++j)
          deleteDirectory(arrayOfFile[j]);
      }
    }
    return deleteFile(paramFile);
  }

  public static boolean deleteDirectory(String paramString)
  {
    return deleteDirectory(new File(paramString));
  }

  public static boolean deleteFile(File paramFile)
  {
    if (paramFile.exists())
      return paramFile.delete();
    return true;
  }

  public static boolean deleteFile(String paramString)
  {
    File localFile1 = new File(paramString);
    File localFile2 = localFile1.getParentFile();
    if (deleteFile(localFile1))
    {
      String[] arrayOfString = localFile2.list();
      if ((arrayOfString != null) && (arrayOfString.length == 0))
        return deleteFile(localFile2);
      return true;
    }
    return false;
  }

  public static boolean exist(Uri paramUri)
  {
    ContentResolver localContentResolver = YNoteApplication.getInstance().getContentResolver();
    try
    {
      localContentResolver.openFileDescriptor(paramUri, "r");
      return true;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
    }
    return false;
  }

  public static boolean exist(String paramString)
  {
    return new File(paramString).exists();
  }

  public static String getArrowIconPath()
  {
    return "file:///android_asset/arrow.png";
  }

  public static Bitmap getBitmapFromFileDescriptor(FileDescriptor paramFileDescriptor)
    throws FileNotFoundException
  {
    monitorenter;
    BitmapFactory.Options localOptions;
    Bitmap localBitmap;
    for (int i = 1600; ; i = 640)
      while (true)
      {
        try
        {
          switch (YNoteApplication.getInstance().getImageQuality())
          {
          default:
            localOptions = new BitmapFactory.Options();
            localOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(paramFileDescriptor, null, localOptions);
            localOptions.inSampleSize = (int)Math.min(Math.floor(localOptions.outWidth / i), Math.floor(localOptions.outHeight / i));
            localOptions.inJustDecodeBounds = false;
            localOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            localOptions.inPurgeable = true;
            localOptions.inInputShareable = true;
            localBitmap = BitmapFactory.decodeFileDescriptor(paramFileDescriptor, null, localOptions);
            throw new FileNotFoundException();
          case 3:
          case 2:
          case 1:
          }
        }
        finally
        {
          monitorexit;
        }
        i = 1200;
        continue;
        i = 800;
      }
    localBitmap.setHasAlpha(true);
    if ((localOptions.outHeight > i) || (localOptions.outWidth > i))
    {
      localBitmap = ImageUtils.compressImage(localBitmap, i, i);
      localBitmap.setHasAlpha(true);
    }
    monitorexit;
    return localBitmap;
  }

  public static Bitmap getBitmapFromUri(Uri paramUri)
    throws FileNotFoundException
  {
    return getBitmapFromFileDescriptor(YNoteApplication.getInstance().getContentResolver().openFileDescriptor(paramUri, "r").getFileDescriptor());
  }

  public static String getFileExtension(String paramString)
  {
    if ((paramString != null) && (paramString.length() > 0))
    {
      int i = paramString.lastIndexOf('.');
      if ((i > -1) && (i < -1 + paramString.length()))
        return paramString.substring(i + 1);
    }
    return "";
  }

  public static String getFileNameFromUri(Uri paramUri)
  {
    if (paramUri.toString().startsWith("content"))
    {
      ContentResolver localContentResolver = YNoteApplication.getInstance().getContentResolver();
      String[] arrayOfString = { "_display_name" };
      Cursor localCursor = null;
      try
      {
        localCursor = localContentResolver.query(paramUri, arrayOfString, null, null, null);
        int i = localCursor.getColumnIndexOrThrow("_display_name");
        if (localCursor.moveToFirst())
        {
          String str2 = localCursor.getString(i);
          localObject2 = str2;
          return localObject2;
        }
        String str1 = getFileNameFromUrl(paramUri.toString());
        Object localObject2 = str1;
        return localObject2;
      }
      finally
      {
        if (localCursor != null)
          localCursor.close();
      }
    }
    return (String)getFileNameFromUrl(paramUri.toString());
  }

  public static String getFileNameFromUrl(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      return "";
    String str = URLDecoder.decode(paramString);
    int i = str.lastIndexOf("/");
    if (i >= 0)
      return str.substring(i + 1);
    return "";
  }

  public static long getFileSize(Uri paramUri)
  {
    ContentResolver localContentResolver;
    String[] arrayOfString;
    Cursor localCursor;
    if (paramUri.toString().startsWith("content"))
    {
      localContentResolver = YNoteApplication.getInstance().getContentResolver();
      arrayOfString = new String[] { "_size" };
      localCursor = null;
    }
    try
    {
      localCursor = localContentResolver.query(paramUri, arrayOfString, null, null, null);
      int i = localCursor.getColumnIndexOrThrow("_size");
      if (localCursor.moveToFirst())
      {
        long l = localCursor.getLong(i);
        return l;
      }
      return new File(paramUri.getPath()).length();
    }
    finally
    {
      if (localCursor != null)
        localCursor.close();
    }
  }

  public static int getIconResouceId(String paramString)
  {
    String str = getSuffix(paramString);
    Integer localInteger = (Integer)drawableMap.get(str);
    if (localInteger != null)
      return localInteger.intValue();
    return 2130837753;
  }

  public static String getResourceIconName(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      return "else.png";
    String str = getSuffix(paramString);
    if (iconMap.containsKey(str))
      return (String)iconMap.get(str);
    return "else.png";
  }

  public static String getResourceIconPath(String paramString)
  {
    return "file:///android_asset/" + getResourceIconName(paramString);
  }

  private static String getSuffix(String paramString)
  {
    int i = paramString.lastIndexOf(".");
    if (i > 0)
      return paramString.substring(i + 1).toLowerCase();
    return "";
  }

  public static boolean isAudio(Uri paramUri)
  {
    return isAudio(getFileNameFromUri(paramUri));
  }

  public static boolean isAudio(String paramString)
  {
    return audioSuffix.contains(getSuffix(paramString));
  }

  public static boolean isCompressFile(Uri paramUri)
  {
    return isCompressFile(getFileNameFromUri(paramUri));
  }

  public static boolean isCompressFile(String paramString)
  {
    return compressSuffix.contains(getSuffix(paramString));
  }

  public static boolean isImage(Uri paramUri)
  {
    return isImage(getFileNameFromUri(paramUri));
  }

  public static boolean isImage(String paramString)
  {
    return imageSuffix.contains(getSuffix(paramString));
  }

  public static boolean isPlayable(Uri paramUri)
  {
    return isPlayable(getFileNameFromUri(paramUri));
  }

  public static boolean isPlayable(String paramString)
  {
    return sPlayAble.contains(getSuffix(paramString));
  }

  public static boolean isVideoFile(Uri paramUri)
  {
    return isVideoFile(getFileNameFromUri(paramUri));
  }

  public static boolean isVideoFile(String paramString)
  {
    return videoSuffix.contains(getSuffix(paramString));
  }

  public static FileDescriptor openFileDescriptor(Uri paramUri, String paramString)
    throws FileNotFoundException
  {
    return YNoteApplication.getInstance().getContentResolver().openFileDescriptor(paramUri, paramString).getFileDescriptor();
  }

  public static FileDescriptor openFileDescriptor(String paramString1, String paramString2)
    throws FileNotFoundException
  {
    return openFileDescriptor(Uri.fromFile(new File(paramString1)), paramString2);
  }

  public static byte[] readFromFileAsBytes(String paramString)
    throws IOException
  {
    File localFile = new File(paramString);
    if (!localFile.exists())
      return null;
    return IOUtils.toByteArray(new FileInputStream(localFile));
  }

  public static String readFromFileAsStr(String paramString)
    throws IOException
  {
    File localFile = new File(paramString);
    if (!localFile.exists())
      return null;
    return IOUtils.toString(new FileInputStream(localFile));
  }

  public static void saveToFile(String paramString1, String paramString2)
    throws IOException
  {
    saveToFile(paramString1, paramString2.getBytes());
  }

  public static void saveToFile(String paramString, byte[] paramArrayOfByte)
    throws IOException
  {
    File localFile = new File(paramString);
    if (localFile.exists())
      localFile.delete();
    createNewFile(localFile);
    FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
    localFileOutputStream.write(paramArrayOfByte);
    localFileOutputStream.flush();
    localFileOutputStream.close();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.FileUtils
 * JD-Core Version:    0.5.4
 */