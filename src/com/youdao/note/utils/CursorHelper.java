package com.youdao.note.utils;

import android.database.Cursor;

public class CursorHelper
{
  private Cursor mCursor = null;

  public CursorHelper(Cursor paramCursor)
  {
    this.mCursor = paramCursor;
  }

  public boolean getBoolean(String paramString)
  {
    return this.mCursor.getInt(this.mCursor.getColumnIndexOrThrow(paramString)) != 0;
  }

  public int getInt(String paramString)
  {
    return this.mCursor.getInt(this.mCursor.getColumnIndexOrThrow(paramString));
  }

  public long getLong(String paramString)
  {
    return this.mCursor.getLong(this.mCursor.getColumnIndexOrThrow(paramString));
  }

  public String getString(String paramString)
  {
    return this.mCursor.getString(this.mCursor.getColumnIndexOrThrow(paramString));
  }

  public boolean moveToNext()
  {
    return this.mCursor.moveToNext();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.youdao.note.utils.CursorHelper
 * JD-Core Version:    0.5.4
 */