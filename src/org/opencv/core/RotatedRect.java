package org.opencv.core;

public class RotatedRect
{
  public double angle;
  public Point center;
  public Size size;

  public RotatedRect()
  {
    this.center = new Point();
    this.size = new Size();
    this.angle = 0.0D;
  }

  public RotatedRect(Point paramPoint, Size paramSize, double paramDouble)
  {
    this.center = paramPoint.clone();
    this.size = paramSize.clone();
    this.angle = paramDouble;
  }

  public RotatedRect(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public Rect boundingRect()
  {
    Point[] arrayOfPoint = new Point[4];
    points(arrayOfPoint);
    Rect localRect = new Rect((int)Math.floor(Math.min(Math.min(Math.min(arrayOfPoint[0].x, arrayOfPoint[1].x), arrayOfPoint[2].x), arrayOfPoint[3].x)), (int)Math.floor(Math.min(Math.min(Math.min(arrayOfPoint[0].y, arrayOfPoint[1].y), arrayOfPoint[2].y), arrayOfPoint[3].y)), (int)Math.ceil(Math.max(Math.max(Math.max(arrayOfPoint[0].x, arrayOfPoint[1].x), arrayOfPoint[2].x), arrayOfPoint[3].x)), (int)Math.ceil(Math.max(Math.max(Math.max(arrayOfPoint[0].y, arrayOfPoint[1].y), arrayOfPoint[2].y), arrayOfPoint[3].y)));
    localRect.width -= -1 + localRect.x;
    localRect.height -= -1 + localRect.y;
    return localRect;
  }

  public RotatedRect clone()
  {
    return new RotatedRect(this.center, this.size, this.angle);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    RotatedRect localRotatedRect;
    do
    {
      return true;
      if (!paramObject instanceof RotatedRect)
        return false;
      localRotatedRect = (RotatedRect)paramObject;
    }
    while ((this.center.equals(localRotatedRect.center)) && (this.size.equals(localRotatedRect.size)) && (this.angle == localRotatedRect.angle));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.center.x);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.center.y);
    int j = i * 31 + (int)(l2 ^ l2 >>> 32);
    long l3 = Double.doubleToLongBits(this.size.width);
    int k = j * 31 + (int)(l3 ^ l3 >>> 32);
    long l4 = Double.doubleToLongBits(this.size.height);
    int l = k * 31 + (int)(l4 ^ l4 >>> 32);
    long l5 = Double.doubleToLongBits(this.angle);
    return l * 31 + (int)(l5 ^ l5 >>> 32);
  }

  public void points(Point[] paramArrayOfPoint)
  {
    double d1 = 3.141592653589793D * this.angle / 180.0D;
    double d2 = 0.5D * Math.cos(d1);
    double d3 = 0.5D * Math.sin(d1);
    paramArrayOfPoint[0] = new Point(this.center.x - d3 * this.size.height - d2 * this.size.width, this.center.y + d2 * this.size.height - d3 * this.size.width);
    paramArrayOfPoint[1] = new Point(this.center.x + d3 * this.size.height - d2 * this.size.width, this.center.y - d2 * this.size.height - d3 * this.size.width);
    paramArrayOfPoint[2] = new Point(2.0D * this.center.x - paramArrayOfPoint[0].x, 2.0D * this.center.y - paramArrayOfPoint[0].y);
    paramArrayOfPoint[3] = new Point(2.0D * this.center.x - paramArrayOfPoint[1].x, 2.0D * this.center.y - paramArrayOfPoint[1].y);
  }

  public void set(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble != null)
    {
      Point localPoint1 = this.center;
      double d2;
      label22: double d3;
      label46: double d4;
      label70: Size localSize2;
      double d5;
      if (paramArrayOfDouble.length > 0)
      {
        d2 = paramArrayOfDouble[0];
        localPoint1.x = d2;
        Point localPoint2 = this.center;
        if (paramArrayOfDouble.length <= 1)
          break label123;
        d3 = paramArrayOfDouble[1];
        localPoint2.y = d3;
        Size localSize1 = this.size;
        if (paramArrayOfDouble.length <= 2)
          break label129;
        d4 = paramArrayOfDouble[2];
        localSize1.width = d4;
        localSize2 = this.size;
        if (paramArrayOfDouble.length <= 3)
          break label135;
        d5 = paramArrayOfDouble[3];
      }
      while (true)
      {
        localSize2.height = d5;
        if (paramArrayOfDouble.length > 4)
          d1 = paramArrayOfDouble[4];
        this.angle = d1;
        return;
        d2 = d1;
        break label22:
        label123: d3 = d1;
        break label46:
        label129: d4 = d1;
        break label70:
        label135: d5 = d1;
      }
    }
    this.center.x = d1;
    this.center.x = d1;
    this.size.width = d1;
    this.size.height = d1;
    this.angle = d1;
  }

  public String toString()
  {
    return "{ " + this.center + " " + this.size + " * " + this.angle + " }";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.RotatedRect
 * JD-Core Version:    0.5.4
 */