package org.opencv.core;

import D;
import java.util.Arrays;

public class Scalar
{
  public double[] val;

  public Scalar(double paramDouble)
  {
    this.val = new double[] { paramDouble, 0.0D, 0.0D, 0.0D };
  }

  public Scalar(double paramDouble1, double paramDouble2)
  {
    this.val = new double[] { paramDouble1, paramDouble2, 0.0D, 0.0D };
  }

  public Scalar(double paramDouble1, double paramDouble2, double paramDouble3)
  {
    this.val = new double[] { paramDouble1, paramDouble2, paramDouble3, 0.0D };
  }

  public Scalar(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
  {
    this.val = new double[] { paramDouble1, paramDouble2, paramDouble3, paramDouble4 };
  }

  public Scalar(double[] paramArrayOfDouble)
  {
    if ((paramArrayOfDouble != null) && (paramArrayOfDouble.length == 4))
    {
      this.val = ((double[])paramArrayOfDouble.clone());
      return;
    }
    this.val = new double[4];
    set(paramArrayOfDouble);
  }

  public static Scalar all(double paramDouble)
  {
    return new Scalar(paramDouble, paramDouble, paramDouble, paramDouble);
  }

  public Scalar clone()
  {
    return new Scalar(this.val);
  }

  public Scalar conj()
  {
    return new Scalar(this.val[0], -this.val[1], -this.val[2], -this.val[3]);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Scalar localScalar;
    do
    {
      return true;
      if (!paramObject instanceof Scalar)
        return false;
      localScalar = (Scalar)paramObject;
    }
    while (Arrays.equals(this.val, localScalar.val));
    return false;
  }

  public int hashCode()
  {
    return 31 + Arrays.hashCode(this.val);
  }

  public boolean isReal()
  {
    return (this.val[1] == 0.0D) && (this.val[2] == 0.0D) && (this.val[3] == 0.0D);
  }

  public Scalar mul(Scalar paramScalar)
  {
    return mul(paramScalar, 1.0D);
  }

  public Scalar mul(Scalar paramScalar, double paramDouble)
  {
    return new Scalar(paramDouble * (this.val[0] * paramScalar.val[0]), paramDouble * (this.val[1] * paramScalar.val[1]), paramDouble * (this.val[2] * paramScalar.val[2]), paramDouble * (this.val[3] * paramScalar.val[3]));
  }

  public void set(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble != null)
    {
      double[] arrayOfDouble4 = this.val;
      double d2;
      label22: double d3;
      label45: double[] arrayOfDouble6;
      double d4;
      if (paramArrayOfDouble.length > 0)
      {
        d2 = paramArrayOfDouble[0];
        arrayOfDouble4[0] = d2;
        double[] arrayOfDouble5 = this.val;
        if (paramArrayOfDouble.length <= 1)
          break label102;
        d3 = paramArrayOfDouble[1];
        arrayOfDouble5[1] = d3;
        arrayOfDouble6 = this.val;
        if (paramArrayOfDouble.length <= 2)
          break label108;
        d4 = paramArrayOfDouble[2];
      }
      while (true)
      {
        arrayOfDouble6[2] = d4;
        double[] arrayOfDouble7 = this.val;
        if (paramArrayOfDouble.length > 3)
          d1 = paramArrayOfDouble[3];
        arrayOfDouble7[3] = d1;
        return;
        d2 = d1;
        break label22:
        label102: d3 = d1;
        break label45:
        label108: d4 = d1;
      }
    }
    double[] arrayOfDouble1 = this.val;
    double[] arrayOfDouble2 = this.val;
    double[] arrayOfDouble3 = this.val;
    this.val[3] = d1;
    arrayOfDouble3[2] = d1;
    arrayOfDouble2[1] = d1;
    arrayOfDouble1[0] = d1;
  }

  public String toString()
  {
    return "[" + this.val[0] + ", " + this.val[1] + ", " + this.val[2] + ", " + this.val[3] + "]";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Scalar
 * JD-Core Version:    0.5.4
 */