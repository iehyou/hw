package org.opencv.core;

public class Size
{
  public double height;
  public double width;

  public Size()
  {
    this(0.0D, 0.0D);
  }

  public Size(double paramDouble1, double paramDouble2)
  {
    this.width = paramDouble1;
    this.height = paramDouble2;
  }

  public Size(Point paramPoint)
  {
    this.width = paramPoint.x;
    this.height = paramPoint.y;
  }

  public Size(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public double area()
  {
    return this.width * this.height;
  }

  public Size clone()
  {
    return new Size(this.width, this.height);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Size localSize;
    do
    {
      return true;
      if (!paramObject instanceof Size)
        return false;
      localSize = (Size)paramObject;
    }
    while ((this.width == localSize.width) && (this.height == localSize.height));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.height);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.width);
    return i * 31 + (int)(l2 ^ l2 >>> 32);
  }

  public void set(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble != null)
    {
      double d2;
      if (paramArrayOfDouble.length > 0)
        d2 = paramArrayOfDouble[0];
      while (true)
      {
        this.width = d2;
        if (paramArrayOfDouble.length > 1)
          d1 = paramArrayOfDouble[1];
        this.height = d1;
        return;
        d2 = d1;
      }
    }
    this.width = d1;
    this.height = d1;
  }

  public String toString()
  {
    return (int)this.width + "x" + (int)this.height;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Size
 * JD-Core Version:    0.5.4
 */