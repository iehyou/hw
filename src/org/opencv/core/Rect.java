package org.opencv.core;

public class Rect
{
  public int height;
  public int width;
  public int x;
  public int y;

  public Rect()
  {
    this(0, 0, 0, 0);
  }

  public Rect(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.x = paramInt1;
    this.y = paramInt2;
    this.width = paramInt3;
    this.height = paramInt4;
  }

  public Rect(Point paramPoint1, Point paramPoint2)
  {
    double d1;
    label21: double d2;
    label45: double d3;
    label70: double d4;
    if (paramPoint1.x < paramPoint2.x)
    {
      d1 = paramPoint1.x;
      this.x = (int)d1;
      if (paramPoint1.y >= paramPoint2.y)
        break label121;
      d2 = paramPoint1.y;
      this.y = (int)d2;
      if (paramPoint1.x <= paramPoint2.x)
        break label130;
      d3 = paramPoint1.x;
      this.width = ((int)d3 - this.x);
      if (paramPoint1.y <= paramPoint2.y)
        break label139;
      d4 = paramPoint1.y;
    }
    while (true)
    {
      this.height = ((int)d4 - this.y);
      return;
      d1 = paramPoint2.x;
      break label21:
      label121: d2 = paramPoint2.y;
      break label45:
      label130: d3 = paramPoint2.x;
      break label70:
      label139: d4 = paramPoint2.y;
    }
  }

  public Rect(Point paramPoint, Size paramSize)
  {
    this((int)paramPoint.x, (int)paramPoint.y, (int)paramSize.width, (int)paramSize.height);
  }

  public Rect(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public double area()
  {
    return this.width * this.height;
  }

  public Point br()
  {
    return new Point(this.x + this.width, this.y + this.height);
  }

  public Rect clone()
  {
    return new Rect(this.x, this.y, this.width, this.height);
  }

  public boolean contains(Point paramPoint)
  {
    return (this.x <= paramPoint.x) && (paramPoint.x < this.x + this.width) && (this.y <= paramPoint.y) && (paramPoint.y < this.y + this.height);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Rect localRect;
    do
    {
      return true;
      if (!paramObject instanceof Rect)
        return false;
      localRect = (Rect)paramObject;
    }
    while ((this.x == localRect.x) && (this.y == localRect.y) && (this.width == localRect.width) && (this.height == localRect.height));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.height);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.width);
    int j = i * 31 + (int)(l2 ^ l2 >>> 32);
    long l3 = Double.doubleToLongBits(this.x);
    int k = j * 31 + (int)(l3 ^ l3 >>> 32);
    long l4 = Double.doubleToLongBits(this.y);
    return k * 31 + (int)(l4 ^ l4 >>> 32);
  }

  public void set(double[] paramArrayOfDouble)
  {
    if (paramArrayOfDouble != null)
    {
      int i;
      label14: int j;
      if (paramArrayOfDouble.length > 0)
      {
        i = (int)paramArrayOfDouble[0];
        this.x = i;
        if (paramArrayOfDouble.length <= 1)
          break label84;
        j = (int)paramArrayOfDouble[1];
        label30: this.y = j;
        if (paramArrayOfDouble.length <= 2)
          break label89;
      }
      for (int k = (int)paramArrayOfDouble[2]; ; k = 0)
      {
        this.width = k;
        int l = paramArrayOfDouble.length;
        int i1 = 0;
        if (l > 3)
          i1 = (int)paramArrayOfDouble[3];
        this.height = i1;
        return;
        i = 0;
        break label14:
        label84: j = 0;
        label89: break label30:
      }
    }
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0;
  }

  public Size size()
  {
    return new Size(this.width, this.height);
  }

  public Point tl()
  {
    return new Point(this.x, this.y);
  }

  public String toString()
  {
    return "{" + this.x + ", " + this.y + ", " + this.width + "x" + this.height + "}";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Rect
 * JD-Core Version:    0.5.4
 */