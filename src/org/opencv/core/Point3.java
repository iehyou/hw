package org.opencv.core;

public class Point3
{
  public double x;
  public double y;
  public double z;

  public Point3()
  {
    this(0.0D, 0.0D, 0.0D);
  }

  public Point3(double paramDouble1, double paramDouble2, double paramDouble3)
  {
    this.x = paramDouble1;
    this.y = paramDouble2;
    this.z = paramDouble3;
  }

  public Point3(Point paramPoint)
  {
    this.x = paramPoint.x;
    this.y = paramPoint.y;
    this.z = 0.0D;
  }

  public Point3(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public Point3 clone()
  {
    return new Point3(this.x, this.y, this.z);
  }

  public Point3 cross(Point3 paramPoint3)
  {
    return new Point3(this.y * paramPoint3.z - this.z * paramPoint3.y, this.z * paramPoint3.x - this.x * paramPoint3.z, this.x * paramPoint3.y - this.y * paramPoint3.x);
  }

  public double dot(Point3 paramPoint3)
  {
    return this.x * paramPoint3.x + this.y * paramPoint3.y + this.z * paramPoint3.z;
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Point3 localPoint3;
    do
    {
      return true;
      if (!paramObject instanceof Point3)
        return false;
      localPoint3 = (Point3)paramObject;
    }
    while ((this.x == localPoint3.x) && (this.y == localPoint3.y) && (this.z == localPoint3.z));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.x);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.y);
    int j = i * 31 + (int)(l2 ^ l2 >>> 32);
    long l3 = Double.doubleToLongBits(this.z);
    return j * 31 + (int)(l3 ^ l3 >>> 32);
  }

  public void set(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble != null)
    {
      double d2;
      label16: double d3;
      if (paramArrayOfDouble.length > 0)
      {
        d2 = paramArrayOfDouble[0];
        this.x = d2;
        if (paramArrayOfDouble.length <= 1)
          break label61;
        d3 = paramArrayOfDouble[1];
      }
      while (true)
      {
        this.y = d3;
        if (paramArrayOfDouble.length > 2)
          d1 = paramArrayOfDouble[2];
        this.z = d1;
        return;
        d2 = d1;
        break label16:
        label61: d3 = d1;
      }
    }
    this.x = d1;
    this.y = d1;
    this.z = d1;
  }

  public String toString()
  {
    return "{" + this.x + ", " + this.y + ", " + this.z + "}";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Point3
 * JD-Core Version:    0.5.4
 */