package org.opencv.core;

public class TermCriteria
{
  public static final int COUNT = 1;
  public static final int EPS = 2;
  public static final int MAX_ITER = 1;
  public double epsilon;
  public int maxCount;
  public int type;

  public TermCriteria()
  {
    this(0, 0, 0.0D);
  }

  public TermCriteria(int paramInt1, int paramInt2, double paramDouble)
  {
    this.type = paramInt1;
    this.maxCount = paramInt2;
    this.epsilon = paramDouble;
  }

  public TermCriteria(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public TermCriteria clone()
  {
    return new TermCriteria(this.type, this.maxCount, this.epsilon);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    TermCriteria localTermCriteria;
    do
    {
      return true;
      if (!paramObject instanceof TermCriteria)
        return false;
      localTermCriteria = (TermCriteria)paramObject;
    }
    while ((this.type == localTermCriteria.type) && (this.maxCount == localTermCriteria.maxCount) && (this.epsilon == localTermCriteria.epsilon));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.type);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.maxCount);
    int j = i * 31 + (int)(l2 ^ l2 >>> 32);
    long l3 = Double.doubleToLongBits(this.epsilon);
    return j * 31 + (int)(l3 ^ l3 >>> 32);
  }

  public void set(double[] paramArrayOfDouble)
  {
    if (paramArrayOfDouble != null)
    {
      int i;
      label14: double d;
      if (paramArrayOfDouble.length > 0)
      {
        i = (int)paramArrayOfDouble[0];
        this.type = i;
        int j = paramArrayOfDouble.length;
        int k = 0;
        if (j > 1)
          k = (int)paramArrayOfDouble[1];
        this.maxCount = k;
        if (paramArrayOfDouble.length <= 2)
          break label65;
        d = paramArrayOfDouble[2];
      }
      while (true)
      {
        this.epsilon = d;
        return;
        i = 0;
        break label14:
        label65: d = 0.0D;
      }
    }
    this.type = 0;
    this.maxCount = 0;
    this.epsilon = 0.0D;
  }

  public String toString()
  {
    if (this == null)
      return "null";
    return "{ type: " + this.type + ", maxCount: " + this.maxCount + ", epsilon: " + this.epsilon + "}";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.TermCriteria
 * JD-Core Version:    0.5.4
 */