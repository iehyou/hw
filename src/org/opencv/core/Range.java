package org.opencv.core;

public class Range
{
  public int end;
  public int start;

  public Range()
  {
    this(0, 0);
  }

  public Range(int paramInt1, int paramInt2)
  {
    this.start = paramInt1;
    this.end = paramInt2;
  }

  public Range(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public static Range all()
  {
    return new Range(-2147483648, 2147483647);
  }

  public Range clone()
  {
    return new Range(this.start, this.end);
  }

  public boolean empty()
  {
    return this.end <= this.start;
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Range localRange;
    do
    {
      return true;
      if (!paramObject instanceof Range)
        return false;
      localRange = (Range)paramObject;
    }
    while ((this.start == localRange.start) && (this.end == localRange.end));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.start);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.end);
    return i * 31 + (int)(l2 ^ l2 >>> 32);
  }

  public Range intersection(Range paramRange)
  {
    Range localRange = new Range(Math.max(paramRange.start, this.start), Math.min(paramRange.end, this.end));
    localRange.end = Math.max(localRange.end, localRange.start);
    return localRange;
  }

  public void set(double[] paramArrayOfDouble)
  {
    if (paramArrayOfDouble != null)
    {
      if (paramArrayOfDouble.length > 0);
      for (int i = (int)paramArrayOfDouble[0]; ; i = 0)
      {
        this.start = i;
        int j = paramArrayOfDouble.length;
        int k = 0;
        if (j > 1)
          k = (int)paramArrayOfDouble[1];
        this.end = k;
        return;
      }
    }
    this.start = 0;
    this.end = 0;
  }

  public Range shift(int paramInt)
  {
    return new Range(paramInt + this.start, paramInt + this.end);
  }

  public int size()
  {
    if (empty())
      return 0;
    return this.end - this.start;
  }

  public String toString()
  {
    return "[" + this.start + ", " + this.end + ")";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Range
 * JD-Core Version:    0.5.4
 */