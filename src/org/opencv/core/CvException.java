package org.opencv.core;

public class CvException extends RuntimeException
{
  private static final long serialVersionUID = 1L;

  public CvException(String paramString)
  {
    super(paramString);
  }

  public String toString()
  {
    return "CvException [" + super.toString() + "]";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.CvException
 * JD-Core Version:    0.5.4
 */