package org.opencv.core;

public final class CvType
{
  public static final int CV_16S = 3;
  public static final int CV_16SC1 = 0;
  public static final int CV_16SC2 = 0;
  public static final int CV_16SC3 = 0;
  public static final int CV_16SC4 = 0;
  public static final int CV_16U = 2;
  public static final int CV_16UC1 = 0;
  public static final int CV_16UC2 = 0;
  public static final int CV_16UC3 = 0;
  public static final int CV_16UC4 = 0;
  public static final int CV_32F = 5;
  public static final int CV_32FC1 = 0;
  public static final int CV_32FC2 = 0;
  public static final int CV_32FC3 = 0;
  public static final int CV_32FC4 = 0;
  public static final int CV_32S = 4;
  public static final int CV_32SC1 = 0;
  public static final int CV_32SC2 = 0;
  public static final int CV_32SC3 = 0;
  public static final int CV_32SC4 = 0;
  public static final int CV_64F = 6;
  public static final int CV_64FC1 = 0;
  public static final int CV_64FC2 = 0;
  public static final int CV_64FC3 = 0;
  public static final int CV_64FC4 = 0;
  public static final int CV_8S = 1;
  public static final int CV_8SC1 = 0;
  public static final int CV_8SC2 = 0;
  public static final int CV_8SC3 = 0;
  public static final int CV_8SC4 = 0;
  public static final int CV_8U = 0;
  public static final int CV_8UC1 = 0;
  public static final int CV_8UC2 = 0;
  public static final int CV_8UC3 = 0;
  public static final int CV_8UC4 = 0;
  private static final int CV_CN_MAX = 512;
  private static final int CV_CN_SHIFT = 3;
  private static final int CV_DEPTH_MAX = 8;
  public static final int CV_USRTYPE1 = 7;

  static
  {
    CV_8SC1 = CV_8SC(1);
    CV_8SC2 = CV_8SC(2);
    CV_8SC3 = CV_8SC(3);
    CV_8SC4 = CV_8SC(4);
    CV_16UC1 = CV_16UC(1);
    CV_16UC2 = CV_16UC(2);
    CV_16UC3 = CV_16UC(3);
    CV_16UC4 = CV_16UC(4);
    CV_16SC1 = CV_16SC(1);
    CV_16SC2 = CV_16SC(2);
    CV_16SC3 = CV_16SC(3);
    CV_16SC4 = CV_16SC(4);
    CV_32SC1 = CV_32SC(1);
    CV_32SC2 = CV_32SC(2);
    CV_32SC3 = CV_32SC(3);
    CV_32SC4 = CV_32SC(4);
    CV_32FC1 = CV_32FC(1);
    CV_32FC2 = CV_32FC(2);
    CV_32FC3 = CV_32FC(3);
    CV_32FC4 = CV_32FC(4);
    CV_64FC1 = CV_64FC(1);
    CV_64FC2 = CV_64FC(2);
    CV_64FC3 = CV_64FC(3);
    CV_64FC4 = CV_64FC(4);
  }

  public static final int CV_16SC(int paramInt)
  {
    return makeType(3, paramInt);
  }

  public static final int CV_16UC(int paramInt)
  {
    return makeType(2, paramInt);
  }

  public static final int CV_32FC(int paramInt)
  {
    return makeType(5, paramInt);
  }

  public static final int CV_32SC(int paramInt)
  {
    return makeType(4, paramInt);
  }

  public static final int CV_64FC(int paramInt)
  {
    return makeType(6, paramInt);
  }

  public static final int CV_8SC(int paramInt)
  {
    return makeType(1, paramInt);
  }

  public static final int CV_8UC(int paramInt)
  {
    return makeType(0, paramInt);
  }

  public static final int ELEM_SIZE(int paramInt)
  {
    switch (depth(paramInt))
    {
    default:
      throw new UnsupportedOperationException("Unsupported CvType value: " + paramInt);
    case 0:
    case 1:
      return channels(paramInt);
    case 2:
    case 3:
      return 2 * channels(paramInt);
    case 4:
    case 5:
      return 4 * channels(paramInt);
    case 6:
    }
    return 8 * channels(paramInt);
  }

  public static final int channels(int paramInt)
  {
    return 1 + (paramInt >> 3);
  }

  public static final int depth(int paramInt)
  {
    return paramInt & 0x7;
  }

  public static final boolean isInteger(int paramInt)
  {
    return depth(paramInt) < 5;
  }

  public static final int makeType(int paramInt1, int paramInt2)
  {
    if ((paramInt2 <= 0) || (paramInt2 >= 512))
      throw new UnsupportedOperationException("Channels count should be 1..511");
    if ((paramInt1 < 0) || (paramInt1 >= 8))
      throw new UnsupportedOperationException("Data type depth should be 0..7");
    return (paramInt1 & 0x7) + (paramInt2 - 1 << 3);
  }

  public static final String typeToString(int paramInt)
  {
    switch (depth(paramInt))
    {
    default:
      throw new UnsupportedOperationException("Unsupported CvType value: " + paramInt);
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    }
    int i;
    for (String str = "CV_8U"; ; str = "CV_USRTYPE1")
      while (true)
      {
        i = channels(paramInt);
        if (i > 4)
          break label158;
        return str + "C" + i;
        str = "CV_8S";
        continue;
        str = "CV_16U";
        continue;
        str = "CV_16S";
        continue;
        str = "CV_32S";
        continue;
        str = "CV_32F";
        continue;
        str = "CV_64F";
      }
    label158: return str + "C(" + i + ")";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.CvType
 * JD-Core Version:    0.5.4
 */