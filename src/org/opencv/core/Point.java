package org.opencv.core;

public class Point
{
  public double x;
  public double y;

  public Point()
  {
    this(0.0D, 0.0D);
  }

  public Point(double paramDouble1, double paramDouble2)
  {
    this.x = paramDouble1;
    this.y = paramDouble2;
  }

  public Point(double[] paramArrayOfDouble)
  {
    set(paramArrayOfDouble);
  }

  public Point clone()
  {
    return new Point(this.x, this.y);
  }

  public double dot(Point paramPoint)
  {
    return this.x * paramPoint.x + this.y * paramPoint.y;
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Point localPoint;
    do
    {
      return true;
      if (!paramObject instanceof Point)
        return false;
      localPoint = (Point)paramObject;
    }
    while ((this.x == localPoint.x) && (this.y == localPoint.y));
    return false;
  }

  public int hashCode()
  {
    long l1 = Double.doubleToLongBits(this.x);
    int i = 31 + (int)(l1 ^ l1 >>> 32);
    long l2 = Double.doubleToLongBits(this.y);
    return i * 31 + (int)(l2 ^ l2 >>> 32);
  }

  public boolean inside(Rect paramRect)
  {
    return paramRect.contains(this);
  }

  public void set(double[] paramArrayOfDouble)
  {
    double d1 = 0.0D;
    if (paramArrayOfDouble != null)
    {
      double d2;
      if (paramArrayOfDouble.length > 0)
        d2 = paramArrayOfDouble[0];
      while (true)
      {
        this.x = d2;
        if (paramArrayOfDouble.length > 1)
          d1 = paramArrayOfDouble[1];
        this.y = d1;
        return;
        d2 = d1;
      }
    }
    this.x = d1;
    this.y = d1;
  }

  public String toString()
  {
    return "{" + this.x + ", " + this.y + "}";
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Point
 * JD-Core Version:    0.5.4
 */