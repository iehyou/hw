package org.opencv.core;

import java.util.ArrayList;
import java.util.List;
import org.opencv.utils.Converters;

public class Core
{
  public static final int CMP_EQ = 0;
  public static final int CMP_GE = 2;
  public static final int CMP_GT = 1;
  public static final int CMP_LE = 4;
  public static final int CMP_LT = 3;
  public static final int CMP_NE = 5;
  public static final int COVAR_COLS = 16;
  public static final int COVAR_NORMAL = 1;
  public static final int COVAR_ROWS = 8;
  public static final int COVAR_SCALE = 4;
  public static final int COVAR_SCRAMBLED = 0;
  public static final int COVAR_USE_AVG = 2;
  private static final int CV_16S = 3;
  private static final int CV_16U = 2;
  private static final int CV_32F = 5;
  private static final int CV_32S = 4;
  private static final int CV_64F = 6;
  private static final int CV_8S = 1;
  private static final int CV_8U = 0;
  private static final int CV_USRTYPE1 = 7;
  public static final int DCT_INVERSE = 1;
  public static final int DCT_ROWS = 4;
  public static final int DECOMP_CHOLESKY = 3;
  public static final int DECOMP_EIG = 2;
  public static final int DECOMP_LU = 0;
  public static final int DECOMP_NORMAL = 16;
  public static final int DECOMP_QR = 4;
  public static final int DECOMP_SVD = 1;
  public static final int DEPTH_MASK = 7;
  public static final int DEPTH_MASK_16S = 8;
  public static final int DEPTH_MASK_16U = 4;
  public static final int DEPTH_MASK_32F = 32;
  public static final int DEPTH_MASK_32S = 16;
  public static final int DEPTH_MASK_64F = 64;
  public static final int DEPTH_MASK_8S = 2;
  public static final int DEPTH_MASK_8U = 1;
  public static final int DEPTH_MASK_ALL = 127;
  public static final int DEPTH_MASK_ALL_BUT_8S = 125;
  public static final int DEPTH_MASK_FLT = 96;
  public static final int DFT_COMPLEX_OUTPUT = 16;
  public static final int DFT_INVERSE = 1;
  public static final int DFT_REAL_OUTPUT = 32;
  public static final int DFT_ROWS = 4;
  public static final int DFT_SCALE = 2;
  public static final int FILLED = -1;
  public static final int FONT_HERSHEY_COMPLEX = 3;
  public static final int FONT_HERSHEY_COMPLEX_SMALL = 5;
  public static final int FONT_HERSHEY_DUPLEX = 2;
  public static final int FONT_HERSHEY_PLAIN = 1;
  public static final int FONT_HERSHEY_SCRIPT_COMPLEX = 7;
  public static final int FONT_HERSHEY_SCRIPT_SIMPLEX = 6;
  public static final int FONT_HERSHEY_SIMPLEX = 0;
  public static final int FONT_HERSHEY_TRIPLEX = 4;
  public static final int FONT_ITALIC = 16;
  public static final int GEMM_1_T = 1;
  public static final int GEMM_2_T = 2;
  public static final int GEMM_3_T = 4;
  public static final int KMEANS_PP_CENTERS = 2;
  public static final int KMEANS_RANDOM_CENTERS = 0;
  public static final int KMEANS_USE_INITIAL_LABELS = 1;
  public static final int LINE_4 = 4;
  public static final int LINE_8 = 8;
  public static final int LINE_AA = 16;
  public static final int MAGIC_MASK = -65536;
  public static final int NORM_INF = 1;
  public static final int NORM_L1 = 2;
  public static final int NORM_L2 = 4;
  public static final int NORM_MINMAX = 32;
  public static final int NORM_RELATIVE = 8;
  public static final int NORM_TYPE_MASK = 7;
  public static final int REDUCE_AVG = 1;
  public static final int REDUCE_MAX = 2;
  public static final int REDUCE_MIN = 3;
  public static final int REDUCE_SUM = 0;
  public static final int SORT_ASCENDING = 0;
  public static final int SORT_DESCENDING = 16;
  public static final int SORT_EVERY_COLUMN = 1;
  public static final int SORT_EVERY_ROW = 0;
  public static final int SVD_FULL_UV = 4;
  public static final int SVD_MODIFY_A = 1;
  public static final int SVD_NO_UV = 2;
  public static final int TYPE_MASK = 4095;

  static
  {
    System.loadLibrary("opencv_java");
  }

  public static void LUT(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    LUT_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void LUT(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    LUT_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  private static native void LUT_0(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  private static native void LUT_1(long paramLong1, long paramLong2, long paramLong3);

  public static double Mahalanobis(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    return Mahalanobis_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native double Mahalanobis_0(long paramLong1, long paramLong2, long paramLong3);

  public static void PCABackProject(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    PCABackProject_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void PCABackProject_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  public static void PCACompute(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    PCACompute_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void PCACompute(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    PCACompute_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  private static native void PCACompute_0(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  private static native void PCACompute_1(long paramLong1, long paramLong2, long paramLong3);

  public static void PCAProject(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    PCAProject_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void PCAProject_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  public static void SVBackSubst(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, Mat paramMat5)
  {
    SVBackSubst_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramMat5.nativeObj);
  }

  private static native void SVBackSubst_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5);

  public static void SVDecomp(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    SVDecomp_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  public static void SVDecomp(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, int paramInt)
  {
    SVDecomp_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramInt);
  }

  private static native void SVDecomp_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt);

  private static native void SVDecomp_1(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  public static void absdiff(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    absdiff_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void absdiff_0(long paramLong1, long paramLong2, long paramLong3);

  public static void add(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    add_2(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void add(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    add_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  public static void add(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, int paramInt)
  {
    add_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramInt);
  }

  public static void addWeighted(Mat paramMat1, double paramDouble1, Mat paramMat2, double paramDouble2, double paramDouble3, Mat paramMat3)
  {
    addWeighted_1(paramMat1.nativeObj, paramDouble1, paramMat2.nativeObj, paramDouble2, paramDouble3, paramMat3.nativeObj);
  }

  public static void addWeighted(Mat paramMat1, double paramDouble1, Mat paramMat2, double paramDouble2, double paramDouble3, Mat paramMat3, int paramInt)
  {
    addWeighted_0(paramMat1.nativeObj, paramDouble1, paramMat2.nativeObj, paramDouble2, paramDouble3, paramMat3.nativeObj, paramInt);
  }

  private static native void addWeighted_0(long paramLong1, double paramDouble1, long paramLong2, double paramDouble2, double paramDouble3, long paramLong3, int paramInt);

  private static native void addWeighted_1(long paramLong1, double paramDouble1, long paramLong2, double paramDouble2, double paramDouble3, long paramLong3);

  private static native void add_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt);

  private static native void add_1(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void add_2(long paramLong1, long paramLong2, long paramLong3);

  public static void bitwise_and(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    bitwise_and_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void bitwise_and(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    bitwise_and_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void bitwise_and_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void bitwise_and_1(long paramLong1, long paramLong2, long paramLong3);

  public static void bitwise_not(Mat paramMat1, Mat paramMat2)
  {
    bitwise_not_1(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void bitwise_not(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    bitwise_not_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void bitwise_not_0(long paramLong1, long paramLong2, long paramLong3);

  private static native void bitwise_not_1(long paramLong1, long paramLong2);

  public static void bitwise_or(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    bitwise_or_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void bitwise_or(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    bitwise_or_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void bitwise_or_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void bitwise_or_1(long paramLong1, long paramLong2, long paramLong3);

  public static void bitwise_xor(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    bitwise_xor_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void bitwise_xor(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    bitwise_xor_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void bitwise_xor_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void bitwise_xor_1(long paramLong1, long paramLong2, long paramLong3);

  public static void calcCovarMatrix(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    calcCovarMatrix_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  public static void calcCovarMatrix(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt1, int paramInt2)
  {
    calcCovarMatrix_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt1, paramInt2);
  }

  private static native void calcCovarMatrix_0(long paramLong1, long paramLong2, long paramLong3, int paramInt1, int paramInt2);

  private static native void calcCovarMatrix_1(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  public static void cartToPolar(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    cartToPolar_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  public static void cartToPolar(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, boolean paramBoolean)
  {
    cartToPolar_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramBoolean);
  }

  private static native void cartToPolar_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, boolean paramBoolean);

  private static native void cartToPolar_1(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  public static boolean checkRange(Mat paramMat)
  {
    return checkRange_4(paramMat.nativeObj);
  }

  public static boolean checkRange(Mat paramMat, boolean paramBoolean)
  {
    return checkRange_3(paramMat.nativeObj, paramBoolean);
  }

  public static boolean checkRange(Mat paramMat, boolean paramBoolean, Point paramPoint)
  {
    double[] arrayOfDouble = new double[2];
    boolean bool = checkRange_2(paramMat.nativeObj, paramBoolean, arrayOfDouble);
    if (paramPoint != null)
    {
      paramPoint.x = arrayOfDouble[0];
      paramPoint.y = arrayOfDouble[1];
    }
    return bool;
  }

  public static boolean checkRange(Mat paramMat, boolean paramBoolean, Point paramPoint, double paramDouble)
  {
    double[] arrayOfDouble = new double[2];
    boolean bool = checkRange_1(paramMat.nativeObj, paramBoolean, arrayOfDouble, paramDouble);
    if (paramPoint != null)
    {
      paramPoint.x = arrayOfDouble[0];
      paramPoint.y = arrayOfDouble[1];
    }
    return bool;
  }

  public static boolean checkRange(Mat paramMat, boolean paramBoolean, Point paramPoint, double paramDouble1, double paramDouble2)
  {
    double[] arrayOfDouble = new double[2];
    boolean bool = checkRange_0(paramMat.nativeObj, paramBoolean, arrayOfDouble, paramDouble1, paramDouble2);
    if (paramPoint != null)
    {
      paramPoint.x = arrayOfDouble[0];
      paramPoint.y = arrayOfDouble[1];
    }
    return bool;
  }

  private static native boolean checkRange_0(long paramLong, boolean paramBoolean, double[] paramArrayOfDouble, double paramDouble1, double paramDouble2);

  private static native boolean checkRange_1(long paramLong, boolean paramBoolean, double[] paramArrayOfDouble, double paramDouble);

  private static native boolean checkRange_2(long paramLong, boolean paramBoolean, double[] paramArrayOfDouble);

  private static native boolean checkRange_3(long paramLong, boolean paramBoolean);

  private static native boolean checkRange_4(long paramLong);

  public static void circle(Mat paramMat, Point paramPoint, int paramInt, Scalar paramScalar)
  {
    circle_3(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramInt, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void circle(Mat paramMat, Point paramPoint, int paramInt1, Scalar paramScalar, int paramInt2)
  {
    circle_2(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramInt1, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2);
  }

  public static void circle(Mat paramMat, Point paramPoint, int paramInt1, Scalar paramScalar, int paramInt2, int paramInt3)
  {
    circle_1(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramInt1, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2, paramInt3);
  }

  public static void circle(Mat paramMat, Point paramPoint, int paramInt1, Scalar paramScalar, int paramInt2, int paramInt3, int paramInt4)
  {
    circle_0(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramInt1, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2, paramInt3, paramInt4);
  }

  private static native void circle_0(long paramLong, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, int paramInt2, int paramInt3, int paramInt4);

  private static native void circle_1(long paramLong, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, int paramInt2, int paramInt3);

  private static native void circle_2(long paramLong, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, int paramInt2);

  private static native void circle_3(long paramLong, double paramDouble1, double paramDouble2, int paramInt, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6);

  public static boolean clipLine(Rect paramRect, Point paramPoint1, Point paramPoint2)
  {
    double[] arrayOfDouble1 = new double[2];
    double[] arrayOfDouble2 = new double[2];
    boolean bool = clipLine_0(paramRect.x, paramRect.y, paramRect.width, paramRect.height, paramPoint1.x, paramPoint1.y, arrayOfDouble1, paramPoint2.x, paramPoint2.y, arrayOfDouble2);
    if (paramPoint1 != null)
    {
      paramPoint1.x = arrayOfDouble1[0];
      paramPoint1.y = arrayOfDouble1[1];
    }
    if (paramPoint2 != null)
    {
      paramPoint2.x = arrayOfDouble2[0];
      paramPoint2.y = arrayOfDouble2[1];
    }
    return bool;
  }

  private static native boolean clipLine_0(int paramInt1, int paramInt2, int paramInt3, int paramInt4, double paramDouble1, double paramDouble2, double[] paramArrayOfDouble1, double paramDouble3, double paramDouble4, double[] paramArrayOfDouble2);

  public static void compare(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    compare_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  private static native void compare_0(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  public static void completeSymm(Mat paramMat)
  {
    completeSymm_1(paramMat.nativeObj);
  }

  public static void completeSymm(Mat paramMat, boolean paramBoolean)
  {
    completeSymm_0(paramMat.nativeObj, paramBoolean);
  }

  private static native void completeSymm_0(long paramLong, boolean paramBoolean);

  private static native void completeSymm_1(long paramLong);

  public static void convertScaleAbs(Mat paramMat1, Mat paramMat2)
  {
    convertScaleAbs_2(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void convertScaleAbs(Mat paramMat1, Mat paramMat2, double paramDouble)
  {
    convertScaleAbs_1(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble);
  }

  public static void convertScaleAbs(Mat paramMat1, Mat paramMat2, double paramDouble1, double paramDouble2)
  {
    convertScaleAbs_0(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramDouble2);
  }

  private static native void convertScaleAbs_0(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2);

  private static native void convertScaleAbs_1(long paramLong1, long paramLong2, double paramDouble);

  private static native void convertScaleAbs_2(long paramLong1, long paramLong2);

  public static int countNonZero(Mat paramMat)
  {
    return countNonZero_0(paramMat.nativeObj);
  }

  private static native int countNonZero_0(long paramLong);

  public static float cubeRoot(float paramFloat)
  {
    return cubeRoot_0(paramFloat);
  }

  private static native float cubeRoot_0(float paramFloat);

  public static void dct(Mat paramMat1, Mat paramMat2)
  {
    dct_1(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void dct(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    dct_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void dct_0(long paramLong1, long paramLong2, int paramInt);

  private static native void dct_1(long paramLong1, long paramLong2);

  public static double determinant(Mat paramMat)
  {
    return determinant_0(paramMat.nativeObj);
  }

  private static native double determinant_0(long paramLong);

  public static void dft(Mat paramMat1, Mat paramMat2)
  {
    dft_2(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void dft(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    dft_1(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  public static void dft(Mat paramMat1, Mat paramMat2, int paramInt1, int paramInt2)
  {
    dft_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt1, paramInt2);
  }

  private static native void dft_0(long paramLong1, long paramLong2, int paramInt1, int paramInt2);

  private static native void dft_1(long paramLong1, long paramLong2, int paramInt);

  private static native void dft_2(long paramLong1, long paramLong2);

  public static void divide(double paramDouble, Mat paramMat1, Mat paramMat2)
  {
    divide_4(paramDouble, paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void divide(double paramDouble, Mat paramMat1, Mat paramMat2, int paramInt)
  {
    divide_3(paramDouble, paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  public static void divide(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    divide_2(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void divide(Mat paramMat1, Mat paramMat2, Mat paramMat3, double paramDouble)
  {
    divide_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramDouble);
  }

  public static void divide(Mat paramMat1, Mat paramMat2, Mat paramMat3, double paramDouble, int paramInt)
  {
    divide_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramDouble, paramInt);
  }

  private static native void divide_0(long paramLong1, long paramLong2, long paramLong3, double paramDouble, int paramInt);

  private static native void divide_1(long paramLong1, long paramLong2, long paramLong3, double paramDouble);

  private static native void divide_2(long paramLong1, long paramLong2, long paramLong3);

  private static native void divide_3(double paramDouble, long paramLong1, long paramLong2, int paramInt);

  private static native void divide_4(double paramDouble, long paramLong1, long paramLong2);

  public static boolean eigen(Mat paramMat1, boolean paramBoolean, Mat paramMat2, Mat paramMat3)
  {
    return eigen_0(paramMat1.nativeObj, paramBoolean, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native boolean eigen_0(long paramLong1, boolean paramBoolean, long paramLong2, long paramLong3);

  public static void ellipse(Mat paramMat, Point paramPoint, Size paramSize, double paramDouble1, double paramDouble2, double paramDouble3, Scalar paramScalar)
  {
    ellipse_3(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramSize.width, paramSize.height, paramDouble1, paramDouble2, paramDouble3, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void ellipse(Mat paramMat, Point paramPoint, Size paramSize, double paramDouble1, double paramDouble2, double paramDouble3, Scalar paramScalar, int paramInt)
  {
    ellipse_2(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramSize.width, paramSize.height, paramDouble1, paramDouble2, paramDouble3, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
  }

  public static void ellipse(Mat paramMat, Point paramPoint, Size paramSize, double paramDouble1, double paramDouble2, double paramDouble3, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    ellipse_1(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramSize.width, paramSize.height, paramDouble1, paramDouble2, paramDouble3, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
  }

  public static void ellipse(Mat paramMat, Point paramPoint, Size paramSize, double paramDouble1, double paramDouble2, double paramDouble3, Scalar paramScalar, int paramInt1, int paramInt2, int paramInt3)
  {
    ellipse_0(paramMat.nativeObj, paramPoint.x, paramPoint.y, paramSize.width, paramSize.height, paramDouble1, paramDouble2, paramDouble3, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2, paramInt3);
  }

  public static void ellipse(Mat paramMat, RotatedRect paramRotatedRect, Scalar paramScalar)
  {
    ellipse_6(paramMat.nativeObj, paramRotatedRect.center.x, paramRotatedRect.center.y, paramRotatedRect.size.width, paramRotatedRect.size.height, paramRotatedRect.angle, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void ellipse(Mat paramMat, RotatedRect paramRotatedRect, Scalar paramScalar, int paramInt)
  {
    ellipse_5(paramMat.nativeObj, paramRotatedRect.center.x, paramRotatedRect.center.y, paramRotatedRect.size.width, paramRotatedRect.size.height, paramRotatedRect.angle, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
  }

  public static void ellipse(Mat paramMat, RotatedRect paramRotatedRect, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    ellipse_4(paramMat.nativeObj, paramRotatedRect.center.x, paramRotatedRect.center.y, paramRotatedRect.size.width, paramRotatedRect.size.height, paramRotatedRect.angle, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
  }

  public static void ellipse2Poly(Point paramPoint, Size paramSize, int paramInt1, int paramInt2, int paramInt3, int paramInt4, List<Point> paramList)
  {
    Mat localMat = new Mat();
    ellipse2Poly_0(paramPoint.x, paramPoint.y, paramSize.width, paramSize.height, paramInt1, paramInt2, paramInt3, paramInt4, localMat.nativeObj);
    Converters.Mat_to_vector_Point(localMat, paramList);
  }

  private static native void ellipse2Poly_0(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong);

  private static native void ellipse_0(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, double paramDouble10, double paramDouble11, int paramInt1, int paramInt2, int paramInt3);

  private static native void ellipse_1(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, double paramDouble10, double paramDouble11, int paramInt1, int paramInt2);

  private static native void ellipse_2(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, double paramDouble10, double paramDouble11, int paramInt);

  private static native void ellipse_3(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, double paramDouble10, double paramDouble11);

  private static native void ellipse_4(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, int paramInt1, int paramInt2);

  private static native void ellipse_5(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9, int paramInt);

  private static native void ellipse_6(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, double paramDouble9);

  public static void exp(Mat paramMat1, Mat paramMat2)
  {
    exp_0(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  private static native void exp_0(long paramLong1, long paramLong2);

  public static void extractChannel(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    extractChannel_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void extractChannel_0(long paramLong1, long paramLong2, int paramInt);

  public static float fastAtan2(float paramFloat1, float paramFloat2)
  {
    return fastAtan2_0(paramFloat1, paramFloat2);
  }

  private static native float fastAtan2_0(float paramFloat1, float paramFloat2);

  public static void fillConvexPoly(Mat paramMat, List<Point> paramList, Scalar paramScalar)
  {
    Mat localMat = Converters.vector_Point_to_Mat(paramList);
    fillConvexPoly_2(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void fillConvexPoly(Mat paramMat, List<Point> paramList, Scalar paramScalar, int paramInt)
  {
    Mat localMat = Converters.vector_Point_to_Mat(paramList);
    fillConvexPoly_1(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
  }

  public static void fillConvexPoly(Mat paramMat, List<Point> paramList, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    Mat localMat = Converters.vector_Point_to_Mat(paramList);
    fillConvexPoly_0(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
  }

  private static native void fillConvexPoly_0(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2);

  private static native void fillConvexPoly_1(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt);

  private static native void fillConvexPoly_2(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);

  public static void fillPoly(Mat paramMat, List<List<Point>> paramList, Scalar paramScalar)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, new ArrayList(i));
      fillPoly_3(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
      return;
    }
  }

  public static void fillPoly(Mat paramMat, List<List<Point>> paramList, Scalar paramScalar, int paramInt)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, new ArrayList(i));
      fillPoly_2(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
      return;
    }
  }

  public static void fillPoly(Mat paramMat, List<List<Point>> paramList, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      ArrayList localArrayList = new ArrayList(i);
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, localArrayList);
      fillPoly_1(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
      return;
    }
  }

  public static void fillPoly(Mat paramMat, List<List<Point>> paramList, Scalar paramScalar, int paramInt1, int paramInt2, Point paramPoint)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      ArrayList localArrayList = new ArrayList(i);
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, localArrayList);
      fillPoly_0(paramMat.nativeObj, localMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2, paramPoint.x, paramPoint.y);
      return;
    }
  }

  private static native void fillPoly_0(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2, double paramDouble5, double paramDouble6);

  private static native void fillPoly_1(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2);

  private static native void fillPoly_2(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt);

  private static native void fillPoly_3(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);

  public static void flip(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    flip_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void flip_0(long paramLong1, long paramLong2, int paramInt);

  public static void gemm(Mat paramMat1, Mat paramMat2, double paramDouble1, Mat paramMat3, double paramDouble2, Mat paramMat4)
  {
    gemm_1(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramMat3.nativeObj, paramDouble2, paramMat4.nativeObj);
  }

  public static void gemm(Mat paramMat1, Mat paramMat2, double paramDouble1, Mat paramMat3, double paramDouble2, Mat paramMat4, int paramInt)
  {
    gemm_0(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramMat3.nativeObj, paramDouble2, paramMat4.nativeObj, paramInt);
  }

  private static native void gemm_0(long paramLong1, long paramLong2, double paramDouble1, long paramLong3, double paramDouble2, long paramLong4, int paramInt);

  private static native void gemm_1(long paramLong1, long paramLong2, double paramDouble1, long paramLong3, double paramDouble2, long paramLong4);

  public static long getCPUTickCount()
  {
    return getCPUTickCount_0();
  }

  private static native long getCPUTickCount_0();

  public static int getNumberOfCPUs()
  {
    return getNumberOfCPUs_0();
  }

  private static native int getNumberOfCPUs_0();

  public static int getOptimalDFTSize(int paramInt)
  {
    return getOptimalDFTSize_0(paramInt);
  }

  private static native int getOptimalDFTSize_0(int paramInt);

  public static Size getTextSize(String paramString, int paramInt1, double paramDouble, int paramInt2, int[] paramArrayOfInt)
  {
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length != 1))
      throw new IllegalArgumentException("'baseLine' must be 'int[1]' or 'null'.");
    return new Size(n_getTextSize(paramString, paramInt1, paramDouble, paramInt2, paramArrayOfInt));
  }

  public static long getTickCount()
  {
    return getTickCount_0();
  }

  private static native long getTickCount_0();

  public static double getTickFrequency()
  {
    return getTickFrequency_0();
  }

  private static native double getTickFrequency_0();

  public static void hconcat(List<Mat> paramList, Mat paramMat)
  {
    hconcat_0(Converters.vector_Mat_to_Mat(paramList).nativeObj, paramMat.nativeObj);
  }

  private static native void hconcat_0(long paramLong1, long paramLong2);

  public static void idct(Mat paramMat1, Mat paramMat2)
  {
    idct_1(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void idct(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    idct_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void idct_0(long paramLong1, long paramLong2, int paramInt);

  private static native void idct_1(long paramLong1, long paramLong2);

  public static void idft(Mat paramMat1, Mat paramMat2)
  {
    idft_2(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void idft(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    idft_1(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  public static void idft(Mat paramMat1, Mat paramMat2, int paramInt1, int paramInt2)
  {
    idft_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt1, paramInt2);
  }

  private static native void idft_0(long paramLong1, long paramLong2, int paramInt1, int paramInt2);

  private static native void idft_1(long paramLong1, long paramLong2, int paramInt);

  private static native void idft_2(long paramLong1, long paramLong2);

  public static void inRange(Mat paramMat1, Scalar paramScalar1, Scalar paramScalar2, Mat paramMat2)
  {
    inRange_0(paramMat1.nativeObj, paramScalar1.val[0], paramScalar1.val[1], paramScalar1.val[2], paramScalar1.val[3], paramScalar2.val[0], paramScalar2.val[1], paramScalar2.val[2], paramScalar2.val[3], paramMat2.nativeObj);
  }

  private static native void inRange_0(long paramLong1, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, long paramLong2);

  public static void insertChannel(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    insertChannel_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void insertChannel_0(long paramLong1, long paramLong2, int paramInt);

  public static double invert(Mat paramMat1, Mat paramMat2)
  {
    return invert_1(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static double invert(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    return invert_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native double invert_0(long paramLong1, long paramLong2, int paramInt);

  private static native double invert_1(long paramLong1, long paramLong2);

  public static double kmeans(Mat paramMat1, int paramInt1, Mat paramMat2, TermCriteria paramTermCriteria, int paramInt2, int paramInt3)
  {
    return kmeans_1(paramMat1.nativeObj, paramInt1, paramMat2.nativeObj, paramTermCriteria.type, paramTermCriteria.maxCount, paramTermCriteria.epsilon, paramInt2, paramInt3);
  }

  public static double kmeans(Mat paramMat1, int paramInt1, Mat paramMat2, TermCriteria paramTermCriteria, int paramInt2, int paramInt3, Mat paramMat3)
  {
    return kmeans_0(paramMat1.nativeObj, paramInt1, paramMat2.nativeObj, paramTermCriteria.type, paramTermCriteria.maxCount, paramTermCriteria.epsilon, paramInt2, paramInt3, paramMat3.nativeObj);
  }

  private static native double kmeans_0(long paramLong1, int paramInt1, long paramLong2, int paramInt2, int paramInt3, double paramDouble, int paramInt4, int paramInt5, long paramLong3);

  private static native double kmeans_1(long paramLong1, int paramInt1, long paramLong2, int paramInt2, int paramInt3, double paramDouble, int paramInt4, int paramInt5);

  public static void line(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar)
  {
    line_3(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void line(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt)
  {
    line_2(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
  }

  public static void line(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    line_1(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
  }

  public static void line(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt1, int paramInt2, int paramInt3)
  {
    line_0(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2, paramInt3);
  }

  private static native void line_0(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt1, int paramInt2, int paramInt3);

  private static native void line_1(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt1, int paramInt2);

  private static native void line_2(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt);

  private static native void line_3(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8);

  public static void log(Mat paramMat1, Mat paramMat2)
  {
    log_0(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  private static native void log_0(long paramLong1, long paramLong2);

  public static void magnitude(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    magnitude_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void magnitude_0(long paramLong1, long paramLong2, long paramLong3);

  public static void max(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    max_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void max_0(long paramLong1, long paramLong2, long paramLong3);

  public static Scalar mean(Mat paramMat)
  {
    return new Scalar(mean_1(paramMat.nativeObj));
  }

  public static Scalar mean(Mat paramMat1, Mat paramMat2)
  {
    return new Scalar(mean_0(paramMat1.nativeObj, paramMat2.nativeObj));
  }

  public static void meanStdDev(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    meanStdDev_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void meanStdDev(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    meanStdDev_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  private static native void meanStdDev_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void meanStdDev_1(long paramLong1, long paramLong2, long paramLong3);

  private static native double[] mean_0(long paramLong1, long paramLong2);

  private static native double[] mean_1(long paramLong);

  public static void merge(List<Mat> paramList, Mat paramMat)
  {
    merge_0(Converters.vector_Mat_to_Mat(paramList).nativeObj, paramMat.nativeObj);
  }

  private static native void merge_0(long paramLong1, long paramLong2);

  public static void min(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    min_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static MinMaxLocResult minMaxLoc(Mat paramMat)
  {
    return minMaxLoc(paramMat, null);
  }

  public static MinMaxLocResult minMaxLoc(Mat paramMat1, Mat paramMat2)
  {
    MinMaxLocResult localMinMaxLocResult = new MinMaxLocResult();
    long l = 0L;
    if (paramMat2 != null)
      l = paramMat2.nativeObj;
    double[] arrayOfDouble = n_minMaxLocManual(paramMat1.nativeObj, l);
    localMinMaxLocResult.minVal = arrayOfDouble[0];
    localMinMaxLocResult.maxVal = arrayOfDouble[1];
    localMinMaxLocResult.minLoc.x = arrayOfDouble[2];
    localMinMaxLocResult.minLoc.y = arrayOfDouble[3];
    localMinMaxLocResult.maxLoc.x = arrayOfDouble[4];
    localMinMaxLocResult.maxLoc.y = arrayOfDouble[5];
    return localMinMaxLocResult;
  }

  private static native void min_0(long paramLong1, long paramLong2, long paramLong3);

  public static void mixChannels(List<Mat> paramList1, List<Mat> paramList2, List<Integer> paramList)
  {
    Mat localMat1 = Converters.vector_Mat_to_Mat(paramList1);
    Mat localMat2 = Converters.vector_Mat_to_Mat(paramList2);
    Mat localMat3 = Converters.vector_int_to_Mat(paramList);
    mixChannels_0(localMat1.nativeObj, localMat2.nativeObj, localMat3.nativeObj);
  }

  private static native void mixChannels_0(long paramLong1, long paramLong2, long paramLong3);

  public static void mulSpectrums(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    mulSpectrums_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  public static void mulSpectrums(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt, boolean paramBoolean)
  {
    mulSpectrums_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt, paramBoolean);
  }

  private static native void mulSpectrums_0(long paramLong1, long paramLong2, long paramLong3, int paramInt, boolean paramBoolean);

  private static native void mulSpectrums_1(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  public static void mulTransposed(Mat paramMat1, Mat paramMat2, boolean paramBoolean)
  {
    mulTransposed_3(paramMat1.nativeObj, paramMat2.nativeObj, paramBoolean);
  }

  public static void mulTransposed(Mat paramMat1, Mat paramMat2, boolean paramBoolean, Mat paramMat3)
  {
    mulTransposed_2(paramMat1.nativeObj, paramMat2.nativeObj, paramBoolean, paramMat3.nativeObj);
  }

  public static void mulTransposed(Mat paramMat1, Mat paramMat2, boolean paramBoolean, Mat paramMat3, double paramDouble)
  {
    mulTransposed_1(paramMat1.nativeObj, paramMat2.nativeObj, paramBoolean, paramMat3.nativeObj, paramDouble);
  }

  public static void mulTransposed(Mat paramMat1, Mat paramMat2, boolean paramBoolean, Mat paramMat3, double paramDouble, int paramInt)
  {
    mulTransposed_0(paramMat1.nativeObj, paramMat2.nativeObj, paramBoolean, paramMat3.nativeObj, paramDouble, paramInt);
  }

  private static native void mulTransposed_0(long paramLong1, long paramLong2, boolean paramBoolean, long paramLong3, double paramDouble, int paramInt);

  private static native void mulTransposed_1(long paramLong1, long paramLong2, boolean paramBoolean, long paramLong3, double paramDouble);

  private static native void mulTransposed_2(long paramLong1, long paramLong2, boolean paramBoolean, long paramLong3);

  private static native void mulTransposed_3(long paramLong1, long paramLong2, boolean paramBoolean);

  public static void multiply(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    multiply_2(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void multiply(Mat paramMat1, Mat paramMat2, Mat paramMat3, double paramDouble)
  {
    multiply_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramDouble);
  }

  public static void multiply(Mat paramMat1, Mat paramMat2, Mat paramMat3, double paramDouble, int paramInt)
  {
    multiply_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramDouble, paramInt);
  }

  private static native void multiply_0(long paramLong1, long paramLong2, long paramLong3, double paramDouble, int paramInt);

  private static native void multiply_1(long paramLong1, long paramLong2, long paramLong3, double paramDouble);

  private static native void multiply_2(long paramLong1, long paramLong2, long paramLong3);

  private static native double[] n_getTextSize(String paramString, int paramInt1, double paramDouble, int paramInt2, int[] paramArrayOfInt);

  private static native double[] n_minMaxLocManual(long paramLong1, long paramLong2);

  public static double norm(Mat paramMat)
  {
    return norm_2(paramMat.nativeObj);
  }

  public static double norm(Mat paramMat, int paramInt)
  {
    return norm_1(paramMat.nativeObj, paramInt);
  }

  public static double norm(Mat paramMat1, int paramInt, Mat paramMat2)
  {
    return norm_0(paramMat1.nativeObj, paramInt, paramMat2.nativeObj);
  }

  public static double norm(Mat paramMat1, Mat paramMat2)
  {
    return norm_5(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static double norm(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    return norm_4(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  public static double norm(Mat paramMat1, Mat paramMat2, int paramInt, Mat paramMat3)
  {
    return norm_3(paramMat1.nativeObj, paramMat2.nativeObj, paramInt, paramMat3.nativeObj);
  }

  private static native double norm_0(long paramLong1, int paramInt, long paramLong2);

  private static native double norm_1(long paramLong, int paramInt);

  private static native double norm_2(long paramLong);

  private static native double norm_3(long paramLong1, long paramLong2, int paramInt, long paramLong3);

  private static native double norm_4(long paramLong1, long paramLong2, int paramInt);

  private static native double norm_5(long paramLong1, long paramLong2);

  public static void normalize(Mat paramMat1, Mat paramMat2)
  {
    normalize_5(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static void normalize(Mat paramMat1, Mat paramMat2, double paramDouble)
  {
    normalize_4(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble);
  }

  public static void normalize(Mat paramMat1, Mat paramMat2, double paramDouble1, double paramDouble2)
  {
    normalize_3(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramDouble2);
  }

  public static void normalize(Mat paramMat1, Mat paramMat2, double paramDouble1, double paramDouble2, int paramInt)
  {
    normalize_2(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramDouble2, paramInt);
  }

  public static void normalize(Mat paramMat1, Mat paramMat2, double paramDouble1, double paramDouble2, int paramInt1, int paramInt2)
  {
    normalize_1(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramDouble2, paramInt1, paramInt2);
  }

  public static void normalize(Mat paramMat1, Mat paramMat2, double paramDouble1, double paramDouble2, int paramInt1, int paramInt2, Mat paramMat3)
  {
    normalize_0(paramMat1.nativeObj, paramMat2.nativeObj, paramDouble1, paramDouble2, paramInt1, paramInt2, paramMat3.nativeObj);
  }

  private static native void normalize_0(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, int paramInt1, int paramInt2, long paramLong3);

  private static native void normalize_1(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, int paramInt1, int paramInt2);

  private static native void normalize_2(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2, int paramInt);

  private static native void normalize_3(long paramLong1, long paramLong2, double paramDouble1, double paramDouble2);

  private static native void normalize_4(long paramLong1, long paramLong2, double paramDouble);

  private static native void normalize_5(long paramLong1, long paramLong2);

  public static void perspectiveTransform(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    perspectiveTransform_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void perspectiveTransform_0(long paramLong1, long paramLong2, long paramLong3);

  public static void phase(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    phase_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void phase(Mat paramMat1, Mat paramMat2, Mat paramMat3, boolean paramBoolean)
  {
    phase_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramBoolean);
  }

  private static native void phase_0(long paramLong1, long paramLong2, long paramLong3, boolean paramBoolean);

  private static native void phase_1(long paramLong1, long paramLong2, long paramLong3);

  public static void polarToCart(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    polarToCart_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  public static void polarToCart(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, boolean paramBoolean)
  {
    polarToCart_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramBoolean);
  }

  private static native void polarToCart_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, boolean paramBoolean);

  private static native void polarToCart_1(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  public static void polylines(Mat paramMat, List<List<Point>> paramList, boolean paramBoolean, Scalar paramScalar)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, new ArrayList(i));
      polylines_3(paramMat.nativeObj, localMat.nativeObj, paramBoolean, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
      return;
    }
  }

  public static void polylines(Mat paramMat, List<List<Point>> paramList, boolean paramBoolean, Scalar paramScalar, int paramInt)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      ArrayList localArrayList = new ArrayList(i);
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, localArrayList);
      polylines_2(paramMat.nativeObj, localMat.nativeObj, paramBoolean, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
      return;
    }
  }

  public static void polylines(Mat paramMat, List<List<Point>> paramList, boolean paramBoolean, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      ArrayList localArrayList = new ArrayList(i);
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, localArrayList);
      polylines_1(paramMat.nativeObj, localMat.nativeObj, paramBoolean, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
      return;
    }
  }

  public static void polylines(Mat paramMat, List<List<Point>> paramList, boolean paramBoolean, Scalar paramScalar, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      ArrayList localArrayList = new ArrayList(i);
      Mat localMat = Converters.vector_vector_Point_to_Mat(paramList, localArrayList);
      polylines_0(paramMat.nativeObj, localMat.nativeObj, paramBoolean, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2, paramInt3);
      return;
    }
  }

  private static native void polylines_0(long paramLong1, long paramLong2, boolean paramBoolean, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2, int paramInt3);

  private static native void polylines_1(long paramLong1, long paramLong2, boolean paramBoolean, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt1, int paramInt2);

  private static native void polylines_2(long paramLong1, long paramLong2, boolean paramBoolean, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt);

  private static native void polylines_3(long paramLong1, long paramLong2, boolean paramBoolean, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);

  public static void pow(Mat paramMat1, double paramDouble, Mat paramMat2)
  {
    pow_0(paramMat1.nativeObj, paramDouble, paramMat2.nativeObj);
  }

  private static native void pow_0(long paramLong1, double paramDouble, long paramLong2);

  public static void putText(Mat paramMat, String paramString, Point paramPoint, int paramInt, double paramDouble, Scalar paramScalar)
  {
    putText_3(paramMat.nativeObj, paramString, paramPoint.x, paramPoint.y, paramInt, paramDouble, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void putText(Mat paramMat, String paramString, Point paramPoint, int paramInt1, double paramDouble, Scalar paramScalar, int paramInt2)
  {
    putText_2(paramMat.nativeObj, paramString, paramPoint.x, paramPoint.y, paramInt1, paramDouble, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2);
  }

  public static void putText(Mat paramMat, String paramString, Point paramPoint, int paramInt1, double paramDouble, Scalar paramScalar, int paramInt2, int paramInt3)
  {
    putText_1(paramMat.nativeObj, paramString, paramPoint.x, paramPoint.y, paramInt1, paramDouble, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2, paramInt3);
  }

  public static void putText(Mat paramMat, String paramString, Point paramPoint, int paramInt1, double paramDouble, Scalar paramScalar, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    putText_0(paramMat.nativeObj, paramString, paramPoint.x, paramPoint.y, paramInt1, paramDouble, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt2, paramInt3, paramBoolean);
  }

  private static native void putText_0(long paramLong, String paramString, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, int paramInt2, int paramInt3, boolean paramBoolean);

  private static native void putText_1(long paramLong, String paramString, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, int paramInt2, int paramInt3);

  private static native void putText_2(long paramLong, String paramString, double paramDouble1, double paramDouble2, int paramInt1, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, int paramInt2);

  private static native void putText_3(long paramLong, String paramString, double paramDouble1, double paramDouble2, int paramInt, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7);

  public static void randShuffle(Mat paramMat)
  {
    randShuffle_1(paramMat.nativeObj);
  }

  public static void randShuffle(Mat paramMat, double paramDouble)
  {
    randShuffle_0(paramMat.nativeObj, paramDouble);
  }

  private static native void randShuffle_0(long paramLong, double paramDouble);

  private static native void randShuffle_1(long paramLong);

  public static void randn(Mat paramMat, double paramDouble1, double paramDouble2)
  {
    randn_0(paramMat.nativeObj, paramDouble1, paramDouble2);
  }

  private static native void randn_0(long paramLong, double paramDouble1, double paramDouble2);

  public static void randu(Mat paramMat, double paramDouble1, double paramDouble2)
  {
    randu_0(paramMat.nativeObj, paramDouble1, paramDouble2);
  }

  private static native void randu_0(long paramLong, double paramDouble1, double paramDouble2);

  public static void rectangle(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar)
  {
    rectangle_3(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  public static void rectangle(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt)
  {
    rectangle_2(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt);
  }

  public static void rectangle(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt1, int paramInt2)
  {
    rectangle_1(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2);
  }

  public static void rectangle(Mat paramMat, Point paramPoint1, Point paramPoint2, Scalar paramScalar, int paramInt1, int paramInt2, int paramInt3)
  {
    rectangle_0(paramMat.nativeObj, paramPoint1.x, paramPoint1.y, paramPoint2.x, paramPoint2.y, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3], paramInt1, paramInt2, paramInt3);
  }

  private static native void rectangle_0(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt1, int paramInt2, int paramInt3);

  private static native void rectangle_1(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt1, int paramInt2);

  private static native void rectangle_2(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8, int paramInt);

  private static native void rectangle_3(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, double paramDouble5, double paramDouble6, double paramDouble7, double paramDouble8);

  public static void reduce(Mat paramMat1, Mat paramMat2, int paramInt1, int paramInt2)
  {
    reduce_1(paramMat1.nativeObj, paramMat2.nativeObj, paramInt1, paramInt2);
  }

  public static void reduce(Mat paramMat1, Mat paramMat2, int paramInt1, int paramInt2, int paramInt3)
  {
    reduce_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt1, paramInt2, paramInt3);
  }

  private static native void reduce_0(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3);

  private static native void reduce_1(long paramLong1, long paramLong2, int paramInt1, int paramInt2);

  public static void repeat(Mat paramMat1, int paramInt1, int paramInt2, Mat paramMat2)
  {
    repeat_0(paramMat1.nativeObj, paramInt1, paramInt2, paramMat2.nativeObj);
  }

  private static native void repeat_0(long paramLong1, int paramInt1, int paramInt2, long paramLong2);

  public static void scaleAdd(Mat paramMat1, double paramDouble, Mat paramMat2, Mat paramMat3)
  {
    scaleAdd_0(paramMat1.nativeObj, paramDouble, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void scaleAdd_0(long paramLong1, double paramDouble, long paramLong2, long paramLong3);

  public static void setIdentity(Mat paramMat)
  {
    setIdentity_1(paramMat.nativeObj);
  }

  public static void setIdentity(Mat paramMat, Scalar paramScalar)
  {
    setIdentity_0(paramMat.nativeObj, paramScalar.val[0], paramScalar.val[1], paramScalar.val[2], paramScalar.val[3]);
  }

  private static native void setIdentity_0(long paramLong, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4);

  private static native void setIdentity_1(long paramLong);

  public static boolean solve(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    return solve_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static boolean solve(Mat paramMat1, Mat paramMat2, Mat paramMat3, int paramInt)
  {
    return solve_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramInt);
  }

  public static int solveCubic(Mat paramMat1, Mat paramMat2)
  {
    return solveCubic_0(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  private static native int solveCubic_0(long paramLong1, long paramLong2);

  public static double solvePoly(Mat paramMat1, Mat paramMat2)
  {
    return solvePoly_1(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  public static double solvePoly(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    return solvePoly_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native double solvePoly_0(long paramLong1, long paramLong2, int paramInt);

  private static native double solvePoly_1(long paramLong1, long paramLong2);

  private static native boolean solve_0(long paramLong1, long paramLong2, long paramLong3, int paramInt);

  private static native boolean solve_1(long paramLong1, long paramLong2, long paramLong3);

  public static void sort(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    sort_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  public static void sortIdx(Mat paramMat1, Mat paramMat2, int paramInt)
  {
    sortIdx_0(paramMat1.nativeObj, paramMat2.nativeObj, paramInt);
  }

  private static native void sortIdx_0(long paramLong1, long paramLong2, int paramInt);

  private static native void sort_0(long paramLong1, long paramLong2, int paramInt);

  public static void split(Mat paramMat, List<Mat> paramList)
  {
    Mat localMat = new Mat();
    split_0(paramMat.nativeObj, localMat.nativeObj);
    Converters.Mat_to_vector_Mat(localMat, paramList);
  }

  private static native void split_0(long paramLong1, long paramLong2);

  public static void sqrt(Mat paramMat1, Mat paramMat2)
  {
    sqrt_0(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  private static native void sqrt_0(long paramLong1, long paramLong2);

  public static void subtract(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    subtract_2(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  public static void subtract(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4)
  {
    subtract_1(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj);
  }

  public static void subtract(Mat paramMat1, Mat paramMat2, Mat paramMat3, Mat paramMat4, int paramInt)
  {
    subtract_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj, paramMat4.nativeObj, paramInt);
  }

  private static native void subtract_0(long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt);

  private static native void subtract_1(long paramLong1, long paramLong2, long paramLong3, long paramLong4);

  private static native void subtract_2(long paramLong1, long paramLong2, long paramLong3);

  public static Scalar sumElems(Mat paramMat)
  {
    return new Scalar(sumElems_0(paramMat.nativeObj));
  }

  private static native double[] sumElems_0(long paramLong);

  public static Scalar trace(Mat paramMat)
  {
    return new Scalar(trace_0(paramMat.nativeObj));
  }

  private static native double[] trace_0(long paramLong);

  public static void transform(Mat paramMat1, Mat paramMat2, Mat paramMat3)
  {
    transform_0(paramMat1.nativeObj, paramMat2.nativeObj, paramMat3.nativeObj);
  }

  private static native void transform_0(long paramLong1, long paramLong2, long paramLong3);

  public static void transpose(Mat paramMat1, Mat paramMat2)
  {
    transpose_0(paramMat1.nativeObj, paramMat2.nativeObj);
  }

  private static native void transpose_0(long paramLong1, long paramLong2);

  public static void vconcat(List<Mat> paramList, Mat paramMat)
  {
    vconcat_0(Converters.vector_Mat_to_Mat(paramList).nativeObj, paramMat.nativeObj);
  }

  private static native void vconcat_0(long paramLong1, long paramLong2);

  public static class MinMaxLocResult
  {
    public Point maxLoc = new Point();
    public double maxVal = 0.0D;
    public Point minLoc = new Point();
    public double minVal = 0.0D;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.core.Core
 * JD-Core Version:    0.5.4
 */