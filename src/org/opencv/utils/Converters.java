package org.opencv.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Rect;

public class Converters
{
  public static void Mat_to_vector_Mat(Mat paramMat, List<Mat> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("mats == null");
    int i = paramMat.rows();
    if ((CvType.CV_32SC2 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_32SC2 != m.type() ||  m.cols()!=1\n" + paramMat);
    paramList.clear();
    int[] arrayOfInt = new int[i * 2];
    paramMat.get(0, 0, arrayOfInt);
    for (int j = 0; j < i; ++j)
      paramList.add(new Mat(arrayOfInt[(j * 2)] << 32 | arrayOfInt[(1 + j * 2)]));
  }

  public static void Mat_to_vector_Point(Mat paramMat, List<Point> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    int i = paramMat.rows();
    int j = paramMat.type();
    if (paramMat.cols() != 1)
      throw new IllegalArgumentException("Input Mat should have one column\n" + paramMat);
    paramList.clear();
    if (j == CvType.CV_32SC2)
    {
      int[] arrayOfInt = new int[i * 2];
      paramMat.get(0, 0, arrayOfInt);
      for (int i1 = 0; ; ++i1)
      {
        if (i1 >= i)
          return;
        paramList.add(new Point(arrayOfInt[(i1 * 2)], arrayOfInt[(1 + i1 * 2)]));
      }
    }
    if (j == CvType.CV_32FC2)
    {
      float[] arrayOfFloat = new float[i * 2];
      paramMat.get(0, 0, arrayOfFloat);
      for (int l = 0; ; ++l)
      {
        if (l >= i)
          return;
        paramList.add(new Point(arrayOfFloat[(l * 2)], arrayOfFloat[(1 + l * 2)]));
      }
    }
    if (j == CvType.CV_64FC2)
    {
      double[] arrayOfDouble = new double[i * 2];
      paramMat.get(0, 0, arrayOfDouble);
      for (int k = 0; ; ++k)
      {
        if (k >= i)
          return;
        paramList.add(new Point(arrayOfDouble[(k * 2)], arrayOfDouble[(1 + k * 2)]));
      }
    }
    throw new IllegalArgumentException("Input Mat should be of CV_32SC2, CV_32FC2 or CV_64FC2 type\n" + paramMat);
  }

  public static void Mat_to_vector_Point2d(Mat paramMat, List<Point> paramList)
  {
    Mat_to_vector_Point(paramMat, paramList);
  }

  public static void Mat_to_vector_Point2f(Mat paramMat, List<Point> paramList)
  {
    Mat_to_vector_Point(paramMat, paramList);
  }

  public static void Mat_to_vector_Point3(Mat paramMat, List<Point3> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    int i = paramMat.rows();
    int j = paramMat.type();
    if (paramMat.cols() != 1)
      throw new IllegalArgumentException("Input Mat should have one column\n" + paramMat);
    paramList.clear();
    if (j == CvType.CV_32SC3)
    {
      int[] arrayOfInt = new int[i * 3];
      paramMat.get(0, 0, arrayOfInt);
      for (int i1 = 0; ; ++i1)
      {
        if (i1 >= i)
          return;
        paramList.add(new Point3(arrayOfInt[(i1 * 3)], arrayOfInt[(1 + i1 * 3)], arrayOfInt[(2 + i1 * 3)]));
      }
    }
    if (j == CvType.CV_32FC3)
    {
      float[] arrayOfFloat = new float[i * 3];
      paramMat.get(0, 0, arrayOfFloat);
      for (int l = 0; ; ++l)
      {
        if (l >= i)
          return;
        paramList.add(new Point3(arrayOfFloat[(l * 3)], arrayOfFloat[(1 + l * 3)], arrayOfFloat[(2 + l * 3)]));
      }
    }
    if (j == CvType.CV_64FC3)
    {
      double[] arrayOfDouble = new double[i * 3];
      paramMat.get(0, 0, arrayOfDouble);
      for (int k = 0; ; ++k)
      {
        if (k >= i)
          return;
        paramList.add(new Point3(arrayOfDouble[(k * 3)], arrayOfDouble[(1 + k * 3)], arrayOfDouble[(2 + k * 3)]));
      }
    }
    throw new IllegalArgumentException("Input Mat should be of CV_32SC3, CV_32FC3 or CV_64FC3 type\n" + paramMat);
  }

  public static void Mat_to_vector_Point3d(Mat paramMat, List<Point3> paramList)
  {
    Mat_to_vector_Point3(paramMat, paramList);
  }

  public static void Mat_to_vector_Point3f(Mat paramMat, List<Point3> paramList)
  {
    Mat_to_vector_Point3(paramMat, paramList);
  }

  public static void Mat_to_vector_Point3i(Mat paramMat, List<Point3> paramList)
  {
    Mat_to_vector_Point3(paramMat, paramList);
  }

  public static void Mat_to_vector_Rect(Mat paramMat, List<Rect> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("rs == null");
    int i = paramMat.rows();
    if ((CvType.CV_32SC4 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_32SC4 != m.type() ||  m.rows()!=1\n" + paramMat);
    paramList.clear();
    int[] arrayOfInt = new int[i * 4];
    paramMat.get(0, 0, arrayOfInt);
    for (int j = 0; j < i; ++j)
      paramList.add(new Rect(arrayOfInt[(j * 4)], arrayOfInt[(1 + j * 4)], arrayOfInt[(2 + j * 4)], arrayOfInt[(3 + j * 4)]));
  }

  public static void Mat_to_vector_char(Mat paramMat, List<Byte> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    int i = paramMat.rows();
    if ((CvType.CV_8SC1 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_8SC1 != m.type() ||  m.cols()!=1\n" + paramMat);
    paramList.clear();
    byte[] arrayOfByte = new byte[i];
    paramMat.get(0, 0, arrayOfByte);
    for (int j = 0; j < i; ++j)
      paramList.add(Byte.valueOf(arrayOfByte[j]));
  }

  public static void Mat_to_vector_float(Mat paramMat, List<Float> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("fs == null");
    int i = paramMat.rows();
    if ((CvType.CV_32FC1 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_32FC1 != m.type() ||  m.cols()!=1\n" + paramMat);
    paramList.clear();
    float[] arrayOfFloat = new float[i];
    paramMat.get(0, 0, arrayOfFloat);
    for (int j = 0; j < i; ++j)
      paramList.add(Float.valueOf(arrayOfFloat[j]));
  }

  public static void Mat_to_vector_int(Mat paramMat, List<Integer> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("is == null");
    int i = paramMat.rows();
    if ((CvType.CV_32SC1 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_32SC1 != m.type() ||  m.cols()!=1\n" + paramMat);
    paramList.clear();
    int[] arrayOfInt = new int[i];
    paramMat.get(0, 0, arrayOfInt);
    for (int j = 0; j < i; ++j)
      paramList.add(Integer.valueOf(arrayOfInt[j]));
  }

  public static void Mat_to_vector_uchar(Mat paramMat, List<Byte> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    int i = paramMat.rows();
    if ((CvType.CV_8UC1 != paramMat.type()) || (paramMat.cols() != 1))
      throw new IllegalArgumentException("CvType.CV_8UC1 != m.type() ||  m.cols()!=1\n" + paramMat);
    paramList.clear();
    byte[] arrayOfByte = new byte[i];
    paramMat.get(0, 0, arrayOfByte);
    for (int j = 0; j < i; ++j)
      paramList.add(Byte.valueOf(arrayOfByte[j]));
  }

  public static void Mat_to_vector_vector_Point2f(Mat paramMat, List<List<Point>> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    if (paramMat == null)
      throw new IllegalArgumentException("Input Mat can't be null");
    ArrayList localArrayList1 = new ArrayList(paramMat.rows());
    Mat_to_vector_Mat(paramMat, localArrayList1);
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    while (localIterator.hasNext())
    {
      Mat_to_vector_Point2f((Mat)localIterator.next(), localArrayList2);
      paramList.add(localArrayList2);
    }
  }

  public static void Mat_to_vector_vector_char(Mat paramMat, List<List<Byte>> paramList)
  {
    if (paramList == null)
      throw new IllegalArgumentException("Output List can't be null");
    if (paramMat == null)
      throw new IllegalArgumentException("Input Mat can't be null");
    ArrayList localArrayList1 = new ArrayList(paramMat.rows());
    Mat_to_vector_Mat(paramMat, localArrayList1);
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    while (localIterator.hasNext())
    {
      Mat_to_vector_char((Mat)localIterator.next(), localArrayList2);
      paramList.add(localArrayList2);
    }
  }

  public static Mat vector_Mat_to_Mat(List<Mat> paramList)
  {
    if (paramList != null);
    Mat localMat;
    int[] arrayOfInt;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label107;
      localMat = new Mat(i, 1, CvType.CV_32SC2);
      arrayOfInt = new int[i * 2];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label97;
        long l = ((Mat)paramList.get(j)).nativeObj;
        arrayOfInt[(j * 2)] = (int)(l >> 32);
        arrayOfInt[(1 + j * 2)] = (int)(0xFFFFFFFF & l);
      }
    }
    label97: localMat.put(0, 0, arrayOfInt);
    return localMat;
    label107: return new Mat();
  }

  public static Mat vector_Point2d_to_Mat(List<Point> paramList)
  {
    return vector_Point_to_Mat(paramList, 6);
  }

  public static Mat vector_Point2f_to_Mat(List<Point> paramList)
  {
    return vector_Point_to_Mat(paramList, 5);
  }

  public static Mat vector_Point3_to_Mat(List<Point3> paramList, int paramInt)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label368;
      switch (paramInt)
      {
      default:
        throw new IllegalArgumentException("'typeDepth' can be CV_32S, CV_32F or CV_64F");
      case 4:
      case 5:
      case 6:
      }
    }
    Mat localMat3 = new Mat(i, 1, CvType.CV_32SC3);
    int[] arrayOfInt = new int[i * 3];
    for (int l = 0; l < i; ++l)
    {
      Point3 localPoint33 = (Point3)paramList.get(l);
      arrayOfInt[(l * 3)] = (int)localPoint33.x;
      arrayOfInt[(1 + l * 3)] = (int)localPoint33.y;
      arrayOfInt[(2 + l * 3)] = (int)localPoint33.z;
    }
    localMat3.put(0, 0, arrayOfInt);
    return localMat3;
    Mat localMat2 = new Mat(i, 1, CvType.CV_32FC3);
    float[] arrayOfFloat = new float[i * 3];
    for (int k = 0; k < i; ++k)
    {
      Point3 localPoint32 = (Point3)paramList.get(k);
      arrayOfFloat[(k * 3)] = (float)localPoint32.x;
      arrayOfFloat[(1 + k * 3)] = (float)localPoint32.y;
      arrayOfFloat[(2 + k * 3)] = (float)localPoint32.z;
    }
    localMat2.put(0, 0, arrayOfFloat);
    return localMat2;
    Mat localMat1 = new Mat(i, 1, CvType.CV_64FC3);
    double[] arrayOfDouble = new double[i * 3];
    for (int j = 0; j < i; ++j)
    {
      Point3 localPoint31 = (Point3)paramList.get(j);
      arrayOfDouble[(j * 3)] = localPoint31.x;
      arrayOfDouble[(1 + j * 3)] = localPoint31.y;
      arrayOfDouble[(2 + j * 3)] = localPoint31.z;
    }
    localMat1.put(0, 0, arrayOfDouble);
    return localMat1;
    label368: return new Mat();
  }

  public static Mat vector_Point3d_to_Mat(List<Point3> paramList)
  {
    return vector_Point3_to_Mat(paramList, 6);
  }

  public static Mat vector_Point3f_to_Mat(List<Point3> paramList)
  {
    return vector_Point3_to_Mat(paramList, 5);
  }

  public static Mat vector_Point3i_to_Mat(List<Point3> paramList)
  {
    return vector_Point3_to_Mat(paramList, 4);
  }

  public static Mat vector_Point_to_Mat(List<Point> paramList)
  {
    return vector_Point_to_Mat(paramList, 4);
  }

  public static Mat vector_Point_to_Mat(List<Point> paramList, int paramInt)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label324;
      switch (paramInt)
      {
      default:
        throw new IllegalArgumentException("'typeDepth' can be CV_32S, CV_32F or CV_64F");
      case 4:
      case 5:
      case 6:
      }
    }
    Mat localMat3 = new Mat(i, 1, CvType.CV_32SC2);
    int[] arrayOfInt = new int[i * 2];
    for (int l = 0; l < i; ++l)
    {
      Point localPoint3 = (Point)paramList.get(l);
      arrayOfInt[(l * 2)] = (int)localPoint3.x;
      arrayOfInt[(1 + l * 2)] = (int)localPoint3.y;
    }
    localMat3.put(0, 0, arrayOfInt);
    return localMat3;
    Mat localMat2 = new Mat(i, 1, CvType.CV_32FC2);
    float[] arrayOfFloat = new float[i * 2];
    for (int k = 0; k < i; ++k)
    {
      Point localPoint2 = (Point)paramList.get(k);
      arrayOfFloat[(k * 2)] = (float)localPoint2.x;
      arrayOfFloat[(1 + k * 2)] = (float)localPoint2.y;
    }
    localMat2.put(0, 0, arrayOfFloat);
    return localMat2;
    Mat localMat1 = new Mat(i, 1, CvType.CV_64FC2);
    double[] arrayOfDouble = new double[i * 2];
    for (int j = 0; j < i; ++j)
    {
      Point localPoint1 = (Point)paramList.get(j);
      arrayOfDouble[(j * 2)] = localPoint1.x;
      arrayOfDouble[(1 + j * 2)] = localPoint1.y;
    }
    localMat1.put(0, 0, arrayOfDouble);
    return localMat1;
    label324: return new Mat();
  }

  public static Mat vector_Rect_to_Mat(List<Rect> paramList)
  {
    if (paramList != null);
    Mat localMat;
    int[] arrayOfInt;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label127;
      localMat = new Mat(i, 1, CvType.CV_32SC4);
      arrayOfInt = new int[i * 4];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label117;
        Rect localRect = (Rect)paramList.get(j);
        arrayOfInt[(j * 4)] = localRect.x;
        arrayOfInt[(1 + j * 4)] = localRect.y;
        arrayOfInt[(2 + j * 4)] = localRect.width;
        arrayOfInt[(3 + j * 4)] = localRect.height;
      }
    }
    label117: localMat.put(0, 0, arrayOfInt);
    return localMat;
    label127: return new Mat();
  }

  public static Mat vector_char_to_Mat(List<Byte> paramList)
  {
    if (paramList != null);
    Mat localMat;
    byte[] arrayOfByte;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label80;
      localMat = new Mat(i, 1, CvType.CV_8SC1);
      arrayOfByte = new byte[i];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label70;
        arrayOfByte[j] = ((Byte)paramList.get(j)).byteValue();
      }
    }
    label70: localMat.put(0, 0, arrayOfByte);
    return localMat;
    label80: return new Mat();
  }

  public static Mat vector_double_to_Mat(List<Double> paramList)
  {
    if (paramList != null);
    Mat localMat;
    double[] arrayOfDouble;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label80;
      localMat = new Mat(i, 1, CvType.CV_64FC1);
      arrayOfDouble = new double[i];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label70;
        arrayOfDouble[j] = ((Double)paramList.get(j)).doubleValue();
      }
    }
    label70: localMat.put(0, 0, arrayOfDouble);
    return localMat;
    label80: return new Mat();
  }

  public static Mat vector_float_to_Mat(List<Float> paramList)
  {
    if (paramList != null);
    Mat localMat;
    float[] arrayOfFloat;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label80;
      localMat = new Mat(i, 1, CvType.CV_32FC1);
      arrayOfFloat = new float[i];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label70;
        arrayOfFloat[j] = ((Float)paramList.get(j)).floatValue();
      }
    }
    label70: localMat.put(0, 0, arrayOfFloat);
    return localMat;
    label80: return new Mat();
  }

  public static Mat vector_int_to_Mat(List<Integer> paramList)
  {
    if (paramList != null);
    Mat localMat;
    int[] arrayOfInt;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label80;
      localMat = new Mat(i, 1, CvType.CV_32SC1);
      arrayOfInt = new int[i];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label70;
        arrayOfInt[j] = ((Integer)paramList.get(j)).intValue();
      }
    }
    label70: localMat.put(0, 0, arrayOfInt);
    return localMat;
    label80: return new Mat();
  }

  public static Mat vector_uchar_to_Mat(List<Byte> paramList)
  {
    if (paramList != null);
    Mat localMat;
    byte[] arrayOfByte;
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label80;
      localMat = new Mat(i, 1, CvType.CV_8UC1);
      arrayOfByte = new byte[i];
      for (int j = 0; ; ++j)
      {
        if (j >= i)
          break label70;
        arrayOfByte[j] = ((Byte)paramList.get(j)).byteValue();
      }
    }
    label70: localMat.put(0, 0, arrayOfByte);
    return localMat;
    label80: return new Mat();
  }

  public static Mat vector_vector_Point_to_Mat(List<List<Point>> paramList, List<Mat> paramList1)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label63;
      Iterator localIterator = paramList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          break label58;
        paramList1.add(vector_Point_to_Mat((List)localIterator.next()));
      }
    }
    label58: return vector_Mat_to_Mat(paramList1);
    label63: return new Mat();
  }

  public static Mat vector_vector_char_to_Mat(List<List<Byte>> paramList, List<Mat> paramList1)
  {
    if (paramList != null);
    for (int i = paramList.size(); ; i = 0)
    {
      if (i <= 0)
        break label63;
      Iterator localIterator = paramList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          break label58;
        paramList1.add(vector_char_to_Mat((List)localIterator.next()));
      }
    }
    label58: return vector_Mat_to_Mat(paramList1);
    label63: return new Mat();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.opencv.utils.Converters
 * JD-Core Version:    0.5.4
 */