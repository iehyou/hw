package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public class DoctypeToken
  implements BaseToken
{
  private String part1;
  private String part2;
  private String part3;
  private String part4;

  public DoctypeToken(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    if (paramString1 != null)
      paramString1 = paramString1.toUpperCase();
    this.part1 = paramString1;
    if (paramString2 != null)
      paramString2 = paramString2.toUpperCase();
    this.part2 = paramString2;
    this.part3 = clean(paramString3);
    this.part4 = clean(paramString4);
  }

  private String clean(String paramString)
  {
    if (paramString != null)
      paramString = paramString.replace('>', ' ').replace('<', ' ').replace('&', ' ').replace('\'', ' ').replace('"', ' ');
    return paramString;
  }

  public String getContent()
  {
    String str1 = "<!DOCTYPE " + this.part1 + " ";
    String str2 = str1 + this.part2 + " \"" + this.part3 + "\"";
    if ((this.part4 != null) && (!"".equals(this.part4)))
      str2 = str2 + " \"" + this.part4 + "\"";
    return str2 + ">";
  }

  public String getName()
  {
    return "";
  }

  public String getPart1()
  {
    return this.part1;
  }

  public String getPart2()
  {
    return this.part2;
  }

  public String getPart3()
  {
    return this.part3;
  }

  public String getPart4()
  {
    return this.part4;
  }

  public boolean isValid()
  {
    if ((this.part1 == null) || ("".equals(this.part1)));
    do
      return false;
    while (((!"public".equalsIgnoreCase(this.part2)) && (!"system".equalsIgnoreCase(this.part2))) || (("system".equalsIgnoreCase(this.part2)) && (this.part4 != null) && (!"".equals(this.part4))) || (("public".equalsIgnoreCase(this.part2)) && (((this.part4 == null) || ("".equals(this.part4))))));
    return true;
  }

  public void serialize(Serializer paramSerializer, Writer paramWriter)
    throws IOException
  {
    paramWriter.write(getContent() + "\n");
  }

  public String toString()
  {
    return getContent();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.DoctypeToken
 * JD-Core Version:    0.5.4
 */