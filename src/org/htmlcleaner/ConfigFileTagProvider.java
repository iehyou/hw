package org.htmlcleaner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigFileTagProvider extends HashMap
  implements ITagInfoProvider
{
  static SAXParserFactory parserFactory = SAXParserFactory.newInstance();
  private boolean generateCode = false;

  static
  {
    parserFactory.setValidating(false);
    parserFactory.setNamespaceAware(false);
  }

  private ConfigFileTagProvider()
  {
  }

  public ConfigFileTagProvider(File paramFile)
  {
    try
    {
      new ConfigParser(this).parse(new InputSource(new FileReader(paramFile)));
      return;
    }
    catch (Exception localException)
    {
      throw new HtmlCleanerException("Error parsing tag configuration file!", localException);
    }
  }

  public ConfigFileTagProvider(URL paramURL)
  {
    try
    {
      Object localObject = paramURL.getContent();
      if (localObject instanceof InputStream)
      {
        InputStreamReader localInputStreamReader = new InputStreamReader((InputStream)localObject);
        new ConfigParser(this).parse(new InputSource(localInputStreamReader));
      }
      return;
    }
    catch (Exception localException)
    {
      throw new HtmlCleanerException("Error parsing tag configuration file!", localException);
    }
  }

  public ConfigFileTagProvider(InputSource paramInputSource)
  {
    try
    {
      new ConfigParser(this).parse(paramInputSource);
      return;
    }
    catch (Exception localException)
    {
      throw new HtmlCleanerException("Error parsing tag configuration file!", localException);
    }
  }

  public static void main(String[] paramArrayOfString)
    throws IOException, SAXException, ParserConfigurationException
  {
    ConfigFileTagProvider localConfigFileTagProvider = new ConfigFileTagProvider();
    localConfigFileTagProvider.generateCode = true;
    File localFile = new File("default.xml");
    localConfigFileTagProvider.getClass();
    ConfigParser localConfigParser = new ConfigParser(localConfigFileTagProvider);
    System.out.println("package " + "org.htmlcleaner" + ";");
    System.out.println("import java.util.HashMap;");
    System.out.println("public class " + "DefaultTagProvider" + " extends HashMap implements ITagInfoProvider {");
    System.out.println("public " + "DefaultTagProvider" + "() {");
    System.out.println("TagInfo tagInfo;");
    localConfigParser.parse(new InputSource(new FileReader(localFile)));
    System.out.println("}");
    System.out.println("}");
  }

  public TagInfo getTagInfo(String paramString)
  {
    return (TagInfo)get(paramString);
  }

  private class ConfigParser extends DefaultHandler
  {
    private String dependencyName = null;
    private TagInfo tagInfo = null;
    private Map tagInfoMap;

    ConfigParser(Map arg2)
    {
      Object localObject;
      this.tagInfoMap = localObject;
    }

    public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
      throws SAXException
    {
      String str;
      if (this.tagInfo != null)
      {
        str = new String(paramArrayOfChar, paramInt1, paramInt2).trim();
        if (!"fatal-tags".equals(this.dependencyName))
          break label85;
        this.tagInfo.defineFatalTags(str);
        if (ConfigFileTagProvider.this.generateCode)
          System.out.println("tagInfo.defineFatalTags(\"" + str + "\");");
      }
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      return;
                      label85: if (!"req-enclosing-tags".equals(this.dependencyName))
                        break label148;
                      this.tagInfo.defineRequiredEnclosingTags(str);
                    }
                    while (!ConfigFileTagProvider.this.generateCode);
                    System.out.println("tagInfo.defineRequiredEnclosingTags(\"" + str + "\");");
                    return;
                    label148: if (!"forbidden-tags".equals(this.dependencyName))
                      break label211;
                    this.tagInfo.defineForbiddenTags(str);
                  }
                  while (!ConfigFileTagProvider.this.generateCode);
                  System.out.println("tagInfo.defineForbiddenTags(\"" + str + "\");");
                  return;
                  label211: if (!"allowed-children-tags".equals(this.dependencyName))
                    break label274;
                  this.tagInfo.defineAllowedChildrenTags(str);
                }
                while (!ConfigFileTagProvider.this.generateCode);
                System.out.println("tagInfo.defineAllowedChildrenTags(\"" + str + "\");");
                return;
                label274: if (!"higher-level-tags".equals(this.dependencyName))
                  break label337;
                this.tagInfo.defineHigherLevelTags(str);
              }
              while (!ConfigFileTagProvider.this.generateCode);
              System.out.println("tagInfo.defineHigherLevelTags(\"" + str + "\");");
              return;
              label337: if (!"close-before-copy-inside-tags".equals(this.dependencyName))
                break label400;
              this.tagInfo.defineCloseBeforeCopyInsideTags(str);
            }
            while (!ConfigFileTagProvider.this.generateCode);
            System.out.println("tagInfo.defineCloseBeforeCopyInsideTags(\"" + str + "\");");
            return;
            label400: if (!"close-inside-copy-after-tags".equals(this.dependencyName))
              break label463;
            this.tagInfo.defineCloseInsideCopyAfterTags(str);
          }
          while (!ConfigFileTagProvider.this.generateCode);
          System.out.println("tagInfo.defineCloseInsideCopyAfterTags(\"" + str + "\");");
          label463: return;
        }
        while (!"close-before-tags".equals(this.dependencyName));
        this.tagInfo.defineCloseBeforeTags(str);
      }
      while (!ConfigFileTagProvider.this.generateCode);
      System.out.println("tagInfo.defineCloseBeforeTags(\"" + str + "\");");
    }

    public void endElement(String paramString1, String paramString2, String paramString3)
      throws SAXException
    {
      if ("tag".equals(paramString3))
      {
        if (this.tagInfo != null)
        {
          this.tagInfoMap.put(this.tagInfo.getName(), this.tagInfo);
          if (ConfigFileTagProvider.this.generateCode)
            System.out.println("this.put(\"" + this.tagInfo.getName() + "\", tagInfo);\n");
        }
        this.tagInfo = null;
      }
      do
        return;
      while ("tags".equals(paramString3));
      this.dependencyName = null;
    }

    public void parse(InputSource paramInputSource)
      throws ParserConfigurationException, SAXException, IOException
    {
      ConfigFileTagProvider.parserFactory.newSAXParser().parse(paramInputSource, this);
    }

    public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
      throws SAXException
    {
      String str2;
      String str3;
      int i;
      label88: int j;
      label101: boolean bool1;
      label119: boolean bool2;
      label137: boolean bool3;
      label155: String str8;
      label213: String str10;
      label238: boolean bool4;
      label267: boolean bool5;
      label299: boolean bool6;
      if ("tag".equals(paramString3))
      {
        String str1 = paramAttributes.getValue("name");
        str2 = paramAttributes.getValue("content");
        str3 = paramAttributes.getValue("section");
        String str4 = paramAttributes.getValue("deprecated");
        String str5 = paramAttributes.getValue("unique");
        String str6 = paramAttributes.getValue("ignore-permitted");
        if ("all".equals(str2))
        {
          i = 0;
          if (!"all".equals(str3))
            break label376;
          j = 0;
          if ((str4 == null) || (!"true".equals(str4)))
            break label398;
          bool1 = true;
          if ((str5 == null) || (!"true".equals(str5)))
            break label404;
          bool2 = true;
          if ((str6 == null) || (!"true".equals(str6)))
            break label410;
          bool3 = true;
          this.tagInfo = new TagInfo(str1, i, j, bool1, bool2, bool3);
          if (ConfigFileTagProvider.this.generateCode)
          {
            String str7 = "tagInfo = new TagInfo(\"#1\", #2, #3, #4, #5, #6);".replaceAll("#1", str1);
            if (!"all".equals(str2))
              break label416;
            str8 = "TagInfo.CONTENT_ALL";
            String str9 = str7.replaceAll("#2", str8);
            if (!"all".equals(str3))
              break label440;
            str10 = "TagInfo.HEAD_AND_BODY";
            String str11 = str9.replaceAll("#3", str10);
            if ((str4 == null) || (!"true".equals(str4)))
              break label464;
            bool4 = true;
            String str12 = str11.replaceAll("#4", Boolean.toString(bool4));
            if ((str5 == null) || (!"true".equals(str5)))
              break label470;
            bool5 = true;
            String str13 = str12.replaceAll("#5", Boolean.toString(bool5));
            if ((str6 == null) || (!"true".equals(str6)))
              break label476;
            bool6 = true;
            label331: String str14 = str13.replaceAll("#6", Boolean.toString(bool6));
            System.out.println(str14);
          }
        }
      }
      do
      {
        return;
        if ("none".equals(str2))
          i = 1;
        i = 2;
        break label88:
        if ("head".equals(str3))
          label376: j = 1;
        j = 2;
        break label101:
        label398: bool1 = false;
        break label119:
        label404: bool2 = false;
        break label137:
        label410: bool3 = false;
        break label155:
        if ("none".equals(str2))
          label416: str8 = "TagInfo.CONTENT_NONE";
        str8 = " TagInfo.CONTENT_TEXT";
        break label213:
        if ("head".equals(str3))
          label440: str10 = "TagInfo.HEAD";
        str10 = "TagInfo.BODY";
        break label238:
        label464: bool4 = false;
        break label267:
        label470: bool5 = false;
        break label299:
        label476: bool6 = false;
        break label331:
      }
      while ("tags".equals(paramString3));
      this.dependencyName = paramString3;
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.ConfigFileTagProvider
 * JD-Core Version:    0.5.4
 */