package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public class CommentNode
  implements BaseToken, HtmlNode
{
  private StringBuilder content;

  public CommentNode(String paramString)
  {
    this.content = new StringBuilder(paramString);
  }

  public String getCommentedContent()
  {
    return "<!--" + this.content + "-->";
  }

  public StringBuilder getContent()
  {
    return this.content;
  }

  public void serialize(Serializer paramSerializer, Writer paramWriter)
    throws IOException
  {
    paramWriter.write(getCommentedContent());
  }

  public String toString()
  {
    return getCommentedContent();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CommentNode
 * JD-Core Version:    0.5.4
 */