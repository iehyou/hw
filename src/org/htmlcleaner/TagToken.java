package org.htmlcleaner;

public abstract class TagToken
  implements BaseToken
{
  protected String name;

  public TagToken()
  {
  }

  public TagToken(String paramString)
  {
    this.name = paramString;
  }

  public String getName()
  {
    return this.name;
  }

  abstract void setAttribute(String paramString1, String paramString2);

  public String toString()
  {
    return this.name;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.TagToken
 * JD-Core Version:    0.5.4
 */