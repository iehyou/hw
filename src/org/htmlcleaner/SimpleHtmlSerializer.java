package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

public class SimpleHtmlSerializer extends HtmlSerializer
{
  public SimpleHtmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializeOpenTag(paramTagNode, paramWriter, false);
    if (isMinimizedTagSyntax(paramTagNode))
      return;
    Iterator localIterator = paramTagNode.getChildren().iterator();
    while (localIterator.hasNext())
    {
      label25: Object localObject = localIterator.next();
      if (localObject instanceof ContentNode)
      {
        String str = localObject.toString();
        if (dontEscape(paramTagNode));
        while (true)
        {
          paramWriter.write(str);
          break label25:
          str = escapeText(str);
        }
      }
      if (!localObject instanceof BaseToken)
        continue;
      ((BaseToken)localObject).serialize(this, paramWriter);
    }
    serializeEndTag(paramTagNode, paramWriter, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.SimpleHtmlSerializer
 * JD-Core Version:    0.5.4
 */