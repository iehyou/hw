package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class HtmlSerializer extends Serializer
{
  protected HtmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected boolean dontEscape(TagNode paramTagNode)
  {
    return isScriptOrStyle(paramTagNode);
  }

  protected String escapeText(String paramString)
  {
    boolean bool1 = this.props.isRecognizeUnicodeChars();
    boolean bool2 = this.props.isTranslateSpecialEntities();
    if (paramString != null)
    {
      int i = paramString.length();
      StringBuilder localStringBuilder = new StringBuilder(i);
      int j = 0;
      if (j < i)
      {
        label40: char c1 = paramString.charAt(j);
        int i1;
        label103: int i2;
        label111: int i3;
        int i4;
        label127: String str7;
        char c3;
        label153: char c2;
        if (c1 == '&')
          if ((j < i - 2) && (paramString.charAt(j + 1) == '#'))
            if (Character.toLowerCase(paramString.charAt(j + 2)) == 'x')
            {
              i1 = 1;
              if (i1 == 0)
                break label197;
              i2 = 3;
              i3 = j + i2;
              if (i1 == 0)
                break label203;
              i4 = 16;
              str7 = "";
              if (i3 < i)
              {
                c3 = paramString.charAt(i3);
                if (c3 != ';')
                  break label210;
              }
              if (!Utils.isValidInt(str7, i4))
                break label374;
              c2 = (char)Integer.parseInt(str7, i4);
              if (Utils.isValidXmlChar(c2))
                break label272;
              j = i3;
            }
        while (true)
        {
          label185: ++j;
          break label40:
          i1 = 0;
          break label103:
          label197: i2 = 2;
          break label111:
          label203: i4 = 10;
          break label127:
          if (Utils.isValidInt(str7 + c3, i4))
          {
            label210: str7 = str7 + c3;
            ++i3;
          }
          --i3;
          break label153:
          if (!Utils.isReservedXmlChar(c2))
          {
            label272: if (bool1);
            for (String str9 = String.valueOf(c2); ; str9 = "&#" + str7 + ";")
            {
              localStringBuilder.append(str9);
              j = i3;
              break label185:
            }
          }
          j = i3;
          localStringBuilder.append("&#" + str7 + ";");
          continue;
          label374: if (this.props.transResCharsToNCR);
          for (String str8 = "&#38;"; ; str8 = "&")
          {
            localStringBuilder.append(str8);
            break label185:
          }
          String str1 = paramString.substring(j, j + Math.min(2 + SpecialEntity.getMaxEntityLength(), i - j));
          int k = str1.indexOf(';');
          if (k > 0)
          {
            String str6 = str1.substring(1, k);
            SpecialEntity localSpecialEntity = SpecialEntity.getEntity(str6);
            if (localSpecialEntity != null)
            {
              Object localObject2;
              if (bool2)
                if (this.props.isTransSpecialEntitiesToNCR())
                {
                  localObject2 = localSpecialEntity.getDecimalNCR();
                  label487: localStringBuilder.append(localObject2);
                }
              while (true)
              {
                j += 1 + str6.length();
                break label185:
                localObject2 = Character.valueOf(localSpecialEntity.getCharacter());
                break label487:
                localStringBuilder.append(localSpecialEntity.getEscapedValue());
              }
            }
          }
          String str2 = paramString.substring(j);
          Iterator localIterator = Utils.RESERVED_XML_CHARS.entrySet().iterator();
          int l;
          Map.Entry localEntry;
          String str4;
          do
          {
            boolean bool3 = localIterator.hasNext();
            l = 0;
            if (!bool3)
              break label682;
            localEntry = (Map.Entry)localIterator.next();
            str4 = (String)localEntry.getValue();
          }
          while (!str2.startsWith(str4));
          String str5;
          if (this.props.transResCharsToNCR)
          {
            str5 = "&#" + ((Character)localEntry.getKey()).charValue() + ";";
            label659: localStringBuilder.append(str5);
            j += -1 + str4.length();
            l = 1;
            label682: if (l != 0)
              continue;
            if (!this.props.transResCharsToNCR)
              break label719;
          }
          for (String str3 = "&#38;"; ; str3 = "&")
          {
            localStringBuilder.append(str3);
            break label185:
            str5 = str4;
            label719: break label659:
          }
          if (Utils.isReservedXmlChar(c1))
          {
            if (this.props.transResCharsToNCR);
            for (Object localObject1 = "&#" + c1 + ";"; ; localObject1 = Character.valueOf(c1))
            {
              localStringBuilder.append(localObject1);
              break label185:
            }
          }
          localStringBuilder.append(c1);
        }
      }
      return localStringBuilder.toString();
    }
    return (String)(String)null;
  }

  protected boolean isMinimizedTagSyntax(TagNode paramTagNode)
  {
    TagInfo localTagInfo = this.props.getTagInfoProvider().getTagInfo(paramTagNode.getName());
    return (localTagInfo != null) && (!paramTagNode.hasChildren()) && (localTagInfo.isEmptyTag());
  }

  protected void serializeEndTag(TagNode paramTagNode, Writer paramWriter, boolean paramBoolean)
    throws IOException
  {
    String str = paramTagNode.getName();
    if (Utils.isEmptyString(str));
    do
    {
      return;
      if ((Utils.getXmlNSPrefix(str) != null) && (!this.props.isNamespacesAware()))
        str = Utils.getXmlName(str);
      paramWriter.write("</" + str + ">");
    }
    while (!paramBoolean);
    paramWriter.write("\n");
  }

  protected void serializeOpenTag(TagNode paramTagNode, Writer paramWriter, boolean paramBoolean)
    throws IOException
  {
    String str1 = paramTagNode.getName();
    if (Utils.isEmptyString(str1));
    do
    {
      return;
      boolean bool = this.props.isNamespacesAware();
      if ((!bool) && (Utils.getXmlNSPrefix(str1) != null))
        str1 = Utils.getXmlName(str1);
      paramWriter.write("<" + str1);
      Iterator localIterator1 = paramTagNode.getAttributes().entrySet().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
        String str4 = (String)localEntry2.getKey();
        if ((!bool) && (Utils.getXmlNSPrefix(str4) != null))
          str4 = Utils.getXmlName(str4);
        paramWriter.write(" " + str4 + "=\"" + escapeText((String)localEntry2.getValue()) + "\"");
      }
      if (bool)
      {
        Map localMap = paramTagNode.getNamespaceDeclarations();
        if (localMap != null)
        {
          Iterator localIterator2 = localMap.entrySet().iterator();
          while (localIterator2.hasNext())
          {
            Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
            String str2 = (String)localEntry1.getKey();
            String str3 = "xmlns";
            if (str2.length() > 0)
              str3 = str3 + ":" + str2;
            paramWriter.write(" " + str3 + "=\"" + escapeText((String)localEntry1.getValue()) + "\"");
          }
        }
      }
      if (!isMinimizedTagSyntax(paramTagNode))
        break label379;
      paramWriter.write(" />");
    }
    while (!paramBoolean);
    paramWriter.write("\n");
    return;
    label379: paramWriter.write(">");
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.HtmlSerializer
 * JD-Core Version:    0.5.4
 */