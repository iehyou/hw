package org.htmlcleaner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class HtmlCleaner
{
  public static final String DEFAULT_CHARSET = System.getProperty("file.encoding");
  private CleanerProperties properties;
  private ITagInfoProvider tagInfoProvider;
  private CleanerTransformations transformations = null;

  public HtmlCleaner()
  {
    this(null, null);
  }

  public HtmlCleaner(CleanerProperties paramCleanerProperties)
  {
    this(null, paramCleanerProperties);
  }

  public HtmlCleaner(ITagInfoProvider paramITagInfoProvider)
  {
    this(paramITagInfoProvider, null);
  }

  public HtmlCleaner(ITagInfoProvider paramITagInfoProvider, CleanerProperties paramCleanerProperties)
  {
    if (paramITagInfoProvider == null)
      paramITagInfoProvider = DefaultTagProvider.getInstance();
    this.tagInfoProvider = paramITagInfoProvider;
    if (paramCleanerProperties == null)
      paramCleanerProperties = new CleanerProperties();
    this.properties = paramCleanerProperties;
    this.properties.tagInfoProvider = this.tagInfoProvider;
  }

  private void addAttributesToTag(TagNode paramTagNode, Map paramMap)
  {
    if (paramMap == null)
      return;
    Map localMap = paramTagNode.getAttributes();
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str = (String)localEntry.getKey();
      if (localMap.containsKey(str))
        continue;
      paramTagNode.setAttribute(str, (String)localEntry.getValue());
    }
  }

  private void addPossibleHeadCandidate(TagInfo paramTagInfo, TagNode paramTagNode, CleanTimeValues paramCleanTimeValues)
  {
    if ((paramTagInfo == null) || (paramTagNode == null) || ((!paramTagInfo.isHeadTag()) && (((!paramTagInfo.isHeadAndBodyTag()) || (!paramCleanTimeValues._headOpened) || (paramCleanTimeValues._bodyOpened)))))
      return;
    paramCleanTimeValues._headTags.add(paramTagNode);
  }

  private void calculateRootNode(CleanTimeValues paramCleanTimeValues)
  {
    CleanTimeValues.access$1302(paramCleanTimeValues, paramCleanTimeValues.htmlNode);
    if (!this.properties.omitHtmlEnvelope)
      return;
    List localList = paramCleanTimeValues.bodyNode.getChildren();
    if (localList == null)
      return;
    Iterator localIterator = localList.iterator();
    Object localObject;
    do
    {
      if (!localIterator.hasNext())
        return;
      localObject = localIterator.next();
    }
    while (!localObject instanceof TagNode);
    CleanTimeValues.access$1302(paramCleanTimeValues, (TagNode)localObject);
  }

  private void closeAll(List<BaseToken> paramList, CleanTimeValues paramCleanTimeValues)
  {
    TagPos localTagPos = paramCleanTimeValues._openTags.findFirstTagPos();
    if (localTagPos == null)
      return;
    closeSnippet(paramList, localTagPos, null, paramCleanTimeValues);
  }

  private List closeSnippet(List paramList, TagPos paramTagPos, Object paramObject, CleanTimeValues paramCleanTimeValues)
  {
    ArrayList localArrayList = new ArrayList();
    ListIterator localListIterator = paramList.listIterator(paramTagPos.position);
    Object localObject1 = null;
    Object localObject2 = localListIterator.next();
    for (int i = 0; ((paramObject == null) && (i == 0)) || ((paramObject != null) && (localObject2 != paramObject)); i = 1)
    {
      label36: List localList;
      TagNode localTagNode2;
      if (isStartToken(localObject2))
      {
        TagNode localTagNode1 = (TagNode)localObject2;
        localArrayList.add(localTagNode1);
        localList = localTagNode1.getItemsToMove();
        if (localList != null)
        {
          OpenTags localOpenTags = paramCleanTimeValues._openTags;
          CleanTimeValues.access$402(paramCleanTimeValues, new OpenTags(null));
          makeTree(localList, localList.listIterator(0), paramCleanTimeValues);
          closeAll(localList, paramCleanTimeValues);
          localTagNode1.setItemsToMove(null);
          CleanTimeValues.access$402(paramCleanTimeValues, localOpenTags);
        }
        localTagNode2 = createTagNode(localTagNode1);
        addPossibleHeadCandidate(this.tagInfoProvider.getTagInfo(localTagNode2.getName()), localTagNode2, paramCleanTimeValues);
        if (localObject1 != null)
        {
          localObject1.addChildren(localList);
          localObject1.addChild(localTagNode2);
          localListIterator.set(null);
          label210: paramCleanTimeValues._openTags.removeTag(localTagNode2.getName());
          localObject1 = localTagNode2;
        }
      }
      while (localListIterator.hasNext())
      {
        localObject2 = localListIterator.next();
        break label36:
        if (localList != null)
        {
          localList.add(localTagNode2);
          localListIterator.set(localList);
        }
        localListIterator.set(localTagNode2);
        break label210:
        if (localObject1 == null)
          continue;
        localListIterator.set(null);
        if (localObject2 == null)
          continue;
        localObject1.addChild(localObject2);
      }
    }
    return localArrayList;
  }

  private void createDocumentNodes(List paramList, CleanTimeValues paramCleanTimeValues)
  {
    Iterator localIterator1 = paramList.iterator();
    label7: Object localObject;
    do
    {
      if (!localIterator1.hasNext())
        break label118;
      localObject = localIterator1.next();
    }
    while (localObject == null);
    int j = 1;
    if (localObject instanceof TagNode)
    {
      TagNode localTagNode3 = (TagNode)localObject;
      addPossibleHeadCandidate(this.tagInfoProvider.getTagInfo(localTagNode3.getName()), localTagNode3, paramCleanTimeValues);
    }
    do
    {
      label68: if (j != 0);
      paramCleanTimeValues.bodyNode.addChild(localObject);
      break label7:
    }
    while (!localObject instanceof ContentNode);
    if (!"".equals(localObject.toString()));
    for (j = 1; ; j = 0)
      break label68:
    label118: Iterator localIterator2 = paramCleanTimeValues._headTags.iterator();
    if (!localIterator2.hasNext())
      label129: return;
    TagNode localTagNode1 = (TagNode)localIterator2.next();
    TagNode localTagNode2 = localTagNode1.getParent();
    int i = 1;
    while (true)
    {
      if (localTagNode2 != null)
      {
        if (!paramCleanTimeValues._headTags.contains(localTagNode2))
          break label206;
        i = 0;
      }
      if (i != 0);
      localTagNode1.removeFromTree();
      paramCleanTimeValues.headNode.addChild(localTagNode1);
      break label129:
      label206: localTagNode2 = localTagNode2.getParent();
    }
  }

  private TagNode createTagNode(String paramString, CleanTimeValues paramCleanTimeValues)
  {
    TagNode localTagNode = new TagNode(paramString);
    if ((paramCleanTimeValues.pruneTagSet != null) && (paramString != null) && (paramCleanTimeValues.pruneTagSet.contains(paramString.toLowerCase())))
      paramCleanTimeValues.pruneNodeSet.add(localTagNode);
    return localTagNode;
  }

  private TagNode createTagNode(TagNode paramTagNode)
  {
    paramTagNode.setFormed();
    return paramTagNode;
  }

  private boolean isAllowedInLastOpenTag(BaseToken paramBaseToken, CleanTimeValues paramCleanTimeValues)
  {
    TagPos localTagPos = paramCleanTimeValues._openTags.getLastTagPos();
    if ((localTagPos != null) && (localTagPos.info != null))
      return localTagPos.info.allowsItem(paramBaseToken);
    return true;
  }

  private boolean isFatalTagSatisfied(TagInfo paramTagInfo, CleanTimeValues paramCleanTimeValues)
  {
    String str;
    if (paramTagInfo != null)
    {
      str = paramTagInfo.getFatalTag();
      if (str != null)
        break label15;
    }
    return true;
    label15: return paramCleanTimeValues._openTags.tagExists(str);
  }

  private boolean isStartToken(Object paramObject)
  {
    return (paramObject instanceof TagNode) && (!((TagNode)paramObject).isFormed());
  }

  private TagNode makeTagNodeCopy(TagNode paramTagNode, CleanTimeValues paramCleanTimeValues)
  {
    TagNode localTagNode = paramTagNode.makeCopy();
    if ((paramCleanTimeValues.pruneTagSet != null) && (paramCleanTimeValues.pruneTagSet.contains(paramTagNode.getName())))
      paramCleanTimeValues.pruneNodeSet.add(localTagNode);
    return localTagNode;
  }

  private boolean mustAddRequiredParent(TagInfo paramTagInfo, CleanTimeValues paramCleanTimeValues)
  {
    if ((paramTagInfo != null) && (paramTagInfo.getRequiredParent() != null))
    {
      String str = paramTagInfo.getFatalTag();
      int i = -1;
      if (str != null)
      {
        TagPos localTagPos2 = paramCleanTimeValues._openTags.findTag(str);
        if (localTagPos2 != null)
          i = localTagPos2.position;
      }
      ListIterator localListIterator = CleanTimeValues.access$400(paramCleanTimeValues).list.listIterator(CleanTimeValues.access$400(paramCleanTimeValues).list.size());
      TagPos localTagPos1;
      do
      {
        if (!localListIterator.hasPrevious())
          break label115;
        localTagPos1 = (TagPos)localListIterator.previous();
      }
      while (!paramTagInfo.isHigher(localTagPos1.name));
      label115: return localTagPos1.position <= i;
    }
    return false;
  }

  private void saveToLastOpenTag(List paramList, BaseToken paramBaseToken, CleanTimeValues paramCleanTimeValues)
  {
    TagPos localTagPos1 = paramCleanTimeValues._openTags.getLastTagPos();
    if ((localTagPos1 != null) && (localTagPos1.info != null) && (localTagPos1.info.isIgnorePermitted()));
    TagPos localTagPos2;
    do
    {
      return;
      localTagPos2 = paramCleanTimeValues._openTags.findTagToPlaceRubbish();
    }
    while (localTagPos2 == null);
    ((TagNode)paramList.get(localTagPos2.position)).addItemForMoving(paramBaseToken);
  }

  private void setPruneTags(String paramString, CleanTimeValues paramCleanTimeValues)
  {
    paramCleanTimeValues.pruneTagSet.clear();
    paramCleanTimeValues.pruneNodeSet.clear();
    if (paramString == null)
      return;
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
    while (localStringTokenizer.hasMoreTokens())
      paramCleanTimeValues.pruneTagSet.add(localStringTokenizer.nextToken().trim().toLowerCase());
  }

  public TagNode clean(File paramFile)
    throws IOException
  {
    return clean(paramFile, DEFAULT_CHARSET);
  }

  public TagNode clean(File paramFile, String paramString)
    throws IOException
  {
    return clean(new InputStreamReader(new FileInputStream(paramFile), paramString));
  }

  public TagNode clean(InputStream paramInputStream)
    throws IOException
  {
    return clean(paramInputStream, DEFAULT_CHARSET);
  }

  public TagNode clean(InputStream paramInputStream, String paramString)
    throws IOException
  {
    return clean(new InputStreamReader(paramInputStream, paramString));
  }

  public TagNode clean(Reader paramReader)
    throws IOException
  {
    return clean(paramReader, new CleanTimeValues(null));
  }

  public TagNode clean(Reader paramReader, CleanTimeValues paramCleanTimeValues)
    throws IOException
  {
    CleanTimeValues.access$402(paramCleanTimeValues, new OpenTags(null));
    CleanTimeValues.access$602(paramCleanTimeValues, false);
    CleanTimeValues.access$702(paramCleanTimeValues, false);
    paramCleanTimeValues._headTags.clear();
    paramCleanTimeValues.allTags.clear();
    setPruneTags(this.properties.pruneTags, paramCleanTimeValues);
    CleanTimeValues.access$1002(paramCleanTimeValues, createTagNode("html", paramCleanTimeValues));
    CleanTimeValues.access$1102(paramCleanTimeValues, createTagNode("body", paramCleanTimeValues));
    CleanTimeValues.access$1202(paramCleanTimeValues, createTagNode("head", paramCleanTimeValues));
    CleanTimeValues.access$1302(paramCleanTimeValues, null);
    paramCleanTimeValues.htmlNode.addChild(paramCleanTimeValues.headNode);
    paramCleanTimeValues.htmlNode.addChild(paramCleanTimeValues.bodyNode);
    1 local1 = new HtmlTokenizer(paramReader, this.properties, this.transformations, this.tagInfoProvider, paramCleanTimeValues)
    {
      TagNode createTagNode(String paramString)
      {
        return HtmlCleaner.this.createTagNode(paramString, this.val$cleanTimeValues);
      }

      void makeTree(List<BaseToken> paramList)
      {
        HtmlCleaner.this.makeTree(paramList, paramList.listIterator(-1 + paramList.size()), this.val$cleanTimeValues);
      }
    };
    local1.start();
    List localList = local1.getTokenList();
    closeAll(localList, paramCleanTimeValues);
    createDocumentNodes(localList, paramCleanTimeValues);
    calculateRootNode(paramCleanTimeValues);
    if ((paramCleanTimeValues.pruneNodeSet != null) && (!paramCleanTimeValues.pruneNodeSet.isEmpty()))
    {
      Iterator localIterator = paramCleanTimeValues.pruneNodeSet.iterator();
      while (localIterator.hasNext())
      {
        TagNode localTagNode1 = (TagNode)localIterator.next();
        TagNode localTagNode2 = localTagNode1.getParent();
        if (localTagNode2 == null)
          continue;
        localTagNode2.removeChild(localTagNode1);
      }
    }
    paramCleanTimeValues.rootNode.setDocType(local1.getDocType());
    return paramCleanTimeValues.rootNode;
  }

  public TagNode clean(String paramString)
  {
    try
    {
      TagNode localTagNode = clean(new StringReader(paramString));
      return localTagNode;
    }
    catch (IOException localIOException)
    {
      throw new HtmlCleanerException(localIOException);
    }
  }

  public TagNode clean(URL paramURL)
    throws IOException
  {
    return clean(paramURL, null);
  }

  public TagNode clean(URL paramURL, String paramString)
    throws IOException
  {
    URLConnection localURLConnection = paramURL.openConnection();
    if (paramString == null)
      paramString = Utils.getCharsetFromContentTypeString(localURLConnection.getHeaderField("Content-Type"));
    if (paramString == null)
      paramString = Utils.getCharsetFromContent(paramURL);
    if (paramString == null)
      paramString = DEFAULT_CHARSET;
    return clean(paramURL.openStream(), paramString);
  }

  public String getInnerHtml(TagNode paramTagNode)
  {
    if (paramTagNode != null)
      try
      {
        String str1 = new SimpleXmlSerializer(this.properties).getAsString(paramTagNode);
        int i = str1.indexOf('>', 1 + str1.indexOf("<" + paramTagNode.getName()));
        int j = str1.lastIndexOf('<');
        if ((i >= 0) && (i <= j))
        {
          String str2 = str1.substring(i + 1, j);
          return str2;
        }
        return null;
      }
      catch (IOException localIOException)
      {
        throw new HtmlCleanerException(localIOException);
      }
    throw new HtmlCleanerException("Cannot return inner html of the null node!");
  }

  public CleanerProperties getProperties()
  {
    return this.properties;
  }

  public ITagInfoProvider getTagInfoProvider()
  {
    return this.tagInfoProvider;
  }

  public CleanerTransformations getTransformations()
  {
    return this.transformations;
  }

  void makeTree(List<BaseToken> paramList, ListIterator<BaseToken> paramListIterator, CleanTimeValues paramCleanTimeValues)
  {
    while (paramListIterator.hasNext())
    {
      label0: BaseToken localBaseToken;
      do
        while (true)
        {
          localBaseToken = (BaseToken)paramListIterator.next();
          if (localBaseToken instanceof EndTagToken)
          {
            EndTagToken localEndTagToken = (EndTagToken)localBaseToken;
            String str2 = localEndTagToken.getName();
            TagInfo localTagInfo3 = this.tagInfoProvider.getTagInfo(str2);
            if (((localTagInfo3 == null) && (this.properties.omitUnknownTags)) || ((localTagInfo3 != null) && (localTagInfo3.isDeprecated()) && (this.properties.omitDeprecatedTags)))
              paramListIterator.set(null);
            if ((localTagInfo3 != null) && (!localTagInfo3.allowsBody()))
              paramListIterator.set(null);
            TagPos localTagPos2 = paramCleanTimeValues._openTags.findTag(str2);
            if (localTagPos2 != null)
            {
              List localList2 = closeSnippet(paramList, localTagPos2, localEndTagToken, paramCleanTimeValues);
              paramListIterator.set(null);
              for (int k = -1 + localList2.size(); ; --k)
              {
                if (k >= 1);
                TagNode localTagNode5 = (TagNode)localList2.get(k);
                if ((localTagInfo3 == null) || (!localTagInfo3.isContinueAfter(localTagNode5.getName())))
                  continue;
                paramListIterator.add(makeTagNodeCopy(localTagNode5, paramCleanTimeValues));
                paramListIterator.previous();
              }
            }
            if (isAllowedInLastOpenTag(localBaseToken, paramCleanTimeValues))
              continue;
            saveToLastOpenTag(paramList, localBaseToken, paramCleanTimeValues);
            paramListIterator.set(null);
          }
          if (!isStartToken(localBaseToken))
            break;
          TagNode localTagNode1 = (TagNode)localBaseToken;
          String str1 = localTagNode1.getName();
          TagInfo localTagInfo1 = this.tagInfoProvider.getTagInfo(str1);
          TagPos localTagPos1;
          if (paramCleanTimeValues._openTags.isEmpty() != 0)
          {
            localTagPos1 = null;
            label313: if (localTagPos1 != null)
              break label379;
          }
          for (TagInfo localTagInfo2 = null; ; localTagInfo2 = this.tagInfoProvider.getTagInfo(localTagPos1.name))
          {
            paramCleanTimeValues.allTags.add(str1);
            if (!"html".equals(str1))
              break;
            addAttributesToTag(paramCleanTimeValues.htmlNode, localTagNode1.getAttributes());
            paramListIterator.set(null);
            break label0:
            localTagPos1 = paramCleanTimeValues._openTags.getLastTagPos();
            label379: break label313:
          }
          if ("body".equals(str1))
          {
            CleanTimeValues.access$702(paramCleanTimeValues, true);
            addAttributesToTag(paramCleanTimeValues.bodyNode, localTagNode1.getAttributes());
            paramListIterator.set(null);
          }
          if ("head".equals(str1))
          {
            CleanTimeValues.access$602(paramCleanTimeValues, true);
            addAttributesToTag(paramCleanTimeValues.headNode, localTagNode1.getAttributes());
            paramListIterator.set(null);
          }
          if (((localTagInfo1 == null) && (this.properties.omitUnknownTags)) || ((localTagInfo1 != null) && (localTagInfo1.isDeprecated()) && (this.properties.omitDeprecatedTags)))
            paramListIterator.set(null);
          if ((localTagInfo1 == null) && (localTagInfo2 != null) && (!localTagInfo2.allowsAnything()))
          {
            saveToLastOpenTag(paramList, localBaseToken, paramCleanTimeValues);
            paramListIterator.set(null);
          }
          if ((localTagInfo1 != null) && (localTagInfo1.hasPermittedTags()) && (paramCleanTimeValues._openTags.someAlreadyOpen(localTagInfo1.getPermittedTags()) != 0))
            paramListIterator.set(null);
          if ((localTagInfo1 != null) && (localTagInfo1.isUnique()) && (paramCleanTimeValues._openTags.tagEncountered(str1) != 0))
            paramListIterator.set(null);
          if (!isFatalTagSatisfied(localTagInfo1, paramCleanTimeValues))
            paramListIterator.set(null);
          if (mustAddRequiredParent(localTagInfo1, paramCleanTimeValues))
          {
            TagNode localTagNode4 = createTagNode(localTagInfo1.getRequiredParent(), paramCleanTimeValues);
            paramListIterator.previous();
            paramListIterator.add(localTagNode4);
            paramListIterator.previous();
          }
          if ((localTagInfo1 != null) && (localTagPos1 != null) && (localTagInfo1.isMustCloseTag(localTagInfo2)))
          {
            List localList1 = closeSnippet(paramList, localTagPos1, localTagNode1, paramCleanTimeValues);
            int i = localList1.size();
            if ((localTagInfo1.hasCopyTags()) && (i > 0))
            {
              ListIterator localListIterator = localList1.listIterator(i);
              ArrayList localArrayList = new ArrayList();
              while (localListIterator.hasPrevious())
              {
                TagNode localTagNode3 = (TagNode)localListIterator.previous();
                if (!localTagInfo1.isCopy(localTagNode3.getName()))
                  break;
                localArrayList.add(0, localTagNode3);
              }
              if (localArrayList.size() > 0)
              {
                Iterator localIterator = localArrayList.iterator();
                while (localIterator.hasNext())
                  paramListIterator.add(makeTagNodeCopy((TagNode)localIterator.next(), paramCleanTimeValues));
                for (int j = 0; j < localArrayList.size(); ++j)
                  paramListIterator.previous();
              }
            }
            paramListIterator.previous();
          }
          if (!isAllowedInLastOpenTag(localBaseToken, paramCleanTimeValues))
          {
            saveToLastOpenTag(paramList, localBaseToken, paramCleanTimeValues);
            paramListIterator.set(null);
          }
          if ((localTagInfo1 != null) && (!localTagInfo1.allowsBody()))
          {
            TagNode localTagNode2 = createTagNode(localTagNode1);
            addPossibleHeadCandidate(localTagInfo1, localTagNode2, paramCleanTimeValues);
            paramListIterator.set(localTagNode2);
          }
          paramCleanTimeValues._openTags.addTag(str1, paramListIterator.previousIndex());
        }
      while (isAllowedInLastOpenTag(localBaseToken, paramCleanTimeValues));
      saveToLastOpenTag(paramList, localBaseToken, paramCleanTimeValues);
      paramListIterator.set(null);
    }
  }

  public void setInnerHtml(TagNode paramTagNode, String paramString)
  {
    if (paramTagNode == null)
      return;
    String str1 = paramTagNode.getName();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("<" + str1 + " marker=''>");
    localStringBuilder.append(paramString);
    localStringBuilder.append("</" + str1 + ">");
    for (TagNode localTagNode1 = paramTagNode.getParent(); localTagNode1 != null; localTagNode1 = localTagNode1.getParent())
    {
      String str2 = localTagNode1.getName();
      localStringBuilder.insert(0, "<" + str2 + ">");
      localStringBuilder.append("</" + str2 + ">");
    }
    TagNode localTagNode2 = clean(localStringBuilder.toString()).findElementHavingAttribute("marker", true);
    if (localTagNode2 == null)
      return;
    paramTagNode.setChildren(localTagNode2.getChildren());
  }

  public void setTransformations(CleanerTransformations paramCleanerTransformations)
  {
    this.transformations = paramCleanerTransformations;
  }

  private class CleanTimeValues
  {
    private boolean _bodyOpened = false;
    private boolean _headOpened = false;
    private Set _headTags = new LinkedHashSet();
    private HtmlCleaner.OpenTags _openTags;
    private Set allTags = new TreeSet();
    private TagNode bodyNode;
    private TagNode headNode;
    private TagNode htmlNode;
    private Set<TagNode> pruneNodeSet = new HashSet();
    private Set<String> pruneTagSet = new HashSet();
    private TagNode rootNode;

    private CleanTimeValues()
    {
    }
  }

  private class OpenTags
  {
    private HtmlCleaner.TagPos last = null;
    private List<HtmlCleaner.TagPos> list = new ArrayList();
    private Set<String> set = new HashSet();

    private OpenTags()
    {
    }

    private void addTag(String paramString, int paramInt)
    {
      this.last = new HtmlCleaner.TagPos(HtmlCleaner.this, paramInt, paramString);
      this.list.add(this.last);
      this.set.add(paramString);
    }

    private HtmlCleaner.TagPos findFirstTagPos()
    {
      if (this.list.isEmpty())
        return null;
      return (HtmlCleaner.TagPos)this.list.get(0);
    }

    private HtmlCleaner.TagPos findTag(String paramString)
    {
      if (paramString != null)
      {
        ListIterator localListIterator = this.list.listIterator(this.list.size());
        TagInfo localTagInfo = HtmlCleaner.this.tagInfoProvider.getTagInfo(paramString);
        String str = null;
        if (localTagInfo != null)
          str = localTagInfo.getFatalTag();
        while (localListIterator.hasPrevious())
        {
          HtmlCleaner.TagPos localTagPos = (HtmlCleaner.TagPos)localListIterator.previous();
          if (paramString.equals(HtmlCleaner.TagPos.access$100(localTagPos)))
            return localTagPos;
          if ((str != null) && (str.equals(HtmlCleaner.TagPos.access$100(localTagPos))))
            return null;
        }
      }
      return null;
    }

    private HtmlCleaner.TagPos findTagToPlaceRubbish()
    {
      Object localObject = null;
      boolean bool = isEmpty();
      HtmlCleaner.TagPos localTagPos = null;
      if (!bool)
      {
        ListIterator localListIterator = this.list.listIterator(this.list.size());
        while (localListIterator.hasPrevious())
        {
          localTagPos = (HtmlCleaner.TagPos)localListIterator.previous();
          if ((((HtmlCleaner.TagPos.access$200(localTagPos) == null) || (HtmlCleaner.TagPos.access$200(localTagPos).allowsAnything()))) && (localObject != null))
            return localObject;
          localObject = localTagPos;
        }
      }
      return localTagPos;
    }

    private HtmlCleaner.TagPos getLastTagPos()
    {
      return this.last;
    }

    private boolean isEmpty()
    {
      return this.list.isEmpty();
    }

    private void removeTag(String paramString)
    {
      ListIterator localListIterator = this.list.listIterator(this.list.size());
      do
        if (!localListIterator.hasPrevious())
          break label53;
      while (!paramString.equals(HtmlCleaner.TagPos.access$100((HtmlCleaner.TagPos)localListIterator.previous())));
      localListIterator.remove();
      label53: if (this.list.isEmpty());
      for (HtmlCleaner.TagPos localTagPos = null; ; localTagPos = (HtmlCleaner.TagPos)this.list.get(-1 + this.list.size()))
      {
        this.last = localTagPos;
        return;
      }
    }

    private boolean someAlreadyOpen(Set paramSet)
    {
      Iterator localIterator = this.list.iterator();
      while (localIterator.hasNext())
        if (paramSet.contains(HtmlCleaner.TagPos.access$100((HtmlCleaner.TagPos)localIterator.next())))
          return true;
      return false;
    }

    private boolean tagEncountered(String paramString)
    {
      return this.set.contains(paramString);
    }

    private boolean tagExists(String paramString)
    {
      return findTag(paramString) != null;
    }
  }

  private class TagPos
  {
    private TagInfo info;
    private String name;
    private int position;

    TagPos(int paramString, String arg3)
    {
      this.position = paramString;
      String str;
      this.name = str;
      this.info = HtmlCleaner.this.tagInfoProvider.getTagInfo(str);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.HtmlCleaner
 * JD-Core Version:    0.5.4
 */