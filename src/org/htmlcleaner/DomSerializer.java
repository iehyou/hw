package org.htmlcleaner;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DomSerializer
{
  protected boolean escapeXml = true;
  protected CleanerProperties props;

  public DomSerializer(CleanerProperties paramCleanerProperties)
  {
    this(paramCleanerProperties, true);
  }

  public DomSerializer(CleanerProperties paramCleanerProperties, boolean paramBoolean)
  {
    this.props = paramCleanerProperties;
    this.escapeXml = paramBoolean;
  }

  private Element createElement(TagNode paramTagNode, Document paramDocument)
  {
    String str1 = paramTagNode.getName();
    boolean bool = this.props.isNamespacesAware();
    String str2 = Utils.getXmlNSPrefix(str1);
    Map localMap = paramTagNode.getNamespaceDeclarations();
    if (str2 != null)
      if (bool)
      {
        str3 = null;
        if (localMap != null)
          str3 = (String)localMap.get(str2);
        if (str3 == null)
          str3 = paramTagNode.getNamespaceURIOnPath(str2);
        if (str3 != null);
      }
    for (String str3 = str2; (bool) && (str3 != null); str3 = paramTagNode.getNamespaceURIOnPath(str2))
      do
      {
        do
        {
          while (true)
          {
            return paramDocument.createElementNS(str3, str1);
            str1 = Utils.getXmlName(str1);
            str3 = null;
          }
          str3 = null;
        }
        while (!bool);
        str3 = null;
        if (localMap == null)
          continue;
        str3 = (String)localMap.get("");
      }
      while (str3 != null);
    return paramDocument.createElement(str1);
  }

  private void createSubnodes(Document paramDocument, Element paramElement, List paramList)
  {
    if (paramList == null)
      return;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      label12: Object localObject1 = localIterator.next();
      if (localObject1 instanceof CommentNode)
        paramElement.appendChild(paramDocument.createComment(((CommentNode)localObject1).getContent().toString()));
      if (localObject1 instanceof ContentNode)
      {
        String str1 = paramElement.getNodeName();
        String str2 = localObject1.toString();
        int i;
        if ((this.props.isUseCdataForScriptAndStyle()) && ((("script".equalsIgnoreCase(str1)) || ("style".equalsIgnoreCase(str1)))))
        {
          i = 1;
          if ((this.escapeXml) && (i == 0))
            label122: str2 = Utils.escapeXml(str2, this.props, true);
          if (i == 0)
            break label179;
        }
        for (Object localObject2 = paramDocument.createCDATASection(str2); ; localObject2 = paramDocument.createTextNode(str2))
        {
          paramElement.appendChild((Node)localObject2);
          break label12:
          i = 0;
          label179: break label122:
        }
      }
      if (localObject1 instanceof TagNode)
      {
        TagNode localTagNode = (TagNode)localObject1;
        Element localElement = createElement(localTagNode, paramDocument);
        setAttributes(localTagNode, localElement);
        createSubnodes(paramDocument, localElement, localTagNode.getChildren());
        paramElement.appendChild(localElement);
      }
      if (!localObject1 instanceof List)
        continue;
      createSubnodes(paramDocument, paramElement, (List)localObject1);
    }
  }

  private void setAttributes(TagNode paramTagNode, Element paramElement)
  {
    Iterator localIterator = paramTagNode.getAttributes().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str1 = (String)localEntry.getKey();
      String str2 = (String)localEntry.getValue();
      if (this.escapeXml)
        str2 = Utils.escapeXml(str2, this.props, true);
      String str3 = Utils.getXmlNSPrefix(str1);
      if (str3 != null)
      {
        if (this.props.isNamespacesAware())
        {
          String str4 = paramTagNode.getNamespaceURIOnPath(str3);
          if (str4 == null)
            str4 = str3;
          paramElement.setAttributeNS(str4, str1, str2);
        }
        paramElement.setAttribute(Utils.getXmlName(str1), str2);
      }
      paramElement.setAttribute(str1, str2);
    }
  }

  public Document createDOM(TagNode paramTagNode)
    throws ParserConfigurationException
  {
    Document localDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
    Element localElement = createElement(paramTagNode, localDocument);
    localDocument.appendChild(localElement);
    setAttributes(paramTagNode, localElement);
    createSubnodes(localDocument, localElement, paramTagNode.getChildren());
    return localDocument;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.DomSerializer
 * JD-Core Version:    0.5.4
 */