package org.htmlcleaner;

import java.util.LinkedHashMap;
import java.util.Map;

public class TagTransformation
{
  private Map attributeTransformations;
  private String destTag;
  private boolean preserveSourceAttributes;
  private String sourceTag;

  public TagTransformation(String paramString)
  {
    this(paramString, null);
  }

  public TagTransformation(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, true);
  }

  public TagTransformation(String paramString1, String paramString2, boolean paramBoolean)
  {
    this.sourceTag = paramString1.toLowerCase();
    if (paramString2 == null);
    for (this.destTag = null; ; this.destTag = paramString1)
    {
      this.preserveSourceAttributes = paramBoolean;
      return;
      if (!Utils.isValidXmlIdentifier(paramString2))
        continue;
      paramString1 = paramString2.toLowerCase();
    }
  }

  public void addAttributeTransformation(String paramString)
  {
    addAttributeTransformation(paramString, null);
  }

  public void addAttributeTransformation(String paramString1, String paramString2)
  {
    if (this.attributeTransformations == null)
      this.attributeTransformations = new LinkedHashMap();
    this.attributeTransformations.put(paramString1.toLowerCase(), paramString2);
  }

  Map getAttributeTransformations()
  {
    return this.attributeTransformations;
  }

  String getDestTag()
  {
    return this.destTag;
  }

  String getSourceTag()
  {
    return this.sourceTag;
  }

  boolean hasAttributeTransformations()
  {
    return this.attributeTransformations != null;
  }

  boolean isPreserveSourceAttributes()
  {
    return this.preserveSourceAttributes;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.TagTransformation
 * JD-Core Version:    0.5.4
 */