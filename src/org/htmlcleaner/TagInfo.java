package org.htmlcleaner;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class TagInfo
{
  protected static final int BODY = 2;
  protected static final int CONTENT_ALL = 0;
  protected static final int CONTENT_NONE = 1;
  protected static final int CONTENT_TEXT = 2;
  protected static final int HEAD = 1;
  protected static final int HEAD_AND_BODY;
  private int belongsTo = 2;
  private Set childTags = new HashSet();
  private int contentType;
  private Set continueAfterTags = new HashSet();
  private Set copyTags = new HashSet();
  private boolean deprecated = false;
  private String fatalTag = null;
  private Set higherTags = new HashSet();
  private boolean ignorePermitted = false;
  private Set mustCloseTags = new HashSet();
  private String name;
  private Set permittedTags = new HashSet();
  private String requiredParent = null;
  private boolean unique = false;

  public TagInfo(String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    this.name = paramString;
    this.contentType = paramInt1;
    this.belongsTo = paramInt2;
    this.deprecated = paramBoolean1;
    this.unique = paramBoolean2;
    this.ignorePermitted = paramBoolean3;
  }

  boolean allowsAnything()
  {
    return (this.contentType == 0) && (this.childTags.size() == 0);
  }

  boolean allowsBody()
  {
    return 1 != this.contentType;
  }

  boolean allowsItem(BaseToken paramBaseToken)
  {
    if ((this.contentType != 1) && (paramBaseToken instanceof TagToken) && ("script".equals(((TagToken)paramBaseToken).getName())));
    while (true)
    {
      return true;
      if (this.contentType == 0)
      {
        if (!this.childTags.isEmpty())
        {
          boolean bool2 = paramBaseToken instanceof TagToken;
          boolean bool3 = false;
          if (bool2)
            bool3 = this.childTags.contains(((TagToken)paramBaseToken).getName());
          return bool3;
        }
        if (this.permittedTags.isEmpty())
          continue;
        if (paramBaseToken instanceof TagToken)
        {
          boolean bool1 = this.permittedTags.contains(((TagToken)paramBaseToken).getName());
          i = 0;
          if (bool1);
        }
        for (int i = 1; ; i = 1)
          return i;
      }
      if (2 != this.contentType)
        break;
      if (paramBaseToken instanceof TagToken)
        return false;
    }
    return false;
  }

  public void defineAllowedChildrenTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.childTags.add(str);
    }
  }

  public void defineCloseBeforeCopyInsideTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.copyTags.add(str);
      this.mustCloseTags.add(str);
    }
  }

  public void defineCloseBeforeTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.mustCloseTags.add(str);
    }
  }

  public void defineCloseInsideCopyAfterTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.continueAfterTags.add(str);
    }
  }

  public void defineFatalTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.fatalTag = str;
      this.higherTags.add(str);
    }
  }

  public void defineForbiddenTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.permittedTags.add(str);
    }
  }

  public void defineHigherLevelTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.higherTags.add(str);
    }
  }

  public void defineRequiredEnclosingTags(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString.toLowerCase(), ",");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str = localStringTokenizer.nextToken();
      this.requiredParent = str;
      this.higherTags.add(str);
    }
  }

  public int getBelongsTo()
  {
    return this.belongsTo;
  }

  public Set getChildTags()
  {
    return this.childTags;
  }

  public int getContentType()
  {
    return this.contentType;
  }

  public Set getContinueAfterTags()
  {
    return this.continueAfterTags;
  }

  public Set getCopyTags()
  {
    return this.copyTags;
  }

  public String getFatalTag()
  {
    return this.fatalTag;
  }

  public Set getHigherTags()
  {
    return this.higherTags;
  }

  public Set getMustCloseTags()
  {
    return this.mustCloseTags;
  }

  public String getName()
  {
    return this.name;
  }

  public Set getPermittedTags()
  {
    return this.permittedTags;
  }

  public String getRequiredParent()
  {
    return this.requiredParent;
  }

  boolean hasCopyTags()
  {
    return !this.copyTags.isEmpty();
  }

  boolean hasPermittedTags()
  {
    return !this.permittedTags.isEmpty();
  }

  boolean isContinueAfter(String paramString)
  {
    return this.continueAfterTags.contains(paramString);
  }

  boolean isCopy(String paramString)
  {
    return this.copyTags.contains(paramString);
  }

  public boolean isDeprecated()
  {
    return this.deprecated;
  }

  public boolean isEmptyTag()
  {
    return 1 == this.contentType;
  }

  boolean isHeadAndBodyTag()
  {
    return (this.belongsTo == 1) || (this.belongsTo == 0);
  }

  boolean isHeadTag()
  {
    return this.belongsTo == 1;
  }

  boolean isHigher(String paramString)
  {
    return this.higherTags.contains(paramString);
  }

  public boolean isIgnorePermitted()
  {
    return this.ignorePermitted;
  }

  boolean isMustCloseTag(TagInfo paramTagInfo)
  {
    int i = 0;
    if (paramTagInfo != null)
    {
      if (!this.mustCloseTags.contains(paramTagInfo.getName()))
      {
        int j = paramTagInfo.contentType;
        i = 0;
        if (j != 2)
          break label36;
      }
      i = 1;
    }
    label36: return i;
  }

  public boolean isUnique()
  {
    return this.unique;
  }

  public void setBelongsTo(int paramInt)
  {
    this.belongsTo = paramInt;
  }

  public void setChildTags(Set paramSet)
  {
    this.childTags = paramSet;
  }

  public void setContinueAfterTags(Set paramSet)
  {
    this.continueAfterTags = paramSet;
  }

  public void setCopyTags(Set paramSet)
  {
    this.copyTags = paramSet;
  }

  public void setDeprecated(boolean paramBoolean)
  {
    this.deprecated = paramBoolean;
  }

  public void setFatalTag(String paramString)
  {
    this.fatalTag = paramString;
  }

  public void setHigherTags(Set paramSet)
  {
    this.higherTags = paramSet;
  }

  public void setIgnorePermitted(boolean paramBoolean)
  {
    this.ignorePermitted = paramBoolean;
  }

  public void setMustCloseTags(Set paramSet)
  {
    this.mustCloseTags = paramSet;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setPermittedTags(Set paramSet)
  {
    this.permittedTags = paramSet;
  }

  public void setRequiredParent(String paramString)
  {
    this.requiredParent = paramString;
  }

  public void setUnique(boolean paramBoolean)
  {
    this.unique = paramBoolean;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.TagInfo
 * JD-Core Version:    0.5.4
 */