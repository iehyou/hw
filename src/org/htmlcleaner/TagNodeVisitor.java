package org.htmlcleaner;

public abstract interface TagNodeVisitor
{
  public abstract boolean visit(TagNode paramTagNode, HtmlNode paramHtmlNode);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.TagNodeVisitor
 * JD-Core Version:    0.5.4
 */