package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public abstract interface BaseToken
{
  public abstract void serialize(Serializer paramSerializer, Writer paramWriter)
    throws IOException;
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.BaseToken
 * JD-Core Version:    0.5.4
 */