package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

public class SimpleXmlSerializer extends XmlSerializer
{
  public SimpleXmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializeOpenTag(paramTagNode, paramWriter, false);
    if (isMinimizedTagSyntax(paramTagNode))
      return;
    Iterator localIterator = paramTagNode.getChildren().iterator();
    while (localIterator.hasNext())
    {
      label25: Object localObject = localIterator.next();
      if (localObject instanceof ContentNode)
      {
        String str1 = localObject.toString();
        if (dontEscape(paramTagNode));
        for (String str2 = str1.replaceAll("]]>", "]]&gt;"); ; str2 = escapeXml(str1))
        {
          paramWriter.write(str2);
          break label25:
        }
      }
      if (!localObject instanceof BaseToken)
        continue;
      ((BaseToken)localObject).serialize(this, paramWriter);
    }
    serializeEndTag(paramTagNode, paramWriter, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.SimpleXmlSerializer
 * JD-Core Version:    0.5.4
 */