package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class PrettyXmlSerializer extends XmlSerializer
{
  private static final String DEFAULT_INDENTATION_STRING = "\t";
  private String indentString = "\t";
  private List<String> indents = new ArrayList();

  public PrettyXmlSerializer(CleanerProperties paramCleanerProperties)
  {
    this(paramCleanerProperties, "\t");
  }

  public PrettyXmlSerializer(CleanerProperties paramCleanerProperties, String paramString)
  {
    super(paramCleanerProperties);
    this.indentString = paramString;
  }

  private String getIndent(int paramInt)
  {
    monitorenter;
    while (true)
    {
      int i;
      Object localObject2;
      try
      {
        i = this.indents.size();
        if (i <= paramInt)
          if (i == 0)
            localObject2 = null;
          else
            while (true)
            {
              this.indents.add(localObject3);
              localObject2 = localObject3;
              ++j;
              break label124:
              localObject2 = (String)this.indents.get(i - 1);
              break label121:
              localObject3 = (String)localObject2 + this.indentString;
            }
        String str = (String)this.indents.get(paramInt);
        return str;
      }
      finally
      {
        monitorexit;
      }
      label121: int j = i;
      label124: if (j > paramInt)
        continue;
      if (localObject2 != null)
        continue;
      Object localObject3 = "";
    }
  }

  private String getIndentedText(String paramString, int paramInt)
  {
    String str1 = getIndent(paramInt);
    StringBuilder localStringBuilder = new StringBuilder(paramString.length());
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "\n\r");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str2 = localStringTokenizer.nextToken().trim();
      if ("".equals(str2))
        continue;
      localStringBuilder.append(str1).append(str2).append("\n");
    }
    return localStringBuilder.toString();
  }

  private String getSingleLineOfChildren(List paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    for (int i = 1; localIterator.hasNext(); i = 0)
    {
      Object localObject = localIterator.next();
      if (!localObject instanceof ContentNode);
      String str;
      do
      {
        return null;
        str = localObject.toString();
        if (i != 0)
          str = Utils.ltrim(str);
        if (localIterator.hasNext())
          continue;
        str = Utils.rtrim(str);
      }
      while ((str.indexOf("\n") >= 0) || (str.indexOf("\r") >= 0));
      localStringBuilder.append(str);
    }
    return localStringBuilder.toString();
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializePrettyXml(paramTagNode, paramWriter, 0);
  }

  protected void serializePrettyXml(TagNode paramTagNode, Writer paramWriter, int paramInt)
    throws IOException
  {
    List localList = paramTagNode.getChildren();
    boolean bool1 = Utils.isEmptyString(paramTagNode.getName());
    String str1;
    label24: String str2;
    boolean bool2;
    if (bool1)
    {
      str1 = "";
      paramWriter.write(str1);
      serializeOpenTag(paramTagNode, paramWriter, true);
      if (!isMinimizedTagSyntax(paramTagNode))
      {
        str2 = getSingleLineOfChildren(localList);
        bool2 = dontEscape(paramTagNode);
        if (str2 == null)
          break label128;
        if (dontEscape(paramTagNode))
          break label112;
        paramWriter.write(escapeXml(str2));
      }
    }
    while (true)
    {
      if (str2 == null)
        paramWriter.write(str1);
      serializeEndTag(paramTagNode, paramWriter, true);
      return;
      str1 = getIndent(paramInt);
      break label24:
      label112: paramWriter.write(str2.replaceAll("]]>", "]]&gt;"));
    }
    if (!bool1)
      label128: paramWriter.write("\n");
    Iterator localIterator = localList.iterator();
    label148: Object localObject;
    do
    {
      if (localIterator.hasNext());
      localObject = localIterator.next();
      if (localObject instanceof TagNode)
      {
        TagNode localTagNode = (TagNode)localObject;
        if (bool1);
        for (int k = paramInt; ; k = paramInt + 1)
        {
          serializePrettyXml(localTagNode, paramWriter, k);
          break label148:
        }
      }
      if (!localObject instanceof ContentNode)
        continue;
      String str4;
      if (bool2)
      {
        str4 = localObject.toString().replaceAll("]]>", "]]&gt;");
        label237: if (!bool1)
          break label274;
      }
      for (int j = paramInt; ; j = paramInt + 1)
      {
        paramWriter.write(getIndentedText(str4, j));
        break label148:
        str4 = escapeXml(localObject.toString());
        label274: break label237:
      }
    }
    while (!localObject instanceof CommentNode);
    String str3 = ((CommentNode)localObject).getCommentedContent();
    if (bool1);
    for (int i = paramInt; ; i = paramInt + 1)
    {
      paramWriter.write(getIndentedText(str3, i));
      break label148:
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.PrettyXmlSerializer
 * JD-Core Version:    0.5.4
 */