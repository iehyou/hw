package org.htmlcleaner;

public class CleanerProperties
{
  public static final String BOOL_ATT_EMPTY = "empty";
  public static final String BOOL_ATT_SELF = "self";
  public static final String BOOL_ATT_TRUE = "true";
  boolean advancedXmlEscape = true;
  boolean allowHtmlInsideAttributes = false;
  boolean allowMultiWordAttributes = true;
  String booleanAttributeValues = "self";
  String hyphenReplacementInComment = "=";
  boolean ignoreQuestAndExclam = true;
  boolean namespacesAware = true;
  boolean omitComments = false;
  boolean omitDeprecatedTags = false;
  boolean omitDoctypeDeclaration = true;
  boolean omitHtmlEnvelope = false;
  boolean omitUnknownTags = false;
  boolean omitXmlDeclaration = false;
  String pruneTags = null;
  boolean recognizeUnicodeChars = true;
  ITagInfoProvider tagInfoProvider = null;
  boolean transResCharsToNCR = false;
  boolean transSpecialEntitiesToNCR = false;
  boolean translateSpecialEntities = true;
  boolean treatDeprecatedTagsAsContent = false;
  boolean treatUnknownTagsAsContent = false;
  boolean useCdataForScriptAndStyle = true;
  boolean useEmptyElementTags = true;

  public String getBooleanAttributeValues()
  {
    return this.booleanAttributeValues;
  }

  public String getHyphenReplacementInComment()
  {
    return this.hyphenReplacementInComment;
  }

  public String getPruneTags()
  {
    return this.pruneTags;
  }

  public ITagInfoProvider getTagInfoProvider()
  {
    return this.tagInfoProvider;
  }

  public boolean isAdvancedXmlEscape()
  {
    return this.advancedXmlEscape;
  }

  public boolean isAllowHtmlInsideAttributes()
  {
    return this.allowHtmlInsideAttributes;
  }

  public boolean isAllowMultiWordAttributes()
  {
    return this.allowMultiWordAttributes;
  }

  public boolean isIgnoreQuestAndExclam()
  {
    return this.ignoreQuestAndExclam;
  }

  public boolean isNamespacesAware()
  {
    return this.namespacesAware;
  }

  public boolean isOmitComments()
  {
    return this.omitComments;
  }

  public boolean isOmitDeprecatedTags()
  {
    return this.omitDeprecatedTags;
  }

  public boolean isOmitDoctypeDeclaration()
  {
    return this.omitDoctypeDeclaration;
  }

  public boolean isOmitHtmlEnvelope()
  {
    return this.omitHtmlEnvelope;
  }

  public boolean isOmitUnknownTags()
  {
    return this.omitUnknownTags;
  }

  public boolean isOmitXmlDeclaration()
  {
    return this.omitXmlDeclaration;
  }

  public boolean isRecognizeUnicodeChars()
  {
    return this.recognizeUnicodeChars;
  }

  public boolean isTransResCharsToNCR()
  {
    return this.transResCharsToNCR;
  }

  public boolean isTransSpecialEntitiesToNCR()
  {
    return this.transSpecialEntitiesToNCR;
  }

  public boolean isTranslateSpecialEntities()
  {
    return this.translateSpecialEntities;
  }

  public boolean isTreatDeprecatedTagsAsContent()
  {
    return this.treatDeprecatedTagsAsContent;
  }

  public boolean isTreatUnknownTagsAsContent()
  {
    return this.treatUnknownTagsAsContent;
  }

  public boolean isUseCdataForScriptAndStyle()
  {
    return this.useCdataForScriptAndStyle;
  }

  public boolean isUseEmptyElementTags()
  {
    return this.useEmptyElementTags;
  }

  public void setAdvancedXmlEscape(boolean paramBoolean)
  {
    this.advancedXmlEscape = paramBoolean;
  }

  public void setAllowHtmlInsideAttributes(boolean paramBoolean)
  {
    this.allowHtmlInsideAttributes = paramBoolean;
  }

  public void setAllowMultiWordAttributes(boolean paramBoolean)
  {
    this.allowMultiWordAttributes = paramBoolean;
  }

  public void setBooleanAttributeValues(String paramString)
  {
    if (("self".equalsIgnoreCase(paramString)) || ("empty".equalsIgnoreCase(paramString)) || ("true".equalsIgnoreCase(paramString)))
    {
      this.booleanAttributeValues = paramString.toLowerCase();
      return;
    }
    this.booleanAttributeValues = "self";
  }

  public void setHyphenReplacementInComment(String paramString)
  {
    this.hyphenReplacementInComment = paramString;
  }

  public void setIgnoreQuestAndExclam(boolean paramBoolean)
  {
    this.ignoreQuestAndExclam = paramBoolean;
  }

  public void setNamespacesAware(boolean paramBoolean)
  {
    this.namespacesAware = paramBoolean;
  }

  public void setOmitComments(boolean paramBoolean)
  {
    this.omitComments = paramBoolean;
  }

  public void setOmitDeprecatedTags(boolean paramBoolean)
  {
    this.omitDeprecatedTags = paramBoolean;
  }

  public void setOmitDoctypeDeclaration(boolean paramBoolean)
  {
    this.omitDoctypeDeclaration = paramBoolean;
  }

  public void setOmitHtmlEnvelope(boolean paramBoolean)
  {
    this.omitHtmlEnvelope = paramBoolean;
  }

  public void setOmitUnknownTags(boolean paramBoolean)
  {
    this.omitUnknownTags = paramBoolean;
  }

  public void setOmitXmlDeclaration(boolean paramBoolean)
  {
    this.omitXmlDeclaration = paramBoolean;
  }

  public void setPruneTags(String paramString)
  {
    this.pruneTags = paramString;
  }

  public void setRecognizeUnicodeChars(boolean paramBoolean)
  {
    this.recognizeUnicodeChars = paramBoolean;
  }

  public void setTransResCharsToNCR(boolean paramBoolean)
  {
    this.transResCharsToNCR = paramBoolean;
  }

  public void setTransSpecialEntitiesToNCR(boolean paramBoolean)
  {
    this.transSpecialEntitiesToNCR = paramBoolean;
  }

  public void setTranslateSpecialEntities(boolean paramBoolean)
  {
    this.translateSpecialEntities = paramBoolean;
  }

  public void setTreatDeprecatedTagsAsContent(boolean paramBoolean)
  {
    this.treatDeprecatedTagsAsContent = paramBoolean;
  }

  public void setTreatUnknownTagsAsContent(boolean paramBoolean)
  {
    this.treatUnknownTagsAsContent = paramBoolean;
  }

  public void setUseCdataForScriptAndStyle(boolean paramBoolean)
  {
    this.useCdataForScriptAndStyle = paramBoolean;
  }

  public void setUseEmptyElementTags(boolean paramBoolean)
  {
    this.useEmptyElementTags = paramBoolean;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CleanerProperties
 * JD-Core Version:    0.5.4
 */