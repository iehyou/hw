package org.htmlcleaner;

public class HtmlCleanerException extends RuntimeException
{
  public HtmlCleanerException()
  {
    this("HtmlCleaner expression occureed!");
  }

  public HtmlCleanerException(String paramString)
  {
    super(paramString);
  }

  public HtmlCleanerException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public HtmlCleanerException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.HtmlCleanerException
 * JD-Core Version:    0.5.4
 */