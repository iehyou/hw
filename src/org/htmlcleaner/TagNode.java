package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TagNode extends TagToken
  implements HtmlNode
{
  private Map<String, String> attributes;
  private List children;
  private DoctypeToken docType;
  private transient boolean isFormed;
  private List<BaseToken> itemsToMove;
  private Map<String, String> nsDeclarations;
  private TagNode parent;

  public TagNode(String paramString)
  {
  }

  private TagNode findElement(ITagNodeCondition paramITagNodeCondition, boolean paramBoolean)
  {
    TagNode localTagNode1;
    if (paramITagNodeCondition == null)
    {
      localTagNode1 = null;
      return localTagNode1;
    }
    for (int i = 0; i < this.children.size(); ++i)
    {
      Object localObject = this.children.get(i);
      if (!localObject instanceof TagNode)
        continue;
      localTagNode1 = (TagNode)localObject;
      if (!paramITagNodeCondition.satisfy(localTagNode1));
      if (!paramBoolean)
        continue;
      TagNode localTagNode2 = localTagNode1.findElement(paramITagNodeCondition, paramBoolean);
      if (localTagNode2 != null)
        return localTagNode2;
    }
    return null;
  }

  private List getElementList(ITagNodeCondition paramITagNodeCondition, boolean paramBoolean)
  {
    LinkedList localLinkedList = new LinkedList();
    if (paramITagNodeCondition == null)
      return localLinkedList;
    for (int i = 0; ; ++i)
    {
      if (i < this.children.size());
      Object localObject = this.children.get(i);
      if (!localObject instanceof TagNode)
        continue;
      TagNode localTagNode = (TagNode)localObject;
      if (paramITagNodeCondition.satisfy(localTagNode))
        localLinkedList.add(localTagNode);
      if (!paramBoolean)
        continue;
      List localList = localTagNode.getElementList(paramITagNodeCondition, paramBoolean);
      if ((localList == null) || (localList.size() <= 0))
        continue;
      localLinkedList.addAll(localList);
    }
  }

  private TagNode[] getElements(ITagNodeCondition paramITagNodeCondition, boolean paramBoolean)
  {
    List localList = getElementList(paramITagNodeCondition, paramBoolean);
    if (localList == null);
    TagNode[] arrayOfTagNode;
    for (int i = 0; ; i = localList.size())
    {
      arrayOfTagNode = new TagNode[i];
      for (int j = 0; ; ++j)
      {
        if (j >= localList.size())
          break label68;
        arrayOfTagNode[j] = ((TagNode)localList.get(j));
      }
    }
    label68: return arrayOfTagNode;
  }

  private boolean traverseInternally(TagNodeVisitor paramTagNodeVisitor)
  {
    if (paramTagNodeVisitor != null)
    {
      if (this.parent != null);
      boolean bool;
      for (int i = 1; ; i = 0)
      {
        bool = paramTagNodeVisitor.visit(this.parent, this);
        if (bool)
          break;
        return false;
      }
      if ((i != 0) && (this.parent == null))
        return true;
      Object[] arrayOfObject = this.children.toArray();
      int j = arrayOfObject.length;
      int k = 0;
      if (k < j)
      {
        label68: Object localObject = arrayOfObject[k];
        if (localObject instanceof TagNode);
        for (bool = ((TagNode)localObject).traverseInternally(paramTagNodeVisitor); ; bool = paramTagNodeVisitor.visit(this, (CommentNode)localObject))
          do
            while (true)
            {
              if (bool);
              ++k;
              break label68:
              if (!localObject instanceof ContentNode)
                break;
              bool = paramTagNodeVisitor.visit(this, (ContentNode)localObject);
            }
          while (!localObject instanceof CommentNode);
      }
    }
    return true;
  }

  @Deprecated
  public void addAttribute(String paramString1, String paramString2)
  {
    setAttribute(paramString1, paramString2);
  }

  public void addChild(Object paramObject)
  {
    if (paramObject == null);
    do
    {
      return;
      if (paramObject instanceof List)
      {
        addChildren((List)paramObject);
        return;
      }
      this.children.add(paramObject);
    }
    while (!paramObject instanceof TagNode);
    ((TagNode)paramObject).parent = this;
  }

  public void addChildren(List paramList)
  {
    if (paramList == null)
      return;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      addChild(localIterator.next());
  }

  void addItemForMoving(BaseToken paramBaseToken)
  {
    if (this.itemsToMove == null)
      this.itemsToMove = new ArrayList();
    this.itemsToMove.add(paramBaseToken);
  }

  public void addNamespaceDeclaration(String paramString1, String paramString2)
  {
    if (this.nsDeclarations == null)
      this.nsDeclarations = new TreeMap();
    this.nsDeclarations.put(paramString1, paramString2);
  }

  void collectNamespacePrefixesOnPath(Set<String> paramSet)
  {
    Map localMap = getNamespaceDeclarations();
    if (localMap != null)
    {
      Iterator localIterator = localMap.keySet().iterator();
      while (localIterator.hasNext())
        paramSet.add((String)localIterator.next());
    }
    if (this.parent == null)
      return;
    this.parent.collectNamespacePrefixesOnPath(paramSet);
  }

  public Object[] evaluateXPath(String paramString)
    throws XPatherException
  {
    return new XPather(paramString).evaluateAgainstNode(this);
  }

  public TagNode findElementByAttValue(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    return findElement(new TagNodeAttValueCondition(paramString1, paramString2, paramBoolean2), paramBoolean1);
  }

  public TagNode findElementByName(String paramString, boolean paramBoolean)
  {
    return findElement(new TagNodeNameCondition(paramString), paramBoolean);
  }

  public TagNode findElementHavingAttribute(String paramString, boolean paramBoolean)
  {
    return findElement(new TagNodeAttExistsCondition(paramString), paramBoolean);
  }

  public TagNode[] getAllElements(boolean paramBoolean)
  {
    return getElements(new TagAllCondition(), paramBoolean);
  }

  public List getAllElementsList(boolean paramBoolean)
  {
    return getElementList(new TagAllCondition(), paramBoolean);
  }

  public String getAttributeByName(String paramString)
  {
    if (paramString != null)
      return (String)this.attributes.get(paramString.toLowerCase());
    return null;
  }

  public Map<String, String> getAttributes()
  {
    return this.attributes;
  }

  public int getChildIndex(HtmlNode paramHtmlNode)
  {
    int i = 0;
    Iterator localIterator = this.children.iterator();
    while (localIterator.hasNext())
    {
      if (localIterator.next() == paramHtmlNode)
        return i;
      ++i;
    }
    return -1;
  }

  public List getChildTagList()
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < this.children.size(); ++i)
    {
      Object localObject = this.children.get(i);
      if (!localObject instanceof TagNode)
        continue;
      localArrayList.add(localObject);
    }
    return localArrayList;
  }

  public TagNode[] getChildTags()
  {
    List localList = getChildTagList();
    TagNode[] arrayOfTagNode = new TagNode[localList.size()];
    for (int i = 0; i < localList.size(); ++i)
      arrayOfTagNode[i] = ((TagNode)localList.get(i));
    return arrayOfTagNode;
  }

  public List getChildren()
  {
    return this.children;
  }

  public DoctypeToken getDocType()
  {
    return this.docType;
  }

  public List getElementListByAttValue(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    return getElementList(new TagNodeAttValueCondition(paramString1, paramString2, paramBoolean2), paramBoolean1);
  }

  public List getElementListByName(String paramString, boolean paramBoolean)
  {
    return getElementList(new TagNodeNameCondition(paramString), paramBoolean);
  }

  public List getElementListHavingAttribute(String paramString, boolean paramBoolean)
  {
    return getElementList(new TagNodeAttExistsCondition(paramString), paramBoolean);
  }

  public TagNode[] getElementsByAttValue(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    return getElements(new TagNodeAttValueCondition(paramString1, paramString2, paramBoolean2), paramBoolean1);
  }

  public TagNode[] getElementsByName(String paramString, boolean paramBoolean)
  {
    return getElements(new TagNodeNameCondition(paramString), paramBoolean);
  }

  public TagNode[] getElementsHavingAttribute(String paramString, boolean paramBoolean)
  {
    return getElements(new TagNodeAttExistsCondition(paramString), paramBoolean);
  }

  List<BaseToken> getItemsToMove()
  {
    return this.itemsToMove;
  }

  public Map<String, String> getNamespaceDeclarations()
  {
    return this.nsDeclarations;
  }

  String getNamespaceURIOnPath(String paramString)
  {
    if (this.nsDeclarations != null)
    {
      Iterator localIterator = this.nsDeclarations.entrySet().iterator();
      Map.Entry localEntry;
      while (localIterator.hasNext())
      {
        localEntry = (Map.Entry)localIterator.next();
        String str = (String)localEntry.getKey();
        if ((str.equals(paramString)) || (("".equals(str)) && (paramString == null)))
          return (String)localEntry.getValue();
      }
    }
    if (this.parent != null)
      return this.parent.getNamespaceURIOnPath(paramString);
    return null;
  }

  public TagNode getParent()
  {
    return this.parent;
  }

  public StringBuffer getText()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    if (i < this.children.size())
    {
      label10: Object localObject = this.children.get(i);
      if (localObject instanceof ContentNode)
        localStringBuffer.append(localObject.toString());
      while (true)
      {
        ++i;
        break label10:
        if (!localObject instanceof TagNode)
          continue;
        localStringBuffer.append(((TagNode)localObject).getText());
      }
    }
    return localStringBuffer;
  }

  public boolean hasAttribute(String paramString)
  {
    if (paramString != null)
      return this.attributes.containsKey(paramString.toLowerCase());
    return false;
  }

  public boolean hasChildren()
  {
    return this.children.size() > 0;
  }

  public void insertChild(int paramInt, HtmlNode paramHtmlNode)
  {
    this.children.add(paramInt, paramHtmlNode);
  }

  public void insertChildAfter(HtmlNode paramHtmlNode1, HtmlNode paramHtmlNode2)
  {
    int i = getChildIndex(paramHtmlNode1);
    if (i < 0)
      return;
    insertChild(i + 1, paramHtmlNode2);
  }

  public void insertChildBefore(HtmlNode paramHtmlNode1, HtmlNode paramHtmlNode2)
  {
    int i = getChildIndex(paramHtmlNode1);
    if (i < 0)
      return;
    insertChild(i, paramHtmlNode2);
  }

  boolean isFormed()
  {
    return this.isFormed;
  }

  TagNode makeCopy()
  {
    TagNode localTagNode = new TagNode(this.name);
    localTagNode.attributes.putAll(this.attributes);
    return localTagNode;
  }

  public void removeAllChildren()
  {
    this.children.clear();
  }

  public void removeAttribute(String paramString)
  {
    if ((paramString == null) || ("".equals(paramString.trim())))
      return;
    this.attributes.remove(paramString.toLowerCase());
  }

  public boolean removeChild(Object paramObject)
  {
    return this.children.remove(paramObject);
  }

  public boolean removeFromTree()
  {
    if (this.parent != null)
    {
      boolean bool = this.parent.removeChild(this);
      this.parent = null;
      return bool;
    }
    return false;
  }

  public void replaceChild(HtmlNode paramHtmlNode1, HtmlNode paramHtmlNode2)
  {
    if (paramHtmlNode2 == null)
      return;
    ListIterator localListIterator = this.children.listIterator();
    do
      if (!localListIterator.hasNext());
    while (localListIterator.next() != paramHtmlNode1);
    localListIterator.set(paramHtmlNode2);
  }

  public void serialize(Serializer paramSerializer, Writer paramWriter)
    throws IOException
  {
    paramSerializer.serialize(this, paramWriter);
  }

  public void setAttribute(String paramString1, String paramString2)
  {
    String str;
    if ((paramString1 != null) && (!"".equals(paramString1.trim())))
    {
      str = paramString1.toLowerCase();
      if (!"xmlns".equals(str))
        break label39;
      addNamespaceDeclaration("", paramString2);
    }
    return;
    if (str.startsWith("xmlns:"))
    {
      label39: addNamespaceDeclaration(str.substring(6), paramString2);
      return;
    }
    Map localMap = this.attributes;
    if (paramString2 == null)
      paramString2 = "";
    localMap.put(str, paramString2);
  }

  void setChildren(List paramList)
  {
    this.children = paramList;
  }

  public void setDocType(DoctypeToken paramDoctypeToken)
  {
    this.docType = paramDoctypeToken;
  }

  void setFormed()
  {
    setFormed(true);
  }

  void setFormed(boolean paramBoolean)
  {
    this.isFormed = paramBoolean;
  }

  void setItemsToMove(List<BaseToken> paramList)
  {
    this.itemsToMove = paramList;
  }

  public boolean setName(String paramString)
  {
    if (Utils.isValidXmlIdentifier(paramString))
    {
      this.name = paramString;
      return true;
    }
    return false;
  }

  void transformAttributes(TagTransformation paramTagTransformation)
  {
    boolean bool1 = paramTagTransformation.isPreserveSourceAttributes();
    boolean bool2 = paramTagTransformation.hasAttributeTransformations();
    if ((!bool2) && (bool1))
      return;
    LinkedHashMap localLinkedHashMap;
    Iterator localIterator;
    if (bool1)
    {
      localLinkedHashMap = new LinkedHashMap(this.attributes);
      if (!bool2)
        break label153;
      localIterator = paramTagTransformation.getAttributeTransformations().entrySet().iterator();
    }
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str1 = (String)localEntry.getKey();
      String str2 = (String)localEntry.getValue();
      if (str2 == null)
      {
        localLinkedHashMap.remove(str1);
        continue;
        localLinkedHashMap = new LinkedHashMap();
      }
      localLinkedHashMap.put(str1, Utils.evaluateTemplate(str2, this.attributes));
    }
    label153: this.attributes = localLinkedHashMap;
  }

  public void traverse(TagNodeVisitor paramTagNodeVisitor)
  {
    traverseInternally(paramTagNodeVisitor);
  }

  public static abstract interface ITagNodeCondition
  {
    public abstract boolean satisfy(TagNode paramTagNode);
  }

  public class TagAllCondition
    implements TagNode.ITagNodeCondition
  {
    public TagAllCondition()
    {
    }

    public boolean satisfy(TagNode paramTagNode)
    {
      return true;
    }
  }

  public class TagNodeAttExistsCondition
    implements TagNode.ITagNodeCondition
  {
    private String attName;

    public TagNodeAttExistsCondition(String arg2)
    {
      Object localObject;
      this.attName = localObject;
    }

    public boolean satisfy(TagNode paramTagNode)
    {
      if (paramTagNode == null)
        return false;
      return paramTagNode.attributes.containsKey(this.attName.toLowerCase());
    }
  }

  public class TagNodeAttValueCondition
    implements TagNode.ITagNodeCondition
  {
    private String attName;
    private String attValue;
    private boolean isCaseSensitive;

    public TagNodeAttValueCondition(String paramString1, String paramBoolean, boolean arg4)
    {
      this.attName = paramString1;
      this.attValue = paramBoolean;
      int i;
      this.isCaseSensitive = i;
    }

    public boolean satisfy(TagNode paramTagNode)
    {
      if ((paramTagNode == null) || (this.attName == null) || (this.attValue == null))
        return false;
      if (this.isCaseSensitive)
        return this.attValue.equals(paramTagNode.getAttributeByName(this.attName));
      return this.attValue.equalsIgnoreCase(paramTagNode.getAttributeByName(this.attName));
    }
  }

  public class TagNodeNameCondition
    implements TagNode.ITagNodeCondition
  {
    private String name;

    public TagNodeNameCondition(String arg2)
    {
      Object localObject;
      this.name = localObject;
    }

    public boolean satisfy(TagNode paramTagNode)
    {
      if (paramTagNode == null)
        return false;
      return paramTagNode.name.equalsIgnoreCase(this.name);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.TagNode
 * JD-Core Version:    0.5.4
 */