package org.htmlcleaner;

public abstract interface ITagInfoProvider
{
  public abstract TagInfo getTagInfo(String paramString);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.ITagInfoProvider
 * JD-Core Version:    0.5.4
 */