package org.htmlcleaner;

import java.util.HashMap;
import java.util.Map;

public class CleanerTransformations
{
  private Map mappings = new HashMap();

  public void addTransformation(TagTransformation paramTagTransformation)
  {
    if (paramTagTransformation == null)
      return;
    this.mappings.put(paramTagTransformation.getSourceTag(), paramTagTransformation);
  }

  public TagTransformation getTransformation(String paramString)
  {
    if (paramString != null)
      return (TagTransformation)this.mappings.get(paramString.toLowerCase());
    return null;
  }

  public boolean hasTransformationForTag(String paramString)
  {
    return (paramString != null) && (this.mappings.containsKey(paramString.toLowerCase()));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CleanerTransformations
 * JD-Core Version:    0.5.4
 */