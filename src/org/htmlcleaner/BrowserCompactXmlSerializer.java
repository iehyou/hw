package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.ListIterator;

public class BrowserCompactXmlSerializer extends XmlSerializer
{
  public BrowserCompactXmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializeOpenTag(paramTagNode, paramWriter, false);
    List localList = paramTagNode.getChildren();
    if (isMinimizedTagSyntax(paramTagNode))
      return;
    ListIterator localListIterator = localList.listIterator();
    while (localListIterator.hasNext())
    {
      label28: Object localObject = localListIterator.next();
      if (localObject instanceof ContentNode)
      {
        String str1 = localObject.toString();
        int i;
        label85: int j;
        if ((str1.length() > 0) && (Character.isWhitespace(str1.charAt(0))))
        {
          i = 1;
          if ((str1.length() <= 1) || (!Character.isWhitespace(str1.charAt(-1 + str1.length()))))
            break label219;
          j = 1;
          label115: if (!dontEscape(paramTagNode))
            break label225;
        }
        for (String str2 = str1.trim().replaceAll("]]>", "]]&gt;"); ; str2 = escapeXml(str1.trim()))
        {
          if (i != 0)
            paramWriter.write(32);
          if (str2.length() != 0)
          {
            paramWriter.write(str2);
            if (j != 0)
              paramWriter.write(32);
          }
          if (localListIterator.hasNext());
          if (!Utils.isWhitespaceString(localListIterator.next()))
            paramWriter.write("\n");
          localListIterator.previous();
          break label28:
          i = 0;
          break label85:
          label219: j = 0;
          label225: break label115:
        }
      }
      if (localObject instanceof CommentNode)
        paramWriter.write(((CommentNode)localObject).getCommentedContent().trim());
      if (!localObject instanceof BaseToken)
        continue;
      ((BaseToken)localObject).serialize(this, paramWriter);
    }
    serializeEndTag(paramTagNode, paramWriter, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.BrowserCompactXmlSerializer
 * JD-Core Version:    0.5.4
 */