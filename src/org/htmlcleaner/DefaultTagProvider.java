package org.htmlcleaner;

import java.util.HashMap;

public class DefaultTagProvider extends HashMap<String, TagInfo>
  implements ITagInfoProvider
{
  private static DefaultTagProvider _instance;

  public DefaultTagProvider()
  {
    TagInfo localTagInfo1 = new TagInfo("div", 0, 2, false, false, false);
    localTagInfo1.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo1.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("div", localTagInfo1);
    put("span", new TagInfo("span", 0, 2, false, false, false));
    TagInfo localTagInfo2 = new TagInfo("meta", 1, 1, false, false, false);
    put("meta", localTagInfo2);
    TagInfo localTagInfo3 = new TagInfo("link", 1, 1, false, false, false);
    put("link", localTagInfo3);
    TagInfo localTagInfo4 = new TagInfo("title", 2, 1, false, true, false);
    put("title", localTagInfo4);
    TagInfo localTagInfo5 = new TagInfo("style", 2, 1, false, false, false);
    put("style", localTagInfo5);
    TagInfo localTagInfo6 = new TagInfo("bgsound", 1, 1, false, false, false);
    put("bgsound", localTagInfo6);
    TagInfo localTagInfo7 = new TagInfo("h1", 0, 2, false, false, false);
    localTagInfo7.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo7.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h1", localTagInfo7);
    TagInfo localTagInfo8 = new TagInfo("h2", 0, 2, false, false, false);
    localTagInfo8.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo8.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h2", localTagInfo8);
    TagInfo localTagInfo9 = new TagInfo("h3", 0, 2, false, false, false);
    localTagInfo9.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo9.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h3", localTagInfo9);
    TagInfo localTagInfo10 = new TagInfo("h4", 0, 2, false, false, false);
    localTagInfo10.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo10.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h4", localTagInfo10);
    TagInfo localTagInfo11 = new TagInfo("h5", 0, 2, false, false, false);
    localTagInfo11.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo11.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h5", localTagInfo11);
    TagInfo localTagInfo12 = new TagInfo("h6", 0, 2, false, false, false);
    localTagInfo12.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo12.defineCloseBeforeTags("h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("h6", localTagInfo12);
    TagInfo localTagInfo13 = new TagInfo("p", 0, 2, false, false, false);
    localTagInfo13.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo13.defineCloseBeforeTags("p,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("p", localTagInfo13);
    put("strong", new TagInfo("strong", 0, 2, false, false, false));
    put("em", new TagInfo("em", 0, 2, false, false, false));
    put("abbr", new TagInfo("abbr", 0, 2, false, false, false));
    put("acronym", new TagInfo("acronym", 0, 2, false, false, false));
    TagInfo localTagInfo14 = new TagInfo("address", 0, 2, false, false, false);
    localTagInfo14.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo14.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("address", localTagInfo14);
    put("bdo", new TagInfo("bdo", 0, 2, false, false, false));
    TagInfo localTagInfo15 = new TagInfo("blockquote", 0, 2, false, false, false);
    localTagInfo15.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo15.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("blockquote", localTagInfo15);
    put("cite", new TagInfo("cite", 0, 2, false, false, false));
    put("q", new TagInfo("q", 0, 2, false, false, false));
    put("code", new TagInfo("code", 0, 2, false, false, false));
    put("ins", new TagInfo("ins", 0, 2, false, false, false));
    put("del", new TagInfo("del", 0, 2, false, false, false));
    put("dfn", new TagInfo("dfn", 0, 2, false, false, false));
    put("kbd", new TagInfo("kbd", 0, 2, false, false, false));
    TagInfo localTagInfo16 = new TagInfo("pre", 0, 2, false, false, false);
    localTagInfo16.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo16.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("pre", localTagInfo16);
    put("samp", new TagInfo("samp", 0, 2, false, false, false));
    TagInfo localTagInfo17 = new TagInfo("listing", 0, 2, false, false, false);
    localTagInfo17.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo17.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("listing", localTagInfo17);
    put("var", new TagInfo("var", 0, 2, false, false, false));
    TagInfo localTagInfo18 = new TagInfo("br", 1, 2, false, false, false);
    put("br", localTagInfo18);
    TagInfo localTagInfo19 = new TagInfo("wbr", 1, 2, false, false, false);
    put("wbr", localTagInfo19);
    TagInfo localTagInfo20 = new TagInfo("nobr", 0, 2, false, false, false);
    localTagInfo20.defineCloseBeforeTags("nobr");
    put("nobr", localTagInfo20);
    TagInfo localTagInfo21 = new TagInfo("xmp", 2, 2, false, false, false);
    put("xmp", localTagInfo21);
    TagInfo localTagInfo22 = new TagInfo("a", 0, 2, false, false, false);
    localTagInfo22.defineCloseBeforeTags("a");
    put("a", localTagInfo22);
    TagInfo localTagInfo23 = new TagInfo("base", 1, 1, false, false, false);
    put("base", localTagInfo23);
    TagInfo localTagInfo24 = new TagInfo("img", 1, 2, false, false, false);
    put("img", localTagInfo24);
    TagInfo localTagInfo25 = new TagInfo("area", 1, 2, false, false, false);
    localTagInfo25.defineFatalTags("map");
    localTagInfo25.defineCloseBeforeTags("area");
    put("area", localTagInfo25);
    TagInfo localTagInfo26 = new TagInfo("map", 0, 2, false, false, false);
    localTagInfo26.defineCloseBeforeTags("map");
    put("map", localTagInfo26);
    put("object", new TagInfo("object", 0, 2, false, false, false));
    TagInfo localTagInfo27 = new TagInfo("param", 1, 2, false, false, false);
    localTagInfo27.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo27.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("param", localTagInfo27);
    put("applet", new TagInfo("applet", 0, 2, true, false, false));
    put("xml", new TagInfo("xml", 0, 2, false, false, false));
    TagInfo localTagInfo28 = new TagInfo("ul", 0, 2, false, false, false);
    localTagInfo28.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo28.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("ul", localTagInfo28);
    TagInfo localTagInfo29 = new TagInfo("ol", 0, 2, false, false, false);
    localTagInfo29.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo29.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("ol", localTagInfo29);
    TagInfo localTagInfo30 = new TagInfo("li", 0, 2, false, false, false);
    localTagInfo30.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo30.defineCloseBeforeTags("li,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("li", localTagInfo30);
    TagInfo localTagInfo31 = new TagInfo("dl", 0, 2, false, false, false);
    localTagInfo31.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo31.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("dl", localTagInfo31);
    TagInfo localTagInfo32 = new TagInfo("dt", 0, 2, false, false, false);
    localTagInfo32.defineCloseBeforeTags("dt,dd");
    put("dt", localTagInfo32);
    TagInfo localTagInfo33 = new TagInfo("dd", 0, 2, false, false, false);
    localTagInfo33.defineCloseBeforeTags("dt,dd");
    put("dd", localTagInfo33);
    TagInfo localTagInfo34 = new TagInfo("menu", 0, 2, true, false, false);
    localTagInfo34.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo34.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("menu", localTagInfo34);
    TagInfo localTagInfo35 = new TagInfo("dir", 0, 2, true, false, false);
    localTagInfo35.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo35.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("dir", localTagInfo35);
    TagInfo localTagInfo36 = new TagInfo("table", 0, 2, false, false, false);
    localTagInfo36.defineAllowedChildrenTags("tr,tbody,thead,tfoot,colgroup,col,form,caption,tr");
    localTagInfo36.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo36.defineCloseBeforeTags("tr,thead,tbody,tfoot,caption,colgroup,table,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param");
    put("table", localTagInfo36);
    TagInfo localTagInfo37 = new TagInfo("tr", 0, 2, false, false, false);
    localTagInfo37.defineFatalTags("table");
    localTagInfo37.defineRequiredEnclosingTags("tbody");
    localTagInfo37.defineAllowedChildrenTags("td,th");
    localTagInfo37.defineHigherLevelTags("thead,tfoot");
    localTagInfo37.defineCloseBeforeTags("tr,td,th,caption,colgroup");
    put("tr", localTagInfo37);
    TagInfo localTagInfo38 = new TagInfo("td", 0, 2, false, false, false);
    localTagInfo38.defineFatalTags("table");
    localTagInfo38.defineRequiredEnclosingTags("tr");
    localTagInfo38.defineCloseBeforeTags("td,th,caption,colgroup");
    put("td", localTagInfo38);
    TagInfo localTagInfo39 = new TagInfo("th", 0, 2, false, false, false);
    localTagInfo39.defineFatalTags("table");
    localTagInfo39.defineRequiredEnclosingTags("tr");
    localTagInfo39.defineCloseBeforeTags("td,th,caption,colgroup");
    put("th", localTagInfo39);
    TagInfo localTagInfo40 = new TagInfo("tbody", 0, 2, false, false, false);
    localTagInfo40.defineFatalTags("table");
    localTagInfo40.defineAllowedChildrenTags("tr,form");
    localTagInfo40.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
    put("tbody", localTagInfo40);
    TagInfo localTagInfo41 = new TagInfo("thead", 0, 2, false, false, false);
    localTagInfo41.defineFatalTags("table");
    localTagInfo41.defineAllowedChildrenTags("tr,form");
    localTagInfo41.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
    put("thead", localTagInfo41);
    TagInfo localTagInfo42 = new TagInfo("tfoot", 0, 2, false, false, false);
    localTagInfo42.defineFatalTags("table");
    localTagInfo42.defineAllowedChildrenTags("tr,form");
    localTagInfo42.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
    put("tfoot", localTagInfo42);
    TagInfo localTagInfo43 = new TagInfo("col", 1, 2, false, false, false);
    localTagInfo43.defineFatalTags("table");
    put("col", localTagInfo43);
    TagInfo localTagInfo44 = new TagInfo("colgroup", 0, 2, false, false, false);
    localTagInfo44.defineFatalTags("table");
    localTagInfo44.defineAllowedChildrenTags("col");
    localTagInfo44.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
    put("colgroup", localTagInfo44);
    TagInfo localTagInfo45 = new TagInfo("caption", 0, 2, false, false, false);
    localTagInfo45.defineFatalTags("table");
    localTagInfo45.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
    put("caption", localTagInfo45);
    TagInfo localTagInfo46 = new TagInfo("form", 0, 2, false, false, true);
    localTagInfo46.defineForbiddenTags("form");
    localTagInfo46.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo46.defineCloseBeforeTags("option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("form", localTagInfo46);
    TagInfo localTagInfo47 = new TagInfo("input", 1, 2, false, false, false);
    localTagInfo47.defineCloseBeforeTags("select,optgroup,option");
    put("input", localTagInfo47);
    TagInfo localTagInfo48 = new TagInfo("textarea", 0, 2, false, false, false);
    localTagInfo48.defineCloseBeforeTags("select,optgroup,option");
    put("textarea", localTagInfo48);
    TagInfo localTagInfo49 = new TagInfo("select", 0, 2, false, false, true);
    localTagInfo49.defineAllowedChildrenTags("option,optgroup");
    localTagInfo49.defineCloseBeforeTags("option,optgroup,select");
    put("select", localTagInfo49);
    TagInfo localTagInfo50 = new TagInfo("option", 2, 2, false, false, true);
    localTagInfo50.defineFatalTags("select");
    localTagInfo50.defineCloseBeforeTags("option");
    put("option", localTagInfo50);
    TagInfo localTagInfo51 = new TagInfo("optgroup", 0, 2, false, false, true);
    localTagInfo51.defineFatalTags("select");
    localTagInfo51.defineAllowedChildrenTags("option");
    localTagInfo51.defineCloseBeforeTags("optgroup");
    put("optgroup", localTagInfo51);
    TagInfo localTagInfo52 = new TagInfo("button", 0, 2, false, false, false);
    localTagInfo52.defineCloseBeforeTags("select,optgroup,option");
    put("button", localTagInfo52);
    put("label", new TagInfo("label", 0, 2, false, false, false));
    TagInfo localTagInfo53 = new TagInfo("fieldset", 0, 2, false, false, false);
    localTagInfo53.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo53.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("fieldset", localTagInfo53);
    TagInfo localTagInfo54 = new TagInfo("legend", 2, 2, false, false, false);
    localTagInfo54.defineRequiredEnclosingTags("fieldset");
    localTagInfo54.defineCloseBeforeTags("legend");
    put("legend", localTagInfo54);
    TagInfo localTagInfo55 = new TagInfo("isindex", 1, 2, true, false, false);
    localTagInfo55.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo55.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("isindex", localTagInfo55);
    TagInfo localTagInfo56 = new TagInfo("script", 0, 0, false, false, false);
    put("script", localTagInfo56);
    TagInfo localTagInfo57 = new TagInfo("noscript", 0, 0, false, false, false);
    put("noscript", localTagInfo57);
    TagInfo localTagInfo58 = new TagInfo("b", 0, 2, false, false, false);
    localTagInfo58.defineCloseInsideCopyAfterTags("u,i,tt,sub,sup,big,small,strike,blink,s");
    put("b", localTagInfo58);
    TagInfo localTagInfo59 = new TagInfo("i", 0, 2, false, false, false);
    localTagInfo59.defineCloseInsideCopyAfterTags("b,u,tt,sub,sup,big,small,strike,blink,s");
    put("i", localTagInfo59);
    TagInfo localTagInfo60 = new TagInfo("u", 0, 2, true, false, false);
    localTagInfo60.defineCloseInsideCopyAfterTags("b,i,tt,sub,sup,big,small,strike,blink,s");
    put("u", localTagInfo60);
    TagInfo localTagInfo61 = new TagInfo("tt", 0, 2, false, false, false);
    localTagInfo61.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,big,small,strike,blink,s");
    put("tt", localTagInfo61);
    TagInfo localTagInfo62 = new TagInfo("sub", 0, 2, false, false, false);
    localTagInfo62.defineCloseInsideCopyAfterTags("b,u,i,tt,sup,big,small,strike,blink,s");
    put("sub", localTagInfo62);
    TagInfo localTagInfo63 = new TagInfo("sup", 0, 2, false, false, false);
    localTagInfo63.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,big,small,strike,blink,s");
    put("sup", localTagInfo63);
    TagInfo localTagInfo64 = new TagInfo("big", 0, 2, false, false, false);
    localTagInfo64.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,small,strike,blink,s");
    put("big", localTagInfo64);
    TagInfo localTagInfo65 = new TagInfo("small", 0, 2, false, false, false);
    localTagInfo65.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,strike,blink,s");
    put("small", localTagInfo65);
    TagInfo localTagInfo66 = new TagInfo("strike", 0, 2, true, false, false);
    localTagInfo66.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,blink,s");
    put("strike", localTagInfo66);
    TagInfo localTagInfo67 = new TagInfo("blink", 0, 2, false, false, false);
    localTagInfo67.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,s");
    put("blink", localTagInfo67);
    TagInfo localTagInfo68 = new TagInfo("marquee", 0, 2, false, false, false);
    localTagInfo68.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo68.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("marquee", localTagInfo68);
    TagInfo localTagInfo69 = new TagInfo("s", 0, 2, true, false, false);
    localTagInfo69.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,blink");
    put("s", localTagInfo69);
    TagInfo localTagInfo70 = new TagInfo("hr", 1, 2, false, false, false);
    localTagInfo70.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo70.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("hr", localTagInfo70);
    put("font", new TagInfo("font", 0, 2, true, false, false));
    TagInfo localTagInfo71 = new TagInfo("basefont", 1, 2, true, false, false);
    put("basefont", localTagInfo71);
    TagInfo localTagInfo72 = new TagInfo("center", 0, 2, true, false, false);
    localTagInfo72.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo72.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("center", localTagInfo72);
    put("comment", new TagInfo("comment", 0, 2, false, false, false));
    put("server", new TagInfo("server", 0, 2, false, false, false));
    put("iframe", new TagInfo("iframe", 0, 2, false, false, false));
    TagInfo localTagInfo73 = new TagInfo("embed", 1, 2, false, false, false);
    localTagInfo73.defineCloseBeforeCopyInsideTags("a,bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font");
    localTagInfo73.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
    put("embed", localTagInfo73);
  }

  public static DefaultTagProvider getInstance()
  {
    monitorenter;
    try
    {
      if (_instance == null)
        _instance = new DefaultTagProvider();
      DefaultTagProvider localDefaultTagProvider = _instance;
      return localDefaultTagProvider;
    }
    finally
    {
      monitorexit;
    }
  }

  public void addTagInfo(TagInfo paramTagInfo)
  {
    if (paramTagInfo == null)
      return;
    put(paramTagInfo.getName().toLowerCase(), paramTagInfo);
  }

  public TagInfo getTagInfo(String paramString)
  {
    return (TagInfo)get(paramString);
  }

  public void removeTagInfo(String paramString)
  {
    if (paramString == null)
      return;
    remove(paramString.toLowerCase());
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.DefaultTagProvider
 * JD-Core Version:    0.5.4
 */