package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.ListIterator;

public class CompactHtmlSerializer extends HtmlSerializer
{
  private int openPreTags = 0;

  public CompactHtmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    boolean bool = "pre".equalsIgnoreCase(paramTagNode.getName());
    if (bool)
      this.openPreTags = (1 + this.openPreTags);
    serializeOpenTag(paramTagNode, paramWriter, false);
    List localList = paramTagNode.getChildren();
    if (isMinimizedTagSyntax(paramTagNode))
      return;
    ListIterator localListIterator = localList.listIterator();
    while (localListIterator.hasNext())
    {
      label54: Object localObject = localListIterator.next();
      if (localObject instanceof ContentNode)
      {
        String str1 = localObject.toString();
        if (this.openPreTags > 0)
          paramWriter.write(str1);
        int i;
        label127: int j;
        if ((str1.length() > 0) && (Character.isWhitespace(str1.charAt(0))))
        {
          i = 1;
          if ((str1.length() <= 1) || (!Character.isWhitespace(str1.charAt(-1 + str1.length()))))
            break label254;
          j = 1;
          label157: if (!dontEscape(paramTagNode))
            break label260;
        }
        for (String str2 = str1.trim(); ; str2 = escapeText(str1.trim()))
        {
          if (i != 0)
            paramWriter.write(32);
          if (str2.length() != 0)
          {
            paramWriter.write(str2);
            if (j != 0)
              paramWriter.write(32);
          }
          if (localListIterator.hasNext());
          if (!Utils.isWhitespaceString(localListIterator.next()))
            paramWriter.write("\n");
          localListIterator.previous();
          break label54:
          i = 0;
          break label127:
          label254: j = 0;
          label260: break label157:
        }
      }
      if (localObject instanceof CommentNode)
        paramWriter.write(((CommentNode)localObject).getCommentedContent().trim());
      if (!localObject instanceof BaseToken)
        continue;
      ((BaseToken)localObject).serialize(this, paramWriter);
    }
    serializeEndTag(paramTagNode, paramWriter, false);
    if (!bool)
      return;
    this.openPreTags = (-1 + this.openPreTags);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CompactHtmlSerializer
 * JD-Core Version:    0.5.4
 */