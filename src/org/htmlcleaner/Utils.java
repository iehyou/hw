package org.htmlcleaner;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils
{
  public static final Map<Character, String> RESERVED_XML_CHARS;
  public static String VAR_END;
  public static String VAR_START = "${";

  static
  {
    VAR_END = "}";
    RESERVED_XML_CHARS = new HashMap();
    RESERVED_XML_CHARS.put(Character.valueOf('&'), "&amp;");
    RESERVED_XML_CHARS.put(Character.valueOf('<'), "&lt;");
    RESERVED_XML_CHARS.put(Character.valueOf('>'), "&gt;");
    RESERVED_XML_CHARS.put(Character.valueOf('"'), "&quot;");
    RESERVED_XML_CHARS.put(Character.valueOf('\''), "&apos;");
  }

  public static String escapeXml(String paramString, CleanerProperties paramCleanerProperties, boolean paramBoolean)
  {
    boolean bool1 = paramCleanerProperties.isAdvancedXmlEscape();
    boolean bool2 = paramCleanerProperties.isRecognizeUnicodeChars();
    boolean bool3 = paramCleanerProperties.isTranslateSpecialEntities();
    if (paramString != null)
    {
      int i = paramString.length();
      StringBuilder localStringBuilder = new StringBuilder(i);
      int j = 0;
      if (j < i)
      {
        label41: char c1 = paramString.charAt(j);
        int i1;
        label113: int i2;
        label121: int i3;
        int i4;
        label137: String str6;
        char c3;
        label163: char c2;
        if (c1 == '&')
          if ((((bool1) || (bool2))) && (j < i - 2) && (paramString.charAt(j + 1) == '#'))
            if (Character.toLowerCase(paramString.charAt(j + 2)) == 'x')
            {
              i1 = 1;
              if (i1 == 0)
                break label207;
              i2 = 3;
              i3 = j + i2;
              if (i1 == 0)
                break label213;
              i4 = 16;
              str6 = "";
              if (i3 < i)
              {
                c3 = paramString.charAt(i3);
                if (c3 != ';')
                  break label220;
              }
              if (!isValidInt(str6, i4))
                break label385;
              c2 = (char)Integer.parseInt(str6, i4);
              if (isValidXmlChar(c2))
                break label282;
              j = i3;
            }
        while (true)
        {
          label195: ++j;
          break label41:
          i1 = 0;
          break label113:
          label207: i2 = 2;
          break label121:
          label213: i4 = 10;
          break label137:
          if (isValidInt(str6 + c3, i4))
          {
            label220: str6 = str6 + c3;
            ++i3;
          }
          --i3;
          break label163:
          if (!isReservedXmlChar(c2))
          {
            label282: if (bool2);
            for (String str7 = String.valueOf(c2); ; str7 = "&#" + str6 + ";")
            {
              localStringBuilder.append(str7);
              j = i3;
              break label195:
            }
          }
          j = i3;
          localStringBuilder.append("&#" + str6 + ";");
          continue;
          label385: localStringBuilder.append("&amp;");
          continue;
          if (bool3)
          {
            String str4 = paramString.substring(j, j + Math.min(2 + SpecialEntity.getMaxEntityLength(), i - j));
            int l = str4.indexOf(';');
            if (l > 0)
            {
              String str5 = str4.substring(1, l);
              SpecialEntity localSpecialEntity = SpecialEntity.getEntity(str5);
              if (localSpecialEntity != null)
              {
                if (paramCleanerProperties.isTransSpecialEntitiesToNCR());
                for (Object localObject3 = localSpecialEntity.getDecimalNCR(); ; localObject3 = Character.valueOf(localSpecialEntity.getCharacter()))
                {
                  localStringBuilder.append(localObject3);
                  j += 1 + str5.length();
                  break label195:
                }
              }
            }
          }
          if (bool1)
          {
            String str1 = paramString.substring(j);
            Iterator localIterator = RESERVED_XML_CHARS.entrySet().iterator();
            int k;
            Map.Entry localEntry;
            String str3;
            do
            {
              boolean bool4 = localIterator.hasNext();
              k = 0;
              if (!bool4)
                break label628;
              localEntry = (Map.Entry)localIterator.next();
              str3 = (String)localEntry.getValue();
            }
            while (!str1.startsWith(str3));
            Object localObject2;
            if (paramBoolean)
            {
              localObject2 = (Serializable)localEntry.getKey();
              label605: localStringBuilder.append(localObject2);
              j += -1 + str3.length();
              k = 1;
              label628: if (k != 0)
                continue;
              if (!paramBoolean)
                break label707;
            }
            for (String str2 = "&"; ; str2 = (String)RESERVED_XML_CHARS.get(Character.valueOf('&')))
              while (true)
              {
                localStringBuilder.append(str2);
                break label195:
                if (paramCleanerProperties.transResCharsToNCR)
                  localObject2 = "&#" + ((Character)localEntry.getKey()).charValue() + ";";
                localObject2 = str3;
                break label605:
                label707: if (!paramCleanerProperties.transResCharsToNCR)
                  break;
                str2 = "&#38;";
              }
          }
          localStringBuilder.append("&amp;");
          continue;
          if (isReservedXmlChar(c1))
          {
            if (paramCleanerProperties.transResCharsToNCR);
            for (Object localObject1 = "&#" + c1 + ";"; ; localObject1 = (Serializable)RESERVED_XML_CHARS.get(Character.valueOf(c1)))
              while (true)
              {
                localStringBuilder.append(localObject1);
                break label195:
                if (!paramBoolean)
                  break;
                localObject1 = Character.valueOf(c1);
              }
          }
          localStringBuilder.append(c1);
        }
      }
      return localStringBuilder.toString();
    }
    return (String)(String)(String)null;
  }

  public static String evaluateTemplate(String paramString, Map paramMap)
  {
    if (paramString == null)
      return paramString;
    StringBuilder localStringBuilder = new StringBuilder();
    int i = paramString.indexOf(VAR_START);
    int j = -1;
    if ((i >= 0) && (i < paramString.length()))
    {
      label25: localStringBuilder.append(paramString.substring(j + 1, i));
      j = paramString.indexOf(VAR_END, i);
      Object localObject;
      if (j > i)
      {
        String str1 = paramString.substring(i + VAR_START.length(), j);
        if (paramMap == null)
          break label142;
        localObject = paramMap.get(str1.toLowerCase());
        label100: if (localObject != null)
          break label149;
      }
      for (String str2 = ""; ; str2 = localObject.toString())
      {
        localStringBuilder.append(str2);
        i = paramString.indexOf(VAR_START, Math.max(j + VAR_END.length(), i + 1));
        break label25:
        label142: localObject = "";
        label149: break label100:
      }
    }
    localStringBuilder.append(paramString.substring(j + 1));
    return (String)localStringBuilder.toString();
  }

  public static String fullUrl(String paramString1, String paramString2)
  {
    if (isFullUrl(paramString2))
      return paramString2;
    if ((paramString2 != null) && (paramString2.startsWith("?")))
    {
      int j = paramString1.indexOf('?');
      int k = paramString1.length();
      if (j < 0)
        return paramString1 + paramString2;
      if (j == k - 1)
        return paramString1.substring(0, k - 1) + paramString2;
      return paramString1 + "&" + paramString2.substring(1);
    }
    boolean bool = paramString2.startsWith("/");
    if (!isFullUrl(paramString1))
      paramString1 = "http://" + paramString1;
    int i;
    label171: String str1;
    if (bool)
    {
      i = paramString1.indexOf("/", 8);
      if (i > 8)
        break label240;
      str1 = paramString1 + "/";
      label198: if (!bool)
        break label253;
    }
    for (String str2 = str1 + paramString2.substring(1); ; str2 = str1 + paramString2)
    {
      return str2;
      i = paramString1.lastIndexOf("/");
      break label171:
      label240: str1 = paramString1.substring(0, i + 1);
      label253: break label198:
    }
  }

  public static String getCharsetFromContent(URL paramURL)
    throws IOException
  {
    InputStream localInputStream = paramURL.openStream();
    byte[] arrayOfByte = new byte[2048];
    if (localInputStream.read(arrayOfByte) > 0)
    {
      String str1 = new String(arrayOfByte);
      Matcher localMatcher = Pattern.compile("\\<meta\\s*http-equiv=[\\\"\\']content-type[\\\"\\']\\s*content\\s*=\\s*[\"']text/html\\s*;\\s*charset=([a-z\\d\\-]*)[\\\"\\'\\>]", 2).matcher(str1);
      if (localMatcher.find())
      {
        String str2 = localMatcher.group(1);
        if (Charset.isSupported(str2))
          return str2;
      }
    }
    return null;
  }

  public static String getCharsetFromContentTypeString(String paramString)
  {
    if (paramString != null)
    {
      Matcher localMatcher = Pattern.compile("charset=([a-z\\d\\-]*)", 2).matcher(paramString);
      if (localMatcher.find())
      {
        String str = localMatcher.group(1);
        if (Charset.isSupported(str))
          return str;
      }
    }
    return null;
  }

  public static String getXmlNSPrefix(String paramString)
  {
    int i = paramString.indexOf(':');
    if (i > 0)
      return paramString.substring(0, i);
    return null;
  }

  public static String getXmlName(String paramString)
  {
    int i = paramString.indexOf(':');
    if ((i > 0) && (i < -1 + paramString.length()))
      paramString = paramString.substring(i + 1);
    return paramString;
  }

  public static boolean isEmptyString(Object paramObject)
  {
    return (paramObject == null) || ("".equals(paramObject.toString().trim()));
  }

  public static boolean isFullUrl(String paramString)
  {
    if (paramString == null);
    String str;
    do
    {
      return false;
      str = paramString.trim().toLowerCase();
    }
    while ((!str.startsWith("http://")) && (!str.startsWith("https://")) && (!str.startsWith("file://")));
    return true;
  }

  public static boolean isHexadecimalDigit(char paramChar)
  {
    return (Character.isDigit(paramChar)) || (paramChar == 'A') || (paramChar == 'a') || (paramChar == 'B') || (paramChar == 'b') || (paramChar == 'C') || (paramChar == 'c') || (paramChar == 'D') || (paramChar == 'd') || (paramChar == 'E') || (paramChar == 'e') || (paramChar == 'F') || (paramChar == 'f');
  }

  public static boolean isIdentifierHelperChar(char paramChar)
  {
    return (':' == paramChar) || ('.' == paramChar) || ('-' == paramChar) || ('_' == paramChar);
  }

  public static boolean isReservedXmlChar(char paramChar)
  {
    return RESERVED_XML_CHARS.containsKey(Character.valueOf(paramChar));
  }

  public static boolean isValidInt(String paramString, int paramInt)
  {
    try
    {
      Integer.parseInt(paramString, paramInt);
      return true;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return false;
  }

  public static boolean isValidXmlChar(char paramChar)
  {
    return ((paramChar >= ' ') && (paramChar <= 55295)) || (paramChar == '\t') || (paramChar == '\n') || (paramChar == '\r') || ((paramChar >= 57344) && (paramChar <= 65533)) || ((paramChar >= 65536) && (paramChar <= 1114111));
  }

  public static boolean isValidXmlIdentifier(String paramString)
  {
    int i;
    if (paramString != null)
    {
      i = paramString.length();
      if (i != 0)
        break label15;
    }
    return false;
    for (int j = 0; j < i; ++j)
    {
      label15: char c = paramString.charAt(j);
      if (((j == 0) && (!Character.isUnicodeIdentifierStart(c)) && (c != '_')) || ((!Character.isUnicodeIdentifierStart(c)) && (!Character.isDigit(c)) && (!isIdentifierHelperChar(c))));
    }
    return true;
  }

  public static boolean isWhitespaceString(Object paramObject)
  {
    int i = 0;
    if (paramObject != null)
    {
      String str = paramObject.toString();
      i = 0;
      if (str != null)
      {
        boolean bool = "".equals(str.trim());
        i = 0;
        if (bool)
          i = 1;
      }
    }
    return i;
  }

  public static String ltrim(String paramString)
  {
    if (paramString == null)
      return null;
    int i = 0;
    int j = paramString.length();
    while ((i < j) && (Character.isWhitespace(paramString.charAt(i))))
      ++i;
    if (i >= j)
      return "";
    return paramString.substring(i);
  }

  public static String rtrim(String paramString)
  {
    if (paramString == null)
      return null;
    for (int i = paramString.length(); (i > 0) && (Character.isWhitespace(paramString.charAt(i - 1))); --i);
    if (i <= 0)
      return "";
    return paramString.substring(0, i);
  }

  public static String[] tokenize(String paramString1, String paramString2)
  {
    if (paramString1 == null)
    {
      arrayOfString = new String[0];
      return arrayOfString;
    }
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString1, paramString2);
    String[] arrayOfString = new String[localStringTokenizer.countTokens()];
    int j;
    for (int i = 0; ; i = j)
    {
      if (localStringTokenizer.hasMoreTokens());
      j = i + 1;
      arrayOfString[i] = localStringTokenizer.nextToken();
    }
  }

  public static void updateTagTransformations(CleanerTransformations paramCleanerTransformations, String paramString1, String paramString2)
  {
    boolean bool;
    if (paramString1.indexOf('.') <= 0)
    {
      bool = true;
      String str = null;
      if (paramString2 != null)
      {
        String[] arrayOfString2 = tokenize(paramString2, ",;");
        int i = arrayOfString2.length;
        str = null;
        if (i > 0)
          str = arrayOfString2[0];
        if (arrayOfString2.length > 1)
        {
          if ((!"true".equalsIgnoreCase(arrayOfString2[1])) && (!"yes".equalsIgnoreCase(arrayOfString2[1])) && (!"1".equals(arrayOfString2[1])))
            break label113;
          bool = true;
        }
      }
      label96: paramCleanerTransformations.addTransformation(new TagTransformation(paramString1, str, bool));
    }
    label113: String[] arrayOfString1;
    TagTransformation localTagTransformation;
    do
    {
      return;
      bool = false;
      break label96:
      arrayOfString1 = tokenize(paramString1, ".");
      localTagTransformation = paramCleanerTransformations.getTransformation(arrayOfString1[0]);
    }
    while (localTagTransformation == null);
    localTagTransformation.addAttributeTransformation(arrayOfString1[1], paramString2);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.Utils
 * JD-Core Version:    0.5.4
 */