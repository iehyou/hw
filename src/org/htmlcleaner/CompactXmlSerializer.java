package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.ListIterator;

public class CompactXmlSerializer extends XmlSerializer
{
  public CompactXmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializeOpenTag(paramTagNode, paramWriter, false);
    List localList = paramTagNode.getChildren();
    if (isMinimizedTagSyntax(paramTagNode))
      return;
    ListIterator localListIterator = localList.listIterator();
    while (localListIterator.hasNext())
    {
      label28: Object localObject = localListIterator.next();
      if (localObject instanceof ContentNode)
      {
        String str1 = localObject.toString().trim();
        if (dontEscape(paramTagNode));
        for (String str2 = str1.replaceAll("]]>", "]]&gt;"); ; str2 = escapeXml(str1))
        {
          paramWriter.write(str2);
          if (localListIterator.hasNext());
          if (!Utils.isWhitespaceString(localListIterator.next()))
            paramWriter.write("\n");
          localListIterator.previous();
          break label28:
        }
      }
      if (localObject instanceof CommentNode)
        paramWriter.write(((CommentNode)localObject).getCommentedContent().trim());
      if (!localObject instanceof BaseToken)
        continue;
      ((BaseToken)localObject).serialize(this, paramWriter);
    }
    serializeEndTag(paramTagNode, paramWriter, false);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CompactXmlSerializer
 * JD-Core Version:    0.5.4
 */