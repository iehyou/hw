package org.htmlcleaner;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public abstract class XmlSerializer extends Serializer
{
  protected XmlSerializer(CleanerProperties paramCleanerProperties)
  {
    super(paramCleanerProperties);
  }

  protected boolean dontEscape(TagNode paramTagNode)
  {
    return (this.props.isUseCdataForScriptAndStyle()) && (isScriptOrStyle(paramTagNode));
  }

  protected String escapeXml(String paramString)
  {
    return Utils.escapeXml(paramString, this.props, false);
  }

  @Deprecated
  public String getXmlAsString(TagNode paramTagNode)
    throws IOException
  {
    return super.getAsString(paramTagNode);
  }

  @Deprecated
  public String getXmlAsString(TagNode paramTagNode, String paramString)
    throws IOException
  {
    return super.getAsString(paramTagNode, paramString);
  }

  protected boolean isMinimizedTagSyntax(TagNode paramTagNode)
  {
    TagInfo localTagInfo = this.props.getTagInfoProvider().getTagInfo(paramTagNode.getName());
    return (paramTagNode.getChildren().size() == 0) && (((this.props.isUseEmptyElementTags()) || ((localTagInfo != null) && (localTagInfo.isEmptyTag()))));
  }

  protected void serializeEndTag(TagNode paramTagNode, Writer paramWriter, boolean paramBoolean)
    throws IOException
  {
    String str = paramTagNode.getName();
    if (Utils.isEmptyString(str));
    do
    {
      return;
      if (dontEscape(paramTagNode))
        paramWriter.write("]]>");
      if ((Utils.getXmlNSPrefix(str) != null) && (!this.props.isNamespacesAware()))
        str = Utils.getXmlName(str);
      paramWriter.write("</" + str + ">");
    }
    while (!paramBoolean);
    paramWriter.write("\n");
  }

  protected void serializeOpenTag(TagNode paramTagNode, Writer paramWriter, boolean paramBoolean)
    throws IOException
  {
    String str1 = paramTagNode.getName();
    if (Utils.isEmptyString(str1));
    do
    {
      return;
      boolean bool1 = this.props.isNamespacesAware();
      String str2 = Utils.getXmlNSPrefix(str1);
      TreeSet localTreeSet = null;
      HashSet localHashSet = null;
      if (str2 != null)
      {
        if (!bool1)
          break label301;
        localHashSet = new HashSet();
        paramTagNode.collectNamespacePrefixesOnPath(localHashSet);
        boolean bool2 = localHashSet.contains(str2);
        localTreeSet = null;
        if (!bool2)
        {
          localTreeSet = new TreeSet();
          localTreeSet.add(str2);
        }
      }
      label100: paramWriter.write("<" + str1);
      Iterator localIterator1 = paramTagNode.getAttributes().entrySet().iterator();
      if (localIterator1.hasNext())
      {
        label140: Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
        String str6 = (String)localEntry2.getKey();
        String str7 = Utils.getXmlNSPrefix(str6);
        if (str7 != null)
        {
          if (!bool1)
            break label317;
          if (localHashSet == null)
          {
            localHashSet = new HashSet();
            paramTagNode.collectNamespacePrefixesOnPath(localHashSet);
          }
          if (!localHashSet.contains(str7))
          {
            if (localTreeSet == null)
              localTreeSet = new TreeSet();
            localTreeSet.add(str7);
          }
        }
        while (true)
        {
          paramWriter.write(" " + str6 + "=\"" + escapeXml((String)localEntry2.getValue()) + "\"");
          break label140:
          label301: str1 = Utils.getXmlName(str1);
          localTreeSet = null;
          localHashSet = null;
          break label100:
          label317: str6 = Utils.getXmlName(str6);
        }
      }
      if (bool1)
      {
        Map localMap = paramTagNode.getNamespaceDeclarations();
        if (localMap != null)
        {
          Iterator localIterator3 = localMap.entrySet().iterator();
          while (localIterator3.hasNext())
          {
            Map.Entry localEntry1 = (Map.Entry)localIterator3.next();
            String str4 = (String)localEntry1.getKey();
            String str5 = "xmlns";
            if (str4.length() > 0)
              str5 = str5 + ":" + str4;
            paramWriter.write(" " + str5 + "=\"" + escapeXml((String)localEntry1.getValue()) + "\"");
          }
        }
      }
      if (localTreeSet != null)
      {
        Iterator localIterator2 = localTreeSet.iterator();
        while (localIterator2.hasNext())
        {
          String str3 = (String)localIterator2.next();
          paramWriter.write(" xmlns:" + str3 + "=\"" + str3 + "\"");
        }
      }
      if (!isMinimizedTagSyntax(paramTagNode))
        break label587;
      paramWriter.write(" />");
    }
    while (!paramBoolean);
    paramWriter.write("\n");
    return;
    if (dontEscape(paramTagNode))
    {
      label587: paramWriter.write("><![CDATA[");
      return;
    }
    paramWriter.write(">");
  }

  @Deprecated
  public void writeXml(TagNode paramTagNode, Writer paramWriter, String paramString)
    throws IOException
  {
    super.write(paramTagNode, paramWriter, paramString);
  }

  @Deprecated
  public void writeXmlToFile(TagNode paramTagNode, String paramString)
    throws IOException
  {
    super.writeToFile(paramTagNode, paramString);
  }

  @Deprecated
  public void writeXmlToFile(TagNode paramTagNode, String paramString1, String paramString2)
    throws IOException
  {
    super.writeToFile(paramTagNode, paramString1, paramString2);
  }

  @Deprecated
  public void writeXmlToStream(TagNode paramTagNode, OutputStream paramOutputStream)
    throws IOException
  {
    super.writeToStream(paramTagNode, paramOutputStream);
  }

  @Deprecated
  public void writeXmlToStream(TagNode paramTagNode, OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    super.writeToStream(paramTagNode, paramOutputStream, paramString);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.XmlSerializer
 * JD-Core Version:    0.5.4
 */