package org.htmlcleaner;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public abstract class Serializer
{
  protected CleanerProperties props;

  protected Serializer(CleanerProperties paramCleanerProperties)
  {
    this.props = paramCleanerProperties;
  }

  public String getAsString(TagNode paramTagNode)
    throws IOException
  {
    return getAsString(paramTagNode, false);
  }

  public String getAsString(TagNode paramTagNode, String paramString)
    throws IOException
  {
    return getAsString(paramTagNode, paramString, false);
  }

  public String getAsString(TagNode paramTagNode, String paramString, boolean paramBoolean)
    throws IOException
  {
    StringWriter localStringWriter = new StringWriter();
    write(paramTagNode, localStringWriter, paramString, paramBoolean);
    return localStringWriter.getBuffer().toString();
  }

  public String getAsString(TagNode paramTagNode, boolean paramBoolean)
    throws IOException
  {
    return getAsString(paramTagNode, HtmlCleaner.DEFAULT_CHARSET, paramBoolean);
  }

  protected boolean isScriptOrStyle(TagNode paramTagNode)
  {
    String str = paramTagNode.getName();
    return ("script".equalsIgnoreCase(str)) || ("style".equalsIgnoreCase(str));
  }

  protected abstract void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException;

  public void write(TagNode paramTagNode, Writer paramWriter, String paramString)
    throws IOException
  {
    write(paramTagNode, paramWriter, paramString, false);
  }

  public void write(TagNode paramTagNode, Writer paramWriter, String paramString, boolean paramBoolean)
    throws IOException
  {
    if (paramBoolean)
      paramTagNode = new HeadlessTagNode(paramTagNode, null);
    BufferedWriter localBufferedWriter = new BufferedWriter(paramWriter);
    if (!this.props.isOmitXmlDeclaration())
    {
      String str1 = "<?xml version=\"1.0\"";
      if (paramString != null)
        str1 = str1 + " encoding=\"" + paramString + "\"";
      String str2 = str1 + "?>";
      localBufferedWriter.write(str2 + "\n");
    }
    if (!this.props.isOmitDoctypeDeclaration())
    {
      DoctypeToken localDoctypeToken = paramTagNode.getDocType();
      if (localDoctypeToken != null)
        localDoctypeToken.serialize(this, localBufferedWriter);
    }
    serialize(paramTagNode, localBufferedWriter);
    localBufferedWriter.flush();
    localBufferedWriter.close();
  }

  public void writeToFile(TagNode paramTagNode, String paramString)
    throws IOException
  {
    writeToFile(paramTagNode, paramString, false);
  }

  public void writeToFile(TagNode paramTagNode, String paramString1, String paramString2)
    throws IOException
  {
    writeToFile(paramTagNode, paramString1, paramString2, false);
  }

  public void writeToFile(TagNode paramTagNode, String paramString1, String paramString2, boolean paramBoolean)
    throws IOException
  {
    writeToStream(paramTagNode, new FileOutputStream(paramString1), paramString2, paramBoolean);
  }

  public void writeToFile(TagNode paramTagNode, String paramString, boolean paramBoolean)
    throws IOException
  {
    writeToFile(paramTagNode, paramString, HtmlCleaner.DEFAULT_CHARSET, paramBoolean);
  }

  public void writeToStream(TagNode paramTagNode, OutputStream paramOutputStream)
    throws IOException
  {
    writeToStream(paramTagNode, paramOutputStream, false);
  }

  public void writeToStream(TagNode paramTagNode, OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    writeToStream(paramTagNode, paramOutputStream, paramString, false);
  }

  public void writeToStream(TagNode paramTagNode, OutputStream paramOutputStream, String paramString, boolean paramBoolean)
    throws IOException
  {
    write(paramTagNode, new OutputStreamWriter(paramOutputStream, paramString), paramString, paramBoolean);
  }

  public void writeToStream(TagNode paramTagNode, OutputStream paramOutputStream, boolean paramBoolean)
    throws IOException
  {
    writeToStream(paramTagNode, paramOutputStream, HtmlCleaner.DEFAULT_CHARSET, paramBoolean);
  }

  private class HeadlessTagNode extends TagNode
  {
    private HeadlessTagNode(TagNode arg2)
    {
      super("");
      Object localObject;
      getAttributes().putAll(localObject.getAttributes());
      getChildren().addAll(localObject.getChildren());
      setDocType(localObject.getDocType());
      Map localMap1 = getNamespaceDeclarations();
      if (localMap1 == null)
        return;
      Map localMap2 = localObject.getNamespaceDeclarations();
      if (localMap2 == null)
        return;
      localMap1.putAll(localMap2);
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.Serializer
 * JD-Core Version:    0.5.4
 */