package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public class ContentNode
  implements BaseToken, HtmlNode
{
  private StringBuilder content;

  public ContentNode(String paramString)
  {
    this.content = new StringBuilder(paramString);
  }

  ContentNode(char[] paramArrayOfChar, int paramInt)
  {
    this.content = new StringBuilder(paramInt + 16);
    this.content.append(paramArrayOfChar, 0, paramInt);
  }

  public StringBuilder getContent()
  {
    return this.content;
  }

  public void serialize(Serializer paramSerializer, Writer paramWriter)
    throws IOException
  {
    paramWriter.write(this.content.toString());
  }

  public String toString()
  {
    return this.content.toString();
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.ContentNode
 * JD-Core Version:    0.5.4
 */