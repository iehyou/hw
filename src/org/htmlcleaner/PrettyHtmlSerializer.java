package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class PrettyHtmlSerializer extends HtmlSerializer
{
  private static final String DEFAULT_INDENTATION_STRING = "\t";
  private String indentString = "\t";
  private List<String> indents = new ArrayList();

  public PrettyHtmlSerializer(CleanerProperties paramCleanerProperties)
  {
    this(paramCleanerProperties, "\t");
  }

  public PrettyHtmlSerializer(CleanerProperties paramCleanerProperties, String paramString)
  {
    super(paramCleanerProperties);
    this.indentString = paramString;
  }

  private String getIndent(int paramInt)
  {
    monitorenter;
    while (true)
    {
      int i;
      Object localObject2;
      try
      {
        i = this.indents.size();
        if (i <= paramInt)
          if (i == 0)
            localObject2 = null;
          else
            while (true)
            {
              this.indents.add(localObject3);
              localObject2 = localObject3;
              ++j;
              break label124:
              localObject2 = (String)this.indents.get(i - 1);
              break label121:
              localObject3 = (String)localObject2 + this.indentString;
            }
        String str = (String)this.indents.get(paramInt);
        return str;
      }
      finally
      {
        monitorexit;
      }
      label121: int j = i;
      label124: if (j > paramInt)
        continue;
      if (localObject2 != null)
        continue;
      Object localObject3 = "";
    }
  }

  private String getIndentedText(String paramString, int paramInt)
  {
    String str1 = getIndent(paramInt);
    StringBuilder localStringBuilder = new StringBuilder(paramString.length());
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "\n\r");
    while (localStringTokenizer.hasMoreTokens())
    {
      String str2 = localStringTokenizer.nextToken().trim();
      if ("".equals(str2))
        continue;
      localStringBuilder.append(str1).append(str2).append("\n");
    }
    return localStringBuilder.toString();
  }

  private String getSingleLineOfChildren(List paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    for (int i = 1; localIterator.hasNext(); i = 0)
    {
      Object localObject = localIterator.next();
      if (!localObject instanceof ContentNode);
      String str;
      do
      {
        return null;
        str = localObject.toString();
        if (i != 0)
          str = Utils.ltrim(str);
        if (localIterator.hasNext())
          continue;
        str = Utils.rtrim(str);
      }
      while ((str.indexOf("\n") >= 0) || (str.indexOf("\r") >= 0));
      localStringBuilder.append(str);
    }
    return localStringBuilder.toString();
  }

  protected void serialize(TagNode paramTagNode, Writer paramWriter)
    throws IOException
  {
    serializePrettyHtml(paramTagNode, paramWriter, 0, false, true);
  }

  protected void serializePrettyHtml(TagNode paramTagNode, Writer paramWriter, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    throws IOException
  {
    List localList = paramTagNode.getChildren();
    String str1 = paramTagNode.getName();
    boolean bool1 = Utils.isEmptyString(str1);
    String str2;
    label28: boolean bool2;
    label75: boolean bool3;
    String str3;
    boolean bool4;
    if (bool1)
    {
      str2 = "";
      if (!paramBoolean1)
      {
        if (!paramBoolean2)
          paramWriter.write("\n");
        paramWriter.write(str2);
      }
      serializeOpenTag(paramTagNode, paramWriter, true);
      if ((!paramBoolean1) && (!"pre".equalsIgnoreCase(str1)))
        break label178;
      bool2 = true;
      bool3 = false;
      if (!isMinimizedTagSyntax(paramTagNode))
      {
        str3 = getSingleLineOfChildren(localList);
        bool4 = dontEscape(paramTagNode);
        if ((bool2) || (str3 == null))
          break label191;
        if (dontEscape(paramTagNode))
          break label184;
      }
    }
    for (String str7 = escapeText(str3); ; str7 = str3)
    {
      paramWriter.write(str7);
      if ((str3 == null) && (!bool2))
      {
        if (!bool3)
          paramWriter.write("\n");
        paramWriter.write(str2);
      }
      serializeEndTag(paramTagNode, paramWriter, false);
      return;
      str2 = getIndent(paramInt);
      break label28:
      label178: bool2 = false;
      label184: break label75:
    }
    label191: Iterator localIterator = localList.iterator();
    label200: String str4;
    while (true)
    {
      if (localIterator.hasNext());
      Object localObject = localIterator.next();
      if (localObject instanceof TagNode)
      {
        TagNode localTagNode = (TagNode)localObject;
        if (bool1);
        for (int k = paramInt; ; k = paramInt + 1)
        {
          serializePrettyHtml(localTagNode, paramWriter, k, bool2, bool3);
          bool3 = false;
          break label200:
        }
      }
      if (localObject instanceof ContentNode)
      {
        if (bool4);
        for (String str5 = localObject.toString(); ; str5 = escapeText(localObject.toString()))
        {
          if (str5.length() > 0);
          if ((!bool4) && (!bool2))
            break;
          paramWriter.write(str5);
          break label200:
        }
        if (Character.isWhitespace(str5.charAt(0)))
        {
          if (!bool3)
          {
            paramWriter.write("\n");
            bool3 = false;
          }
          if (str5.trim().length() > 0)
          {
            String str6 = Utils.rtrim(str5);
            if (bool1);
            for (int j = paramInt; ; j = paramInt + 1)
            {
              paramWriter.write(getIndentedText(str6, j));
              break label200:
            }
          }
          bool3 = true;
        }
        if (str5.trim().length() > 0)
          paramWriter.write(Utils.rtrim(str5));
        if (localIterator.hasNext())
          continue;
        paramWriter.write("\n");
        bool3 = true;
      }
      if (!localObject instanceof CommentNode)
        continue;
      if ((!bool3) && (!bool2))
      {
        paramWriter.write("\n");
        bool3 = false;
      }
      str4 = ((CommentNode)localObject).getCommentedContent();
      if (!bool4)
        break;
      label495: paramWriter.write(str4);
    }
    if (bool1);
    for (int i = paramInt; ; i = paramInt + 1)
    {
      str4 = getIndentedText(str4, i);
      break label495:
    }
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.PrettyHtmlSerializer
 * JD-Core Version:    0.5.4
 */