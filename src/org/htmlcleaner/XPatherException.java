package org.htmlcleaner;

public class XPatherException extends Exception
{
  public XPatherException()
  {
    this("Error in evaluating XPath expression!");
  }

  public XPatherException(String paramString)
  {
    super(paramString);
  }

  public XPatherException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public XPatherException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.XPatherException
 * JD-Core Version:    0.5.4
 */