package org.htmlcleaner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class XPather
{
  private String[] tokenArray;

  public XPather(String paramString)
  {
    StringTokenizer localStringTokenizer = new StringTokenizer(paramString, "/()[]\"'=<>", true);
    this.tokenArray = new String[localStringTokenizer.countTokens()];
    int j;
    for (int i = 0; localStringTokenizer.hasMoreTokens(); i = j)
    {
      String[] arrayOfString = this.tokenArray;
      j = i + 1;
      arrayOfString[i] = localStringTokenizer.nextToken();
    }
  }

  private Collection evaluateAgainst(Collection paramCollection1, int paramInt1, int paramInt2, boolean paramBoolean1, int paramInt3, int paramInt4, boolean paramBoolean2, Collection paramCollection2)
    throws XPatherException
  {
    if ((paramInt1 >= 0) && (paramInt2 < this.tokenArray.length) && (paramInt1 <= paramInt2))
    {
      if (!"".equals(this.tokenArray[paramInt1].trim()))
        break label61;
      int i7 = paramInt1 + 1;
      paramCollection1 = evaluateAgainst(paramCollection1, i7, paramInt2, paramBoolean1, paramInt3, paramInt4, paramBoolean2, paramCollection2);
    }
    return paramCollection1;
    if (isToken("(", paramInt1))
    {
      label61: int i4 = findClosingIndex(paramInt1, paramInt2);
      if (i4 > 0)
      {
        int i5 = paramInt1 + 1;
        int i6 = i4 - 1;
        return evaluateAgainst(evaluateAgainst(paramCollection1, i5, i6, false, paramInt3, paramInt4, paramBoolean2, paramCollection2), i4 + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
      }
      throwStandardException();
    }
    while (true)
    {
      throw new XPatherException();
      if (isToken("[", paramInt1))
      {
        int i1 = findClosingIndex(paramInt1, paramInt2);
        if ((i1 > 0) && (paramCollection1 instanceof Collection))
        {
          int i2 = paramInt1 + 1;
          int i3 = i1 - 1;
          return evaluateAgainst(filterByCondition(paramCollection1, i2, i3), i1 + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
        }
        throwStandardException();
      }
      if ((isToken("\"", paramInt1)) || (isToken("'", paramInt1)))
      {
        int i = findClosingIndex(paramInt1, paramInt2);
        if (i > paramInt1)
          return evaluateAgainst(singleton(flatten(paramInt1 + 1, i - 1)), i + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
        throwStandardException();
      }
      if ((((isToken("=", paramInt1)) || (isToken("<", paramInt1)) || (isToken(">", paramInt1)))) && (paramBoolean2))
      {
        Collection localCollection2;
        String str2;
        if ((isToken("=", paramInt1 + 1)) && (((isToken("<", paramInt1)) || (isToken(">", paramInt1)))))
        {
          localCollection2 = evaluateAgainst(paramCollection2, paramInt1 + 2, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
          str2 = this.tokenArray[paramInt1] + this.tokenArray[(paramInt1 + 1)];
        }
        Collection localCollection1;
        String str1;
        for (boolean bool2 = evaluateLogic(paramCollection1, localCollection2, str2); ; bool2 = evaluateLogic(paramCollection1, localCollection1, str1))
        {
          return singleton(new Boolean(bool2));
          localCollection1 = evaluateAgainst(paramCollection2, paramInt1 + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
          str1 = this.tokenArray[paramInt1];
        }
      }
      if (!isToken("/", paramInt1))
        break;
      boolean bool1 = isToken("/", paramInt1 + 1);
      if (bool1)
        ++paramInt1;
      if (paramInt1 < paramInt2)
      {
        int k = -1 + findClosingIndex(paramInt1, paramInt2);
        if (k <= paramInt1)
          k = paramInt2;
        int l = paramInt1 + 1;
        return evaluateAgainst(evaluateAgainst(paramCollection1, l, k, bool1, 1, paramInt4, paramBoolean2, paramCollection2), k + 1, paramInt2, false, 1, paramInt4, paramBoolean2, paramCollection2);
      }
      throwStandardException();
    }
    if (isFunctionCall(paramInt1, paramInt2))
    {
      int j = findClosingIndex(paramInt1 + 1, paramInt2);
      return evaluateAgainst(evaluateFunction(paramCollection1, paramInt1, paramInt2, paramInt3, paramInt4, paramBoolean2), j + 1, paramInt2, false, 1, paramInt4, paramBoolean2, paramCollection2);
    }
    if (isValidInteger(this.tokenArray[paramInt1]))
      return evaluateAgainst(singleton(new Integer(this.tokenArray[paramInt1])), paramInt1 + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
    if (isValidDouble(this.tokenArray[paramInt1]))
      return evaluateAgainst(singleton(new Double(this.tokenArray[paramInt1])), paramInt1 + 1, paramInt2, false, paramInt3, paramInt4, paramBoolean2, paramCollection2);
    return getElementsByName(paramCollection1, paramInt1, paramInt2, paramBoolean1, paramBoolean2);
  }

  private Collection evaluateFunction(Collection paramCollection, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean)
    throws XPatherException
  {
    String str = this.tokenArray[paramInt1].trim();
    ArrayList localArrayList = new ArrayList();
    int i = paramCollection.size();
    Iterator localIterator1 = paramCollection.iterator();
    int j = 0;
    while (true)
    {
      label39: if (!localIterator1.hasNext())
        break label389;
      Object localObject1 = localIterator1.next();
      ++j;
      if ("last".equals(str))
      {
        if (paramBoolean);
        for (int l = paramInt4; ; l = i)
        {
          localArrayList.add(new Integer(l));
          break label39:
        }
      }
      if ("position".equals(str))
      {
        if (paramBoolean);
        for (int k = paramInt3; ; k = j)
        {
          localArrayList.add(new Integer(k));
          break label39:
        }
      }
      if ("text".equals(str))
      {
        if (localObject1 instanceof TagNode)
          localArrayList.add(((TagNode)localObject1).getText());
        if (!localObject1 instanceof String)
          continue;
        localArrayList.add(localObject1.toString());
      }
      if (!"count".equals(str))
        break;
      localArrayList.add(new Integer(evaluateAgainst(paramCollection, paramInt1 + 2, paramInt2 - 1, false, paramInt3, 0, paramBoolean, null).size()));
    }
    if ("data".equals(str))
    {
      Iterator localIterator2 = evaluateAgainst(paramCollection, paramInt1 + 2, paramInt2 - 1, false, paramInt3, 0, paramBoolean, null).iterator();
      while (true)
      {
        if (localIterator2.hasNext());
        Object localObject2 = localIterator2.next();
        if (localObject2 instanceof TagNode)
          localArrayList.add(((TagNode)localObject2).getText());
        if (!localObject2 instanceof String)
          continue;
        localArrayList.add(localObject2.toString());
      }
    }
    throw new XPatherException("Unknown function " + str + "!");
    label389: return localArrayList;
  }

  private boolean evaluateLogic(Collection paramCollection1, Collection paramCollection2, String paramString)
  {
    int i = 1;
    if ((paramCollection1 == null) || (paramCollection1.size() == 0) || (paramCollection2 == null) || (paramCollection2.size() == 0))
      i = 0;
    while (true)
    {
      return i;
      Object localObject1 = paramCollection1.iterator().next();
      Object localObject2 = paramCollection2.iterator().next();
      if ((localObject1 instanceof Number) && (localObject2 instanceof Number))
      {
        double d1 = ((Number)localObject1).doubleValue();
        double d2 = ((Number)localObject2).doubleValue();
        if ("=".equals(paramString))
        {
          if (d1 != d2);
          return false;
        }
        if ("<".equals(paramString))
        {
          if (d1 >= d2);
          return false;
        }
        if (">".equals(paramString))
        {
          if (d1 <= d2);
          return false;
        }
        if ("<=".equals(paramString))
        {
          if (d1 > d2);
          return false;
        }
        if (!">=".equals(paramString))
          break;
        if (d1 < d2);
        return false;
      }
      int j = toText(localObject1).compareTo(toText(localObject2));
      if ("=".equals(paramString))
      {
        if (j != 0);
        return false;
      }
      if ("<".equals(paramString))
      {
        if (j >= 0);
        return false;
      }
      if (">".equals(paramString))
      {
        if (j <= 0);
        return false;
      }
      if ("<=".equals(paramString))
      {
        if (j > 0);
        return false;
      }
      if (!">=".equals(paramString))
        break;
      if (j < 0)
        return false;
    }
    return false;
  }

  private Collection filterByCondition(Collection paramCollection, int paramInt1, int paramInt2)
    throws XPatherException
  {
    ArrayList localArrayList1 = new ArrayList();
    Iterator localIterator = paramCollection.iterator();
    int i = 0;
    int j = paramCollection.size();
    while (localIterator.hasNext())
    {
      Object localObject1 = localIterator.next();
      ++i;
      ArrayList localArrayList2 = new ArrayList(evaluateAgainst(singleton(localObject1), paramInt1, paramInt2, false, i, j, true, singleton(localObject1)));
      if (localArrayList2.size() < 1)
        continue;
      Object localObject2 = localArrayList2.get(0);
      if (localObject2 instanceof Boolean)
      {
        if (!((Boolean)localObject2).booleanValue())
          continue;
        localArrayList1.add(localObject1);
      }
      if (localObject2 instanceof Integer)
      {
        if (((Integer)localObject2).intValue() != i)
          continue;
        localArrayList1.add(localObject1);
      }
      localArrayList1.add(localObject1);
    }
    return localArrayList1;
  }

  private int findClosingIndex(int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2)
    {
      String str = this.tokenArray[paramInt1];
      int i2;
      if ("\"".equals(str))
        for (i2 = paramInt1 + 1; ; ++i2)
        {
          if (i2 > paramInt2)
            break label452;
          if ("\"".equals(this.tokenArray[i2]))
            return i2;
        }
      if ("'".equals(str))
        for (i2 = paramInt1 + 1; ; ++i2)
        {
          if (i2 > paramInt2)
            break label452;
          if ("'".equals(this.tokenArray[i2]));
        }
      if (("(".equals(str)) || ("[".equals(str)) || ("/".equals(str)))
      {
        int i = 1;
        int j = 1;
        int k;
        label142: int l;
        label154: int i1;
        if ("(".equals(str))
        {
          k = 1;
          if (!"[".equals(str))
            break label237;
          l = 1;
          if (!"/".equals(str))
            break label243;
          i1 = 1;
          label166: i2 = paramInt1 + 1;
          label171: if (i2 > paramInt2)
            break label452;
          if (!"\"".equals(this.tokenArray[i2]))
            break label255;
          if (i != 0)
            break label249;
          i = 1;
        }
        while (true)
        {
          label200: if ((i == 0) || (j == 0) || (k != 0) || (l != 0) || (i1 != 0));
          ++i2;
          break label171:
          k = 0;
          break label142:
          label237: l = 0;
          break label154:
          label243: i1 = 0;
          break label166:
          label249: i = 0;
          continue;
          if ("'".equals(this.tokenArray[i2]))
          {
            label255: if (j == 0);
            for (j = 1; ; j = 0)
              break label200:
          }
          if (("(".equals(this.tokenArray[i2])) && (i != 0) && (j != 0))
            ++k;
          if ((")".equals(this.tokenArray[i2])) && (i != 0) && (j != 0))
            --k;
          if (("[".equals(this.tokenArray[i2])) && (i != 0) && (j != 0))
            ++l;
          if (("]".equals(this.tokenArray[i2])) && (i != 0) && (j != 0))
            --l;
          if ((!"/".equals(this.tokenArray[i2])) || (i == 0) || (j == 0) || (k != 0) || (l != 0))
            continue;
          --i1;
        }
      }
    }
    label452: return -1;
  }

  private String flatten(int paramInt1, int paramInt2)
  {
    if (paramInt1 <= paramInt2)
    {
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = paramInt1; i <= paramInt2; ++i)
        localStringBuffer.append(this.tokenArray[i]);
      return localStringBuffer.toString();
    }
    return "";
  }

  private Collection getElementsByName(Collection paramCollection, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    throws XPatherException
  {
    String str1 = this.tokenArray[paramInt1].trim();
    if (isAtt(str1))
    {
      String str2 = str1.substring(1);
      ArrayList localArrayList = new ArrayList();
      if (paramBoolean1)
      {
        localObject3 = new LinkedHashSet();
        Iterator localIterator3 = paramCollection.iterator();
        while (true)
        {
          if (!localIterator3.hasNext())
            break label113;
          Object localObject5 = localIterator3.next();
          if (!localObject5 instanceof TagNode)
            continue;
          List localList2 = ((TagNode)localObject5).getAllElementsList(true);
          ((Collection)localObject3).addAll(localList2);
        }
      }
      Object localObject3 = paramCollection;
      label113: Iterator localIterator4 = ((Collection)localObject3).iterator();
      while (localIterator4.hasNext())
      {
        Object localObject4 = localIterator4.next();
        if (localObject4 instanceof TagNode)
        {
          TagNode localTagNode4 = (TagNode)localObject4;
          if ("*".equals(str2))
            localArrayList.addAll(evaluateAgainst(localTagNode4.getAttributes().values(), paramInt1 + 1, paramInt2, false, 1, 1, paramBoolean2, null));
          String str3 = localTagNode4.getAttributeByName(str2);
          if (str3 == null)
            continue;
          localArrayList.addAll(evaluateAgainst(singleton(str3), paramInt1 + 1, paramInt2, false, 1, 1, paramBoolean2, null));
        }
        throwStandardException();
      }
      return localArrayList;
    }
    LinkedHashSet localLinkedHashSet1 = new LinkedHashSet();
    Iterator localIterator1 = paramCollection.iterator();
    int i = 0;
    while (localIterator1.hasNext())
    {
      Object localObject1 = localIterator1.next();
      if (localObject1 instanceof TagNode)
      {
        TagNode localTagNode1 = (TagNode)localObject1;
        ++i;
        boolean bool1 = ".".equals(str1);
        boolean bool2 = "..".equals(str1);
        boolean bool3 = "*".equals(str1);
        label355: Collection localCollection1;
        if (bool1)
        {
          localObject2 = singleton(localTagNode1);
          LinkedHashSet localLinkedHashSet2 = new LinkedHashSet((Collection)localObject2);
          localCollection1 = evaluateAgainst(localLinkedHashSet2, paramInt1 + 1, paramInt2, false, i, localLinkedHashSet2.size(), paramBoolean2, null);
          if (!paramBoolean1)
            break label593;
          List localList1 = localTagNode1.getChildTagList();
          if ((bool1) || (bool2) || (bool3))
            localLinkedHashSet1.addAll(localCollection1);
          Iterator localIterator2 = localList1.iterator();
          while (true)
          {
            if (localIterator2.hasNext());
            TagNode localTagNode2 = (TagNode)localIterator2.next();
            Collection localCollection2 = getElementsByName(singleton(localTagNode2), paramInt1, paramInt2, paramBoolean1, paramBoolean2);
            if ((!bool1) && (!bool2) && (!bool3) && (localCollection1.contains(localTagNode2)))
              localLinkedHashSet1.add(localTagNode2);
            localLinkedHashSet1.addAll(localCollection2);
          }
        }
        if (bool2)
        {
          TagNode localTagNode3 = localTagNode1.getParent();
          if (localTagNode3 != null);
          for (localObject2 = singleton(localTagNode3); ; localObject2 = new ArrayList())
            break label355:
        }
        if (bool3);
        for (Object localObject2 = localTagNode1.getChildTagList(); ; localObject2 = localTagNode1.getElementListByName(str1, false))
          break label355:
        label593: localLinkedHashSet1.addAll(localCollection1);
      }
      throwStandardException();
    }
    return (Collection)(Collection)localLinkedHashSet1;
  }

  private boolean isAtt(String paramString)
  {
    return (paramString != null) && (paramString.length() > 1) && (paramString.startsWith("@"));
  }

  private boolean isFunctionCall(int paramInt1, int paramInt2)
  {
    if ((!isIdentifier(this.tokenArray[paramInt1])) && (!isToken("(", paramInt1 + 1)));
    do
      return false;
    while (findClosingIndex(paramInt1 + 1, paramInt2) <= paramInt1 + 1);
    return true;
  }

  private boolean isIdentifier(String paramString)
  {
    if (paramString == null);
    String str;
    do
    {
      return false;
      str = paramString.trim();
    }
    while ((str.length() <= 0) || (!Character.isLetter(str.charAt(0))));
    for (int i = 1; ; ++i)
    {
      if (i < str.length());
      char c = str.charAt(i);
      if ((c != '_') && (c != '-') && (!Character.isLetterOrDigit(c)));
    }
  }

  private boolean isToken(String paramString, int paramInt)
  {
    int i = this.tokenArray.length;
    return (paramInt >= 0) && (paramInt < i) && (this.tokenArray[paramInt].trim().equals(paramString.trim()));
  }

  private boolean isValidDouble(String paramString)
  {
    try
    {
      Double.parseDouble(paramString);
      return true;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return false;
  }

  private boolean isValidInteger(String paramString)
  {
    try
    {
      Integer.parseInt(paramString);
      return true;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return false;
  }

  private Collection singleton(Object paramObject)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramObject);
    return localArrayList;
  }

  private void throwStandardException()
    throws XPatherException
  {
    throw new XPatherException();
  }

  private String toText(Object paramObject)
  {
    if (paramObject == null)
      return "";
    if (paramObject instanceof TagNode)
      return ((TagNode)paramObject).getText().toString();
    return paramObject.toString();
  }

  public Object[] evaluateAgainstNode(TagNode paramTagNode)
    throws XPatherException
  {
    if (paramTagNode == null)
      throw new XPatherException("Cannot evaluate XPath expression against null value!");
    Collection localCollection = evaluateAgainst(singleton(paramTagNode), 0, -1 + this.tokenArray.length, false, 1, 0, false, null);
    Object[] arrayOfObject = new Object[localCollection.size()];
    Iterator localIterator = localCollection.iterator();
    int j;
    for (int i = 0; localIterator.hasNext(); i = j)
    {
      j = i + 1;
      arrayOfObject[i] = localIterator.next();
    }
    return arrayOfObject;
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.XPather
 * JD-Core Version:    0.5.4
 */