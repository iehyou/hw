package org.htmlcleaner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class CommandLine
{
  private static String getArgValue(String[] paramArrayOfString, String paramString)
  {
    for (int i = 0; i < paramArrayOfString.length; ++i)
    {
      String str1 = paramArrayOfString[i];
      int j = str1.indexOf('=');
      if (j < 0)
        continue;
      String str2 = str1.substring(0, j).trim();
      String str3 = str1.substring(j + 1).trim();
      if (str2.toLowerCase().startsWith(paramString.toLowerCase()))
        return str3;
    }
    return "";
  }

  public static void main(String[] paramArrayOfString)
    throws IOException, XPatherException
  {
    String str1 = getArgValue(paramArrayOfString, "src");
    if ("".equals(str1))
    {
      System.err.println("Usage: java -jar htmlcleanerXX.jar src = <url | file> [incharset = <charset>] [dest = <file>] [outcharset = <charset>] [taginfofile=<file>] [options...]");
      System.err.println("");
      System.err.println("where options include:");
      System.err.println("    outputtype=simple* | compact | browser-compact | pretty | htmlsimple | htmlcompact | htmlpretty");
      System.err.println("    advancedxmlescape=true* | false");
      System.err.println("    transrescharstoncr=true | false*");
      System.err.println("    usecdata=true* | false");
      System.err.println("    specialentities=true* | false");
      System.err.println("    transspecialentitiestoncr=true | false*");
      System.err.println("    unicodechars=true* | false");
      System.err.println("    omitunknowntags=true | false*");
      System.err.println("    treatunknowntagsascontent=true | false*");
      System.err.println("    omitdeprtags=true | false*");
      System.err.println("    treatdeprtagsascontent=true | false*");
      System.err.println("    omitcomments=true | false*");
      System.err.println("    omitxmldecl=true | false*");
      System.err.println("    omitdoctypedecl=true* | false");
      System.err.println("    useemptyelementtags=true* | false");
      System.err.println("    allowmultiwordattributes=true* | false");
      System.err.println("    allowhtmlinsideattributes=true | false*");
      System.err.println("    ignoreqe=true* | false");
      System.err.println("    namespacesaware=true* | false");
      System.err.println("    hyphenreplacement=<string value> [=]");
      System.err.println("    prunetags=<string value> []");
      System.err.println("    booleanatts=self* | empty | true");
      System.err.println("    nodebyxpath=<xpath expression>");
      System.err.println("    omitenvelope=true | false*");
      System.err.println("    t:<sourcetagX>[=<desttag>[,<preserveatts>]]");
      System.err.println("    t:<sourcetagX>.<destattrY>[=<template>]");
      System.exit(1);
    }
    String str2 = getArgValue(paramArrayOfString, "incharset");
    if ("".equals(str2))
      str2 = HtmlCleaner.DEFAULT_CHARSET;
    String str3 = getArgValue(paramArrayOfString, "outcharset");
    if ("".equals(str3))
      str3 = HtmlCleaner.DEFAULT_CHARSET;
    String str4 = getArgValue(paramArrayOfString, "dest");
    String str5 = getArgValue(paramArrayOfString, "outputtype");
    String str6 = getArgValue(paramArrayOfString, "advancedxmlescape");
    String str7 = getArgValue(paramArrayOfString, "transrescharstoncr");
    String str8 = getArgValue(paramArrayOfString, "usecdata");
    String str9 = getArgValue(paramArrayOfString, "specialentities");
    String str10 = getArgValue(paramArrayOfString, "transspecialentitiestoncr");
    String str11 = getArgValue(paramArrayOfString, "unicodechars");
    String str12 = getArgValue(paramArrayOfString, "omitunknowntags");
    String str13 = getArgValue(paramArrayOfString, "treatunknowntagsascontent");
    String str14 = getArgValue(paramArrayOfString, "omitdeprtags");
    String str15 = getArgValue(paramArrayOfString, "treatdeprtagsascontent");
    String str16 = getArgValue(paramArrayOfString, "omitcomments");
    String str17 = getArgValue(paramArrayOfString, "omitxmldecl");
    String str18 = getArgValue(paramArrayOfString, "omitdoctypedecl");
    String str19 = getArgValue(paramArrayOfString, "omithtmlenvelope");
    String str20 = getArgValue(paramArrayOfString, "useemptyelementtags");
    String str21 = getArgValue(paramArrayOfString, "allowmultiwordattributes");
    String str22 = getArgValue(paramArrayOfString, "allowhtmlinsideattributes");
    String str23 = getArgValue(paramArrayOfString, "ignoreqe");
    String str24 = getArgValue(paramArrayOfString, "namespacesaware");
    String str25 = getArgValue(paramArrayOfString, "hyphenreplacement");
    String str26 = getArgValue(paramArrayOfString, "prunetags");
    String str27 = getArgValue(paramArrayOfString, "booleanatts");
    String str28 = getArgValue(paramArrayOfString, "nodebyxpath");
    boolean bool = toBoolean(getArgValue(paramArrayOfString, "omitenvelope"));
    String str29 = getArgValue(paramArrayOfString, "taginfofile");
    HtmlCleaner localHtmlCleaner;
    label550: CleanerProperties localCleanerProperties;
    TreeMap localTreeMap;
    int i;
    label1000: String str32;
    int k;
    String str33;
    if (!"".equals(str29))
    {
      File localFile1 = new File(str29);
      localHtmlCleaner = new HtmlCleaner(new ConfigFileTagProvider(localFile1));
      localCleanerProperties = localHtmlCleaner.getProperties();
      if (!"".equals(str12))
        localCleanerProperties.setOmitUnknownTags(toBoolean(str12));
      if (!"".equals(str13))
        localCleanerProperties.setTreatUnknownTagsAsContent(toBoolean(str13));
      if (!"".equals(str14))
        localCleanerProperties.setOmitDeprecatedTags(toBoolean(str14));
      if (!"".equals(str15))
        localCleanerProperties.setTreatDeprecatedTagsAsContent(toBoolean(str15));
      if (!"".equals(str6))
        localCleanerProperties.setAdvancedXmlEscape(toBoolean(str6));
      if (!"".equals(str7))
        localCleanerProperties.setTransResCharsToNCR(toBoolean(str7));
      if (!"".equals(str8))
        localCleanerProperties.setUseCdataForScriptAndStyle(toBoolean(str8));
      if (!"".equals(str9))
        localCleanerProperties.setTranslateSpecialEntities(toBoolean(str9));
      if (!"".equals(str10))
        localCleanerProperties.setTransSpecialEntitiesToNCR(toBoolean(str10));
      if (!"".equals(str11))
        localCleanerProperties.setRecognizeUnicodeChars(toBoolean(str11));
      if (!"".equals(str16))
        localCleanerProperties.setOmitComments(toBoolean(str16));
      if (!"".equals(str17))
        localCleanerProperties.setOmitXmlDeclaration(toBoolean(str17));
      if (!"".equals(str18))
        localCleanerProperties.setOmitDoctypeDeclaration(toBoolean(str18));
      if (!"".equals(str19))
        localCleanerProperties.setOmitHtmlEnvelope(toBoolean(str19));
      if (!"".equals(str20))
        localCleanerProperties.setUseEmptyElementTags(toBoolean(str20));
      if (!"".equals(str21))
        localCleanerProperties.setAllowMultiWordAttributes(toBoolean(str21));
      if (!"".equals(str22))
        localCleanerProperties.setAllowHtmlInsideAttributes(toBoolean(str22));
      if (!"".equals(str23))
        localCleanerProperties.setIgnoreQuestAndExclam(toBoolean(str23));
      if (!"".equals(str24))
        localCleanerProperties.setNamespacesAware(toBoolean(str24));
      if (!"".equals(str25))
        localCleanerProperties.setHyphenReplacementInComment(str25);
      if (!"".equals(str26))
        localCleanerProperties.setPruneTags(str26);
      if (!"".equals(str27))
        localCleanerProperties.setBooleanAttributeValues(str27);
      localTreeMap = new TreeMap();
      i = 0;
      if (i >= paramArrayOfString.length)
        break label1124;
      String str31 = paramArrayOfString[i];
      if ((str31.startsWith("t:")) && (str31.length() > 2))
      {
        str32 = str31.substring(2);
        k = str32.indexOf('=');
        if (k > 0)
          break label1097;
        str33 = str32;
        label1059: if (k > 0)
          break label1110;
      }
    }
    for (Object localObject2 = null; ; localObject2 = str32.substring(k + 1))
    {
      localTreeMap.put(str33, localObject2);
      ++i;
      break label1000:
      localHtmlCleaner = new HtmlCleaner();
      break label550:
      label1097: str33 = str32.substring(0, k);
      label1110: break label1059:
    }
    if (localTreeMap != null)
    {
      label1124: CleanerTransformations localCleanerTransformations = new CleanerTransformations();
      Iterator localIterator = localTreeMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        Utils.updateTagTransformations(localCleanerTransformations, (String)localEntry.getKey(), (String)localEntry.getValue());
      }
      localHtmlCleaner.setTransformations(localCleanerTransformations);
    }
    long l = System.currentTimeMillis();
    String str30 = str1.toLowerCase();
    TagNode localTagNode;
    label1262: int j;
    label1284: Object localObject1;
    if ((str30.startsWith("http://")) || (str30.startsWith("https://")))
    {
      URL localURL = new URL(str1);
      localTagNode = localHtmlCleaner.clean(localURL, str2);
      if (!"".equals(str28))
      {
        Object[] arrayOfObject = localTagNode.evaluateXPath(str28);
        j = 0;
        if (j < arrayOfObject.length)
        {
          if (!arrayOfObject[j] instanceof TagNode)
            break label1457;
          localTagNode = (TagNode)arrayOfObject[j];
          System.out.println("Node successfully found by XPath.");
        }
        if (j == arrayOfObject.length)
          System.out.println("Node not found by XPath expression - whole html tree is going to be serialized!");
      }
      if ((str4 != null) && (!"".equals(str4.trim())))
        break label1463;
      localObject1 = System.out;
      label1362: if (!"compact".equals(str5))
        break label1477;
      CompactXmlSerializer localCompactXmlSerializer = new CompactXmlSerializer(localCleanerProperties);
      localCompactXmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
    }
    while (true)
    {
      System.out.println("Finished successfully in " + (System.currentTimeMillis() - l) + "ms.");
      return;
      File localFile2 = new File(str1);
      localTagNode = localHtmlCleaner.clean(localFile2, str2);
      break label1262:
      label1457: ++j;
      break label1284:
      label1463: localObject1 = new FileOutputStream(str4);
      break label1362:
      if ("browser-compact".equals(str5))
      {
        label1477: BrowserCompactXmlSerializer localBrowserCompactXmlSerializer = new BrowserCompactXmlSerializer(localCleanerProperties);
        localBrowserCompactXmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
      }
      if ("pretty".equals(str5))
      {
        PrettyXmlSerializer localPrettyXmlSerializer = new PrettyXmlSerializer(localCleanerProperties);
        localPrettyXmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
      }
      if ("htmlsimple".equals(str5))
      {
        SimpleHtmlSerializer localSimpleHtmlSerializer = new SimpleHtmlSerializer(localCleanerProperties);
        localSimpleHtmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
      }
      if ("htmlcompact".equals(str5))
      {
        CompactHtmlSerializer localCompactHtmlSerializer = new CompactHtmlSerializer(localCleanerProperties);
        localCompactHtmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
      }
      if ("htmlpretty".equals(str5))
      {
        PrettyHtmlSerializer localPrettyHtmlSerializer = new PrettyHtmlSerializer(localCleanerProperties);
        localPrettyHtmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
      }
      SimpleXmlSerializer localSimpleXmlSerializer = new SimpleXmlSerializer(localCleanerProperties);
      localSimpleXmlSerializer.writeToStream(localTagNode, (OutputStream)localObject1, str3, bool);
    }
  }

  private static boolean toBoolean(String paramString)
  {
    return (paramString != null) && ((("on".equalsIgnoreCase(paramString)) || ("true".equalsIgnoreCase(paramString)) || ("yes".equalsIgnoreCase(paramString))));
  }
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.htmlcleaner.CommandLine
 * JD-Core Version:    0.5.4
 */