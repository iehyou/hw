package org.apache.http.entity.mime;

public abstract interface MultipartUploadListener
{
  public abstract void onUploaded(long paramLong);
}

/* Location:           D:\soft\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     org.apache.http.entity.mime.MultipartUploadListener
 * JD-Core Version:    0.5.4
 */