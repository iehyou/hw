.class public Lorg/opencv/utils/Converters;
.super Ljava/lang/Object;
.source "Converters.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 9
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    const/4 v7, 0x0

    .line 246
    if-nez p1, :cond_0

    .line 247
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "mats == null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 248
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v3

    .line 249
    .local v3, count:I
    sget v5, Lorg/opencv/core/CvType;->CV_32SC2:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    if-ne v5, v6, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_2

    .line 250
    :cond_1
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CvType.CV_32SC2 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 253
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 254
    mul-int/lit8 v5, v3, 0x2

    new-array v2, v5, [I

    .line 255
    .local v2, buff:[I
    invoke-virtual {p0, v7, v7, v2}, Lorg/opencv/core/Mat;->get(II[I)I

    .line 256
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    if-ge v4, v3, :cond_3

    .line 257
    mul-int/lit8 v5, v4, 0x2

    aget v5, v2, v5

    int-to-long v5, v5

    const/16 v7, 0x20

    shl-long/2addr v5, v7

    mul-int/lit8 v7, v4, 0x2

    add-int/lit8 v7, v7, 0x1

    aget v7, v2, v7

    int-to-long v7, v7

    or-long v0, v5, v7

    .line 258
    .local v0, addr:J
    new-instance v5, Lorg/opencv/core/Mat;

    invoke-direct {v5, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 260
    .end local v0           #addr:J
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 9
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v6, 0x0

    .line 150
    if-nez p1, :cond_0

    .line 151
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Output List can\'t be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 152
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 153
    .local v1, count:I
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v3

    .line 154
    .local v3, type:I
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    .line 155
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Input Mat should have one column\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 157
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 158
    sget v4, Lorg/opencv/core/CvType;->CV_32SC2:I

    if-ne v3, v4, :cond_2

    .line 159
    mul-int/lit8 v4, v1, 0x2

    new-array v0, v4, [I

    .line 160
    .local v0, buff:[I
    invoke-virtual {p0, v6, v6, v0}, Lorg/opencv/core/Mat;->get(II[I)I

    .line 161
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_5

    .line 162
    new-instance v4, Lorg/opencv/core/Point;

    mul-int/lit8 v5, v2, 0x2

    aget v5, v0, v5

    int-to-double v5, v5

    mul-int/lit8 v7, v2, 0x2

    add-int/lit8 v7, v7, 0x1

    aget v7, v0, v7

    int-to-double v7, v7

    invoke-direct {v4, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 164
    .end local v0           #buff:[I
    .end local v2           #i:I
    :cond_2
    sget v4, Lorg/opencv/core/CvType;->CV_32FC2:I

    if-ne v3, v4, :cond_3

    .line 165
    mul-int/lit8 v4, v1, 0x2

    new-array v0, v4, [F

    .line 166
    .local v0, buff:[F
    invoke-virtual {p0, v6, v6, v0}, Lorg/opencv/core/Mat;->get(II[F)I

    .line 167
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_1
    if-ge v2, v1, :cond_5

    .line 168
    new-instance v4, Lorg/opencv/core/Point;

    mul-int/lit8 v5, v2, 0x2

    aget v5, v0, v5

    float-to-double v5, v5

    mul-int/lit8 v7, v2, 0x2

    add-int/lit8 v7, v7, 0x1

    aget v7, v0, v7

    float-to-double v7, v7

    invoke-direct {v4, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 170
    .end local v0           #buff:[F
    .end local v2           #i:I
    :cond_3
    sget v4, Lorg/opencv/core/CvType;->CV_64FC2:I

    if-ne v3, v4, :cond_4

    .line 171
    mul-int/lit8 v4, v1, 0x2

    new-array v0, v4, [D

    .line 172
    .local v0, buff:[D
    invoke-virtual {p0, v6, v6, v0}, Lorg/opencv/core/Mat;->get(II[D)I

    .line 173
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_2
    if-ge v2, v1, :cond_5

    .line 174
    new-instance v4, Lorg/opencv/core/Point;

    mul-int/lit8 v5, v2, 0x2

    aget-wide v5, v0, v5

    mul-int/lit8 v7, v2, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-wide v7, v0, v7

    invoke-direct {v4, v5, v6, v7, v8}, Lorg/opencv/core/Point;-><init>(DD)V

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 177
    .end local v0           #buff:[D
    .end local v2           #i:I
    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Input Mat should be of CV_32SC2, CV_32FC2 or CV_64FC2 type\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 180
    .restart local v2       #i:I
    :cond_5
    return-void
.end method

.method public static Mat_to_vector_Point2d(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 147
    return-void
.end method

.method public static Mat_to_vector_Point2f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 143
    return-void
.end method

.method public static Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 11
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    const/4 v2, 0x0

    .line 195
    if-nez p1, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output List can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v8

    .line 198
    .local v8, count:I
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v10

    .line 199
    .local v10, type:I
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Input Mat should have one column\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 203
    sget v0, Lorg/opencv/core/CvType;->CV_32SC3:I

    if-ne v10, v0, :cond_2

    .line 204
    mul-int/lit8 v0, v8, 0x3

    new-array v7, v0, [I

    .line 205
    .local v7, buff:[I
    invoke-virtual {p0, v2, v2, v7}, Lorg/opencv/core/Mat;->get(II[I)I

    .line 206
    const/4 v9, 0x0

    .local v9, i:I
    :goto_0
    if-ge v9, v8, :cond_5

    .line 207
    new-instance v0, Lorg/opencv/core/Point3;

    mul-int/lit8 v1, v9, 0x3

    aget v1, v7, v1

    int-to-double v1, v1

    mul-int/lit8 v3, v9, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v3, v7, v3

    int-to-double v3, v3

    mul-int/lit8 v5, v9, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v5, v7, v5

    int-to-double v5, v5

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 209
    .end local v7           #buff:[I
    .end local v9           #i:I
    :cond_2
    sget v0, Lorg/opencv/core/CvType;->CV_32FC3:I

    if-ne v10, v0, :cond_3

    .line 210
    mul-int/lit8 v0, v8, 0x3

    new-array v7, v0, [F

    .line 211
    .local v7, buff:[F
    invoke-virtual {p0, v2, v2, v7}, Lorg/opencv/core/Mat;->get(II[F)I

    .line 212
    const/4 v9, 0x0

    .restart local v9       #i:I
    :goto_1
    if-ge v9, v8, :cond_5

    .line 213
    new-instance v0, Lorg/opencv/core/Point3;

    mul-int/lit8 v1, v9, 0x3

    aget v1, v7, v1

    float-to-double v1, v1

    mul-int/lit8 v3, v9, 0x3

    add-int/lit8 v3, v3, 0x1

    aget v3, v7, v3

    float-to-double v3, v3

    mul-int/lit8 v5, v9, 0x3

    add-int/lit8 v5, v5, 0x2

    aget v5, v7, v5

    float-to-double v5, v5

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 215
    .end local v7           #buff:[F
    .end local v9           #i:I
    :cond_3
    sget v0, Lorg/opencv/core/CvType;->CV_64FC3:I

    if-ne v10, v0, :cond_4

    .line 216
    mul-int/lit8 v0, v8, 0x3

    new-array v7, v0, [D

    .line 217
    .local v7, buff:[D
    invoke-virtual {p0, v2, v2, v7}, Lorg/opencv/core/Mat;->get(II[D)I

    .line 218
    const/4 v9, 0x0

    .restart local v9       #i:I
    :goto_2
    if-ge v9, v8, :cond_5

    .line 219
    new-instance v0, Lorg/opencv/core/Point3;

    mul-int/lit8 v1, v9, 0x3

    aget-wide v1, v7, v1

    mul-int/lit8 v3, v9, 0x3

    add-int/lit8 v3, v3, 0x1

    aget-wide v3, v7, v3

    mul-int/lit8 v5, v9, 0x3

    add-int/lit8 v5, v5, 0x2

    aget-wide v5, v7, v5

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 222
    .end local v7           #buff:[D
    .end local v9           #i:I
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Input Mat should be of CV_32SC3, CV_32FC3 or CV_64FC3 type\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    .restart local v9       #i:I
    :cond_5
    return-void
.end method

.method public static Mat_to_vector_Point3d(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 192
    return-void
.end method

.method public static Mat_to_vector_Point3f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 188
    return-void
.end method

.method public static Mat_to_vector_Point3i(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 0
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    invoke-static {p0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point3(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 184
    return-void
.end method

.method public static Mat_to_vector_Rect(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 8
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, rs:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Rect;>;"
    const/4 v5, 0x0

    .line 415
    if-nez p1, :cond_0

    .line 416
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "rs == null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 417
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 418
    .local v1, count:I
    sget v3, Lorg/opencv/core/CvType;->CV_32SC4:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 419
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CvType.CV_32SC4 != m.type() ||  m.rows()!=1\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 422
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 423
    mul-int/lit8 v3, v1, 0x4

    new-array v0, v3, [I

    .line 424
    .local v0, buff:[I
    invoke-virtual {p0, v5, v5, v0}, Lorg/opencv/core/Mat;->get(II[I)I

    .line 425
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 426
    new-instance v3, Lorg/opencv/core/Rect;

    mul-int/lit8 v4, v2, 0x4

    aget v4, v0, v4

    mul-int/lit8 v5, v2, 0x4

    add-int/lit8 v5, v5, 0x1

    aget v5, v0, v5

    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    aget v6, v0, v6

    mul-int/lit8 v7, v2, 0x4

    add-int/lit8 v7, v7, 0x3

    aget v7, v0, v7

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/opencv/core/Rect;-><init>(IIII)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 428
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, bs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v5, 0x0

    .line 379
    if-nez p1, :cond_0

    .line 380
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Output List can\'t be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 381
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 382
    .local v1, count:I
    sget v3, Lorg/opencv/core/CvType;->CV_8SC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 383
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CvType.CV_8SC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 386
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 387
    new-array v0, v1, [B

    .line 388
    .local v0, buff:[B
    invoke-virtual {p0, v5, v5, v0}, Lorg/opencv/core/Mat;->get(II[B)I

    .line 389
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 390
    aget-byte v3, v0, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 392
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_float(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, fs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v5, 0x0

    .line 280
    if-nez p1, :cond_0

    .line 281
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "fs == null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 282
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 283
    .local v1, count:I
    sget v3, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 284
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CvType.CV_32FC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 287
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 288
    new-array v0, v1, [F

    .line 289
    .local v0, buff:[F
    invoke-virtual {p0, v5, v5, v0}, Lorg/opencv/core/Mat;->get(II[F)I

    .line 290
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 291
    aget v3, v0, v2

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 293
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_int(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, is:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .line 363
    if-nez p1, :cond_0

    .line 364
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "is == null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 365
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 366
    .local v1, count:I
    sget v3, Lorg/opencv/core/CvType;->CV_32SC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 367
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CvType.CV_32SC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 370
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 371
    new-array v0, v1, [I

    .line 372
    .local v0, buff:[I
    invoke-virtual {p0, v5, v5, v0}, Lorg/opencv/core/Mat;->get(II[I)I

    .line 373
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 374
    aget v3, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 376
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_uchar(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, us:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v5, 0x0

    .line 313
    if-nez p1, :cond_0

    .line 314
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Output List can\'t be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 315
    :cond_0
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    .line 316
    .local v1, count:I
    sget v3, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 317
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CvType.CV_8UC1 != m.type() ||  m.cols()!=1\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 320
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 321
    new-array v0, v1, [B

    .line 322
    .local v0, buff:[B
    invoke-virtual {p0, v5, v5, v0}, Lorg/opencv/core/Mat;->get(II[B)I

    .line 323
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 324
    aget-byte v3, v0, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 326
    :cond_3
    return-void
.end method

.method public static Mat_to_vector_vector_Point2f(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 486
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    if-nez p1, :cond_0

    .line 487
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Output List can\'t be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 489
    :cond_0
    if-nez p0, :cond_1

    .line 490
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Input Mat can\'t be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 492
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 493
    .local v1, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    invoke-static {p0, v1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 494
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 495
    .local v3, pt:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/opencv/core/Mat;

    .line 496
    .local v2, mi:Lorg/opencv/core/Mat;
    invoke-static {v2, v3}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point2f(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 497
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 499
    .end local v2           #mi:Lorg/opencv/core/Mat;
    :cond_2
    return-void
.end method

.method public static Mat_to_vector_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 6
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 629
    .local p1, llb:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Ljava/lang/Byte;>;>;"
    if-nez p1, :cond_0

    .line 630
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Output List can\'t be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 632
    :cond_0
    if-nez p0, :cond_1

    .line 633
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Input Mat can\'t be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 635
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 636
    .local v2, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    invoke-static {p0, v2}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 638
    .local v1, lb:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Mat;

    .line 639
    .local v3, mi:Lorg/opencv/core/Mat;
    invoke-static {v3, v1}, Lorg/opencv/utils/Converters;->Mat_to_vector_char(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 640
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 642
    .end local v3           #mi:Lorg/opencv/core/Mat;
    :cond_2
    return-void
.end method

.method public static vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    const/4 v7, 0x0

    .line 229
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 230
    .local v3, count:I
    :goto_0
    if-lez v3, :cond_2

    .line 231
    new-instance v5, Lorg/opencv/core/Mat;

    const/4 v6, 0x1

    sget v8, Lorg/opencv/core/CvType;->CV_32SC2:I

    invoke-direct {v5, v3, v6, v8}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 232
    .local v5, res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v3, 0x2

    new-array v2, v6, [I

    .line 233
    .local v2, buff:[I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1
    if-ge v4, v3, :cond_1

    .line 234
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/opencv/core/Mat;

    iget-wide v0, v6, Lorg/opencv/core/Mat;->nativeObj:J

    .line 235
    .local v0, addr:J
    mul-int/lit8 v6, v4, 0x2

    const/16 v8, 0x20

    shr-long v8, v0, v8

    long-to-int v8, v8

    aput v8, v2, v6

    .line 236
    mul-int/lit8 v6, v4, 0x2

    add-int/lit8 v6, v6, 0x1

    const-wide/16 v8, -0x1

    and-long/2addr v8, v0

    long-to-int v8, v8

    aput v8, v2, v6

    .line 233
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v0           #addr:J
    .end local v2           #buff:[I
    .end local v3           #count:I
    .end local v4           #i:I
    .end local v5           #res:Lorg/opencv/core/Mat;
    :cond_0
    move v3, v7

    .line 229
    goto :goto_0

    .line 238
    .restart local v2       #buff:[I
    .restart local v3       #count:I
    .restart local v4       #i:I
    .restart local v5       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v5, v7, v7, v2}, Lorg/opencv/core/Mat;->put(II[I)I

    .line 242
    .end local v2           #buff:[I
    .end local v4           #i:I
    :goto_2
    return-object v5

    .line 240
    .end local v5           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v5, Lorg/opencv/core/Mat;

    invoke-direct {v5}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v5       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_Point2d_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v0, 0x6

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point2f_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;
    .locals 9
    .parameter
    .parameter "typeDepth"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;I)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 90
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 91
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_4

    .line 92
    packed-switch p1, :pswitch_data_0

    .line 133
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "\'typeDepth\' can be CV_32S, CV_32F or CV_64F"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .end local v1           #count:I
    :cond_0
    move v1, v5

    .line 90
    goto :goto_0

    .line 94
    .restart local v1       #count:I
    :pswitch_0
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_32SC3:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 95
    .local v4, res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x3

    new-array v0, v6, [I

    .line 96
    .local v0, buff:[I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 97
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point3;

    .line 98
    .local v3, p:Lorg/opencv/core/Point3;
    mul-int/lit8 v6, v2, 0x3

    iget-wide v7, v3, Lorg/opencv/core/Point3;->x:D

    double-to-int v7, v7

    aput v7, v0, v6

    .line 99
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point3;->y:D

    double-to-int v7, v7

    aput v7, v0, v6

    .line 100
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point3;->z:D

    double-to-int v7, v7

    aput v7, v0, v6

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    .end local v3           #p:Lorg/opencv/core/Point3;
    :cond_1
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[I)I

    .line 138
    .end local v0           #buff:[I
    .end local v2           #i:I
    :goto_2
    return-object v4

    .line 107
    .end local v4           #res:Lorg/opencv/core/Mat;
    :pswitch_1
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_32FC3:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 108
    .restart local v4       #res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x3

    new-array v0, v6, [F

    .line 109
    .local v0, buff:[F
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_3
    if-ge v2, v1, :cond_2

    .line 110
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point3;

    .line 111
    .restart local v3       #p:Lorg/opencv/core/Point3;
    mul-int/lit8 v6, v2, 0x3

    iget-wide v7, v3, Lorg/opencv/core/Point3;->x:D

    double-to-float v7, v7

    aput v7, v0, v6

    .line 112
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point3;->y:D

    double-to-float v7, v7

    aput v7, v0, v6

    .line 113
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point3;->z:D

    double-to-float v7, v7

    aput v7, v0, v6

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 115
    .end local v3           #p:Lorg/opencv/core/Point3;
    :cond_2
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[F)I

    goto :goto_2

    .line 120
    .end local v0           #buff:[F
    .end local v2           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :pswitch_2
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_64FC3:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 121
    .restart local v4       #res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x3

    new-array v0, v6, [D

    .line 122
    .local v0, buff:[D
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_4
    if-ge v2, v1, :cond_3

    .line 123
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point3;

    .line 124
    .restart local v3       #p:Lorg/opencv/core/Point3;
    mul-int/lit8 v6, v2, 0x3

    iget-wide v7, v3, Lorg/opencv/core/Point3;->x:D

    aput-wide v7, v0, v6

    .line 125
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point3;->y:D

    aput-wide v7, v0, v6

    .line 126
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point3;->z:D

    aput-wide v7, v0, v6

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 128
    .end local v3           #p:Lorg/opencv/core/Point3;
    :cond_3
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_2

    .line 136
    .end local v0           #buff:[D
    .end local v2           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_4
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2

    .line 92
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static vector_Point3d_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    const/4 v0, 0x6

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point3f_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 81
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point3i_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point3;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point3;>;"
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point3_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public static vector_Point_to_Mat(Ljava/util/List;I)Lorg/opencv/core/Mat;
    .locals 9
    .parameter
    .parameter "typeDepth"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;I)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 28
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 29
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_4

    .line 30
    packed-switch p1, :pswitch_data_0

    .line 68
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "\'typeDepth\' can be CV_32S, CV_32F or CV_64F"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .end local v1           #count:I
    :cond_0
    move v1, v5

    .line 28
    goto :goto_0

    .line 32
    .restart local v1       #count:I
    :pswitch_0
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_32SC2:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 33
    .local v4, res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x2

    new-array v0, v6, [I

    .line 34
    .local v0, buff:[I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 35
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point;

    .line 36
    .local v3, p:Lorg/opencv/core/Point;
    mul-int/lit8 v6, v2, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point;->x:D

    double-to-int v7, v7

    aput v7, v0, v6

    .line 37
    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point;->y:D

    double-to-int v7, v7

    aput v7, v0, v6

    .line 34
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 39
    .end local v3           #p:Lorg/opencv/core/Point;
    :cond_1
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[I)I

    .line 73
    .end local v0           #buff:[I
    .end local v2           #i:I
    :goto_2
    return-object v4

    .line 44
    .end local v4           #res:Lorg/opencv/core/Mat;
    :pswitch_1
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_32FC2:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 45
    .restart local v4       #res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x2

    new-array v0, v6, [F

    .line 46
    .local v0, buff:[F
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_3
    if-ge v2, v1, :cond_2

    .line 47
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point;

    .line 48
    .restart local v3       #p:Lorg/opencv/core/Point;
    mul-int/lit8 v6, v2, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point;->x:D

    double-to-float v7, v7

    aput v7, v0, v6

    .line 49
    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point;->y:D

    double-to-float v7, v7

    aput v7, v0, v6

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 51
    .end local v3           #p:Lorg/opencv/core/Point;
    :cond_2
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[F)I

    goto :goto_2

    .line 56
    .end local v0           #buff:[F
    .end local v2           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :pswitch_2
    new-instance v4, Lorg/opencv/core/Mat;

    sget v6, Lorg/opencv/core/CvType;->CV_64FC2:I

    invoke-direct {v4, v1, v7, v6}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 57
    .restart local v4       #res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x2

    new-array v0, v6, [D

    .line 58
    .local v0, buff:[D
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_4
    if-ge v2, v1, :cond_3

    .line 59
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Point;

    .line 60
    .restart local v3       #p:Lorg/opencv/core/Point;
    mul-int/lit8 v6, v2, 0x2

    iget-wide v7, v3, Lorg/opencv/core/Point;->x:D

    aput-wide v7, v0, v6

    .line 61
    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x1

    iget-wide v7, v3, Lorg/opencv/core/Point;->y:D

    aput-wide v7, v0, v6

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 63
    .end local v3           #p:Lorg/opencv/core/Point;
    :cond_3
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    goto :goto_2

    .line 71
    .end local v0           #buff:[D
    .end local v2           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_4
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2

    .line 30
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static vector_Rect_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Rect;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, rs:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Rect;>;"
    const/4 v5, 0x0

    .line 396
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 397
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_2

    .line 398
    new-instance v4, Lorg/opencv/core/Mat;

    const/4 v6, 0x1

    sget v7, Lorg/opencv/core/CvType;->CV_32SC4:I

    invoke-direct {v4, v1, v6, v7}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 399
    .local v4, res:Lorg/opencv/core/Mat;
    mul-int/lit8 v6, v1, 0x4

    new-array v0, v6, [I

    .line 400
    .local v0, buff:[I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 401
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/opencv/core/Rect;

    .line 402
    .local v3, r:Lorg/opencv/core/Rect;
    mul-int/lit8 v6, v2, 0x4

    iget v7, v3, Lorg/opencv/core/Rect;->x:I

    aput v7, v0, v6

    .line 403
    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x1

    iget v7, v3, Lorg/opencv/core/Rect;->y:I

    aput v7, v0, v6

    .line 404
    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    iget v7, v3, Lorg/opencv/core/Rect;->width:I

    aput v7, v0, v6

    .line 405
    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x3

    iget v7, v3, Lorg/opencv/core/Rect;->height:I

    aput v7, v0, v6

    .line 400
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0           #buff:[I
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #r:Lorg/opencv/core/Rect;
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_0
    move v1, v5

    .line 396
    goto :goto_0

    .line 407
    .restart local v0       #buff:[I
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v4       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v4, v5, v5, v0}, Lorg/opencv/core/Mat;->put(II[I)I

    .line 411
    .end local v0           #buff:[I
    .end local v2           #i:I
    :goto_2
    return-object v4

    .line 409
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_char_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, bs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v6, 0x0

    .line 330
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 331
    .local v2, count:I
    :goto_0
    if-lez v2, :cond_2

    .line 332
    new-instance v4, Lorg/opencv/core/Mat;

    const/4 v5, 0x1

    sget v7, Lorg/opencv/core/CvType;->CV_8SC1:I

    invoke-direct {v4, v2, v5, v7}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 333
    .local v4, res:Lorg/opencv/core/Mat;
    new-array v1, v2, [B

    .line 334
    .local v1, buff:[B
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 335
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 336
    .local v0, b:B
    aput-byte v0, v1, v3

    .line 334
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0           #b:B
    .end local v1           #buff:[B
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_0
    move v2, v6

    .line 330
    goto :goto_0

    .line 338
    .restart local v1       #buff:[B
    .restart local v2       #count:I
    .restart local v3       #i:I
    .restart local v4       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v4, v6, v6, v1}, Lorg/opencv/core/Mat;->put(II[B)I

    .line 342
    .end local v1           #buff:[B
    .end local v3           #i:I
    :goto_2
    return-object v4

    .line 340
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_double_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, ds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Double;>;"
    const/4 v7, 0x0

    .line 533
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 534
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_2

    .line 535
    new-instance v3, Lorg/opencv/core/Mat;

    const/4 v6, 0x1

    sget v8, Lorg/opencv/core/CvType;->CV_64FC1:I

    invoke-direct {v3, v1, v6, v8}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 536
    .local v3, res:Lorg/opencv/core/Mat;
    new-array v0, v1, [D

    .line 537
    .local v0, buff:[D
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 538
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 539
    .local v4, v:D
    aput-wide v4, v0, v2

    .line 537
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0           #buff:[D
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #res:Lorg/opencv/core/Mat;
    .end local v4           #v:D
    :cond_0
    move v1, v7

    .line 533
    goto :goto_0

    .line 541
    .restart local v0       #buff:[D
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v3       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v3, v7, v7, v0}, Lorg/opencv/core/Mat;->put(II[D)I

    .line 545
    .end local v0           #buff:[D
    .end local v2           #i:I
    :goto_2
    return-object v3

    .line 543
    .end local v3           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-direct {v3}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v3       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_float_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, fs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Float;>;"
    const/4 v6, 0x0

    .line 264
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 265
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_2

    .line 266
    new-instance v4, Lorg/opencv/core/Mat;

    const/4 v5, 0x1

    sget v7, Lorg/opencv/core/CvType;->CV_32FC1:I

    invoke-direct {v4, v1, v5, v7}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 267
    .local v4, res:Lorg/opencv/core/Mat;
    new-array v0, v1, [F

    .line 268
    .local v0, buff:[F
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    if-ge v3, v1, :cond_1

    .line 269
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 270
    .local v2, f:F
    aput v2, v0, v3

    .line 268
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0           #buff:[F
    .end local v1           #count:I
    .end local v2           #f:F
    .end local v3           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_0
    move v1, v6

    .line 264
    goto :goto_0

    .line 272
    .restart local v0       #buff:[F
    .restart local v1       #count:I
    .restart local v3       #i:I
    .restart local v4       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v4, v6, v6, v0}, Lorg/opencv/core/Mat;->put(II[F)I

    .line 276
    .end local v0           #buff:[F
    .end local v3           #i:I
    :goto_2
    return-object v4

    .line 274
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_int_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, is:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 347
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 348
    .local v1, count:I
    :goto_0
    if-lez v1, :cond_2

    .line 349
    new-instance v3, Lorg/opencv/core/Mat;

    const/4 v5, 0x1

    sget v7, Lorg/opencv/core/CvType;->CV_32SC1:I

    invoke-direct {v3, v1, v5, v7}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 350
    .local v3, res:Lorg/opencv/core/Mat;
    new-array v0, v1, [I

    .line 351
    .local v0, buff:[I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v1, :cond_1

    .line 352
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 353
    .local v4, v:I
    aput v4, v0, v2

    .line 351
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0           #buff:[I
    .end local v1           #count:I
    .end local v2           #i:I
    .end local v3           #res:Lorg/opencv/core/Mat;
    .end local v4           #v:I
    :cond_0
    move v1, v6

    .line 347
    goto :goto_0

    .line 355
    .restart local v0       #buff:[I
    .restart local v1       #count:I
    .restart local v2       #i:I
    .restart local v3       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v3, v6, v6, v0}, Lorg/opencv/core/Mat;->put(II[I)I

    .line 359
    .end local v0           #buff:[I
    .end local v2           #i:I
    :goto_2
    return-object v3

    .line 357
    .end local v3           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-direct {v3}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v3       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_uchar_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .local p0, bs:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v6, 0x0

    .line 297
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 298
    .local v2, count:I
    :goto_0
    if-lez v2, :cond_2

    .line 299
    new-instance v4, Lorg/opencv/core/Mat;

    const/4 v5, 0x1

    sget v7, Lorg/opencv/core/CvType;->CV_8UC1:I

    invoke-direct {v4, v2, v5, v7}, Lorg/opencv/core/Mat;-><init>(III)V

    .line 300
    .local v4, res:Lorg/opencv/core/Mat;
    new-array v1, v2, [B

    .line 301
    .local v1, buff:[B
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 302
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 303
    .local v0, b:B
    aput-byte v0, v1, v3

    .line 301
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0           #b:B
    .end local v1           #buff:[B
    .end local v2           #count:I
    .end local v3           #i:I
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_0
    move v2, v6

    .line 297
    goto :goto_0

    .line 305
    .restart local v1       #buff:[B
    .restart local v2       #count:I
    .restart local v3       #i:I
    .restart local v4       #res:Lorg/opencv/core/Mat;
    :cond_1
    invoke-virtual {v4, v6, v6, v1}, Lorg/opencv/core/Mat;->put(II[B)I

    .line 309
    .end local v1           #buff:[B
    .end local v3           #i:I
    :goto_2
    return-object v4

    .line 307
    .end local v4           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v4, Lorg/opencv/core/Mat;

    invoke-direct {v4}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v4       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 473
    .local p0, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    .local p1, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 474
    .local v1, lCount:I
    :goto_0
    if-lez v1, :cond_2

    .line 475
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 476
    .local v2, lpt:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static {v2}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 473
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #lCount:I
    .end local v2           #lpt:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 477
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #lCount:I
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v3

    .line 481
    .end local v0           #i$:Ljava/util/Iterator;
    .local v3, res:Lorg/opencv/core/Mat;
    :goto_2
    return-object v3

    .line 479
    .end local v3           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-direct {v3}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v3       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method

.method public static vector_vector_char_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;)",
            "Lorg/opencv/core/Mat;"
        }
    .end annotation

    .prologue
    .line 617
    .local p0, llb:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Ljava/lang/Byte;>;>;"
    .local p1, mats:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 618
    .local v1, lCount:I
    :goto_0
    if-lez v1, :cond_2

    .line 619
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 620
    .local v2, lb:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    invoke-static {v2}, Lorg/opencv/utils/Converters;->vector_char_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 617
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #lCount:I
    .end local v2           #lb:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Byte;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 621
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #lCount:I
    :cond_1
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v3

    .line 625
    .end local v0           #i$:Ljava/util/Iterator;
    .local v3, res:Lorg/opencv/core/Mat;
    :goto_2
    return-object v3

    .line 623
    .end local v3           #res:Lorg/opencv/core/Mat;
    :cond_2
    new-instance v3, Lorg/opencv/core/Mat;

    invoke-direct {v3}, Lorg/opencv/core/Mat;-><init>()V

    .restart local v3       #res:Lorg/opencv/core/Mat;
    goto :goto_2
.end method
