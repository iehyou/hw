.class public Lorg/opencv/core/Size;
.super Ljava/lang/Object;
.source "Size.java"


# instance fields
.field public height:D

.field public width:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 24
    invoke-direct {p0, v0, v1, v0, v1}, Lorg/opencv/core/Size;-><init>(DD)V

    .line 25
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 0
    .parameter "width"
    .parameter "height"

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lorg/opencv/core/Size;->width:D

    .line 20
    iput-wide p3, p0, Lorg/opencv/core/Size;->height:D

    .line 21
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Point;)V
    .locals 2
    .parameter "p"

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iget-wide v0, p1, Lorg/opencv/core/Point;->x:D

    iput-wide v0, p0, Lorg/opencv/core/Size;->width:D

    .line 29
    iget-wide v0, p1, Lorg/opencv/core/Point;->y:D

    iput-wide v0, p0, Lorg/opencv/core/Size;->height:D

    .line 30
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .parameter "vals"

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p0, p1}, Lorg/opencv/core/Size;->set([D)V

    .line 34
    return-void
.end method


# virtual methods
.method public area()D
    .locals 4

    .prologue
    .line 47
    iget-wide v0, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v2, p0, Lorg/opencv/core/Size;->height:D

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0}, Lorg/opencv/core/Size;->clone()Lorg/opencv/core/Size;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/opencv/core/Size;
    .locals 5

    .prologue
    .line 51
    new-instance v0, Lorg/opencv/core/Size;

    iget-wide v1, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p0, Lorg/opencv/core/Size;->height:D

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/opencv/core/Size;-><init>(DD)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    if-ne p0, p1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lorg/opencv/core/Size;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 70
    check-cast v0, Lorg/opencv/core/Size;

    .line 71
    .local v0, it:Lorg/opencv/core/Size;
    iget-wide v3, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    cmpl-double v3, v3, v5

    if-nez v3, :cond_3

    iget-wide v3, p0, Lorg/opencv/core/Size;->height:D

    iget-wide v5, v0, Lorg/opencv/core/Size;->height:D

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 56
    const/16 v0, 0x1f

    .line 57
    .local v0, prime:I
    const/4 v1, 0x1

    .line 59
    .local v1, result:I
    iget-wide v4, p0, Lorg/opencv/core/Size;->height:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 60
    .local v2, temp:J
    ushr-long v4, v2, v6

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int/lit8 v1, v4, 0x1f

    .line 61
    iget-wide v4, p0, Lorg/opencv/core/Size;->width:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 62
    mul-int/lit8 v4, v1, 0x1f

    ushr-long v5, v2, v6

    xor-long/2addr v5, v2

    long-to-int v5, v5

    add-int v1, v4, v5

    .line 63
    return v1
.end method

.method public set([D)V
    .locals 5
    .parameter "vals"

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 37
    if-eqz p1, :cond_2

    .line 38
    array-length v0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x0

    aget-wide v0, p1, v0

    :goto_0
    iput-wide v0, p0, Lorg/opencv/core/Size;->width:D

    .line 39
    array-length v0, p1

    if-le v0, v4, :cond_0

    aget-wide v2, p1, v4

    :cond_0
    iput-wide v2, p0, Lorg/opencv/core/Size;->height:D

    .line 44
    :goto_1
    return-void

    :cond_1
    move-wide v0, v2

    .line 38
    goto :goto_0

    .line 41
    :cond_2
    iput-wide v2, p0, Lorg/opencv/core/Size;->width:D

    .line 42
    iput-wide v2, p0, Lorg/opencv/core/Size;->height:D

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lorg/opencv/core/Size;->width:D

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/opencv/core/Size;->height:D

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
