.class public Lorg/opencv/core/Core;
.super Ljava/lang/Object;
.source "Core.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/opencv/core/Core$MinMaxLocResult;
    }
.end annotation


# static fields
.field public static final CMP_EQ:I = 0x0

.field public static final CMP_GE:I = 0x2

.field public static final CMP_GT:I = 0x1

.field public static final CMP_LE:I = 0x4

.field public static final CMP_LT:I = 0x3

.field public static final CMP_NE:I = 0x5

.field public static final COVAR_COLS:I = 0x10

.field public static final COVAR_NORMAL:I = 0x1

.field public static final COVAR_ROWS:I = 0x8

.field public static final COVAR_SCALE:I = 0x4

.field public static final COVAR_SCRAMBLED:I = 0x0

.field public static final COVAR_USE_AVG:I = 0x2

.field private static final CV_16S:I = 0x3

.field private static final CV_16U:I = 0x2

.field private static final CV_32F:I = 0x5

.field private static final CV_32S:I = 0x4

.field private static final CV_64F:I = 0x6

.field private static final CV_8S:I = 0x1

.field private static final CV_8U:I = 0x0

.field private static final CV_USRTYPE1:I = 0x7

.field public static final DCT_INVERSE:I = 0x1

.field public static final DCT_ROWS:I = 0x4

.field public static final DECOMP_CHOLESKY:I = 0x3

.field public static final DECOMP_EIG:I = 0x2

.field public static final DECOMP_LU:I = 0x0

.field public static final DECOMP_NORMAL:I = 0x10

.field public static final DECOMP_QR:I = 0x4

.field public static final DECOMP_SVD:I = 0x1

.field public static final DEPTH_MASK:I = 0x7

.field public static final DEPTH_MASK_16S:I = 0x8

.field public static final DEPTH_MASK_16U:I = 0x4

.field public static final DEPTH_MASK_32F:I = 0x20

.field public static final DEPTH_MASK_32S:I = 0x10

.field public static final DEPTH_MASK_64F:I = 0x40

.field public static final DEPTH_MASK_8S:I = 0x2

.field public static final DEPTH_MASK_8U:I = 0x1

.field public static final DEPTH_MASK_ALL:I = 0x7f

.field public static final DEPTH_MASK_ALL_BUT_8S:I = 0x7d

.field public static final DEPTH_MASK_FLT:I = 0x60

.field public static final DFT_COMPLEX_OUTPUT:I = 0x10

.field public static final DFT_INVERSE:I = 0x1

.field public static final DFT_REAL_OUTPUT:I = 0x20

.field public static final DFT_ROWS:I = 0x4

.field public static final DFT_SCALE:I = 0x2

.field public static final FILLED:I = -0x1

.field public static final FONT_HERSHEY_COMPLEX:I = 0x3

.field public static final FONT_HERSHEY_COMPLEX_SMALL:I = 0x5

.field public static final FONT_HERSHEY_DUPLEX:I = 0x2

.field public static final FONT_HERSHEY_PLAIN:I = 0x1

.field public static final FONT_HERSHEY_SCRIPT_COMPLEX:I = 0x7

.field public static final FONT_HERSHEY_SCRIPT_SIMPLEX:I = 0x6

.field public static final FONT_HERSHEY_SIMPLEX:I = 0x0

.field public static final FONT_HERSHEY_TRIPLEX:I = 0x4

.field public static final FONT_ITALIC:I = 0x10

.field public static final GEMM_1_T:I = 0x1

.field public static final GEMM_2_T:I = 0x2

.field public static final GEMM_3_T:I = 0x4

.field public static final KMEANS_PP_CENTERS:I = 0x2

.field public static final KMEANS_RANDOM_CENTERS:I = 0x0

.field public static final KMEANS_USE_INITIAL_LABELS:I = 0x1

.field public static final LINE_4:I = 0x4

.field public static final LINE_8:I = 0x8

.field public static final LINE_AA:I = 0x10

.field public static final MAGIC_MASK:I = -0x10000

.field public static final NORM_INF:I = 0x1

.field public static final NORM_L1:I = 0x2

.field public static final NORM_L2:I = 0x4

.field public static final NORM_MINMAX:I = 0x20

.field public static final NORM_RELATIVE:I = 0x8

.field public static final NORM_TYPE_MASK:I = 0x7

.field public static final REDUCE_AVG:I = 0x1

.field public static final REDUCE_MAX:I = 0x2

.field public static final REDUCE_MIN:I = 0x3

.field public static final REDUCE_SUM:I = 0x0

.field public static final SORT_ASCENDING:I = 0x0

.field public static final SORT_DESCENDING:I = 0x10

.field public static final SORT_EVERY_COLUMN:I = 0x1

.field public static final SORT_EVERY_ROW:I = 0x0

.field public static final SVD_FULL_UV:I = 0x4

.field public static final SVD_MODIFY_A:I = 0x1

.field public static final SVD_NO_UV:I = 0x2

.field public static final TYPE_MASK:I = 0xfff


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6720
    const-string v0, "opencv_java"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6598
    return-void
.end method

.method public static LUT(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "lut"
    .parameter "dst"

    .prologue
    .line 168
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->LUT_1(JJJ)V

    .line 170
    return-void
.end method

.method public static LUT(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "src"
    .parameter "lut"
    .parameter "dst"
    .parameter "interpolation"

    .prologue
    .line 135
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->LUT_0(JJJI)V

    .line 137
    return-void
.end method

.method private static native LUT_0(JJJI)V
.end method

.method private static native LUT_1(JJJ)V
.end method

.method public static Mahalanobis(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 8
    .parameter "v1"
    .parameter "v2"
    .parameter "icovar"

    .prologue
    .line 199
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->Mahalanobis_0(JJJ)D

    move-result-wide v6

    .line 201
    .local v6, retVal:D
    return-wide v6
.end method

.method private static native Mahalanobis_0(JJJ)D
.end method

.method public static PCABackProject(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "data"
    .parameter "mean"
    .parameter "eigenvectors"
    .parameter "result"

    .prologue
    .line 212
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->PCABackProject_0(JJJJ)V

    .line 214
    return-void
.end method

.method private static native PCABackProject_0(JJJJ)V
.end method

.method public static PCACompute(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "data"
    .parameter "mean"
    .parameter "eigenvectors"

    .prologue
    .line 233
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->PCACompute_1(JJJ)V

    .line 235
    return-void
.end method

.method public static PCACompute(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "data"
    .parameter "mean"
    .parameter "eigenvectors"
    .parameter "maxComponents"

    .prologue
    .line 225
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->PCACompute_0(JJJI)V

    .line 227
    return-void
.end method

.method private static native PCACompute_0(JJJI)V
.end method

.method private static native PCACompute_1(JJJ)V
.end method

.method public static PCAProject(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "data"
    .parameter "mean"
    .parameter "eigenvectors"
    .parameter "result"

    .prologue
    .line 246
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->PCAProject_0(JJJJ)V

    .line 248
    return-void
.end method

.method private static native PCAProject_0(JJJJ)V
.end method

.method public static SVBackSubst(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 10
    .parameter "w"
    .parameter "u"
    .parameter "vt"
    .parameter "rhs"
    .parameter "dst"

    .prologue
    .line 259
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v8, p4, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->SVBackSubst_0(JJJJJ)V

    .line 261
    return-void
.end method

.method private static native SVBackSubst_0(JJJJJ)V
.end method

.method public static SVDecomp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src"
    .parameter "w"
    .parameter "u"
    .parameter "vt"

    .prologue
    .line 280
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->SVDecomp_1(JJJJ)V

    .line 282
    return-void
.end method

.method public static SVDecomp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9
    .parameter "src"
    .parameter "w"
    .parameter "u"
    .parameter "vt"
    .parameter "flags"

    .prologue
    .line 272
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->SVDecomp_0(JJJJI)V

    .line 274
    return-void
.end method

.method private static native SVDecomp_0(JJJJI)V
.end method

.method private static native SVDecomp_1(JJJJ)V
.end method

.method public static absdiff(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 326
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->absdiff_0(JJJ)V

    .line 328
    return-void
.end method

.method private static native absdiff_0(JJJ)V
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 492
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->add_2(JJJ)V

    .line 494
    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 440
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->add_1(JJJJ)V

    .line 442
    return-void
.end method

.method public static add(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"
    .parameter "dtype"

    .prologue
    .line 386
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->add_0(JJJJI)V

    .line 388
    return-void
.end method

.method public static addWeighted(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DDLorg/opencv/core/Mat;)V
    .locals 13
    .parameter "src1"
    .parameter "alpha"
    .parameter "src2"
    .parameter "beta"
    .parameter "gamma"
    .parameter "dst"

    .prologue
    .line 574
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v5, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p8

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v3, p1

    move-wide/from16 v7, p4

    move-wide/from16 v9, p6

    invoke-static/range {v1 .. v12}, Lorg/opencv/core/Core;->addWeighted_1(JDJDDJ)V

    .line 576
    return-void
.end method

.method public static addWeighted(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DDLorg/opencv/core/Mat;I)V
    .locals 14
    .parameter "src1"
    .parameter "alpha"
    .parameter "src2"
    .parameter "beta"
    .parameter "gamma"
    .parameter "dst"
    .parameter "dtype"

    .prologue
    .line 537
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-wide v5, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p8

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v3, p1

    move-wide/from16 v7, p4

    move-wide/from16 v9, p6

    move/from16 v13, p9

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->addWeighted_0(JDJDDJI)V

    .line 539
    return-void
.end method

.method private static native addWeighted_0(JDJDDJI)V
.end method

.method private static native addWeighted_1(JDJDDJ)V
.end method

.method private static native add_0(JJJJI)V
.end method

.method private static native add_1(JJJJ)V
.end method

.method private static native add_2(JJJ)V
.end method

.method public static bitwise_and(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 659
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_and_1(JJJ)V

    .line 661
    return-void
.end method

.method public static bitwise_and(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 620
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_and_0(JJJJ)V

    .line 622
    return-void
.end method

.method private static native bitwise_and_0(JJJJ)V
.end method

.method private static native bitwise_and_1(JJJ)V
.end method

.method public static bitwise_not(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 716
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->bitwise_not_1(JJ)V

    .line 718
    return-void
.end method

.method public static bitwise_not(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 691
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_not_0(JJJ)V

    .line 693
    return-void
.end method

.method private static native bitwise_not_0(JJJ)V
.end method

.method private static native bitwise_not_1(JJ)V
.end method

.method public static bitwise_or(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 801
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_or_1(JJJ)V

    .line 803
    return-void
.end method

.method public static bitwise_or(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 762
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_or_0(JJJJ)V

    .line 764
    return-void
.end method

.method private static native bitwise_or_0(JJJJ)V
.end method

.method private static native bitwise_or_1(JJJ)V
.end method

.method public static bitwise_xor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 888
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->bitwise_xor_1(JJJ)V

    .line 890
    return-void
.end method

.method public static bitwise_xor(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 848
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->bitwise_xor_0(JJJJ)V

    .line 850
    return-void
.end method

.method private static native bitwise_xor_0(JJJJ)V
.end method

.method private static native bitwise_xor_1(JJJ)V
.end method

.method public static calcCovarMatrix(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "samples"
    .parameter "covar"
    .parameter "mean"
    .parameter "flags"

    .prologue
    .line 1018
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->calcCovarMatrix_1(JJJI)V

    .line 1020
    return-void
.end method

.method public static calcCovarMatrix(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 8
    .parameter "samples"
    .parameter "covar"
    .parameter "mean"
    .parameter "flags"
    .parameter "ctype"

    .prologue
    .line 956
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->calcCovarMatrix_0(JJJII)V

    .line 958
    return-void
.end method

.method private static native calcCovarMatrix_0(JJJII)V
.end method

.method private static native calcCovarMatrix_1(JJJI)V
.end method

.method public static cartToPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "x"
    .parameter "y"
    .parameter "magnitude"
    .parameter "angle"

    .prologue
    .line 1089
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->cartToPolar_1(JJJJ)V

    .line 1091
    return-void
.end method

.method public static cartToPolar(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 9
    .parameter "x"
    .parameter "y"
    .parameter "magnitude"
    .parameter "angle"
    .parameter "angleInDegrees"

    .prologue
    .line 1057
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->cartToPolar_0(JJJJZ)V

    .line 1059
    return-void
.end method

.method private static native cartToPolar_0(JJJJZ)V
.end method

.method private static native cartToPolar_1(JJJJ)V
.end method

.method public static checkRange(Lorg/opencv/core/Mat;)Z
    .locals 3
    .parameter "a"

    .prologue
    .line 1223
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->checkRange_4(J)Z

    move-result v0

    .line 1225
    .local v0, retVal:Z
    return v0
.end method

.method public static checkRange(Lorg/opencv/core/Mat;Z)Z
    .locals 3
    .parameter "a"
    .parameter "quiet"

    .prologue
    .line 1200
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Core;->checkRange_3(JZ)Z

    move-result v0

    .line 1202
    .local v0, retVal:Z
    return v0
.end method

.method public static checkRange(Lorg/opencv/core/Mat;ZLorg/opencv/core/Point;)Z
    .locals 4
    .parameter "a"
    .parameter "quiet"
    .parameter "pt"

    .prologue
    .line 1174
    const/4 v2, 0x2

    new-array v0, v2, [D

    .line 1175
    .local v0, pt_out:[D
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, p1, v0}, Lorg/opencv/core/Core;->checkRange_2(JZ[D)Z

    move-result v1

    .line 1176
    .local v1, retVal:Z
    if-eqz p2, :cond_0

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    iput-wide v2, p2, Lorg/opencv/core/Point;->x:D

    const/4 v2, 0x1

    aget-wide v2, v0, v2

    iput-wide v2, p2, Lorg/opencv/core/Point;->y:D

    .line 1177
    :cond_0
    return v1
.end method

.method public static checkRange(Lorg/opencv/core/Mat;ZLorg/opencv/core/Point;D)Z
    .locals 7
    .parameter "a"
    .parameter "quiet"
    .parameter "pt"
    .parameter "minVal"

    .prologue
    .line 1148
    const/4 v0, 0x2

    new-array v3, v0, [D

    .line 1149
    .local v3, pt_out:[D
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->checkRange_1(JZ[DD)Z

    move-result v6

    .line 1150
    .local v6, retVal:Z
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    aget-wide v0, v3, v0

    iput-wide v0, p2, Lorg/opencv/core/Point;->x:D

    const/4 v0, 0x1

    aget-wide v0, v3, v0

    iput-wide v0, p2, Lorg/opencv/core/Point;->y:D

    .line 1151
    :cond_0
    return v6
.end method

.method public static checkRange(Lorg/opencv/core/Mat;ZLorg/opencv/core/Point;DD)Z
    .locals 9
    .parameter "a"
    .parameter "quiet"
    .parameter "pt"
    .parameter "minVal"
    .parameter "maxVal"

    .prologue
    .line 1121
    const/4 v0, 0x2

    new-array v3, v0, [D

    .line 1122
    .local v3, pt_out:[D
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->checkRange_0(JZ[DDD)Z

    move-result v8

    .line 1123
    .local v8, retVal:Z
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    aget-wide v0, v3, v0

    iput-wide v0, p2, Lorg/opencv/core/Point;->x:D

    const/4 v0, 0x1

    aget-wide v0, v3, v0

    iput-wide v0, p2, Lorg/opencv/core/Point;->y:D

    .line 1124
    :cond_0
    return v8
.end method

.method private static native checkRange_0(JZ[DDD)Z
.end method

.method private static native checkRange_1(JZ[DD)Z
.end method

.method private static native checkRange_2(JZ[D)Z
.end method

.method private static native checkRange_3(JZ)Z
.end method

.method private static native checkRange_4(J)Z
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;)V
    .locals 16
    .parameter "img"
    .parameter "center"
    .parameter "radius"
    .parameter "color"

    .prologue
    .line 1322
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v7, v10

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v7, v12

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x3

    aget-wide v14, v7, v14

    move/from16 v7, p2

    invoke-static/range {v1 .. v15}, Lorg/opencv/core/Core;->circle_3(JDDIDDDD)V

    .line 1324
    return-void
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;I)V
    .locals 17
    .parameter "img"
    .parameter "center"
    .parameter "radius"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 1301
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v7, v10

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v7, v12

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x3

    aget-wide v14, v7, v14

    move/from16 v7, p2

    move/from16 v16, p4

    invoke-static/range {v1 .. v16}, Lorg/opencv/core/Core;->circle_2(JDDIDDDDI)V

    .line 1303
    return-void
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;II)V
    .locals 18
    .parameter "img"
    .parameter "center"
    .parameter "radius"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"

    .prologue
    .line 1278
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v7, v10

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v7, v12

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x3

    aget-wide v14, v7, v14

    move/from16 v7, p2

    move/from16 v16, p4

    move/from16 v17, p5

    invoke-static/range {v1 .. v17}, Lorg/opencv/core/Core;->circle_1(JDDIDDDDII)V

    .line 1280
    return-void
.end method

.method public static circle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;ILorg/opencv/core/Scalar;III)V
    .locals 19
    .parameter "img"
    .parameter "center"
    .parameter "radius"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .parameter "shift"

    .prologue
    .line 1254
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x1

    aget-wide v10, v7, v10

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v12, v7, v12

    move-object/from16 v0, p3

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x3

    aget-wide v14, v7, v14

    move/from16 v7, p2

    move/from16 v16, p4

    move/from16 v17, p5

    move/from16 v18, p6

    invoke-static/range {v1 .. v18}, Lorg/opencv/core/Core;->circle_0(JDDIDDDDIII)V

    .line 1256
    return-void
.end method

.method private static native circle_0(JDDIDDDDIII)V
.end method

.method private static native circle_1(JDDIDDDDII)V
.end method

.method private static native circle_2(JDDIDDDDI)V
.end method

.method private static native circle_3(JDDIDDDD)V
.end method

.method public static clipLine(Lorg/opencv/core/Rect;Lorg/opencv/core/Point;Lorg/opencv/core/Point;)Z
    .locals 16
    .parameter "imgRect"
    .parameter "pt1"
    .parameter "pt2"

    .prologue
    .line 1348
    const/4 v1, 0x2

    new-array v9, v1, [D

    .line 1349
    .local v9, pt1_out:[D
    const/4 v1, 0x2

    new-array v14, v1, [D

    .line 1350
    .local v14, pt2_out:[D
    move-object/from16 v0, p0

    iget v1, v0, Lorg/opencv/core/Rect;->x:I

    move-object/from16 v0, p0

    iget v2, v0, Lorg/opencv/core/Rect;->y:I

    move-object/from16 v0, p0

    iget v3, v0, Lorg/opencv/core/Rect;->width:I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/opencv/core/Rect;->height:I

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v7, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v10, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/opencv/core/Point;->y:D

    invoke-static/range {v1 .. v14}, Lorg/opencv/core/Core;->clipLine_0(IIIIDD[DDD[D)Z

    move-result v15

    .line 1351
    .local v15, retVal:Z
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    aget-wide v1, v9, v1

    move-object/from16 v0, p1

    iput-wide v1, v0, Lorg/opencv/core/Point;->x:D

    const/4 v1, 0x1

    aget-wide v1, v9, v1

    move-object/from16 v0, p1

    iput-wide v1, v0, Lorg/opencv/core/Point;->y:D

    .line 1352
    :cond_0
    if-eqz p2, :cond_1

    const/4 v1, 0x0

    aget-wide v1, v14, v1

    move-object/from16 v0, p2

    iput-wide v1, v0, Lorg/opencv/core/Point;->x:D

    const/4 v1, 0x1

    aget-wide v1, v14, v1

    move-object/from16 v0, p2

    iput-wide v1, v0, Lorg/opencv/core/Point;->y:D

    .line 1353
    :cond_1
    return v15
.end method

.method private static native clipLine_0(IIIIDD[DDD[D)Z
.end method

.method public static compare(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "cmpop"

    .prologue
    .line 1411
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->compare_0(JJJI)V

    .line 1413
    return-void
.end method

.method private static native compare_0(JJJI)V
.end method

.method public static completeSymm(Lorg/opencv/core/Mat;)V
    .locals 2
    .parameter "mtx"

    .prologue
    .line 1462
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->completeSymm_1(J)V

    .line 1464
    return-void
.end method

.method public static completeSymm(Lorg/opencv/core/Mat;Z)V
    .locals 2
    .parameter "mtx"
    .parameter "lowerToUpper"

    .prologue
    .line 1440
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lorg/opencv/core/Core;->completeSymm_0(JZ)V

    .line 1442
    return-void
.end method

.method private static native completeSymm_0(JZ)V
.end method

.method private static native completeSymm_1(J)V
.end method

.method public static convertScaleAbs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 1557
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->convertScaleAbs_2(JJ)V

    .line 1559
    return-void
.end method

.method public static convertScaleAbs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"

    .prologue
    .line 1528
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->convertScaleAbs_1(JJD)V

    .line 1530
    return-void
.end method

.method public static convertScaleAbs(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V
    .locals 8
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"
    .parameter "beta"

    .prologue
    .line 1498
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->convertScaleAbs_0(JJDD)V

    .line 1500
    return-void
.end method

.method private static native convertScaleAbs_0(JJDD)V
.end method

.method private static native convertScaleAbs_1(JJD)V
.end method

.method private static native convertScaleAbs_2(JJ)V
.end method

.method public static countNonZero(Lorg/opencv/core/Mat;)I
    .locals 3
    .parameter "src"

    .prologue
    .line 1586
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->countNonZero_0(J)I

    move-result v0

    .line 1588
    .local v0, retVal:I
    return v0
.end method

.method private static native countNonZero_0(J)I
.end method

.method public static cubeRoot(F)F
    .locals 1
    .parameter "val"

    .prologue
    .line 1610
    invoke-static {p0}, Lorg/opencv/core/Core;->cubeRoot_0(F)F

    move-result v0

    .line 1612
    .local v0, retVal:F
    return v0
.end method

.method private static native cubeRoot_0(F)F
.end method

.method public static dct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 1753
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->dct_1(JJ)V

    .line 1755
    return-void
.end method

.method public static dct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 1688
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->dct_0(JJI)V

    .line 1690
    return-void
.end method

.method private static native dct_0(JJI)V
.end method

.method private static native dct_1(JJ)V
.end method

.method public static determinant(Lorg/opencv/core/Mat;)D
    .locals 4
    .parameter "mtx"

    .prologue
    .line 1787
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Core;->determinant_0(J)D

    move-result-wide v0

    .line 1789
    .local v0, retVal:D
    return-wide v0
.end method

.method private static native determinant_0(J)D
.end method

.method public static dft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 2252
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->dft_2(JJ)V

    .line 2254
    return-void
.end method

.method public static dft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 2120
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->dft_1(JJI)V

    .line 2122
    return-void
.end method

.method public static dft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "flags"
    .parameter "nonzeroRows"

    .prologue
    .line 1959
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->dft_0(JJII)V

    .line 1961
    return-void
.end method

.method private static native dft_0(JJII)V
.end method

.method private static native dft_1(JJI)V
.end method

.method private static native dft_2(JJ)V
.end method

.method public static divide(DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "scale"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 2430
    iget-wide v2, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, p0

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->divide_4(DJJ)V

    .line 2432
    return-void
.end method

.method public static divide(DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "scale"
    .parameter "src2"
    .parameter "dst"
    .parameter "dtype"

    .prologue
    .line 2398
    iget-wide v2, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v0, p0

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->divide_3(DJJI)V

    .line 2400
    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 2358
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->divide_2(JJJ)V

    .line 2360
    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "scale"

    .prologue
    .line 2326
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->divide_1(JJJD)V

    .line 2328
    return-void
.end method

.method public static divide(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DI)V
    .locals 9
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "scale"
    .parameter "dtype"

    .prologue
    .line 2293
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    move v8, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->divide_0(JJJDI)V

    .line 2295
    return-void
.end method

.method private static native divide_0(JJJDI)V
.end method

.method private static native divide_1(JJJD)V
.end method

.method private static native divide_2(JJJ)V
.end method

.method private static native divide_3(DJJI)V
.end method

.method private static native divide_4(DJJ)V
.end method

.method public static eigen(Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Z
    .locals 8
    .parameter "src"
    .parameter "computeEigenvectors"
    .parameter "eigenvalues"
    .parameter "eigenvectors"

    .prologue
    .line 2465
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->eigen_0(JZJJ)Z

    move-result v7

    .line 2467
    .local v7, retVal:Z
    return v7
.end method

.method private static native eigen_0(JZJJ)Z
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;)V
    .locals 25
    .parameter "img"
    .parameter "center"
    .parameter "axes"
    .parameter "angle"
    .parameter "startAngle"
    .parameter "endAngle"
    .parameter "color"

    .prologue
    .line 2604
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v17, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x1

    aget-wide v19, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v21, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v23, v11, v12

    move-wide/from16 v11, p3

    move-wide/from16 v13, p5

    move-wide/from16 v15, p7

    invoke-static/range {v1 .. v24}, Lorg/opencv/core/Core;->ellipse_3(JDDDDDDDDDDD)V

    .line 2606
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;I)V
    .locals 26
    .parameter "img"
    .parameter "center"
    .parameter "axes"
    .parameter "angle"
    .parameter "startAngle"
    .parameter "endAngle"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 2573
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v17, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x1

    aget-wide v19, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v21, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v23, v11, v12

    move-wide/from16 v11, p3

    move-wide/from16 v13, p5

    move-wide/from16 v15, p7

    move/from16 v25, p10

    invoke-static/range {v1 .. v25}, Lorg/opencv/core/Core;->ellipse_2(JDDDDDDDDDDDI)V

    .line 2575
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;II)V
    .locals 27
    .parameter "img"
    .parameter "center"
    .parameter "axes"
    .parameter "angle"
    .parameter "startAngle"
    .parameter "endAngle"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"

    .prologue
    .line 2540
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v17, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x1

    aget-wide v19, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v21, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v23, v11, v12

    move-wide/from16 v11, p3

    move-wide/from16 v13, p5

    move-wide/from16 v15, p7

    move/from16 v25, p10

    move/from16 v26, p11

    invoke-static/range {v1 .. v26}, Lorg/opencv/core/Core;->ellipse_1(JDDDDDDDDDDDII)V

    .line 2542
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Size;DDDLorg/opencv/core/Scalar;III)V
    .locals 28
    .parameter "img"
    .parameter "center"
    .parameter "axes"
    .parameter "angle"
    .parameter "startAngle"
    .parameter "endAngle"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .parameter "shift"

    .prologue
    .line 2506
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v17, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x1

    aget-wide v19, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x2

    aget-wide v21, v11, v12

    move-object/from16 v0, p9

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v23, v11, v12

    move-wide/from16 v11, p3

    move-wide/from16 v13, p5

    move-wide/from16 v15, p7

    move/from16 v25, p10

    move/from16 v26, p11

    move/from16 v27, p12

    invoke-static/range {v1 .. v27}, Lorg/opencv/core/Core;->ellipse_0(JDDDDDDDDDDDIII)V

    .line 2508
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;)V
    .locals 21
    .parameter "img"
    .parameter "box"
    .parameter "color"

    .prologue
    .line 2701
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v3, v3, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v5, v5, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v7, v7, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v9, v9, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p1

    iget-wide v11, v0, Lorg/opencv/core/RotatedRect;->angle:D

    move-object/from16 v0, p2

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v13, v13, v14

    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v15, v15, v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aget-wide v17, v17, v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v19, v0

    const/16 v20, 0x3

    aget-wide v19, v19, v20

    invoke-static/range {v1 .. v20}, Lorg/opencv/core/Core;->ellipse_6(JDDDDDDDDD)V

    .line 2703
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;I)V
    .locals 22
    .parameter "img"
    .parameter "box"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 2672
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v3, v3, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v5, v5, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v7, v7, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v9, v9, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p1

    iget-wide v11, v0, Lorg/opencv/core/RotatedRect;->angle:D

    move-object/from16 v0, p2

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v13, v13, v14

    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v15, v15, v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aget-wide v17, v17, v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v19, v0

    const/16 v20, 0x3

    aget-wide v19, v19, v20

    move/from16 v21, p3

    invoke-static/range {v1 .. v21}, Lorg/opencv/core/Core;->ellipse_5(JDDDDDDDDDI)V

    .line 2674
    return-void
.end method

.method public static ellipse(Lorg/opencv/core/Mat;Lorg/opencv/core/RotatedRect;Lorg/opencv/core/Scalar;II)V
    .locals 23
    .parameter "img"
    .parameter "box"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"

    .prologue
    .line 2641
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v3, v3, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/opencv/core/RotatedRect;->center:Lorg/opencv/core/Point;

    iget-wide v5, v5, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p1

    iget-object v7, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v7, v7, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p1

    iget-object v9, v0, Lorg/opencv/core/RotatedRect;->size:Lorg/opencv/core/Size;

    iget-wide v9, v9, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p1

    iget-wide v11, v0, Lorg/opencv/core/RotatedRect;->angle:D

    move-object/from16 v0, p2

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x0

    aget-wide v13, v13, v14

    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x1

    aget-wide v15, v15, v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x2

    aget-wide v17, v17, v18

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v19, v0

    const/16 v20, 0x3

    aget-wide v19, v19, v20

    move/from16 v21, p3

    move/from16 v22, p4

    invoke-static/range {v1 .. v22}, Lorg/opencv/core/Core;->ellipse_4(JDDDDDDDDDII)V

    .line 2643
    return-void
.end method

.method public static ellipse2Poly(Lorg/opencv/core/Point;Lorg/opencv/core/Size;IIIILjava/util/List;)V
    .locals 16
    .parameter "center"
    .parameter "axes"
    .parameter "angle"
    .parameter "arcStart"
    .parameter "arcEnd"
    .parameter "delta"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Point;",
            "Lorg/opencv/core/Size;",
            "IIII",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2731
    .local p6, pts:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    new-instance v15, Lorg/opencv/core/Mat;

    invoke-direct {v15}, Lorg/opencv/core/Mat;-><init>()V

    .line 2732
    .local v15, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p0

    iget-wide v3, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Size;->width:D

    move-object/from16 v0, p1

    iget-wide v7, v0, Lorg/opencv/core/Size;->height:D

    iget-wide v13, v15, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v9, p2

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    invoke-static/range {v1 .. v14}, Lorg/opencv/core/Core;->ellipse2Poly_0(DDDDIIIIJ)V

    .line 2733
    move-object/from16 v0, p6

    invoke-static {v15, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 2734
    return-void
.end method

.method private static native ellipse2Poly_0(DDDDIIIIJ)V
.end method

.method private static native ellipse_0(JDDDDDDDDDDDIII)V
.end method

.method private static native ellipse_1(JDDDDDDDDDDDII)V
.end method

.method private static native ellipse_2(JDDDDDDDDDDDI)V
.end method

.method private static native ellipse_3(JDDDDDDDDDDD)V
.end method

.method private static native ellipse_4(JDDDDDDDDDII)V
.end method

.method private static native ellipse_5(JDDDDDDDDDI)V
.end method

.method private static native ellipse_6(JDDDDDDDDD)V
.end method

.method public static exp(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 2770
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->exp_0(JJ)V

    .line 2772
    return-void
.end method

.method private static native exp_0(JJ)V
.end method

.method public static extractChannel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "coi"

    .prologue
    .line 2783
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->extractChannel_0(JJI)V

    .line 2785
    return-void
.end method

.method private static native extractChannel_0(JJI)V
.end method

.method public static fastAtan2(FF)F
    .locals 1
    .parameter "y"
    .parameter "x"

    .prologue
    .line 2808
    invoke-static {p0, p1}, Lorg/opencv/core/Core;->fastAtan2_0(FF)F

    move-result v0

    .line 2810
    .local v0, retVal:F
    return v0
.end method

.method private static native fastAtan2_0(FF)F
.end method

.method public static fillConvexPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;)V
    .locals 13
    .parameter "img"
    .parameter
    .parameter "color"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2887
    .local p1, points:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v12

    .line 2888
    .local v12, points_mat:Lorg/opencv/core/Mat;
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, v12, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v4, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    iget-object v6, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x1

    aget-wide v6, v6, v7

    iget-object v8, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x2

    aget-wide v8, v8, v9

    iget-object v10, p2, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x3

    aget-wide v10, v10, v11

    invoke-static/range {v0 .. v11}, Lorg/opencv/core/Core;->fillConvexPoly_2(JJDDDD)V

    .line 2890
    return-void
.end method

.method public static fillConvexPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;I)V
    .locals 15
    .parameter "img"
    .parameter
    .parameter "color"
    .parameter "lineType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;",
            "Lorg/opencv/core/Scalar;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2863
    .local p1, points:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static/range {p1 .. p1}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v14

    .line 2864
    .local v14, points_mat:Lorg/opencv/core/Mat;
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v14, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v5, v5, v6

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v7, v7, v8

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v9, v9, v10

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v11, v11, v12

    move/from16 v13, p3

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->fillConvexPoly_1(JJDDDDI)V

    .line 2866
    return-void
.end method

.method public static fillConvexPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;II)V
    .locals 16
    .parameter "img"
    .parameter
    .parameter "color"
    .parameter "lineType"
    .parameter "shift"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;",
            "Lorg/opencv/core/Scalar;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 2838
    .local p1, points:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    invoke-static/range {p1 .. p1}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v15

    .line 2839
    .local v15, points_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v15, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v5, v5, v6

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v7, v7, v8

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v9, v9, v10

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v11, v11, v12

    move/from16 v13, p3

    move/from16 v14, p4

    invoke-static/range {v1 .. v14}, Lorg/opencv/core/Core;->fillConvexPoly_0(JJDDDDII)V

    .line 2841
    return-void
.end method

.method private static native fillConvexPoly_0(JJDDDDII)V
.end method

.method private static native fillConvexPoly_1(JJDDDDI)V
.end method

.method private static native fillConvexPoly_2(JJDDDD)V
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;)V
    .locals 15
    .parameter "img"
    .parameter
    .parameter "color"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2989
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v14, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    invoke-direct {v14, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2990
    .local v14, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v13

    .line 2991
    .local v13, pts_mat:Lorg/opencv/core/Mat;
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v13, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v5, v5, v6

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v7, v7, v8

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v9, v9, v10

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v11, v11, v12

    invoke-static/range {v1 .. v12}, Lorg/opencv/core/Core;->fillPoly_3(JJDDDD)V

    .line 2993
    return-void

    .line 2989
    .end local v13           #pts_mat:Lorg/opencv/core/Mat;
    .end local v14           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;I)V
    .locals 16
    .parameter "img"
    .parameter
    .parameter "color"
    .parameter "lineType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;",
            "Lorg/opencv/core/Scalar;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2966
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2967
    .local v15, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v14

    .line 2968
    .local v14, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v14, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v5, v5, v6

    move-object/from16 v0, p2

    iget-object v7, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v7, v7, v8

    move-object/from16 v0, p2

    iget-object v9, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v9, v9, v10

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v11, v11, v12

    move/from16 v13, p3

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->fillPoly_2(JJDDDDI)V

    .line 2970
    return-void

    .line 2966
    .end local v14           #pts_mat:Lorg/opencv/core/Mat;
    .end local v15           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;II)V
    .locals 18
    .parameter "img"
    .parameter
    .parameter "color"
    .parameter "lineType"
    .parameter "shift"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;",
            "Lorg/opencv/core/Scalar;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 2942
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v17, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2943
    .local v17, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v16

    .line 2944
    .local v16, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, v16

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    move-object/from16 v0, p2

    iget-object v8, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x1

    aget-wide v8, v8, v9

    move-object/from16 v0, p2

    iget-object v10, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x2

    aget-wide v10, v10, v11

    move-object/from16 v0, p2

    iget-object v12, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x3

    aget-wide v12, v12, v13

    move/from16 v14, p3

    move/from16 v15, p4

    invoke-static/range {v2 .. v15}, Lorg/opencv/core/Core;->fillPoly_1(JJDDDDII)V

    .line 2946
    return-void

    .line 2942
    .end local v16           #pts_mat:Lorg/opencv/core/Mat;
    .end local v17           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fillPoly(Lorg/opencv/core/Mat;Ljava/util/List;Lorg/opencv/core/Scalar;IILorg/opencv/core/Point;)V
    .locals 22
    .parameter "img"
    .parameter
    .parameter "color"
    .parameter "lineType"
    .parameter "shift"
    .parameter "offset"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;",
            "Lorg/opencv/core/Scalar;",
            "II",
            "Lorg/opencv/core/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2917
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v21, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2918
    .local v21, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v20

    .line 2919
    .local v20, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, v20

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x0

    aget-wide v6, v6, v7

    move-object/from16 v0, p2

    iget-object v8, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x1

    aget-wide v8, v8, v9

    move-object/from16 v0, p2

    iget-object v10, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x2

    aget-wide v10, v10, v11

    move-object/from16 v0, p2

    iget-object v12, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x3

    aget-wide v12, v12, v13

    move-object/from16 v0, p5

    iget-wide v0, v0, Lorg/opencv/core/Point;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p5

    iget-wide v0, v0, Lorg/opencv/core/Point;->y:D

    move-wide/from16 v18, v0

    move/from16 v14, p3

    move/from16 v15, p4

    invoke-static/range {v2 .. v19}, Lorg/opencv/core/Core;->fillPoly_0(JJDDDDIIDD)V

    .line 2921
    return-void

    .line 2917
    .end local v20           #pts_mat:Lorg/opencv/core/Mat;
    .end local v21           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static native fillPoly_0(JJDDDDIIDD)V
.end method

.method private static native fillPoly_1(JJDDDDII)V
.end method

.method private static native fillPoly_2(JJDDDDI)V
.end method

.method private static native fillPoly_3(JJDDDD)V
.end method

.method public static flip(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flipCode"

    .prologue
    .line 3042
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->flip_0(JJI)V

    .line 3044
    return-void
.end method

.method private static native flip_0(JJI)V
.end method

.method public static gemm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DLorg/opencv/core/Mat;)V
    .locals 13
    .parameter "src1"
    .parameter "src2"
    .parameter "alpha"
    .parameter "src3"
    .parameter "gamma"
    .parameter "dst"

    .prologue
    .line 3121
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p7

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v5, p2

    move-wide/from16 v9, p5

    invoke-static/range {v1 .. v12}, Lorg/opencv/core/Core;->gemm_1(JJDJDJ)V

    .line 3123
    return-void
.end method

.method public static gemm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;DLorg/opencv/core/Mat;I)V
    .locals 14
    .parameter "src1"
    .parameter "src2"
    .parameter "alpha"
    .parameter "src3"
    .parameter "gamma"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 3086
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p4

    iget-wide v7, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p7

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v5, p2

    move-wide/from16 v9, p5

    move/from16 v13, p8

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->gemm_0(JJDJDJI)V

    .line 3088
    return-void
.end method

.method private static native gemm_0(JJDJDJI)V
.end method

.method private static native gemm_1(JJDJDJ)V
.end method

.method public static getCPUTickCount()J
    .locals 2

    .prologue
    .line 3151
    invoke-static {}, Lorg/opencv/core/Core;->getCPUTickCount_0()J

    move-result-wide v0

    .line 3153
    .local v0, retVal:J
    return-wide v0
.end method

.method private static native getCPUTickCount_0()J
.end method

.method public static getNumberOfCPUs()I
    .locals 1

    .prologue
    .line 3164
    invoke-static {}, Lorg/opencv/core/Core;->getNumberOfCPUs_0()I

    move-result v0

    .line 3166
    .local v0, retVal:I
    return v0
.end method

.method private static native getNumberOfCPUs_0()I
.end method

.method public static getOptimalDFTSize(I)I
    .locals 1
    .parameter "vecsize"

    .prologue
    .line 3209
    invoke-static {p0}, Lorg/opencv/core/Core;->getOptimalDFTSize_0(I)I

    move-result v0

    .line 3211
    .local v0, retVal:I
    return v0
.end method

.method private static native getOptimalDFTSize_0(I)I
.end method

.method public static getTextSize(Ljava/lang/String;IDI[I)Lorg/opencv/core/Size;
    .locals 3
    .parameter "text"
    .parameter "fontFace"
    .parameter "fontScale"
    .parameter "thickness"
    .parameter "baseLine"

    .prologue
    .line 6709
    if-eqz p5, :cond_0

    array-length v1, p5

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 6710
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'baseLine\' must be \'int[1]\' or \'null\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 6711
    :cond_0
    new-instance v0, Lorg/opencv/core/Size;

    invoke-static/range {p0 .. p5}, Lorg/opencv/core/Core;->n_getTextSize(Ljava/lang/String;IDI[I)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Size;-><init>([D)V

    .line 6712
    .local v0, retVal:Lorg/opencv/core/Size;
    return-object v0
.end method

.method public static getTickCount()J
    .locals 2

    .prologue
    .line 3233
    invoke-static {}, Lorg/opencv/core/Core;->getTickCount_0()J

    move-result-wide v0

    .line 3235
    .local v0, retVal:J
    return-wide v0
.end method

.method private static native getTickCount_0()J
.end method

.method public static getTickFrequency()D
    .locals 2

    .prologue
    .line 3254
    invoke-static {}, Lorg/opencv/core/Core;->getTickFrequency_0()D

    move-result-wide v0

    .line 3256
    .local v0, retVal:D
    return-wide v0
.end method

.method private static native getTickFrequency_0()D
.end method

.method public static hconcat(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 5
    .parameter
    .parameter "dst"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3266
    .local p0, src:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 3267
    .local v0, src_mat:Lorg/opencv/core/Mat;
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->hconcat_0(JJ)V

    .line 3269
    return-void
.end method

.method private static native hconcat_0(JJ)V
.end method

.method public static idct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 3319
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->idct_1(JJ)V

    .line 3321
    return-void
.end method

.method public static idct(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 3296
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->idct_0(JJI)V

    .line 3298
    return-void
.end method

.method private static native idct_0(JJI)V
.end method

.method private static native idct_1(JJ)V
.end method

.method public static idft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 3418
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->idft_2(JJ)V

    .line 3420
    return-void
.end method

.method public static idft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 3388
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->idft_1(JJI)V

    .line 3390
    return-void
.end method

.method public static idft(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "flags"
    .parameter "nonzeroRows"

    .prologue
    .line 3357
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->idft_0(JJII)V

    .line 3359
    return-void
.end method

.method private static native idft_0(JJII)V
.end method

.method private static native idft_1(JJI)V
.end method

.method private static native idft_2(JJ)V
.end method

.method public static inRange(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;Lorg/opencv/core/Scalar;Lorg/opencv/core/Mat;)V
    .locals 22
    .parameter "src"
    .parameter "lowerb"
    .parameter "upperb"
    .parameter "dst"

    .prologue
    .line 3459
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x1

    aget-wide v6, v6, v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x2

    aget-wide v8, v8, v9

    move-object/from16 v0, p1

    iget-object v10, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x3

    aget-wide v10, v10, v11

    move-object/from16 v0, p2

    iget-object v12, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x0

    aget-wide v12, v12, v13

    move-object/from16 v0, p2

    iget-object v14, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v15, 0x1

    aget-wide v14, v14, v15

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v16, v0

    const/16 v17, 0x2

    aget-wide v16, v16, v17

    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-wide v18, v18, v19

    move-object/from16 v0, p3

    iget-wide v0, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide/from16 v20, v0

    invoke-static/range {v2 .. v21}, Lorg/opencv/core/Core;->inRange_0(JDDDDDDDDJ)V

    .line 3461
    return-void
.end method

.method private static native inRange_0(JDDDDDDDDJ)V
.end method

.method public static insertChannel(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "coi"

    .prologue
    .line 3472
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->insertChannel_0(JJI)V

    .line 3474
    return-void
.end method

.method private static native insertChannel_0(JJI)V
.end method

.method public static invert(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 6
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 3558
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lorg/opencv/core/Core;->invert_1(JJ)D

    move-result-wide v0

    .line 3560
    .local v0, retVal:D
    return-wide v0
.end method

.method public static invert(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 3520
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5, p2}, Lorg/opencv/core/Core;->invert_0(JJI)D

    move-result-wide v0

    .line 3522
    .local v0, retVal:D
    return-wide v0
.end method

.method private static native invert_0(JJI)D
.end method

.method private static native invert_1(JJ)D
.end method

.method public static kmeans(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/TermCriteria;II)D
    .locals 14
    .parameter "data"
    .parameter "K"
    .parameter "bestLabels"
    .parameter "criteria"
    .parameter "attempts"
    .parameter "flags"

    .prologue
    .line 3663
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget v6, v0, Lorg/opencv/core/TermCriteria;->type:I

    move-object/from16 v0, p3

    iget v7, v0, Lorg/opencv/core/TermCriteria;->maxCount:I

    move-object/from16 v0, p3

    iget-wide v8, v0, Lorg/opencv/core/TermCriteria;->epsilon:D

    move v3, p1

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v1 .. v11}, Lorg/opencv/core/Core;->kmeans_1(JIJIIDII)D

    move-result-wide v12

    .line 3665
    .local v12, retVal:D
    return-wide v12
.end method

.method public static kmeans(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;Lorg/opencv/core/TermCriteria;IILorg/opencv/core/Mat;)D
    .locals 16
    .parameter "data"
    .parameter "K"
    .parameter "bestLabels"
    .parameter "criteria"
    .parameter "attempts"
    .parameter "flags"
    .parameter "centers"

    .prologue
    .line 3614
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget v6, v0, Lorg/opencv/core/TermCriteria;->type:I

    move-object/from16 v0, p3

    iget v7, v0, Lorg/opencv/core/TermCriteria;->maxCount:I

    move-object/from16 v0, p3

    iget-wide v8, v0, Lorg/opencv/core/TermCriteria;->epsilon:D

    move-object/from16 v0, p6

    iget-wide v12, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move/from16 v3, p1

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->kmeans_0(JIJIIDIIJ)D

    move-result-wide v14

    .line 3616
    .local v14, retVal:D
    return-wide v14
.end method

.method private static native kmeans_0(JIJIIDIIJ)D
.end method

.method private static native kmeans_1(JIJIIDII)D
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 19
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"

    .prologue
    .line 3780
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    invoke-static/range {v1 .. v18}, Lorg/opencv/core/Core;->line_3(JDDDDDDDD)V

    .line 3782
    return-void
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V
    .locals 20
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 3755
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    invoke-static/range {v1 .. v19}, Lorg/opencv/core/Core;->line_2(JDDDDDDDDI)V

    .line 3757
    return-void
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;II)V
    .locals 21
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"

    .prologue
    .line 3729
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    move/from16 v20, p5

    invoke-static/range {v1 .. v20}, Lorg/opencv/core/Core;->line_1(JDDDDDDDDII)V

    .line 3731
    return-void
.end method

.method public static line(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;III)V
    .locals 22
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .parameter "shift"

    .prologue
    .line 3699
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    move/from16 v20, p5

    move/from16 v21, p6

    invoke-static/range {v1 .. v21}, Lorg/opencv/core/Core;->line_0(JDDDDDDDDIII)V

    .line 3701
    return-void
.end method

.method private static native line_0(JDDDDDDDDIII)V
.end method

.method private static native line_1(JDDDDDDDDII)V
.end method

.method private static native line_2(JDDDDDDDDI)V
.end method

.method private static native line_3(JDDDDDDDD)V
.end method

.method public static log(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 3819
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->log_0(JJ)V

    .line 3821
    return-void
.end method

.method private static native log_0(JJ)V
.end method

.method public static magnitude(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "magnitude"

    .prologue
    .line 3851
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->magnitude_0(JJJ)V

    .line 3853
    return-void
.end method

.method private static native magnitude_0(JJJ)V
.end method

.method public static max(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 3893
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->max_0(JJJ)V

    .line 3895
    return-void
.end method

.method private static native max_0(JJJ)V
.end method

.method public static mean(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3
    .parameter "src"

    .prologue
    .line 3955
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->mean_1(J)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Scalar;-><init>([D)V

    .line 3957
    .local v0, retVal:Lorg/opencv/core/Scalar;
    return-object v0
.end method

.method public static mean(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 5
    .parameter "src"
    .parameter "mask"

    .prologue
    .line 3927
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->mean_0(JJ)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Scalar;-><init>([D)V

    .line 3929
    .local v0, retVal:Lorg/opencv/core/Scalar;
    return-object v0
.end method

.method public static meanStdDev(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "mean"
    .parameter "stddev"

    .prologue
    .line 4039
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->meanStdDev_1(JJJ)V

    .line 4041
    return-void
.end method

.method public static meanStdDev(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src"
    .parameter "mean"
    .parameter "stddev"
    .parameter "mask"

    .prologue
    .line 4000
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->meanStdDev_0(JJJJ)V

    .line 4002
    return-void
.end method

.method private static native meanStdDev_0(JJJJ)V
.end method

.method private static native meanStdDev_1(JJJ)V
.end method

.method private static native mean_0(JJ)[D
.end method

.method private static native mean_1(J)[D
.end method

.method public static merge(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 5
    .parameter
    .parameter "dst"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4073
    .local p0, mv:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 4074
    .local v0, mv_mat:Lorg/opencv/core/Mat;
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->merge_0(JJ)V

    .line 4076
    return-void
.end method

.method private static native merge_0(JJ)V
.end method

.method public static min(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 4116
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->min_0(JJJ)V

    .line 4118
    return-void
.end method

.method public static minMaxLoc(Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;
    .locals 1
    .parameter "src"

    .prologue
    .line 6685
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/opencv/core/Core;->minMaxLoc(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;

    move-result-object v0

    return-object v0
.end method

.method public static minMaxLoc(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Core$MinMaxLocResult;
    .locals 7
    .parameter "src"
    .parameter "mask"

    .prologue
    .line 6642
    new-instance v2, Lorg/opencv/core/Core$MinMaxLocResult;

    invoke-direct {v2}, Lorg/opencv/core/Core$MinMaxLocResult;-><init>()V

    .line 6643
    .local v2, res:Lorg/opencv/core/Core$MinMaxLocResult;
    const-wide/16 v0, 0x0

    .line 6644
    .local v0, maskNativeObj:J
    if-eqz p1, :cond_0

    .line 6645
    iget-wide v0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    .line 6647
    :cond_0
    iget-wide v4, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v4, v5, v0, v1}, Lorg/opencv/core/Core;->n_minMaxLocManual(JJ)[D

    move-result-object v3

    .line 6648
    .local v3, resarr:[D
    const/4 v4, 0x0

    aget-wide v4, v3, v4

    iput-wide v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->minVal:D

    .line 6649
    const/4 v4, 0x1

    aget-wide v4, v3, v4

    iput-wide v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->maxVal:D

    .line 6650
    iget-object v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->minLoc:Lorg/opencv/core/Point;

    const/4 v5, 0x2

    aget-wide v5, v3, v5

    iput-wide v5, v4, Lorg/opencv/core/Point;->x:D

    .line 6651
    iget-object v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->minLoc:Lorg/opencv/core/Point;

    const/4 v5, 0x3

    aget-wide v5, v3, v5

    iput-wide v5, v4, Lorg/opencv/core/Point;->y:D

    .line 6652
    iget-object v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->maxLoc:Lorg/opencv/core/Point;

    const/4 v5, 0x4

    aget-wide v5, v3, v5

    iput-wide v5, v4, Lorg/opencv/core/Point;->x:D

    .line 6653
    iget-object v4, v2, Lorg/opencv/core/Core$MinMaxLocResult;->maxLoc:Lorg/opencv/core/Point;

    const/4 v5, 0x5

    aget-wide v5, v3, v5

    iput-wide v5, v4, Lorg/opencv/core/Point;->y:D

    .line 6654
    return-object v2
.end method

.method private static native min_0(JJJ)V
.end method

.method public static mixChannels(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4164
    .local p0, src:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    .local p1, dst:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    .local p2, fromTo:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v8

    .line 4165
    .local v8, src_mat:Lorg/opencv/core/Mat;
    invoke-static {p1}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v6

    .line 4166
    .local v6, dst_mat:Lorg/opencv/core/Mat;
    invoke-static {p2}, Lorg/opencv/utils/Converters;->vector_int_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v7

    .line 4167
    .local v7, fromTo_mat:Lorg/opencv/core/Mat;
    iget-wide v0, v8, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, v6, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v7, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->mixChannels_0(JJJ)V

    .line 4169
    return-void
.end method

.method private static native mixChannels_0(JJJ)V
.end method

.method public static mulSpectrums(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 7
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "flags"

    .prologue
    .line 4233
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->mulSpectrums_1(JJJI)V

    .line 4235
    return-void
.end method

.method public static mulSpectrums(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;IZ)V
    .locals 8
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "flags"
    .parameter "conjB"

    .prologue
    .line 4203
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->mulSpectrums_0(JJJIZ)V

    .line 4205
    return-void
.end method

.method private static native mulSpectrums_0(JJJIZ)V
.end method

.method private static native mulSpectrums_1(JJJI)V
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "aTa"

    .prologue
    .line 4406
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->mulTransposed_3(JJZ)V

    .line 4408
    return-void
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;)V
    .locals 7
    .parameter "src"
    .parameter "dst"
    .parameter "aTa"
    .parameter "delta"

    .prologue
    .line 4370
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->mulTransposed_2(JJZJ)V

    .line 4372
    return-void
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;D)V
    .locals 9
    .parameter "src"
    .parameter "dst"
    .parameter "aTa"
    .parameter "delta"
    .parameter "scale"

    .prologue
    .line 4327
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v7, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->mulTransposed_1(JJZJD)V

    .line 4329
    return-void
.end method

.method public static mulTransposed(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ZLorg/opencv/core/Mat;DI)V
    .locals 10
    .parameter "src"
    .parameter "dst"
    .parameter "aTa"
    .parameter "delta"
    .parameter "scale"
    .parameter "dtype"

    .prologue
    .line 4283
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v7, p4

    move/from16 v9, p6

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->mulTransposed_0(JJZJDI)V

    .line 4285
    return-void
.end method

.method private static native mulTransposed_0(JJZJDI)V
.end method

.method private static native mulTransposed_1(JJZJD)V
.end method

.method private static native mulTransposed_2(JJZJ)V
.end method

.method private static native mulTransposed_3(JJZ)V
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 4522
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->multiply_2(JJJ)V

    .line 4524
    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "scale"

    .prologue
    .line 4486
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->multiply_1(JJJD)V

    .line 4488
    return-void
.end method

.method public static multiply(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DI)V
    .locals 9
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "scale"
    .parameter "dtype"

    .prologue
    .line 4449
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v6, p3

    move v8, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->multiply_0(JJJDI)V

    .line 4451
    return-void
.end method

.method private static native multiply_0(JJJDI)V
.end method

.method private static native multiply_1(JJJD)V
.end method

.method private static native multiply_2(JJJ)V
.end method

.method private static native n_getTextSize(Ljava/lang/String;IDI[I)[D
.end method

.method private static native n_minMaxLocManual(JJ)[D
.end method

.method public static norm(Lorg/opencv/core/Mat;)D
    .locals 4
    .parameter "src1"

    .prologue
    .line 4681
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Core;->norm_2(J)D

    move-result-wide v0

    .line 4683
    .local v0, retVal:D
    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;I)D
    .locals 4
    .parameter "src1"
    .parameter "normType"

    .prologue
    .line 4631
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, p1}, Lorg/opencv/core/Core;->norm_1(JI)D

    move-result-wide v0

    .line 4633
    .local v0, retVal:D
    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)D
    .locals 6
    .parameter "src1"
    .parameter "normType"
    .parameter "mask"

    .prologue
    .line 4580
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, p1, v4, v5}, Lorg/opencv/core/Core;->norm_0(JIJ)D

    move-result-wide v0

    .line 4582
    .local v0, retVal:D
    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 6
    .parameter "src1"
    .parameter "src2"

    .prologue
    .line 4843
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lorg/opencv/core/Core;->norm_5(JJ)D

    move-result-wide v0

    .line 4845
    .local v0, retVal:D
    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "normType"

    .prologue
    .line 4792
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5, p2}, Lorg/opencv/core/Core;->norm_4(JJI)D

    move-result-wide v0

    .line 4794
    .local v0, retVal:D
    return-wide v0
.end method

.method public static norm(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;ILorg/opencv/core/Mat;)D
    .locals 9
    .parameter "src1"
    .parameter "src2"
    .parameter "normType"
    .parameter "mask"

    .prologue
    .line 4740
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v5, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->norm_3(JJIJ)D

    move-result-wide v7

    .line 4742
    .local v7, retVal:D
    return-wide v7
.end method

.method private static native norm_0(JIJ)D
.end method

.method private static native norm_1(JI)D
.end method

.method private static native norm_2(J)D
.end method

.method private static native norm_3(JJIJ)D
.end method

.method private static native norm_4(JJI)D
.end method

.method private static native norm_5(JJ)D
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 5106
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->normalize_5(JJ)V

    .line 5108
    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;D)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"

    .prologue
    .line 5067
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->normalize_4(JJD)V

    .line 5069
    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DD)V
    .locals 8
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"
    .parameter "beta"

    .prologue
    .line 5026
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->normalize_3(JJDD)V

    .line 5028
    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDI)V
    .locals 9
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"
    .parameter "beta"
    .parameter "norm_type"

    .prologue
    .line 4983
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move v8, p6

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->normalize_2(JJDDI)V

    .line 4985
    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDII)V
    .locals 10
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"
    .parameter "beta"
    .parameter "norm_type"
    .parameter "dtype"

    .prologue
    .line 4939
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    move-wide v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->normalize_1(JJDDII)V

    .line 4941
    return-void
.end method

.method public static normalize(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;DDIILorg/opencv/core/Mat;)V
    .locals 13
    .parameter "src"
    .parameter "dst"
    .parameter "alpha"
    .parameter "beta"
    .parameter "norm_type"
    .parameter "dtype"
    .parameter "mask"

    .prologue
    .line 4894
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p8

    iget-wide v11, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v5, p2

    move-wide/from16 v7, p4

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-static/range {v1 .. v12}, Lorg/opencv/core/Core;->normalize_0(JJDDIIJ)V

    .line 4896
    return-void
.end method

.method private static native normalize_0(JJDDIIJ)V
.end method

.method private static native normalize_1(JJDDII)V
.end method

.method private static native normalize_2(JJDDI)V
.end method

.method private static native normalize_3(JJDD)V
.end method

.method private static native normalize_4(JJD)V
.end method

.method private static native normalize_5(JJ)V
.end method

.method public static perspectiveTransform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "m"

    .prologue
    .line 5155
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->perspectiveTransform_0(JJJ)V

    .line 5157
    return-void
.end method

.method private static native perspectiveTransform_0(JJJ)V
.end method

.method public static phase(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "x"
    .parameter "y"
    .parameter "angle"

    .prologue
    .line 5216
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->phase_1(JJJ)V

    .line 5218
    return-void
.end method

.method public static phase(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 7
    .parameter "x"
    .parameter "y"
    .parameter "angle"
    .parameter "angleInDegrees"

    .prologue
    .line 5189
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->phase_0(JJJZ)V

    .line 5191
    return-void
.end method

.method private static native phase_0(JJJZ)V
.end method

.method private static native phase_1(JJJ)V
.end method

.method public static polarToCart(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "magnitude"
    .parameter "angle"
    .parameter "x"
    .parameter "y"

    .prologue
    .line 5301
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->polarToCart_1(JJJJ)V

    .line 5303
    return-void
.end method

.method public static polarToCart(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Z)V
    .locals 9
    .parameter "magnitude"
    .parameter "angle"
    .parameter "x"
    .parameter "y"
    .parameter "angleInDegrees"

    .prologue
    .line 5262
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->polarToCart_0(JJJJZ)V

    .line 5264
    return-void
.end method

.method private static native polarToCart_0(JJJJZ)V
.end method

.method private static native polarToCart_1(JJJJ)V
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;)V
    .locals 16
    .parameter "img"
    .parameter
    .parameter "isClosed"
    .parameter "color"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;Z",
            "Lorg/opencv/core/Scalar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 5402
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v15, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 5403
    .local v15, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v14

    .line 5404
    .local v14, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v14, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v6, v5, v6

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v8, v5, v8

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v10, v5, v10

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v12, v5, v12

    move/from16 v5, p2

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Core;->polylines_3(JJZDDDD)V

    .line 5406
    return-void

    .line 5402
    .end local v14           #pts_mat:Lorg/opencv/core/Mat;
    .end local v15           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;I)V
    .locals 18
    .parameter "img"
    .parameter
    .parameter "isClosed"
    .parameter "color"
    .parameter "thickness"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;Z",
            "Lorg/opencv/core/Scalar;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 5379
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v17, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 5380
    .local v17, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v16

    .line 5381
    .local v16, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, v16

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x0

    aget-wide v7, v6, v7

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x1

    aget-wide v9, v6, v9

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x2

    aget-wide v11, v6, v11

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x3

    aget-wide v13, v6, v13

    move/from16 v6, p2

    move/from16 v15, p4

    invoke-static/range {v2 .. v15}, Lorg/opencv/core/Core;->polylines_2(JJZDDDDI)V

    .line 5383
    return-void

    .line 5379
    .end local v16           #pts_mat:Lorg/opencv/core/Mat;
    .end local v17           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;II)V
    .locals 19
    .parameter "img"
    .parameter
    .parameter "isClosed"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;Z",
            "Lorg/opencv/core/Scalar;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 5355
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v18, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 5356
    .local v18, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v17

    .line 5357
    .local v17, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, v17

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x0

    aget-wide v7, v6, v7

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x1

    aget-wide v9, v6, v9

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x2

    aget-wide v11, v6, v11

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x3

    aget-wide v13, v6, v13

    move/from16 v6, p2

    move/from16 v15, p4

    move/from16 v16, p5

    invoke-static/range {v2 .. v16}, Lorg/opencv/core/Core;->polylines_1(JJZDDDDII)V

    .line 5359
    return-void

    .line 5355
    .end local v17           #pts_mat:Lorg/opencv/core/Mat;
    .end local v18           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static polylines(Lorg/opencv/core/Mat;Ljava/util/List;ZLorg/opencv/core/Scalar;III)V
    .locals 20
    .parameter "img"
    .parameter
    .parameter "isClosed"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .parameter "shift"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;>;Z",
            "Lorg/opencv/core/Scalar;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 5330
    .local p1, pts:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lorg/opencv/core/Point;>;>;"
    new-instance v19, Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 5331
    .local v19, pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lorg/opencv/utils/Converters;->vector_vector_Point_to_Mat(Ljava/util/List;Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v18

    .line 5332
    .local v18, pts_mat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, v18

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x0

    aget-wide v7, v6, v7

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x1

    aget-wide v9, v6, v9

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v11, 0x2

    aget-wide v11, v6, v11

    move-object/from16 v0, p3

    iget-object v6, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v13, 0x3

    aget-wide v13, v6, v13

    move/from16 v6, p2

    move/from16 v15, p4

    move/from16 v16, p5

    move/from16 v17, p6

    invoke-static/range {v2 .. v17}, Lorg/opencv/core/Core;->polylines_0(JJZDDDDIII)V

    .line 5334
    return-void

    .line 5330
    .end local v18           #pts_mat:Lorg/opencv/core/Mat;
    .end local v19           #pts_tmplm:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static native polylines_0(JJZDDDDIII)V
.end method

.method private static native polylines_1(JJZDDDDII)V
.end method

.method private static native polylines_2(JJZDDDDI)V
.end method

.method private static native polylines_3(JJZDDDD)V
.end method

.method public static pow(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "power"
    .parameter "dst"

    .prologue
    .line 5443
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->pow_0(JDJ)V

    .line 5445
    return-void
.end method

.method private static native pow_0(JDJ)V
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;)V
    .locals 19
    .parameter "img"
    .parameter "text"
    .parameter "org"
    .parameter "fontFace"
    .parameter "fontScale"
    .parameter "color"

    .prologue
    .line 5571
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v11, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v13, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v15, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x3

    aget-wide v17, v3, v8

    move-object/from16 v3, p1

    move/from16 v8, p3

    move-wide/from16 v9, p4

    invoke-static/range {v1 .. v18}, Lorg/opencv/core/Core;->putText_3(JLjava/lang/String;DDIDDDDD)V

    .line 5573
    return-void
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;I)V
    .locals 20
    .parameter "img"
    .parameter "text"
    .parameter "org"
    .parameter "fontFace"
    .parameter "fontScale"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 5542
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v11, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v13, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v15, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x3

    aget-wide v17, v3, v8

    move-object/from16 v3, p1

    move/from16 v8, p3

    move-wide/from16 v9, p4

    move/from16 v19, p7

    invoke-static/range {v1 .. v19}, Lorg/opencv/core/Core;->putText_2(JLjava/lang/String;DDIDDDDDI)V

    .line 5544
    return-void
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;II)V
    .locals 21
    .parameter "img"
    .parameter "text"
    .parameter "org"
    .parameter "fontFace"
    .parameter "fontScale"
    .parameter "color"
    .parameter "thickness"
    .parameter "linetype"

    .prologue
    .line 5512
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v11, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v13, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v15, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x3

    aget-wide v17, v3, v8

    move-object/from16 v3, p1

    move/from16 v8, p3

    move-wide/from16 v9, p4

    move/from16 v19, p7

    move/from16 v20, p8

    invoke-static/range {v1 .. v20}, Lorg/opencv/core/Core;->putText_1(JLjava/lang/String;DDIDDDDDII)V

    .line 5514
    return-void
.end method

.method public static putText(Lorg/opencv/core/Mat;Ljava/lang/String;Lorg/opencv/core/Point;IDLorg/opencv/core/Scalar;IIZ)V
    .locals 22
    .parameter "img"
    .parameter "text"
    .parameter "org"
    .parameter "fontFace"
    .parameter "fontScale"
    .parameter "color"
    .parameter "thickness"
    .parameter "linetype"
    .parameter "bottomLeftOrigin"

    .prologue
    .line 5481
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p2

    iget-wide v4, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x0

    aget-wide v11, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v13, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x2

    aget-wide v15, v3, v8

    move-object/from16 v0, p6

    iget-object v3, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x3

    aget-wide v17, v3, v8

    move-object/from16 v3, p1

    move/from16 v8, p3

    move-wide/from16 v9, p4

    move/from16 v19, p7

    move/from16 v20, p8

    move/from16 v21, p9

    invoke-static/range {v1 .. v21}, Lorg/opencv/core/Core;->putText_0(JLjava/lang/String;DDIDDDDDIIZ)V

    .line 5483
    return-void
.end method

.method private static native putText_0(JLjava/lang/String;DDIDDDDDIIZ)V
.end method

.method private static native putText_1(JLjava/lang/String;DDIDDDDDII)V
.end method

.method private static native putText_2(JLjava/lang/String;DDIDDDDDI)V
.end method

.method private static native putText_3(JLjava/lang/String;DDIDDDDD)V
.end method

.method public static randShuffle(Lorg/opencv/core/Mat;)V
    .locals 2
    .parameter "dst"

    .prologue
    .line 5592
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->randShuffle_1(J)V

    .line 5594
    return-void
.end method

.method public static randShuffle(Lorg/opencv/core/Mat;D)V
    .locals 2
    .parameter "dst"
    .parameter "iterFactor"

    .prologue
    .line 5584
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, p2}, Lorg/opencv/core/Core;->randShuffle_0(JD)V

    .line 5586
    return-void
.end method

.method private static native randShuffle_0(JD)V
.end method

.method private static native randShuffle_1(J)V
.end method

.method public static randn(Lorg/opencv/core/Mat;DD)V
    .locals 6
    .parameter "dst"
    .parameter "mean"
    .parameter "stddev"

    .prologue
    .line 5623
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->randn_0(JDD)V

    .line 5625
    return-void
.end method

.method private static native randn_0(JDD)V
.end method

.method public static randu(Lorg/opencv/core/Mat;DD)V
    .locals 6
    .parameter "dst"
    .parameter "low"
    .parameter "high"

    .prologue
    .line 5658
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->randu_0(JDD)V

    .line 5660
    return-void
.end method

.method private static native randu_0(JDD)V
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;)V
    .locals 19
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"

    .prologue
    .line 5763
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    invoke-static/range {v1 .. v18}, Lorg/opencv/core/Core;->rectangle_3(JDDDDDDDD)V

    .line 5765
    return-void
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;I)V
    .locals 20
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"

    .prologue
    .line 5741
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    invoke-static/range {v1 .. v19}, Lorg/opencv/core/Core;->rectangle_2(JDDDDDDDDI)V

    .line 5743
    return-void
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;II)V
    .locals 21
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"

    .prologue
    .line 5716
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    move/from16 v20, p5

    invoke-static/range {v1 .. v20}, Lorg/opencv/core/Core;->rectangle_1(JDDDDDDDDII)V

    .line 5718
    return-void
.end method

.method public static rectangle(Lorg/opencv/core/Mat;Lorg/opencv/core/Point;Lorg/opencv/core/Point;Lorg/opencv/core/Scalar;III)V
    .locals 22
    .parameter "img"
    .parameter "pt1"
    .parameter "pt2"
    .parameter "color"
    .parameter "thickness"
    .parameter "lineType"
    .parameter "shift"

    .prologue
    .line 5690
    move-object/from16 v0, p0

    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    move-object/from16 v0, p1

    iget-wide v3, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p2

    iget-wide v7, v0, Lorg/opencv/core/Point;->x:D

    move-object/from16 v0, p2

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    move-object/from16 v0, p3

    iget-object v11, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x0

    aget-wide v11, v11, v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v14, 0x1

    aget-wide v13, v13, v14

    move-object/from16 v0, p3

    iget-object v15, v0, Lorg/opencv/core/Scalar;->val:[D

    const/16 v16, 0x2

    aget-wide v15, v15, v16

    move-object/from16 v0, p3

    iget-object v0, v0, Lorg/opencv/core/Scalar;->val:[D

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-wide v17, v17, v18

    move/from16 v19, p4

    move/from16 v20, p5

    move/from16 v21, p6

    invoke-static/range {v1 .. v21}, Lorg/opencv/core/Core;->rectangle_0(JDDDDDDDDIII)V

    .line 5692
    return-void
.end method

.method private static native rectangle_0(JDDDDDDDDIII)V
.end method

.method private static native rectangle_1(JDDDDDDDDII)V
.end method

.method private static native rectangle_2(JDDDDDDDDI)V
.end method

.method private static native rectangle_3(JDDDDDDDD)V
.end method

.method public static reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;II)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "dim"
    .parameter "rtype"

    .prologue
    .line 5829
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->reduce_1(JJII)V

    .line 5831
    return-void
.end method

.method public static reduce(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;III)V
    .locals 7
    .parameter "src"
    .parameter "dst"
    .parameter "dim"
    .parameter "rtype"
    .parameter "dtype"

    .prologue
    .line 5800
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->reduce_0(JJIII)V

    .line 5802
    return-void
.end method

.method private static native reduce_0(JJIII)V
.end method

.method private static native reduce_1(JJII)V
.end method

.method public static repeat(Lorg/opencv/core/Mat;IILorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "ny"
    .parameter "nx"
    .parameter "dst"

    .prologue
    .line 5864
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->repeat_0(JIIJ)V

    .line 5866
    return-void
.end method

.method private static native repeat_0(JIIJ)V
.end method

.method public static scaleAdd(Lorg/opencv/core/Mat;DLorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "alpha"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 5901
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p3, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p4, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v2, p1

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->scaleAdd_0(JDJJ)V

    .line 5903
    return-void
.end method

.method private static native scaleAdd_0(JDJJ)V
.end method

.method public static setIdentity(Lorg/opencv/core/Mat;)V
    .locals 2
    .parameter "mtx"

    .prologue
    .line 5961
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Core;->setIdentity_1(J)V

    .line 5963
    return-void
.end method

.method public static setIdentity(Lorg/opencv/core/Mat;Lorg/opencv/core/Scalar;)V
    .locals 10
    .parameter "mtx"
    .parameter "s"

    .prologue
    .line 5934
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v2, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    iget-object v4, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    iget-object v6, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x2

    aget-wide v6, v6, v7

    iget-object v8, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x3

    aget-wide v8, v8, v9

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Core;->setIdentity_0(JDDDD)V

    .line 5936
    return-void
.end method

.method private static native setIdentity_0(JDDDD)V
.end method

.method private static native setIdentity_1(J)V
.end method

.method public static solve(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Z
    .locals 7
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 6050
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->solve_1(JJJ)Z

    move-result v6

    .line 6052
    .local v6, retVal:Z
    return v6
.end method

.method public static solve(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)Z
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 6015
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    move v6, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Core;->solve_0(JJJI)Z

    move-result v7

    .line 6017
    .local v7, retVal:Z
    return v7
.end method

.method public static solveCubic(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)I
    .locals 5
    .parameter "coeffs"
    .parameter "roots"

    .prologue
    .line 6082
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->solveCubic_0(JJ)I

    move-result v0

    .line 6084
    .local v0, retVal:I
    return v0
.end method

.method private static native solveCubic_0(JJ)I
.end method

.method public static solvePoly(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)D
    .locals 6
    .parameter "coeffs"
    .parameter "roots"

    .prologue
    .line 6130
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lorg/opencv/core/Core;->solvePoly_1(JJ)D

    move-result-wide v0

    .line 6132
    .local v0, retVal:D
    return-wide v0
.end method

.method public static solvePoly(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)D
    .locals 6
    .parameter "coeffs"
    .parameter "roots"
    .parameter "maxIters"

    .prologue
    .line 6109
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5, p2}, Lorg/opencv/core/Core;->solvePoly_0(JJI)D

    move-result-wide v0

    .line 6111
    .local v0, retVal:D
    return-wide v0
.end method

.method private static native solvePoly_0(JJI)D
.end method

.method private static native solvePoly_1(JJ)D
.end method

.method private static native solve_0(JJJI)Z
.end method

.method private static native solve_1(JJJ)Z
.end method

.method public static sort(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 6166
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->sort_0(JJI)V

    .line 6168
    return-void
.end method

.method public static sortIdx(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "src"
    .parameter "dst"
    .parameter "flags"

    .prologue
    .line 6202
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Core;->sortIdx_0(JJI)V

    .line 6204
    return-void
.end method

.method private static native sortIdx_0(JJI)V
.end method

.method private static native sort_0(JJI)V
.end method

.method public static split(Lorg/opencv/core/Mat;Ljava/util/List;)V
    .locals 5
    .parameter "m"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/opencv/core/Mat;",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6235
    .local p1, mv:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-direct {v0}, Lorg/opencv/core/Mat;-><init>()V

    .line 6236
    .local v0, mv_mat:Lorg/opencv/core/Mat;
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->split_0(JJ)V

    .line 6237
    invoke-static {v0, p1}, Lorg/opencv/utils/Converters;->Mat_to_vector_Mat(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 6238
    return-void
.end method

.method private static native split_0(JJ)V
.end method

.method public static sqrt(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 6263
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->sqrt_0(JJ)V

    .line 6265
    return-void
.end method

.method private static native sqrt_0(JJ)V
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"

    .prologue
    .line 6447
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->subtract_2(JJJ)V

    .line 6449
    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 8
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"

    .prologue
    .line 6389
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v7}, Lorg/opencv/core/Core;->subtract_1(JJJJ)V

    .line 6391
    return-void
.end method

.method public static subtract(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;I)V
    .locals 9
    .parameter "src1"
    .parameter "src2"
    .parameter "dst"
    .parameter "mask"
    .parameter "dtype"

    .prologue
    .line 6329
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v6, p3, Lorg/opencv/core/Mat;->nativeObj:J

    move v8, p4

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Core;->subtract_0(JJJJI)V

    .line 6331
    return-void
.end method

.method private static native subtract_0(JJJJI)V
.end method

.method private static native subtract_1(JJJJ)V
.end method

.method private static native subtract_2(JJJ)V
.end method

.method public static sumElems(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3
    .parameter "src"

    .prologue
    .line 6476
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->sumElems_0(J)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Scalar;-><init>([D)V

    .line 6478
    .local v0, retVal:Lorg/opencv/core/Scalar;
    return-object v0
.end method

.method private static native sumElems_0(J)[D
.end method

.method public static trace(Lorg/opencv/core/Mat;)Lorg/opencv/core/Scalar;
    .locals 3
    .parameter "mtx"

    .prologue
    .line 6501
    new-instance v0, Lorg/opencv/core/Scalar;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Core;->trace_0(J)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Scalar;-><init>([D)V

    .line 6503
    .local v0, retVal:Lorg/opencv/core/Scalar;
    return-object v0
.end method

.method private static native trace_0(J)[D
.end method

.method public static transform(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "src"
    .parameter "dst"
    .parameter "m"

    .prologue
    .line 6550
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Core;->transform_0(JJJ)V

    .line 6552
    return-void
.end method

.method private static native transform_0(JJJ)V
.end method

.method public static transpose(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "src"
    .parameter "dst"

    .prologue
    .line 6578
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Core;->transpose_0(JJ)V

    .line 6580
    return-void
.end method

.method private static native transpose_0(JJ)V
.end method

.method public static vconcat(Ljava/util/List;Lorg/opencv/core/Mat;)V
    .locals 5
    .parameter
    .parameter "dst"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Mat;",
            ">;",
            "Lorg/opencv/core/Mat;",
            ")V"
        }
    .end annotation

    .prologue
    .line 6590
    .local p0, src:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Mat;>;"
    invoke-static {p0}, Lorg/opencv/utils/Converters;->vector_Mat_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 6591
    .local v0, src_mat:Lorg/opencv/core/Mat;
    iget-wide v1, v0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Core;->vconcat_0(JJ)V

    .line 6593
    return-void
.end method

.method private static native vconcat_0(JJ)V
.end method
