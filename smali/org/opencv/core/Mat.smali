.class public Lorg/opencv/core/Mat;
.super Ljava/lang/Object;
.source "Mat.java"


# instance fields
.field public final nativeObj:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    invoke-static {}, Lorg/opencv/core/Mat;->n_Mat()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 167
    return-void
.end method

.method public constructor <init>(III)V
    .locals 2
    .parameter "rows"
    .parameter "cols"
    .parameter "type"

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    invoke-static {p1, p2, p3}, Lorg/opencv/core/Mat;->n_Mat(III)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 197
    return-void
.end method

.method public constructor <init>(IIILorg/opencv/core/Scalar;)V
    .locals 11
    .parameter "rows"
    .parameter "cols"
    .parameter "type"
    .parameter "s"

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    iget-object v0, p4, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x0

    aget-wide v3, v0, v1

    iget-object v0, p4, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x1

    aget-wide v5, v0, v1

    iget-object v0, p4, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x2

    aget-wide v7, v0, v1

    iget-object v0, p4, Lorg/opencv/core/Scalar;->val:[D

    const/4 v1, 0x3

    aget-wide v9, v0, v1

    move v0, p1

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v10}, Lorg/opencv/core/Mat;->n_Mat(IIIDDDD)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 260
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .parameter "addr"

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Native object address is NULL"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iput-wide p1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 144
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Mat;Lorg/opencv/core/Range;)V
    .locals 4
    .parameter "m"
    .parameter "rowRange"

    .prologue
    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    iget-wide v0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget v2, p2, Lorg/opencv/core/Range;->start:I

    iget v3, p2, Lorg/opencv/core/Range;->end:I

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Mat;->n_Mat(JII)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 361
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Mat;Lorg/opencv/core/Range;Lorg/opencv/core/Range;)V
    .locals 6
    .parameter "m"
    .parameter "rowRange"
    .parameter "colRange"

    .prologue
    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iget-wide v0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget v2, p2, Lorg/opencv/core/Range;->start:I

    iget v3, p2, Lorg/opencv/core/Range;->end:I

    iget v4, p3, Lorg/opencv/core/Range;->start:I

    iget v5, p3, Lorg/opencv/core/Range;->end:I

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_Mat(JIIII)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 330
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Mat;Lorg/opencv/core/Rect;)V
    .locals 7
    .parameter "m"
    .parameter "roi"

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iget-wide v0, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget v2, p2, Lorg/opencv/core/Rect;->x:I

    iget v3, p2, Lorg/opencv/core/Rect;->x:I

    iget v4, p2, Lorg/opencv/core/Rect;->width:I

    add-int/2addr v3, v4

    iget v4, p2, Lorg/opencv/core/Rect;->y:I

    iget v5, p2, Lorg/opencv/core/Rect;->y:I

    iget v6, p2, Lorg/opencv/core/Rect;->height:I

    add-int/2addr v5, v6

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_Mat(JIIII)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 394
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Size;I)V
    .locals 4
    .parameter "size"
    .parameter "type"

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iget-wide v0, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v2, p1, Lorg/opencv/core/Size;->height:D

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Mat;->n_Mat(DDI)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 227
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Size;ILorg/opencv/core/Scalar;)V
    .locals 14
    .parameter "size"
    .parameter "type"
    .parameter "s"

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    iget-wide v1, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p1, Lorg/opencv/core/Size;->height:D

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v6, 0x0

    aget-wide v6, v5, v6

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v8, 0x1

    aget-wide v8, v5, v8

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v10, 0x2

    aget-wide v10, v5, v10

    move-object/from16 v0, p3

    iget-object v5, v0, Lorg/opencv/core/Scalar;->val:[D

    const/4 v12, 0x3

    aget-wide v12, v5, v12

    move/from16 v5, p2

    invoke-static/range {v1 .. v13}, Lorg/opencv/core/Mat;->n_Mat(DDIDDDD)J

    move-result-wide v1

    iput-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    .line 293
    return-void
.end method

.method public static diag(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "d"

    .prologue
    .line 985
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_diag(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 987
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public static eye(III)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "rows"
    .parameter "cols"
    .parameter "type"

    .prologue
    .line 1097
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0, p1, p2}, Lorg/opencv/core/Mat;->n_eye(III)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1099
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public static eye(Lorg/opencv/core/Size;I)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "size"
    .parameter "type"

    .prologue
    .line 1121
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p0, Lorg/opencv/core/Size;->height:D

    invoke-static {v1, v2, v3, v4, p1}, Lorg/opencv/core/Mat;->n_eye(DDI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1123
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method private static native locateROI_0(J[D[D)V
.end method

.method private static native nDump(J)Ljava/lang/String;
.end method

.method private static native nGet(JII)[D
.end method

.method private static native nGetB(JIII[B)I
.end method

.method private static native nGetD(JIII[D)I
.end method

.method private static native nGetF(JIII[F)I
.end method

.method private static native nGetI(JIII[I)I
.end method

.method private static native nGetS(JIII[S)I
.end method

.method private static native nPutB(JIII[B)I
.end method

.method private static native nPutD(JIII[D)I
.end method

.method private static native nPutF(JIII[F)I
.end method

.method private static native nPutI(JIII[I)I
.end method

.method private static native nPutS(JIII[S)I
.end method

.method private static native n_Mat()J
.end method

.method private static native n_Mat(DDI)J
.end method

.method private static native n_Mat(DDIDDDD)J
.end method

.method private static native n_Mat(III)J
.end method

.method private static native n_Mat(IIIDDDD)J
.end method

.method private static native n_Mat(JII)J
.end method

.method private static native n_Mat(JIIII)J
.end method

.method private static native n_adjustROI(JIIII)J
.end method

.method private static native n_assignTo(JJ)V
.end method

.method private static native n_assignTo(JJI)V
.end method

.method private static native n_channels(J)I
.end method

.method private static native n_checkVector(JI)I
.end method

.method private static native n_checkVector(JII)I
.end method

.method private static native n_checkVector(JIIZ)I
.end method

.method private static native n_clone(J)J
.end method

.method private static native n_col(JI)J
.end method

.method private static native n_colRange(JII)J
.end method

.method private static native n_cols(J)I
.end method

.method private static native n_convertTo(JJI)V
.end method

.method private static native n_convertTo(JJID)V
.end method

.method private static native n_convertTo(JJIDD)V
.end method

.method private static native n_copyTo(JJ)V
.end method

.method private static native n_copyTo(JJJ)V
.end method

.method private static native n_create(JDDI)V
.end method

.method private static native n_create(JIII)V
.end method

.method private static native n_cross(JJ)J
.end method

.method private static native n_dataAddr(J)J
.end method

.method private static native n_delete(J)V
.end method

.method private static native n_depth(J)I
.end method

.method private static native n_diag(J)J
.end method

.method private static native n_diag(JI)J
.end method

.method private static native n_dot(JJ)D
.end method

.method private static native n_elemSize(J)J
.end method

.method private static native n_elemSize1(J)J
.end method

.method private static native n_empty(J)Z
.end method

.method private static native n_eye(DDI)J
.end method

.method private static native n_eye(III)J
.end method

.method private static native n_inv(J)J
.end method

.method private static native n_inv(JI)J
.end method

.method private static native n_isContinuous(J)Z
.end method

.method private static native n_isSubmatrix(J)Z
.end method

.method private static native n_mul(JJ)J
.end method

.method private static native n_mul(JJD)J
.end method

.method private static native n_ones(DDI)J
.end method

.method private static native n_ones(III)J
.end method

.method private static native n_push_back(JJ)V
.end method

.method private static native n_release(J)V
.end method

.method private static native n_reshape(JI)J
.end method

.method private static native n_reshape(JII)J
.end method

.method private static native n_row(JI)J
.end method

.method private static native n_rowRange(JII)J
.end method

.method private static native n_rows(J)I
.end method

.method private static native n_setTo(JDDDD)J
.end method

.method private static native n_setTo(JJ)J
.end method

.method private static native n_setTo(JJJ)J
.end method

.method private static native n_size(J)[D
.end method

.method private static native n_step1(J)J
.end method

.method private static native n_step1(JI)J
.end method

.method private static native n_submat(JIIII)J
.end method

.method private static native n_submat_rr(JIIII)J
.end method

.method private static native n_t(J)J
.end method

.method private static native n_total(J)J
.end method

.method private static native n_type(J)I
.end method

.method private static native n_zeros(DDI)J
.end method

.method private static native n_zeros(III)J
.end method

.method public static ones(III)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "rows"
    .parameter "cols"
    .parameter "type"

    .prologue
    .line 1339
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0, p1, p2}, Lorg/opencv/core/Mat;->n_ones(III)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1341
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public static ones(Lorg/opencv/core/Size;I)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "size"
    .parameter "type"

    .prologue
    .line 1367
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p0, Lorg/opencv/core/Size;->height:D

    invoke-static {v1, v2, v3, v4, p1}, Lorg/opencv/core/Mat;->n_ones(DDI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1369
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public static zeros(III)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "rows"
    .parameter "cols"
    .parameter "type"

    .prologue
    .line 1855
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0, p1, p2}, Lorg/opencv/core/Mat;->n_zeros(III)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1857
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public static zeros(Lorg/opencv/core/Size;I)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "size"
    .parameter "type"

    .prologue
    .line 1883
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Size;->width:D

    iget-wide v3, p0, Lorg/opencv/core/Size;->height:D

    invoke-static {v1, v2, v3, v4, p1}, Lorg/opencv/core/Mat;->n_zeros(DDI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1885
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method


# virtual methods
.method public adjustROI(IIII)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "dtop"
    .parameter "dbottom"
    .parameter "dleft"
    .parameter "dright"

    .prologue
    .line 433
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_adjustROI(JIIII)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 435
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public assignTo(Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "m"

    .prologue
    .line 473
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Mat;->n_assignTo(JJ)V

    .line 475
    return-void
.end method

.method public assignTo(Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "m"
    .parameter "type"

    .prologue
    .line 456
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Mat;->n_assignTo(JJI)V

    .line 458
    return-void
.end method

.method public channels()I
    .locals 3

    .prologue
    .line 492
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_channels(J)I

    move-result v0

    .line 494
    .local v0, retVal:I
    return v0
.end method

.method public checkVector(I)I
    .locals 3
    .parameter "elemChannels"

    .prologue
    .line 521
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_checkVector(JI)I

    move-result v0

    .line 523
    .local v0, retVal:I
    return v0
.end method

.method public checkVector(II)I
    .locals 3
    .parameter "elemChannels"
    .parameter "depth"

    .prologue
    .line 513
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1, p2}, Lorg/opencv/core/Mat;->n_checkVector(JII)I

    move-result v0

    .line 515
    .local v0, retVal:I
    return v0
.end method

.method public checkVector(IIZ)I
    .locals 3
    .parameter "elemChannels"
    .parameter "depth"
    .parameter "requireContinuous"

    .prologue
    .line 505
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1, p2, p3}, Lorg/opencv/core/Mat;->n_checkVector(JIIZ)I

    move-result v0

    .line 507
    .local v0, retVal:I
    return v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->clone()Lorg/opencv/core/Mat;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/opencv/core/Mat;
    .locals 3

    .prologue
    .line 542
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_clone(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 544
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public col(I)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "x"

    .prologue
    .line 566
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_col(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 568
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public colRange(II)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "startcol"
    .parameter "endcol"

    .prologue
    .line 589
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1, p2}, Lorg/opencv/core/Mat;->n_colRange(JII)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 591
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public colRange(Lorg/opencv/core/Range;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "r"

    .prologue
    .line 611
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget v3, p1, Lorg/opencv/core/Range;->start:I

    iget v4, p1, Lorg/opencv/core/Range;->end:I

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Mat;->n_colRange(JII)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 613
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public cols()I
    .locals 3

    .prologue
    .line 623
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_cols(J)I

    move-result v0

    .line 625
    .local v0, retVal:I
    return v0
.end method

.method public convertTo(Lorg/opencv/core/Mat;I)V
    .locals 4
    .parameter "m"
    .parameter "rtype"

    .prologue
    .line 703
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3, p2}, Lorg/opencv/core/Mat;->n_convertTo(JJI)V

    .line 705
    return-void
.end method

.method public convertTo(Lorg/opencv/core/Mat;ID)V
    .locals 7
    .parameter "m"
    .parameter "rtype"
    .parameter "alpha"

    .prologue
    .line 679
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Mat;->n_convertTo(JJID)V

    .line 681
    return-void
.end method

.method public convertTo(Lorg/opencv/core/Mat;IDD)V
    .locals 9
    .parameter "m"
    .parameter "rtype"
    .parameter "alpha"
    .parameter "beta"

    .prologue
    .line 654
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move v4, p2

    move-wide v5, p3

    move-wide v7, p5

    invoke-static/range {v0 .. v8}, Lorg/opencv/core/Mat;->n_convertTo(JJIDD)V

    .line 656
    return-void
.end method

.method public copyTo(Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "m"

    .prologue
    .line 734
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Mat;->n_copyTo(JJ)V

    .line 736
    return-void
.end method

.method public copyTo(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)V
    .locals 6
    .parameter "m"
    .parameter "mask"

    .prologue
    .line 767
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_copyTo(JJJ)V

    .line 769
    return-void
.end method

.method public create(III)V
    .locals 2
    .parameter "rows"
    .parameter "cols"
    .parameter "type"

    .prologue
    .line 808
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, p2, p3}, Lorg/opencv/core/Mat;->n_create(JIII)V

    .line 810
    return-void
.end method

.method public create(Lorg/opencv/core/Size;I)V
    .locals 7
    .parameter "size"
    .parameter "type"

    .prologue
    .line 848
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Size;->width:D

    iget-wide v4, p1, Lorg/opencv/core/Size;->height:D

    move v6, p2

    invoke-static/range {v0 .. v6}, Lorg/opencv/core/Mat;->n_create(JDDI)V

    .line 850
    return-void
.end method

.method public cross(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "m"

    .prologue
    .line 871
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Mat;->n_cross(JJ)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 873
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public dataAddr()J
    .locals 4

    .prologue
    .line 883
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Mat;->n_dataAddr(J)J

    move-result-wide v0

    .line 885
    .local v0, retVal:J
    return-wide v0
.end method

.method public depth()I
    .locals 3

    .prologue
    .line 912
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_depth(J)I

    move-result v0

    .line 914
    .local v0, retVal:I
    return v0
.end method

.method public diag()Lorg/opencv/core/Mat;
    .locals 4

    .prologue
    .line 957
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lorg/opencv/core/Mat;->n_diag(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 959
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public diag(I)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "d"

    .prologue
    .line 940
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_diag(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 942
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public dot(Lorg/opencv/core/Mat;)D
    .locals 6
    .parameter "m"

    .prologue
    .line 1010
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lorg/opencv/core/Mat;->n_dot(JJ)D

    move-result-wide v0

    .line 1012
    .local v0, retVal:D
    return-wide v0
.end method

.method public dump()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1905
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Mat;->nDump(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public elemSize()J
    .locals 4

    .prologue
    .line 1030
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Mat;->n_elemSize(J)J

    move-result-wide v0

    .line 1032
    .local v0, retVal:J
    return-wide v0
.end method

.method public elemSize1()J
    .locals 4

    .prologue
    .line 1051
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Mat;->n_elemSize1(J)J

    move-result-wide v0

    .line 1053
    .local v0, retVal:J
    return-wide v0
.end method

.method public empty()Z
    .locals 3

    .prologue
    .line 1072
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_empty(J)Z

    move-result v0

    .line 1074
    .local v0, retVal:Z
    return v0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 1890
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Mat;->n_delete(J)V

    .line 1891
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1892
    return-void
.end method

.method public get(II[B)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1976
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1977
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1978
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1983
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1984
    :cond_3
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nGetB(JIII[B)I

    move-result v0

    return v0

    .line 1986
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(II[D)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 2032
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 2033
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 2034
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 2039
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 2040
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nGetD(JIII[D)I

    move-result v0

    return v0

    .line 2042
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(II[F)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 2018
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 2019
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 2020
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 2025
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2026
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nGetF(JIII[F)I

    move-result v0

    return v0

    .line 2028
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(II[I)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 2004
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 2005
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 2006
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 2011
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 2012
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nGetI(JIII[I)I

    move-result v0

    return v0

    .line 2014
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(II[S)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1990
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1991
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1992
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1997
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1998
    :cond_3
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nGetS(JIII[S)I

    move-result v0

    return v0

    .line 2000
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public get(II)[D
    .locals 2
    .parameter "row"
    .parameter "col"

    .prologue
    .line 2046
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1, p2}, Lorg/opencv/core/Mat;->nGet(JII)[D

    move-result-object v0

    return-object v0
.end method

.method public getNativeObjAddr()J
    .locals 2

    .prologue
    .line 2058
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    return-wide v0
.end method

.method public height()I
    .locals 1

    .prologue
    .line 2050
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v0

    return v0
.end method

.method public inv()Lorg/opencv/core/Mat;
    .locals 3

    .prologue
    .line 1169
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_inv(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1171
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public inv(I)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "method"

    .prologue
    .line 1151
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_inv(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1153
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public isContinuous()Z
    .locals 3

    .prologue
    .line 1217
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_isContinuous(J)Z

    move-result v0

    .line 1219
    .local v0, retVal:Z
    return v0
.end method

.method public isSubmatrix()Z
    .locals 3

    .prologue
    .line 1229
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_isSubmatrix(J)Z

    move-result v0

    .line 1231
    .local v0, retVal:Z
    return v0
.end method

.method public locateROI(Lorg/opencv/core/Size;Lorg/opencv/core/Point;)V
    .locals 6
    .parameter "wholeSize"
    .parameter "ofs"

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1258
    new-array v1, v2, [D

    .line 1259
    .local v1, wholeSize_out:[D
    new-array v0, v2, [D

    .line 1260
    .local v0, ofs_out:[D
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v1, v0}, Lorg/opencv/core/Mat;->locateROI_0(J[D[D)V

    .line 1261
    if-eqz p1, :cond_0

    aget-wide v2, v1, v4

    iput-wide v2, p1, Lorg/opencv/core/Size;->width:D

    aget-wide v2, v1, v5

    iput-wide v2, p1, Lorg/opencv/core/Size;->height:D

    .line 1262
    :cond_0
    if-eqz p2, :cond_1

    aget-wide v2, v0, v4

    iput-wide v2, p2, Lorg/opencv/core/Point;->x:D

    aget-wide v2, v0, v5

    iput-wide v2, p2, Lorg/opencv/core/Point;->y:D

    .line 1263
    :cond_1
    return-void
.end method

.method public mul(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "m"

    .prologue
    .line 1310
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Mat;->n_mul(JJ)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1312
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public mul(Lorg/opencv/core/Mat;D)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "m"
    .parameter "scale"

    .prologue
    .line 1288
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_mul(JJD)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1290
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public push_back(Lorg/opencv/core/Mat;)V
    .locals 4
    .parameter "m"

    .prologue
    .line 1391
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, v2, v3}, Lorg/opencv/core/Mat;->n_push_back(JJ)V

    .line 1393
    return-void
.end method

.method public put(II[B)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1962
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1963
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1964
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1969
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1970
    :cond_3
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nPutB(JIII[B)I

    move-result v0

    return v0

    .line 1972
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public varargs put(II[D)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1909
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1910
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1911
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1916
    :cond_2
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nPutD(JIII[D)I

    move-result v0

    return v0
.end method

.method public put(II[F)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1920
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1921
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1922
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1927
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1928
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nPutF(JIII[F)I

    move-result v0

    return v0

    .line 1930
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(II[I)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1934
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1935
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1936
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1941
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1942
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nPutI(JIII[I)I

    move-result v0

    return v0

    .line 1944
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(II[S)I
    .locals 7
    .parameter "row"
    .parameter "col"
    .parameter "data"

    .prologue
    .line 1948
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v6

    .line 1949
    .local v6, t:I
    if-eqz p3, :cond_0

    array-length v0, p3

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 1950
    :cond_0
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provided data element number ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") should be multiple of the Mat channels count ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Lorg/opencv/core/CvType;->channels(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    array-length v0, p3

    goto :goto_0

    .line 1955
    :cond_2
    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    invoke-static {v6}, Lorg/opencv/core/CvType;->depth(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 1956
    :cond_3
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    array-length v4, p3

    move v2, p1

    move v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->nPutS(JIII[S)I

    move-result v0

    return v0

    .line 1958
    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mat data type is not compatible: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 1421
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1}, Lorg/opencv/core/Mat;->n_release(J)V

    .line 1423
    return-void
.end method

.method public reshape(I)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "cn"

    .prologue
    .line 1489
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_reshape(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1491
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public reshape(II)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "cn"
    .parameter "rows"

    .prologue
    .line 1458
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1, p2}, Lorg/opencv/core/Mat;->n_reshape(JII)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1460
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public row(I)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "y"

    .prologue
    .line 1518
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1}, Lorg/opencv/core/Mat;->n_row(JI)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1520
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public rowRange(II)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "startrow"
    .parameter "endrow"

    .prologue
    .line 1541
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, p1, p2}, Lorg/opencv/core/Mat;->n_rowRange(JII)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1543
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public rowRange(Lorg/opencv/core/Range;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "r"

    .prologue
    .line 1563
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget v3, p1, Lorg/opencv/core/Range;->start:I

    iget v4, p1, Lorg/opencv/core/Range;->end:I

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Mat;->n_rowRange(JII)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1565
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public rows()I
    .locals 3

    .prologue
    .line 1575
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_rows(J)I

    move-result v0

    .line 1577
    .local v0, retVal:I
    return v0
.end method

.method public setTo(Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 5
    .parameter "value"

    .prologue
    .line 1623
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v3, p1, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2, v3, v4}, Lorg/opencv/core/Mat;->n_setTo(JJ)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1625
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public setTo(Lorg/opencv/core/Mat;Lorg/opencv/core/Mat;)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "value"
    .parameter "mask"

    .prologue
    .line 1608
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v2, p1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, p2, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_setTo(JJJ)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1610
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public setTo(Lorg/opencv/core/Scalar;)Lorg/opencv/core/Mat;
    .locals 11
    .parameter "s"

    .prologue
    .line 1587
    new-instance v10, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget-object v2, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    iget-object v4, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    iget-object v6, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v7, 0x2

    aget-wide v6, v6, v7

    iget-object v8, p1, Lorg/opencv/core/Scalar;->val:[D

    const/4 v9, 0x3

    aget-wide v8, v8, v9

    invoke-static/range {v0 .. v9}, Lorg/opencv/core/Mat;->n_setTo(JDDDD)J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1589
    .local v10, retVal:Lorg/opencv/core/Mat;
    return-object v10
.end method

.method public size()Lorg/opencv/core/Size;
    .locals 3

    .prologue
    .line 1643
    new-instance v0, Lorg/opencv/core/Size;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_size(J)[D

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/opencv/core/Size;-><init>([D)V

    .line 1645
    .local v0, retVal:Lorg/opencv/core/Size;
    return-object v0
.end method

.method public step1()J
    .locals 4

    .prologue
    .line 1681
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Mat;->n_step1(J)J

    move-result-wide v0

    .line 1683
    .local v0, retVal:J
    return-wide v0
.end method

.method public step1(I)J
    .locals 4
    .parameter "i"

    .prologue
    .line 1665
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, p1}, Lorg/opencv/core/Mat;->n_step1(JI)J

    move-result-wide v0

    .line 1667
    .local v0, retVal:J
    return-wide v0
.end method

.method public submat(IIII)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "rowStart"
    .parameter "rowEnd"
    .parameter "colStart"
    .parameter "colEnd"

    .prologue
    .line 1710
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_submat_rr(JIIII)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1712
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public submat(Lorg/opencv/core/Range;Lorg/opencv/core/Range;)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "rowRange"
    .parameter "colRange"

    .prologue
    .line 1738
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget v2, p1, Lorg/opencv/core/Range;->start:I

    iget v3, p1, Lorg/opencv/core/Range;->end:I

    iget v4, p2, Lorg/opencv/core/Range;->start:I

    iget v5, p2, Lorg/opencv/core/Range;->end:I

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_submat_rr(JIIII)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1740
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public submat(Lorg/opencv/core/Rect;)Lorg/opencv/core/Mat;
    .locals 7
    .parameter "roi"

    .prologue
    .line 1763
    new-instance v6, Lorg/opencv/core/Mat;

    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    iget v2, p1, Lorg/opencv/core/Rect;->x:I

    iget v3, p1, Lorg/opencv/core/Rect;->y:I

    iget v4, p1, Lorg/opencv/core/Rect;->width:I

    iget v5, p1, Lorg/opencv/core/Rect;->height:I

    invoke-static/range {v0 .. v5}, Lorg/opencv/core/Mat;->n_submat(JIIII)J

    move-result-wide v0

    invoke-direct {v6, v0, v1}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1765
    .local v6, retVal:Lorg/opencv/core/Mat;
    return-object v6
.end method

.method public t()Lorg/opencv/core/Mat;
    .locals 3

    .prologue
    .line 1785
    new-instance v0, Lorg/opencv/core/Mat;

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_t(J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    .line 1787
    .local v0, retVal:Lorg/opencv/core/Mat;
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Mat [ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->rows()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->type()I

    move-result v1

    invoke-static {v1}, Lorg/opencv/core/CvType;->typeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isCont="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->isContinuous()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSubmat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->isSubmatrix()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nativeObj=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataAddr=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/opencv/core/Mat;->dataAddr()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public total()J
    .locals 4

    .prologue
    .line 1805
    iget-wide v2, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3}, Lorg/opencv/core/Mat;->n_total(J)J

    move-result-wide v0

    .line 1807
    .local v0, retVal:J
    return-wide v0
.end method

.method public type()I
    .locals 3

    .prologue
    .line 1826
    iget-wide v1, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v1, v2}, Lorg/opencv/core/Mat;->n_type(J)I

    move-result v0

    .line 1828
    .local v0, retVal:I
    return v0
.end method

.method public width()I
    .locals 1

    .prologue
    .line 2054
    invoke-virtual {p0}, Lorg/opencv/core/Mat;->cols()I

    move-result v0

    return v0
.end method
