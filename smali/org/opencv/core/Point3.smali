.class public Lorg/opencv/core/Point3;
.super Ljava/lang/Object;
.source "Point3.java"


# instance fields
.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v1, 0x0

    .line 25
    move-object v0, p0

    move-wide v3, v1

    move-wide v5, v1

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    .line 26
    return-void
.end method

.method public constructor <init>(DDD)V
    .locals 0
    .parameter "x"
    .parameter "y"
    .parameter "z"

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lorg/opencv/core/Point3;->x:D

    .line 20
    iput-wide p3, p0, Lorg/opencv/core/Point3;->y:D

    .line 21
    iput-wide p5, p0, Lorg/opencv/core/Point3;->z:D

    .line 22
    return-void
.end method

.method public constructor <init>(Lorg/opencv/core/Point;)V
    .locals 2
    .parameter "p"

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iget-wide v0, p1, Lorg/opencv/core/Point;->x:D

    iput-wide v0, p0, Lorg/opencv/core/Point3;->x:D

    .line 30
    iget-wide v0, p1, Lorg/opencv/core/Point;->y:D

    iput-wide v0, p0, Lorg/opencv/core/Point3;->y:D

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/opencv/core/Point3;->z:D

    .line 32
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .parameter "vals"

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/opencv/core/Point3;-><init>()V

    .line 36
    invoke-virtual {p0, p1}, Lorg/opencv/core/Point3;->set([D)V

    .line 37
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0}, Lorg/opencv/core/Point3;->clone()Lorg/opencv/core/Point3;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/opencv/core/Point3;
    .locals 7

    .prologue
    .line 52
    new-instance v0, Lorg/opencv/core/Point3;

    iget-wide v1, p0, Lorg/opencv/core/Point3;->x:D

    iget-wide v3, p0, Lorg/opencv/core/Point3;->y:D

    iget-wide v5, p0, Lorg/opencv/core/Point3;->z:D

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    return-object v0
.end method

.method public cross(Lorg/opencv/core/Point3;)Lorg/opencv/core/Point3;
    .locals 11
    .parameter "p"

    .prologue
    .line 60
    new-instance v0, Lorg/opencv/core/Point3;

    iget-wide v1, p0, Lorg/opencv/core/Point3;->y:D

    iget-wide v3, p1, Lorg/opencv/core/Point3;->z:D

    mul-double/2addr v1, v3

    iget-wide v3, p0, Lorg/opencv/core/Point3;->z:D

    iget-wide v5, p1, Lorg/opencv/core/Point3;->y:D

    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    iget-wide v3, p0, Lorg/opencv/core/Point3;->z:D

    iget-wide v5, p1, Lorg/opencv/core/Point3;->x:D

    mul-double/2addr v3, v5

    iget-wide v5, p0, Lorg/opencv/core/Point3;->x:D

    iget-wide v7, p1, Lorg/opencv/core/Point3;->z:D

    mul-double/2addr v5, v7

    sub-double/2addr v3, v5

    iget-wide v5, p0, Lorg/opencv/core/Point3;->x:D

    iget-wide v7, p1, Lorg/opencv/core/Point3;->y:D

    mul-double/2addr v5, v7

    iget-wide v7, p0, Lorg/opencv/core/Point3;->y:D

    iget-wide v9, p1, Lorg/opencv/core/Point3;->x:D

    mul-double/2addr v7, v9

    sub-double/2addr v5, v7

    invoke-direct/range {v0 .. v6}, Lorg/opencv/core/Point3;-><init>(DDD)V

    return-object v0
.end method

.method public dot(Lorg/opencv/core/Point3;)D
    .locals 6
    .parameter "p"

    .prologue
    .line 56
    iget-wide v0, p0, Lorg/opencv/core/Point3;->x:D

    iget-wide v2, p1, Lorg/opencv/core/Point3;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lorg/opencv/core/Point3;->y:D

    iget-wide v4, p1, Lorg/opencv/core/Point3;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lorg/opencv/core/Point3;->z:D

    iget-wide v4, p1, Lorg/opencv/core/Point3;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .parameter "obj"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-ne p0, p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 80
    :cond_1
    instance-of v3, p1, Lorg/opencv/core/Point3;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 81
    check-cast v0, Lorg/opencv/core/Point3;

    .line 82
    .local v0, it:Lorg/opencv/core/Point3;
    iget-wide v3, p0, Lorg/opencv/core/Point3;->x:D

    iget-wide v5, v0, Lorg/opencv/core/Point3;->x:D

    cmpl-double v3, v3, v5

    if-nez v3, :cond_3

    iget-wide v3, p0, Lorg/opencv/core/Point3;->y:D

    iget-wide v5, v0, Lorg/opencv/core/Point3;->y:D

    cmpl-double v3, v3, v5

    if-nez v3, :cond_3

    iget-wide v3, p0, Lorg/opencv/core/Point3;->z:D

    iget-wide v5, v0, Lorg/opencv/core/Point3;->z:D

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/16 v7, 0x20

    .line 65
    const/16 v0, 0x1f

    .line 66
    .local v0, prime:I
    const/4 v1, 0x1

    .line 68
    .local v1, result:I
    iget-wide v4, p0, Lorg/opencv/core/Point3;->x:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 69
    .local v2, temp:J
    ushr-long v4, v2, v7

    xor-long/2addr v4, v2

    long-to-int v4, v4

    add-int/lit8 v1, v4, 0x1f

    .line 70
    iget-wide v4, p0, Lorg/opencv/core/Point3;->y:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 71
    mul-int/lit8 v4, v1, 0x1f

    ushr-long v5, v2, v7

    xor-long/2addr v5, v2

    long-to-int v5, v5

    add-int v1, v4, v5

    .line 72
    iget-wide v4, p0, Lorg/opencv/core/Point3;->z:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 73
    mul-int/lit8 v4, v1, 0x1f

    ushr-long v5, v2, v7

    xor-long/2addr v5, v2

    long-to-int v5, v5

    add-int v1, v4, v5

    .line 74
    return v1
.end method

.method public set([D)V
    .locals 6
    .parameter "vals"

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 40
    if-eqz p1, :cond_3

    .line 41
    array-length v0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x0

    aget-wide v0, p1, v0

    :goto_0
    iput-wide v0, p0, Lorg/opencv/core/Point3;->x:D

    .line 42
    array-length v0, p1

    if-le v0, v4, :cond_2

    aget-wide v0, p1, v4

    :goto_1
    iput-wide v0, p0, Lorg/opencv/core/Point3;->y:D

    .line 43
    array-length v0, p1

    if-le v0, v5, :cond_0

    aget-wide v2, p1, v5

    :cond_0
    iput-wide v2, p0, Lorg/opencv/core/Point3;->z:D

    .line 49
    :goto_2
    return-void

    :cond_1
    move-wide v0, v2

    .line 41
    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 42
    goto :goto_1

    .line 45
    :cond_3
    iput-wide v2, p0, Lorg/opencv/core/Point3;->x:D

    .line 46
    iput-wide v2, p0, Lorg/opencv/core/Point3;->y:D

    .line 47
    iput-wide v2, p0, Lorg/opencv/core/Point3;->z:D

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/opencv/core/Point3;->x:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/opencv/core/Point3;->y:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/opencv/core/Point3;->z:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
