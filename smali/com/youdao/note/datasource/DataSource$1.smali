.class Lcom/youdao/note/datasource/DataSource$1;
.super Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.source "DataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/ResourceMetaSwitcher",
        "<",
        "Lcom/youdao/note/data/resource/AbstractResource",
        "<+",
        "Lcom/youdao/note/data/resource/IResourceMeta;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/datasource/DataSource;

.field final synthetic val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/resource/BaseResourceMeta;Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter

    .prologue
    .line 274
    iput-object p1, p0, Lcom/youdao/note/datasource/DataSource$1;->this$0:Lcom/youdao/note/datasource/DataSource;

    iput-object p3, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {p0, p2}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    return-void
.end method


# virtual methods
.method protected onAudioType()Lcom/youdao/note/data/resource/AbstractResource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    const/4 v0, 0x0

    .line 323
    .local v0, meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/AudioResourceMeta;

    if-eqz v1, :cond_0

    .line 324
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    check-cast v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    .line 328
    .restart local v0       #meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    :goto_0
    new-instance v1, Lcom/youdao/note/data/resource/AudioResource;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/AudioResource;-><init>(Lcom/youdao/note/data/resource/AudioResourceMeta;)V

    return-object v1

    .line 326
    :cond_0
    new-instance v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {v0, v1}, Lcom/youdao/note/data/resource/AudioResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .restart local v0       #meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    goto :goto_0
.end method

.method protected bridge synthetic onAudioType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$1;->onAudioType()Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    return-object v0
.end method

.method protected onDoodleType()Lcom/youdao/note/data/resource/AbstractResource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    const/4 v0, 0x0

    .line 279
    .local v0, doodleResourceMeta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    if-eqz v1, :cond_0

    .line 280
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #doodleResourceMeta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    check-cast v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    .line 284
    .restart local v0       #doodleResourceMeta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    :goto_0
    new-instance v1, Lcom/youdao/note/data/resource/DoodleResource;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/DoodleResource;-><init>(Lcom/youdao/note/data/resource/DoodleResourceMeta;)V

    return-object v1

    .line 282
    :cond_0
    new-instance v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    .end local v0           #doodleResourceMeta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {v0, v1}, Lcom/youdao/note/data/resource/DoodleResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .restart local v0       #doodleResourceMeta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    goto :goto_0
.end method

.method protected bridge synthetic onDoodleType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$1;->onDoodleType()Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    return-object v0
.end method

.method protected onGeneralType()Lcom/youdao/note/data/resource/AbstractResource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    const/4 v0, 0x0

    .line 312
    .local v0, meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    if-eqz v1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    check-cast v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    .line 317
    .restart local v0       #meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    :goto_0
    new-instance v1, Lcom/youdao/note/data/resource/GeneralResource;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/GeneralResource;-><init>(Lcom/youdao/note/data/resource/GeneralResourceMeta;)V

    return-object v1

    .line 315
    :cond_0
    new-instance v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {v0, v1}, Lcom/youdao/note/data/resource/GeneralResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .restart local v0       #meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    goto :goto_0
.end method

.method protected bridge synthetic onGeneralType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$1;->onGeneralType()Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    return-object v0
.end method

.method protected onHandwriteType()Lcom/youdao/note/data/resource/AbstractResource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    const/4 v0, 0x0

    .line 290
    .local v0, meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    if-eqz v1, :cond_0

    .line 291
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    check-cast v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    .line 295
    .restart local v0       #meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    :goto_0
    new-instance v1, Lcom/youdao/note/data/resource/HandwriteResource;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/HandwriteResource;-><init>(Lcom/youdao/note/data/resource/HandwriteResourceMeta;)V

    return-object v1

    .line 293
    :cond_0
    new-instance v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .restart local v0       #meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    goto :goto_0
.end method

.method protected bridge synthetic onHandwriteType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$1;->onHandwriteType()Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    return-object v0
.end method

.method protected onImageType()Lcom/youdao/note/data/resource/AbstractResource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    const/4 v0, 0x0

    .line 301
    .local v0, meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v1, :cond_0

    .line 302
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    check-cast v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 306
    .restart local v0       #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :goto_0
    new-instance v1, Lcom/youdao/note/data/resource/ImageResource;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/ImageResource;-><init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-object v1

    .line 304
    :cond_0
    new-instance v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .end local v0           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource$1;->val$baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {v0, v1}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .restart local v0       #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    goto :goto_0
.end method

.method protected bridge synthetic onImageType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$1;->onImageType()Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    return-object v0
.end method
