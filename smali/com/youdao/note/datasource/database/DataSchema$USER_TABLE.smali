.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$USER_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "USER_TABLE"
.end annotation


# static fields
.field public static final COLUMN_DEF_PROPS:Ljava/lang/String; = "props text not null default \'\'"

.field public static final CREATE_USER_TABLE_SQL:Ljava/lang/String; = "create table user ( _id varchar(128) not null primary key, used_space integer, quota_space integer, default_notebook varchar(128), last_push_time text not null, last_sync_time text, props text not null default \'\' );"

.field public static final DEFAULT_NOTEBOOK:Ljava/lang/String; = "default_notebook"

.field public static final DROP_USER_TABLE_SQL:Ljava/lang/String; = "drop table if exists user"

.field public static final LAST_PUSH_TIME:Ljava/lang/String; = "last_push_time"

.field public static final LAST_SYNC_TIME:Ljava/lang/String; = "last_sync_time"

.field public static final PROPS:Ljava/lang/String; = "props"

.field public static final QUOTA_SPACE:Ljava/lang/String; = "quota_space"

.field public static final TABLE_NAME:Ljava/lang/String; = "user"

.field public static final USED_SPACE:Ljava/lang/String; = "used_space"

.field public static final USER_NAME:Ljava/lang/String; = "_id"
