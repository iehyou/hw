.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$NOTE_BOOKS_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Lcom/youdao/note/datasource/database/DataSchema$BASE_NOTE_META_TABLE;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NOTE_BOOKS_TABLE"
.end annotation


# static fields
.field public static final COLUMN_DEF_PROPS:Ljava/lang/String; = "props text not null default \'\'"

.field public static final COLUMN_DEF_TYPE:Ljava/lang/String; = "type integer not null default 0"

.field public static final CREATE_NOTEBOOK_TABLE_SQL:Ljava/lang/String; = "create table note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default \'\' );"

.field public static final DROP_NOTE_BOOKS_TABLE_SQL:Ljava/lang/String; = "drop table if exists note_books"

.field public static final NOTEBOOK:Ljava/lang/String; = "_id"

.field public static final NOTE_NUMBER:Ljava/lang/String; = "note_number"

.field public static final PROPS:Ljava/lang/String; = "props"

.field public static final TABLE_NAME:Ljava/lang/String; = "note_books"

.field public static final TYPE:Ljava/lang/String; = "type"
