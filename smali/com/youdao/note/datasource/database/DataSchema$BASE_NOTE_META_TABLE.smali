.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$BASE_NOTE_META_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BASE_NOTE_META_TABLE"
.end annotation


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final IS_DELETED:Ljava/lang/String; = "is_deleted"

.field public static final IS_DIRTY:Ljava/lang/String; = "is_dirty"

.field public static final LAST_SYNC_TIME:Ljava/lang/String; = "last_sync_time"

.field public static final MODIFY_TIME:Ljava/lang/String; = "modify_time"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final VERSION:Ljava/lang/String; = "version"
