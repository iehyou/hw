.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$SEARCH_HISTORY_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SEARCH_HISTORY_TABLE"
.end annotation


# static fields
.field public static final CREATE_SEARCH_HISTORY_TABLE_SQL:Ljava/lang/String; = "create table search_history ( _id integer primary key autoincrement, query text not null unique, timestamp integer not null);"

.field public static final DROP_SEARCH_HISTORY_TABLE_SQL:Ljava/lang/String; = "drop table if exists search_history"

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final TABLE_NAME:Ljava/lang/String; = "search_history"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"
