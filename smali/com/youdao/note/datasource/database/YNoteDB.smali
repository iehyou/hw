.class public Lcom/youdao/note/datasource/database/YNoteDB;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "YNoteDB.java"

# interfaces
.implements Lcom/youdao/note/datasource/database/DataSchema;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/datasource/database/YNoteDB;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "context"

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getYNoteDBName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    iput-object v2, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 48
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "YNote database reset and db open is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getYNoteDBName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "dbName"

    .prologue
    const/4 v1, 0x0

    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v1, v0}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    iput-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 60
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 61
    return-void
.end method

.method private escape(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "keyword"

    .prologue
    .line 524
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 525
    .local v4, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, arr$:[C
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    .line 526
    .local v1, c:C
    sparse-switch v1, :sswitch_data_0

    .line 542
    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 525
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 536
    :sswitch_0
    const/16 v5, 0x21

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 539
    :sswitch_1
    const-string v5, "\'\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 545
    .end local v1           #c:C
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 526
    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x25 -> :sswitch_0
        0x26 -> :sswitch_0
        0x27 -> :sswitch_1
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2f -> :sswitch_0
        0x5b -> :sswitch_0
        0x5d -> :sswitch_0
        0x5f -> :sswitch_0
    .end sparse-switch
.end method

.method private getNoteBookFromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;
    .locals 1
    .parameter "cursor"

    .prologue
    .line 619
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 621
    :try_start_0
    invoke-static {p1}, Lcom/youdao/note/data/NoteBook;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 623
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private getNoteMeta(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;
    .locals 2
    .parameter "cursor"

    .prologue
    .line 607
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 608
    invoke-static {p1}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 614
    .local v0, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .end local v0           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :goto_0
    return-object v0

    .line 611
    :cond_0
    const/4 v0, 0x0

    .line 614
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private getResourcesFromCursor(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 2
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 594
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 596
    .local v0, resourceList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    :goto_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    invoke-static {p1}, Lcom/youdao/note/data/resource/ResourceUtils;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 600
    :catchall_0
    move-exception v1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 602
    return-object v0
.end method

.method private innerResetDB()V
    .locals 2

    .prologue
    .line 628
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists note_meta"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 629
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists note_books"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists resource_meta"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 631
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists user"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists root_note_books"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 633
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists cache"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 634
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists content_version"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/database/YNoteDB;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 636
    return-void
.end method


# virtual methods
.method public deleteCacheItem(Ljava/lang/String;)V
    .locals 5
    .parameter "itemId"

    .prologue
    .line 147
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 149
    return-void
.end method

.method public deleteNoteBook(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 1
    .parameter "noteMeta"

    .prologue
    .line 574
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteNoteBook(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteNoteBook(Ljava/lang/String;)Z
    .locals 6
    .parameter "noteBookId"

    .prologue
    const/4 v5, 0x1

    .line 567
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_books"

    const-string v2, "_id = ?"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 570
    return v5
.end method

.method public deleteNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 7
    .parameter "noteMeta"

    .prologue
    const/4 v6, 0x1

    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delete "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from database."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v2, "_id = ?"

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 425
    return v6
.end method

.method public deleteResource(Lcom/youdao/note/data/resource/IResourceMeta;)Z
    .locals 1
    .parameter "meta"

    .prologue
    .line 234
    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteResourceById(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteResourceById(Ljava/lang/String;)Z
    .locals 6
    .parameter "resourceId"

    .prologue
    const/4 v5, 0x1

    .line 244
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "resource_meta"

    const-string v2, "_id = ?"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 245
    return v5
.end method

.method public deleteResourceByNote(Ljava/lang/String;)Z
    .locals 6
    .parameter "noteId"

    .prologue
    const/4 v5, 0x1

    .line 254
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "resource_meta"

    const-string v2, "noteid = ?"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 255
    return v5
.end method

.method public getAllNoteBookMetasCursor()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 488
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_books"

    const-string v3, "is_deleted = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const-string v7, "modify_time desc"

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 490
    .local v9, cursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Total "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " note books fetched from database."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    return-object v9
.end method

.method public getAllNoteMetasCursor()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 477
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v3, "is_deleted = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const-string v7, "modify_time desc"

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 479
    .local v9, cursor:Landroid/database/Cursor;
    sget-object v0, Lcom/youdao/note/datasource/database/YNoteDB;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Total "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fetched from database."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-object v9
.end method

.method public getCacheItem(I)Landroid/database/Cursor;
    .locals 9
    .parameter "type"

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache"

    const-string v3, "item_type = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v7, "touch_time asc"

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getCachedNoteCount()I
    .locals 3

    .prologue
    .line 167
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "select count(*) from cache where item_type = 1"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 171
    .local v0, s:Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    long-to-int v1, v1

    .line 173
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method public getCachedResourceSize()J
    .locals 3

    .prologue
    .line 182
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "select sum(length ) from cache where item_type = 2"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 187
    .local v0, s:Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 189
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method public getDirtyAndMovedNoteMetas()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v3, "is_dirty = 1 or server_notebook != notebook"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 307
    .local v8, cursor:Landroid/database/Cursor;
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 310
    .local v9, noteMetas:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/youdao/note/data/NoteMeta;>;"
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-static {v8}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 314
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 316
    return-object v9
.end method

.method public getDirtyNotebookMetas()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_books"

    const-string v3, "is_dirty = 1"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getDirtyResourcesOf(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .parameter "noteId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 326
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "resource_meta"

    const-string v3, "is_dirty = 1 and noteid = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 330
    .local v9, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourcesFromCursor(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getNoteBookCount(Ljava/lang/String;)I
    .locals 4
    .parameter "noteBookId"

    .prologue
    .line 200
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "select count(*) from note_meta where notebook = \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 204
    .local v0, s:Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    long-to-int v1, v1

    .line 206
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method public getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;
    .locals 10
    .parameter "noteBook"

    .prologue
    const/4 v2, 0x0

    .line 554
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_books"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 557
    .local v9, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookFromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method public getNoteBookMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;
    .locals 9
    .parameter "title"

    .prologue
    const/4 v2, 0x0

    .line 561
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_books"

    const-string v3, "title = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 563
    .local v8, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v8}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookFromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method public getNoteByNoteBook(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .parameter "noteBookId"

    .prologue
    const/4 v2, 0x0

    .line 501
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v3, "notebook = ? and is_deleted = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    const-string v7, "modify_time desc"

    move-object v5, v2

    move-object v6, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 504
    .local v9, cursor:Landroid/database/Cursor;
    return-object v9
.end method

.method public getNoteContentVersion(Ljava/lang/String;)I
    .locals 11
    .parameter "noteId"

    .prologue
    const/4 v2, 0x0

    .line 259
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "content_version"

    const-string v3, "note_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 261
    .local v9, cursor:Landroid/database/Cursor;
    new-instance v10, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v10, v9}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 263
    .local v10, helper:Lcom/youdao/note/utils/CursorHelper;
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    const-string v0, "content_version"

    invoke-virtual {v10, v0}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 269
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    .line 266
    :cond_0
    const/4 v0, -0x1

    .line 269
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;
    .locals 10
    .parameter "noteId"

    .prologue
    const/4 v2, 0x0

    .line 578
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v3, "_id = ? and is_deleted = 0"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 584
    .local v9, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMeta(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    return-object v0
.end method

.method public getNoteMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;
    .locals 10
    .parameter "title"

    .prologue
    const/4 v2, 0x0

    .line 588
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    const-string v3, "title = ? and is_deleted = 0"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 590
    .local v9, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMeta(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    return-object v0
.end method

.method public getResourceById(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 11
    .parameter "resourceId"

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "resource_meta"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 339
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    invoke-static {v9}, Lcom/youdao/note/data/resource/ResourceUtils;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 346
    .local v10, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .end local v10           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :goto_0
    return-object v10

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v10, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getResourcesByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .parameter "noteId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "resource_meta"

    const-string v3, "noteid = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 300
    .local v9, cursor:Landroid/database/Cursor;
    invoke-direct {p0, v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourcesFromCursor(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRootMeta()Lcom/youdao/note/data/NoteBook;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 429
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "root_note_books"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 432
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    invoke-static {v8}, Lcom/youdao/note/data/NoteBook;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 437
    .local v9, noteBook:Lcom/youdao/note/data/NoteBook;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 439
    .end local v9           #noteBook:Lcom/youdao/note/data/NoteBook;
    :goto_0
    return-object v9

    .line 437
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v9, v2

    .line 439
    goto :goto_0

    .line 437
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getUserMeta(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;
    .locals 11
    .parameter "userName"

    .prologue
    const/4 v2, 0x0

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user name is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "user"

    const-string v3, "_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 361
    .local v9, cursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cursor count is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-static {v9}, Lcom/youdao/note/data/UserMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/UserMeta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 368
    .local v10, userMeta:Lcom/youdao/note/data/UserMeta;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 370
    .end local v10           #userMeta:Lcom/youdao/note/data/UserMeta;
    :goto_0
    return-object v10

    .line 368
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v10, v2

    .line 370
    goto :goto_0

    .line 368
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z
    .locals 4
    .parameter "noteBook"

    .prologue
    .line 463
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 464
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "_id"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string v1, "create_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getCreateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 466
    const-string v1, "modify_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getModifyTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 467
    const-string v1, "version"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 468
    const-string v1, "last_sync_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getLastSyncTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 469
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v1, "is_dirty"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->isDirty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 471
    const-string v1, "is_deleted"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 472
    const-string v1, "note_number"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 473
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "note_books"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method public insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 4
    .parameter "noteMeta"

    .prologue
    .line 391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert one noteMeta "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 393
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "notebook"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v1, "_id"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v1, "author"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v1, "create_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getCreateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 398
    const-string v1, "modify_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 399
    const-string v1, "server_notebook"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getServerNoteBook()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v1, "version"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 401
    const-string v1, "length"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getLength()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 402
    const-string v1, "last_sync_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getLastSyncTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 403
    const-string v1, "props"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getPropsAsStr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v1, "is_dirty"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 405
    const-string v1, "is_deleted"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 406
    const-string v1, "source"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getSourceUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "note_meta"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method public insertOrUpdateResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z
    .locals 4
    .parameter "meta"

    .prologue
    .line 216
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 217
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "_id"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v1, "filename"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v1, "noteid"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v1, "version"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    const-string v1, "length"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 222
    const-string v1, "is_dirty"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->isDirty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 223
    const-string v1, "type"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 224
    const-string v1, "props"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getPropsAsStr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "resource_meta"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method public insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z
    .locals 6
    .parameter "userName"
    .parameter "userMeta"

    .prologue
    .line 374
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User Meta info updated "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", userName is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 376
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v2, "used_space"

    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getUsedSpace()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 378
    const-string v2, "quota_space"

    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getQuotaSpace()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 379
    const-string v2, "default_notebook"

    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v2, "last_push_time"

    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getLastPushTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 381
    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getLastSynceTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 382
    const-string v2, "last_sync_time"

    invoke-virtual {p2}, Lcom/youdao/note/data/UserMeta;->getLastSynceTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 384
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "user"

    invoke-static {v2, v3, v1}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 385
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getUserMeta(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    .line 386
    .local v0, meta2:Lcom/youdao/note/data/UserMeta;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "haa user meta is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    const/4 v2, 0x1

    return v2
.end method

.method public markDeleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 1
    .parameter "noteMeta"

    .prologue
    const/4 v0, 0x1

    .line 411
    invoke-virtual {p1, v0}, Lcom/youdao/note/data/NoteMeta;->setDeleted(Z)V

    .line 412
    invoke-virtual {p1, v0}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 413
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v0

    return v0
.end method

.method public markDeleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "noteBook"

    .prologue
    const/4 v0, 0x1

    .line 417
    invoke-virtual {p1, v0}, Lcom/youdao/note/data/NoteBook;->setDeleted(Z)V

    .line 418
    invoke-virtual {p1, v0}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 419
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter "db"

    .prologue
    .line 65
    const-string v0, "create table note_meta ( _id varchar(128) primary key, server_notebook varchar(128), notebook varchar(128), create_time varchar(32), modify_time varchar(32), author varchar(64), props text not null, title varchar(512), version integer not null, last_sync_time varchar(32), source varchar(128), is_dirty boolean not null, is_deleted boolean not null, length integer not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    const-string v0, "CREATE INDEX notes_on_notebook ON note_meta (notebook);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 67
    const-string v0, "create table note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default \'\' );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 68
    const-string v0, "create table resource_meta ( _id varchar(128) not null primary key, filename varchar(256) not null, length integer not null, version integer not null, is_dirty boolean not null, noteid varchar(128) not null, type integer not null default 0,props text not null default \'\' );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 69
    const-string v0, "create index resource_noteid_index on resource_meta ( noteid)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    const-string v0, "create index resource_index on resource_meta ( _id ) "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    const-string v0, "create table user ( _id varchar(128) not null primary key, used_space integer, quota_space integer, default_notebook varchar(128), last_push_time text not null, last_sync_time text, props text not null default \'\' );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "create table root_note_books ( _id varchar(128) primary key, version integer not null, create_time varchar(32), modify_time varchar(32), title varchar(512), note_number integer not null, is_dirty boolean not null, is_deleted boolean not null, last_sync_time varchar(32), type integer not null default 0,props text not null default \'\' );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    const-string v0, "create table cache ( _id varchar(128) primary key not null, touch_time varchar(32) not null, length varchar(32), item_type integer not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    const-string v0, "create index cache_type_index on cache ( item_type ) "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 75
    const-string v0, "create table content_version ( note_id varchar(128) primary key, content_version integer not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 80
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 88
    const-string v0, "alter table resources rename to resource_meta"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    const-string v0, "alter table notes rename to note_meta"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    const-string v0, "alter table resource_meta add type integer not null default 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 94
    const-string v0, "alter table resource_meta add props text not null default \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 96
    const-string v0, "alter table user add props text not null default \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 98
    const-string v0, "alter table note_books add type integer not null default 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    const-string v0, "alter table note_books add props text not null default \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 102
    const-string v0, "alter table root_note_books add type integer not null default 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 104
    const-string v0, "alter table root_note_books add props text not null default \'\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 107
    :cond_0
    return-void
.end method

.method public removeNoteContentVersion(Ljava/lang/String;)Z
    .locals 6
    .parameter "noteId"

    .prologue
    const/4 v5, 0x1

    .line 292
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "content_version"

    const-string v2, "note_id = ?"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 293
    return v5
.end method

.method public resetDataBase()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/youdao/note/datasource/database/YNoteDB;->innerResetDB()V

    .line 114
    return-void
.end method

.method public searchNotes(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .parameter "keyword"

    .prologue
    const/4 v2, 0x0

    .line 508
    invoke-direct {p0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 509
    .local v9, escapedKeyword:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "escaped keyword is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "note_meta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "title like \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%\' escape \'!\' and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is_deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "modify_time desc"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 520
    .local v8, cursor:Landroid/database/Cursor;
    return-object v8
.end method

.method public touchCacheItem(Ljava/lang/String;I)Z
    .locals 4
    .parameter "itemId"
    .parameter "type"

    .prologue
    .line 135
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 136
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v1, "item_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 138
    const-string v1, "touch_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 139
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "cache"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method public touchCacheItem(Ljava/lang/String;IJ)Z
    .locals 4
    .parameter "itemId"
    .parameter "type"
    .parameter "length"

    .prologue
    .line 126
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 127
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v1, "item_type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string v1, "length"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 130
    const-string v1, "touch_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 131
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "cache"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method public updateNoteContentVersion(Ljava/lang/String;I)V
    .locals 3
    .parameter "noteId"
    .parameter "contentVersion"

    .prologue
    .line 285
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 286
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "note_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v1, "content_version"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 288
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "content_version"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 289
    return-void
.end method

.method public updateRootNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z
    .locals 4
    .parameter "rootMeta"

    .prologue
    .line 443
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 444
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "_id"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v1, "create_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getCreateTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 446
    const-string v1, "modify_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getModifyTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 447
    const-string v1, "version"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 448
    const-string v1, "last_sync_time"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getLastSyncTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 449
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v1, "is_dirty"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->isDirty()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 451
    const-string v1, "note_number"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 452
    const-string v1, "is_deleted"

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 453
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "root_note_books"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method
