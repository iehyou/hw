.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$RESOURCE_META_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RESOURCE_META_TABLE"
.end annotation


# static fields
.field public static final COLUMN_DEF_PROPS:Ljava/lang/String; = "props text not null default \'\'"

.field public static final COLUMN_DEF_TYPE:Ljava/lang/String; = "type integer not null default 0"

.field public static final CREATE_RESOURCE_INDEX_SQL:Ljava/lang/String; = "create index resource_index on resource_meta ( _id ) "

.field public static final CREATE_RESOURCE_META_TABLE_SQL:Ljava/lang/String; = "create table resource_meta ( _id varchar(128) not null primary key, filename varchar(256) not null, length integer not null, version integer not null, is_dirty boolean not null, noteid varchar(128) not null, type integer not null default 0,props text not null default \'\' );"

.field public static final CREATE_RESOURCE_NOTE_ID_INDEX_SQL:Ljava/lang/String; = "create index resource_noteid_index on resource_meta ( noteid)"

.field public static final DROP_RESOURCE_META_TABLE_SQL:Ljava/lang/String; = "drop table if exists resource_meta"

.field public static final FILE_LENGTH:Ljava/lang/String; = "length"

.field public static final FILE_NAME:Ljava/lang/String; = "filename"

.field public static final IS_DIRTY:Ljava/lang/String; = "is_dirty"

.field public static final NOTEID:Ljava/lang/String; = "noteid"

.field public static final PROPS:Ljava/lang/String; = "props"

.field public static final RESOURCE_ID:Ljava/lang/String; = "_id"

.field public static final RESOURCE_ID_INDEX_NAME:Ljava/lang/String; = "resource_index"

.field public static final RESOURCE_NOTE_ID_INDEX_NAME:Ljava/lang/String; = "resource_noteid_index"

.field public static final TABLE_NAME:Ljava/lang/String; = "resource_meta"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VERSION:Ljava/lang/String; = "version"
