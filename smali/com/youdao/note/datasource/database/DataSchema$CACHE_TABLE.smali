.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$CACHE_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CACHE_TABLE"
.end annotation


# static fields
.field public static final CREATE_CACHE_TABLE_SQL:Ljava/lang/String; = "create table cache ( _id varchar(128) primary key not null, touch_time varchar(32) not null, length varchar(32), item_type integer not null);"

.field public static final CREATE_CACHE_TYPE_INDEX_SQL:Ljava/lang/String; = "create index cache_type_index on cache ( item_type ) "

.field public static final DROP_CACHE_TABLE_SQL:Ljava/lang/String; = "drop table if exists cache"

.field public static final ITEM_ID:Ljava/lang/String; = "_id"

.field public static final ITEM_TYPE:Ljava/lang/String; = "item_type"

.field public static final LENGTH:Ljava/lang/String; = "length"

.field public static final TABLE_NAME:Ljava/lang/String; = "cache"

.field public static final TOUCH_TIME:Ljava/lang/String; = "touch_time"

.field public static final TYPE_INDEX_NAME:Ljava/lang/String; = "cache_type_index"

.field public static final TYPE_NOTE:I = 0x1

.field public static final TYPE_RESOURCE:I = 0x2
