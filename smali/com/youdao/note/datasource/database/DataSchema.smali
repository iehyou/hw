.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema;
.super Ljava/lang/Object;
.source "DataSchema.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/datasource/database/DataSchema$SEARCH_HISTORY_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$USER_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$CACHE_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$RESOURCE_META_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$ROOT_NOTEBOOK_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$NOTE_BOOKS_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$NOTE_CONTENT_VERSION_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$NOTE_META_TABLE;,
        Lcom/youdao/note/datasource/database/DataSchema$BASE_NOTE_META_TABLE;
    }
.end annotation


# static fields
.field public static final DATABASE_VERSION:I = 0x2

.field public static final DATABASE_VERSION_1:I = 0x1

.field public static final DATABASE_VERSION_2:I = 0x2

.field public static final DROP_TABLE:Ljava/lang/String; = "drop table if exists "
