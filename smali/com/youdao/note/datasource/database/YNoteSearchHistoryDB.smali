.class public Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "YNoteSearchHistoryDB.java"

# interfaces
.implements Lcom/youdao/note/datasource/database/DataSchema;


# static fields
.field private static final MAX_SEARCH_HISTORY:I = 0x100


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 37
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getYNoteSearchHistryDBName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .parameter "context"
    .parameter "dataBaseName"

    .prologue
    const/4 v1, 0x0

    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v1, v0}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 26
    iput-object v1, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 43
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 44
    return-void
.end method

.method private innerResetDataBase()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "drop table if exists search_history"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 83
    return-void
.end method

.method private truncate()V
    .locals 8

    .prologue
    const/16 v3, 0x100

    .line 86
    invoke-virtual {p0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->getAllQuery()Landroid/database/Cursor;

    move-result-object v0

    .line 88
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v1, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v1, v0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 89
    .local v1, helper:Lcom/youdao/note/utils/CursorHelper;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 90
    const/16 v2, 0x100

    invoke-interface {v0, v2}, Landroid/database/Cursor;->move(I)Z

    .line 91
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "search_history"

    const-string v4, "_id"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    invoke-virtual {v1, v7}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    .end local v1           #helper:Lcom/youdao/note/utils/CursorHelper;
    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2

    .restart local v1       #helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 102
    return-void
.end method


# virtual methods
.method public clearHistory()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "search_history"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 73
    return-void
.end method

.method public getAllQuery()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "search_history"

    const-string v7, "timestamp desc"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter "db"

    .prologue
    .line 48
    const-string v0, "create table search_history ( _id integer primary key autoincrement, query text not null unique, timestamp integer not null);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    const-string v0, "YNoteSearchHistory table created."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->innerResetDataBase()V

    .line 55
    return-void
.end method

.method public resetDataBase()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->innerResetDataBase()V

    .line 78
    return-void
.end method

.method public saveQuery(Ljava/lang/String;)V
    .locals 6
    .parameter "query"

    .prologue
    .line 59
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 60
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 62
    iget-object v1, p0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "search_history"

    invoke-static {v1, v2, v0}, Lcom/youdao/note/datasource/database/DBUtils;->replaceValues(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 63
    invoke-direct {p0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->truncate()V

    .line 64
    return-void
.end method
