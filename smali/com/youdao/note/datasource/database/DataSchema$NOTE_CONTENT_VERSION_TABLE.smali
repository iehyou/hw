.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$NOTE_CONTENT_VERSION_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NOTE_CONTENT_VERSION_TABLE"
.end annotation


# static fields
.field public static final CONTENT_VERSION:Ljava/lang/String; = "content_version"

.field public static final CREATE_NOTES_CONTENT_TABLE_SQL:Ljava/lang/String; = "create table content_version ( note_id varchar(128) primary key, content_version integer not null);"

.field public static final DROP_NOTES_CONTENT_TABLE_SQL:Ljava/lang/String; = "drop table if exists content_version"

.field public static final NOTEID:Ljava/lang/String; = "note_id"

.field public static final TABLE_NAME:Ljava/lang/String; = "content_version"
