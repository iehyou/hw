.class public interface abstract Lcom/youdao/note/datasource/database/DataSchema$NOTE_META_TABLE;
.super Ljava/lang/Object;
.source "DataSchema.java"

# interfaces
.implements Lcom/youdao/note/datasource/database/DataSchema$BASE_NOTE_META_TABLE;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/database/DataSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NOTE_META_TABLE"
.end annotation


# static fields
.field public static final AUTHOR:Ljava/lang/String; = "author"

.field public static final CREATE_NOTEBOOK_INDEX_SQL:Ljava/lang/String; = "CREATE INDEX notes_on_notebook ON note_meta (notebook);"

.field public static final CREATE_NOTE_META_TABLE_SQL:Ljava/lang/String; = "create table note_meta ( _id varchar(128) primary key, server_notebook varchar(128), notebook varchar(128), create_time varchar(32), modify_time varchar(32), author varchar(64), props text not null, title varchar(512), version integer not null, last_sync_time varchar(32), source varchar(128), is_dirty boolean not null, is_deleted boolean not null, length integer not null);"

.field public static final DROP_NOTE_META_TABLE_SQL:Ljava/lang/String; = "drop table if exists note_meta"

.field public static final LENGTH:Ljava/lang/String; = "length"

.field public static final NOTEBOOK:Ljava/lang/String; = "notebook"

.field public static final NOTEBOOK_INDEX_NAME:Ljava/lang/String; = "notes_on_notebook"

.field public static final NOTEID:Ljava/lang/String; = "_id"

.field public static final PROPS:Ljava/lang/String; = "props"

.field public static final SERVER_NOTEBOOK:Ljava/lang/String; = "server_notebook"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final TABLE_NAME:Ljava/lang/String; = "note_meta"
