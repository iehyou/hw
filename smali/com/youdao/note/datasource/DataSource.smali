.class public Lcom/youdao/note/datasource/DataSource;
.super Ljava/lang/Object;
.source "DataSource.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$HTML;
.implements Lcom/youdao/note/utils/Consts;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

.field private doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

.field private generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

.field private handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

.field private imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

.field private mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

.field private mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

.field private mYNote:Lcom/youdao/note/YNoteApplication;

.field private mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

.field private mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

.field private snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

.field private tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

.field private thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/datasource/DataSource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    .line 79
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    .line 81
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

    .line 83
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    .line 85
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

    .line 87
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

    .line 89
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

    .line 91
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

    .line 93
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    .line 95
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

    .line 97
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

    .line 99
    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

    .line 104
    check-cast p1, Lcom/youdao/note/YNoteApplication;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 105
    return-void
.end method

.method private adjustNoteBookCount(Lcom/youdao/note/data/NoteBook;)Z
    .locals 2
    .parameter "noteBook"

    .prologue
    .line 575
    if-nez p1, :cond_0

    .line 576
    const/4 v0, 0x1

    .line 580
    :goto_0
    return v0

    .line 578
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookCount(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/youdao/note/data/NoteBook;->setNoteNumber(I)V

    .line 580
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    goto :goto_0
.end method

.method private adjustNotebookCount(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V
    .locals 1
    .parameter "noteMeta"
    .parameter "oriNotebookId"

    .prologue
    .line 560
    if-eqz p2, :cond_0

    .line 561
    invoke-virtual {p0, p2}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    .line 563
    :cond_0
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    .line 564
    return-void
.end method

.method private cursor2Array(Landroid/database/Cursor;Z)[Lcom/youdao/note/data/NoteMeta;
    .locals 6
    .parameter "cursor"
    .parameter "isNoteBook"

    .prologue
    .line 823
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Notes size is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 824
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p1}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 825
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    new-array v3, v4, [Lcom/youdao/note/data/NoteMeta;

    .line 826
    .local v3, noteMetas:[Lcom/youdao/note/data/NoteMeta;
    const/4 v1, 0x0

    .line 827
    .local v1, idx:I
    :goto_0
    invoke-virtual {v0}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 828
    add-int/lit8 v2, v1, 0x1

    .end local v1           #idx:I
    .local v2, idx:I
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    aput-object v4, v3, v1

    move v1, v2

    .end local v2           #idx:I
    .restart local v1       #idx:I
    goto :goto_0

    .line 830
    :cond_0
    return-object v3
.end method

.method private cursor2NoteBookArray(Landroid/database/Cursor;)[Lcom/youdao/note/data/NoteBook;
    .locals 6
    .parameter "cursor"

    .prologue
    .line 834
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Notes size is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 835
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p1}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 836
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    new-array v3, v4, [Lcom/youdao/note/data/NoteBook;

    .line 837
    .local v3, noteBooks:[Lcom/youdao/note/data/NoteBook;
    const/4 v1, 0x0

    .line 838
    .local v1, idx:I
    :goto_0
    invoke-virtual {v0}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 839
    add-int/lit8 v2, v1, 0x1

    .end local v1           #idx:I
    .local v2, idx:I
    invoke-static {v0}, Lcom/youdao/note/data/NoteBook;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteBook;

    move-result-object v4

    aput-object v4, v3, v1

    move v1, v2

    .end local v2           #idx:I
    .restart local v1       #idx:I
    goto :goto_0

    .line 841
    :cond_0
    return-object v3
.end method

.method private cursor2NoteBookList(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notes size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 856
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p1}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 857
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 858
    .local v1, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 859
    invoke-static {v0}, Lcom/youdao/note/data/NoteBook;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 861
    :cond_0
    return-object v1
.end method

.method private cursor2NoteMetaList(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 845
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notes size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 846
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p1}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 847
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 848
    .local v1, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 851
    :cond_0
    return-object v1
.end method

.method private deleteNotesInNotebook(Ljava/lang/String;)Z
    .locals 4
    .parameter "notebookId"

    .prologue
    .line 736
    const/4 v2, 0x1

    .line 737
    .local v2, succeed:Z
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteByNoteBook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 739
    .local v0, cursor:Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 740
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 741
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->deleteNoteById(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    and-int/2addr v2, v3

    .line 742
    goto :goto_0

    .line 744
    .end local v1           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 746
    return v2

    .line 744
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method private getSearchDb()Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;
    .locals 3

    .prologue
    .line 968
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    if-nez v0, :cond_1

    .line 969
    const-class v1, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    monitor-enter v1

    .line 970
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    if-nez v0, :cond_0

    .line 971
    new-instance v0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    .line 973
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 975
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    return-object v0

    .line 973
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getTmpResourcePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "path"

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private markDeleteNotesInNotebook(Ljava/lang/String;)Z
    .locals 4
    .parameter "notebookId"

    .prologue
    .line 750
    const/4 v2, 0x1

    .line 751
    .local v2, succeed:Z
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteByNoteBook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 753
    .local v0, cursor:Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 754
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 755
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->markDeleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    and-int/2addr v2, v3

    .line 756
    goto :goto_0

    .line 758
    .end local v1           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 760
    return v2

    .line 758
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3
.end method


# virtual methods
.method public adjustNoteBookCount(Ljava/lang/String;)Z
    .locals 2
    .parameter "noteBookId"

    .prologue
    .line 567
    if-eqz p1, :cond_0

    .line 568
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 569
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Lcom/youdao/note/data/NoteBook;)Z

    move-result v1

    .line 571
    .end local v0           #noteBook:Lcom/youdao/note/data/NoteBook;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public clearCache()Z
    .locals 2

    .prologue
    .line 889
    const/4 v0, 0x1

    .line 890
    .local v0, succeed:Z
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/NoteCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 891
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getImageCache()Lcom/youdao/note/datasource/localcache/ImageCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/ImageCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 892
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 893
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getSnippetCache()Lcom/youdao/note/datasource/localcache/SnippetCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/SnippetCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 894
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getHandwriteCache()Lcom/youdao/note/datasource/localcache/HandwriteCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/HandwriteCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 895
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDoodleCache()Lcom/youdao/note/datasource/localcache/DoodleCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/DoodleCache;->clearCache()Z

    move-result v1

    and-int/2addr v0, v1

    .line 896
    return v0
.end method

.method public clearResourcesInNote(Ljava/lang/String;)V
    .locals 4
    .parameter "noteId"

    .prologue
    .line 338
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 339
    .local v2, resources:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 340
    .local v1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 342
    .end local v1           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    return-void
.end method

.method public clearSearchHistory()V
    .locals 1

    .prologue
    .line 929
    invoke-direct {p0}, Lcom/youdao/note/datasource/DataSource;->getSearchDb()Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->clearHistory()V

    .line 930
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteDB;->close()V

    .line 940
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    if-eqz v0, :cond_1

    .line 941
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->close()V

    .line 943
    :cond_1
    return-void
.end method

.method public deleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 1
    .parameter "noteMeta"

    .prologue
    .line 632
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->deleteNoteById(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteNote(Ljava/lang/String;)Z
    .locals 2
    .parameter "entryPath"

    .prologue
    .line 621
    invoke-static {p1}, Lcom/youdao/note/utils/YNoteUtils;->splitEntryPath(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 622
    .local v0, tmpStrs:[Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNoteById(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public deleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "noteBookMeta"

    .prologue
    .line 732
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->deleteNoteBook(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteNoteBook(Ljava/lang/String;)Z
    .locals 1
    .parameter "noteBookId"

    .prologue
    .line 720
    invoke-direct {p0, p1}, Lcom/youdao/note/datasource/DataSource;->deleteNotesInNotebook(Ljava/lang/String;)Z

    .line 721
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteNoteBook(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteNoteById(Ljava/lang/String;)Z
    .locals 8
    .parameter "noteId"

    .prologue
    const/4 v5, 0x1

    .line 636
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 637
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {p0, v2}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 639
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 640
    .local v3, noteMeta:Lcom/youdao/note/data/NoteMeta;
    if-nez v3, :cond_2

    .line 653
    :cond_1
    :goto_1
    return v5

    .line 643
    :cond_2
    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v4

    .line 644
    .local v4, snippet:Lcom/youdao/note/data/Snippet;
    if-eqz v4, :cond_3

    .line 645
    invoke-virtual {v4}, Lcom/youdao/note/data/Snippet;->getRelativePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/youdao/note/datasource/DataSource;->deleteSnippet(Ljava/lang/String;)Z

    .line 648
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/youdao/note/datasource/localcache/NoteCache;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 649
    iget-object v6, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/datasource/database/YNoteDB;->removeNoteContentVersion(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 653
    :goto_2
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 650
    :catch_0
    move-exception v0

    .line 651
    .local v0, e:Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to delete cache for note "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/youdao/note/utils/L;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 3
    .parameter "meta"

    .prologue
    .line 347
    :try_start_0
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 348
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/datasource/DataSource;->getTmpResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 349
    invoke-static {p1}, Lcom/youdao/note/data/resource/ResourceUtils;->hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteResource(Lcom/youdao/note/data/resource/IResourceMeta;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :goto_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, e:Ljava/io/IOException;
    const-string v1, "delete resource failed"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public deleteSnippet(Ljava/lang/String;)Z
    .locals 3
    .parameter "relativePath"

    .prologue
    .line 241
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getSnippetCache()Lcom/youdao/note/datasource/localcache/SnippetCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/localcache/SnippetCache;->deleteCacheItem(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 244
    :goto_0
    return v1

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to delete snippet "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 244
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public existNoteMeta(Ljava/lang/String;)Z
    .locals 1
    .parameter "noteId"

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public existResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z
    .locals 2
    .parameter "meta"

    .prologue
    .line 407
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->exist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public existThumbnail(Ljava/lang/String;)Z
    .locals 1
    .parameter "relativePath"

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->exist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public exsitNoteBook(Ljava/lang/String;)Z
    .locals 2
    .parameter "noteBookId"

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 250
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllSearchHistory()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 911
    invoke-direct {p0}, Lcom/youdao/note/datasource/DataSource;->getSearchDb()Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->getAllQuery()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getAllSearchHistoryList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 915
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getAllSearchHistory()Landroid/database/Cursor;

    move-result-object v0

    .line 917
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 918
    .local v2, queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v1, v0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 919
    .local v1, helper:Lcom/youdao/note/utils/CursorHelper;
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 920
    const-string v3, "query"

    invoke-virtual {v1, v3}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 924
    .end local v1           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v2           #queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3

    .restart local v1       #helper:Lcom/youdao/note/utils/CursorHelper;
    .restart local v2       #queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method public getAudioCache()Lcom/youdao/note/datasource/localcache/AudioCache;
    .locals 3

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

    if-nez v0, :cond_1

    .line 1046
    const-class v1, Lcom/youdao/note/datasource/localcache/AudioCache;

    monitor-enter v1

    .line 1047
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

    if-nez v0, :cond_0

    .line 1048
    new-instance v0, Lcom/youdao/note/datasource/localcache/AudioCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/AudioCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

    .line 1050
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1052
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->audioCache:Lcom/youdao/note/datasource/localcache/AudioCache;

    return-object v0

    .line 1050
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getAvableNoteTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "noteTitle"

    .prologue
    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

    if-nez v0, :cond_1

    .line 980
    const-class v1, Lcom/youdao/note/datasource/localcache/CacheManager;

    monitor-enter v1

    .line 981
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

    if-nez v0, :cond_0

    .line 982
    new-instance v0, Lcom/youdao/note/datasource/localcache/CacheManager;

    invoke-direct {v0}, Lcom/youdao/note/datasource/localcache/CacheManager;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

    .line 984
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mCacheManager:Lcom/youdao/note/datasource/localcache/CacheManager;

    return-object v0

    .line 984
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDb()Lcom/youdao/note/datasource/database/YNoteDB;
    .locals 3

    .prologue
    .line 957
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    if-nez v0, :cond_1

    .line 958
    const-class v1, Lcom/youdao/note/datasource/database/YNoteDB;

    monitor-enter v1

    .line 959
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    if-nez v0, :cond_0

    .line 960
    new-instance v0, Lcom/youdao/note/datasource/database/YNoteDB;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/database/YNoteDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    .line 962
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    return-object v0

    .line 962
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDefaultNoteBookId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDirtyAndMovedNoteMetas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteDB;->getDirtyAndMovedNoteMetas()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getDirtyNoteBookMetas()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getDirtyNotebookMetas()Landroid/database/Cursor;

    move-result-object v0

    .line 129
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->cursor2NoteBookList(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 131
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public getDirtyResourcesOf(Lcom/youdao/note/data/NoteMeta;)Ljava/util/List;
    .locals 2
    .parameter "noteMeta"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/NoteMeta;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getDirtyResourcesOf(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getDoodleCache()Lcom/youdao/note/datasource/localcache/DoodleCache;
    .locals 3

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

    if-nez v0, :cond_1

    .line 1024
    const-class v1, Lcom/youdao/note/datasource/localcache/DoodleCache;

    monitor-enter v1

    .line 1025
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

    if-nez v0, :cond_0

    .line 1026
    new-instance v0, Lcom/youdao/note/datasource/localcache/DoodleCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/DoodleCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

    .line 1028
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1030
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->doodleCache:Lcom/youdao/note/datasource/localcache/DoodleCache;

    return-object v0

    .line 1028
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGeneralResourceCache()Lcom/youdao/note/datasource/localcache/GeneralResourceCache;
    .locals 3

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    if-nez v0, :cond_1

    .line 1079
    const-class v1, Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    monitor-enter v1

    .line 1080
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    if-nez v0, :cond_0

    .line 1081
    new-instance v0, Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/GeneralResourceCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    .line 1083
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1085
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->generalResourceCache:Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    return-object v0

    .line 1083
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getHandwriteCache()Lcom/youdao/note/datasource/localcache/HandwriteCache;
    .locals 3

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

    if-nez v0, :cond_1

    .line 1035
    const-class v1, Lcom/youdao/note/datasource/localcache/HandwriteCache;

    monitor-enter v1

    .line 1036
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

    if-nez v0, :cond_0

    .line 1037
    new-instance v0, Lcom/youdao/note/datasource/localcache/HandwriteCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/HandwriteCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

    .line 1039
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->handwriteCache:Lcom/youdao/note/datasource/localcache/HandwriteCache;

    return-object v0

    .line 1039
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getImageCache()Lcom/youdao/note/datasource/localcache/ImageCache;
    .locals 3

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

    if-nez v0, :cond_1

    .line 1057
    const-class v1, Lcom/youdao/note/datasource/localcache/ImageCache;

    monitor-enter v1

    .line 1058
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

    if-nez v0, :cond_0

    .line 1059
    new-instance v0, Lcom/youdao/note/datasource/localcache/ImageCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/ImageCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

    .line 1061
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1063
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->imageCache:Lcom/youdao/note/datasource/localcache/ImageCache;

    return-object v0

    .line 1061
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getImageResource(Lcom/youdao/note/data/resource/ImageResourceMeta;)Lcom/youdao/note/data/resource/ImageResource;
    .locals 1
    .parameter "meta"

    .prologue
    .line 152
    new-instance v0, Lcom/youdao/note/data/resource/ImageResource;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/resource/ImageResource;-><init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-object v0
.end method

.method public getImageResource(Ljava/lang/String;)Lcom/youdao/note/data/resource/ImageResource;
    .locals 5
    .parameter "resourceId"

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, p1, v4}, Lcom/youdao/note/datasource/localcache/CacheManager;->touchCacheItem(Ljava/lang/String;I)V

    .line 162
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourceById(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    .line 163
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    new-instance v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v0, v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 164
    .local v0, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    new-instance v1, Lcom/youdao/note/data/resource/ImageResource;

    invoke-direct {v1}, Lcom/youdao/note/data/resource/ImageResource;-><init>()V

    .line 165
    .local v1, imageResource:Lcom/youdao/note/data/resource/ImageResource;
    invoke-virtual {v1, v0}, Lcom/youdao/note/data/resource/ImageResource;->setMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 166
    return-object v1
.end method

.method public getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;
    .locals 7
    .parameter "noteMeta"

    .prologue
    const/4 v3, 0x0

    .line 422
    new-instance v1, Lcom/youdao/note/data/Note;

    invoke-direct {v1, p1, v3}, Lcom/youdao/note/data/Note;-><init>(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V

    .line 425
    .local v1, note:Lcom/youdao/note/data/Note;
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/youdao/note/datasource/localcache/NoteCache;->getNoteContent(Lcom/youdao/note/data/Note;)Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, noteContent:Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 427
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;

    move-result-object v4

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/youdao/note/datasource/localcache/CacheManager;->touchCacheItem(Ljava/lang/String;I)V

    .line 429
    invoke-virtual {v1, v2}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 438
    .end local v1           #note:Lcom/youdao/note/data/Note;
    .end local v2           #noteContent:Ljava/lang/String;
    :goto_0
    return-object v1

    .line 432
    .restart local v1       #note:Lcom/youdao/note/data/Note;
    .restart local v2       #noteContent:Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/youdao/note/datasource/database/YNoteDB;->removeNoteContentVersion(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v3

    .line 433
    goto :goto_0

    .line 436
    .end local v2           #noteContent:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 437
    .local v0, e:Ljava/io/IOException;
    const-string v4, "Failed to load note content from cache."

    invoke-static {p0, v4, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v3

    .line 438
    goto :goto_0
.end method

.method public getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;
    .locals 1
    .parameter "noteBook"

    .prologue
    .line 693
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method public getNoteBookMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;
    .locals 1
    .parameter "title"

    .prologue
    .line 703
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteBookMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method public getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;
    .locals 3

    .prologue
    .line 990
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

    if-nez v0, :cond_1

    .line 991
    const-class v1, Lcom/youdao/note/datasource/localcache/NoteCache;

    monitor-enter v1

    .line 992
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

    if-nez v0, :cond_0

    .line 993
    new-instance v0, Lcom/youdao/note/datasource/localcache/NoteCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/NoteCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

    .line 995
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 997
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mNoteCache:Lcom/youdao/note/datasource/localcache/NoteCache;

    return-object v0

    .line 995
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getNoteContentVersion(Ljava/lang/String;)I
    .locals 1
    .parameter "noteId"

    .prologue
    .line 865
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteContentVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;
    .locals 1
    .parameter "noteId"

    .prologue
    .line 678
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    return-object v0
.end method

.method public getNoteMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;
    .locals 1
    .parameter "title"

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    return-object v0
.end method

.method public getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;
    .locals 1
    .parameter "baseMeta"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ")",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    new-instance v0, Lcom/youdao/note/datasource/DataSource$1;

    invoke-direct {v0, p0, p1, p1}, Lcom/youdao/note/datasource/DataSource$1;-><init>(Lcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/resource/BaseResourceMeta;Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource$1;->doSwitch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/AbstractResource;

    return-object v0
.end method

.method public getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 1089
    new-instance v0, Lcom/youdao/note/datasource/DataSource$2;

    invoke-direct {v0, p0, p1}, Lcom/youdao/note/datasource/DataSource$2;-><init>(Lcom/youdao/note/datasource/DataSource;I)V

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource$2;->doSwitch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    return-object v0
.end method

.method public getResourceLength(Lcom/youdao/note/data/resource/BaseResourceMeta;)J
    .locals 3
    .parameter "meta"

    .prologue
    .line 170
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 171
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    return-wide v1
.end method

.method public getResourceMeta(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1
    .parameter "resourceId"

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourceById(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method public getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .parameter "noteId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourcesByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;
    .locals 3
    .parameter "meta"

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;

    move-result-object v0

    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/datasource/localcache/CacheManager;->touchCacheItem(Ljava/lang/String;I)V

    .line 148
    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRootMeta()Lcom/youdao/note/data/NoteBook;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteDB;->getRootMeta()Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method public getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;
    .locals 2
    .parameter "meta"

    .prologue
    .line 219
    new-instance v0, Lcom/youdao/note/data/Snippet;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/Snippet;-><init>(Lcom/youdao/note/data/NoteMeta;)V

    .line 220
    .local v0, snippet:Lcom/youdao/note/data/Snippet;
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->hasSnippet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    .end local v0           #snippet:Lcom/youdao/note/data/Snippet;
    :goto_0
    return-object v0

    .line 223
    .restart local v0       #snippet:Lcom/youdao/note/data/Snippet;
    :cond_0
    invoke-virtual {v0}, Lcom/youdao/note/data/Snippet;->exist()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    invoke-virtual {v0}, Lcom/youdao/note/data/Snippet;->getRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteSnippet(Ljava/lang/String;)Z

    .line 226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSnippetCache()Lcom/youdao/note/datasource/localcache/SnippetCache;
    .locals 3

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

    if-nez v0, :cond_1

    .line 1013
    const-class v1, Lcom/youdao/note/datasource/localcache/SnippetCache;

    monitor-enter v1

    .line 1014
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

    if-nez v0, :cond_0

    .line 1015
    new-instance v0, Lcom/youdao/note/datasource/localcache/SnippetCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/SnippetCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

    .line 1017
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1019
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->snippetCache:Lcom/youdao/note/datasource/localcache/SnippetCache;

    return-object v0

    .line 1017
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getTempFileCache()Lcom/youdao/note/datasource/localcache/TempFileCache;
    .locals 3

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

    if-nez v0, :cond_1

    .line 1068
    const-class v1, Lcom/youdao/note/datasource/localcache/TempFileCache;

    monitor-enter v1

    .line 1069
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

    if-nez v0, :cond_0

    .line 1070
    new-instance v0, Lcom/youdao/note/datasource/localcache/TempFileCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/TempFileCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

    .line 1072
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->tempFileCache:Lcom/youdao/note/datasource/localcache/TempFileCache;

    return-object v0

    .line 1072
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getThumbnail(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)Lcom/youdao/note/data/Thumbnail;
    .locals 1
    .parameter "meta"

    .prologue
    .line 236
    new-instance v0, Lcom/youdao/note/data/Thumbnail;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/Thumbnail;-><init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V

    return-object v0
.end method

.method public getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;
    .locals 3

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    if-nez v0, :cond_1

    .line 1002
    const-class v1, Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    monitor-enter v1

    .line 1003
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    if-nez v0, :cond_0

    .line 1004
    new-instance v0, Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    iget-object v2, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v2}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    .line 1006
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->thumbnailCache:Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    return-object v0

    .line 1006
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;
    .locals 2
    .parameter "meta"

    .prologue
    .line 116
    invoke-static {p1}, Lcom/youdao/note/data/resource/ResourceUtils;->hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v0

    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lcom/youdao/note/data/resource/IResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->getResourceIconPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUserMeta()Lcom/youdao/note/data/UserMeta;
    .locals 2

    .prologue
    .line 715
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getUserMeta(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    return-object v0
.end method

.method public insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z
    .locals 1
    .parameter "note"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 499
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->updateNoteCache(Lcom/youdao/note/data/Note;)Z

    move-result v0

    .line 502
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertOrUpdateNote(Lcom/youdao/note/data/Note;Ljava/lang/String;)Z
    .locals 2
    .parameter "note"
    .parameter "oriNoteBookId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 526
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    .line 527
    .local v0, oriNoteMeta:Lcom/youdao/note/data/NoteMeta;
    if-eqz v0, :cond_0

    .line 528
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/youdao/note/datasource/localcache/NoteCache;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 530
    :cond_0
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 531
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/youdao/note/datasource/DataSource;->adjustNotebookCount(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V

    .line 532
    const/4 v1, 0x1

    .line 534
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "noteBookMeta"

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    return v0
.end method

.method public insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 4
    .parameter "noteMeta"

    .prologue
    .line 512
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    .line 513
    .local v0, oriNoteMeta:Lcom/youdao/note/data/NoteMeta;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 517
    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v1

    .line 518
    .local v1, snippet:Lcom/youdao/note/data/Snippet;
    if-eqz v1, :cond_0

    .line 519
    invoke-virtual {v1}, Lcom/youdao/note/data/Snippet;->getRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/youdao/note/datasource/DataSource;->deleteSnippet(Ljava/lang/String;)Z

    .line 522
    .end local v1           #snippet:Lcom/youdao/note/data/Snippet;
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v2

    return v2
.end method

.method public insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)Z
    .locals 5
    .parameter "noteMeta"
    .parameter "oriNotebookId"

    .prologue
    const/4 v4, 0x0

    .line 539
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    .line 540
    .local v2, oriNoteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {p0, v2}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v1

    .line 541
    .local v1, note:Lcom/youdao/note/data/Note;
    if-eqz v1, :cond_2

    .line 543
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/youdao/note/data/Note;->setNoteMeta(Lcom/youdao/note/data/NoteMeta;)V

    .line 544
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/youdao/note/datasource/localcache/NoteCache;->updateNote(Lcom/youdao/note/data/Note;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 556
    :cond_0
    :goto_0
    return v4

    .line 547
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/youdao/note/datasource/localcache/NoteCache;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 553
    :cond_2
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 554
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/datasource/DataSource;->adjustNotebookCount(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V

    goto :goto_0

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, e:Ljava/io/IOException;
    const-string v3, "Failed to move note content."

    invoke-static {p0, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResourceMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->updateResourceCache(Lcom/youdao/note/data/resource/AbstractResource;)Z

    move-result v0

    .line 265
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertOrUpdateResourceMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z
    .locals 1
    .parameter "meta"

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v0

    return v0
.end method

.method public insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z
    .locals 1
    .parameter "userName"
    .parameter "userMeta"

    .prologue
    .line 880
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z

    move-result v0

    return v0
.end method

.method public listAllNoteBooks()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteDB;->getAllNoteBookMetasCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public listAllNoteBooksAsArray()[Lcom/youdao/note/data/NoteBook;
    .locals 2

    .prologue
    .line 777
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->listAllNoteBooks()Landroid/database/Cursor;

    move-result-object v0

    .line 779
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->cursor2NoteBookArray(Landroid/database/Cursor;)[Lcom/youdao/note/data/NoteBook;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 781
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public listAllNoteBooksAsList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 786
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->listAllNoteBooks()Landroid/database/Cursor;

    move-result-object v0

    .line 788
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->cursor2NoteBookList(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 790
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public listAllNotes()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 800
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getAllNoteMetasCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 801
    .local v0, cursor:Landroid/database/Cursor;
    return-object v0
.end method

.method public listAllNotesAsArray()[Lcom/youdao/note/data/NoteMeta;
    .locals 2

    .prologue
    .line 805
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->listAllNotes()Landroid/database/Cursor;

    move-result-object v0

    .line 807
    .local v0, cursor:Landroid/database/Cursor;
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/youdao/note/datasource/DataSource;->cursor2Array(Landroid/database/Cursor;Z)[Lcom/youdao/note/data/NoteMeta;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 809
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public listNoteByNotebook(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .parameter "noteBookId"

    .prologue
    .line 764
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteByNoteBook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public listNotesByNotebookAsList(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .parameter "noteBookId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->listNoteByNotebook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 816
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->cursor2NoteMetaList(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 818
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public markDeleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 1
    .parameter "noteMeta"

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->markDeleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 665
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    .line 667
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public markDeleteNotebook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "noteBook"

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->markDeleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 672
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/youdao/note/datasource/DataSource;->markDeleteNotesInNotebook(Ljava/lang/String;)Z

    move-result v0

    .line 674
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveNotesBetweenNoteBook(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .parameter "noteBookIdFrom"
    .parameter "noteBookIdTo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/youdao/note/exceptions/NoteBookNotExistException;
        }
    .end annotation

    .prologue
    .line 455
    invoke-virtual {p0, p2}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v4

    .line 456
    .local v4, toNoteBook:Lcom/youdao/note/data/NoteBook;
    if-nez v4, :cond_0

    .line 457
    new-instance v5, Lcom/youdao/note/exceptions/NoteBookNotExistException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " not exist"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/youdao/note/exceptions/NoteBookNotExistException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 459
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteByNoteBook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 461
    .local v0, cursor:Landroid/database/Cursor;
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 462
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 463
    .local v3, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v2

    .line 464
    .local v2, note:Lcom/youdao/note/data/Note;
    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v5

    if-nez v5, :cond_2

    .line 467
    invoke-virtual {v3, p2}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/youdao/note/datasource/localcache/NoteCache;->deleteNote(Lcom/youdao/note/data/Note;)Z

    .line 471
    invoke-virtual {v3, p2}, Lcom/youdao/note/data/NoteMeta;->setNoteBook(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/youdao/note/datasource/localcache/NoteCache;->updateNote(Lcom/youdao/note/data/Note;)Z

    .line 473
    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    if-nez v5, :cond_1

    .line 474
    const/4 v5, 0x0

    .line 483
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 487
    .end local v2           #note:Lcom/youdao/note/data/Note;
    .end local v3           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :goto_1
    return v5

    .line 476
    .restart local v2       #note:Lcom/youdao/note/data/Note;
    .restart local v3       #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :catch_0
    move-exception v1

    .line 477
    .local v1, e:Ljava/io/IOException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to move note "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 479
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 483
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #note:Lcom/youdao/note/data/Note;
    .end local v3           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 485
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    .line 486
    invoke-virtual {p0, p2}, Lcom/youdao/note/datasource/DataSource;->adjustNoteBookCount(Ljava/lang/String;)Z

    .line 487
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public reGenerateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 4
    .parameter "noteMeta"

    .prologue
    .line 588
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v2

    .line 589
    .local v2, note:Lcom/youdao/note/data/Note;
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNoteId()Ljava/lang/String;

    move-result-object v1

    .line 590
    .local v1, newNoteId:Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/youdao/note/data/NoteMeta;->setNoteId(Ljava/lang/String;)V

    .line 591
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V

    .line 592
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 593
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/youdao/note/datasource/DataSource;->getAvableNoteTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/youdao/note/data/NoteMeta;->setTitle(Ljava/lang/String;)V

    .line 594
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    .line 596
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/youdao/note/datasource/localcache/NoteCache;->updateNote(Lcom/youdao/note/data/Note;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 599
    :goto_0
    return v3

    .line 597
    :catch_0
    move-exception v0

    .line 598
    .local v0, e:Ljava/io/IOException;
    const-string v3, "regenerate note cache failed."

    invoke-static {p0, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 599
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public removeNoteContentVersion(Ljava/lang/String;)Z
    .locals 1
    .parameter "noteId"

    .prologue
    .line 869
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->removeNoteContentVersion(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public resetDataBase()Z
    .locals 3

    .prologue
    .line 901
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/database/YNoteDB;->resetDataBase()V

    .line 902
    invoke-direct {p0}, Lcom/youdao/note/datasource/DataSource;->getSearchDb()Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->resetDataBase()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    const/4 v1, 0x1

    .line 906
    :goto_0
    return v1

    .line 904
    :catch_0
    move-exception v0

    .line 905
    .local v0, e:Ljava/lang/Exception;
    sget-object v1, Lcom/youdao/note/datasource/DataSource;->TAG:Ljava/lang/String;

    const-string v2, "Failed to reset data base."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 906
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public saveSearchQuery(Ljava/lang/String;)V
    .locals 1
    .parameter "query"

    .prologue
    .line 933
    invoke-direct {p0}, Lcom/youdao/note/datasource/DataSource;->getSearchDb()Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->saveQuery(Ljava/lang/String;)V

    .line 934
    return-void
.end method

.method public searchNotes(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;
    .locals 4
    .parameter "keyword"

    .prologue
    .line 608
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->searchNotes(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 610
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Lcom/youdao/note/data/ListNoteMetas;

    invoke-direct {v2}, Lcom/youdao/note/data/ListNoteMetas;-><init>()V

    .line 611
    .local v2, ret:Lcom/youdao/note/data/ListNoteMetas;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/youdao/note/datasource/DataSource;->cursor2Array(Landroid/database/Cursor;Z)[Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 612
    .local v1, noteMetas:[Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v2, v1}, Lcom/youdao/note/data/ListNoteMetas;->setFileMetas([Lcom/youdao/note/data/NoteMeta;)V

    .line 613
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/data/ListNoteMetas;->setTotalNumber(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v2

    .end local v1           #noteMetas:[Lcom/youdao/note/data/NoteMeta;
    .end local v2           #ret:Lcom/youdao/note/data/ListNoteMetas;
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method public switchUser()V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    if-eqz v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteDB;->close()V

    .line 949
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    if-eqz v0, :cond_1

    .line 950
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;->close()V

    .line 952
    :cond_1
    new-instance v0, Lcom/youdao/note/datasource/database/YNoteDB;

    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v1}, Lcom/youdao/note/datasource/database/YNoteDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteDb:Lcom/youdao/note/datasource/database/YNoteDB;

    .line 953
    new-instance v0, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    iget-object v1, p0, Lcom/youdao/note/datasource/DataSource;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-direct {v0, v1}, Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/datasource/DataSource;->mYNoteSearchDb:Lcom/youdao/note/datasource/database/YNoteSearchHistoryDB;

    .line 954
    return-void
.end method

.method public updateNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "noteBook"

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    return v0
.end method

.method public updateNoteCache(Lcom/youdao/note/data/Note;)Z
    .locals 6
    .parameter "note"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/localcache/NoteCache;->updateNote(Lcom/youdao/note/data/Note;)Z

    move-result v0

    .line 386
    .local v0, ret:Z
    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/datasource/database/YNoteDB;->updateNoteContentVersion(Ljava/lang/String;I)V

    .line 389
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getContentBytes()[B

    move-result-object v4

    array-length v4, v4

    int-to-long v4, v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->touchCacheItem(Ljava/lang/String;IJ)V

    .line 392
    :cond_0
    return v0
.end method

.method public updateNoteFromCache(Lcom/youdao/note/data/Note;)Z
    .locals 3
    .parameter "note"

    .prologue
    .line 397
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/localcache/NoteCache;->getNoteContent(Lcom/youdao/note/data/Note;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update note from cache "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    const/4 v1, 0x1

    .line 402
    :goto_0
    return v1

    .line 400
    :catch_0
    move-exception v0

    .line 401
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to update note from cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 402
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateResourceCache(Lcom/youdao/note/data/resource/AbstractResource;)Z
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 359
    .local p1, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->isDataEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 361
    :try_start_0
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z

    move-result v1

    .line 363
    .local v1, ret:Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update resrouce cache succeed. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    if-eqz v1, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getCacheManager()Lcom/youdao/note/datasource/localcache/CacheManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractResource;->getLength()J

    move-result-wide v5

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/youdao/note/datasource/localcache/CacheManager;->touchCacheItem(Ljava/lang/String;IJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    .end local v1           #ret:Z
    :cond_0
    :goto_0
    return v1

    .line 372
    :catch_0
    move-exception v0

    .line 373
    .local v0, e:Ljava/io/IOException;
    const-string v2, "Failed to update resource cache."

    invoke-static {p0, v2, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 374
    const/4 v1, 0x0

    goto :goto_0

    .line 377
    .end local v0           #e:Ljava/io/IOException;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateRootNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z
    .locals 3
    .parameter "rootMeta"

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/database/YNoteDB;->updateRootNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    .line 180
    .local v0, ret:Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update root noet book meta ret is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    return v0
.end method

.method public writeSnippet(Lcom/youdao/note/data/Snippet;)Z
    .locals 2
    .parameter "snippet"

    .prologue
    .line 207
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getSnippetCache()Lcom/youdao/note/datasource/localcache/SnippetCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/localcache/SnippetCache;->updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 210
    :goto_0
    return v1

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "Failed to update snippet cache."

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 210
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeThumbnail(Lcom/youdao/note/data/Thumbnail;)Z
    .locals 2
    .parameter "thumbnail"

    .prologue
    .line 186
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 189
    :goto_0
    return v1

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, e:Ljava/io/IOException;
    const-string v1, "Failed to update thumbnail cache."

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 189
    const/4 v1, 0x0

    goto :goto_0
.end method
