.class Lcom/youdao/note/datasource/DataSource$2;
.super Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.source "DataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/ResourceMetaSwitcher",
        "<",
        "Lcom/youdao/note/datasource/localcache/BaseResourceCache;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/datasource/DataSource;


# direct methods
.method constructor <init>(Lcom/youdao/note/datasource/DataSource;I)V
    .locals 0
    .parameter
    .parameter "x0"

    .prologue
    .line 1089
    iput-object p1, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {p0, p2}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected onAudioType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getAudioCache()Lcom/youdao/note/datasource/localcache/AudioCache;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onAudioType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$2;->onAudioType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    return-object v0
.end method

.method protected onDoodleType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getDoodleCache()Lcom/youdao/note/datasource/localcache/DoodleCache;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onDoodleType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$2;->onDoodleType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    return-object v0
.end method

.method protected onGeneralType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getGeneralResourceCache()Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onGeneralType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$2;->onGeneralType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    return-object v0
.end method

.method protected onHandwriteType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getHandwriteCache()Lcom/youdao/note/datasource/localcache/HandwriteCache;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onHandwriteType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$2;->onHandwriteType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    return-object v0
.end method

.method protected onImageType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/youdao/note/datasource/DataSource$2;->this$0:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getImageCache()Lcom/youdao/note/datasource/localcache/ImageCache;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onImageType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1089
    invoke-virtual {p0}, Lcom/youdao/note/datasource/DataSource$2;->onImageType()Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v0

    return-object v0
.end method
