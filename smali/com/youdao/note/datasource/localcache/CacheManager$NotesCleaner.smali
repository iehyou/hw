.class Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;
.super Ljava/lang/Thread;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/localcache/CacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotesCleaner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/datasource/localcache/CacheManager;


# direct methods
.method private constructor <init>(Lcom/youdao/note/datasource/localcache/CacheManager;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/datasource/localcache/CacheManager;Lcom/youdao/note/datasource/localcache/CacheManager$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;-><init>(Lcom/youdao/note/datasource/localcache/CacheManager;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 91
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    const-string v10, "Start to clean notes."

    invoke-static {v9, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/youdao/note/datasource/database/YNoteDB;->getCacheItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 94
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v2, v0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 96
    .local v2, helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/database/YNoteDB;->getCachedNoteCount()I

    move-result v9

    const/16 v10, 0x1f4

    if-le v9, v10, :cond_4

    invoke-virtual {v2}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 97
    const-string v9, "_id"

    invoke-virtual {v2, v9}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 98
    .local v6, noteId:Ljava/lang/String;
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/youdao/note/datasource/database/YNoteDB;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v7

    .line 99
    .local v7, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v7}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 100
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourcesByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 101
    .local v5, metas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 102
    .local v4, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 103
    invoke-static {v4}, Lcom/youdao/note/data/resource/ResourceUtils;->hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 104
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->deleteCacheItem(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 116
    .end local v2           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v5           #metas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    .end local v6           #noteId:Ljava/lang/String;
    .end local v7           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :catch_0
    move-exception v1

    .line 117
    .local v1, e:Ljava/lang/Exception;
    :try_start_1
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    const-string v10, "Failed to clean cache notes."

    invoke-static {v9, v10, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 121
    .end local v1           #e:Ljava/lang/Exception;
    :goto_2
    return-void

    .line 107
    .restart local v2       #helper:Lcom/youdao/note/utils/CursorHelper;
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #metas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    .restart local v6       #noteId:Ljava/lang/String;
    .restart local v7       #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_2
    :try_start_2
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/youdao/note/datasource/localcache/NoteCache;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 108
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v8

    .line 109
    .local v8, snippet:Lcom/youdao/note/data/Snippet;
    if-eqz v8, :cond_3

    .line 110
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v8}, Lcom/youdao/note/data/Snippet;->getRelativePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/youdao/note/datasource/DataSource;->deleteSnippet(Ljava/lang/String;)Z

    .line 112
    :cond_3
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v9}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v9

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteCacheItem(Ljava/lang/String;)V

    .line 113
    iget-object v9, p0, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Clean one note "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 119
    .end local v2           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #metas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    .end local v6           #noteId:Ljava/lang/String;
    .end local v7           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    .end local v8           #snippet:Lcom/youdao/note/data/Snippet;
    :catchall_0
    move-exception v9

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v9

    .restart local v2       #helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method
