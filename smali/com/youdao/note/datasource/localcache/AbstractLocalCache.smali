.class public abstract Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
.super Ljava/lang/Object;
.source "AbstractLocalCache.java"


# static fields
.field private static final ROOT_STORE:Ljava/lang/String; = "YoudaoNote"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    const-class v2, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->TAG:Ljava/lang/String;

    .line 34
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getStoreDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "YoudaoNote/.nomedia"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 35
    .local v1, ignoreFile:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, e:Ljava/io/IOException;
    const-class v2, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;

    const-string v3, "Failed to create ignore file."

    invoke-static {v2, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 45
    check-cast p1, Lcom/youdao/note/YNoteApplication;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 46
    return-void
.end method


# virtual methods
.method public clearCache()Z
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getDataPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->deleteDirectory(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteCacheItem(Ljava/lang/String;)Z
    .locals 2
    .parameter "relativePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, filePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->deleteFile(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public exist(Ljava/lang/String;)Z
    .locals 5
    .parameter "relativePath"

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, filePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->exist(Ljava/lang/String;)Z

    move-result v1

    .line 92
    .local v1, ret:Z
    sget-object v2, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " exist is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    return v1
.end method

.method public getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "relativePath"

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getDataPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCacheItemAsBytes(Ljava/lang/String;)[B
    .locals 2
    .parameter "relativePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, filePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->readFromFileAsBytes(Ljava/lang/String;)[B

    move-result-object v1

    return-object v1
.end method

.method protected getCacheItemAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "relativePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, filePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->readFromFileAsStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected abstract getDataName()Ljava/lang/String;
.end method

.method public getDataPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getYNoteCachePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getDataName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPathCreateIfNotExist(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "pathStr"

    .prologue
    .line 55
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, path:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 57
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 59
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getYNoteCachePath()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getStoreDir()Ljava/io/File;

    move-result-object v0

    .line 50
    .local v0, dataDir:Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/YoudaoNote"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, yNoteDirPath:Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getPathCreateIfNotExist(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z
    .locals 4
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-interface {p1}, Lcom/youdao/note/data/ICacheable;->getRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, filePath:Ljava/lang/String;
    invoke-interface {p1}, Lcom/youdao/note/data/ICacheable;->getContentBytes()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->saveToFile(Ljava/lang/String;[B)V

    .line 69
    sget-object v1, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Succeed to update cache item "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const/4 v1, 0x1

    return v1
.end method
