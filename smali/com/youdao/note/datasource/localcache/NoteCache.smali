.class public Lcom/youdao/note/datasource/localcache/NoteCache;
.super Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
.source "NoteCache.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method


# virtual methods
.method public deleteNote(Lcom/youdao/note/data/Note;)Z
    .locals 1
    .parameter "note"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getRelativePath()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->deleteCacheItem(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteNote(Lcom/youdao/note/data/NoteMeta;)Z
    .locals 1
    .parameter "noteMeta"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->deleteCacheItem(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected getDataName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, "Note"

    return-object v0
.end method

.method public getNoteContent(Lcom/youdao/note/data/Note;)Ljava/lang/String;
    .locals 1
    .parameter "note"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getRelativePath()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getCacheItemAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateNote(Lcom/youdao/note/data/Note;)Z
    .locals 1
    .parameter "note"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z

    move-result v0

    return v0
.end method
