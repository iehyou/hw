.class public Lcom/youdao/note/datasource/localcache/CacheManager;
.super Ljava/lang/Object;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/datasource/localcache/CacheManager$1;,
        Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;,
        Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;
    }
.end annotation


# static fields
.field public static final MAX_CACHE_NOTES_NUMBER:I = 0x1f4

.field public static MAX_CACHE_RESOURCE_SIZE:J


# instance fields
.field private dataSource:Lcom/youdao/note/datasource/DataSource;

.field private lock:Ljava/lang/Object;

.field private mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

.field private mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-wide/32 v0, 0xc800000

    sput-wide v0, Lcom/youdao/note/datasource/localcache/CacheManager;->MAX_CACHE_RESOURCE_SIZE:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    .line 40
    iput-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    .line 42
    iput-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->lock:Ljava/lang/Object;

    .line 124
    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    return-object v0
.end method

.method private checkNotesCount()V
    .locals 4

    .prologue
    .line 74
    iget-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/datasource/database/YNoteDB;->getCachedNoteCount()I

    move-result v0

    .line 75
    .local v0, count:I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Total cached notes size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    const/16 v1, 0x1f4

    if-le v0, v1, :cond_2

    .line 77
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->isAlive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 79
    :cond_0
    new-instance v1, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;-><init>(Lcom/youdao/note/datasource/localcache/CacheManager;Lcom/youdao/note/datasource/localcache/CacheManager$1;)V

    iput-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    .line 80
    iget-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->setDaemon(Z)V

    .line 81
    iget-object v1, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->start()V

    .line 83
    :cond_1
    monitor-exit v2

    .line 85
    :cond_2
    return-void

    .line 83
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private checkResourcesSize()V
    .locals 5

    .prologue
    .line 60
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/datasource/database/YNoteDB;->getCachedResourceSize()J

    move-result-wide v0

    .line 61
    .local v0, size:J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Total cached resources size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UnitUtils;->getSizeInMegaByte(J)D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " M"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-wide v2, Lcom/youdao/note/datasource/localcache/CacheManager;->MAX_CACHE_RESOURCE_SIZE:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 63
    iget-object v3, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 64
    :try_start_0
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    invoke-virtual {v2}, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->isAlive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 65
    :cond_0
    new-instance v2, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    invoke-direct {v2, p0}, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;-><init>(Lcom/youdao/note/datasource/localcache/CacheManager;)V

    iput-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    .line 66
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->setDaemon(Z)V

    .line 67
    iget-object v2, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    invoke-virtual {v2}, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->start()V

    .line 69
    :cond_1
    monitor-exit v3

    .line 71
    :cond_2
    return-void

    .line 69
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method


# virtual methods
.method public isCleaning()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mNotesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/localcache/CacheManager$NotesCleaner;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->mResourcesCleaner:Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public touchCacheItem(Ljava/lang/String;I)V
    .locals 1
    .parameter "itemId"
    .parameter "cacheType"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/youdao/note/datasource/database/YNoteDB;->touchCacheItem(Ljava/lang/String;I)Z

    .line 57
    return-void
.end method

.method public touchCacheItem(Ljava/lang/String;IJ)V
    .locals 1
    .parameter "itemId"
    .parameter "cacheType"
    .parameter "length"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/datasource/database/YNoteDB;->touchCacheItem(Ljava/lang/String;IJ)Z

    .line 48
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 49
    invoke-direct {p0}, Lcom/youdao/note/datasource/localcache/CacheManager;->checkNotesCount()V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/youdao/note/datasource/localcache/CacheManager;->checkResourcesSize()V

    goto :goto_0
.end method
