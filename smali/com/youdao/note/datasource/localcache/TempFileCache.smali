.class public Lcom/youdao/note/datasource/localcache/TempFileCache;
.super Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
.source "TempFileCache.java"


# static fields
.field public static final SUFFIX_NOTE_SNAPSHOT:Ljava/lang/String; = "-note-snapshot.jpg"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method


# virtual methods
.method public clean()V
    .locals 7

    .prologue
    .line 46
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/TempFileCache;->getDataPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    .local v1, dir:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 48
    .local v3, files:[Ljava/io/File;
    if-nez v3, :cond_1

    .line 54
    :cond_0
    return-void

    .line 51
    :cond_1
    move-object v0, v3

    .local v0, arr$:[Ljava/io/File;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 52
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 51
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public clean(Ljava/lang/String;)V
    .locals 7
    .parameter "suffix"

    .prologue
    .line 33
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/youdao/note/datasource/localcache/TempFileCache;->getDataPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .local v1, dir:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 35
    .local v3, files:[Ljava/io/File;
    if-nez v3, :cond_1

    .line 43
    :cond_0
    return-void

    .line 38
    :cond_1
    move-object v0, v3

    .local v0, arr$:[Ljava/io/File;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 39
    .local v2, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 40
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 38
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected getDataName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const-string v0, "temp"

    return-object v0
.end method

.method public getTempFile(Ljava/lang/String;)Lcom/youdao/note/data/TempFile;
    .locals 1
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/youdao/note/data/TempFile;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/TempFile;-><init>(Ljava/lang/String;)V

    .line 29
    .local v0, tempFile:Lcom/youdao/note/data/TempFile;
    return-object v0
.end method

.method public updateTempFile(Lcom/youdao/note/data/TempFile;)V
    .locals 0
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->updateCacheItem(Lcom/youdao/note/data/ICacheable;)Z

    .line 25
    return-void
.end method
