.class public Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;
.super Ljava/lang/Thread;
.source "CacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/datasource/localcache/CacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ResourcesCleaner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/datasource/localcache/CacheManager;


# direct methods
.method public constructor <init>(Lcom/youdao/note/datasource/localcache/CacheManager;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 127
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    const-string v6, "Start to clearn resources"

    invoke-static {v5, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lcom/youdao/note/datasource/database/YNoteDB;->getCacheItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 130
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v2, v0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 132
    .local v2, helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/database/YNoteDB;->getCachedResourceSize()J

    move-result-wide v5

    sget-wide v7, Lcom/youdao/note/datasource/localcache/CacheManager;->MAX_CACHE_RESOURCE_SIZE:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    invoke-virtual {v2}, Lcom/youdao/note/utils/CursorHelper;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 133
    const-string v5, "_id"

    invoke-virtual {v2, v5}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, resourceId:Ljava/lang/String;
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/youdao/note/datasource/database/YNoteDB;->getResourceById(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    .line 135
    .local v3, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->isDirty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 136
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/youdao/note/datasource/DataSource;->getResourceCache(I)Lcom/youdao/note/datasource/localcache/BaseResourceCache;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/youdao/note/datasource/localcache/BaseResourceCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 137
    invoke-static {v3}, Lcom/youdao/note/data/resource/ResourceUtils;->hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 138
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/youdao/note/datasource/localcache/ThumbnailCache;->deleteCacheItem(Ljava/lang/String;)Z

    .line 140
    :cond_1
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    #getter for: Lcom/youdao/note/datasource/localcache/CacheManager;->dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v5}, Lcom/youdao/note/datasource/localcache/CacheManager;->access$100(Lcom/youdao/note/datasource/localcache/CacheManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/datasource/DataSource;->getDb()Lcom/youdao/note/datasource/database/YNoteDB;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/youdao/note/datasource/database/YNoteDB;->deleteCacheItem(Ljava/lang/String;)V

    .line 141
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Clean one resource "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 145
    .end local v2           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v3           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v4           #resourceId:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 146
    .local v1, e:Ljava/lang/Exception;
    :try_start_1
    iget-object v5, p0, Lcom/youdao/note/datasource/localcache/CacheManager$ResourcesCleaner;->this$0:Lcom/youdao/note/datasource/localcache/CacheManager;

    const-string v6, "Failed to clean resources."

    invoke-static {v5, v6, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 150
    .end local v1           #e:Ljava/lang/Exception;
    :goto_1
    return-void

    .line 148
    .restart local v2       #helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v2           #helper:Lcom/youdao/note/utils/CursorHelper;
    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5
.end method
