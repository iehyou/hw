.class public Lcom/youdao/note/exceptions/ResourceTooLargeException;
.super Ljava/lang/Exception;
.source "ResourceTooLargeException.java"


# static fields
.field private static final serialVersionUID:J = -0x4c3d19783ecc90b1L


# instance fields
.field private fileName:Ljava/lang/String;

.field private fileSize:J


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .parameter "fileName"
    .parameter "fileSize"

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/youdao/note/exceptions/ResourceTooLargeException;->fileName:Ljava/lang/String;

    .line 16
    iput-wide p2, p0, Lcom/youdao/note/exceptions/ResourceTooLargeException;->fileSize:J

    .line 17
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/youdao/note/exceptions/ResourceTooLargeException;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/youdao/note/exceptions/ResourceTooLargeException;->fileSize:J

    return-wide v0
.end method
