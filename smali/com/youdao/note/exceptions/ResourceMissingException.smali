.class public Lcom/youdao/note/exceptions/ResourceMissingException;
.super Ljava/lang/Exception;
.source "ResourceMissingException.java"


# static fields
.field private static final serialVersionUID:J = -0xd68e50566676dd1L


# instance fields
.field private mNoteTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .parameter "noteTitle"
    .parameter "e"

    .prologue
    .line 24
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/exceptions/ResourceMissingException;->mNoteTitle:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/youdao/note/exceptions/ResourceMissingException;->mNoteTitle:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public getNoteTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/youdao/note/exceptions/ResourceMissingException;->mNoteTitle:Ljava/lang/String;

    return-object v0
.end method
