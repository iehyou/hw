.class public Lcom/youdao/note/exceptions/DownloadResourceException;
.super Ljava/lang/Exception;
.source "DownloadResourceException.java"


# static fields
.field private static final serialVersionUID:J = 0x578117f9dac09d9eL


# instance fields
.field private mResourceId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "resourceId"

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/youdao/note/exceptions/DownloadResourceException;->mResourceId:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter "resourceId"
    .parameter "e"

    .prologue
    .line 30
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 31
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, Lcom/youdao/note/exceptions/DownloadResourceException;->mResourceId:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public getResourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/youdao/note/exceptions/DownloadResourceException;->mResourceId:Ljava/lang/String;

    return-object v0
.end method
