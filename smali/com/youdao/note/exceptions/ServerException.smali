.class public Lcom/youdao/note/exceptions/ServerException;
.super Ljava/lang/Exception;
.source "ServerException.java"


# static fields
.field public static final ACCESS_DENIED:I = 0xcf

.field public static final MAIL_BAN:I = 0xe8

.field public static final MAIL_BIG:I = 0xe9

.field public static final MAIL_RECV_LIMIT:I = 0xe6

.field public static final NOTE_ALREADY_DELETED:I = 0x130

.field public static final PARENT_NOT_EXIST:I = 0xe1

.field public static final RESOURCE_EXIST:I = 0xe7

.field public static final RESOURCE_NOT_EXIST:I = 0xd1

.field public static final SERVICE_INVALID:I = 0xd0

.field public static final UNKNOWN_EXCEPTION:I = 0xcd

.field public static final USER_SPACE_FULL:I = 0xd2

.field private static final serialVersionUID:J = 0x35f26606d8a789f9L


# instance fields
.field private mErrorCode:I

.field private mErrorInfo:Ljava/lang/String;

.field private mResponseCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 3
    .parameter "resCode"
    .parameter "errorInfo"

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 44
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorInfo:Ljava/lang/String;

    .line 46
    const/16 v2, 0xc8

    iput v2, p0, Lcom/youdao/note/exceptions/ServerException;->mResponseCode:I

    .line 48
    const/16 v2, 0x1f4

    iput v2, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorCode:I

    .line 56
    iput p1, p0, Lcom/youdao/note/exceptions/ServerException;->mResponseCode:I

    .line 57
    iput-object p2, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorInfo:Ljava/lang/String;

    .line 59
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, jsonObject:Lorg/json/JSONObject;
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorCode:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v1           #jsonObject:Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, e:Lorg/json/JSONException;
    const-string v2, "Failed to get error code"

    invoke-static {p0, v2, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorCode:I

    return v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/youdao/note/exceptions/ServerException;->mResponseCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Response code is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/youdao/note/exceptions/ServerException;->mResponseCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/exceptions/ServerException;->mErrorInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
