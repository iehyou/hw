.class public Lcom/youdao/note/exceptions/LoginException;
.super Ljava/lang/Exception;
.source "LoginException.java"


# static fields
.field public static final CODE_PASSWORD_ERROR:I = 0x1cc

.field public static final CODE_SUCCEED:I = 0xc8

.field public static final CODE_USER_NOT_EXIST:I = 0x1a4

.field private static final serialVersionUID:J = -0x78e5152673780fL


# instance fields
.field private mLoginResultCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .parameter "loginResultCode"

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/exceptions/LoginException;->mLoginResultCode:I

    .line 30
    iput p1, p0, Lcom/youdao/note/exceptions/LoginException;->mLoginResultCode:I

    .line 31
    return-void
.end method


# virtual methods
.method public getLoginResultCode()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/youdao/note/exceptions/LoginException;->mLoginResultCode:I

    return v0
.end method
