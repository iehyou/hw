.class public Lcom/youdao/note/service/RecordService;
.super Landroid/app/Service;
.source "RecordService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/service/RecordService$RecorderBinder;,
        Lcom/youdao/note/service/RecordService$RecordDurationListener;
    }
.end annotation


# static fields
.field public static final ACTION_START_RECORD:Ljava/lang/String; = "com.youdao.note.START_RECORD"

.field public static final ACTION_STOP_RECORD:Ljava/lang/String; = "com.youdao.note.STOP_RECORD"

.field private static final DURATION_UPDATE_INTERVAL:J = 0x3e8L

.field private static final MESSAGE_UPDATE_DURATION:I = 0x1

.field public static final RECORDING:I = 0x1

.field private static final SAMPLE_RATE:I = 0x1f40


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mContext:Landroid/content/Context;

.field private mDurationListener:Lcom/youdao/note/service/RecordService$RecordDurationListener;

.field private mFile:Ljava/io/File;

.field private mHandler:Landroid/os/Handler;

.field private mNotificationTime:J

.field private mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

.field private mTotalTimeMs:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 50
    new-instance v0, Lcom/youdao/note/service/RecordService$RecorderBinder;

    invoke-direct {v0, p0}, Lcom/youdao/note/service/RecordService$RecorderBinder;-><init>(Lcom/youdao/note/service/RecordService;)V

    iput-object v0, p0, Lcom/youdao/note/service/RecordService;->mBinder:Landroid/os/IBinder;

    .line 66
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    .line 76
    new-instance v0, Lcom/youdao/note/service/RecordService$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/service/RecordService$1;-><init>(Lcom/youdao/note/service/RecordService;)V

    iput-object v0, p0, Lcom/youdao/note/service/RecordService;->mHandler:Landroid/os/Handler;

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/ui/audio/YNoteRecorder;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/service/RecordService;)J
    .locals 2
    .parameter "x0"

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    return-wide v0
.end method

.method static synthetic access$114(Lcom/youdao/note/service/RecordService;J)J
    .locals 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService$RecordDurationListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mDurationListener:Lcom/youdao/note/service/RecordService$RecordDurationListener;

    return-object v0
.end method

.method private dismissNotification()V
    .locals 3

    .prologue
    .line 264
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/youdao/note/service/RecordService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 265
    .local v0, nm:Landroid/app/NotificationManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 266
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    .line 267
    return-void
.end method

.method private updateNotification()V
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/service/RecordService;->updateNotification(Z)V

    .line 261
    return-void
.end method


# virtual methods
.method public getRecordStatus()I
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    if-nez v0, :cond_0

    .line 203
    const/4 v0, 0x1

    .line 205
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->getRecordState()I

    move-result v0

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Ljava/io/File;Lcom/youdao/note/service/RecordService$RecordDurationListener;)V
    .locals 3
    .parameter "context"
    .parameter "file"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 113
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/service/RecordService;->getRecordStatus()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 117
    new-instance v0, Lcom/youdao/note/exceptions/RecordBusyException;

    invoke-direct {v0}, Lcom/youdao/note/exceptions/RecordBusyException;-><init>()V

    throw v0

    .line 119
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    .line 120
    iput-object p1, p0, Lcom/youdao/note/service/RecordService;->mContext:Landroid/content/Context;

    .line 121
    iput-object p2, p0, Lcom/youdao/note/service/RecordService;->mFile:Ljava/io/File;

    .line 122
    iput-object p3, p0, Lcom/youdao/note/service/RecordService;->mDurationListener:Lcom/youdao/note/service/RecordService$RecordDurationListener;

    .line 123
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/youdao/note/YNoteApplication;->setIsRecording(Z)V

    .line 125
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter "intent"

    .prologue
    .line 139
    const-string v0, "onBind called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public pauseRecord()V
    .locals 2

    .prologue
    .line 175
    const-string v0, "audio record paused."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->pause()V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 180
    invoke-direct {p0}, Lcom/youdao/note/service/RecordService;->updateNotification()V

    .line 181
    return-void
.end method

.method public startRecord(Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;)V
    .locals 5
    .parameter "listener"

    .prologue
    .line 148
    const-string v1, "audio record started."

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    if-nez v1, :cond_0

    .line 158
    new-instance v1, Lcom/youdao/note/ui/audio/YNoteRecorder;

    iget-object v2, p0, Lcom/youdao/note/service/RecordService;->mFile:Ljava/io/File;

    const/16 v3, 0x1f40

    invoke-direct {v1, v2, v3}, Lcom/youdao/note/ui/audio/YNoteRecorder;-><init>(Ljava/io/File;I)V

    iput-object v1, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v1, p1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->setVolumeChangeListener(Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;)V

    .line 161
    iget-object v1, p0, Lcom/youdao/note/service/RecordService;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->start()V

    .line 168
    invoke-direct {p0}, Lcom/youdao/note/service/RecordService;->updateNotification()V

    .line 169
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopRecord()V
    .locals 2

    .prologue
    .line 187
    const-string v0, "audio record stoped."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->stopRecord()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/service/RecordService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 193
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setIsRecording(Z)V

    .line 195
    return-void
.end method

.method public updateNotification(Z)V
    .locals 13
    .parameter "needTicker"

    .prologue
    const-wide/16 v11, 0x0

    const/16 v10, 0xff

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 211
    iget-object v3, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v3}, Lcom/youdao/note/ui/audio/YNoteRecorder;->getRecordState()I

    move-result v3

    if-ne v3, v9, :cond_1

    .line 213
    :cond_0
    const-string v3, "notification dismissed."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    invoke-direct {p0}, Lcom/youdao/note/service/RecordService;->dismissNotification()V

    .line 253
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-wide v3, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    cmp-long v3, v3, v11

    if-gez v3, :cond_2

    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    .line 221
    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/youdao/note/service/RecordService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v1, notificationIntent:Landroid/content/Intent;
    const-string v3, "entry_from"

    const-string v4, "statusBar"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const/high16 v3, 0x800

    invoke-static {p0, v8, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 225
    .local v2, pendingIntent:Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v3}, Lcom/youdao/note/ui/audio/YNoteRecorder;->getRecordState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 226
    new-instance v0, Landroid/app/Notification;

    iget-wide v3, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    const-wide/16 v5, 0x2

    rem-long/2addr v3, v5

    cmp-long v3, v3, v11

    if-nez v3, :cond_3

    const v3, 0x7f0200ea

    move v4, v3

    :goto_1
    if-eqz p1, :cond_4

    const v3, 0x7f0a0048

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    invoke-static {v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v3, v5}, Lcom/youdao/note/service/RecordService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iget-wide v5, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    invoke-direct {v0, v4, v3, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 230
    .local v0, notification:Landroid/app/Notification;
    const v3, 0x7f0a0045

    invoke-virtual {p0, v3}, Lcom/youdao/note/service/RecordService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iget-wide v4, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p0, v3, v4, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 236
    invoke-static {v10, v10, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    iput v3, v0, Landroid/app/Notification;->ledARGB:I

    .line 237
    const/16 v3, 0x3e8

    iput v3, v0, Landroid/app/Notification;->ledOnMS:I

    .line 238
    const/16 v3, 0x7d0

    iput v3, v0, Landroid/app/Notification;->ledOffMS:I

    .line 251
    :goto_3
    invoke-virtual {p0, v9, v0}, Lcom/youdao/note/service/RecordService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0

    .line 226
    .end local v0           #notification:Landroid/app/Notification;
    :cond_3
    const v3, 0x7f0200eb

    move v4, v3

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 240
    :cond_5
    new-instance v0, Landroid/app/Notification;

    const v3, 0x7f0200e9

    const v4, 0x7f0a0049

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    invoke-static {v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/youdao/note/service/RecordService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-wide v5, p0, Lcom/youdao/note/service/RecordService;->mNotificationTime:J

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 246
    .restart local v0       #notification:Landroid/app/Notification;
    const v3, 0x7f0a0046

    invoke-virtual {p0, v3}, Lcom/youdao/note/service/RecordService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    iget-wide v4, p0, Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p0, v3, v4, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_3
.end method
