.class Lcom/youdao/note/service/RecordService$1;
.super Landroid/os/Handler;
.source "RecordService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/service/RecordService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/service/RecordService;


# direct methods
.method constructor <init>(Lcom/youdao/note/service/RecordService;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "message"

    .prologue
    const-wide/16 v2, 0x3e8

    const/4 v1, 0x1

    .line 79
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_1

    .line 80
    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    #getter for: Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;
    invoke-static {v0}, Lcom/youdao/note/service/RecordService;->access$000(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/ui/audio/YNoteRecorder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    #getter for: Lcom/youdao/note/service/RecordService;->mRecorder:Lcom/youdao/note/ui/audio/YNoteRecorder;
    invoke-static {v0}, Lcom/youdao/note/service/RecordService;->access$000(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/ui/audio/YNoteRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0, v1, v2, v3}, Lcom/youdao/note/service/RecordService$1;->sendEmptyMessageDelayed(IJ)Z

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    invoke-static {v0, v2, v3}, Lcom/youdao/note/service/RecordService;->access$114(Lcom/youdao/note/service/RecordService;J)J

    .line 84
    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    #getter for: Lcom/youdao/note/service/RecordService;->mDurationListener:Lcom/youdao/note/service/RecordService$RecordDurationListener;
    invoke-static {v0}, Lcom/youdao/note/service/RecordService;->access$200(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService$RecordDurationListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    #getter for: Lcom/youdao/note/service/RecordService;->mDurationListener:Lcom/youdao/note/service/RecordService$RecordDurationListener;
    invoke-static {v0}, Lcom/youdao/note/service/RecordService;->access$200(Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService$RecordDurationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    #getter for: Lcom/youdao/note/service/RecordService;->mTotalTimeMs:J
    invoke-static {v1}, Lcom/youdao/note/service/RecordService;->access$100(Lcom/youdao/note/service/RecordService;)J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/service/RecordService$RecordDurationListener;->onDurationChanged(J)V

    .line 86
    iget-object v0, p0, Lcom/youdao/note/service/RecordService$1;->this$0:Lcom/youdao/note/service/RecordService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/service/RecordService;->updateNotification(Z)V

    .line 89
    :cond_1
    return-void
.end method
