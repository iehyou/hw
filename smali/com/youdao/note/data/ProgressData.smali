.class public Lcom/youdao/note/data/ProgressData;
.super Lcom/youdao/note/data/BaseData;
.source "ProgressData.java"


# instance fields
.field private mId:Ljava/lang/String;

.field private mProgress:I

.field private mTotalSize:J


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .parameter "p"

    .prologue
    .line 26
    const-string v0, ""

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/youdao/note/data/ProgressData;-><init>(Ljava/lang/String;IJ)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .parameter "id"
    .parameter "p"
    .parameter "totalSize"

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 30
    iput p2, p0, Lcom/youdao/note/data/ProgressData;->mProgress:I

    .line 31
    iput-object p1, p0, Lcom/youdao/note/data/ProgressData;->mId:Ljava/lang/String;

    .line 32
    iput-wide p3, p0, Lcom/youdao/note/data/ProgressData;->mTotalSize:J

    .line 33
    return-void
.end method


# virtual methods
.method public declared-synchronized getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/data/ProgressData;->mId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProgress()I
    .locals 1

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/youdao/note/data/ProgressData;->mProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTotalSize()J
    .locals 2

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/youdao/note/data/ProgressData;->mTotalSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProgress(I)V
    .locals 1
    .parameter "p"

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/youdao/note/data/ProgressData;->mProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    monitor-exit p0

    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
