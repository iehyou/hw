.class public Lcom/youdao/note/data/BaseMetaData;
.super Lcom/youdao/note/data/BaseData;
.source "BaseMetaData.java"

# interfaces
.implements Lcom/youdao/note/data/IMeta;


# static fields
.field private static final serialVersionUID:J = 0x25dd004d2ad856b0L


# instance fields
.field private isDeleted:Z

.field private isDirty:Z

.field private mLength:J

.field private version:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 19
    iput v0, p0, Lcom/youdao/note/data/BaseMetaData;->version:I

    .line 21
    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    .line 23
    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/BaseMetaData;)V
    .locals 2
    .parameter "meta"

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 19
    iput v0, p0, Lcom/youdao/note/data/BaseMetaData;->version:I

    .line 21
    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    .line 23
    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    .line 30
    iget-boolean v0, p1, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    .line 31
    iget-boolean v0, p1, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    iput-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    .line 32
    iget v0, p1, Lcom/youdao/note/data/BaseMetaData;->version:I

    iput v0, p0, Lcom/youdao/note/data/BaseMetaData;->version:I

    .line 33
    iget-wide v0, p1, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    iput-wide v0, p0, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    .line 34
    return-void
.end method


# virtual methods
.method public getLength()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    return-wide v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/youdao/note/data/BaseMetaData;->version:I

    return v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    return v0
.end method

.method public isDirty()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    return v0
.end method

.method public setDeleted(Z)V
    .locals 0
    .parameter "isDeleted"

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/youdao/note/data/BaseMetaData;->isDeleted:Z

    .line 50
    return-void
.end method

.method public setDirty(Z)V
    .locals 0
    .parameter "isDirty"

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/youdao/note/data/BaseMetaData;->isDirty:Z

    .line 54
    return-void
.end method

.method public setLength(J)V
    .locals 0
    .parameter "length"

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/youdao/note/data/BaseMetaData;->mLength:J

    .line 67
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .parameter "version"

    .prologue
    .line 41
    iput p1, p0, Lcom/youdao/note/data/BaseMetaData;->version:I

    .line 42
    return-void
.end method
