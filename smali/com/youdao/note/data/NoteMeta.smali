.class public Lcom/youdao/note/data/NoteMeta;
.super Lcom/youdao/note/data/BaseMetaData;
.source "NoteMeta.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lcom/youdao/note/utils/EmptyInstance;
.implements Lcom/youdao/note/data/IEntryPath;


# static fields
.field public static final NOTE_TYPE_ATTACH:I = 0x2

.field public static final NOTE_TYPE_THUMBNAIL:I = 0x4

.field public static final PROP_SNIPPET:Ljava/lang/String; = "thmurl"

.field public static final PROP_SUMMARY_NAME:Ljava/lang/String; = "dg"

.field public static final PROP_TP:Ljava/lang/String; = "tp"

.field public static final TIME_REVERSE_COMPARATOR:Ljava/util/Comparator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x3616f568bf8e8341L


# instance fields
.field private author:Ljava/lang/String;

.field private createTime:J

.field private extProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mLastSyncTime:J

.field private mNoteBook:Ljava/lang/String;

.field private mNoteId:Ljava/lang/String;

.field private mServerNoteBook:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private modifyTime:J

.field private noteNumber:I

.field private sourceUrl:Ljava/lang/String;

.field private tags:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/youdao/note/data/NoteMetaTimeComparator;

    invoke-direct {v0}, Lcom/youdao/note/data/NoteMetaTimeComparator;-><init>()V

    sput-object v0, Lcom/youdao/note/data/NoteMeta;->TIME_REVERSE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 91
    invoke-direct {p0}, Lcom/youdao/note/data/BaseMetaData;-><init>()V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 55
    iput-wide v3, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 57
    iput-wide v3, p0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 59
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    .line 61
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    .line 63
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->tags:[Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 76
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 87
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 89
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 8
    .parameter "entryPath"

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 99
    invoke-direct {p0}, Lcom/youdao/note/data/BaseMetaData;-><init>()V

    .line 53
    const-string v1, ""

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 55
    iput-wide v6, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 57
    iput-wide v6, p0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 59
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    .line 61
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v5}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    .line 63
    iput-object v3, p0, Lcom/youdao/note/data/NoteMeta;->tags:[Ljava/lang/String;

    .line 65
    iput v4, p0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    .line 67
    const-string v1, ""

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 69
    const-string v1, ""

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 76
    iput-object v3, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 87
    iput-object v3, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 89
    iput-object v3, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 100
    invoke-static {p1}, Lcom/youdao/note/utils/YNoteUtils;->splitEntryPath(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, tmp:[Ljava/lang/String;
    aget-object v1, v0, v4

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 102
    aget-object v1, v0, v5

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 6
    .parameter "isDir"

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Lcom/youdao/note/data/BaseMetaData;-><init>()V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 55
    iput-wide v4, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 57
    iput-wide v4, p0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 59
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    .line 63
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->tags:[Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 76
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 87
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 89
    iput-object v2, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 94
    invoke-virtual {p0, v3}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 96
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNoteId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;
    .locals 2
    .parameter "cursor"

    .prologue
    .line 354
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 355
    const/4 v1, 0x0

    .line 358
    :goto_0
    return-object v1

    .line 357
    :cond_0
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 358
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    goto :goto_0
.end method

.method public static fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteMeta;
    .locals 4
    .parameter "helper"

    .prologue
    .line 362
    new-instance v1, Lcom/youdao/note/data/NoteMeta;

    invoke-direct {v1}, Lcom/youdao/note/data/NoteMeta;-><init>()V

    .line 363
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const-string v2, "create_time"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 364
    const-string v2, "modify_time"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 365
    const-string v2, "version"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/NoteMeta;->setVersion(I)V

    .line 366
    const-string v2, "last_sync_time"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    .line 367
    const-string v2, "title"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 369
    const-string v2, "is_dirty"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 370
    const-string v2, "is_deleted"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/NoteMeta;->setDeleted(Z)V

    .line 372
    const-string v2, "_id"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 373
    const-string v2, "notebook"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 374
    const-string v2, "server_notebook"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 375
    const-string v2, "author"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 376
    const-string v2, "source"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 377
    const-string v2, "length"

    invoke-virtual {p0, v2}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/data/NoteMeta;->setLength(J)V

    .line 379
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    const-string v3, "props"

    invoke-virtual {p0, v3}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/youdao/note/utils/YNoteUtils;->json2Map(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :goto_0
    return-object v1

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, e:Lorg/json/JSONException;
    new-instance v2, Ljava/util/HashMap;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, v1, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    .line 382
    const-string v2, "Convert data from database error, just create a empty props."

    invoke-static {v1, v2}, Lcom/youdao/note/utils/L;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static fromJsonObject(Lorg/json/JSONObject;)Lcom/youdao/note/data/NoteMeta;
    .locals 1
    .parameter "jsonObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/youdao/note/data/NoteMeta;->fromJsonObject(Lorg/json/JSONObject;Z)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    return-object v0
.end method

.method public static fromJsonObject(Lorg/json/JSONObject;Z)Lcom/youdao/note/data/NoteMeta;
    .locals 6
    .parameter "jsonObject"
    .parameter "thin"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x3e8

    .line 303
    new-instance v0, Lcom/youdao/note/data/NoteMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/NoteMeta;-><init>()V

    .line 304
    .local v0, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const-string v2, "p"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/NoteMeta;->setEntryPath(Ljava/lang/String;)V

    .line 305
    const-string v2, "v"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/NoteMeta;->setVersion(I)V

    .line 306
    if-nez p1, :cond_2

    .line 307
    const-string v2, "tl"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 309
    const-string v2, "del"

    invoke-static {p0, v2}, Lcom/youdao/note/data/NoteMeta;->toBool(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/NoteMeta;->setDeleted(Z)V

    .line 310
    const-string v2, "ct"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    mul-long/2addr v2, v4

    iput-wide v2, v0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 311
    const-string v2, "mt"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    mul-long/2addr v2, v4

    iput-wide v2, v0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 312
    const-string v2, "pp"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/utils/YNoteUtils;->json2Map(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, v0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    .line 313
    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getNoteMetaType()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    .line 314
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/NoteMeta;->setSnippetUrl(Ljava/lang/String;)V

    .line 316
    :cond_0
    const-string v2, "tg"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, tag:Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 318
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/NoteMeta;->setTags([Ljava/lang/String;)V

    .line 320
    :cond_1
    const-string v2, "sz"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/youdao/note/data/NoteMeta;->setLength(J)V

    .line 321
    const-string v2, "nn"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    .line 322
    const-string v2, "au"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 323
    const-string v2, "su"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 325
    .end local v1           #tag:Ljava/lang/String;
    :cond_2
    return-object v0
.end method

.method public static fromJsonString(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;
    .locals 2
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 335
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 336
    .local v0, jsonObject:Lorg/json/JSONObject;
    invoke-static {v0}, Lcom/youdao/note/data/NoteMeta;->fromJsonObject(Lorg/json/JSONObject;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    return-object v1
.end method

.method public static fromJsonString(Ljava/lang/String;Z)Lcom/youdao/note/data/NoteMeta;
    .locals 2
    .parameter "json"
    .parameter "thin"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 340
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 341
    .local v0, jsonObject:Lorg/json/JSONObject;
    invoke-static {v0, p1}, Lcom/youdao/note/data/NoteMeta;->fromJsonObject(Lorg/json/JSONObject;Z)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    return-object v1
.end method

.method private safeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 124
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string p1, ""

    .line 127
    .end local p1
    :cond_0
    return-object p1
.end method


# virtual methods
.method public genRelativePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    return-wide v0
.end method

.method public getEntryPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastSyncTime()J
    .locals 2

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    .prologue
    .line 170
    iget-wide v0, p0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    return-wide v0
.end method

.method public getNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteMetaType()I
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "tp"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "tp"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNoteNumber()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    return v0
.end method

.method public getPropsAsStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 252
    const-string v1, "{}"

    .line 255
    :goto_0
    return-object v1

    .line 254
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 255
    .local v0, jsonObject:Lorg/json/JSONObject;
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getServerNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    return-object v0
.end method

.method public getSnippetUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "thmurl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/youdao/note/data/NoteMeta;->safeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "dg"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/youdao/note/data/NoteMeta;->safeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTags()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->tags:[Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    const-string v0, "\u65e0\u4e3b\u9898"

    .line 151
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAttachment()Z
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->getNoteMetaType()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSnippet()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMoved()Z
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needSync()Z
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->isMoved()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 0
    .parameter "author"

    .prologue
    .line 192
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->author:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setCreateTime(J)V
    .locals 0
    .parameter "createTime"

    .prologue
    .line 165
    iput-wide p1, p0, Lcom/youdao/note/data/NoteMeta;->createTime:J

    .line 166
    return-void
.end method

.method public setEntryPath(Ljava/lang/String;)V
    .locals 2
    .parameter "entryPath"

    .prologue
    .line 137
    if-nez p1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-static {p1}, Lcom/youdao/note/utils/YNoteUtils;->splitEntryPath(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, tmp:[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 142
    iget-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 143
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    goto :goto_0
.end method

.method public setHasAttachment()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "tp"

    invoke-virtual {p0}, Lcom/youdao/note/data/NoteMeta;->getNoteMetaType()I

    move-result v2

    or-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .parameter "modifyTime"

    .prologue
    .line 174
    iput-wide p1, p0, Lcom/youdao/note/data/NoteMeta;->modifyTime:J

    .line 175
    return-void
.end method

.method public setNoteBook(Ljava/lang/String;)V
    .locals 1
    .parameter "noteBook"

    .prologue
    .line 228
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteBook:Ljava/lang/String;

    .line 229
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 230
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 232
    :cond_0
    return-void
.end method

.method public setNoteId(Ljava/lang/String;)V
    .locals 0
    .parameter "noteId"

    .prologue
    .line 239
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->mNoteId:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setNoteNumber(I)V
    .locals 0
    .parameter "noteNumber"

    .prologue
    .line 183
    iput p1, p0, Lcom/youdao/note/data/NoteMeta;->noteNumber:I

    .line 184
    return-void
.end method

.method public setServerNoteBook(Ljava/lang/String;)V
    .locals 0
    .parameter "noteBook"

    .prologue
    .line 345
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->mServerNoteBook:Ljava/lang/String;

    .line 346
    return-void
.end method

.method public setSnippetFID(Ljava/lang/String;)V
    .locals 4
    .parameter "FID"

    .prologue
    .line 281
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "thmurl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " v=0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    return-void
.end method

.method public setSnippetUrl(Ljava/lang/String;)V
    .locals 3
    .parameter "snippetUrl"

    .prologue
    .line 285
    if-nez p1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "thmurl"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "thmurl"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setSourceUrl(Ljava/lang/String;)V
    .locals 0
    .parameter "sourceUrl"

    .prologue
    .line 200
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->sourceUrl:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 2
    .parameter "summary"

    .prologue
    .line 120
    iget-object v0, p0, Lcom/youdao/note/data/NoteMeta;->extProp:Ljava/util/Map;

    const-string v1, "dg"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-void
.end method

.method public setTags([Ljava/lang/String;)V
    .locals 0
    .parameter "tags"

    .prologue
    .line 269
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->tags:[Ljava/lang/String;

    .line 270
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter "title"

    .prologue
    .line 156
    iput-object p1, p0, Lcom/youdao/note/data/NoteMeta;->mTitle:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public setmLastSyncTime(J)V
    .locals 0
    .parameter "mLastSyncTime"

    .prologue
    .line 260
    iput-wide p1, p0, Lcom/youdao/note/data/NoteMeta;->mLastSyncTime:J

    .line 261
    return-void
.end method
