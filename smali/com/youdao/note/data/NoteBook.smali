.class public Lcom/youdao/note/data/NoteBook;
.super Lcom/youdao/note/data/BaseMetaData;
.source "NoteBook.java"

# interfaces
.implements Lcom/youdao/note/utils/EmptyInstance;


# static fields
.field private static final serialVersionUID:J = -0x2689df8a2a3a72bcL


# instance fields
.field private createTime:J

.field private mLastSyncTime:J

.field private mNoteBookId:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private modifyTime:J

.field private noteNumber:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/youdao/note/data/BaseMetaData;-><init>()V

    .line 30
    iput-wide v0, p0, Lcom/youdao/note/data/NoteBook;->createTime:J

    .line 32
    iput-wide v0, p0, Lcom/youdao/note/data/NoteBook;->modifyTime:J

    .line 34
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/youdao/note/data/NoteBook;->mLastSyncTime:J

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/NoteBook;->mTitle:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/NoteBook;->noteNumber:I

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/youdao/note/data/NoteBook;->createTime:J

    .line 45
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNoteId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/data/NoteBook;->mNoteBookId:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteBook;
    .locals 2
    .parameter "cursor"

    .prologue
    .line 121
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 122
    const/4 v1, 0x0

    .line 125
    :goto_0
    return-object v1

    .line 124
    :cond_0
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 125
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    invoke-static {v0}, Lcom/youdao/note/data/NoteBook;->fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    goto :goto_0
.end method

.method public static fromCursorHelper(Lcom/youdao/note/utils/CursorHelper;)Lcom/youdao/note/data/NoteBook;
    .locals 3
    .parameter "helper"

    .prologue
    .line 129
    new-instance v0, Lcom/youdao/note/data/NoteBook;

    invoke-direct {v0}, Lcom/youdao/note/data/NoteBook;-><init>()V

    .line 130
    .local v0, noteMeta:Lcom/youdao/note/data/NoteBook;
    const-string v1, "create_time"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/NoteBook;->setCreateTime(J)V

    .line 131
    const-string v1, "modify_time"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/NoteBook;->setModifyTime(J)V

    .line 132
    const-string v1, "version"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setVersion(I)V

    .line 133
    const-string v1, "last_sync_time"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/NoteBook;->setLastSyncTime(J)V

    .line 134
    const-string v1, "title"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setTitle(Ljava/lang/String;)V

    .line 135
    const-string v1, "is_dirty"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 136
    const-string v1, "is_deleted"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDeleted(Z)V

    .line 137
    const-string v1, "_id"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setNoteBookId(Ljava/lang/String;)V

    .line 138
    const-string v1, "note_number"

    invoke-virtual {p0, v1}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setNoteNumber(I)V

    .line 139
    return-object v0
.end method

.method public static fromJsonObject(Lorg/json/JSONObject;)Lcom/youdao/note/data/NoteBook;
    .locals 5
    .parameter "jsonObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide/16 v3, 0x3e8

    .line 107
    new-instance v0, Lcom/youdao/note/data/NoteBook;

    invoke-direct {v0}, Lcom/youdao/note/data/NoteBook;-><init>()V

    .line 108
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    const-string v1, "v"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setVersion(I)V

    .line 109
    const-string v1, "tl"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/youdao/note/data/NoteBook;->mTitle:Ljava/lang/String;

    .line 110
    const-string v1, "del"

    invoke-static {p0, v1}, Lcom/youdao/note/data/NoteBook;->toBool(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDeleted(Z)V

    .line 111
    const-string v1, "ct"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    mul-long/2addr v1, v3

    iput-wide v1, v0, Lcom/youdao/note/data/NoteBook;->createTime:J

    .line 112
    const-string v1, "mt"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    mul-long/2addr v1, v3

    iput-wide v1, v0, Lcom/youdao/note/data/NoteBook;->modifyTime:J

    .line 113
    const-string v1, "p"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setNoteBookId(Ljava/lang/String;)V

    .line 114
    const-string v1, "sz"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/NoteBook;->setLength(J)V

    .line 115
    const-string v1, "nn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/youdao/note/data/NoteBook;->noteNumber:I

    .line 116
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 117
    return-object v0
.end method


# virtual methods
.method public getCreateTime()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/youdao/note/data/NoteBook;->createTime:J

    return-wide v0
.end method

.method public getEntryPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/data/NoteBook;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastSyncTime()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/youdao/note/data/NoteBook;->mLastSyncTime:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/youdao/note/data/NoteBook;->modifyTime:J

    return-wide v0
.end method

.method public getNoteBookId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/youdao/note/data/NoteBook;->mNoteBookId:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteNumber()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/youdao/note/data/NoteBook;->noteNumber:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/youdao/note/data/NoteBook;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isRoot()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/youdao/note/data/NoteBook;->mNoteBookId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public setCreateTime(J)V
    .locals 0
    .parameter "createTime"

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/youdao/note/data/NoteBook;->createTime:J

    .line 55
    return-void
.end method

.method public setLastSyncTime(J)V
    .locals 0
    .parameter "lastSyncTime"

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/youdao/note/data/NoteBook;->mLastSyncTime:J

    .line 71
    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .parameter "modifyTime"

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/youdao/note/data/NoteBook;->modifyTime:J

    .line 63
    return-void
.end method

.method public setNoteBookId(Ljava/lang/String;)V
    .locals 0
    .parameter "noteBookId"

    .prologue
    .line 94
    iput-object p1, p0, Lcom/youdao/note/data/NoteBook;->mNoteBookId:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setNoteNumber(I)V
    .locals 0
    .parameter "noteNumber"

    .prologue
    .line 86
    iput p1, p0, Lcom/youdao/note/data/NoteBook;->noteNumber:I

    .line 87
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter "title"

    .prologue
    .line 78
    iput-object p1, p0, Lcom/youdao/note/data/NoteBook;->mTitle:Ljava/lang/String;

    .line 79
    return-void
.end method
