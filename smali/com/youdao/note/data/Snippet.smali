.class public Lcom/youdao/note/data/Snippet;
.super Lcom/youdao/note/data/AbstractLocalCacheData;
.source "Snippet.java"


# static fields
.field private static final FID_PATTERN:Ljava/util/regex/Pattern; = null

.field public static final SNIPPET_FILENAME:Ljava/lang/String; = "snippet.png"

.field private static final VERSION_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final serialVersionUID:J = 0x7ece86041316fd3eL


# instance fields
.field private noteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "fid=([\\w\\d]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/data/Snippet;->FID_PATTERN:Ljava/util/regex/Pattern;

    .line 17
    const-string v0, "v=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/data/Snippet;->VERSION_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter "noteMeta"

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/youdao/note/data/Snippet;->noteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 23
    return-void
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getSnippetCache()Lcom/youdao/note/datasource/localcache/SnippetCache;

    move-result-object v0

    return-object v0
.end method

.method public getNoteMeta()Lcom/youdao/note/data/NoteMeta;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/youdao/note/data/Snippet;->noteMeta:Lcom/youdao/note/data/NoteMeta;

    return-object v0
.end method

.method public getRelativePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/data/Snippet;->parseFID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "snippet.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public parseFID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    sget-object v1, Lcom/youdao/note/data/Snippet;->FID_PATTERN:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/youdao/note/data/Snippet;->noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 31
    .local v0, m:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 34
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public parseVERSION()I
    .locals 3

    .prologue
    .line 38
    sget-object v1, Lcom/youdao/note/data/Snippet;->VERSION_PATTERN:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lcom/youdao/note/data/Snippet;->noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteMeta;->getSnippetUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 39
    .local v0, m:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 42
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
