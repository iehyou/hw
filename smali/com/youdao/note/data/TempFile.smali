.class public Lcom/youdao/note/data/TempFile;
.super Lcom/youdao/note/data/AbstractLocalCacheData;
.source "TempFile.java"


# static fields
.field private static final serialVersionUID:J = -0x27a4308cbaa1e93bL


# instance fields
.field private relativePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "relativePath"

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/youdao/note/data/TempFile;->relativePath:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static newNoteSnapshot(Ljava/lang/String;)Lcom/youdao/note/data/TempFile;
    .locals 3
    .parameter "noteId"

    .prologue
    .line 41
    new-instance v0, Lcom/youdao/note/data/TempFile;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-note-snapshot.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/youdao/note/data/TempFile;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getTempFileCache()Lcom/youdao/note/datasource/localcache/TempFileCache;

    move-result-object v0

    return-object v0
.end method

.method public getRelativePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/data/TempFile;->relativePath:Ljava/lang/String;

    return-object v0
.end method

.method public setContentBytes([B)V
    .locals 0
    .parameter "contentBytes"

    .prologue
    .line 27
    iput-object p1, p0, Lcom/youdao/note/data/TempFile;->datas:[B

    .line 28
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .parameter "relativePath"

    .prologue
    .line 23
    iput-object p1, p0, Lcom/youdao/note/data/TempFile;->relativePath:Ljava/lang/String;

    .line 24
    return-void
.end method
