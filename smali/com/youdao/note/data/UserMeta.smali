.class public Lcom/youdao/note/data/UserMeta;
.super Lcom/youdao/note/data/BaseData;
.source "UserMeta.java"


# instance fields
.field private lastLoginTime:J

.field private mDefaultNotebook:Ljava/lang/String;

.field private mLastPushTime:J

.field private mLastSynceTime:J

.field private mQuotaSpace:J

.field private mUsedSpace:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 24
    iput-wide v1, p0, Lcom/youdao/note/data/UserMeta;->mUsedSpace:J

    .line 26
    iput-wide v1, p0, Lcom/youdao/note/data/UserMeta;->lastLoginTime:J

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/UserMeta;->mDefaultNotebook:Ljava/lang/String;

    .line 30
    iput-wide v1, p0, Lcom/youdao/note/data/UserMeta;->mQuotaSpace:J

    .line 32
    iput-wide v1, p0, Lcom/youdao/note/data/UserMeta;->mLastPushTime:J

    .line 34
    iput-wide v1, p0, Lcom/youdao/note/data/UserMeta;->mLastSynceTime:J

    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/UserMeta;
    .locals 4
    .parameter "cursor"

    .prologue
    .line 118
    new-instance v0, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v0, p0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 119
    .local v0, helper:Lcom/youdao/note/utils/CursorHelper;
    new-instance v1, Lcom/youdao/note/data/UserMeta;

    invoke-direct {v1}, Lcom/youdao/note/data/UserMeta;-><init>()V

    .line 120
    .local v1, userMeta:Lcom/youdao/note/data/UserMeta;
    const-string v2, "used_space"

    invoke-virtual {v0, v2}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mUsedSpace:J

    .line 121
    const-string v2, "quota_space"

    invoke-virtual {v0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mQuotaSpace:J

    .line 122
    const-string v2, "default_notebook"

    invoke-virtual {v0, v2}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/UserMeta;->mDefaultNotebook:Ljava/lang/String;

    .line 123
    const-string v2, "last_push_time"

    invoke-virtual {v0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mLastPushTime:J

    .line 124
    const-string v2, "last_sync_time"

    invoke-virtual {v0, v2}, Lcom/youdao/note/utils/CursorHelper;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mLastSynceTime:J

    .line 125
    return-object v1
.end method

.method public static fromJsonString(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;
    .locals 4
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, jsonObject:Lorg/json/JSONObject;
    new-instance v1, Lcom/youdao/note/data/UserMeta;

    invoke-direct {v1}, Lcom/youdao/note/data/UserMeta;-><init>()V

    .line 109
    .local v1, userMeta:Lcom/youdao/note/data/UserMeta;
    const-string v2, "df"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/youdao/note/data/UserMeta;->mDefaultNotebook:Ljava/lang/String;

    .line 110
    const-string v2, "ll"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->lastLoginTime:J

    .line 111
    const-string v2, "lp"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mLastPushTime:J

    .line 112
    const-string v2, "u"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mUsedSpace:J

    .line 113
    const-string v2, "q"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/youdao/note/data/UserMeta;->mQuotaSpace:J

    .line 114
    return-object v1
.end method


# virtual methods
.method public getDefaultNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/youdao/note/data/UserMeta;->mDefaultNotebook:Ljava/lang/String;

    return-object v0
.end method

.method public getLastLoginTime()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/youdao/note/data/UserMeta;->lastLoginTime:J

    return-wide v0
.end method

.method public getLastPushTime()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mLastPushTime:J

    return-wide v0
.end method

.method public getLastSynceTime()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mLastSynceTime:J

    return-wide v0
.end method

.method public getQuotaSpace()J
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mQuotaSpace:J

    return-wide v0
.end method

.method public getUsedSpace()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mUsedSpace:J

    return-wide v0
.end method

.method public setDefaultNoteBook(Ljava/lang/String;)V
    .locals 0
    .parameter "defaultNotebook"

    .prologue
    .line 75
    iput-object p1, p0, Lcom/youdao/note/data/UserMeta;->mDefaultNotebook:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setLastLoginTime(J)V
    .locals 0
    .parameter "lastLoginTime"

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/youdao/note/data/UserMeta;->lastLoginTime:J

    .line 62
    return-void
.end method

.method public setLastPushTime(J)V
    .locals 0
    .parameter "lastPushTime"

    .prologue
    .line 103
    iput-wide p1, p0, Lcom/youdao/note/data/UserMeta;->mLastPushTime:J

    .line 104
    return-void
.end method

.method public setLastSynceTime(J)V
    .locals 0
    .parameter "lastSynceTime"

    .prologue
    .line 129
    iput-wide p1, p0, Lcom/youdao/note/data/UserMeta;->mLastSynceTime:J

    .line 130
    return-void
.end method

.method public setQuotaSpace(I)V
    .locals 2
    .parameter "quotaSpace"

    .prologue
    .line 89
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mQuotaSpace:J

    .line 90
    return-void
.end method

.method public setUsedSpace(I)V
    .locals 2
    .parameter "usedSpace"

    .prologue
    .line 47
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/youdao/note/data/UserMeta;->mUsedSpace:J

    .line 48
    return-void
.end method
