.class public Lcom/youdao/note/data/LoginResult;
.super Lcom/youdao/note/data/BaseData;
.source "LoginResult.java"


# static fields
.field public static final CODE_PASSWORD_ERROR:I = 0x1cc

.field public static final CODE_SUCCEED:I = 0xc8

.field public static final CODE_TOO_MANY_ERROR:I = 0x19c

.field public static final CODE_UNKNOW_REASON:I = -0x1

.field public static final CODE_USER_NOT_EXIST:I = 0x1a4


# instance fields
.field private mPassWord:Ljava/lang/String;

.field private mPersistCookie:Ljava/lang/String;

.field private mResultCode:I

.field private mSessionCookie:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .parameter "exceptionCode"

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 26
    const/16 v0, 0xc8

    iput v0, p0, Lcom/youdao/note/data/LoginResult;->mResultCode:I

    .line 37
    invoke-virtual {p0, p1}, Lcom/youdao/note/data/LoginResult;->setResultCode(I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "userName"
    .parameter "password"
    .parameter "persistCookie"
    .parameter "sessionCookie"

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 26
    const/16 v0, 0xc8

    iput v0, p0, Lcom/youdao/note/data/LoginResult;->mResultCode:I

    .line 41
    iput-object p3, p0, Lcom/youdao/note/data/LoginResult;->mPersistCookie:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/youdao/note/data/LoginResult;->mSessionCookie:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/youdao/note/data/LoginResult;->mUserName:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/youdao/note/data/LoginResult;->mPassWord:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public getPassWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/youdao/note/data/LoginResult;->mPassWord:Ljava/lang/String;

    return-object v0
.end method

.method public getPersistCookie()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/youdao/note/data/LoginResult;->mPersistCookie:Ljava/lang/String;

    return-object v0
.end method

.method public getResultCode()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/youdao/note/data/LoginResult;->mResultCode:I

    return v0
.end method

.method public getSessionCookie()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/youdao/note/data/LoginResult;->mSessionCookie:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/data/LoginResult;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public setResultCode(I)V
    .locals 0
    .parameter "mResultCode"

    .prologue
    .line 64
    iput p1, p0, Lcom/youdao/note/data/LoginResult;->mResultCode:I

    .line 65
    return-void
.end method
