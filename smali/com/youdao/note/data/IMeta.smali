.class public interface abstract Lcom/youdao/note/data/IMeta;
.super Ljava/lang/Object;
.source "IMeta.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getLength()J
.end method

.method public abstract getVersion()I
.end method

.method public abstract isDeleted()Z
.end method

.method public abstract isDirty()Z
.end method

.method public abstract setDeleted(Z)V
.end method

.method public abstract setDirty(Z)V
.end method

.method public abstract setLength(J)V
.end method

.method public abstract setVersion(I)V
.end method
