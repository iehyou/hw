.class public Lcom/youdao/note/data/RemoteErrorData;
.super Lcom/youdao/note/data/BaseData;
.source "RemoteErrorData.java"


# static fields
.field public static final ERROR_NEWORK:I = 0x1


# instance fields
.field private mErrorType:I

.field private mException:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .parameter "error"

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mException:Ljava/lang/Exception;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mErrorType:I

    .line 27
    iput p1, p0, Lcom/youdao/note/data/RemoteErrorData;->mErrorType:I

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mException:Ljava/lang/Exception;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mErrorType:I

    .line 23
    iput-object p1, p0, Lcom/youdao/note/data/RemoteErrorData;->mException:Ljava/lang/Exception;

    .line 24
    return-void
.end method


# virtual methods
.method public getErrorType()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mErrorType:I

    return v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/youdao/note/data/RemoteErrorData;->mException:Ljava/lang/Exception;

    return-object v0
.end method

.method public setErrorType(I)V
    .locals 0
    .parameter "error"

    .prologue
    .line 39
    iput p1, p0, Lcom/youdao/note/data/RemoteErrorData;->mErrorType:I

    .line 40
    return-void
.end method
