.class final Lcom/youdao/note/data/resource/ResourceUtils$1;
.super Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.source "ResourceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/ResourceMetaSwitcher",
        "<",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/AudioResourceMeta;-><init>()V

    .line 111
    .local v0, meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    const-string v1, "mp3"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AudioResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/youdao/note/utils/StringUtils;->currentTimeStr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".mp3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AudioResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 113
    return-object v0
.end method

.method protected bridge synthetic onAudioType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;->onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/DoodleResourceMeta;-><init>()V

    .line 79
    .local v0, meta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/DoodleResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/DoodleResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/DoodleResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 81
    return-object v0
.end method

.method protected bridge synthetic onDoodleType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;->onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/GeneralResourceMeta;-><init>()V

    .line 103
    .local v0, meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    const-string v1, ""

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/GeneralResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0}, Lcom/youdao/note/data/resource/GeneralResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/GeneralResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 105
    return-object v0
.end method

.method protected bridge synthetic onGeneralType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;->onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 3

    .prologue
    .line 86
    new-instance v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;-><init>()V

    .line 87
    .local v0, meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 89
    return-object v0
.end method

.method protected bridge synthetic onHandwriteType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;->onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>()V

    .line 95
    .local v0, meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 97
    return-object v0
.end method

.method protected bridge synthetic onImageType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;->onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method
