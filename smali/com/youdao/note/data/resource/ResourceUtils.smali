.class public Lcom/youdao/note/data/resource/ResourceUtils;
.super Ljava/lang/Object;
.source "ResourceUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 6
    .parameter "cursor"

    .prologue
    .line 26
    new-instance v1, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v1, p0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 27
    .local v1, helper:Lcom/youdao/note/utils/CursorHelper;
    const-string v4, "type"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 28
    .local v3, resourceType:I
    invoke-static {v3}, Lcom/youdao/note/data/resource/ResourceUtils;->newResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    .line 29
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    const-string v4, "filename"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 30
    const-string v4, "noteid"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setNoteId(Ljava/lang/String;)V

    .line 31
    const-string v4, "version"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setVersion(I)V

    .line 32
    const-string v4, "_id"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 33
    const-string v4, "length"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getInt(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v4, v5}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setLength(J)V

    .line 34
    const-string v4, "is_dirty"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setDirty(Z)V

    .line 36
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    const-string v5, "props"

    invoke-virtual {v1, v5}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/youdao/note/utils/YNoteUtils;->json2Map(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v4

    iput-object v4, v2, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-object v2

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, e:Lorg/json/JSONException;
    const-class v4, Lcom/youdao/note/data/resource/ResourceUtils;

    const-string v5, "Resource init failed."

    invoke-static {v4, v5, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static genEmptyAudioResource()Lcom/youdao/note/data/resource/AudioResource;
    .locals 2

    .prologue
    .line 69
    new-instance v1, Lcom/youdao/note/data/resource/AudioResource;

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/AudioResource;-><init>(Lcom/youdao/note/data/resource/AudioResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyDoodleResource()Lcom/youdao/note/data/resource/DoodleResource;
    .locals 2

    .prologue
    .line 59
    new-instance v1, Lcom/youdao/note/data/resource/DoodleResource;

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/DoodleResource;-><init>(Lcom/youdao/note/data/resource/DoodleResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyGeneralResource()Lcom/youdao/note/data/resource/GeneralResource;
    .locals 2

    .prologue
    .line 64
    new-instance v1, Lcom/youdao/note/data/resource/GeneralResource;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/GeneralResource;-><init>(Lcom/youdao/note/data/resource/GeneralResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyHandwriteResource()Lcom/youdao/note/data/resource/HandwriteResource;
    .locals 2

    .prologue
    .line 54
    new-instance v1, Lcom/youdao/note/data/resource/HandwriteResource;

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/HandwriteResource;-><init>(Lcom/youdao/note/data/resource/HandwriteResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyImageResource()Lcom/youdao/note/data/resource/ImageResource;
    .locals 2

    .prologue
    .line 49
    new-instance v1, Lcom/youdao/note/data/resource/ImageResource;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/ImageResource;-><init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyImageResource(Ljava/lang/String;)Lcom/youdao/note/data/resource/ImageResource;
    .locals 2
    .parameter "fileName"

    .prologue
    .line 44
    new-instance v1, Lcom/youdao/note/data/resource/ImageResource;

    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(ILjava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/resource/ImageResource;-><init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-object v1
.end method

.method public static genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2
    .parameter "resourceType"

    .prologue
    .line 74
    new-instance v1, Lcom/youdao/note/data/resource/ResourceUtils$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/data/resource/ResourceUtils$1;-><init>(I)V

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ResourceUtils$1;->doSwitch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 117
    .local v0, ret:Lcom/youdao/note/data/resource/BaseResourceMeta;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setDirty(Z)V

    .line 118
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setVersion(I)V

    .line 119
    return-object v0
.end method

.method public static genEmptyResourceMeta(ILjava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2
    .parameter "resourceType"
    .parameter "fileName"

    .prologue
    .line 123
    if-nez p1, :cond_0

    .line 124
    invoke-static {p0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    .line 126
    :cond_0
    new-instance v1, Lcom/youdao/note/data/resource/ResourceUtils$2;

    invoke-direct {v1, p0, p1}, Lcom/youdao/note/data/resource/ResourceUtils$2;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ResourceUtils$2;->doSwitch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 169
    .local v0, ret:Lcom/youdao/note/data/resource/BaseResourceMeta;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setDirty(Z)V

    .line 170
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setVersion(I)V

    goto :goto_0
.end method

.method public static hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z
    .locals 1
    .parameter "meta"

    .prologue
    .line 22
    instance-of v0, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    return v0
.end method

.method public static newResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1
    .parameter "resourceType"

    .prologue
    .line 175
    new-instance v0, Lcom/youdao/note/data/resource/ResourceUtils$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;-><init>(I)V

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->doSwitch()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    return-object v0
.end method
