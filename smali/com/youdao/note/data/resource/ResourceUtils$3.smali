.class final Lcom/youdao/note/data/resource/ResourceUtils$3;
.super Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.source "ResourceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/data/resource/ResourceUtils;->newResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/ResourceMetaSwitcher",
        "<",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/AudioResourceMeta;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic onAudioType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/DoodleResourceMeta;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic onDoodleType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1

    .prologue
    .line 194
    new-instance v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/GeneralResourceMeta;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic onGeneralType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1

    .prologue
    .line 184
    new-instance v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic onHandwriteType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1

    .prologue
    .line 189
    new-instance v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic onImageType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$3;->onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method
