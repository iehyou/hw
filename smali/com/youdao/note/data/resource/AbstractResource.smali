.class public abstract Lcom/youdao/note/data/resource/AbstractResource;
.super Lcom/youdao/note/data/AbstractLocalCacheData;
.source "AbstractResource.java"

# interfaces
.implements Lcom/youdao/note/data/resource/IResource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">",
        "Lcom/youdao/note/data/AbstractLocalCacheData;",
        "Lcom/youdao/note/data/resource/IResource;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x29fafa68dcca2f11L


# instance fields
.field protected meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    .local p1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;,"TT;"
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 16
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    return-object v0
.end method

.method public getRelativePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setContentBytes([B)V
    .locals 3
    .parameter "contentBytes"

    .prologue
    .line 36
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    iput-object p1, p0, Lcom/youdao/note/data/resource/AbstractResource;->datas:[B

    .line 37
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    array-length v1, p1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setLength(J)V

    .line 38
    return-void
.end method

.method public setMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, this:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<TT;>;"
    .local p1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;,"TT;"
    iput-object p1, p0, Lcom/youdao/note/data/resource/AbstractResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 33
    return-void
.end method
