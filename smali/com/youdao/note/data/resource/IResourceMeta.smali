.class public interface abstract Lcom/youdao/note/data/resource/IResourceMeta;
.super Ljava/lang/Object;
.source "IResourceMeta.java"

# interfaces
.implements Lcom/youdao/note/data/IMeta;


# virtual methods
.method public abstract genRelativePath()Ljava/lang/String;
.end method

.method public abstract getFileName()Ljava/lang/String;
.end method

.method public abstract getNoteId()Ljava/lang/String;
.end method

.method public abstract getResourceId()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method

.method public abstract setFileName(Ljava/lang/String;)V
.end method

.method public abstract setNoteId(Ljava/lang/String;)V
.end method

.method public abstract setResourceId(Ljava/lang/String;)V
.end method

.method public abstract setType(I)V
.end method
