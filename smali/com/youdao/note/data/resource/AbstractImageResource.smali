.class public abstract Lcom/youdao/note/data/resource/AbstractImageResource;
.super Lcom/youdao/note/data/resource/AbstractResource;
.source "AbstractImageResource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
        ">",
        "Lcom/youdao/note/data/resource/AbstractResource",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x4d0227c2def2b278L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    .local p0, this:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractResource;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, this:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    .local p1, meta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;,"TT;"
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractResource;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 21
    .local p0, this:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractImageResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    check-cast v0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getHeight()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 17
    .local p0, this:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractImageResource;->meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    check-cast v0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getWidth()I

    move-result v0

    return v0
.end method
