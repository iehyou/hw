.class final Lcom/youdao/note/data/resource/ResourceUtils$2;
.super Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.source "ResourceUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(ILjava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/ResourceMetaSwitcher",
        "<",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter

    .prologue
    .line 126
    iput-object p2, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/AudioResourceMeta;-><init>()V

    .line 163
    .local v0, meta:Lcom/youdao/note/data/resource/AudioResourceMeta;
    const-string v1, "mp3"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AudioResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AudioResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 165
    return-object v0
.end method

.method protected bridge synthetic onAudioType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$2;->onAudioType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/DoodleResourceMeta;-><init>()V

    .line 131
    .local v0, meta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/DoodleResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/DoodleResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 133
    return-object v0
.end method

.method protected bridge synthetic onDoodleType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$2;->onDoodleType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/GeneralResourceMeta;-><init>()V

    .line 155
    .local v0, meta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    const-string v1, ""

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/GeneralResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 156
    iget-object v1, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/GeneralResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 157
    return-object v0
.end method

.method protected bridge synthetic onGeneralType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$2;->onGeneralType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;-><init>()V

    .line 139
    .local v0, meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 141
    return-object v0
.end method

.method protected bridge synthetic onHandwriteType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$2;->onHandwriteType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>()V

    .line 147
    .local v0, meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    const-string v1, "jpg"

    invoke-static {v1}, Lcom/youdao/note/utils/IdUtils;->genResourceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setResourceId(Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/youdao/note/data/resource/ResourceUtils$2;->val$fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 149
    return-object v0
.end method

.method protected bridge synthetic onImageType()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceUtils$2;->onImageType()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method
