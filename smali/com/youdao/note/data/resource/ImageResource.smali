.class public Lcom/youdao/note/data/resource/ImageResource;
.super Lcom/youdao/note/data/resource/AbstractImageResource;
.source "ImageResource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/AbstractImageResource",
        "<",
        "Lcom/youdao/note/data/resource/ImageResourceMeta;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x3acbddf5e6fe9383L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 0
    .parameter "meta"

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractImageResource;-><init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getImageCache()Lcom/youdao/note/datasource/localcache/ImageCache;

    move-result-object v0

    return-object v0
.end method
