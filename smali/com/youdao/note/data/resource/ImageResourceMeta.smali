.class public Lcom/youdao/note/data/resource/ImageResourceMeta;
.super Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
.source "ImageResourceMeta.java"


# static fields
.field public static final FROM_CAMERA:I = 0x2

.field public static final FROM_ORIGIN:I = 0x0

.field public static final FROM_PHOTO:I = 0x1

.field private static final KEY_PICFROM:Ljava/lang/String; = "picFrom"

.field private static final serialVersionUID:J = -0x62e4f177452af8d1L


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;-><init>()V

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setType(I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setType(I)V

    .line 29
    return-void
.end method


# virtual methods
.method public copyTempPicture(Landroid/net/Uri;)V
    .locals 2
    .parameter "originUri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v0

    .line 47
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/youdao/note/utils/FileUtils;->copyFile(Landroid/net/Uri;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public getPicFrom()I
    .locals 3

    .prologue
    .line 32
    iget-object v1, p0, Lcom/youdao/note/data/resource/ImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v2, "picFrom"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 33
    .local v0, picFrom:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 34
    const/4 v1, 0x0

    .line 36
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getTempFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getStoreDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 52
    .local v0, file:Ljava/io/File;
    return-object v0
.end method

.method public setPicFrom(I)V
    .locals 3
    .parameter "picFrom"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/data/resource/ImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v1, "picFrom"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method
