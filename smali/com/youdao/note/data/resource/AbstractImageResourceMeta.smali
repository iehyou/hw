.class public Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
.super Lcom/youdao/note/data/resource/BaseResourceMeta;
.source "AbstractImageResourceMeta.java"


# static fields
.field private static final KEY_HEIGHT:Ljava/lang/String; = "height"

.field private static final KEY_WIDTH:Ljava/lang/String; = "width"

.field private static final serialVersionUID:J = 0x6aa68c3c22824219L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/youdao/note/data/resource/BaseResourceMeta;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter "meta"

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 24
    return-void
.end method

.method private loadWidthAndHeight()V
    .locals 6

    .prologue
    .line 54
    :try_start_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/datasource/DataSource;->getImageCache()Lcom/youdao/note/datasource/localcache/ImageCache;

    move-result-object v3

    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/datasource/localcache/ImageCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, filePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/utils/ImageUtils;->getBitmapOption(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 58
    .local v2, option:Landroid/graphics/BitmapFactory$Options;
    iget-object v3, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v4, "width"

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v3, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v4, "height"

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v1           #filePath:Ljava/lang/String;
    .end local v2           #option:Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v3, ""

    invoke-static {p0, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public getHeight()I
    .locals 3

    .prologue
    .line 27
    iget-object v1, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v2, "height"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 28
    .local v0, height:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 29
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->loadWidthAndHeight()V

    .line 30
    iget-object v1, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v2, "height"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #height:Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .line 32
    .restart local v0       #height:Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public getWidth()I
    .locals 3

    .prologue
    .line 40
    iget-object v1, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v2, "width"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 41
    .local v0, width:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->loadWidthAndHeight()V

    .line 43
    iget-object v1, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v2, "width"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #width:Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .line 45
    .restart local v0       #width:Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public setHeight(I)V
    .locals 3
    .parameter "height"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v1, "height"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public setWidth(I)V
    .locals 3
    .parameter "width"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->transientValues:Ljava/util/Map;

    const-string v1, "width"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method
