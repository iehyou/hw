.class public Lcom/youdao/note/data/resource/BaseResourceMeta;
.super Lcom/youdao/note/data/BaseMetaData;
.source "BaseResourceMeta.java"

# interfaces
.implements Lcom/youdao/note/data/resource/IResourceMeta;


# static fields
.field private static final ICON_SRC:Ljava/lang/String; = "icon_src"

.field private static final serialVersionUID:J = 0x5652d2689d0c6266L


# instance fields
.field protected extProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected fileName:Ljava/lang/String;

.field protected mNoteId:Ljava/lang/String;

.field protected mResourceId:Ljava/lang/String;

.field protected transientValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private type:I


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/youdao/note/data/BaseMetaData;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->transientValues:Ljava/util/Map;

    .line 40
    return-void
.end method

.method protected constructor <init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 2
    .parameter "meta"

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/youdao/note/data/BaseMetaData;-><init>(Lcom/youdao/note/data/BaseMetaData;)V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->transientValues:Ljava/util/Map;

    .line 31
    iget-object v0, p1, Lcom/youdao/note/data/resource/BaseResourceMeta;->mNoteId:Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mNoteId:Ljava/lang/String;

    .line 32
    iget-object v0, p1, Lcom/youdao/note/data/resource/BaseResourceMeta;->mResourceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mResourceId:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Lcom/youdao/note/data/resource/BaseResourceMeta;->fileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->fileName:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->type:I

    .line 35
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->transientValues:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->transientValues:Ljava/util/Map;

    .line 36
    iget-object v0, p1, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    iput-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    .line 37
    return-void
.end method


# virtual methods
.method public genRelativePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mNoteId:Ljava/lang/String;

    return-object v0
.end method

.method public getPropsAsStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    iget-object v1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 96
    const-string v1, "{}"

    .line 99
    :goto_0
    return-object v1

    .line 98
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 99
    .local v0, jsonObject:Lorg/json/JSONObject;
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getResourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mResourceId:Ljava/lang/String;

    return-object v0
.end method

.method public getSrc()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    iget-object v1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    const-string v2, "icon_src"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 86
    .local v0, ret:Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got src"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    check-cast v0, Ljava/lang/String;

    .line 91
    .end local v0           #ret:Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 90
    .restart local v0       #ret:Ljava/lang/Object;
    :cond_0
    const-string v1, "got src nothing"

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    const-string v0, ""

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->type:I

    return v0
.end method

.method public isDeleted()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setDeleted(Z)V
    .locals 1
    .parameter "isDeleted"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .parameter "fileName"

    .prologue
    .line 68
    iput-object p1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->fileName:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setNoteId(Ljava/lang/String;)V
    .locals 0
    .parameter "mNoteId"

    .prologue
    .line 52
    iput-object p1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mNoteId:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setResourceId(Ljava/lang/String;)V
    .locals 0
    .parameter "mResourceId"

    .prologue
    .line 60
    iput-object p1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->mResourceId:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setSrc(Ljava/lang/String;)V
    .locals 2
    .parameter "src"

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "set src "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->extProp:Ljava/util/Map;

    const-string v1, "icon_src"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method public setType(I)V
    .locals 0
    .parameter "type"

    .prologue
    .line 76
    iput p1, p0, Lcom/youdao/note/data/resource/BaseResourceMeta;->type:I

    .line 77
    return-void
.end method
