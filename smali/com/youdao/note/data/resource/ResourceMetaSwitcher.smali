.class public abstract Lcom/youdao/note/data/resource/ResourceMetaSwitcher;
.super Ljava/lang/Object;
.source "ResourceMetaSwitcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .parameter "type"

    .prologue
    .line 19
    .local p0, this:Lcom/youdao/note/data/resource/ResourceMetaSwitcher;,"Lcom/youdao/note/data/resource/ResourceMetaSwitcher<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->type:I

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 15
    .local p0, this:Lcom/youdao/note/data/resource/ResourceMetaSwitcher;,"Lcom/youdao/note/data/resource/ResourceMetaSwitcher<TT;>;"
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;-><init>(I)V

    .line 16
    return-void
.end method


# virtual methods
.method public doSwitch()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/youdao/note/data/resource/ResourceMetaSwitcher;,"Lcom/youdao/note/data/resource/ResourceMetaSwitcher<TT;>;"
    const/4 v0, 0x0

    .line 24
    iget v1, p0, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->type:I

    packed-switch v1, :pswitch_data_0

    .line 30
    const-string v1, "unknow type"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v0

    .line 25
    :pswitch_0
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->onDoodleType()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 26
    :pswitch_1
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->onHandwriteType()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 27
    :pswitch_2
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->onImageType()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 28
    :pswitch_3
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->onGeneralType()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 29
    :pswitch_4
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/ResourceMetaSwitcher;->onAudioType()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 24
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method protected abstract onAudioType()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract onDoodleType()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract onGeneralType()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract onHandwriteType()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract onImageType()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
