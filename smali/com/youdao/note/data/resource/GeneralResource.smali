.class public Lcom/youdao/note/data/resource/GeneralResource;
.super Lcom/youdao/note/data/resource/AbstractResource;
.source "GeneralResource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/AbstractResource",
        "<",
        "Lcom/youdao/note/data/resource/GeneralResourceMeta;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x37f26f55953a72b3L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractResource;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/GeneralResourceMeta;)V
    .locals 0
    .parameter "meta"

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractResource;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getGeneralResourceCache()Lcom/youdao/note/datasource/localcache/GeneralResourceCache;

    move-result-object v0

    return-object v0
.end method
