.class public Lcom/youdao/note/data/resource/HandwriteResource;
.super Lcom/youdao/note/data/resource/AbstractImageResource;
.source "HandwriteResource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/AbstractImageResource",
        "<",
        "Lcom/youdao/note/data/resource/HandwriteResourceMeta;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x82c05702506cef7L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/HandwriteResourceMeta;)V
    .locals 0
    .parameter "meta"

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractImageResource;-><init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getHandwriteCache()Lcom/youdao/note/datasource/localcache/HandwriteCache;

    move-result-object v0

    return-object v0
.end method
