.class public Lcom/youdao/note/data/resource/AudioResource;
.super Lcom/youdao/note/data/resource/AbstractResource;
.source "AudioResource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/resource/AbstractResource",
        "<",
        "Lcom/youdao/note/data/resource/AudioResourceMeta;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x735560605f3f126dL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/youdao/note/data/resource/AbstractResource;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/data/resource/AudioResourceMeta;)V
    .locals 0
    .parameter "meta"

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/youdao/note/data/resource/AbstractResource;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getAudioCache()Lcom/youdao/note/datasource/localcache/AudioCache;

    move-result-object v0

    return-object v0
.end method
