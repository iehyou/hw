.class Lcom/youdao/note/data/NoteMetaTimeComparator;
.super Ljava/lang/Object;
.source "NoteMeta.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/youdao/note/data/NoteMeta;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/data/NoteMeta;)I
    .locals 4
    .parameter "noteMeta1"
    .parameter "noteMeta2"

    .prologue
    .line 395
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 396
    const/4 v0, 0x0

    .line 398
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 392
    check-cast p1, Lcom/youdao/note/data/NoteMeta;

    .end local p1
    check-cast p2, Lcom/youdao/note/data/NoteMeta;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/youdao/note/data/NoteMetaTimeComparator;->compare(Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/data/NoteMeta;)I

    move-result v0

    return v0
.end method
