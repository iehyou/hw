.class public Lcom/youdao/note/data/Note;
.super Lcom/youdao/note/data/AbstractLocalCacheData;
.source "Note.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$HTML;
.implements Lcom/youdao/note/utils/EmptyInstance;
.implements Lcom/youdao/note/data/IEntryPath;


# static fields
.field private static final serialVersionUID:J = -0x2fc1f90240d64be1L


# instance fields
.field private mHtmlBody:Ljava/lang/String;

.field private mNoteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V
    .locals 1
    .parameter "noteMeta"
    .parameter "body"

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 33
    iput-object p1, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 34
    invoke-virtual {p0, p2}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "entryPath"

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/data/Note;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "entryPath"
    .parameter "body"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 42
    new-instance v0, Lcom/youdao/note/data/NoteMeta;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/NoteMeta;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 43
    invoke-virtual {p0, p2}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .parameter "isDir"

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 28
    new-instance v0, Lcom/youdao/note/data/NoteMeta;

    invoke-direct {v0, p1}, Lcom/youdao/note/data/NoteMeta;-><init>(Z)V

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    return-object v0
.end method

.method public getContentBytes()[B
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method public getEditableHtml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"editor2.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</body></html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getEntryPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHtml()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"detail.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</body></html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getNoteCache()Lcom/youdao/note/datasource/localcache/NoteCache;

    move-result-object v0

    return-object v0
.end method

.method public getModifyTime()J
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoteMeta()Lcom/youdao/note/data/NoteMeta;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    return-object v0
.end method

.method public getRelativePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getSourceUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v0

    return v0
.end method

.method public isDirty()Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v0

    return v0
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 1
    .parameter "body"

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/youdao/note/data/Note;->mHtmlBody:Ljava/lang/String;

    goto :goto_0
.end method

.method public setDirty(Z)V
    .locals 1
    .parameter "isDirty"

    .prologue
    .line 100
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 101
    return-void
.end method

.method public setNoteBookId(Ljava/lang/String;)V
    .locals 1
    .parameter "noteBookId"

    .prologue
    .line 121
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/data/NoteMeta;->setNoteBook(Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public setNoteId(Ljava/lang/String;)V
    .locals 1
    .parameter "noteId"

    .prologue
    .line 128
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/data/NoteMeta;->setNoteId(Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method public setNoteMeta(Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter "noteMeta"

    .prologue
    .line 108
    iput-object p1, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 109
    return-void
.end method

.method public setServerNoteBook(Ljava/lang/String;)V
    .locals 1
    .parameter "noteBookId"

    .prologue
    .line 125
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .parameter "title"

    .prologue
    .line 117
    iget-object v0, p0, Lcom/youdao/note/data/Note;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/data/NoteMeta;->setTitle(Ljava/lang/String;)V

    .line 118
    return-void
.end method
