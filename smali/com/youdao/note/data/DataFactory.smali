.class public Lcom/youdao/note/data/DataFactory;
.super Ljava/lang/Object;
.source "DataFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newNotebookMeta(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;
    .locals 3
    .parameter "title"

    .prologue
    .line 20
    new-instance v0, Lcom/youdao/note/data/NoteBook;

    invoke-direct {v0}, Lcom/youdao/note/data/NoteBook;-><init>()V

    .line 21
    .local v0, notebookMeta:Lcom/youdao/note/data/NoteBook;
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNotebookId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setNoteBookId(Ljava/lang/String;)V

    .line 22
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/NoteBook;->setModifyTime(J)V

    .line 24
    invoke-virtual {v0, p0}, Lcom/youdao/note/data/NoteBook;->setTitle(Ljava/lang/String;)V

    .line 25
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setVersion(I)V

    .line 26
    return-object v0
.end method
