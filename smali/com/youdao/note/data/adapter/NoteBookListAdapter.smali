.class public Lcom/youdao/note/data/adapter/NoteBookListAdapter;
.super Lcom/youdao/note/data/adapter/BaseListAdapter;
.source "NoteBookListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/adapter/BaseListAdapter",
        "<",
        "Lcom/youdao/note/data/NoteBook;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p2, datas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    const v0, 0x7f030031

    invoke-direct {p0, p1, v0, p2}, Lcom/youdao/note/data/adapter/BaseListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 37
    move-object v3, p2

    .line 38
    .local v3, view:Landroid/view/View;
    if-nez v3, :cond_0

    .line 39
    iget-object v4, p0, Lcom/youdao/note/data/adapter/NoteBookListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030031

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 41
    :cond_0
    invoke-virtual {p0, p1}, Lcom/youdao/note/data/adapter/NoteBookListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteBook;

    .line 42
    .local v1, noteBook:Lcom/youdao/note/data/NoteBook;
    const v4, 0x7f070086

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 43
    .local v2, textView:Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    const v4, 0x7f070087

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    .local v0, countView:Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getNoteNumber()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-object v3
.end method
