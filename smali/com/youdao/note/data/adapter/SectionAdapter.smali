.class public Lcom/youdao/note/data/adapter/SectionAdapter;
.super Landroid/widget/BaseAdapter;
.source "SectionAdapter.java"


# instance fields
.field private labelFormator:Ljava/text/SimpleDateFormat;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mTitleFormator:Ljava/lang/String;

.field private mTotalCount:I

.field private mUnSyncedTitle:Ljava/lang/String;

.field private sectionCount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "inflater"
    .parameter "cursor"
    .parameter "formator"
    .parameter "unsyncedTitle"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 57
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 41
    new-array v0, v3, [Lcom/youdao/note/data/adapter/SectionItem;

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->labelFormator:Ljava/text/SimpleDateFormat;

    .line 45
    iput v3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    .line 47
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    .line 58
    iput-object p1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 59
    iput-object p4, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 61
    invoke-virtual {p0, p2}, Lcom/youdao/note/data/adapter/SectionAdapter;->resetSections(Landroid/database/Cursor;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "inflater"
    .parameter "formator"
    .parameter "unsyncedTitle"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 41
    new-array v0, v3, [Lcom/youdao/note/data/adapter/SectionItem;

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->labelFormator:Ljava/text/SimpleDateFormat;

    .line 45
    iput v3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    .line 47
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    .line 52
    iput-object p1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 53
    iput-object p3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;[Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "inflater"
    .parameter "noteMetas"
    .parameter "formator"
    .parameter "unsyncedTitle"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 41
    new-array v0, v3, [Lcom/youdao/note/data/adapter/SectionItem;

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->labelFormator:Ljava/text/SimpleDateFormat;

    .line 45
    iput v3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    .line 47
    iput-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    .line 65
    iput-object p1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 66
    iput-object p4, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    .line 68
    invoke-virtual {p0, p2}, Lcom/youdao/note/data/adapter/SectionAdapter;->resetSections([Lcom/youdao/note/data/NoteMeta;)V

    .line 69
    return-void
.end method

.method private addNoteMeta(ILjava/util/Date;Ljava/util/List;Lcom/youdao/note/data/NoteMeta;)I
    .locals 3
    .parameter "lastMonth"
    .parameter "date"
    .parameter
    .parameter "noteMeta"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Date;",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/adapter/SectionItem;",
            ">;",
            "Lcom/youdao/note/data/NoteMeta;",
            ")I"
        }
    .end annotation

    .prologue
    .line 111
    .local p3, list:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/adapter/SectionItem;>;"
    invoke-virtual {p4}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 112
    iget-object v1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->labelFormator:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, labelText:Ljava/lang/String;
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    invoke-virtual {p2}, Ljava/util/Date;->getMonth()I

    move-result v1

    if-eq v1, p1, :cond_1

    .line 114
    :cond_0
    new-instance v1, Lcom/youdao/note/data/adapter/SectionItem;

    invoke-direct {v1, v0}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-virtual {p2}, Ljava/util/Date;->getMonth()I

    move-result p1

    .line 117
    :cond_1
    new-instance v1, Lcom/youdao/note/data/adapter/SectionItem;

    invoke-direct {v1, p4}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Lcom/youdao/note/data/NoteMeta;)V

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-direct {p0, v0}, Lcom/youdao/note/data/adapter/SectionAdapter;->incSectionCount(Ljava/lang/String;)V

    .line 119
    return p1
.end method

.method private incSectionCount(Ljava/lang/String;)V
    .locals 3
    .parameter "labelText"

    .prologue
    .line 123
    iget-object v1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 124
    .local v0, count:Ljava/lang/Integer;
    iget-object v2, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    return-void

    .line 124
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 201
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 206
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .parameter "position"

    .prologue
    .line 216
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v0

    return v0
.end method

.method public getSectionCount(Ljava/lang/String;)I
    .locals 2
    .parameter "labelText"

    .prologue
    .line 128
    iget-object v1, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 129
    .local v0, ret:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 130
    const/4 v1, 0x0

    .line 132
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getUnsyncedTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lcom/youdao/note/data/adapter/SectionAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/adapter/SectionItem;

    .line 138
    .local v3, item:Lcom/youdao/note/data/adapter/SectionItem;
    move-object/from16 v9, p2

    .line 139
    .local v9, view:Landroid/view/View;
    const/4 v0, 0x0

    .line 140
    .local v0, convertItem:Lcom/youdao/note/data/adapter/SectionItem;
    if-eqz v9, :cond_0

    .line 141
    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #convertItem:Lcom/youdao/note/data/adapter/SectionItem;
    check-cast v0, Lcom/youdao/note/data/adapter/SectionItem;

    .line 143
    .restart local v0       #convertItem:Lcom/youdao/note/data/adapter/SectionItem;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/youdao/note/data/adapter/SectionAdapter;->getItemViewType(I)I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    .line 144
    if-eqz v9, :cond_1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_2

    .line 146
    :cond_1
    iget-object v10, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f030021

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 147
    const v10, 0x7f07005a

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 149
    .local v8, titleText:Landroid/widget/TextView;
    iget-object v10, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    .end local v8           #titleText:Landroid/widget/TextView;
    :cond_2
    :goto_0
    invoke-virtual {v9, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 191
    return-object v9

    .line 151
    :cond_3
    invoke-virtual {p0, p1}, Lcom/youdao/note/data/adapter/SectionAdapter;->getItemViewType(I)I

    move-result v10

    if-nez v10, :cond_6

    .line 152
    if-eqz v9, :cond_4

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v10

    if-eqz v10, :cond_5

    .line 154
    :cond_4
    iget-object v10, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f030007

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 156
    :cond_5
    const v10, 0x7f070017

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 157
    .local v7, textView:Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/youdao/note/data/adapter/SectionItem;->getLabel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    const v10, 0x7f070018

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 160
    .local v2, countView:Landroid/widget/TextView;
    iget-object v10, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionCount:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/youdao/note/data/adapter/SectionItem;->getLabel()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 163
    .end local v2           #countView:Landroid/widget/TextView;
    .end local v7           #textView:Landroid/widget/TextView;
    :cond_6
    invoke-virtual {v3}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    .line 164
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const/4 v1, 0x0

    .line 165
    .local v1, convertNoteMeta:Lcom/youdao/note/data/NoteMeta;
    if-eqz v0, :cond_7

    .line 166
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 168
    :cond_7
    if-nez v1, :cond_8

    .line 169
    iget-object v10, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f030038

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 171
    :cond_8
    const v10, 0x7f070089

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 172
    .restart local v7       #textView:Landroid/widget/TextView;
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    const v10, 0x7f07007d

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/youdao/note/ui/AsyncImageView;

    .line 175
    .local v5, snippetView:Lcom/youdao/note/ui/AsyncImageView;
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->hasSnippet()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 176
    invoke-virtual {v5, v4}, Lcom/youdao/note/ui/AsyncImageView;->loadSnippet(Lcom/youdao/note/data/NoteMeta;)V

    .line 180
    :goto_1
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->hasAttachment()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 181
    const v10, 0x7f070098

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 185
    :goto_2
    const v10, 0x7f070099

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 186
    .local v6, summary:Landroid/widget/TextView;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<font color=\'grey\'>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/youdao/note/utils/StringUtils;->getPrettyTime2(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</font>  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getSummary()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 178
    .end local v6           #summary:Landroid/widget/TextView;
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {v5, v10}, Lcom/youdao/note/ui/AsyncImageView;->setVisibility(I)V

    goto :goto_1

    .line 183
    :cond_a
    const v10, 0x7f070098

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .parameter "position"

    .prologue
    .line 236
    const/4 v0, 0x1

    return v0
.end method

.method public resetSections(Landroid/database/Cursor;)V
    .locals 8
    .parameter "cursor"

    .prologue
    .line 85
    const/4 v2, -0x1

    .line 86
    .local v2, lastMonth:I
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    iput v5, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    .line 87
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 89
    .local v0, date:Ljava/util/Date;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v1, dirtyNotesList:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/adapter/SectionItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/adapter/SectionItem;>;"
    new-instance v5, Lcom/youdao/note/data/adapter/SectionItem;

    iget-object v6, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    iget v7, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    invoke-direct {v5, v6, v7}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v5, Lcom/youdao/note/data/adapter/SectionItem;

    iget-object v6, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 94
    invoke-static {p1}, Lcom/youdao/note/data/NoteMeta;->fromCursor(Landroid/database/Cursor;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    .line 95
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    iget-object v5, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    new-instance v5, Lcom/youdao/note/data/adapter/SectionItem;

    invoke-direct {v5, v4}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Lcom/youdao/note/data/NoteMeta;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v5, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mUnSyncedTitle:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/youdao/note/data/adapter/SectionAdapter;->incSectionCount(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_0
    invoke-direct {p0, v2, v0, v3, v4}, Lcom/youdao/note/data/adapter/SectionAdapter;->addNoteMeta(ILjava/util/Date;Ljava/util/List;Lcom/youdao/note/data/NoteMeta;)I

    move-result v2

    goto :goto_0

    .line 102
    .end local v4           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 103
    const/4 v5, 0x1

    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 105
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 106
    const/4 v5, 0x0

    new-array v5, v5, [Lcom/youdao/note/data/adapter/SectionItem;

    invoke-interface {v1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/youdao/note/data/adapter/SectionItem;

    iput-object v5, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    .line 107
    return-void
.end method

.method public resetSections([Lcom/youdao/note/data/NoteMeta;)V
    .locals 10
    .parameter "noteMetas"

    .prologue
    .line 72
    const/4 v3, -0x1

    .line 73
    .local v3, lastMonth:I
    array-length v7, p1

    iput v7, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    .line 74
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 75
    .local v1, date:Ljava/util/Date;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v5, list:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/adapter/SectionItem;>;"
    new-instance v7, Lcom/youdao/note/data/adapter/SectionItem;

    iget-object v8, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTitleFormator:Ljava/lang/String;

    iget v9, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->mTotalCount:I

    invoke-direct {v7, v8, v9}, Lcom/youdao/note/data/adapter/SectionItem;-><init>(Ljava/lang/String;I)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    move-object v0, p1

    .local v0, arr$:[Lcom/youdao/note/data/NoteMeta;
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v6, v0, v2

    .line 78
    .local v6, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-direct {p0, v3, v1, v5, v6}, Lcom/youdao/note/data/adapter/SectionAdapter;->addNoteMeta(ILjava/util/Date;Ljava/util/List;Lcom/youdao/note/data/NoteMeta;)I

    move-result v3

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    .end local v6           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_0
    const/4 v7, 0x0

    new-array v7, v7, [Lcom/youdao/note/data/adapter/SectionItem;

    invoke-interface {v5, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/youdao/note/data/adapter/SectionItem;

    iput-object v7, p0, Lcom/youdao/note/data/adapter/SectionAdapter;->sectionItems:[Lcom/youdao/note/data/adapter/SectionItem;

    .line 81
    return-void
.end method
