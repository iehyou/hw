.class public abstract Lcom/youdao/note/data/adapter/BaseListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BaseListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .parameter "context"
    .parameter "resourceId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    .local p3, datas:Ljava/util/List;,"Ljava/util/List<TT;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mResourceId:I

    .line 39
    iput-object p1, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mContext:Landroid/content/Context;

    .line 40
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 42
    iput p2, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mResourceId:I

    .line 43
    iput-object p3, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mDatas:Ljava/util/List;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    .local p2, datas:Ljava/util/List;,"Ljava/util/List<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/youdao/note/data/adapter/BaseListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 36
    return-void
.end method


# virtual methods
.method public createFooterView(ILandroid/widget/ListView;)Landroid/view/View;
    .locals 3
    .parameter "layout"
    .parameter "parent"

    .prologue
    .line 80
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget-object v1, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 81
    .local v0, view:Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 82
    return-object v0
.end method

.method public createHeaderView(ILandroid/widget/ListView;)Landroid/view/View;
    .locals 3
    .parameter "layout"
    .parameter "parent"

    .prologue
    .line 74
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget-object v1, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 75
    .local v0, view:Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 76
    return-object v0
.end method

.method protected createView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "parent"

    .prologue
    .line 91
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createViewFromResource(ILandroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .parameter "position"
    .parameter "parent"
    .parameter "resourceId"

    .prologue
    .line 87
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 48
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mDatas:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 53
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 67
    .local p0, this:Lcom/youdao/note/data/adapter/BaseListAdapter;,"Lcom/youdao/note/data/adapter/BaseListAdapter<TT;>;"
    iget v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mResourceId:I

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0, p1, p3}, Lcom/youdao/note/data/adapter/BaseListAdapter;->createView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/youdao/note/data/adapter/BaseListAdapter;->mResourceId:I

    invoke-virtual {p0, p1, p3, v0}, Lcom/youdao/note/data/adapter/BaseListAdapter;->createViewFromResource(ILandroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
