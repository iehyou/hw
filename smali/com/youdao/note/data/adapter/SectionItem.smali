.class public Lcom/youdao/note/data/adapter/SectionItem;
.super Ljava/lang/Object;
.source "SectionItem.java"


# instance fields
.field private mItemType:I

.field private mLabel:Ljava/lang/String;

.field private mNoteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/NoteMeta;)V
    .locals 2
    .parameter "noteMeta"

    .prologue
    const/4 v1, 0x1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 17
    iput v1, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 18
    iput-object p1, p0, Lcom/youdao/note/data/adapter/SectionItem;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "sectionLabel"

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 23
    iput-object p1, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .parameter "format"
    .parameter "totalCount"

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    .line 28
    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteMeta()Lcom/youdao/note/data/NoteMeta;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/data/adapter/SectionItem;->mItemType:I

    return v0
.end method
