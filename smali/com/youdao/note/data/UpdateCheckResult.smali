.class public Lcom/youdao/note/data/UpdateCheckResult;
.super Lcom/youdao/note/data/BaseData;
.source "UpdateCheckResult.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1bc5afafed189497L


# instance fields
.field private mNewFeatures:Ljava/lang/String;

.field private mNewVersionFound:Z

.field private mUpdateUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/data/UpdateCheckResult;->mNewVersionFound:Z

    .line 34
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "newVersionFound"
    .parameter "updateUrl"
    .parameter "newFeatures"

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 27
    iput-boolean p1, p0, Lcom/youdao/note/data/UpdateCheckResult;->mNewVersionFound:Z

    .line 28
    iput-object p2, p0, Lcom/youdao/note/data/UpdateCheckResult;->mUpdateUrl:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/youdao/note/data/UpdateCheckResult;->mNewFeatures:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getNewFeatures()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/youdao/note/data/UpdateCheckResult;->mNewFeatures:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/youdao/note/data/UpdateCheckResult;->mUpdateUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isNewVersionFoun()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/youdao/note/data/UpdateCheckResult;->mNewVersionFound:Z

    return v0
.end method
