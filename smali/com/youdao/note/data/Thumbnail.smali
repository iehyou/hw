.class public Lcom/youdao/note/data/Thumbnail;
.super Lcom/youdao/note/data/AbstractLocalCacheData;
.source "Thumbnail.java"


# static fields
.field private static final serialVersionUID:J = -0x76e0c777190315a8L


# instance fields
.field private imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V
    .locals 0
    .parameter "imageMeta"

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/youdao/note/data/Thumbnail;->imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    .line 16
    return-void
.end method


# virtual methods
.method public getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/youdao/note/data/Thumbnail;->imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    return-object v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/youdao/note/data/Thumbnail;->imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getLength()J

    move-result-wide v0

    return-wide v0
.end method

.method protected getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getThumbnailCache()Lcom/youdao/note/datasource/localcache/ThumbnailCache;

    move-result-object v0

    return-object v0
.end method

.method public getRelativePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/youdao/note/data/Thumbnail;->imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
