.class public Lcom/youdao/note/data/PathCollection;
.super Lcom/youdao/note/data/BaseData;
.source "PathCollection.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x5d7a68802045c2cbL


# instance fields
.field private final mNoteBooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;"
        }
    .end annotation
.end field

.field private final mNoteMetas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation
.end field

.field private root:Lcom/youdao/note/data/NoteBook;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteBooks:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    return-void
.end method

.method public static fromJsonString(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;
    .locals 5
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 76
    .local v1, jsonArray:Lorg/json/JSONArray;
    new-instance v3, Lcom/youdao/note/data/PathCollection;

    invoke-direct {v3}, Lcom/youdao/note/data/PathCollection;-><init>()V

    .line 78
    .local v3, syncData:Lcom/youdao/note/data/PathCollection;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 79
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 80
    .local v2, object:Lorg/json/JSONObject;
    const-string v4, "dr"

    invoke-static {v2, v4}, Lcom/youdao/note/data/PathCollection;->toBool(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    invoke-static {v2}, Lcom/youdao/note/data/NoteBook;->fromJsonObject(Lorg/json/JSONObject;)Lcom/youdao/note/data/NoteBook;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/PathCollection;->addNoteBook(Lcom/youdao/note/data/NoteBook;)V

    .line 78
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    invoke-static {v2}, Lcom/youdao/note/data/NoteMeta;->fromJsonObject(Lorg/json/JSONObject;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/PathCollection;->addNoteMeta(Lcom/youdao/note/data/NoteMeta;)V

    goto :goto_1

    .line 86
    .end local v2           #object:Lorg/json/JSONObject;
    :cond_1
    return-object v3
.end method


# virtual methods
.method public addNoteBook(Lcom/youdao/note/data/NoteBook;)V
    .locals 1
    .parameter "noteBook"

    .prologue
    .line 46
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->isRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iput-object p1, p0, Lcom/youdao/note/data/PathCollection;->root:Lcom/youdao/note/data/NoteBook;

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteBooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addNoteMeta(Lcom/youdao/note/data/NoteMeta;)V
    .locals 1
    .parameter "noteMeta"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method

.method public getFirstNoteMeta()Lcom/youdao/note/data/NoteMeta;
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/NoteMeta;

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNoteBooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteBooks:Ljava/util/List;

    return-object v0
.end method

.method public getNoteMetas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    return-object v0
.end method

.method public getRoot()Lcom/youdao/note/data/NoteBook;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->root:Lcom/youdao/note/data/NoteBook;

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->mNoteBooks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/data/PathCollection;->mNoteMetas:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, Lcom/youdao/note/data/PathCollection;->root:Lcom/youdao/note/data/NoteBook;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
