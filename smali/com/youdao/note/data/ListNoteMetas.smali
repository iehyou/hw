.class public Lcom/youdao/note/data/ListNoteMetas;
.super Lcom/youdao/note/data/BaseData;
.source "ListNoteMetas.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1cb4909a0150f503L


# instance fields
.field private fileMetas:[Lcom/youdao/note/data/NoteMeta;

.field private totalNumber:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/data/ListNoteMetas;->totalNumber:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    return-void
.end method

.method public static fromJsonString(Ljava/lang/String;ZZ)Lcom/youdao/note/data/ListNoteMetas;
    .locals 6
    .parameter "json"
    .parameter "withCount"
    .parameter "thin"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 53
    .local v1, jsonArray:Lorg/json/JSONArray;
    new-instance v2, Lcom/youdao/note/data/ListNoteMetas;

    invoke-direct {v2}, Lcom/youdao/note/data/ListNoteMetas;-><init>()V

    .line 54
    .local v2, listFileMetas:Lcom/youdao/note/data/ListNoteMetas;
    const/4 v3, 0x0

    .line 55
    .local v3, subArray:Lorg/json/JSONArray;
    if-eqz p1, :cond_0

    .line 56
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getInt(I)I

    move-result v4

    iput v4, v2, Lcom/youdao/note/data/ListNoteMetas;->totalNumber:I

    .line 57
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #subArray:Lorg/json/JSONArray;
    check-cast v3, Lorg/json/JSONArray;

    .line 61
    .restart local v3       #subArray:Lorg/json/JSONArray;
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    new-array v4, v4, [Lcom/youdao/note/data/NoteMeta;

    iput-object v4, v2, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    .line 62
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 63
    iget-object v5, v2, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    invoke-static {v4, p2}, Lcom/youdao/note/data/NoteMeta;->fromJsonObject(Lorg/json/JSONObject;Z)Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    aput-object v4, v5, v0

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59
    .end local v0           #i:I
    :cond_0
    move-object v3, v1

    goto :goto_0

    .line 66
    .restart local v0       #i:I
    :cond_1
    return-object v2
.end method


# virtual methods
.method public getFileMetas()[Lcom/youdao/note/data/NoteMeta;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    if-nez v0, :cond_0

    .line 44
    sget-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_NOTEMETAS:[Lcom/youdao/note/data/NoteMeta;

    .line 46
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    goto :goto_0
.end method

.method public getTotalNumber()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/youdao/note/data/ListNoteMetas;->totalNumber:I

    return v0
.end method

.method public setFileMetas([Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter "fileMetas"

    .prologue
    .line 39
    iput-object p1, p0, Lcom/youdao/note/data/ListNoteMetas;->fileMetas:[Lcom/youdao/note/data/NoteMeta;

    .line 40
    return-void
.end method

.method public setTotalNumber(I)V
    .locals 0
    .parameter "totalNumber"

    .prologue
    .line 31
    iput p1, p0, Lcom/youdao/note/data/ListNoteMetas;->totalNumber:I

    .line 32
    return-void
.end method
