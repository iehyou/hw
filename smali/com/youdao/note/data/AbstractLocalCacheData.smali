.class public abstract Lcom/youdao/note/data/AbstractLocalCacheData;
.super Lcom/youdao/note/data/BaseData;
.source "AbstractLocalCacheData.java"

# interfaces
.implements Lcom/youdao/note/data/ICacheable;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x25be39aa8ae6cc00L


# instance fields
.field protected datas:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/youdao/note/data/BaseData;-><init>()V

    return-void
.end method


# virtual methods
.method public exist()Z
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->exist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getAbslutePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getAbsolutePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentBytes()[B
    .locals 3

    .prologue
    .line 20
    iget-object v1, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    if-nez v1, :cond_0

    .line 22
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;

    move-result-object v1

    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getRelativePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/localcache/AbstractLocalCache;->getCacheItemAsBytes(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    return-object v1

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, e:Ljava/io/IOException;
    const-string v1, "getContentBytes failed!"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    array-length v0, v0

    int-to-long v0, v0

    .line 59
    :goto_0
    return-wide v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/youdao/note/data/AbstractLocalCacheData;->getAbslutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method protected abstract getLocalCache()Lcom/youdao/note/datasource/localcache/AbstractLocalCache;
.end method

.method public isDataEmpty()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDataLoaded()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseData()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    .line 53
    return-void
.end method

.method public setContentBytes([B)V
    .locals 0
    .parameter "datas"

    .prologue
    .line 40
    iput-object p1, p0, Lcom/youdao/note/data/AbstractLocalCacheData;->datas:[B

    .line 41
    return-void
.end method
