.class public Lcom/youdao/note/tool/img/ImageProcess;
.super Ljava/lang/Object;
.source "ImageProcess.java"


# static fields
.field private static loadLibSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const/4 v1, 0x1

    sput-boolean v1, Lcom/youdao/note/tool/img/ImageProcess;->loadLibSuccess:Z

    .line 26
    :try_start_0
    const-string v1, "ynote_lib_img"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .local v0, e:Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 27
    .end local v0           #e:Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 28
    .restart local v0       #e:Ljava/lang/UnsatisfiedLinkError;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/youdao/note/tool/img/ImageProcess;->loadLibSuccess:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;
    .locals 3
    .parameter "b"

    .prologue
    .line 37
    new-instance v0, Lorg/opencv/core/Mat;

    invoke-static {p0}, Lcom/youdao/note/tool/img/ImageProcess;->nBitmapToMat(Landroid/graphics/Bitmap;)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/opencv/core/Mat;-><init>(J)V

    return-object v0
.end method

.method public static MatToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)Z
    .locals 2
    .parameter "m"
    .parameter "b"

    .prologue
    .line 41
    iget-wide v0, p0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v0, v1, p1}, Lcom/youdao/note/tool/img/ImageProcess;->nMatToBitmap(JLandroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method public static native balancing(JJDZ)V
.end method

.method public static native blackEnhancing(JJDZ)V
.end method

.method public static coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 2
    .parameter "a"
    .parameter "b"

    .prologue
    .line 84
    invoke-static {p0, p1}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    const/high16 v1, 0x3f00

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs containsInfinitePoint([Landroid/graphics/PointF;)Z
    .locals 3
    .parameter "points"

    .prologue
    const v2, 0x7f7fffff

    .line 68
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 69
    aget-object v1, p0, v0

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    aget-object v1, p0, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    .line 70
    :cond_0
    const/4 v1, 0x1

    .line 73
    :goto_1
    return v1

    .line 68
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static convertOpenCVPoint(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/opencv/core/Point;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, vertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    if-nez p0, :cond_1

    .line 202
    const/4 v2, 0x0

    .line 208
    :cond_0
    return-object v2

    .line 204
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v2, result:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/opencv/core/Point;

    .line 206
    .local v1, point:Lorg/opencv/core/Point;
    new-instance v3, Landroid/graphics/PointF;

    iget-wide v4, v1, Lorg/opencv/core/Point;->x:D

    double-to-float v4, v4

    iget-wide v5, v1, Lorg/opencv/core/Point;->y:D

    double-to-float v5, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static copyPointF(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1
    .parameter "a"
    .parameter "b"

    .prologue
    .line 267
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget v0, p0, Landroid/graphics/PointF;->x:F

    iput v0, p1, Landroid/graphics/PointF;->x:F

    .line 271
    iget v0, p0, Landroid/graphics/PointF;->y:F

    iput v0, p1, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method public static crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 4
    .parameter "a"
    .parameter "b"
    .parameter "c"

    .prologue
    .line 47
    iget v0, p2, Landroid/graphics/PointF;->y:F

    iget v1, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/PointF;->x:F

    iget v2, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method public static native edgeEnhancing(JJDZ)V
.end method

.method public static native getAnimFrame(JJJJII)V
.end method

.method public static native getDestImageSize(JJ)[I
.end method

.method public static getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 6
    .parameter "a"
    .parameter "b"

    .prologue
    const-wide/high16 v4, 0x4000

    .line 88
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private static getLineIntersectionPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 11
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "d"

    .prologue
    .line 114
    iget v8, p1, Landroid/graphics/PointF;->x:F

    iget v9, p0, Landroid/graphics/PointF;->x:F

    sub-float v1, v8, v9

    .line 115
    .local v1, xba:F
    iget v8, p1, Landroid/graphics/PointF;->y:F

    iget v9, p0, Landroid/graphics/PointF;->y:F

    sub-float v5, v8, v9

    .line 116
    .local v5, yba:F
    iget v8, p3, Landroid/graphics/PointF;->x:F

    iget v9, p2, Landroid/graphics/PointF;->x:F

    sub-float v3, v8, v9

    .line 117
    .local v3, xdc:F
    iget v8, p3, Landroid/graphics/PointF;->y:F

    iget v9, p2, Landroid/graphics/PointF;->y:F

    sub-float v7, v8, v9

    .line 118
    .local v7, ydc:F
    iget v8, p2, Landroid/graphics/PointF;->x:F

    iget v9, p0, Landroid/graphics/PointF;->x:F

    sub-float v2, v8, v9

    .line 119
    .local v2, xca:F
    iget v8, p2, Landroid/graphics/PointF;->y:F

    iget v9, p0, Landroid/graphics/PointF;->y:F

    sub-float v6, v8, v9

    .line 120
    .local v6, yca:F
    mul-float v8, v1, v3

    mul-float/2addr v8, v6

    mul-float v9, v5, v3

    iget v10, p0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    mul-float v9, v1, v7

    iget v10, p2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v9, v10

    sub-float v0, v8, v9

    .line 122
    .local v0, x:F
    mul-float v8, v5, v3

    mul-float v9, v1, v7

    sub-float/2addr v8, v9

    div-float/2addr v0, v8

    .line 126
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 127
    const v0, 0x7f7fffff

    .line 129
    :cond_0
    mul-float v8, v5, v7

    mul-float/2addr v8, v2

    mul-float v9, v1, v7

    iget v10, p0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    mul-float v9, v5, v3

    iget v10, p2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v10

    sub-float v4, v8, v9

    .line 131
    .local v4, y:F
    mul-float v8, v1, v7

    mul-float v9, v5, v3

    sub-float/2addr v8, v9

    div-float/2addr v4, v8

    .line 135
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 136
    const v4, 0x7f7fffff

    .line 138
    :cond_1
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v0, v4}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v8
.end method

.method public static getLineSegmentIntersectionPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 7
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "d"

    .prologue
    const v3, 0x7f7fffff

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 146
    invoke-static {p0, p1, p2, p3}, Lcom/youdao/note/tool/img/ImageProcess;->getLineIntersectionPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    .line 148
    .local v0, point:Landroid/graphics/PointF;
    iget v2, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v2, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 158
    .end local v0           #point:Landroid/graphics/PointF;
    :cond_1
    :goto_0
    return-object v0

    .line 152
    .restart local v0       #point:Landroid/graphics/PointF;
    :cond_2
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget v4, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    iget v4, p1, Landroid/graphics/PointF;->y:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    cmpg-float v2, v2, v6

    if-gez v2, :cond_3

    move-object v0, v1

    .line 153
    goto :goto_0

    .line 155
    :cond_3
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget v3, p3, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    iget v4, p3, Landroid/graphics/PointF;->y:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    cmpg-float v2, v2, v6

    if-gez v2, :cond_1

    move-object v0, v1

    .line 156
    goto :goto_0
.end method

.method private static getMidPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 5
    .parameter "a"
    .parameter "b"

    .prologue
    const/high16 v4, 0x4000

    .line 95
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    div-float/2addr v1, v4

    iget v2, p0, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, v3

    div-float/2addr v2, v4

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public static varargs isClockwise([Landroid/graphics/PointF;)Z
    .locals 14
    .parameter "p"

    .prologue
    .line 242
    if-nez p0, :cond_0

    .line 243
    const/4 v10, 0x0

    .line 260
    :goto_0
    return v10

    .line 245
    :cond_0
    array-length v10, p0

    const/4 v11, 0x3

    if-ge v10, v11, :cond_1

    .line 246
    const/4 v10, 0x1

    goto :goto_0

    .line 248
    :cond_1
    const/4 v7, 0x0

    .line 249
    .local v7, sumRadian:F
    const/4 v5, 0x0

    .local v5, i:I
    :goto_1
    array-length v10, p0

    if-ge v5, v10, :cond_4

    .line 250
    aget-object v0, p0, v5

    .line 251
    .local v0, a:Landroid/graphics/PointF;
    add-int/lit8 v10, v5, 0x1

    array-length v11, p0

    rem-int/2addr v10, v11

    aget-object v1, p0, v10

    .line 252
    .local v1, b:Landroid/graphics/PointF;
    add-int/lit8 v10, v5, 0x2

    array-length v11, p0

    rem-int/2addr v10, v11

    aget-object v2, p0, v10

    .line 253
    .local v2, c:Landroid/graphics/PointF;
    invoke-static {v0, v1}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v8

    .line 254
    .local v8, vectorABLength:F
    invoke-static {v1, v2}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v9

    .line 255
    .local v9, vectorBCLength:F
    iget v10, v1, Landroid/graphics/PointF;->x:F

    iget v11, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    iget v11, v2, Landroid/graphics/PointF;->x:F

    iget v12, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v11, v12

    mul-float/2addr v10, v11

    iget v11, v1, Landroid/graphics/PointF;->y:F

    iget v12, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v11, v12

    iget v12, v2, Landroid/graphics/PointF;->y:F

    iget v13, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float v4, v10, v11

    .line 256
    .local v4, dotProduct:F
    mul-float v10, v8, v9

    div-float v10, v4, v10

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->acos(D)D

    move-result-wide v10

    double-to-float v6, v10

    .line 257
    .local v6, radian:F
    iget v10, v1, Landroid/graphics/PointF;->x:F

    iget v11, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    iget v11, v2, Landroid/graphics/PointF;->y:F

    iget v12, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v11, v12

    mul-float/2addr v10, v11

    iget v11, v1, Landroid/graphics/PointF;->y:F

    iget v12, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v11, v12

    iget v12, v2, Landroid/graphics/PointF;->x:F

    iget v13, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_2

    const/4 v3, 0x1

    .line 258
    .local v3, clockwise:Z
    :goto_2
    if-eqz v3, :cond_3

    .end local v6           #radian:F
    :goto_3
    add-float/2addr v7, v6

    .line 249
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 257
    .end local v3           #clockwise:Z
    .restart local v6       #radian:F
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 258
    .restart local v3       #clockwise:Z
    :cond_3
    neg-float v6, v6

    goto :goto_3

    .line 260
    .end local v0           #a:Landroid/graphics/PointF;
    .end local v1           #b:Landroid/graphics/PointF;
    .end local v2           #c:Landroid/graphics/PointF;
    .end local v3           #clockwise:Z
    .end local v4           #dotProduct:F
    .end local v6           #radian:F
    .end local v8           #vectorABLength:F
    .end local v9           #vectorBCLength:F
    :cond_4
    const/4 v10, 0x0

    cmpl-float v10, v7, v10

    if-lez v10, :cond_5

    const/4 v10, 0x1

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public static isInRectangle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 6
    .parameter "a"
    .parameter "b"
    .parameter "c"

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 102
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/graphics/PointF;

    aput-object p0, v2, v1

    aput-object p1, v2, v0

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v2}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    :goto_0
    return v1

    :cond_0
    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_1

    iget v2, p2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/PointF;->y:F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static isInSameLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 2
    .parameter "a"
    .parameter "b"
    .parameter "c"

    .prologue
    .line 77
    invoke-static {p0, p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    const/high16 v1, 0x3f00

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 5
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "d"

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 54
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/graphics/PointF;

    aput-object p0, v2, v0

    aput-object p1, v2, v1

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    invoke-static {v2}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p0, p1, p3}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    invoke-static {p2, p3, p0}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p2, p3, p1}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_4

    :cond_2
    invoke-static {p0, p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p0, p1, p3}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpl-float v2, v2, v4

    if-nez v2, :cond_3

    invoke-static {p2, p3, p0}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p2, p3, p1}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_4

    :cond_3
    invoke-static {p0, p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p0, p1, p3}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    invoke-static {p2, p3, p0}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v2

    invoke-static {p2, p3, p1}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    mul-float/2addr v2, v3

    cmpl-float v2, v2, v4

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static isLoadImageLibSuccess()Z
    .locals 1

    .prologue
    .line 32
    sget-boolean v0, Lcom/youdao/note/tool/img/ImageProcess;->loadLibSuccess:Z

    return v0
.end method

.method public static isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 9
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "d"

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 165
    const/4 v6, 0x4

    new-array v6, v6, [Landroid/graphics/PointF;

    aput-object p0, v6, v4

    aput-object p1, v6, v5

    const/4 v7, 0x2

    aput-object p2, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-static {v6}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v4

    .line 168
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/youdao/note/tool/img/ImageProcess;->isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {p0, p3, p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 171
    invoke-static {p0, p3}, Lcom/youdao/note/tool/img/ImageProcess;->getMidPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    .line 172
    .local v1, midAD:Landroid/graphics/PointF;
    invoke-static {p1, p2}, Lcom/youdao/note/tool/img/ImageProcess;->getMidPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 173
    .local v2, midBC:Landroid/graphics/PointF;
    invoke-static {v1, v2, p0}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v6

    invoke-static {v1, v2, p1}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v7

    mul-float/2addr v6, v7

    cmpg-float v6, v6, v8

    if-ltz v6, :cond_0

    invoke-static {v1, v2, p3}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v6

    invoke-static {v1, v2, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v7

    mul-float/2addr v6, v7

    cmpg-float v6, v6, v8

    if-ltz v6, :cond_0

    .line 177
    invoke-static {p0, p1}, Lcom/youdao/note/tool/img/ImageProcess;->getMidPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    .line 178
    .local v0, midAB:Landroid/graphics/PointF;
    invoke-static {p3, p2}, Lcom/youdao/note/tool/img/ImageProcess;->getMidPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    .line 179
    .local v3, midDC:Landroid/graphics/PointF;
    invoke-static {v0, v3, p0}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v6

    invoke-static {v0, v3, p3}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v7

    mul-float/2addr v6, v7

    cmpg-float v6, v6, v8

    if-ltz v6, :cond_0

    invoke-static {v0, v3, p1}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v6

    invoke-static {v0, v3, p2}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v7

    mul-float/2addr v6, v7

    cmpg-float v6, v6, v8

    if-ltz v6, :cond_0

    move v4, v5

    .line 183
    goto :goto_0
.end method

.method private static native nBitmapToMat(Landroid/graphics/Bitmap;)J
.end method

.method private static native nMatToBitmap(JLandroid/graphics/Bitmap;)Z
.end method

.method public static native quadrangleLocation(JJ)V
.end method

.method public static native rectify(JJJ[I)V
.end method

.method public static rotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;FFZ)Landroid/graphics/PointF;
    .locals 8
    .parameter "point"
    .parameter "central"
    .parameter "oldScale"
    .parameter "newScale"
    .parameter "clockwise"

    .prologue
    .line 187
    iget v4, p0, Landroid/graphics/PointF;->x:F

    iget v5, p1, Landroid/graphics/PointF;->x:F

    sub-float v2, v4, v5

    .line 188
    .local v2, oldVectorX:F
    iget v4, p0, Landroid/graphics/PointF;->y:F

    iget v5, p1, Landroid/graphics/PointF;->y:F

    sub-float v3, v4, v5

    .line 190
    .local v3, oldVectorY:F
    if-eqz p4, :cond_0

    .line 191
    neg-float v0, v3

    .line 192
    .local v0, newVectorX:F
    move v1, v2

    .line 197
    .local v1, newVectorY:F
    :goto_0
    new-instance v4, Landroid/graphics/PointF;

    mul-float v5, v0, p3

    div-float/2addr v5, p2

    iget v6, p1, Landroid/graphics/PointF;->x:F

    add-float/2addr v5, v6

    mul-float v6, v1, p3

    div-float/2addr v6, p2

    iget v7, p1, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v4

    .line 194
    .end local v0           #newVectorX:F
    .end local v1           #newVectorY:F
    :cond_0
    move v0, v3

    .line 195
    .restart local v0       #newVectorX:F
    neg-float v1, v2

    .restart local v1       #newVectorY:F
    goto :goto_0
.end method

.method public static native scale(JJ)V
.end method

.method public static scale(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 6
    .parameter "srcBitmap"
    .parameter "dstBitmap"

    .prologue
    .line 327
    invoke-static {p0}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v1

    .line 328
    .local v1, srcMat:Lorg/opencv/core/Mat;
    invoke-static {p1}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 329
    .local v0, dstMat:Lorg/opencv/core/Mat;
    iget-wide v2, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lcom/youdao/note/tool/img/ImageProcess;->scale(JJ)V

    .line 330
    invoke-static {v0, p1}, Lcom/youdao/note/tool/img/ImageProcess;->MatToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)Z

    .line 331
    return-void
.end method

.method private static native smooth(JJ)V
.end method

.method public static smooth(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 6
    .parameter "srcBitmap"
    .parameter "dstBitmap"

    .prologue
    .line 334
    invoke-static {p0}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v1

    .line 335
    .local v1, srcMat:Lorg/opencv/core/Mat;
    invoke-static {p1}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v0

    .line 336
    .local v0, dstMat:Lorg/opencv/core/Mat;
    iget-wide v2, v1, Lorg/opencv/core/Mat;->nativeObj:J

    iget-wide v4, v0, Lorg/opencv/core/Mat;->nativeObj:J

    invoke-static {v2, v3, v4, v5}, Lcom/youdao/note/tool/img/ImageProcess;->smooth(JJ)V

    .line 337
    invoke-static {v0, p1}, Lcom/youdao/note/tool/img/ImageProcess;->MatToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)Z

    .line 338
    return-void
.end method

.method public static sortVertices(Ljava/util/List;)V
    .locals 14
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, vertices:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x4

    .line 215
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-eq v5, v9, :cond_1

    .line 236
    :cond_0
    return-void

    .line 218
    :cond_1
    const/4 v5, 0x6

    new-array v1, v5, [[I

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    aput-object v5, v1, v10

    new-array v5, v9, [I

    fill-array-data v5, :array_1

    aput-object v5, v1, v11

    new-array v5, v9, [I

    fill-array-data v5, :array_2

    aput-object v5, v1, v12

    new-array v5, v9, [I

    fill-array-data v5, :array_3

    aput-object v5, v1, v13

    new-array v5, v9, [I

    fill-array-data v5, :array_4

    aput-object v5, v1, v9

    const/4 v5, 0x5

    new-array v6, v9, [I

    fill-array-data v6, :array_5

    aput-object v6, v1, v5

    .line 220
    .local v1, arrangements:[[I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    array-length v5, v1

    if-ge v2, v5, :cond_0

    .line 221
    aget-object v0, v1, v2

    .line 222
    .local v0, arrangement:[I
    aget v5, v0, v10

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aget v6, v0, v11

    invoke-interface {p0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    aget v7, v0, v12

    invoke-interface {p0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/PointF;

    aget v8, v0, v13

    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/PointF;

    invoke-static {v5, v6, v7, v8}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v5

    if-eqz v5, :cond_3

    new-array v6, v9, [Landroid/graphics/PointF;

    aget v5, v0, v10

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aput-object v5, v6, v10

    aget v5, v0, v11

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aput-object v5, v6, v11

    aget v5, v0, v12

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aput-object v5, v6, v12

    aget v5, v0, v13

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aput-object v5, v6, v13

    invoke-static {v6}, Lcom/youdao/note/tool/img/ImageProcess;->isClockwise([Landroid/graphics/PointF;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 226
    new-array v4, v9, [Landroid/graphics/PointF;

    .line 227
    .local v4, temp:[Landroid/graphics/PointF;
    const/4 v3, 0x0

    .local v3, j:I
    :goto_1
    if-ge v3, v9, :cond_2

    .line 228
    aget v5, v0, v3

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    aput-object v5, v4, v3

    .line 227
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 230
    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v9, :cond_0

    .line 231
    aget-object v5, v4, v3

    invoke-interface {p0, v3, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 230
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 220
    .end local v3           #j:I
    .end local v4           #temp:[Landroid/graphics/PointF;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 218
    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_1
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_3
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_4
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_5
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public static toGray(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .parameter "bmpOriginal"

    .prologue
    const/4 v8, 0x0

    .line 309
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 310
    .local v4, height:I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 312
    .local v6, width:I
    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 313
    .local v0, bmpGrayscale:Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 314
    .local v1, c:Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 315
    .local v5, paint:Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/ColorMatrix;

    invoke-direct {v2}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 316
    .local v2, cm:Landroid/graphics/ColorMatrix;
    invoke-virtual {v2, v8}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 317
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 318
    .local v3, f:Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 319
    invoke-virtual {v1, p0, v8, v8, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 320
    return-object v0
.end method
