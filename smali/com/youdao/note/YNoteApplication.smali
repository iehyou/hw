.class public Lcom/youdao/note/YNoteApplication;
.super Landroid/app/Application;
.source "YNoteApplication.java"

# interfaces
.implements Lcom/youdao/note/utils/PreferenceKeys;
.implements Lcom/youdao/note/utils/EmptyInstance;
.implements Lcom/youdao/note/activity/ActivityConsts;
.implements Lcom/youdao/note/utils/Consts;


# static fields
.field private static final DEBUG_KEY:Ljava/lang/String; = "is_debug"

.field private static final RESETDB:Z = false

.field private static final TAG:Ljava/lang/String; = null

.field public static final UNLOGIN_NOTEBOOK:Ljava/lang/String; = "default_notebook"

.field public static final UNLOGIN_USER:Ljava/lang/String; = "unlogin@unlogin"

.field private static mYNoteInstance:Lcom/youdao/note/YNoteApplication;


# instance fields
.field private isCmwap:Z

.field private isRecording:Z

.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field private mLogRecorder:Lcom/youdao/note/LogRecorder;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSyncManager:Lcom/youdao/note/task/SyncManager;

.field private mTaskManager:Lcom/youdao/note/task/TaskManager;

.field private receiver:Landroid/content/BroadcastReceiver;

.field private skitchManager:Lcom/youdao/note/ui/skitch/SkitchMetaManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/YNoteApplication;->TAG:Ljava/lang/String;

    .line 81
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/YNoteApplication;->mYNoteInstance:Lcom/youdao/note/YNoteApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 75
    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 77
    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 79
    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    .line 83
    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 85
    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    .line 89
    new-instance v0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->skitchManager:Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    .line 91
    iput-boolean v1, p0, Lcom/youdao/note/YNoteApplication;->isRecording:Z

    .line 93
    new-instance v0, Lcom/youdao/note/YNoteApplication$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/YNoteApplication$1;-><init>(Lcom/youdao/note/YNoteApplication;)V

    iput-object v0, p0, Lcom/youdao/note/YNoteApplication;->receiver:Landroid/content/BroadcastReceiver;

    .line 566
    iput-boolean v1, p0, Lcom/youdao/note/YNoteApplication;->isCmwap:Z

    return-void
.end method

.method public static getInstance()Lcom/youdao/note/YNoteApplication;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/youdao/note/YNoteApplication;->mYNoteInstance:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method


# virtual methods
.method public acceptLicense()V
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "license_accepted"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 257
    return-void
.end method

.method public attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .parameter "base"

    .prologue
    .line 521
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 522
    return-void
.end method

.method public clearData()V
    .locals 3

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isDebug()Z

    move-result v0

    .line 183
    .local v0, debug:Z
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    invoke-virtual {p0, v0}, Lcom/youdao/note/YNoteApplication;->setDebug(Z)V

    .line 185
    sget-object v1, Lcom/youdao/note/YNoteApplication;->TAG:Ljava/lang/String;

    const-string v2, "clear cache."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->clearCache()Z

    .line 187
    sget-object v1, Lcom/youdao/note/YNoteApplication;->TAG:Ljava/lang/String;

    const-string v2, "reset data base."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->resetDataBase()Z

    .line 189
    return-void
.end method

.method public getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .locals 3

    .prologue
    .line 535
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/youdao/note/YNoteApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 536
    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 537
    .local v1, info:Landroid/net/NetworkInfo;
    return-object v1
.end method

.method public getAutoSyncPeriod()I
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, autoSyncPeriodKey:Ljava/lang/String;
    iget-object v2, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "-1"

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 345
    .local v1, peroid:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    const/4 v2, -0x1

    .line 348
    :goto_0
    return v2

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public getDataSource()Lcom/youdao/note/datasource/DataSource;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    return-object v0
.end method

.method public getDefaultNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getUserMeta()Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v0

    .line 316
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "default_notebook"

    goto :goto_0
.end method

.method public getDoodlePaintWidth()F
    .locals 3

    .prologue
    .line 425
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "doodle_paint_width"

    sget v2, Lcom/youdao/note/ui/skitch/SkitchConsts;->DEFAULT_PAINT_WIDTH:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getHandWritePaintWidth()F
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "handwrite_paint_width"

    sget v2, Lcom/youdao/note/ui/skitch/SkitchConsts;->DEFAULT_PAINT_WIDTH:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getHandwriteMode()I
    .locals 3

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isOpengGL2Available()Z

    move-result v1

    if-nez v1, :cond_0

    .line 358
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/youdao/note/YNoteApplication;->setHandwriteMode(I)V

    .line 360
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 361
    .local v0, handwriteModeKey:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "1"

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getHandwriteWordHeight()I
    .locals 4

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 597
    .local v0, height:I
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 602
    .local v1, max_height:I
    const/16 v2, 0x46

    if-ge v1, v2, :cond_0

    .line 603
    const/16 v1, 0x46

    .line 605
    :cond_0
    if-le v0, v1, :cond_1

    .end local v1           #max_height:I
    :goto_0
    return v1

    .restart local v1       #max_height:I
    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "note.corp.youdao.com/"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "note.youdao.com/"

    goto :goto_0
.end method

.method public getIgnoreVersion()I
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "ignore_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getImageQuality()I
    .locals 3

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 353
    .local v0, imageQUalityKey:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getKeyFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "note."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageVersionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastCheckUpdateTime()J
    .locals 4

    .prologue
    .line 400
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "last_chcekupdate"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastLoginUserName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "last_username"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogRecorder()Lcom/youdao/note/LogRecorder;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    return-object v0
.end method

.method public getMaxResourceSize()J
    .locals 2

    .prologue
    .line 217
    const-wide/32 v0, 0x1800000

    return-wide v0
.end method

.method public getPackageVersionName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 175
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "unknowVersionName"

    goto :goto_0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "password"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPersistCookie()Ljava/lang/String;
    .locals 3

    .prologue
    .line 388
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "cookie"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPinLock()Ljava/lang/String;
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pinlock"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSessionCookie()Ljava/lang/String;
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "session_cookie"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->skitchManager:Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    return-object v0
.end method

.method public getStoreDir()Ljava/io/File;
    .locals 3

    .prologue
    .line 541
    const/4 v0, 0x0

    .line 542
    .local v0, dataDir:Ljava/io/File;
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 548
    :goto_0
    return-object v0

    .line 546
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public getSyncManager()Lcom/youdao/note/task/SyncManager;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    return-object v0
.end method

.method public getTaskManager()Lcom/youdao/note/task/TaskManager;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    return-object v0
.end method

.method public getUpdateUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ynote.elibom.youdao.com/noteproxy/update"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?keyfrom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "http://m.note.youdao.com/noteproxy/update"

    goto :goto_0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/youdao/note/YNoteApplication;->USERNAME:Ljava/lang/String;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, username:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    const-string v0, "unlogin@unlogin"

    .line 234
    .end local v0           #username:Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getYNoteDBName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    const-string v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_ynote.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYNoteSearchHistryDBName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 478
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    const-string v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_search_history.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDebug()Z
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "is_debug"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isEverLogin()Z
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "ever_login"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isFirstTime()Z
    .locals 5

    .prologue
    .line 264
    iget-object v2, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "last_use_version"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, lastUseVersion:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageVersionName()Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, version:Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isLoadImageLibSuccess()Z
    .locals 1

    .prologue
    .line 581
    invoke-static {}, Lcom/youdao/note/tool/img/ImageProcess;->isLoadImageLibSuccess()Z

    move-result v0

    return v0
.end method

.method public isLoadRecortLibSuccess()Z
    .locals 1

    .prologue
    .line 577
    invoke-static {}, Lcom/youdao/note/ui/audio/YNoteRecorder;->isLoadLibSuccess()Z

    move-result v0

    return v0
.end method

.method public isLogin()Z
    .locals 2

    .prologue
    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cookies is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPersistCookie()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPersistCookie()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPersistCookie()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const-string v1, "NTES_PASSPORT"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getUserMeta()Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNetworkAvailable()Z
    .locals 2

    .prologue
    .line 525
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 526
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOpengGL2Available()Z
    .locals 4

    .prologue
    .line 586
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/youdao/note/YNoteApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 587
    .local v0, activityManager:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v1

    .line 589
    .local v1, configurationInfo:Landroid/content/pm/ConfigurationInfo;
    iget v2, v1, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    const v3, 0x10001

    if-lt v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPinlockEnable()Z
    .locals 3

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, key:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/youdao/note/YNoteApplication;->isRecording:Z

    return v0
.end method

.method public isShowHandwriteGuide()Z
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "show_handwrite_guide"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isShowHandwriteModeTips()Z
    .locals 3

    .prologue
    .line 291
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "show_handwrite_mode_tips"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isShowMailGuide()Z
    .locals 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "show_mail_guide"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isUsingCMWAP()Z
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/youdao/note/YNoteApplication;->isCmwap:Z

    return v0
.end method

.method public isWifiAvailable()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 530
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 531
    .local v0, info:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public licenseAccepted()Z
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "license_accepted"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public logOut()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->stopAutoSync()V

    .line 457
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->stopAll()V

    .line 458
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/youdao/note/YNoteApplication;->setPersistCookie(Ljava/lang/String;)Z

    .line 459
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/YNoteApplication;->setPinlockEnable(Z)Z

    .line 460
    return-void
.end method

.method public markDeleteNote(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)V
    .locals 4
    .parameter "activity"
    .parameter "noteMeta"

    .prologue
    .line 516
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0, p2}, Lcom/youdao/note/datasource/DataSource;->markDeleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 517
    const v0, 0x7f0a00a8

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showFormattedToast(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 518
    return-void
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 117
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 118
    sput-object p0, Lcom/youdao/note/YNoteApplication;->mYNoteInstance:Lcom/youdao/note/YNoteApplication;

    .line 119
    new-instance v3, Lcom/youdao/note/datasource/DataSource;

    invoke-direct {v3, p0}, Lcom/youdao/note/datasource/DataSource;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 120
    new-instance v3, Lcom/youdao/note/task/TaskManager;

    iget-object v4, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {v3, v4}, Lcom/youdao/note/task/TaskManager;-><init>(Lcom/youdao/note/datasource/DataSource;)V

    iput-object v3, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 121
    new-instance v3, Lcom/youdao/note/task/SyncManager;

    iget-object v4, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v5, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {v3, v4, v5}, Lcom/youdao/note/task/SyncManager;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/datasource/DataSource;)V

    iput-object v3, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    .line 122
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getAutoSyncPeriod()I

    move-result v3

    if-gtz v3, :cond_1

    .line 123
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v3}, Lcom/youdao/note/task/SyncManager;->stopAutoSync()V

    .line 127
    :goto_0
    new-instance v3, Lcom/youdao/note/LogRecorder;

    invoke-direct {v3, p0}, Lcom/youdao/note/LogRecorder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    .line 128
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v3}, Lcom/youdao/note/LogRecorder;->reportIfNecessary()V

    .line 129
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v3}, Lcom/youdao/note/LogRecorder;->start()V

    .line 131
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-static {v3}, Lcom/youdao/note/utils/L;->setLogger(Lcom/youdao/note/LogRecorder;)V

    .line 135
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 137
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "debug"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/youdao/note/utils/L;->setDebug(Z)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    :goto_1
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    new-instance v3, Lcom/youdao/note/YNoteApplication$2;

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Lcom/youdao/note/utils/IpUtils;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/youdao/note/YNoteApplication$2;-><init>(Lcom/youdao/note/YNoteApplication;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication$2;->execute()V

    .line 163
    :cond_0
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 164
    .local v2, filter:Landroid/content/IntentFilter;
    const-string v3, "com.youdao.note.action.ACCESS_DENIED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/youdao/note/YNoteApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 167
    return-void

    .line 125
    .end local v2           #filter:Landroid/content/IntentFilter;
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getAutoSyncPeriod()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/task/SyncManager;->resetTime(I)V

    goto :goto_0

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public sendEditNote(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2
    .parameter "activity"
    .parameter "noteId"

    .prologue
    .line 509
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/EditNoteActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 510
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 511
    const-string v1, "noteid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 513
    return-void
.end method

.method public sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2
    .parameter "activity"
    .parameter "action"

    .prologue
    .line 552
    iget-boolean v1, p0, Lcom/youdao/note/YNoteApplication;->isRecording:Z

    if-eqz v1, :cond_1

    .line 553
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/MainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 557
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 558
    if-eqz p2, :cond_2

    .line 559
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 561
    :cond_2
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 562
    instance-of v1, p1, Lcom/youdao/note/activity/MainActivity;

    if-nez v1, :cond_0

    .line 563
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public sendToSearch(Landroid/app/Activity;)V
    .locals 2
    .parameter "activity"

    .prologue
    .line 504
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/SearchActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 505
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 506
    return-void
.end method

.method public sendUrl(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2
    .parameter "activity"
    .parameter "url"

    .prologue
    .line 463
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 464
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 465
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 466
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 467
    return-void
.end method

.method public setCookie(Lcom/youdao/note/data/LoginResult;)Z
    .locals 1
    .parameter "loginResult"

    .prologue
    .line 378
    invoke-virtual {p1}, Lcom/youdao/note/data/LoginResult;->getPersistCookie()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/YNoteApplication;->setPersistCookie(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/youdao/note/data/LoginResult;->getSessionCookie()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/YNoteApplication;->setSessionCookie(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDebug(Z)V
    .locals 2
    .parameter "debug"

    .prologue
    .line 205
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "is_debug"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 206
    return-void
.end method

.method public setDoodlePaintWidth(F)V
    .locals 2
    .parameter "width"

    .prologue
    .line 429
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "doodle_paint_width"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 430
    return-void
.end method

.method public setEverLogin()Z
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ever_login"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setHandWritePaintWidth(F)V
    .locals 2
    .parameter "width"

    .prologue
    .line 437
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "handwrite_paint_width"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 438
    return-void
.end method

.method public setHandwriteMode(I)V
    .locals 3
    .parameter "mode"

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, handwriteModeKey:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 367
    return-void
.end method

.method public setIgnoreVersion(I)Z
    .locals 2
    .parameter "version"

    .prologue
    .line 441
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ignore_version"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setIsCmwap(Z)V
    .locals 0
    .parameter "isCmwap"

    .prologue
    .line 573
    iput-boolean p1, p0, Lcom/youdao/note/YNoteApplication;->isCmwap:Z

    .line 574
    return-void
.end method

.method public setIsRecording(Z)V
    .locals 0
    .parameter "recording"

    .prologue
    .line 201
    iput-boolean p1, p0, Lcom/youdao/note/YNoteApplication;->isRecording:Z

    .line 202
    return-void
.end method

.method public setNotFirst()V
    .locals 3

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getPackageVersionName()Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, version:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_use_version"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 272
    return-void
.end method

.method public setPassword(Ljava/lang/String;)Z
    .locals 2
    .parameter "password"

    .prologue
    .line 374
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "password"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setPersistCookie(Ljava/lang/String;)Z
    .locals 4
    .parameter "persistCookie"

    .prologue
    .line 384
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "cookie"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NTES_PASSPORT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setPinLock(I)Z
    .locals 3
    .parameter "pinCode"

    .prologue
    .line 412
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pinlock"

    invoke-static {p1}, Lcom/youdao/note/utils/IdUtils;->genPinCode(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setPinlockEnable(Z)Z
    .locals 3
    .parameter "enable"

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, key:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    return v1
.end method

.method public setSessionCookie(Ljava/lang/String;)Z
    .locals 4
    .parameter "sessionCookie"

    .prologue
    .line 392
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "session_cookie"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NTES_SESS="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method public setShowHandwriteGuide(Z)V
    .locals 2
    .parameter "isShow"

    .prologue
    .line 287
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_handwrite_guide"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 288
    return-void
.end method

.method public setShowHandwriteModeTips(Z)V
    .locals 2
    .parameter "isShow"

    .prologue
    .line 295
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_handwrite_mode_tips"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 296
    return-void
.end method

.method public setShowMailGuide(Z)V
    .locals 2
    .parameter "isShow"

    .prologue
    .line 279
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_mail_guide"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 280
    return-void
.end method

.method public setSkitchManager(Lcom/youdao/note/ui/skitch/SkitchMetaManager;)V
    .locals 0
    .parameter "skitchManager"

    .prologue
    .line 500
    iput-object p1, p0, Lcom/youdao/note/YNoteApplication;->skitchManager:Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    .line 501
    return-void
.end method

.method public setUserName(Ljava/lang/String;)Z
    .locals 2
    .parameter "userName"

    .prologue
    .line 238
    if-nez p1, :cond_0

    .line 239
    const-string p1, ""

    .line 241
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 242
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/youdao/note/YNoteApplication;->USERNAME:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->switchUser()V

    .line 244
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_username"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 245
    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopAllTask()V
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->stopAutoSync()V

    .line 491
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->stopAll()V

    .line 492
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->stop()V

    .line 493
    return-void
.end method

.method public stopAutoSync()V
    .locals 3

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, autoSyncPeriodKey:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "-1"

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 339
    iget-object v1, p0, Lcom/youdao/note/YNoteApplication;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v1}, Lcom/youdao/note/task/SyncManager;->stopAutoSync()V

    .line 340
    return-void
.end method

.method public syncOnlyUnderWifi()Z
    .locals 3

    .prologue
    .line 325
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public updateCheckUpdateTime()Z
    .locals 4

    .prologue
    .line 404
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_chcekupdate"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method
