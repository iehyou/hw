.class public final Lcom/youdao/note/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final LoginActivity:I = 0x7f070066

.field public static final MainActivity:I = 0x7f070003

.field public static final NewNoteActivity:I = 0x7f07007f

.field public static final NewNoteActivityButton:I = 0x7f070097

.field public static final NoteBookListActivity:I = 0x7f070082

.field public static final NoteListActivity:I = 0x7f070081

.field public static final SearchActivity:I = 0x7f0700c3

.field public static final about:I = 0x7f0700ba

.field public static final accept:I = 0x7f070057

.field public static final account:I = 0x7f07005e

.field public static final actions:I = 0x7f07002a

.field public static final alphabet_scrollbar:I = 0x7f07006b

.field public static final attach_icon:I = 0x7f070098

.field public static final audio_pause:I = 0x7f070009

.field public static final audio_play:I = 0x7f070008

.field public static final audio_player:I = 0x7f07002b

.field public static final audio_record:I = 0x7f07000e

.field public static final audio_recorder:I = 0x7f07002c

.field public static final audio_seek_bar:I = 0x7f07000a

.field public static final audio_stop:I = 0x7f07000d

.field public static final btn_edit:I = 0x7f070092

.field public static final btn_mail_share:I = 0x7f070093

.field public static final button_cancel:I = 0x7f07006f

.field public static final button_gallary:I = 0x7f0700c2

.field public static final button_new:I = 0x7f070005

.field public static final button_ok:I = 0x7f07006e

.field public static final button_photo:I = 0x7f070006

.field public static final button_record:I = 0x7f070007

.field public static final button_search:I = 0x7f070004

.field public static final camera:I = 0x7f070026

.field public static final cancel:I = 0x7f070015

.field public static final canvas:I = 0x7f070019

.field public static final checkBox1:I = 0x7f070070

.field public static final check_update:I = 0x7f0700b9

.field public static final clear_button:I = 0x7f070068

.field public static final clear_search_history:I = 0x7f0700c1

.field public static final code_box:I = 0x7f0700a0

.field public static final code_hint:I = 0x7f07009f

.field public static final complete:I = 0x7f070050

.field public static final contact_list:I = 0x7f070069

.field public static final container:I = 0x7f07002f

.field public static final container_top:I = 0x7f070078

.field public static final control:I = 0x7f0700a4

.field public static final copy_paste:I = 0x7f0700bc

.field public static final copyright:I = 0x7f070065

.field public static final create_time:I = 0x7f07008f

.field public static final curPos:I = 0x7f07000b

.field public static final date_divider:I = 0x7f070016

.field public static final delete:I = 0x7f070054

.field public static final delete_image:I = 0x7f070055

.field public static final delete_text:I = 0x7f070056

.field public static final deny:I = 0x7f070058

.field public static final detail:I = 0x7f0700be

.field public static final doSearch:I = 0x7f0700ad

.field public static final doodle:I = 0x7f070029

.field public static final doodle_color:I = 0x7f07001f

.field public static final doodle_colorbox:I = 0x7f07001b

.field public static final doodle_footer:I = 0x7f070021

.field public static final doodle_paint:I = 0x7f07001e

.field public static final doodle_paint_slider:I = 0x7f07001a

.field public static final doodle_save:I = 0x7f070022

.field public static final doodle_toolbar:I = 0x7f07001c

.field public static final doodle_trash:I = 0x7f070020

.field public static final doodle_undo:I = 0x7f07001d

.field public static final duration:I = 0x7f07000c

.field public static final edit:I = 0x7f0700bf

.field public static final edit_footer:I = 0x7f07002d

.field public static final edit_layout:I = 0x7f070031

.field public static final edit_text:I = 0x7f070032

.field public static final edit_toolbar:I = 0x7f070023

.field public static final emailer_address:I = 0x7f070073

.field public static final emailer_name:I = 0x7f070072

.field public static final empty:I = 0x7f07006c

.field public static final empty_notebooks:I = 0x7f070095

.field public static final empty_notes:I = 0x7f070094

.field public static final empty_text:I = 0x7f07006d

.field public static final empty_tip:I = 0x7f070096

.field public static final exit:I = 0x7f0700bb

.field public static final feedback:I = 0x7f0700b7

.field public static final fill:I = 0x7f070001

.field public static final fix_header:I = 0x7f07005b

.field public static final frame_layout:I = 0x7f070088

.field public static final fresh:I = 0x7f0700bd

.field public static final gallary:I = 0x7f070027

.field public static final handwrite_blank:I = 0x7f07003e

.field public static final handwrite_bottom:I = 0x7f070037

.field public static final handwrite_delete:I = 0x7f070040

.field public static final handwrite_footer:I = 0x7f070043

.field public static final handwrite_guide:I = 0x7f070045

.field public static final handwrite_guide_button_never_show:I = 0x7f070047

.field public static final handwrite_guide_button_start:I = 0x7f070046

.field public static final handwrite_pageView:I = 0x7f070038

.field public static final handwrite_paint:I = 0x7f070041

.field public static final handwrite_paint_slider:I = 0x7f07003b

.field public static final handwrite_return:I = 0x7f07003f

.field public static final handwrite_save:I = 0x7f070044

.field public static final handwrite_toolbar:I = 0x7f07003d

.field public static final handwrite_trash:I = 0x7f070042

.field public static final handwrite_view_header:I = 0x7f070039

.field public static final handwriting:I = 0x7f070028

.field public static final help:I = 0x7f070064

.field public static final html_content:I = 0x7f0700b0

.field public static final image:I = 0x7f07004e

.field public static final image_view_bitmap:I = 0x7f070051

.field public static final inidcator:I = 0x7f070034

.field public static final input_box:I = 0x7f070085

.field public static final keyboard:I = 0x7f0700a1

.field public static final keyword:I = 0x7f070067

.field public static final label_sub_text:I = 0x7f07009e

.field public static final label_text:I = 0x7f07005a

.field public static final linearLayout1:I = 0x7f070071

.field public static final linearLayout4:I = 0x7f07007b

.field public static final listView:I = 0x7f07006a

.field public static final list_label_layout:I = 0x7f070059

.field public static final login:I = 0x7f070060

.field public static final login_fields:I = 0x7f07005d

.field public static final login_layout:I = 0x7f07005c

.field public static final logo:I = 0x7f070061

.field public static final mail_add_receiver:I = 0x7f070079

.field public static final mail_info_edit:I = 0x7f07007c

.field public static final mail_receiver:I = 0x7f070077

.field public static final mail_selection_list_section:I = 0x7f070074

.field public static final mail_selection_toast:I = 0x7f070075

.field public static final mail_send:I = 0x7f07007e

.field public static final mail_share_guide:I = 0x7f07008c

.field public static final mail_title_edit:I = 0x7f07007a

.field public static final menu:I = 0x7f070048

.field public static final message:I = 0x7f07009a

.field public static final modify_time:I = 0x7f07008e

.field public static final new_notebook:I = 0x7f0700c0

.field public static final note_book_count:I = 0x7f070087

.field public static final note_book_title:I = 0x7f070086

.field public static final note_content:I = 0x7f070030

.field public static final note_label:I = 0x7f070017

.field public static final note_list_title:I = 0x7f070084

.field public static final note_loding:I = 0x7f07008a

.field public static final note_title:I = 0x7f070089

.field public static final notebook:I = 0x7f07008d

.field public static final ok:I = 0x7f070014

.field public static final option0:I = 0x7f070049

.field public static final option1:I = 0x7f07004a

.field public static final option2:I = 0x7f07004b

.field public static final option3:I = 0x7f07004c

.field public static final pageview:I = 0x7f07003a

.field public static final password:I = 0x7f07005f

.field public static final password_layout:I = 0x7f07009b

.field public static final percentage:I = 0x7f0700af

.field public static final pin_label_bg:I = 0x7f07009d

.field public static final player:I = 0x7f07008b

.field public static final recommend:I = 0x7f0700b6

.field public static final recommend_by_email:I = 0x7f0700a2

.field public static final recommend_by_message:I = 0x7f0700a3

.field public static final record:I = 0x7f070025

.field public static final recording:I = 0x7f07000f

.field public static final rectification:I = 0x7f070052

.field public static final rectification_text:I = 0x7f070053

.field public static final redo:I = 0x7f07004f

.field public static final regist_help:I = 0x7f070062

.field public static final register:I = 0x7f070063

.field public static final require_login_layout:I = 0x7f0700a6

.field public static final resource_detail:I = 0x7f0700aa

.field public static final resource_manage:I = 0x7f070024

.field public static final resource_size:I = 0x7f0700a9

.field public static final resource_text:I = 0x7f0700a7

.field public static final save:I = 0x7f07002e

.field public static final scrollView1:I = 0x7f070076

.field public static final search_history:I = 0x7f0700ab

.field public static final section_count:I = 0x7f070018

.field public static final setting:I = 0x7f0700b5

.field public static final signout:I = 0x7f0700b8

.field public static final snippet_image:I = 0x7f07007d

.field public static final source:I = 0x7f070091

.field public static final source_layout:I = 0x7f070090

.field public static final start_sync:I = 0x7f070080

.field public static final stop:I = 0x7f0700a5

.field public static final stroke:I = 0x7f070000

.field public static final suggest_word:I = 0x7f0700ac

.field public static final summary:I = 0x7f070099

.field public static final sync:I = 0x7f0700b4

.field public static final sync_progress:I = 0x7f0700ae

.field public static final sync_progress_bar:I = 0x7f070083

.field public static final thumbnail:I = 0x7f0700a8

.field public static final title:I = 0x7f070012

.field public static final title_layout:I = 0x7f070011

.field public static final try_later:I = 0x7f070036

.field public static final try_now:I = 0x7f070035

.field public static final username:I = 0x7f07009c

.field public static final view_container:I = 0x7f070013

.field public static final viewflow:I = 0x7f070033

.field public static final volume:I = 0x7f070010

.field public static final website:I = 0x7f070002

.field public static final whiteboard_view:I = 0x7f07004d

.field public static final write_done:I = 0x7f0700b3

.field public static final write_view_gl:I = 0x7f0700b2

.field public static final write_view_layout:I = 0x7f07003c

.field public static final write_view_normal:I = 0x7f0700b1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
