.class public interface abstract Lcom/youdao/note/utils/PreferenceKeys;
.super Ljava/lang/Object;
.source "PreferenceKeys.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/utils/PreferenceKeys$STAT;
    }
.end annotation


# static fields
.field public static final DOODLE_PAINT_WIDTH:Ljava/lang/String; = "doodle_paint_width"

.field public static final EVER_LOGIN:Ljava/lang/String; = "ever_login"

.field public static final HANDWRITE_PAINT_WIDTH:Ljava/lang/String; = "handwrite_paint_width"

.field public static final IGNORE_VERSION:Ljava/lang/String; = "ignore_version"

.field public static final LAST_CHECK_UPDATE:Ljava/lang/String; = "last_chcekupdate"

.field public static final LAST_LOGIN_USERNAME:Ljava/lang/String; = "last_username"

.field public static final LAST_USE_VERSION:Ljava/lang/String; = "last_use_version"

.field public static final LICENSE_ACCEPTED:Ljava/lang/String; = "license_accepted"

.field public static final PACKAGE_DOWNLOAD_URL:Ljava/lang/String; = "download_url"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final PERSIST_COOKIE:Ljava/lang/String; = "cookie"

.field public static final PIN_LOCK:Ljava/lang/String; = "pinlock"

.field public static final SERVER_PACKAGE_VERSION:Ljava/lang/String; = "server_version"

.field public static final SESSION_COOKIE:Ljava/lang/String; = "session_cookie"

.field public static final SHOW_HANDWRITE_GUIDE:Ljava/lang/String; = "show_handwrite_guide"

.field public static final SHOW_HANDWRITE_MODE_TIPS:Ljava/lang/String; = "show_handwrite_mode_tips"

.field public static final SHOW_MAIL_GUIDE:Ljava/lang/String; = "show_mail_guide"

.field public static final UPDATE_IGNORED:Ljava/lang/String; = "update_ignored"

.field public static final USERNAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/utils/PreferenceKeys;->USERNAME:Ljava/lang/String;

    return-void
.end method
