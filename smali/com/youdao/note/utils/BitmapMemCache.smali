.class public Lcom/youdao/note/utils/BitmapMemCache;
.super Ljava/lang/Object;
.source "BitmapMemCache.java"


# static fields
.field private static sInstance:Lcom/youdao/note/utils/BitmapMemCache;


# instance fields
.field private cache:Lcom/youdao/note/utils/LRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/youdao/note/utils/LRUCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/youdao/note/utils/LRUCache;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Lcom/youdao/note/utils/LRUCache;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/utils/BitmapMemCache;->cache:Lcom/youdao/note/utils/LRUCache;

    .line 40
    return-void
.end method

.method private decodeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 2
    .parameter "path"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 65
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 66
    .local v0, options:Landroid/graphics/BitmapFactory$Options;
    iput p3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 67
    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 68
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance()Lcom/youdao/note/utils/BitmapMemCache;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/youdao/note/utils/BitmapMemCache;->sInstance:Lcom/youdao/note/utils/BitmapMemCache;

    if-nez v0, :cond_1

    .line 29
    const-class v1, Lcom/youdao/note/utils/BitmapMemCache;

    monitor-enter v1

    .line 30
    :try_start_0
    sget-object v0, Lcom/youdao/note/utils/BitmapMemCache;->sInstance:Lcom/youdao/note/utils/BitmapMemCache;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/youdao/note/utils/BitmapMemCache;

    invoke-direct {v0}, Lcom/youdao/note/utils/BitmapMemCache;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/BitmapMemCache;->sInstance:Lcom/youdao/note/utils/BitmapMemCache;

    .line 33
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    :cond_1
    sget-object v0, Lcom/youdao/note/utils/BitmapMemCache;->sInstance:Lcom/youdao/note/utils/BitmapMemCache;

    return-object v0

    .line 33
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getBitmap(Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "imgUri"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/youdao/note/utils/BitmapMemCache;->getBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 4
    .parameter "path"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 47
    move-object v2, p1

    .line 48
    .local v2, key:Ljava/lang/String;
    iget-object v3, p0, Lcom/youdao/note/utils/BitmapMemCache;->cache:Lcom/youdao/note/utils/LRUCache;

    invoke-virtual {v3, v2}, Lcom/youdao/note/utils/LRUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 50
    .local v0, bReference:Ljava/lang/ref/SoftReference;,"Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 51
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/utils/BitmapMemCache;->decodeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 52
    .local v1, bitmap:Landroid/graphics/Bitmap;
    new-instance v0, Ljava/lang/ref/SoftReference;

    .end local v0           #bReference:Ljava/lang/ref/SoftReference;,"Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 53
    .restart local v0       #bReference:Ljava/lang/ref/SoftReference;,"Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    iget-object v3, p0, Lcom/youdao/note/utils/BitmapMemCache;->cache:Lcom/youdao/note/utils/LRUCache;

    invoke-virtual {v3, v2, v0}, Lcom/youdao/note/utils/LRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 57
    :goto_0
    return-object v1

    .line 55
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .restart local v1       #bitmap:Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public removeBitmap(Ljava/lang/String;)V
    .locals 1
    .parameter "path"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/youdao/note/utils/BitmapMemCache;->cache:Lcom/youdao/note/utils/LRUCache;

    invoke-virtual {v0, p1}, Lcom/youdao/note/utils/LRUCache;->remove(Ljava/lang/Object;)V

    .line 62
    return-void
.end method
