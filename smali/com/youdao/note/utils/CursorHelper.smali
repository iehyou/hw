.class public Lcom/youdao/note/utils/CursorHelper;
.super Ljava/lang/Object;
.source "CursorHelper.java"


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .parameter "cursor"

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    .line 21
    iput-object p1, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    .line 22
    return-void
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;)Z
    .locals 2
    .parameter "columnName"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 2
    .parameter "columnName"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 2
    .parameter "columnName"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "columnName"

    .prologue
    .line 25
    iget-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/youdao/note/utils/CursorHelper;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    return v0
.end method
