.class public Lcom/youdao/note/utils/LRUCache;
.super Ljava/lang/Object;
.source "LRUCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/utils/LRUCache$CacheResizeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final factor:F = 0.75f


# instance fields
.field private cacheSize:I

.field private mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/youdao/note/utils/LRUCache$CacheResizeListener",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .parameter "cacheSize"

    .prologue
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    const/high16 v3, 0x3f40

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/youdao/note/utils/LRUCache;->mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;

    .line 34
    iput p1, p0, Lcom/youdao/note/utils/LRUCache;->cacheSize:I

    .line 35
    int-to-float v1, p1

    div-float/2addr v1, v3

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v0, v1, 0x1

    .line 36
    .local v0, capacity:I
    new-instance v1, Lcom/youdao/note/utils/LRUCache$1;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v3, v2}, Lcom/youdao/note/utils/LRUCache$1;-><init>(Lcom/youdao/note/utils/LRUCache;IFZ)V

    iput-object v1, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/utils/LRUCache;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 19
    iget v0, p0, Lcom/youdao/note/utils/LRUCache;->cacheSize:I

    return v0
.end method

.method static synthetic access$100(Lcom/youdao/note/utils/LRUCache;)Lcom/youdao/note/utils/LRUCache$CacheResizeListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 19
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 59
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit p0

    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAll()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove(Ljava/lang/Object;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setCacheReizeListener(Lcom/youdao/note/utils/LRUCache$CacheResizeListener;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/utils/LRUCache$CacheResizeListener",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    .local p1, mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;,"Lcom/youdao/note/utils/LRUCache$CacheResizeListener<TK;TV;>;"
    iput-object p1, p0, Lcom/youdao/note/utils/LRUCache;->mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;

    .line 76
    return-void
.end method

.method public declared-synchronized usedEntries()I
    .locals 1

    .prologue
    .line 63
    .local p0, this:Lcom/youdao/note/utils/LRUCache;,"Lcom/youdao/note/utils/LRUCache<TK;TV;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/utils/LRUCache;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
