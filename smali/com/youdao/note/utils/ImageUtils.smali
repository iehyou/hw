.class public Lcom/youdao/note/utils/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# static fields
.field public static final HIGH_SIZE:I = 0x4b0

.field public static final LOW_SIZE:I = 0x280

.field public static final NOMAL_SIZE:I = 0x320

.field public static final QUALITY_HIGH:I = 0x3

.field public static final QUALITY_LOW:I = 0x1

.field public static final QUALITY_NORMAL:I = 0x2

.field public static final QUALITY_ORIGINAL:I = 0x4

.field private static final THUMBNAIL_SIZE:I = 0x1e0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bitmap2bytes(Landroid/graphics/Bitmap;)[B
    .locals 2
    .parameter "image"

    .prologue
    .line 111
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x4b

    invoke-static {p0, v0, v1}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B
    .locals 2
    .parameter "image"
    .parameter "format"
    .parameter "quality"

    .prologue
    .line 115
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 116
    .local v0, os:Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, p1, p2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 117
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public static bytes2bitmap([B)Landroid/graphics/Bitmap;
    .locals 5
    .parameter "bytes"

    .prologue
    const/4 v4, 0x0

    .line 121
    array-length v2, p0

    invoke-static {p0, v4, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    .local v0, immutable:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 123
    .local v1, mutable:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 124
    return-object v1
.end method

.method public static bytesPerPixel(Landroid/graphics/Bitmap$Config;)I
    .locals 1
    .parameter "config"

    .prologue
    .line 269
    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    .line 270
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne p0, v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 271
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static compressImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 11
    .parameter "bitmap"
    .parameter "maxHeight"
    .parameter "maxWidth"

    .prologue
    const/4 v1, 0x0

    .line 95
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 96
    .local v5, matrix:Landroid/graphics/Matrix;
    if-le p1, p2, :cond_2

    move v8, p1

    .line 97
    .local v8, maxLen:I
    :goto_0
    const/high16 v9, 0x3f80

    .line 98
    .local v9, scale:F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-gt v0, v8, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-le v0, v8, :cond_1

    .line 99
    :cond_0
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v7, v0, v2

    .line 100
    .local v7, heightScale:F
    int-to-float v0, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v10, v0, v2

    .line 101
    .local v10, widthScale:F
    cmpg-float v0, v7, v10

    if-gez v0, :cond_3

    move v9, v7

    .line 102
    :goto_1
    invoke-virtual {v5, v9, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 103
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 106
    .end local v7           #heightScale:F
    .end local v10           #widthScale:F
    .end local p0
    :cond_1
    return-object p0

    .end local v8           #maxLen:I
    .end local v9           #scale:F
    .restart local p0
    :cond_2
    move v8, p2

    .line 96
    goto :goto_0

    .restart local v7       #heightScale:F
    .restart local v8       #maxLen:I
    .restart local v9       #scale:F
    .restart local v10       #widthScale:F
    :cond_3
    move v9, v10

    .line 101
    goto :goto_1
.end method

.method public static compressImage([BII)Landroid/graphics/Bitmap;
    .locals 2
    .parameter "bytes"
    .parameter "maxHeight"
    .parameter "maxWidth"

    .prologue
    .line 68
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/youdao/note/utils/ImageUtils;->compressImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static cropImage(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 14
    .parameter "bitmap"
    .parameter "size"

    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 75
    .local v9, height:I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    .line 76
    .local v11, width:I
    if-ge v9, v11, :cond_0

    move v3, v9

    .line 77
    .local v3, minSize:I
    :goto_0
    div-int/lit8 v7, v9, 0x2

    .line 78
    .local v7, centerH:I
    div-int/lit8 v8, v11, 0x2

    .line 79
    .local v8, centerW:I
    int-to-double v0, p1

    const-wide/high16 v12, 0x3ff0

    mul-double/2addr v0, v12

    int-to-double v12, v3

    div-double/2addr v0, v12

    double-to-float v10, v0

    .line 80
    .local v10, scale:F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 81
    .local v5, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v5, v10, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 82
    const-string v0, "ImageUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v3, 0x2

    sub-int v2, v8, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v2, v3, 0x2

    sub-int v2, v7, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    div-int/lit8 v0, v3, 0x2

    sub-int v1, v8, v0

    div-int/lit8 v0, v3, 0x2

    sub-int v2, v7, v0

    const/4 v6, 0x1

    move-object v0, p0

    move v4, v3

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .end local v3           #minSize:I
    .end local v5           #matrix:Landroid/graphics/Matrix;
    .end local v7           #centerH:I
    .end local v8           #centerW:I
    .end local v10           #scale:F
    :cond_0
    move v3, v11

    .line 76
    goto :goto_0
.end method

.method public static getBitmapOption(Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 256
    const-string v1, "r"

    invoke-static {p0, v1}, Lcom/youdao/note/utils/FileUtils;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 257
    .local v0, fd:Ljava/io/FileDescriptor;
    invoke-static {v0}, Lcom/youdao/note/utils/ImageUtils;->getBitmapOption(Ljava/io/FileDescriptor;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    return-object v1
.end method

.method public static getBitmapOption(Ljava/io/FileDescriptor;)Landroid/graphics/BitmapFactory$Options;
    .locals 2
    .parameter "fd"

    .prologue
    .line 249
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 250
    .local v0, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 251
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 252
    return-object v0
.end method

.method public static getBitmapOption(Ljava/lang/String;)Landroid/graphics/BitmapFactory$Options;
    .locals 3
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 261
    const-string v2, "r"

    invoke-static {p0, v2}, Lcom/youdao/note/utils/FileUtils;->openFileDescriptor(Ljava/lang/String;Ljava/lang/String;)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 262
    .local v0, fd:Ljava/io/FileDescriptor;
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 263
    .local v1, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 264
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 265
    return-object v1
.end method

.method public static getImageDimensions(Ljava/lang/String;)Landroid/graphics/Rect;
    .locals 7
    .parameter "filePath"

    .prologue
    .line 54
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 55
    .local v3, localOptions:Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 57
    :try_start_0
    invoke-static {p0, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 58
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 59
    .local v0, j:I
    iget v1, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 60
    .local v1, k:I
    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v0           #j:I
    .end local v1           #k:I
    :goto_0
    return-object v4

    .line 62
    :catch_0
    move-exception v2

    .line 63
    .local v2, localException:Ljava/lang/Exception;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V
    .locals 0
    .parameter
    .parameter "img"
    .parameter "quality"
    .parameter "needCrop"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<TT;>;",
            "Landroid/graphics/Bitmap;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    invoke-static {p0, p1, p3}, Lcom/youdao/note/utils/ImageUtils;->saveThumbnail(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;Z)V

    .line 145
    invoke-static {p0, p1, p2}, Lcom/youdao/note/utils/ImageUtils;->saveImageResource(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;I)V

    .line 146
    return-void
.end method

.method public static persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/net/Uri;IZ)V
    .locals 4
    .parameter
    .parameter "imageUri"
    .parameter "quality"
    .parameter "needCrop"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<TT;>;",
            "Landroid/net/Uri;",
            "IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 152
    .local v1, img:Landroid/graphics/Bitmap;
    invoke-static {p0, v1, p3}, Lcom/youdao/note/utils/ImageUtils;->saveThumbnail(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;Z)V

    .line 153
    const/4 v3, 0x4

    if-ne p2, v3, :cond_0

    .line 154
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    .line 156
    .local v0, dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/AbstractImageResource;

    .line 158
    .local v2, tmpResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/youdao/note/utils/FileUtils;->copyFile(Landroid/net/Uri;Ljava/lang/String;)V

    .line 159
    invoke-static {v2, p1}, Lcom/youdao/note/utils/ImageUtils;->setResourceSize(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/net/Uri;)V

    .line 160
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/youdao/note/data/resource/AbstractImageResource;->setMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 161
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->releaseData()V

    .line 165
    .end local v0           #dataSource:Lcom/youdao/note/datasource/DataSource;
    .end local v2           #tmpResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    :goto_0
    return-void

    .line 163
    :cond_0
    invoke-static {p0, v1, p2}, Lcom/youdao/note/utils/ImageUtils;->saveImageResource(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method private static resizeImg(Landroid/graphics/Bitmap;Lcom/youdao/note/data/resource/AbstractImageResource;ILcom/youdao/note/data/resource/AbstractImageResource;)V
    .locals 4
    .parameter "img"
    .parameter
    .parameter "size"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<+",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">;I",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<+",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, tmpResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/AbstractImageResourceMeta;>;"
    .local p3, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/AbstractImageResourceMeta;>;"
    invoke-static {p0, p2, p2}, Lcom/youdao/note/utils/ImageUtils;->compressImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 225
    .local v0, tmpBitmap:Landroid/graphics/Bitmap;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/youdao/note/data/resource/AbstractImageResource;->setContentBytes([B)V

    .line 226
    invoke-static {p3, v0}, Lcom/youdao/note/utils/ImageUtils;->setResourceSize(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;)V

    .line 227
    invoke-virtual {p3}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/AbstractImageResource;->getLength()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setLength(J)V

    .line 228
    return-void
.end method

.method private static saveImageResource(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;I)V
    .locals 5
    .parameter
    .parameter "img"
    .parameter "quality"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<TT;>;",
            "Landroid/graphics/Bitmap;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    .line 193
    .local v0, dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/AbstractImageResource;

    .line 198
    .local v1, tmpResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<TT;>;"
    packed-switch p2, :pswitch_data_0

    .line 210
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-static {p1, v2, v3}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/resource/AbstractImageResource;->setContentBytes([B)V

    .line 211
    invoke-static {p0, p1}, Lcom/youdao/note/utils/ImageUtils;->setResourceSize(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;)V

    .line 212
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AbstractImageResource;->getLength()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setLength(J)V

    .line 215
    :goto_0
    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->updateResourceCache(Lcom/youdao/note/data/resource/AbstractResource;)Z

    .line 216
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/youdao/note/data/resource/AbstractImageResource;->setMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 217
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AbstractImageResource;->releaseData()V

    .line 218
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->releaseData()V

    .line 219
    return-void

    .line 200
    :pswitch_0
    const/16 v2, 0x4b0

    invoke-static {p1, v1, v2, p0}, Lcom/youdao/note/utils/ImageUtils;->resizeImg(Landroid/graphics/Bitmap;Lcom/youdao/note/data/resource/AbstractImageResource;ILcom/youdao/note/data/resource/AbstractImageResource;)V

    goto :goto_0

    .line 203
    :pswitch_1
    const/16 v2, 0x320

    invoke-static {p1, v1, v2, p0}, Lcom/youdao/note/utils/ImageUtils;->resizeImg(Landroid/graphics/Bitmap;Lcom/youdao/note/data/resource/AbstractImageResource;ILcom/youdao/note/data/resource/AbstractImageResource;)V

    goto :goto_0

    .line 206
    :pswitch_2
    const/16 v2, 0x280

    invoke-static {p1, v1, v2, p0}, Lcom/youdao/note/utils/ImageUtils;->resizeImg(Landroid/graphics/Bitmap;Lcom/youdao/note/data/resource/AbstractImageResource;ILcom/youdao/note/data/resource/AbstractImageResource;)V

    goto :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static saveThumbnail(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;Z)V
    .locals 4
    .parameter
    .parameter "img"
    .parameter "needCrop"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<+",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/AbstractImageResourceMeta;>;"
    const/16 v3, 0x1e0

    .line 170
    new-instance v0, Lcom/youdao/note/data/Thumbnail;

    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-direct {v0, v2}, Lcom/youdao/note/data/Thumbnail;-><init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V

    .line 174
    .local v0, thumbnail:Lcom/youdao/note/data/Thumbnail;
    if-eqz p2, :cond_0

    .line 175
    invoke-static {p1, v3, v3}, Lcom/youdao/note/utils/ImageUtils;->compressImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 177
    .local v1, tmpBitmap:Landroid/graphics/Bitmap;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-static {v1, v2, v3}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/Thumbnail;->setContentBytes([B)V

    .line 183
    .end local v1           #tmpBitmap:Landroid/graphics/Bitmap;
    :goto_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/youdao/note/datasource/DataSource;->writeThumbnail(Lcom/youdao/note/data/Thumbnail;)Z

    .line 186
    return-void

    .line 181
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getContentBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/youdao/note/data/Thumbnail;->setContentBytes([B)V

    goto :goto_0
.end method

.method private static setResourceSize(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;)V
    .locals 2
    .parameter
    .parameter "tmpBitmap"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<+",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 231
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/AbstractImageResourceMeta;>;"
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setHeight(I)V

    .line 232
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setWidth(I)V

    .line 233
    return-void
.end method

.method private static setResourceSize(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/net/Uri;)V
    .locals 7
    .parameter
    .parameter "imageUri"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractImageResource",
            "<+",
            "Lcom/youdao/note/data/resource/AbstractImageResourceMeta;",
            ">;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 237
    .local p0, imageResource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/AbstractImageResourceMeta;>;"
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 238
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const-string v4, "r"

    invoke-virtual {v0, p1, v4}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 239
    .local v1, descriptor:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    .line 240
    .local v2, fd:Ljava/io/FileDescriptor;
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 241
    .local v3, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 242
    const/4 v4, 0x0

    invoke-static {v2, v4, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 243
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v4, v5}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setHeight(I)V

    .line 244
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v4, v5}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setWidth(I)V

    .line 245
    invoke-virtual {p0}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->setLength(J)V

    .line 246
    return-void
.end method
