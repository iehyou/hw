.class public interface abstract Lcom/youdao/note/utils/Consts$Request;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Request"
.end annotation


# static fields
.field public static final COOKIE_FILED:Ljava/lang/String; = "cookie"

.field public static final COOKIE_PREFIX:Ljava/lang/String; = "cookie="

.field public static final NTES_PASSPORT:Ljava/lang/String; = "NTES_PASSPORT"

.field public static final NTES_SESSION:Ljava/lang/String; = "NTES_SESS"

.field public static final SESSION_COOKIE_FILED:Ljava/lang/String; = "X-Set-NTES-SESS"
