.class public interface abstract Lcom/youdao/note/utils/Consts;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/utils/Consts$DATA_NAME;,
        Lcom/youdao/note/utils/Consts$APIS;,
        Lcom/youdao/note/utils/Consts$Request;,
        Lcom/youdao/note/utils/Consts$HTML;,
        Lcom/youdao/note/utils/Consts$DATA_TYPE;
    }
.end annotation


# static fields
.field public static final ARROW_ASSET:Ljava/lang/String; = "file:///android_asset/arrow.png"

.field public static final ASSET_PREFIX:Ljava/lang/String; = "file:///android_asset/"

.field public static final CMWAP_HOST:Ljava/lang/String; = "10.0.0.172"

.field public static final DEBUG_MAX_RESOURCE_SIZE:J = 0x19000L

.field public static final DOWNLOAD_ASSET:Ljava/lang/String; = "file:///android_asset/download.png"

.field public static final JUHUA_ASSET:Ljava/lang/String; = "file:///android_asset/juhua.gif"

.field public static final MAX_RESOURCE_SIZE:J = 0x1800000L

.field public static final ROOT_NOTEBOOK:Ljava/lang/String; = "/"

.field public static final SUMMARY_PREFIX:Ljava/lang/String; = "                       "
