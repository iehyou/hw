.class public interface abstract Lcom/youdao/note/utils/Consts$DATA_NAME;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DATA_NAME"
.end annotation


# static fields
.field public static final AUTHOR:Ljava/lang/String; = "au"

.field public static final CREATE_TIME:Ljava/lang/String; = "ct"

.field public static final DEFAULT_NOTE_BOOK:Ljava/lang/String; = "df"

.field public static final ENTRY_PATH:Ljava/lang/String; = "p"

.field public static final EXT_PROP:Ljava/lang/String; = "pp"

.field public static final IS_DEL:Ljava/lang/String; = "del"

.field public static final IS_DIR:Ljava/lang/String; = "dr"

.field public static final LAST_LOGIN_TIME:Ljava/lang/String; = "ll"

.field public static final LAST_PUSH_TIME:Ljava/lang/String; = "lp"

.field public static final LENGTH:Ljava/lang/String; = "sz"

.field public static final MODIFY_TIME:Ljava/lang/String; = "mt"

.field public static final NOTE_NUMBER:Ljava/lang/String; = "nn"

.field public static final QUTA_SPACE:Ljava/lang/String; = "q"

.field public static final SOURCE_URL:Ljava/lang/String; = "su"

.field public static final TAGS:Ljava/lang/String; = "tg"

.field public static final TITLE:Ljava/lang/String; = "tl"

.field public static final USED_SPACE:Ljava/lang/String; = "u"

.field public static final VERSION:Ljava/lang/String; = "v"
