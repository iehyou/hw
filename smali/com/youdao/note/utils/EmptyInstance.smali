.class public interface abstract Lcom/youdao/note/utils/EmptyInstance;
.super Ljava/lang/Object;
.source "EmptyInstance.java"


# static fields
.field public static final EMPTY_BYTES:[B = null

.field public static final EMPTY_DATA:Lcom/youdao/note/data/BaseData; = null

.field public static final EMPTY_NOTEMETAS:[Lcom/youdao/note/data/NoteMeta; = null

.field public static final EMPTY_OBJECT:Ljava/lang/Object; = null

.field public static final EMPTY_STRING:Ljava/lang/String; = ""

.field public static final EMPTY_STRINGS:[Ljava/lang/String; = null

.field public static final EMPTY_STRINGS2:[Ljava/lang/String; = null

.field public static final EMPTY_TITLE:Ljava/lang/String; = "\u65e0\u4e3b\u9898"

.field public static final EMPTY_VOIDS:[Ljava/lang/Void;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19
    new-array v0, v3, [B

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_BYTES:[B

    .line 23
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_STRINGS:[Ljava/lang/String;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_STRINGS2:[Ljava/lang/String;

    .line 27
    new-array v0, v3, [Ljava/lang/Void;

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_VOIDS:[Ljava/lang/Void;

    .line 31
    new-array v0, v3, [Lcom/youdao/note/data/NoteMeta;

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_NOTEMETAS:[Lcom/youdao/note/data/NoteMeta;

    .line 33
    new-instance v0, Lcom/youdao/note/data/BaseData;

    invoke-direct {v0}, Lcom/youdao/note/data/BaseData;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_DATA:Lcom/youdao/note/data/BaseData;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_OBJECT:Ljava/lang/Object;

    return-void
.end method
