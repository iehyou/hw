.class public Lcom/youdao/note/utils/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts;


# static fields
.field private static final DEFAULT_FILE_ICON:Ljava/lang/String; = "else.png"

.field private static audioSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static compressSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static drawableMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static flashSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static iconMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static imageSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static pageSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPlayAble:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static videoSuffix:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f020050

    const v6, 0x7f0200c8

    const v5, 0x7f020019

    const v4, 0x7f02000b

    const v3, 0x7f0200fc

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    .line 58
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "jpg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "jpeg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "bmp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "gif"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "psd"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "ia"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    const-string v1, "png"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    const-string v1, "mp3"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    const-string v1, "amr"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    const-string v1, "3gpp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    const-string v1, "flac"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mp3"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "aac"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "ogg"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "wav"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "amr"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "3gpp"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "flac"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "m4a"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "ape"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    .line 84
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "mp3"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "aac"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "ogg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "wav"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "flac"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "m4a"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "ape"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "amr"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    .line 94
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "rm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "rmvb"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "avi"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "mkv"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "mpg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "mpeg"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "wmv"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "ts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "m4v"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "mp4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "wma"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "3gp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "rm"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "rmvb"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "avi"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mkv"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mpg"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mpeg"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "wmv"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "ts"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "m4v"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mp4"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "wma"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "3gp"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->flashSuffix:Ljava/util/Set;

    .line 122
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->flashSuffix:Ljava/util/Set;

    const-string v1, "swf"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->flashSuffix:Ljava/util/Set;

    const-string v1, "flv"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "swf"

    const v2, 0x7f020052

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "flv"

    const v2, 0x7f020052

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    .line 129
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    const-string v1, "zip"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    const-string v1, "rar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    const-string v1, "7zp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "zip"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "rar"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "7zp"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->pageSuffix:Ljava/util/Set;

    .line 138
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->pageSuffix:Ljava/util/Set;

    const-string v1, "html"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->pageSuffix:Ljava/util/Set;

    const-string v1, "htm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->pageSuffix:Ljava/util/Set;

    const-string v1, "mht"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "html"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "htm"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "mht"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    .line 147
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "pdf"

    const-string v2, "pdf64.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "doc"

    const-string v2, "word.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "docx"

    const-string v2, "word.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "ppt"

    const-string v2, "powerpoint.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "pptx"

    const-string v2, "powerpoint.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "txt"

    const-string v2, "txt.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "xls"

    const-string v2, "excel.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    const-string v1, "xlsx"

    const-string v2, "excel.png"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "pdf"

    const v2, 0x7f0200be

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "doc"

    const v2, 0x7f02010b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "docx"

    const v2, 0x7f02010b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "ppt"

    const v2, 0x7f0200c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "pptx"

    const v2, 0x7f0200c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "txt"

    const v2, 0x7f0200f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "xls"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    const-string v1, "xlsx"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    const-string v1, "audio.png"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->addIcons(Ljava/util/Set;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    const-string v1, "video.png"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->addIcons(Ljava/util/Set;Ljava/lang/String;)V

    .line 167
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->flashSuffix:Ljava/util/Set;

    const-string v1, "swf.png"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->addIcons(Ljava/util/Set;Ljava/lang/String;)V

    .line 168
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    const-string v1, "rar.png"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->addIcons(Ljava/util/Set;Ljava/lang/String;)V

    .line 169
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->pageSuffix:Ljava/util/Set;

    const-string v1, "html.png"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/FileUtils;->addIcons(Ljava/util/Set;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addIcons(Ljava/util/Set;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter "icon"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 173
    .local p0, suffixSet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 174
    .local v1, suffix:Ljava/lang/String;
    sget-object v2, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    invoke-interface {v2, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 176
    .end local v1           #suffix:Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static copyFile(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 14
    .parameter "srcUri"
    .parameter "dstPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 388
    const-class v12, Lcom/youdao/note/utils/FileUtils;

    const-string v13, "try to copy same file."

    invoke-static {v12, v13}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 392
    .local v1, contentResolver:Landroid/content/ContentResolver;
    const-string v12, "r"

    invoke-virtual {v1, p0, v12}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 393
    .local v2, descriptor:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    .line 394
    .local v4, fd:Ljava/io/FileDescriptor;
    const/4 v5, 0x0

    .line 395
    .local v5, fis:Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 396
    .local v7, fos:Ljava/io/FileOutputStream;
    const/4 v9, 0x0

    .line 397
    .local v9, inChannel:Ljava/nio/channels/FileChannel;
    const/4 v10, 0x0

    .line 399
    .local v10, outChannel:Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 400
    .end local v5           #fis:Ljava/io/FileInputStream;
    .local v6, fis:Ljava/io/FileInputStream;
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 401
    .local v3, dstFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_2

    .line 402
    invoke-static {v3}, Lcom/youdao/note/utils/FileUtils;->createNewFile(Ljava/io/File;)Z

    .line 404
    :cond_2
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 405
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .local v8, fos:Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 406
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v10

    .line 407
    const/16 v12, 0x400

    invoke-static {v12}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 409
    .local v0, buffer:Ljava/nio/ByteBuffer;
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 410
    invoke-virtual {v9, v0}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v11

    .line 411
    .local v11, r:I
    const/4 v12, -0x1

    if-ne v11, v12, :cond_6

    .line 419
    if-eqz v9, :cond_3

    :try_start_3
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 423
    :cond_3
    :goto_2
    if-eqz v10, :cond_4

    :try_start_4
    invoke-virtual {v10}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 427
    :cond_4
    :goto_3
    if-eqz v6, :cond_5

    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 431
    :cond_5
    :goto_4
    if-eqz v8, :cond_0

    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    .line 432
    :catch_0
    move-exception v12

    goto :goto_0

    .line 414
    :cond_6
    :try_start_7
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 415
    invoke-virtual {v10, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 418
    .end local v0           #buffer:Ljava/nio/ByteBuffer;
    .end local v11           #r:I
    :catchall_0
    move-exception v12

    move-object v7, v8

    .end local v8           #fos:Ljava/io/FileOutputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 419
    .end local v3           #dstFile:Ljava/io/File;
    .end local v6           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :goto_5
    if-eqz v9, :cond_7

    :try_start_8
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 423
    :cond_7
    :goto_6
    if-eqz v10, :cond_8

    :try_start_9
    invoke-virtual {v10}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 427
    :cond_8
    :goto_7
    if-eqz v5, :cond_9

    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 431
    :cond_9
    :goto_8
    if-eqz v7, :cond_a

    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 433
    :cond_a
    :goto_9
    throw v12

    .line 420
    .end local v5           #fis:Ljava/io/FileInputStream;
    .end local v7           #fos:Ljava/io/FileOutputStream;
    .restart local v0       #buffer:Ljava/nio/ByteBuffer;
    .restart local v3       #dstFile:Ljava/io/File;
    .restart local v6       #fis:Ljava/io/FileInputStream;
    .restart local v8       #fos:Ljava/io/FileOutputStream;
    .restart local v11       #r:I
    :catch_1
    move-exception v12

    goto :goto_2

    .line 424
    :catch_2
    move-exception v12

    goto :goto_3

    .line 428
    :catch_3
    move-exception v12

    goto :goto_4

    .line 420
    .end local v0           #buffer:Ljava/nio/ByteBuffer;
    .end local v3           #dstFile:Ljava/io/File;
    .end local v6           #fis:Ljava/io/FileInputStream;
    .end local v8           #fos:Ljava/io/FileOutputStream;
    .end local v11           #r:I
    .restart local v5       #fis:Ljava/io/FileInputStream;
    .restart local v7       #fos:Ljava/io/FileOutputStream;
    :catch_4
    move-exception v13

    goto :goto_6

    .line 424
    :catch_5
    move-exception v13

    goto :goto_7

    .line 428
    :catch_6
    move-exception v13

    goto :goto_8

    .line 432
    :catch_7
    move-exception v13

    goto :goto_9

    .line 418
    :catchall_1
    move-exception v12

    goto :goto_5

    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v6       #fis:Ljava/io/FileInputStream;
    :catchall_2
    move-exception v12

    move-object v5, v6

    .end local v6           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    goto :goto_5
.end method

.method public static createNewFile(Ljava/io/File;)Z
    .locals 2
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 455
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 456
    .local v0, parentFile:Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 457
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 458
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 460
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->createNewFile()Z

    move-result v1

    return v1
.end method

.method public static deleteDirectory(Ljava/io/File;)Z
    .locals 6
    .parameter "dir"

    .prologue
    .line 348
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 349
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 350
    .local v2, files:[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 351
    move-object v0, v2

    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 352
    .local v1, file:Ljava/io/File;
    invoke-static {v1}, Lcom/youdao/note/utils/FileUtils;->deleteDirectory(Ljava/io/File;)Z

    .line 351
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 356
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #files:[Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_0
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->deleteFile(Ljava/io/File;)Z

    move-result v5

    return v5
.end method

.method public static deleteDirectory(Ljava/lang/String;)Z
    .locals 1
    .parameter "dirPath"

    .prologue
    .line 344
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->deleteDirectory(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public static deleteFile(Ljava/io/File;)Z
    .locals 1
    .parameter "file"

    .prologue
    .line 337
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static deleteFile(Ljava/lang/String;)Z
    .locals 4
    .parameter "filePath"

    .prologue
    .line 324
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 325
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 326
    .local v1, parentFile:Ljava/io/File;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->deleteFile(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 327
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 328
    .local v2, subFiles:[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_0

    .line 329
    invoke-static {v1}, Lcom/youdao/note/utils/FileUtils;->deleteFile(Ljava/io/File;)Z

    move-result v3

    .line 333
    .end local v2           #subFiles:[Ljava/lang/String;
    :goto_0
    return v3

    .line 331
    .restart local v2       #subFiles:[Ljava/lang/String;
    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 333
    .end local v2           #subFiles:[Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static exist(Landroid/net/Uri;)Z
    .locals 3
    .parameter "uri"

    .prologue
    .line 469
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 471
    .local v0, contentResolver:Landroid/content/ContentResolver;
    :try_start_0
    const-string v2, "r"

    invoke-virtual {v0, p0, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 472
    const/4 v2, 0x1

    .line 474
    :goto_0
    return v2

    .line 473
    :catch_0
    move-exception v1

    .line 474
    .local v1, e:Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static exist(Ljava/lang/String;)Z
    .locals 2
    .parameter "filePath"

    .prologue
    .line 464
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 465
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static getArrowIconPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    const-string v0, "file:///android_asset/arrow.png"

    return-object v0
.end method

.method public static declared-synchronized getBitmapFromFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 10
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 496
    const-class v5, Lcom/youdao/note/utils/FileUtils;

    monitor-enter v5

    const/16 v2, 0x640

    .line 497
    .local v2, maxSize:I
    :try_start_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getImageQuality()I

    move-result v1

    .line 498
    .local v1, imgQuarity:I
    packed-switch v1, :pswitch_data_0

    .line 511
    :goto_0
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 512
    .local v3, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 513
    const/4 v4, 0x0

    invoke-static {p0, v4, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 514
    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v4, v2

    int-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/2addr v4, v2

    int-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    double-to-int v4, v6

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 517
    const/4 v4, 0x0

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 518
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v4, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 519
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 520
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 521
    const/4 v4, 0x0

    invoke-static {p0, v4, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 522
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 523
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #imgQuarity:I
    .end local v3           #options:Landroid/graphics/BitmapFactory$Options;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 500
    .restart local v1       #imgQuarity:I
    :pswitch_0
    const/16 v2, 0x4b0

    .line 501
    goto :goto_0

    .line 503
    :pswitch_1
    const/16 v2, 0x320

    .line 504
    goto :goto_0

    .line 506
    :pswitch_2
    const/16 v2, 0x280

    .line 507
    goto :goto_0

    .line 525
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    .restart local v3       #options:Landroid/graphics/BitmapFactory$Options;
    :cond_0
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 526
    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gt v4, v2, :cond_1

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-le v4, v2, :cond_2

    .line 527
    :cond_1
    invoke-static {v0, v2, v2}, Lcom/youdao/note/utils/ImageUtils;->compressImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 528
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    :cond_2
    monitor-exit v5

    return-object v0

    .line 498
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 484
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 485
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const-string v3, "r"

    invoke-virtual {v0, p0, v3}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 486
    .local v1, descriptor:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    .line 487
    .local v2, fd:Ljava/io/FileDescriptor;
    invoke-static {v2}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v3

    return-object v3
.end method

.method public static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "name"

    .prologue
    .line 534
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 535
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 537
    .local v0, i:I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 538
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 541
    .end local v0           #i:I
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .parameter "uri"

    .prologue
    .line 244
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 245
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 246
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_display_name"

    aput-object v3, v2, v1

    .line 247
    .local v2, proj:[Ljava/lang/String;
    const/4 v7, 0x0

    .line 249
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 250
    const-string v1, "_display_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 251
    .local v6, column_index:I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 257
    if-eqz v7, :cond_0

    .line 258
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 262
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    .end local v2           #proj:[Ljava/lang/String;
    .end local v6           #column_index:I
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object v1

    .line 254
    .restart local v0       #contentResolver:Landroid/content/ContentResolver;
    .restart local v2       #proj:[Ljava/lang/String;
    .restart local v6       #column_index:I
    .restart local v7       #cursor:Landroid/database/Cursor;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 257
    if-eqz v7, :cond_0

    .line 258
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 257
    .end local v6           #column_index:I
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 258
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .line 262
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    .end local v2           #proj:[Ljava/lang/String;
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_3
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFileNameFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "url"

    .prologue
    .line 267
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string v1, ""

    .line 275
    :goto_0
    return-object v1

    .line 270
    :cond_0
    invoke-static {p0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 271
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 272
    .local v0, slashPos:I
    if-ltz v0, :cond_1

    .line 273
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 275
    :cond_1
    const-string v1, ""

    goto :goto_0
.end method

.method public static getFileSize(Landroid/net/Uri;)J
    .locals 9
    .parameter "uri"

    .prologue
    .line 223
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 224
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 225
    .local v0, contentResolver:Landroid/content/ContentResolver;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_size"

    aput-object v3, v2, v1

    .line 226
    .local v2, proj:[Ljava/lang/String;
    const/4 v7, 0x0

    .line 228
    .local v7, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 229
    const-string v1, "_size"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 230
    .local v6, column_index:I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 231
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v3

    .line 234
    if-eqz v7, :cond_0

    .line 235
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 240
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    .end local v2           #proj:[Ljava/lang/String;
    .end local v6           #column_index:I
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-wide v3

    .line 234
    .restart local v0       #contentResolver:Landroid/content/ContentResolver;
    .restart local v2       #proj:[Ljava/lang/String;
    .restart local v6       #column_index:I
    .restart local v7       #cursor:Landroid/database/Cursor;
    :cond_1
    if-eqz v7, :cond_2

    .line 235
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 239
    .end local v0           #contentResolver:Landroid/content/ContentResolver;
    .end local v2           #proj:[Ljava/lang/String;
    .end local v6           #column_index:I
    .end local v7           #cursor:Landroid/database/Cursor;
    :cond_2
    new-instance v8, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    .local v8, file:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v3

    goto :goto_0

    .line 234
    .end local v8           #file:Ljava/io/File;
    .restart local v0       #contentResolver:Landroid/content/ContentResolver;
    .restart local v2       #proj:[Ljava/lang/String;
    .restart local v7       #cursor:Landroid/database/Cursor;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    .line 235
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static getIconResouceId(Ljava/lang/String;)I
    .locals 3
    .parameter "fileName"

    .prologue
    .line 193
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, suffix:Ljava/lang/String;
    sget-object v2, Lcom/youdao/note/utils/FileUtils;->drawableMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 195
    .local v0, ret:Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 198
    :goto_0
    return v2

    :cond_0
    const v2, 0x7f0200f9

    goto :goto_0
.end method

.method public static getResourceIconName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "fileName"

    .prologue
    .line 212
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    const-string v1, "else.png"

    .line 219
    :goto_0
    return-object v1

    .line 215
    :cond_0
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, suffix:Ljava/lang/String;
    sget-object v1, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    sget-object v1, Lcom/youdao/note/utils/FileUtils;->iconMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 219
    :cond_1
    const-string v1, "else.png"

    goto :goto_0
.end method

.method public static getResourceIconPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "fileName"

    .prologue
    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file:///android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getResourceIconName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "fileName"

    .prologue
    .line 203
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 204
    .local v0, dotIdx:I
    if-lez v0, :cond_0

    .line 205
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 207
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static isAudio(Landroid/net/Uri;)Z
    .locals 1
    .parameter "uri"

    .prologue
    .line 305
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isAudio(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isAudio(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 301
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->audioSuffix:Ljava/util/Set;

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isCompressFile(Landroid/net/Uri;)Z
    .locals 1
    .parameter "uri"

    .prologue
    .line 313
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isCompressFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isCompressFile(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 309
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->compressSuffix:Ljava/util/Set;

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isImage(Landroid/net/Uri;)Z
    .locals 1
    .parameter "uri"

    .prologue
    .line 297
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isImage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isImage(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 293
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->imageSuffix:Ljava/util/Set;

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isPlayable(Landroid/net/Uri;)Z
    .locals 1
    .parameter "uri"

    .prologue
    .line 289
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isPlayable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isPlayable(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 285
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->sPlayAble:Ljava/util/Set;

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isVideoFile(Landroid/net/Uri;)Z
    .locals 1
    .parameter "uri"

    .prologue
    .line 320
    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isVideoFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isVideoFile(Ljava/lang/String;)Z
    .locals 2
    .parameter "url"

    .prologue
    .line 317
    sget-object v0, Lcom/youdao/note/utils/FileUtils;->videoSuffix:Ljava/util/Set;

    invoke-static {p0}, Lcom/youdao/note/utils/FileUtils;->getSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/FileDescriptor;
    .locals 3
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 545
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 546
    .local v0, contentResolver:Landroid/content/ContentResolver;
    invoke-virtual {v0, p0, p1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 547
    .local v1, descriptor:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    return-object v2
.end method

.method public static openFileDescriptor(Ljava/lang/String;Ljava/lang/String;)Ljava/io/FileDescriptor;
    .locals 1
    .parameter "filePath"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 550
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/youdao/note/utils/FileUtils;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/FileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static readFromFileAsBytes(Ljava/lang/String;)[B
    .locals 4
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v1, file:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 379
    const/4 v0, 0x0

    .line 383
    :goto_0
    return-object v0

    .line 381
    :cond_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 382
    .local v2, is:Ljava/io/FileInputStream;
    invoke-static {v2}, Lcom/youdao/note/utils/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 383
    .local v0, bytes:[B
    goto :goto_0
.end method

.method public static readFromFileAsStr(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 368
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 369
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 370
    const/4 v2, 0x0

    .line 373
    :goto_0
    return-object v2

    .line 372
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 373
    .local v1, is:Ljava/io/FileInputStream;
    invoke-static {v1}, Lcom/youdao/note/utils/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static saveToFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "filePath"
    .parameter "content"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 439
    .local v0, bytes:[B
    invoke-static {p0, v0}, Lcom/youdao/note/utils/FileUtils;->saveToFile(Ljava/lang/String;[B)V

    .line 440
    return-void
.end method

.method public static saveToFile(Ljava/lang/String;[B)V
    .locals 3
    .parameter "filePath"
    .parameter "bytes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 443
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 444
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 445
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 447
    :cond_0
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->createNewFile(Ljava/io/File;)Z

    .line 448
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 449
    .local v1, os:Ljava/io/FileOutputStream;
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 450
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 451
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 452
    return-void
.end method
