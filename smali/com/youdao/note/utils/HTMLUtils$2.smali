.class final Lcom/youdao/note/utils/HTMLUtils$2;
.super Ljava/lang/Object;
.source "HTMLUtils.java"

# interfaces
.implements Lorg/htmlcleaner/TagNodeVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/utils/HTMLUtils;->convertToServerHtml(Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dataSource:Lcom/youdao/note/datasource/DataSource;


# direct methods
.method constructor <init>(Lcom/youdao/note/datasource/DataSource;)V
    .locals 0
    .parameter

    .prologue
    .line 328
    iput-object p1, p0, Lcom/youdao/note/utils/HTMLUtils$2;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private recoverSrc(Lorg/htmlcleaner/TagNode;)V
    .locals 10
    .parameter "tagNode"

    .prologue
    .line 383
    const-string v7, "bak-src"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384
    .local v1, bakSrc:Ljava/lang/String;
    const-string v7, "bak-style"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 385
    .local v2, bakStyle:Ljava/lang/String;
    const-string v7, "style"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 386
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 387
    const-string v7, "style"

    invoke-virtual {p1, v7, v2}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_0
    const-string v7, "bak-style"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 391
    const-string v7, "bak-width"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 392
    .local v3, bakWidth:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 393
    const-string v7, "width"

    invoke-virtual {p1, v7, v3}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_1
    const-string v7, "bak-width"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 397
    const-string v7, "bak-height"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, bakHeight:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 399
    const-string v7, "height"

    invoke-virtual {p1, v7, v0}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_2
    const-string v7, "bak-height"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 403
    invoke-static {}, Lcom/youdao/note/utils/HTMLUtils;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bakSrc is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 405
    const-string v7, "src"

    invoke-virtual {p1, v7, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string v7, "bak-src"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 438
    :goto_0
    const-string v7, "id"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 439
    return-void

    .line 411
    :cond_3
    const-string v7, "id"

    invoke-virtual {p1, v7}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, resourceId:Ljava/lang/String;
    iget-object v7, p0, Lcom/youdao/note/utils/HTMLUtils$2;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v6}, Lcom/youdao/note/datasource/DataSource;->getResourceMeta(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    .line 413
    .local v4, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    if-eqz v4, :cond_5

    .line 414
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .local v5, pathBuilder:Ljava/lang/StringBuilder;
    const-string v7, "http://"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/youdao/note/YNoteApplication;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "yws/res/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getVersion()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    instance-of v7, v4, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    if-eqz v7, :cond_4

    .line 424
    const-string v7, "src"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    :cond_4
    const-string v7, "path"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v7, "src"

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getSrc()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const-string v7, "filename"

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v7, "filelength"

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 435
    .end local v5           #pathBuilder:Ljava/lang/StringBuilder;
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to find resource "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private resetNode(Lorg/htmlcleaner/TagNode;)V
    .locals 1
    .parameter "parentNode"

    .prologue
    .line 377
    const-string v0, "onClick"

    invoke-virtual {p1, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 378
    const-string v0, "class"

    invoke-virtual {p1, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 379
    const-string v0, "bak-src"

    invoke-virtual {p1, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 380
    return-void
.end method


# virtual methods
.method public visit(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/HtmlNode;)Z
    .locals 8
    .parameter "parentNode"
    .parameter "htmlNode"

    .prologue
    const/4 v7, 0x0

    .line 333
    instance-of v5, p2, Lorg/htmlcleaner/TagNode;

    if-eqz v5, :cond_0

    move-object v3, p2

    .line 334
    check-cast v3, Lorg/htmlcleaner/TagNode;

    .line 336
    .local v3, tagNode:Lorg/htmlcleaner/TagNode;
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/htmlcleaner/TagNode;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "table"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "rborder"

    const-string v6, "class"

    invoke-virtual {p1, v6}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 342
    invoke-virtual {v3}, Lorg/htmlcleaner/TagNode;->getChildTagList()Ljava/util/List;

    move-result-object v0

    .line 343
    .local v0, childrens:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 344
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/htmlcleaner/TagNode;

    invoke-virtual {v5}, Lorg/htmlcleaner/TagNode;->getChildTagList()Ljava/util/List;

    move-result-object v4

    .line 345
    .local v4, tdTags:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 349
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/htmlcleaner/TagNode;

    invoke-virtual {v5}, Lorg/htmlcleaner/TagNode;->getChildTagList()Ljava/util/List;

    move-result-object v2

    .line 350
    .local v2, imgTags:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 351
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/htmlcleaner/TagNode;

    .line 352
    .local v1, imgTag:Lorg/htmlcleaner/TagNode;
    invoke-direct {p0, v1}, Lcom/youdao/note/utils/HTMLUtils$2;->recoverSrc(Lorg/htmlcleaner/TagNode;)V

    .line 357
    #calls: Lcom/youdao/note/utils/HTMLUtils;->replaceNode(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/TagNode;)V
    invoke-static {p1, v1}, Lcom/youdao/note/utils/HTMLUtils;->access$100(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/TagNode;)V

    .line 373
    .end local v0           #childrens:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    .end local v1           #imgTag:Lorg/htmlcleaner/TagNode;
    .end local v2           #imgTags:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    .end local v3           #tagNode:Lorg/htmlcleaner/TagNode;
    .end local v4           #tdTags:Ljava/util/List;,"Ljava/util/List<Lorg/htmlcleaner/TagNode;>;"
    :cond_0
    :goto_0
    const/4 v5, 0x1

    return v5

    .line 366
    .restart local v3       #tagNode:Lorg/htmlcleaner/TagNode;
    :cond_1
    invoke-virtual {v3}, Lorg/htmlcleaner/TagNode;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "img"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "android-resource-img"

    const-string v6, "class"

    invoke-virtual {v3, v6}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 369
    invoke-direct {p0, v3}, Lcom/youdao/note/utils/HTMLUtils$2;->recoverSrc(Lorg/htmlcleaner/TagNode;)V

    .line 370
    invoke-direct {p0, v3}, Lcom/youdao/note/utils/HTMLUtils$2;->resetNode(Lorg/htmlcleaner/TagNode;)V

    goto :goto_0
.end method
