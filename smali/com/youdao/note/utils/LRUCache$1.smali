.class Lcom/youdao/note/utils/LRUCache$1;
.super Ljava/util/LinkedHashMap;
.source "LRUCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/utils/LRUCache;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x56f38c4f07874931L


# instance fields
.field final synthetic this$0:Lcom/youdao/note/utils/LRUCache;


# direct methods
.method constructor <init>(Lcom/youdao/note/utils/LRUCache;IFZ)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 36
    .local p0, this:Lcom/youdao/note/utils/LRUCache$1;,"Lcom/youdao/note/utils/LRUCache.1;"
    iput-object p1, p0, Lcom/youdao/note/utils/LRUCache$1;->this$0:Lcom/youdao/note/utils/LRUCache;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, this:Lcom/youdao/note/utils/LRUCache$1;,"Lcom/youdao/note/utils/LRUCache.1;"
    .local p1, eldest:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<TK;TV;>;"
    invoke-virtual {p0}, Lcom/youdao/note/utils/LRUCache$1;->size()I

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/utils/LRUCache$1;->this$0:Lcom/youdao/note/utils/LRUCache;

    #getter for: Lcom/youdao/note/utils/LRUCache;->cacheSize:I
    invoke-static {v2}, Lcom/youdao/note/utils/LRUCache;->access$000(Lcom/youdao/note/utils/LRUCache;)I

    move-result v2

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    .line 42
    .local v0, todel:Z
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/utils/LRUCache$1;->this$0:Lcom/youdao/note/utils/LRUCache;

    #getter for: Lcom/youdao/note/utils/LRUCache;->mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;
    invoke-static {v1}, Lcom/youdao/note/utils/LRUCache;->access$100(Lcom/youdao/note/utils/LRUCache;)Lcom/youdao/note/utils/LRUCache$CacheResizeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/youdao/note/utils/LRUCache$1;->this$0:Lcom/youdao/note/utils/LRUCache;

    #getter for: Lcom/youdao/note/utils/LRUCache;->mCacheReizeListener:Lcom/youdao/note/utils/LRUCache$CacheResizeListener;
    invoke-static {v1}, Lcom/youdao/note/utils/LRUCache;->access$100(Lcom/youdao/note/utils/LRUCache;)Lcom/youdao/note/utils/LRUCache$CacheResizeListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/youdao/note/utils/LRUCache$CacheResizeListener;->onEntryToDel(Ljava/util/Map$Entry;)V

    .line 45
    :cond_0
    return v0

    .line 41
    .end local v0           #todel:Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
