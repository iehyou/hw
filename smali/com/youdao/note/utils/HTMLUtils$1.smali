.class final Lcom/youdao/note/utils/HTMLUtils$1;
.super Ljava/lang/Object;
.source "HTMLUtils.java"

# interfaces
.implements Lorg/htmlcleaner/TagNodeVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/utils/HTMLUtils;->convertToLocalHtml(Ljava/lang/String;Lcom/youdao/note/datasource/DataSource;Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dataSource:Lcom/youdao/note/datasource/DataSource;

.field final synthetic val$noteId:Ljava/lang/String;

.field final synthetic val$resourceMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/lang/String;Lcom/youdao/note/datasource/DataSource;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, Lcom/youdao/note/utils/HTMLUtils$1;->val$resourceMap:Ljava/util/Map;

    iput-object p2, p0, Lcom/youdao/note/utils/HTMLUtils$1;->val$noteId:Ljava/lang/String;

    iput-object p3, p0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private convertAttachResrouce(Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/resource/GeneralResourceMeta;)V
    .locals 5
    .parameter "tag"
    .parameter "meta"

    .prologue
    .line 231
    const-class v3, Lcom/youdao/note/utils/HTMLUtils;

    const-string v4, "Convert attatch resource called."

    invoke-static {v3, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    invoke-virtual {p1}, Lorg/htmlcleaner/TagNode;->getParent()Lorg/htmlcleaner/TagNode;

    move-result-object v1

    .line 233
    .local v1, parent:Lorg/htmlcleaner/TagNode;
    invoke-virtual {v1, p1}, Lorg/htmlcleaner/TagNode;->getChildIndex(Lorg/htmlcleaner/HtmlNode;)I

    move-result v0

    .line 234
    .local v0, nodeIndex:I
    invoke-virtual {v1, p1}, Lorg/htmlcleaner/TagNode;->removeChild(Ljava/lang/Object;)Z

    .line 235
    iget-object v3, p0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-static {v3, p1, p2}, Lcom/youdao/note/utils/HTMLUtils;->createResourceNode(Lcom/youdao/note/datasource/DataSource;Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lorg/htmlcleaner/TagNode;

    move-result-object v2

    .line 236
    .local v2, table:Lorg/htmlcleaner/TagNode;
    invoke-virtual {v1, v0, v2}, Lorg/htmlcleaner/TagNode;->insertChild(ILorg/htmlcleaner/HtmlNode;)V

    .line 237
    return-void
.end method

.method private convertImageResource(Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/Thumbnail;)V
    .locals 4
    .parameter "tag"
    .parameter "thumbnail"

    .prologue
    .line 214
    iget-object v0, p0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p2}, Lcom/youdao/note/data/Thumbnail;->getRelativePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->existThumbnail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    invoke-static {}, Lcom/youdao/note/task/TaskManager;->getInstance()Lcom/youdao/note/task/TaskManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/youdao/note/task/TaskManager;->pullThumbnail(Lcom/youdao/note/data/Thumbnail;)V

    .line 217
    :cond_0
    const-class v0, Lcom/youdao/note/utils/HTMLUtils;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resource length is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/youdao/note/data/Thumbnail;->getLength()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    const-string v0, "class"

    const-string v1, "android-resource-img"

    invoke-virtual {p1, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "onClick"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.View.viewResource(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/youdao/note/data/Thumbnail;->getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method private getResource(Ljava/lang/String;Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;
    .locals 4
    .parameter "src"
    .parameter "path"
    .parameter "meta"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ")",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    if-eqz p2, :cond_0

    .line 201
    const/4 v2, 0x1

    invoke-static {p2, v2}, Lcom/youdao/note/utils/YNoteUtils;->extractResource(Ljava/lang/String;I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    .line 202
    .local v0, generalMeta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    new-instance v2, Lcom/youdao/note/data/resource/GeneralResource;

    invoke-direct {v2, v0}, Lcom/youdao/note/data/resource/GeneralResource;-><init>(Lcom/youdao/note/data/resource/GeneralResourceMeta;)V

    .line 206
    .end local v0           #generalMeta:Lcom/youdao/note/data/resource/GeneralResourceMeta;
    :goto_0
    return-object v2

    .line 204
    :cond_0
    new-instance v1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-direct {v1, p3}, Lcom/youdao/note/data/resource/ImageResourceMeta;-><init>(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 205
    .local v1, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 206
    new-instance v2, Lcom/youdao/note/data/resource/ImageResource;

    invoke-direct {v2, v1}, Lcom/youdao/note/data/resource/ImageResource;-><init>(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    goto :goto_0
.end method


# virtual methods
.method public visit(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/HtmlNode;)Z
    .locals 19
    .parameter "parentNode"
    .parameter "htmlNode"

    .prologue
    .line 119
    move-object/from16 v0, p2

    instance-of v0, v0, Lorg/htmlcleaner/TagNode;

    move/from16 v17, v0

    if-eqz v17, :cond_4

    move-object/from16 v14, p2

    .line 120
    check-cast v14, Lorg/htmlcleaner/TagNode;

    .line 121
    .local v14, tag:Lorg/htmlcleaner/TagNode;
    invoke-virtual {v14}, Lorg/htmlcleaner/TagNode;->getName()Ljava/lang/String;

    move-result-object v15

    .line 122
    .local v15, tagName:Ljava/lang/String;
    const-string v17, "img"

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 123
    const-string v17, "src"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 124
    .local v13, src:Ljava/lang/String;
    const-string v17, "path"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 125
    .local v10, path:Ljava/lang/String;
    const-string v17, "filename"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 127
    .local v4, fileName:Ljava/lang/String;
    if-eqz v10, :cond_5

    const/4 v12, 0x1

    .line 132
    .local v12, resourceType:I
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 133
    const-string v17, "title"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 135
    :cond_0
    const-string v17, "filelength"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, filelenStr:Ljava/lang/String;
    const/4 v6, 0x0

    .line 137
    .local v6, filelength:I
    if-eqz v5, :cond_1

    .line 139
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 142
    :cond_1
    :goto_1
    invoke-static {v13, v12}, Lcom/youdao/note/utils/YNoteUtils;->extractResource(Ljava/lang/String;I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    .line 144
    .local v3, baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    if-eqz v3, :cond_7

    .line 145
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v10, v3}, Lcom/youdao/note/utils/HTMLUtils$1;->getResource(Ljava/lang/String;Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v11

    .line 146
    .local v11, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$resourceMap:Ljava/util/Map;

    move-object/from16 v17, v0

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getResourceId()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    .line 149
    int-to-long v0, v6

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setLength(J)V

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$noteId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setNoteId(Ljava/lang/String;)V

    .line 151
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 152
    invoke-virtual {v3, v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 161
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v17, v0

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getResourceId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/youdao/note/datasource/DataSource;->getResourceMeta(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v9

    .line 163
    .local v9, oldResource:Lcom/youdao/note/data/resource/IResourceMeta;
    if-eqz v9, :cond_3

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getVersion()I

    move-result v17

    invoke-interface {v9}, Lcom/youdao/note/data/resource/IResourceMeta;->getVersion()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_3

    .line 166
    const-string v17, "try to delete reousrce."

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v17, v0

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 170
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z

    .line 171
    const-string v17, "src"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, bakSrc:Ljava/lang/String;
    const-string v17, "bak-src"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0, v2}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v17, "bak-style"

    const-string v18, "style"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v17, "style"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 175
    const-string v17, "bak-width"

    const-string v18, "width"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v17, "width"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 177
    const-string v17, "bak-height"

    const-string v18, "height"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->getAttributeByName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v17, "height"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/htmlcleaner/TagNode;->removeAttribute(Ljava/lang/String;)V

    .line 179
    const-string v17, "id"

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/AbstractResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v17, "src"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/utils/HTMLUtils$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    if-eqz v10, :cond_6

    move-object v7, v11

    .line 183
    check-cast v7, Lcom/youdao/note/data/resource/GeneralResource;

    .line 184
    .local v7, generalResource:Lcom/youdao/note/data/resource/GeneralResource;
    invoke-virtual {v7}, Lcom/youdao/note/data/resource/GeneralResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v17

    check-cast v17, Lcom/youdao/note/data/resource/GeneralResourceMeta;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v14, v1}, Lcom/youdao/note/utils/HTMLUtils$1;->convertAttachResrouce(Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/resource/GeneralResourceMeta;)V

    .line 196
    .end local v2           #bakSrc:Ljava/lang/String;
    .end local v3           #baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v4           #fileName:Ljava/lang/String;
    .end local v5           #filelenStr:Ljava/lang/String;
    .end local v6           #filelength:I
    .end local v7           #generalResource:Lcom/youdao/note/data/resource/GeneralResource;
    .end local v9           #oldResource:Lcom/youdao/note/data/resource/IResourceMeta;
    .end local v10           #path:Ljava/lang/String;
    .end local v11           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    .end local v12           #resourceType:I
    .end local v13           #src:Ljava/lang/String;
    .end local v14           #tag:Lorg/htmlcleaner/TagNode;
    .end local v15           #tagName:Ljava/lang/String;
    :cond_4
    :goto_2
    const/16 v17, 0x1

    return v17

    .line 127
    .restart local v4       #fileName:Ljava/lang/String;
    .restart local v10       #path:Ljava/lang/String;
    .restart local v13       #src:Ljava/lang/String;
    .restart local v14       #tag:Lorg/htmlcleaner/TagNode;
    .restart local v15       #tagName:Ljava/lang/String;
    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_0

    .restart local v2       #bakSrc:Ljava/lang/String;
    .restart local v3       #baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .restart local v5       #filelenStr:Ljava/lang/String;
    .restart local v6       #filelength:I
    .restart local v9       #oldResource:Lcom/youdao/note/data/resource/IResourceMeta;
    .restart local v11       #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    .restart local v12       #resourceType:I
    :cond_6
    move-object v8, v11

    .line 186
    check-cast v8, Lcom/youdao/note/data/resource/ImageResource;

    .line 187
    .local v8, imageResource:Lcom/youdao/note/data/resource/ImageResource;
    new-instance v16, Lcom/youdao/note/data/Thumbnail;

    invoke-virtual {v8}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v17

    check-cast v17, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    invoke-direct/range {v16 .. v17}, Lcom/youdao/note/data/Thumbnail;-><init>(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)V

    .line 188
    .local v16, thumbnail:Lcom/youdao/note/data/Thumbnail;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/youdao/note/utils/HTMLUtils$1;->convertImageResource(Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/Thumbnail;)V

    goto :goto_2

    .line 192
    .end local v2           #bakSrc:Ljava/lang/String;
    .end local v8           #imageResource:Lcom/youdao/note/data/resource/ImageResource;
    .end local v9           #oldResource:Lcom/youdao/note/data/resource/IResourceMeta;
    .end local v11           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    .end local v16           #thumbnail:Lcom/youdao/note/data/Thumbnail;
    :cond_7
    invoke-static {}, Lcom/youdao/note/utils/HTMLUtils;->access$000()Ljava/lang/String;

    move-result-object v17

    const-string v18, "got resource null."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 140
    .end local v3           #baseMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :catch_0
    move-exception v17

    goto/16 :goto_1
.end method
