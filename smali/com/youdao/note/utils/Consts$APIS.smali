.class public interface abstract Lcom/youdao/note/utils/Consts$APIS;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "APIS"
.end annotation


# static fields
.field public static final DEFAULT_BASE_VERSION:I = -0x1

.field public static final DEFAULT_BEGIN:I = 0x0

.field public static final DEFAULT_DEPTH:I = 0x0

.field public static final DEFAULT_DIGEST_LENGTH:I = 0x64

.field public static final DEFAULT_HEIGHT:I = 0x0

.field public static final DEFAULT_LENGTH:I = 0x3e8

.field public static final DEFAULT_LIST_MODE:I = 0x0

.field public static final DEFAULT_PATH:Ljava/lang/String; = "/"

.field public static final DEFAULT_THIN:Z = false

.field public static final DEFAULT_THRH_SIZE:J = 0x1800L

.field public static final DEFAULT_TIME_PARAMETER:J = -0x1L

.field public static final DEFAULT_VERSION:I = -0x1

.field public static final DEFAULT_WIDTH:I = 0x0

.field public static final EMAIL_RECEIVER_LIMIT:I = 0x3

.field public static final HOST:Ljava/lang/String; = "note.youdao.com/"

.field public static final HOST_DEBUG:Ljava/lang/String; = "note.corp.youdao.com/"

.field public static final HTTP:Ljava/lang/String; = "http://"

.field public static final LOGIN_URL:Ljava/lang/String; = "http://reg.163.com/services/userlogin?username=%s&password=%s&type=1&userip=%s&product=note&savelogin=1&passtype=0&back_url=http://www.youdao.com"

.field public static final METHOD_CLEAR:Ljava/lang/String; = "clear"

.field public static final METHOD_GET:Ljava/lang/String; = "get"

.field public static final METHOD_GETRES:Ljava/lang/String; = "getres"

.field public static final METHOD_MOVE:Ljava/lang/String; = "mv"

.field public static final METHOD_PUT:Ljava/lang/String; = "put"

.field public static final METHOD_PUTFILE:Ljava/lang/String; = "putfile"

.field public static final METHOD_PUTRES:Ljava/lang/String; = "putres"

.field public static final METHOD_RECOVER:Ljava/lang/String; = "recover"

.field public static final METHOD_RM:Ljava/lang/String; = "rm"

.field public static final METHOD_SENDMAIL:Ljava/lang/String; = "mail"

.field public static final METHOD_THM:Ljava/lang/String; = "thm"

.field public static final METHOD_UPDATE:Ljava/lang/String; = "update"

.field public static final NAME_AUTHOR:Ljava/lang/String; = "au"

.field public static final NAME_BASE_VERSION:Ljava/lang/String; = "bv"

.field public static final NAME_BEGIN:Ljava/lang/String; = "b"

.field public static final NAME_BYTE_STREAM:Ljava/lang/String; = "bs"

.field public static final NAME_CREATE_TIME_BEGIN:Ljava/lang/String; = "cb"

.field public static final NAME_CRERTE_TIME_END:Ljava/lang/String; = "ce"

.field public static final NAME_DEL:Ljava/lang/String; = "del"

.field public static final NAME_DEPTH:Ljava/lang/String; = "dp"

.field public static final NAME_DEST_PATH:Ljava/lang/String; = "dst"

.field public static final NAME_FILE_ID:Ljava/lang/String; = "fid"

.field public static final NAME_HEIGHT:Ljava/lang/String; = "h"

.field public static final NAME_ISDIR:Ljava/lang/String; = "r"

.field public static final NAME_KEY_WORD:Ljava/lang/String; = "kw"

.field public static final NAME_LENGTH:Ljava/lang/String; = "l"

.field public static final NAME_LIST_MODE:Ljava/lang/String; = "m"

.field public static final NAME_MAIL_SHARE_MESSAGE:Ljava/lang/String; = "msg"

.field public static final NAME_MAIL_SHARE_RECEIVERS:Ljava/lang/String; = "to"

.field public static final NAME_MAIL_SHARE_SUBJECT:Ljava/lang/String; = "subject"

.field public static final NAME_METHOD_PARAM:Ljava/lang/String; = "method"

.field public static final NAME_MODIFY_TIME_END:Ljava/lang/String; = "me"

.field public static final NAME_MODITY_TIME_BEGIN:Ljava/lang/String; = "mb"

.field public static final NAME_NEED_SUMMARY:Ljava/lang/String; = "dg"

.field public static final NAME_NOTE_BOOK:Ljava/lang/String; = "nb"

.field public static final NAME_PATH:Ljava/lang/String; = "p"

.field public static final NAME_PATH_LIST:Ljava/lang/String; = "ps"

.field public static final NAME_RESOURECE_ID:Ljava/lang/String; = "fid"

.field public static final NAME_SOURCE:Ljava/lang/String; = "src"

.field public static final NAME_SUMMARY_LEN:Ljava/lang/String; = "dn"

.field public static final NAME_THIN:Ljava/lang/String; = "th"

.field public static final NAME_THM:Ljava/lang/String; = "thm"

.field public static final NAME_THRH:Ljava/lang/String; = "thr"

.field public static final NAME_TITLE:Ljava/lang/String; = "tl"

.field public static final NAME_VERSION:Ljava/lang/String; = "v"

.field public static final NAME_WIDTH:Ljava/lang/String; = "w"

.field public static final PATH_FILE:Ljava/lang/String; = "file"

.field public static final PATH_FILEMETA:Ljava/lang/String; = "filemeta"

.field public static final PATH_LIST:Ljava/lang/String; = "list"

.field public static final PATH_LOG:Ljava/lang/String; = "ilogrpt"

.field public static final PATH_PREFIX:Ljava/lang/String; = "yws/mapi/"

.field public static final PATH_SEARCH:Ljava/lang/String; = "search"

.field public static final PATH_SHARE:Ljava/lang/String; = "share"

.field public static final PATH_TRASH:Ljava/lang/String; = "trash"

.field public static final PATH_USER:Ljava/lang/String; = "user"

.field public static final RES_PREFIX:Ljava/lang/String; = "yws/res/"
