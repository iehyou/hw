.class public Lcom/youdao/note/utils/L;
.super Ljava/lang/Object;
.source "L.java"


# static fields
.field private static sDebug:Z

.field private static sLogger:Lcom/youdao/note/LogRecorder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x1

    sput-boolean v0, Lcom/youdao/note/utils/L;->sDebug:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "object"
    .parameter "m"

    .prologue
    .line 38
    sget-boolean v0, Lcom/youdao/note/utils/L;->sDebug:Z

    if-eqz v0, :cond_0

    .line 39
    invoke-static {p0}, Lcom/youdao/note/utils/L;->getTagName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .parameter "object"
    .parameter "m"
    .parameter "t"

    .prologue
    .line 48
    if-eqz p2, :cond_0

    .line 49
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 50
    .local v1, sw:Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 51
    .local v0, pw:Ljava/io/PrintWriter;
    invoke-virtual {p2, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 52
    sget-object v2, Lcom/youdao/note/utils/L;->sLogger:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/LogRecorder;->setError(Ljava/lang/String;)V

    .line 54
    .end local v0           #pw:Ljava/io/PrintWriter;
    .end local v1           #sw:Ljava/io/StringWriter;
    :cond_0
    sget-boolean v2, Lcom/youdao/note/utils/L;->sDebug:Z

    if-eqz v2, :cond_1

    .line 55
    invoke-static {p0}, Lcom/youdao/note/utils/L;->getTagName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 57
    :cond_1
    return-void
.end method

.method private static getTagName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .parameter "o"

    .prologue
    .line 60
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 61
    check-cast p0, Ljava/lang/Class;

    .end local p0
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 63
    .restart local p0
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/L;->getTagName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "object"
    .parameter "m"

    .prologue
    .line 44
    invoke-static {p0}, Lcom/youdao/note/utils/L;->getTagName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    return-void
.end method

.method public static setDebug(Z)V
    .locals 0
    .parameter "debug"

    .prologue
    .line 24
    sput-boolean p0, Lcom/youdao/note/utils/L;->sDebug:Z

    .line 25
    return-void
.end method

.method public static setLogger(Lcom/youdao/note/LogRecorder;)V
    .locals 0
    .parameter "logger"

    .prologue
    .line 28
    sput-object p0, Lcom/youdao/note/utils/L;->sLogger:Lcom/youdao/note/LogRecorder;

    .line 29
    return-void
.end method

.method public static w(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .parameter "object"
    .parameter "m"

    .prologue
    .line 32
    sget-boolean v0, Lcom/youdao/note/utils/L;->sDebug:Z

    if-eqz v0, :cond_0

    .line 33
    invoke-static {p0}, Lcom/youdao/note/utils/L;->getTagName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    :cond_0
    return-void
.end method
