.class public interface abstract Lcom/youdao/note/utils/Consts$DATA_TYPE;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DATA_TYPE"
.end annotation


# static fields
.field public static final ADD_RESOURCE:I = 0x12

.field public static final ADD_RESOURCE_FINISH:I = 0x13

.field public static final AUTO_NOTE_REFRESH:I = 0xf

.field public static final CHECK_NEED_UPDATE:I = 0x14

.field public static final CHECK_VERSION_UPDATE:I = 0x15

.field public static final DATA_TYPE_LIST:I = 0x1

.field public static final DATA_TYPE_NOTE:I = 0x2

.field public static final DATA_TYPE_NOTE_BOOK:I = 0x3

.field public static final DATA_TYPE_THUMBNAIL:I = 0x5

.field public static final DATA_TYPE_USER:I = 0x4

.field public static final DEL_RESULT:I = 0xc

.field public static final DOWNLOAD_PROGRESS:I = 0x8

.field public static final DOWNLOAD_RESULT:I = 0x9

.field public static final FILEMETA_UPDATE:I = 0x10

.field public static final LOGIN_RESULT:I = 0xb

.field public static final MAIL_SHARE:I = 0x11

.field public static final NOTE_META:I = 0xe

.field public static final SEARCH_RESULT:I = 0xa

.field public static final SYNC_PROGRESS:I = 0x7

.field public static final SYNC_RESULT:I = 0x6

.field public static final USERMETA_RESULT:I = 0xd
