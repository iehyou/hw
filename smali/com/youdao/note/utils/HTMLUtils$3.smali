.class final Lcom/youdao/note/utils/HTMLUtils$3;
.super Ljava/lang/Object;
.source "HTMLUtils.java"

# interfaces
.implements Lorg/htmlcleaner/TagNodeVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/utils/HTMLUtils;->extractSummary(Ljava/lang/String;I)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$maxSize:I

.field final synthetic val$ret:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>(Ljava/lang/StringBuilder;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 459
    iput-object p1, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    iput p2, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$maxSize:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/HtmlNode;)Z
    .locals 7
    .parameter "parentNode"
    .parameter "htmlNode"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 462
    instance-of v4, p2, Lorg/htmlcleaner/ContentNode;

    if-eqz v4, :cond_0

    move-object v1, p2

    .line 463
    check-cast v1, Lorg/htmlcleaner/ContentNode;

    .line 464
    .local v1, contentNode:Lorg/htmlcleaner/ContentNode;
    invoke-virtual {v1}, Lorg/htmlcleaner/ContentNode;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/youdao/note/utils/HTMLUtils;->revertHTMLText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 465
    .local v0, content:Ljava/lang/String;
    iget-object v4, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$maxSize:I

    iget-object v6, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    sub-int/2addr v5, v6

    if-ge v4, v5, :cond_1

    .line 467
    iget-object v3, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    .end local v0           #content:Ljava/lang/String;
    .end local v1           #contentNode:Lorg/htmlcleaner/ContentNode;
    :cond_0
    :goto_0
    return v2

    .line 470
    .restart local v0       #content:Ljava/lang/String;
    .restart local v1       #contentNode:Lorg/htmlcleaner/ContentNode;
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$maxSize:I

    iget-object v5, p0, Lcom/youdao/note/utils/HTMLUtils$3;->val$ret:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v3

    .line 471
    goto :goto_0
.end method
