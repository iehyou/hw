.class public Lcom/youdao/note/utils/HTMLUtils;
.super Ljava/lang/Object;
.source "HTMLUtils.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$APIS;


# static fields
.field public static final ANDROID_RESOURCE_ATT:Ljava/lang/String; = "rborder"

.field public static final ANDROID_RESOURCE_IMG:Ljava/lang/String; = "android-resource-img"

.field private static final ATTR_FILENAME:Ljava/lang/String; = "filename"

.field private static final ATTR_LENGTH:Ljava/lang/String; = "filelength"

.field private static final ATTR_PATH:Ljava/lang/String; = "path"

.field private static final ATTR_SRC:Ljava/lang/String; = "src"

.field private static final ATTR_TITLE:Ljava/lang/String; = "title"

.field private static final BAK_HEIGHT:Ljava/lang/String; = "bak-height"

.field private static final BAK_SRC:Ljava/lang/String; = "bak-src"

.field private static final BAK_STYLE:Ljava/lang/String; = "bak-style"

.field private static final BAK_WIDTH:Ljava/lang/String; = "bak-width"

.field public static final BR:Ljava/lang/String; = "<br/>"

.field public static final EMPTY_FOOTER:Ljava/lang/String; = "<br/><span>&nbsp;</span>"

.field private static final TAG:Ljava/lang/String; = null

.field private static final UNUSED_HTML_FOOT:Ljava/lang/String; = "</body></html>"

.field private static final UNUSED_HTML_HEAD:Ljava/lang/String; = "<html><head></head><body>"

.field private static sCleaner:Lorg/htmlcleaner/HtmlCleaner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    const-class v1, Lcom/youdao/note/utils/HTMLUtils;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/youdao/note/utils/HTMLUtils;->TAG:Ljava/lang/String;

    .line 87
    new-instance v1, Lorg/htmlcleaner/HtmlCleaner;

    invoke-direct {v1}, Lorg/htmlcleaner/HtmlCleaner;-><init>()V

    sput-object v1, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    .line 88
    sget-object v1, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    invoke-virtual {v1}, Lorg/htmlcleaner/HtmlCleaner;->getProperties()Lorg/htmlcleaner/CleanerProperties;

    move-result-object v0

    .line 89
    .local v0, props:Lorg/htmlcleaner/CleanerProperties;
    invoke-virtual {v0, v2}, Lorg/htmlcleaner/CleanerProperties;->setAllowHtmlInsideAttributes(Z)V

    .line 90
    invoke-virtual {v0, v2}, Lorg/htmlcleaner/CleanerProperties;->setAllowMultiWordAttributes(Z)V

    .line 91
    invoke-virtual {v0, v2}, Lorg/htmlcleaner/CleanerProperties;->setRecognizeUnicodeChars(Z)V

    .line 92
    invoke-virtual {v0, v2}, Lorg/htmlcleaner/CleanerProperties;->setOmitComments(Z)V

    .line 93
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/youdao/note/utils/HTMLUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/TagNode;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/youdao/note/utils/HTMLUtils;->replaceNode(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/TagNode;)V

    return-void
.end method

.method public static declared-synchronized convertToLocalHtml(Ljava/lang/String;Lcom/youdao/note/datasource/DataSource;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .parameter "noteId"
    .parameter "dataSource"
    .parameter "html"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    const-class v6, Lcom/youdao/note/utils/HTMLUtils;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    invoke-virtual {v5, p2}, Lorg/htmlcleaner/HtmlCleaner;->clean(Ljava/lang/String;)Lorg/htmlcleaner/TagNode;

    move-result-object v3

    .line 108
    .local v3, node:Lorg/htmlcleaner/TagNode;
    sget-object v5, Lcom/youdao/note/utils/HTMLUtils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Before convert to local html is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 112
    .local v4, resourceMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-virtual {p1, p0}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 113
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v3           #node:Lorg/htmlcleaner/TagNode;
    .end local v4           #resourceMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    .line 116
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #node:Lorg/htmlcleaner/TagNode;
    .restart local v4       #resourceMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    :cond_0
    :try_start_1
    new-instance v5, Lcom/youdao/note/utils/HTMLUtils$1;

    invoke-direct {v5, v4, p0, p1}, Lcom/youdao/note/utils/HTMLUtils$1;-><init>(Ljava/util/Map;Ljava/lang/String;Lcom/youdao/note/datasource/DataSource;)V

    invoke-virtual {v3, v5}, Lorg/htmlcleaner/TagNode;->traverse(Lorg/htmlcleaner/TagNodeVisitor;)V

    .line 243
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 244
    .local v0, deletedResource:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {p1, v5}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_1

    .line 247
    .end local v0           #deletedResource:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    :cond_1
    invoke-static {v3}, Lcom/youdao/note/utils/HTMLUtils;->serilizeHtml(Lorg/htmlcleaner/TagNode;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    monitor-exit v6

    return-object v5
.end method

.method public static declared-synchronized convertToServerHtml(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter "html"

    .prologue
    .line 326
    const-class v5, Lcom/youdao/note/utils/HTMLUtils;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    invoke-virtual {v4, p0}, Lorg/htmlcleaner/HtmlCleaner;->clean(Ljava/lang/String;)Lorg/htmlcleaner/TagNode;

    move-result-object v3

    .line 327
    .local v3, node:Lorg/htmlcleaner/TagNode;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v1

    .line 328
    .local v1, dataSource:Lcom/youdao/note/datasource/DataSource;
    new-instance v4, Lcom/youdao/note/utils/HTMLUtils$2;

    invoke-direct {v4, v1}, Lcom/youdao/note/utils/HTMLUtils$2;-><init>(Lcom/youdao/note/datasource/DataSource;)V

    invoke-virtual {v3, v4}, Lorg/htmlcleaner/TagNode;->traverse(Lorg/htmlcleaner/TagNodeVisitor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    :try_start_1
    invoke-static {v3}, Lcom/youdao/note/utils/HTMLUtils;->serilizeHtml(Lorg/htmlcleaner/TagNode;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 446
    :goto_0
    monitor-exit v5

    return-object v0

    .line 445
    :catch_0
    move-exception v2

    .local v2, e:Ljava/lang/Exception;
    move-object v0, p0

    .line 446
    goto :goto_0

    .line 326
    .end local v1           #dataSource:Lcom/youdao/note/datasource/DataSource;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #node:Lorg/htmlcleaner/TagNode;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method public static createResourceNode(Lcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lorg/htmlcleaner/TagNode;
    .locals 3
    .parameter "dataSource"
    .parameter "meta"

    .prologue
    .line 258
    new-instance v0, Lorg/htmlcleaner/TagNode;

    const-string v1, "img"

    invoke-direct {v0, v1}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 259
    .local v0, img:Lorg/htmlcleaner/TagNode;
    const-string v1, "id"

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-static {p0, v0, p1}, Lcom/youdao/note/utils/HTMLUtils;->createResourceNode(Lcom/youdao/note/datasource/DataSource;Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lorg/htmlcleaner/TagNode;

    move-result-object v1

    return-object v1
.end method

.method public static createResourceNode(Lcom/youdao/note/datasource/DataSource;Lorg/htmlcleaner/TagNode;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lorg/htmlcleaner/TagNode;
    .locals 20
    .parameter "dataSource"
    .parameter "tag"
    .parameter "meta"

    .prologue
    .line 265
    new-instance v9, Lorg/htmlcleaner/TagNode;

    const-string v14, "table"

    invoke-direct {v9, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 266
    .local v9, table:Lorg/htmlcleaner/TagNode;
    const-string v14, "contenteditable"

    const-string v15, "false"

    invoke-virtual {v9, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    new-instance v12, Lorg/htmlcleaner/TagNode;

    const-string v14, "tr"

    invoke-direct {v12, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 269
    .local v12, tr1:Lorg/htmlcleaner/TagNode;
    new-instance v6, Lorg/htmlcleaner/TagNode;

    const-string v14, "td"

    invoke-direct {v6, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 270
    .local v6, iconTd:Lorg/htmlcleaner/TagNode;
    const-string v14, "width"

    const-string v15, "35px"

    invoke-virtual {v6, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v14, "rowspan"

    const-string v15, "2"

    invoke-virtual {v6, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v14, "src"

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/youdao/note/utils/FileUtils;->getResourceIconPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v14, "class"

    const-string v15, "attachicon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 275
    invoke-virtual {v12, v6}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 277
    new-instance v5, Lorg/htmlcleaner/TagNode;

    const-string v14, "td"

    invoke-direct {v5, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 278
    .local v5, fileNameTd:Lorg/htmlcleaner/TagNode;
    const-string v14, "width"

    const-string v15, "230px"

    invoke-virtual {v5, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v4, Lorg/htmlcleaner/ContentNode;

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v4, v14}, Lorg/htmlcleaner/ContentNode;-><init>(Ljava/lang/String;)V

    .line 280
    .local v4, fileNameNode:Lorg/htmlcleaner/ContentNode;
    invoke-virtual {v5, v4}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 281
    invoke-virtual {v12, v5}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 283
    new-instance v2, Lorg/htmlcleaner/TagNode;

    const-string v14, "td"

    invoke-direct {v2, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 284
    .local v2, arrowTd:Lorg/htmlcleaner/TagNode;
    const-string v14, "width"

    const-string v15, "20px"

    invoke-virtual {v2, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v14, "rowspan"

    const-string v15, "2"

    invoke-virtual {v2, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    new-instance v7, Lorg/htmlcleaner/TagNode;

    const-string v14, "img"

    invoke-direct {v7, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 287
    .local v7, imgButton:Lorg/htmlcleaner/TagNode;
    const-string v14, "class"

    const-string v15, "arrow"

    invoke-virtual {v7, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->existResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 294
    const-string v14, "src"

    const-string v15, "file:///android_asset/arrow.png"

    invoke-virtual {v7, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :goto_0
    invoke-virtual {v2, v7}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 300
    invoke-virtual {v12, v2}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 301
    invoke-virtual {v9, v12}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 303
    new-instance v13, Lorg/htmlcleaner/TagNode;

    const-string v14, "tr"

    invoke-direct {v13, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 304
    .local v13, tr2:Lorg/htmlcleaner/TagNode;
    new-instance v8, Lorg/htmlcleaner/TagNode;

    const-string v14, "td"

    invoke-direct {v8, v14}, Lorg/htmlcleaner/TagNode;-><init>(Ljava/lang/String;)V

    .line 305
    .local v8, sizeTd:Lorg/htmlcleaner/TagNode;
    const-string v14, "style"

    const-string v15, "color:gray"

    invoke-virtual {v8, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/youdao/note/datasource/DataSource;->getTmpResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 307
    .local v11, tmpFilePath:Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .local v10, tmpFile:Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 310
    new-instance v3, Lorg/htmlcleaner/ContentNode;

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v14

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4059

    mul-double v16, v16, v18

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/youdao/note/utils/UnitUtils;->getSize(JI)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v3, v14}, Lorg/htmlcleaner/ContentNode;-><init>(Ljava/lang/String;)V

    .line 314
    .local v3, contentNode:Lorg/htmlcleaner/ContentNode;
    :goto_1
    invoke-virtual {v8, v3}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 315
    invoke-virtual {v13, v8}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 317
    invoke-virtual {v9, v13}, Lorg/htmlcleaner/TagNode;->addChild(Ljava/lang/Object;)V

    .line 319
    const-string v14, "onClick"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "window.View.viewResource(\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\')"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v14, "class"

    const-string v15, "rborder"

    invoke-virtual {v9, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    return-object v9

    .line 296
    .end local v3           #contentNode:Lorg/htmlcleaner/ContentNode;
    .end local v8           #sizeTd:Lorg/htmlcleaner/TagNode;
    .end local v10           #tmpFile:Ljava/io/File;
    .end local v11           #tmpFilePath:Ljava/lang/String;
    .end local v13           #tr2:Lorg/htmlcleaner/TagNode;
    :cond_0
    const-string v14, "src"

    const-string v15, "file:///android_asset/download.png"

    invoke-virtual {v7, v14, v15}, Lorg/htmlcleaner/TagNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 312
    .restart local v8       #sizeTd:Lorg/htmlcleaner/TagNode;
    .restart local v10       #tmpFile:Ljava/io/File;
    .restart local v11       #tmpFilePath:Ljava/lang/String;
    .restart local v13       #tr2:Lorg/htmlcleaner/TagNode;
    :cond_1
    new-instance v3, Lorg/htmlcleaner/ContentNode;

    invoke-virtual/range {p2 .. p2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v14

    invoke-static {v14, v15}, Lcom/youdao/note/utils/UnitUtils;->getSize(J)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v3, v14}, Lorg/htmlcleaner/ContentNode;-><init>(Ljava/lang/String;)V

    .restart local v3       #contentNode:Lorg/htmlcleaner/ContentNode;
    goto :goto_1
.end method

.method public static escAndEncodeHTML(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "s"

    .prologue
    .line 488
    if-nez p0, :cond_0

    .line 489
    const-string v4, ""

    .line 511
    :goto_0
    return-object v4

    .line 491
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 492
    .local v0, buffer:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 493
    .local v3, len:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v3, :cond_7

    .line 494
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 495
    .local v1, ch:C
    const/16 v4, 0x22

    if-ne v1, v4, :cond_1

    .line 496
    const-string v4, "&quot;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 497
    :cond_1
    const/16 v4, 0x26

    if-ne v1, v4, :cond_2

    .line 498
    const-string v4, "&amp;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 499
    :cond_2
    const/16 v4, 0x3c

    if-ne v1, v4, :cond_3

    .line 500
    const-string v4, "&lt;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 501
    :cond_3
    const/16 v4, 0x3e

    if-ne v1, v4, :cond_4

    .line 502
    const-string v4, "&gt;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 503
    :cond_4
    const/16 v4, 0x20

    if-ne v1, v4, :cond_5

    .line 504
    const-string v4, "&nbsp;"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 505
    :cond_5
    const/16 v4, 0xa

    if-ne v1, v4, :cond_6

    .line 506
    const-string v4, "<br/>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 508
    :cond_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 511
    .end local v1           #ch:C
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static declared-synchronized extractSummary(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .parameter "html"
    .parameter "maxSize"

    .prologue
    .line 457
    const-class v3, Lcom/youdao/note/utils/HTMLUtils;

    monitor-enter v3

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 458
    .local v1, ret:Ljava/lang/StringBuilder;
    sget-object v2, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    invoke-virtual {v2, p0}, Lorg/htmlcleaner/HtmlCleaner;->clean(Ljava/lang/String;)Lorg/htmlcleaner/TagNode;

    move-result-object v0

    .line 459
    .local v0, node:Lorg/htmlcleaner/TagNode;
    new-instance v2, Lcom/youdao/note/utils/HTMLUtils$3;

    invoke-direct {v2, v1, p1}, Lcom/youdao/note/utils/HTMLUtils$3;-><init>(Ljava/lang/StringBuilder;I)V

    invoke-virtual {v0, v2}, Lorg/htmlcleaner/TagNode;->traverse(Lorg/htmlcleaner/TagNodeVisitor;)V

    .line 478
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit v3

    return-object v2

    .line 457
    .end local v0           #node:Lorg/htmlcleaner/TagNode;
    .end local v1           #ret:Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static replaceNode(Lorg/htmlcleaner/TagNode;Lorg/htmlcleaner/TagNode;)V
    .locals 2
    .parameter "node"
    .parameter "newNode"

    .prologue
    .line 251
    invoke-virtual {p0}, Lorg/htmlcleaner/TagNode;->getParent()Lorg/htmlcleaner/TagNode;

    move-result-object v1

    .line 252
    .local v1, parent:Lorg/htmlcleaner/TagNode;
    invoke-virtual {v1, p0}, Lorg/htmlcleaner/TagNode;->getChildIndex(Lorg/htmlcleaner/HtmlNode;)I

    move-result v0

    .line 253
    .local v0, nodeIndex:I
    invoke-virtual {v1, v0, p1}, Lorg/htmlcleaner/TagNode;->insertChild(ILorg/htmlcleaner/HtmlNode;)V

    .line 254
    invoke-virtual {v1, p0}, Lorg/htmlcleaner/TagNode;->removeChild(Ljava/lang/Object;)Z

    .line 255
    return-void
.end method

.method public static revertHTMLText(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "s"

    .prologue
    .line 482
    const-string v0, "&lt;"

    const-string v1, "<"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&gt;"

    const-string v2, ">"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&quot;"

    const-string v2, "\""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&amp;"

    const-string v2, "&"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&nbsp;"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized serilizeHtml(Lorg/htmlcleaner/TagNode;)Ljava/lang/String;
    .locals 7
    .parameter "node"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 515
    const-class v4, Lcom/youdao/note/utils/HTMLUtils;

    monitor-enter v4

    :try_start_0
    new-instance v1, Lorg/htmlcleaner/SimpleHtmlSerializer;

    sget-object v3, Lcom/youdao/note/utils/HTMLUtils;->sCleaner:Lorg/htmlcleaner/HtmlCleaner;

    invoke-virtual {v3}, Lorg/htmlcleaner/HtmlCleaner;->getProperties()Lorg/htmlcleaner/CleanerProperties;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/htmlcleaner/SimpleHtmlSerializer;-><init>(Lorg/htmlcleaner/CleanerProperties;)V

    .line 516
    .local v1, serializer:Lorg/htmlcleaner/SimpleHtmlSerializer;
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 517
    .local v2, sw:Ljava/io/StringWriter;
    invoke-virtual {p0, v1, v2}, Lorg/htmlcleaner/TagNode;->serialize(Lorg/htmlcleaner/Serializer;Ljava/io/Writer;)V

    .line 518
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, content:Ljava/lang/String;
    const-string v3, "<html><head></head><body>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "</body></html>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 521
    const-string v3, "<html><head></head><body>"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const-string v6, "</body></html>"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 524
    :cond_0
    const-string v3, "<br/><span>&nbsp;</span>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 525
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "<br/><span>&nbsp;</span>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 528
    :cond_1
    const-class v3, Lcom/youdao/note/utils/HTMLUtils;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "converted string is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    monitor-exit v4

    return-object v0

    .line 515
    .end local v0           #content:Ljava/lang/String;
    .end local v1           #serializer:Lorg/htmlcleaner/SimpleHtmlSerializer;
    .end local v2           #sw:Ljava/io/StringWriter;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method
