.class public interface abstract Lcom/youdao/note/utils/Consts$HTML;
.super Ljava/lang/Object;
.source "Consts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/Consts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HTML"
.end annotation


# static fields
.field public static final EDITABLE_HTML_HEAD:Ljava/lang/String; = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"editor2.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>"

.field public static final HTML_ENCODING:Ljava/lang/String; = "utf-8"

.field public static final HTML_FOOTER:Ljava/lang/String; = "</body></html>"

.field public static final HTML_HEAD:Ljava/lang/String; = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/><script type=\"text/javascript\" src=\"detail.js\"></script><link rel=\"stylesheet\" href=\"editor.css\" /></head><body>"

.field public static final HTML_TYPE:Ljava/lang/String; = "text/html"
