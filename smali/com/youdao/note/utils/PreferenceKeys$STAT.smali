.class public interface abstract Lcom/youdao/note/utils/PreferenceKeys$STAT;
.super Ljava/lang/Object;
.source "PreferenceKeys.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/utils/PreferenceKeys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "STAT"
.end annotation


# static fields
.field public static final ADD_ART_TIMES:Ljava/lang/String; = "AddArtTimes"

.field public static final ADD_CAMERA_TIMES:Ljava/lang/String; = "AddCameraTimes"

.field public static final ADD_NOTE_TIMES:Ljava/lang/String; = "AddNoteTimes"

.field public static final ADD_PAINT_TIMES:Ljava/lang/String; = "AddPaintTimes"

.field public static final ADD_PHOTO_TIMES:Ljava/lang/String; = "AddPhotoTimes"

.field public static final ADD_RECORD_TIMES:Ljava/lang/String; = "AddRecordTimes"

.field public static final APP_ADD_TIMES:Ljava/lang/String; = "AppAddTimes"

.field public static final ATTACH_NUM:Ljava/lang/String; = "AttachNum"

.field public static final AUTO_SYNC_STATUS:Ljava/lang/String; = "AutoSyncStatus"

.field public static final CAMERA_NOTE_TIMES:Ljava/lang/String; = "CameraNoteTimes"

.field public static final CONTENT_COPY_TIMES:Ljava/lang/String; = "ContentCopyTimes"

.field public static final DPI:Ljava/lang/String; = "dpi"

.field public static final EDIT_NOTE_TIMES:Ljava/lang/String; = "EditNoteTimes"

.field public static final ENTRY_STATUS_BAR:Ljava/lang/String; = "statusBar"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final EVER_REPORT:Ljava/lang/String; = "ever_report"

.field public static final HANDWRITE_MODE:Ljava/lang/String; = "machine"

.field public static final HANDWRITE_MODE_BAD:Ljava/lang/String; = "BadNWrite"

.field public static final HANDWRITE_MODE_FINE:Ljava/lang/String; = "FineNWrite"

.field public static final ICON_ALL_TIMES:Ljava/lang/String; = "IconAllTimes"

.field public static final ICON_NEW_TIMES:Ljava/lang/String; = "IconNewTimes"

.field public static final ICON_NOTEBOOK_TIMES:Ljava/lang/String; = "IconNotebookTimes"

.field public static final ICON_SYNC_TIMES:Ljava/lang/String; = "IconSyncTimes"

.field public static final IMAGE_RECTIFICATION_TIMES:Ljava/lang/String; = "WbCorrectTimes"

.field public static final IMEI:Ljava/lang/String; = "imei"

.field public static final KEYFROM:Ljava/lang/String; = "keyfrom"

.field public static final LAST_REPORT_TIME:Ljava/lang/String; = "last_report_time"

.field public static final LOGIN_STATUS:Ljava/lang/String; = "LoginStatus"

.field public static final MAIL_SEND_TIMES:Ljava/lang/String; = "SendMailTimes"

.field public static final MAIL_SHARE_TIMES:Ljava/lang/String; = "EditMailTimes"

.field public static final MID:Ljava/lang/String; = "mid"

.field public static final MODEL:Ljava/lang/String; = "model"

.field public static final NOTE_DETAIL_TIMES:Ljava/lang/String; = "NoteDetailTimes"

.field public static final PASS_CODE_STATUS:Ljava/lang/String; = "PassCodeStatus"

.field public static final PHONE_VERSION:Ljava/lang/String; = "phoneVersion"

.field public static final RECORD_FROM_BAR:Ljava/lang/String; = "RecordFromBar"

.field public static final RECORD_NOTE_TIMES:Ljava/lang/String; = "RecordNoteTimes"

.field public static final RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final SEARCH_NOTE_TIMES:Ljava/lang/String; = "SearchNoteTimes"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final STAT_DURATION:Ljava/lang/String; = "StatDuration"

.field public static final TEXT_NOTE_TIMES:Ljava/lang/String; = "TextNoteTimes"

.field public static final VENDOR:Ljava/lang/String; = "vendor"

.field public static final VIEW_ATTACH_TIMES:Ljava/lang/String; = "ViewAttachTimes"

.field public static final VIEW_NOTE_TIMES:Ljava/lang/String; = "ViewNoteTimes"

.field public static final VIEW_PIC_TIMES:Ljava/lang/String; = "ViewPicTimes"

.field public static final WB_CORRECT_TIMES:Ljava/lang/String; = "WbCorrectTimes"

.field public static final WIDGET_ADD_TIMES:Ljava/lang/String; = "WidgetAddTimes"
