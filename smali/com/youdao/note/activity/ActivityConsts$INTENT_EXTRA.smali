.class public interface abstract Lcom/youdao/note/activity/ActivityConsts$INTENT_EXTRA;
.super Ljava/lang/Object;
.source "ActivityConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/ActivityConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "INTENT_EXTRA"
.end annotation


# static fields
.field public static final ENTRY_FROM:Ljava/lang/String; = "entry_from"

.field public static final EXTRA_EMAILS:Ljava/lang/String; = "emails"

.field public static final EXTRA_LIST_TYPE:Ljava/lang/String; = "listType"

.field public static final EXTRA_LOGIN_ERROR:Ljava/lang/String; = "loginError"

.field public static final EXTRA_NOTE:Ljava/lang/String; = "note"

.field public static final EXTRA_NOTEBOOK:Ljava/lang/String; = "noteBook"

.field public static final EXTRA_NOTEMETA:Ljava/lang/String; = "noteMeta"

.field public static final EXTRA_RESOURCE_META:Ljava/lang/String; = "resourceMeta"

.field public static final EXTRA_RESOURCE_META_LIST:Ljava/lang/String; = "resourceMetaList"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field public static final NOTEID:Ljava/lang/String; = "noteid"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final USERNAME:Ljava/lang/String; = "username"

.field public static final WEB_CONTENT:Ljava/lang/String; = "web_conetnt"
