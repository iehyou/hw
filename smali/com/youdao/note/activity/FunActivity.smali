.class public Lcom/youdao/note/activity/FunActivity;
.super Landroid/app/Activity;
.source "FunActivity.java"


# static fields
.field private static final CHAR_SIZE:I = 0x28

.field private static final MESSAGE_NEXT:I = 0x1

.field private static final mCheckInterval:J = 0x32L

.field private static final mScale:[I = null

.field private static final mText:Ljava/lang/String; = "\u5176\u5b9e\u8fd9\u4e16\u4e0a\u6700\u6050\u6016\u7684\uff0c\u4e0d\u662f\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\uff0c\u800c\u662f\u5728\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\u7684\u65f6\u5019\uff0c\u4f60\u4f38\u51fa\u4e86\u624b\uff0c\u53d1\u73b0\u4e0a\u9762\u8d6b\u7136\u957f\u7740\u516d\u4e2a\u6307\u5934\u3002\u2014\u2014\u300a\u5450\u558a\u4e0d\u8d77\u6765\u300b"


# instance fields
.field private mCanceled:Z

.field private mCharPerLine:I

.field private mChars:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mContainer:Landroid/view/ViewGroup;

.field private mCurrentCharIdx:I

.field private mHandler:Landroid/os/Handler;

.field private mLastTouchTime:J

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/youdao/note/activity/FunActivity;->mScale:[I

    return-void

    :array_0
    .array-data 0x4
        0x14t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
        0xct 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mContainer:Landroid/view/ViewGroup;

    .line 56
    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mWidth:I

    .line 58
    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mCharPerLine:I

    .line 60
    iput-boolean v1, p0, Lcom/youdao/note/activity/FunActivity;->mCanceled:Z

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "\u5176\u5b9e\u8fd9\u4e16\u4e0a\u6700\u6050\u6016\u7684\uff0c\u4e0d\u662f\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\uff0c\u800c\u662f\u5728\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\u7684\u65f6\u5019\uff0c\u4f60\u4f38\u51fa\u4e86\u624b\uff0c\u53d1\u73b0\u4e0a\u9762\u8d6b\u7136\u957f\u7740\u516d\u4e2a\u6307\u5934\u3002\u2014\u2014\u300a\u5450\u558a\u4e0d\u8d77\u6765\u300b"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mChars:Ljava/util/List;

    .line 64
    new-instance v0, Lcom/youdao/note/activity/FunActivity$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/FunActivity$1;-><init>(Lcom/youdao/note/activity/FunActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mHandler:Landroid/os/Handler;

    .line 142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/activity/FunActivity;->mLastTouchTime:J

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/FunActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I

    return v0
.end method

.method static synthetic access$008(Lcom/youdao/note/activity/FunActivity;)I
    .locals 2
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I

    return v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/FunActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/youdao/note/activity/FunActivity;->mCanceled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/FunActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/FunActivity;->mCharPerLine:I

    return v0
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/FunActivity;)Landroid/view/ViewGroup;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/FunActivity;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mChars:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/FunActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/FunActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    .line 129
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/FunActivity;->requestWindowFeature(I)Z

    .line 130
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 131
    const v1, 0x7f030011

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/FunActivity;->setContentView(I)V

    .line 132
    invoke-virtual {p0}, Lcom/youdao/note/activity/FunActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 133
    .local v0, display:Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mWidth:I

    .line 134
    iget v1, p0, Lcom/youdao/note/activity/FunActivity;->mWidth:I

    div-int/lit8 v1, v1, 0x28

    iput v1, p0, Lcom/youdao/note/activity/FunActivity;->mCharPerLine:I

    .line 135
    const v1, 0x7f07002f

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/FunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/youdao/note/activity/FunActivity;->mContainer:Landroid/view/ViewGroup;

    .line 136
    iget-object v1, p0, Lcom/youdao/note/activity/FunActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/youdao/note/activity/FunActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 137
    const-string v1, "Char"

    const-string v2, "message send"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 176
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/FunActivity;->mCanceled:Z

    .line 178
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 182
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/FunActivity;->mCanceled:Z

    .line 185
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .parameter "event"

    .prologue
    const/4 v12, 0x1

    const/high16 v11, 0x4220

    const/4 v10, 0x0

    .line 145
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 146
    const-string v6, "Char"

    const-string v7, "up"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v6, p0, Lcom/youdao/note/activity/FunActivity;->mChars:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 148
    .local v3, view:Landroid/widget/TextView;
    invoke-virtual {v3, v10, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 151
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #view:Landroid/widget/TextView;
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/youdao/note/activity/FunActivity;->mLastTouchTime:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x32

    cmp-long v6, v6, v8

    if-gez v6, :cond_2

    .line 171
    :cond_1
    :goto_1
    return v10

    .line 154
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 155
    .local v4, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 156
    .local v5, y:F
    const-string v6, "Char"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const/4 v6, 0x2

    new-array v2, v6, [I

    .line 158
    .local v2, location:[I
    iget-object v6, p0, Lcom/youdao/note/activity/FunActivity;->mChars:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 159
    .restart local v3       #view:Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 160
    aget v6, v2, v10

    int-to-float v6, v6

    sub-float v6, v4, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    div-float/2addr v6, v11

    aget v7, v2, v12

    int-to-float v7, v7

    sub-float v7, v5, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    div-float/2addr v7, v11

    add-float/2addr v6, v7

    float-to-int v0, v6

    .line 162
    .local v0, dis:I
    sget-object v6, Lcom/youdao/note/activity/FunActivity;->mScale:[I

    array-length v6, v6

    if-ge v0, v6, :cond_3

    .line 163
    sget-object v6, Lcom/youdao/note/activity/FunActivity;->mScale:[I

    aget v6, v6, v0

    add-int/lit8 v6, v6, 0x28

    int-to-float v6, v6

    invoke-virtual {v3, v10, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_2

    .line 165
    :cond_3
    invoke-virtual {v3, v10, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_2

    .line 169
    .end local v0           #dis:I
    .end local v3           #view:Landroid/widget/TextView;
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/youdao/note/activity/FunActivity;->mLastTouchTime:J

    goto :goto_1
.end method
