.class Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;
.super Landroid/webkit/WebViewClient;
.source "WebActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/WebActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageLoadWebClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/WebActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/WebActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    iput-object p1, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .parameter "view"
    .parameter "url"

    .prologue
    const/4 v3, 0x1

    .line 145
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Page loaded "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    const-string v0, "http://m.note.youdao.com/soft-manager/feedback?soft=note"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    invoke-static {v0}, Lcom/youdao/note/activity/WebActivity;->access$208(Lcom/youdao/note/activity/WebActivity;)I

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mFeedBackLoadTimes:I
    invoke-static {v0}, Lcom/youdao/note/activity/WebActivity;->access$200(Lcom/youdao/note/activity/WebActivity;)I

    move-result v0

    if-le v0, v3, :cond_1

    .line 150
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/youdao/note/activity/WebActivity;->access$300(Lcom/youdao/note/activity/WebActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/WebActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 153
    return-void
.end method
