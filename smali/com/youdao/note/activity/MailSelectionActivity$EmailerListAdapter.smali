.class Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MailSelectionActivity.java"

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/MailSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EmailerListAdapter"
.end annotation


# instance fields
.field private alphabetPosMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private isSearch:Z

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/activity/MailSelectionActivity$ListItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mInflater:Landroid/view/LayoutInflater;

.field private sections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/youdao/note/activity/MailSelectionActivity;

.field private typeCount:I


# direct methods
.method public constructor <init>(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/util/List;Landroid/content/Context;Z)V
    .locals 11
    .parameter
    .parameter
    .parameter "context"
    .parameter "isSearch"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/activity/MailSelectionActivity$Emailer;",
            ">;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, datas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/activity/MailSelectionActivity$Emailer;>;"
    const/4 v10, 0x0

    const/4 v8, 0x1

    .line 386
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 387
    iput-boolean p4, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->isSearch:Z

    .line 388
    if-eqz p4, :cond_1

    move v7, v8

    :goto_0
    iput v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->typeCount:I

    .line 389
    const-string v7, "layout_inflater"

    invoke-virtual {p3, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    iput-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 391
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->itemList:Ljava/util/List;

    .line 392
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    .line 393
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->alphabetPosMap:Ljava/util/Map;

    .line 394
    const-string v0, ""

    .line 395
    .local v0, currentIndex:Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, position:I
    const/4 v6, 0x0

    .line 396
    .local v6, section:I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    .line 397
    .local v1, e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    if-nez p4, :cond_0

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->index:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$500(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 398
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->index:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$500(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v0

    .line 399
    new-instance v3, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    invoke-direct {v3, p1, v10}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;Lcom/youdao/note/activity/MailSelectionActivity$1;)V

    .line 400
    .local v3, item:Lcom/youdao/note/activity/MailSelectionActivity$ListItem;
    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->index:Ljava/lang/String;
    invoke-static {v3, v0}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1402(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;Ljava/lang/String;)Ljava/lang/String;

    .line 401
    const/4 v7, 0x0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->type:I
    invoke-static {v3, v7}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1502(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;I)I

    .line 402
    iget-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->itemList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    new-instance v4, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v4, v7, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 404
    .local v4, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    iget-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->alphabetPosMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    add-int/lit8 v5, v5, 0x1

    .line 407
    add-int/lit8 v6, v6, 0x1

    .line 409
    .end local v3           #item:Lcom/youdao/note/activity/MailSelectionActivity$ListItem;
    .end local v4           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_0
    new-instance v3, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    invoke-direct {v3, p1, v10}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;Lcom/youdao/note/activity/MailSelectionActivity$1;)V

    .line 410
    .restart local v3       #item:Lcom/youdao/note/activity/MailSelectionActivity$ListItem;
    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->type:I
    invoke-static {v3, v8}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1502(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;I)I

    .line 411
    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    invoke-static {v3, v1}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1602(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    .line 412
    iget-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->itemList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 413
    add-int/lit8 v5, v5, 0x1

    .line 414
    goto :goto_1

    .line 388
    .end local v0           #currentIndex:Ljava/lang/String;
    .end local v1           #e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #item:Lcom/youdao/note/activity/MailSelectionActivity$ListItem;
    .end local v5           #position:I
    .end local v6           #section:I
    :cond_1
    const/4 v7, 0x2

    goto/16 :goto_0

    .line 415
    .restart local v0       #currentIndex:Ljava/lang/String;
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v5       #position:I
    .restart local v6       #section:I
    :cond_2
    return-void
.end method

.method static synthetic access$1700(Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;Landroid/widget/CompoundButton;ZI)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 378
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->checkBoxChanged(Landroid/widget/CompoundButton;ZI)V

    return-void
.end method

.method private checkBoxChanged(Landroid/widget/CompoundButton;ZI)V
    .locals 3
    .parameter "buttonView"
    .parameter "selected"
    .parameter "position"

    .prologue
    .line 485
    invoke-virtual {p0, p3}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v1

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1600(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    move-result-object v0

    .line 487
    .local v0, emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->getSelectedCount()I
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity;->access$1800(Lcom/youdao/note/activity/MailSelectionActivity;)I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 488
    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/MailSelectionActivity;->showDialog(I)V

    .line 489
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 495
    :goto_0
    return-void

    .line 492
    :cond_0
    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z
    invoke-static {v0, p2}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$1202(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Z)Z

    .line 493
    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->updateButtonText()V
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity;->access$1900(Lcom/youdao/note/activity/MailSelectionActivity;)V

    .line 494
    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->hideSoftKeyboard()V
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity;->access$600(Lcom/youdao/note/activity/MailSelectionActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public getAlphabetByPosition(I)Ljava/lang/String;
    .locals 5
    .parameter "position"

    .prologue
    .line 423
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getSectionForPosition(I)I

    move-result v2

    .line 424
    .local v2, section:I
    iget-object v3, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 425
    .local v1, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 426
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v3

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->index:Ljava/lang/String;
    invoke-static {v3}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1400(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)Ljava/lang/String;

    move-result-object v3

    .line 429
    .end local v1           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->itemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;
    .locals 1
    .parameter "position"

    .prologue
    .line 504
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->itemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 378
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 509
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .parameter "position"

    .prologue
    .line 476
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v0

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->type:I
    invoke-static {v0}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1500(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)I

    move-result v0

    return v0
.end method

.method public getPositionByAlphabet(Ljava/lang/String;)I
    .locals 2
    .parameter "index"

    .prologue
    .line 418
    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->alphabetPosMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 419
    .local v0, pos:Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 4
    .parameter "section"

    .prologue
    .line 527
    iget-object v2, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 528
    .local v1, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 529
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 532
    .end local v1           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 4
    .parameter "position"

    .prologue
    .line 537
    const/4 v2, 0x0

    .line 538
    .local v2, section:I
    iget-object v3, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 539
    .local v1, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt p1, v3, :cond_0

    .line 540
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 545
    .end local v1           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_0
    return v2
.end method

.method public bridge synthetic getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getSections()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSections()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 514
    iget-boolean v5, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->isSearch:Z

    if-eqz v5, :cond_1

    .line 515
    const/4 v5, 0x0

    new-array v4, v5, [Ljava/lang/String;

    .line 522
    :cond_0
    return-object v4

    .line 517
    :cond_1
    iget-object v5, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    .line 518
    .local v4, ret:[Ljava/lang/String;
    const/4 v0, 0x0

    .line 519
    .local v0, i:I
    iget-object v5, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->sections:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 520
    .local v3, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    add-int/lit8 v1, v0, 0x1

    .end local v0           #i:I
    .local v1, i:I
    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getPositionForSection(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v5

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->index:Ljava/lang/String;
    invoke-static {v5}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1400(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    move v0, v1

    .end local v1           #i:I
    .restart local v0       #i:I
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v8, 0x0

    .line 434
    const/4 v5, 0x0

    .line 435
    .local v5, view:Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItemViewType(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 436
    if-eqz p2, :cond_0

    .line 437
    move-object v5, p2

    .line 441
    :goto_0
    const v6, 0x7f070070

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 442
    .local v0, cb:Landroid/widget/CheckBox;
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 443
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v6

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    invoke-static {v6}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1600(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    move-result-object v1

    .line 444
    .local v1, emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    const v6, 0x7f070072

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 445
    .local v4, tv_name:Landroid/widget/TextView;
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->name:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    const v6, 0x7f070073

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 447
    .local v3, tv_address:Landroid/widget/TextView;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->type:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$300(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->address:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$100(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$1200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 449
    new-instance v6, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter$1;

    invoke-direct {v6, p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter$1;-><init>(Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;I)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 455
    new-instance v6, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter$2;

    invoke-direct {v6, p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter$2;-><init>(Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 471
    .end local v0           #cb:Landroid/widget/CheckBox;
    .end local v1           #emailer:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    .end local v3           #tv_address:Landroid/widget/TextView;
    .end local v4           #tv_name:Landroid/widget/TextView;
    :goto_1
    return-object v5

    .line 439
    :cond_0
    iget-object v6, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030028

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    goto :goto_0

    .line 463
    :cond_1
    if-eqz p2, :cond_2

    .line 464
    move-object v5, p2

    .line 468
    :goto_2
    const v6, 0x7f070074

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 469
    .local v2, tv:Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getItem(I)Lcom/youdao/note/activity/MailSelectionActivity$ListItem;

    move-result-object v6

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->index:Ljava/lang/String;
    invoke-static {v6}, Lcom/youdao/note/activity/MailSelectionActivity$ListItem;->access$1400(Lcom/youdao/note/activity/MailSelectionActivity$ListItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 466
    .end local v2           #tv:Landroid/widget/TextView;
    :cond_2
    iget-object v6, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030029

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->typeCount:I

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 555
    return-void
.end method
