.class Lcom/youdao/note/activity/BaseEditActivity$2;
.super Landroid/os/AsyncTask;
.source "BaseEditActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/BaseEditActivity;->saveNote()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/BaseEditActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/BaseEditActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 280
    iput-object p1, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 10
    .parameter "params"

    .prologue
    const/4 v9, 0x1

    .line 284
    const/4 v5, 0x1

    .line 286
    .local v5, succeed:Z
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v6}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 287
    .local v3, noteMeta:Lcom/youdao/note/data/NoteMeta;
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    #calls: Lcom/youdao/note/activity/BaseEditActivity;->appendResources()V
    invoke-static {v6}, Lcom/youdao/note/activity/BaseEditActivity;->access$000(Lcom/youdao/note/activity/BaseEditActivity;)V

    .line 289
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v6}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x64

    invoke-static {v6, v7}, Lcom/youdao/note/utils/HTMLUtils;->extractSummary(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/youdao/note/data/NoteMeta;->setSummary(Ljava/lang/String;)V

    .line 290
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/youdao/note/data/NoteMeta;->setNoteBook(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v6

    if-gtz v6, :cond_0

    .line 298
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V

    .line 301
    :cond_0
    invoke-virtual {v3, v9}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/youdao/note/data/NoteMeta;->setModifyTime(J)V

    .line 303
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v6}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    int-to-long v6, v6

    invoke-virtual {v3, v6, v7}, Lcom/youdao/note/data/NoteMeta;->setLength(J)V

    .line 309
    const-string v6, "youdao_note_debug_19880222"

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 310
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    iget-object v7, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v7, v7, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v7}, Lcom/youdao/note/LogRecorder;->dumpStatistics()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 312
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "edited note meta is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 313
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 314
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v6, v2}, Lcom/youdao/note/datasource/DataSource;->existResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 317
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v6, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v4

    .line 319
    .local v4, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<*>;"
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v6, v4}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 320
    const/4 v5, 0x0

    goto :goto_0

    .line 323
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v4           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<*>;"
    :cond_3
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    #calls: Lcom/youdao/note/activity/BaseEditActivity;->generateSnippet(Lcom/youdao/note/data/NoteMeta;)V
    invoke-static {v6, v3}, Lcom/youdao/note/activity/BaseEditActivity;->access$100(Lcom/youdao/note/activity/BaseEditActivity;Lcom/youdao/note/data/NoteMeta;)V

    .line 324
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "note content is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v7, v7, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v7}, Lcom/youdao/note/data/Note;->getHtml()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v6}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 326
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    const-string v7, "&nbsp;"

    invoke-virtual {v6, v7}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 329
    :cond_4
    :try_start_0
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v7, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v7, v7, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    iget-object v8, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v8, v8, Lcom/youdao/note/activity/BaseEditActivity;->oriNoteBookId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    and-int/2addr v5, v6

    .line 340
    if-eqz v5, :cond_6

    .line 341
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 343
    :goto_1
    return-object v6

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, e:Ljava/io/IOException;
    const-string v6, "No space left on device"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 332
    const-string v6, "No space left on device."

    invoke-static {p0, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1

    .line 335
    :cond_5
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1

    .line 337
    .end local v0           #e:Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 338
    .local v0, e:Ljava/lang/Exception;
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1

    .line 343
    .end local v0           #e:Ljava/lang/Exception;
    :cond_6
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 280
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 349
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->dismissDialog(I)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity$2;->this$0:Lcom/youdao/note/activity/BaseEditActivity;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->finishEdit(I)V

    .line 353
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 280
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity$2;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
