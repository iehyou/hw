.class Lcom/youdao/note/activity/NoteDetailActivity$1;
.super Ljava/lang/Object;
.source "NoteDetailActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteDetailActivity;->initUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteDetailActivity;

.field final synthetic val$frameLayout:Landroid/view/View;

.field final synthetic val$guide:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iput-object p2, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->val$guide:Landroid/view/View;

    iput-object p3, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->val$frameLayout:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isShowMailGuide()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->val$guide:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->val$frameLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #getter for: Lcom/youdao/note/activity/NoteDetailActivity;->lastWidth:I
    invoke-static {v1}, Lcom/youdao/note/activity/NoteDetailActivity;->access$000(Lcom/youdao/note/activity/NoteDetailActivity;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->showMailGuide()V
    invoke-static {v0}, Lcom/youdao/note/activity/NoteDetailActivity;->access$100(Lcom/youdao/note/activity/NoteDetailActivity;)V

    .line 132
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$1;->val$frameLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    #setter for: Lcom/youdao/note/activity/NoteDetailActivity;->lastWidth:I
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->access$002(Lcom/youdao/note/activity/NoteDetailActivity;I)I

    .line 134
    :cond_0
    return-void
.end method
