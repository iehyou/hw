.class public Lcom/youdao/note/activity/SearchActivity;
.super Lcom/youdao/note/activity/BaseListFileActivity;
.source "SearchActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/SearchActivity$SearchAdapter;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEYWORD:Ljava/lang/String; = "keyword"

.field private static final EXTRA_NOTES:Ljava/lang/String; = "extra_notes"

.field private static final QUERY_LOADING:I = 0x1


# instance fields
.field private EMPTY_TEMPLATE:Ljava/lang/String;

.field private clearButton:Landroid/widget/ImageButton;

.field private localSearcher:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            "Lcom/youdao/note/data/ListNoteMetas;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptyText:Landroid/widget/TextView;

.field private mKeyWord:Ljava/lang/String;

.field private mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

.field private mQueryDialog:Landroid/app/ProgressDialog;

.field mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTextView:Landroid/widget/AutoCompleteTextView;

.field private showingHistory:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseListFileActivity;-><init>()V

    .line 75
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    .line 79
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    .line 81
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mEmptyText:Landroid/widget/TextView;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/SearchActivity;->showingHistory:Z

    .line 91
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;

    .line 93
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    .line 95
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->localSearcher:Landroid/os/AsyncTask;

    .line 97
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->EMPTY_TEMPLATE:Ljava/lang/String;

    .line 273
    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    .line 436
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/SearchActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/youdao/note/activity/SearchActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 72
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/SearchActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/SearchActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    return-void
.end method

.method private checkUpdateSearchHistryDb()V
    .locals 2

    .prologue
    .line 123
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->getAllSearchHistoryList()Ljava/util/List;

    move-result-object v0

    .line 124
    .local v0, searchList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 125
    new-instance v1, Lcom/youdao/note/activity/SearchActivity$1;

    invoke-direct {v1, p0, v0}, Lcom/youdao/note/activity/SearchActivity$1;-><init>(Lcom/youdao/note/activity/SearchActivity;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/youdao/note/activity/SearchActivity$1;->start()V

    .line 140
    :cond_0
    return-void
.end method

.method private doLocalSearch()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 253
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->localSearcher:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->localSearcher:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    const v0, 0x7f0a0052

    invoke-static {p0, v0}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 259
    :cond_1
    new-instance v0, Lcom/youdao/note/activity/SearchActivity$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/SearchActivity$2;-><init>(Lcom/youdao/note/activity/SearchActivity;)V

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/SearchActivity$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->localSearcher:Landroid/os/AsyncTask;

    .line 271
    return-void
.end method

.method private performSearch()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 235
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    invoke-static {v1}, Lcom/youdao/note/utils/StringUtils;->isBlank(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 237
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->addSearchTimes()Z

    .line 238
    new-instance v0, Landroid/provider/SearchRecentSuggestions;

    sget-object v1, Lcom/youdao/note/provider/SearchSuggestProvider;->AUTHORITY:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v3}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 240
    .local v0, suggestions:Landroid/provider/SearchRecentSuggestions;
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->saveSearchQuery(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/SearchActivity;->showDialog(I)V

    .line 244
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v2, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/youdao/note/task/TaskManager;->searchNotes(Ljava/lang/String;)V

    .line 250
    .end local v0           #suggestions:Landroid/provider/SearchRecentSuggestions;
    :cond_0
    :goto_0
    return-void

    .line 247
    .restart local v0       #suggestions:Landroid/provider/SearchRecentSuggestions;
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->doLocalSearch()V

    goto :goto_0
.end method

.method private renderSearchResult()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 346
    iget-boolean v3, p0, Lcom/youdao/note/activity/SearchActivity;->showingHistory:Z

    if-eqz v3, :cond_0

    .line 347
    const v3, 0x7f070059

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 348
    .local v1, labelView:Landroid/view/View;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 349
    iput-boolean v7, p0, Lcom/youdao/note/activity/SearchActivity;->showingHistory:Z

    .line 351
    .end local v1           #labelView:Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {v3}, Lcom/youdao/note/data/ListNoteMetas;->getFileMetas()[Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    array-length v3, v3

    if-nez v3, :cond_1

    .line 352
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/SearchActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 353
    iget-object v3, p0, Lcom/youdao/note/activity/SearchActivity;->mEmptyText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/youdao/note/activity/SearchActivity;->EMPTY_TEMPLATE:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    :goto_0
    const-string v3, "Search result got"

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 360
    return-void

    .line 355
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 356
    .local v2, resources:Landroid/content/res/Resources;
    new-instance v0, Lcom/youdao/note/data/adapter/SectionAdapter;

    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {v4}, Lcom/youdao/note/data/ListNoteMetas;->getFileMetas()[Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    const v5, 0x7f0a0042

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a0043

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/youdao/note/data/adapter/SectionAdapter;-><init>(Landroid/view/LayoutInflater;[Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    .local v0, adapter:Lcom/youdao/note/data/adapter/SectionAdapter;
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/SearchActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method private setAndFilterSearchResult(Lcom/youdao/note/data/ListNoteMetas;)V
    .locals 8
    .parameter "data"

    .prologue
    .line 324
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    .line 325
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v5, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {v6}, Lcom/youdao/note/data/ListNoteMetas;->getFileMetas()[Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    .local v0, arr$:[Lcom/youdao/note/data/NoteMeta;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 327
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 328
    .local v3, newNoteMeta:Lcom/youdao/note/data/NoteMeta;
    if-eqz v3, :cond_0

    .line 329
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 332
    .end local v3           #newNoteMeta:Lcom/youdao/note/data/NoteMeta;
    .end local v4           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_1
    sget-object v6, Lcom/youdao/note/data/NoteMeta;->TIME_REVERSE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v5, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 333
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {v6}, Lcom/youdao/note/data/ListNoteMetas;->getTotalNumber()I

    move-result v6

    iget-object v7, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {v7}, Lcom/youdao/note/data/ListNoteMetas;->getFileMetas()[Lcom/youdao/note/data/NoteMeta;

    move-result-object v7

    array-length v7, v7

    if-ne v6, v7, :cond_2

    .line 334
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/data/ListNoteMetas;->setTotalNumber(I)V

    .line 336
    :cond_2
    iget-object v7, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    const/4 v6, 0x0

    new-array v6, v6, [Lcom/youdao/note/data/NoteMeta;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v7, v6}, Lcom/youdao/note/data/ListNoteMetas;->setFileMetas([Lcom/youdao/note/data/NoteMeta;)V

    .line 337
    return-void
.end method

.method private setHistoryAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .parameter "listAdapter"

    .prologue
    .line 143
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mEmptyText:Landroid/widget/TextView;

    const v1, 0x7f0a0055

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 145
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/SearchActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SearchActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 383
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 387
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 374
    return-void
.end method

.method protected bindControlls()V
    .locals 2

    .prologue
    .line 153
    const v1, 0x7f0700ad

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 154
    .local v0, view:Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, p0}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 156
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    return-void
.end method

.method public getAllSearchHistory()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 405
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/youdao/note/provider/SearchSuggestProvider;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/suggestions"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getAllSearchHistoryList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 410
    .local v2, historySet:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getAllSearchHistory()Landroid/database/Cursor;

    move-result-object v0

    .line 412
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 413
    .local v3, queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v1, v0}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 414
    .local v1, helper:Lcom/youdao/note/utils/CursorHelper;
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 415
    const-string v4, "query"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 416
    const-string v4, "query"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    const-string v4, "query"

    invoke-virtual {v1, v4}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 423
    .end local v1           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v3           #queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v4

    .line 420
    .restart local v1       #helper:Lcom/youdao/note/utils/CursorHelper;
    .restart local v3       #queryHistory:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    invoke-static {v3}, Ljava/util/Collections;->reverse(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 428
    if-nez p1, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-direct {p0, v0}, Lcom/youdao/note/activity/SearchActivity;->setAndFilterSearchResult(Lcom/youdao/note/data/ListNoteMetas;)V

    .line 430
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->renderSearchResult()V

    .line 434
    :goto_0
    return-void

    .line 432
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/BaseListFileActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 395
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070068

    if-ne v0, v1, :cond_1

    .line 396
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0700ad

    if-ne v0, v1, :cond_0

    .line 398
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 399
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 100
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->requestWindowFeature(I)Z

    .line 101
    const v1, 0x7f030045

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->setContentView(I)V

    .line 102
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->checkUpdateSearchHistryDb()V

    .line 104
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 105
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 106
    const v1, 0x7f070067

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    .line 107
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;-><init>(Lcom/youdao/note/activity/SearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 109
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, p0}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 110
    const v1, 0x7f070068

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->clearButton:Landroid/widget/ImageButton;

    .line 111
    const v1, 0x7f07006d

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mEmptyText:Landroid/widget/TextView;

    .line 113
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->EMPTY_TEMPLATE:Ljava/lang/String;

    .line 114
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f030046

    const v3, 0x7f0700ab

    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getAllSearchHistoryList()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, p0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;

    .line 117
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-direct {p0, v1}, Lcom/youdao/note/activity/SearchActivity;->setHistoryAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->bindControlls()V

    .line 119
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    .line 120
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 277
    packed-switch p1, :pswitch_data_0

    .line 292
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 279
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    .line 280
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 282
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 283
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/youdao/note/activity/SearchActivity$3;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/SearchActivity$3;-><init>(Lcom/youdao/note/activity/SearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 289
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 229
    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 230
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f050004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 231
    const/4 v1, 0x1

    return v1
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 364
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 366
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    .line 367
    const/4 v0, 0x1

    .line 369
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-boolean v0, p0, Lcom/youdao/note/activity/SearchActivity;->showingHistory:Z

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 163
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/youdao/note/activity/BaseListFileActivity;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 533
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "on key down called. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 534
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/youdao/note/activity/SearchActivity;->onSearchRequested()Z

    .line 536
    const/4 v0, 0x1

    .line 538
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseListFileActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700c1

    if-ne v2, v3, :cond_0

    .line 211
    new-instance v0, Landroid/provider/SearchRecentSuggestions;

    sget-object v2, Lcom/youdao/note/provider/SearchSuggestProvider;->AUTHORITY:Ljava/lang/String;

    invoke-direct {v0, p0, v2, v1}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 213
    .local v0, suggestions:Landroid/provider/SearchRecentSuggestions;
    invoke-virtual {v0}, Landroid/provider/SearchRecentSuggestions;->clearHistory()V

    .line 215
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/youdao/note/activity/SearchActivity;->setHistoryAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    .end local v0           #suggestions:Landroid/provider/SearchRecentSuggestions;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/youdao/note/activity/SearchActivity;->showingHistory:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryHistoryAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 189
    const-string v1, "keyword"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 190
    .local v0, s:Ljava/io/Serializable;
    if-eqz v0, :cond_0

    .line 191
    check-cast v0, Ljava/lang/String;

    .end local v0           #s:Ljava/io/Serializable;
    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 193
    :cond_0
    const-string v1, "extra_notes"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 194
    .restart local v0       #s:Ljava/io/Serializable;
    if-eqz v0, :cond_1

    .line 195
    check-cast v0, Lcom/youdao/note/data/ListNoteMetas;

    .end local v0           #s:Ljava/io/Serializable;
    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    .line 196
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->renderSearchResult()V

    .line 199
    :cond_1
    const-string v1, "onRestoreInstanceState called."

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->onResume()V

    .line 206
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "bundle"

    .prologue
    .line 176
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    if-eqz v0, :cond_0

    .line 177
    const-string v0, "extra_notes"

    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mListNoteMetas:Lcom/youdao/note/data/ListNoteMetas;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "keyword"

    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_1
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 183
    const-string v0, "onSaveInstanceState called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mKeyWord:Ljava/lang/String;

    .line 341
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->performSearch()V

    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 171
    invoke-super {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->onStart()V

    .line 172
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 378
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 1
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 297
    packed-switch p1, :pswitch_data_0

    .line 317
    .end local p2
    :goto_0
    return-void

    .line 299
    .restart local p2
    :pswitch_0
    const-string v0, "Search result got."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mQueryDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/SearchActivity;->dismissDialog(I)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 304
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity;->mTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/SearchActivity;->hideKeyboard(Landroid/os/IBinder;)V

    .line 305
    if-eqz p3, :cond_1

    .line 306
    check-cast p2, Lcom/youdao/note/data/ListNoteMetas;

    .end local p2
    invoke-direct {p0, p2}, Lcom/youdao/note/activity/SearchActivity;->setAndFilterSearchResult(Lcom/youdao/note/data/ListNoteMetas;)V

    .line 307
    invoke-direct {p0}, Lcom/youdao/note/activity/SearchActivity;->renderSearchResult()V

    goto :goto_0

    .line 309
    .restart local p2
    :cond_1
    const v0, 0x7f0a007d

    invoke-static {p0, v0}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
