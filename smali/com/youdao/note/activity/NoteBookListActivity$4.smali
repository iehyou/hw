.class Lcom/youdao/note/activity/NoteBookListActivity$4;
.super Ljava/lang/Object;
.source "NoteBookListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteBookListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteBookListActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteBookListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 258
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 262
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$500(Lcom/youdao/note/activity/NoteBookListActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    #setter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$202(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 263
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/activity/NoteBookListActivity;->access$200(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    const v1, 0x7f0a0014

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$200(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/youdao/note/activity/NoteBookListActivity;->checkNotebookNameExist(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v0, v1, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$600(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v0}, Lcom/youdao/note/activity/NoteBookListActivity;->access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$500(Lcom/youdao/note/activity/NoteBookListActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setTitle(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v0}, Lcom/youdao/note/activity/NoteBookListActivity;->access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteBook;->setDirty(Z)V

    .line 270
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    .line 271
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$4;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #calls: Lcom/youdao/note/activity/NoteBookListActivity;->update()V
    invoke-static {v0}, Lcom/youdao/note/activity/NoteBookListActivity;->access$400(Lcom/youdao/note/activity/NoteBookListActivity;)V

    goto :goto_0
.end method
