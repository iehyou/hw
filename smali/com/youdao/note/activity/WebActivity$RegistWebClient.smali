.class Lcom/youdao/note/activity/WebActivity$RegistWebClient;
.super Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;
.source "WebActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/WebActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegistWebClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/WebActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/WebActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 156
    iput-object p1, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/WebActivity$RegistWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;)V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6
    .parameter "view"
    .parameter "url"

    .prologue
    const/4 v5, 0x1

    .line 159
    const-string v2, "http://note.elibom.youdao.com/noteproxy/login"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 160
    invoke-static {}, Lcom/youdao/note/activity/WebActivity;->access$400()Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 161
    .local v1, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    #setter for: Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/youdao/note/activity/WebActivity;->access$502(Lcom/youdao/note/activity/WebActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 163
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    #setter for: Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/youdao/note/activity/WebActivity;->access$602(Lcom/youdao/note/activity/WebActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 164
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got user name and password "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/youdao/note/activity/WebActivity;->access$500(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;
    invoke-static {v4}, Lcom/youdao/note/activity/WebActivity;->access$600(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    .end local v1           #matcher:Ljava/util/regex/Matcher;
    :cond_0
    const-string v2, "http://note.elibom.youdao.com/noteproxy/list"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "http://note.elibom.youdao.com/noteproxy/login"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    const-string v3, "finsh regist."

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 172
    .local v0, data:Landroid/content/Intent;
    const-string v2, "username"

    iget-object v3, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;
    invoke-static {v3}, Lcom/youdao/note/activity/WebActivity;->access$500(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v2, "password"

    iget-object v3, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;
    invoke-static {v3}, Lcom/youdao/note/activity/WebActivity;->access$600(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v0}, Lcom/youdao/note/activity/WebActivity;->setResult(ILandroid/content/Intent;)V

    .line 175
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    invoke-virtual {v2}, Lcom/youdao/note/activity/WebActivity;->finish()V

    .line 179
    .end local v0           #data:Landroid/content/Intent;
    :cond_2
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load url "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity$RegistWebClient;->this$0:Lcom/youdao/note/activity/WebActivity;

    #getter for: Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;
    invoke-static {v2}, Lcom/youdao/note/activity/WebActivity;->access$700(Lcom/youdao/note/activity/WebActivity;)Landroid/webkit/WebView;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 182
    return v5
.end method
