.class abstract Lcom/youdao/note/activity/PinlockActivity$BaseLocker;
.super Ljava/lang/Object;
.source "PinlockActivity.java"

# interfaces
.implements Lcom/youdao/note/activity/PinlockActivity$ILocker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/PinlockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseLocker"
.end annotation


# static fields
.field protected static final BLUE:I = 0x2

.field private static final BUNDLE_CODE_INPUT_TIMES:Ljava/lang/String; = "code_times"

.field private static final BUNDLE_CODE_STACK:Ljava/lang/String; = "code_stack"

.field private static final BUNDLE_CURRENT_CODES:Ljava/lang/String; = "current_codes"

.field private static final MESSAGE_INPUT_COMPLETE:I = 0x1

.field private static final PINCODE_SIZE:I = 0x4

.field protected static final RED:I = 0x1


# instance fields
.field private handler:Landroid/os/Handler;

.field private mCodeArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mCodeBox:Landroid/view/View;

.field private mCodeInputTimes:I

.field protected mCodeStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentCodes:I

.field private mKeyboard:Landroid/view/View;

.field private mLabel:Landroid/widget/TextView;

.field private mPinLabelBg:Landroid/view/View;

.field private mPreviousCodes:I

.field private mSubLabel:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/youdao/note/activity/PinlockActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/PinlockActivity;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 111
    iput-object p1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    .line 125
    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPreviousCodes:I

    .line 127
    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    .line 129
    iput-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mLabel:Landroid/widget/TextView;

    .line 131
    iput-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mSubLabel:Landroid/widget/TextView;

    .line 133
    iput-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPinLabelBg:Landroid/view/View;

    .line 135
    iput-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeBox:Landroid/view/View;

    .line 137
    iput-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mKeyboard:Landroid/view/View;

    .line 145
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker$1;-><init>(Lcom/youdao/note/activity/PinlockActivity$BaseLocker;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->handler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;)V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    .line 245
    const/4 v2, 0x0

    iput v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    .line 246
    iget-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 247
    .local v1, textView:Landroid/widget/TextView;
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 249
    .end local v1           #textView:Landroid/widget/TextView;
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->clear()V

    .line 250
    return-void
.end method

.method public getCodeInputTimes()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    return v0
.end method

.method protected getCurrentCodes()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    return v0
.end method

.method protected getPreviousCodes()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPreviousCodes:I

    return v0
.end method

.method protected hideCodeBox()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeBox:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    return-void
.end method

.method protected hideKeyboard()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mKeyboard:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    return-void
.end method

.method public initContentView()V
    .locals 13

    .prologue
    const v12, 0x7f0700a1

    const v11, 0x7f0700a0

    .line 161
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v10, 0x7f03003e

    invoke-virtual {v9, v10}, Lcom/youdao/note/activity/PinlockActivity;->setContentView(I)V

    .line 162
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v10, 0x7f07005a

    invoke-virtual {v9, v10}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mLabel:Landroid/widget/TextView;

    .line 163
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v10, 0x7f07009e

    invoke-virtual {v9, v10}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mSubLabel:Landroid/widget/TextView;

    .line 164
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-virtual {v9, v11}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeBox:Landroid/view/View;

    .line 165
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-virtual {v9, v12}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mKeyboard:Landroid/view/View;

    .line 166
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v10, 0x7f07009d

    invoke-virtual {v9, v10}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPinLabelBg:Landroid/view/View;

    .line 167
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-virtual {v9, v12}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TableLayout;

    .line 168
    .local v6, keyboardTable:Landroid/widget/TableLayout;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    invoke-virtual {v6}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v9

    if-ge v3, v9, :cond_5

    .line 169
    invoke-virtual {v6, v3}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TableRow;

    .line 170
    .local v7, row:Landroid/widget/TableRow;
    const/4 v4, 0x0

    .local v4, j:I
    :goto_1
    invoke-virtual {v7}, Landroid/widget/TableRow;->getChildCount()I

    move-result v9

    if-ge v4, v9, :cond_4

    .line 171
    invoke-virtual {v7, v4}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 172
    .local v5, key:Landroid/view/View;
    const/4 v0, 0x0

    .line 173
    .local v0, code:I
    const/4 v9, 0x3

    if-ne v3, v9, :cond_3

    .line 174
    if-nez v4, :cond_1

    .line 175
    const/4 v0, -0x1

    .line 184
    :cond_0
    :goto_2
    move v8, v0

    .line 185
    .local v8, tmpCode:I
    new-instance v9, Lcom/youdao/note/activity/PinlockActivity$BaseLocker$2;

    invoke-direct {v9, p0, v8}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker$2;-><init>(Lcom/youdao/note/activity/PinlockActivity$BaseLocker;I)V

    invoke-virtual {v5, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 176
    .end local v8           #tmpCode:I
    :cond_1
    const/4 v9, 0x1

    if-ne v4, v9, :cond_2

    .line 177
    const/4 v0, 0x0

    goto :goto_2

    .line 178
    :cond_2
    const/4 v9, 0x2

    if-ne v4, v9, :cond_0

    .line 179
    const/4 v0, -0x2

    goto :goto_2

    .line 182
    :cond_3
    mul-int/lit8 v9, v3, 0x3

    add-int/2addr v9, v4

    add-int/lit8 v0, v9, 0x1

    goto :goto_2

    .line 168
    .end local v0           #code:I
    .end local v5           #key:Landroid/view/View;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 194
    .end local v4           #j:I
    .end local v7           #row:Landroid/widget/TableRow;
    :cond_5
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-virtual {v9, v11}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 195
    .local v2, codeBoxLayout:Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v9

    if-ge v3, v9, :cond_7

    .line 196
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 197
    .local v1, codeBox:Landroid/widget/TextView;
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v9}, Ljava/util/Stack;->size()I

    move-result v9

    if-ge v3, v9, :cond_6

    .line 198
    const-string v9, "*"

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    :cond_6
    iget-object v9, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 202
    .end local v1           #codeBox:Landroid/widget/TextView;
    :cond_7
    return-void
.end method

.method public onInput(I)V
    .locals 4
    .parameter "code"

    .prologue
    const/4 v3, 0x4

    .line 206
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnInput called. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/PinlockActivity;->setResult(I)V

    .line 209
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-virtual {v0}, Lcom/youdao/note/activity/PinlockActivity;->finish()V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ge v0, v3, :cond_3

    .line 218
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 222
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected onLockCodeCompleted()V
    .locals 6

    .prologue
    .line 228
    iget v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPreviousCodes:I

    .line 229
    const/4 v3, 0x0

    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    .line 230
    iget-object v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 231
    .local v0, i:Ljava/lang/Integer;
    iget v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    iget v4, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    mul-int/lit8 v4, v4, 0xa

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    goto :goto_0

    .line 233
    .end local v0           #i:Ljava/lang/Integer;
    :cond_0
    iget-object v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 234
    .local v2, textView:Landroid/widget/TextView;
    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 236
    .end local v2           #textView:Landroid/widget/TextView;
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->clear()V

    .line 237
    iget v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    .line 238
    return-void
.end method

.method public restoreState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 295
    const-string v0, "code_times"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    .line 296
    const-string v0, "current_codes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    .line 297
    const-string v0, "code_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    .line 298
    return-void
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 288
    const-string v0, "code_times"

    iget v1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeInputTimes:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    const-string v0, "current_codes"

    iget v1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCurrentCodes:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 290
    const-string v0, "code_stack"

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mCodeStack:Ljava/util/Stack;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 291
    return-void
.end method

.method protected setLabel(I)V
    .locals 1
    .parameter "resid"

    .prologue
    .line 261
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 262
    return-void
.end method

.method protected setLabelBgColor(I)V
    .locals 2
    .parameter "color"

    .prologue
    .line 280
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPinLabelBg:Landroid/view/View;

    const v1, 0x7f0200c1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 285
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mPinLabelBg:Landroid/view/View;

    const v1, 0x7f0200c0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected setSubLabel(I)V
    .locals 2
    .parameter "resid"

    .prologue
    .line 265
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mSubLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 266
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mSubLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->mSubLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    :cond_0
    return-void
.end method
