.class Lcom/youdao/note/activity/ResourceListActivity$1;
.super Ljava/lang/Object;
.source "ResourceListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/ResourceListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/ResourceListActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/ResourceListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 129
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #getter for: Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/youdao/note/activity/ResourceListActivity;->access$100(Lcom/youdao/note/activity/ResourceListActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #getter for: Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I
    invoke-static {v2}, Lcom/youdao/note/activity/ResourceListActivity;->access$000(Lcom/youdao/note/activity/ResourceListActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 131
    .local v0, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #getter for: Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/youdao/note/activity/ResourceListActivity;->access$100(Lcom/youdao/note/activity/ResourceListActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #getter for: Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I
    invoke-static {v2}, Lcom/youdao/note/activity/ResourceListActivity;->access$000(Lcom/youdao/note/activity/ResourceListActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 132
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "to be removed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, v0}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 137
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #getter for: Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/youdao/note/activity/ResourceListActivity;->access$100(Lcom/youdao/note/activity/ResourceListActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    #calls: Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity()V
    invoke-static {v1}, Lcom/youdao/note/activity/ResourceListActivity;->access$200(Lcom/youdao/note/activity/ResourceListActivity;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$1;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    invoke-virtual {v1}, Lcom/youdao/note/activity/ResourceListActivity;->onContentChanged()V

    goto :goto_0
.end method
