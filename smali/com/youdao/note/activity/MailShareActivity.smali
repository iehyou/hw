.class public Lcom/youdao/note/activity/MailShareActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "MailShareActivity.java"


# static fields
.field private static final BUNDLE_NOTE_ID:Ljava/lang/String; = "bundle_noteId"

.field private static final DIALOG_INVALID_MAILS:I = 0x1

.field private static final DIALOG_MAILS_EMPTY:I = 0x2

.field private static final DIALOG_RECEIVERS_LIMIT:I = 0x7

.field private static final DIALOG_SENDING:I = 0x5

.field private static final DIALOG_SEND_FAILED:I = 0x6

.field private static final DIALOG_TITLE_EMPTY:I = 0x3

.field private static final DIALOG_TRASH:I = 0x4

.field private static final PATTERN:Ljava/util/regex/Pattern; = null

.field private static final REQUEST_PICK_CONTACTS:I = 0x1


# instance fields
.field private noteId:Ljava/lang/String;

.field private receivers:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "^([^<]+)<[^>]*>$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/activity/MailShareActivity;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 331
    const-string v0, ""

    iput-object v0, p0, Lcom/youdao/note/activity/MailShareActivity;->receivers:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/MailShareActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->doSendMail()V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/MailShareActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->checkEmails()V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/MailShareActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->hideSoftKeyboard()V

    return-void
.end method

.method private checkEmails()V
    .locals 20

    .prologue
    .line 334
    const v18, 0x7f070077

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 335
    .local v17, textView:Landroid/widget/TextView;
    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v18

    const-string v19, "[,\uff0c]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 336
    .local v14, mails:[Ljava/lang/String;
    const/4 v6, 0x0

    .line 337
    .local v6, gotMail:Z
    const/4 v5, 0x0

    .line 338
    .local v5, gotInvalid:Z
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v9, invalidMails:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    array-length v0, v14

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_2

    .line 340
    aget-object v18, v14, v7

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v14, v7

    .line 341
    aget-object v13, v14, v7

    .line 342
    .local v13, mail:Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_0

    .line 343
    const/4 v6, 0x1

    .line 344
    invoke-static {v13}, Lcom/youdao/note/utils/StringUtils;->isEmail(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 345
    sget-object v18, Lcom/youdao/note/activity/MailShareActivity;->PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    .line 346
    .local v12, m:Ljava/util/regex/Matcher;
    invoke-virtual {v12}, Ljava/util/regex/Matcher;->find()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 347
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 348
    aput-object v13, v14, v7

    .line 349
    invoke-static {v13}, Lcom/youdao/note/utils/StringUtils;->isEmail(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 350
    const/16 v18, 0x0

    aput-object v18, v14, v7

    .line 351
    const/4 v5, 0x1

    .line 352
    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    .end local v12           #m:Ljava/util/regex/Matcher;
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 355
    .restart local v12       #m:Ljava/util/regex/Matcher;
    :cond_1
    const/16 v18, 0x0

    aput-object v18, v14, v7

    .line 356
    const/4 v5, 0x1

    .line 357
    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 362
    .end local v12           #m:Ljava/util/regex/Matcher;
    .end local v13           #mail:Ljava/lang/String;
    :cond_2
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 363
    .local v16, sb:Ljava/lang/StringBuilder;
    const/4 v10, 0x1

    .line 364
    .local v10, isFirst:Z
    const/4 v4, 0x0

    .line 365
    .local v4, count:I
    move-object v2, v14

    .local v2, arr$:[Ljava/lang/String;
    array-length v11, v2

    .local v11, len$:I
    const/4 v8, 0x0

    .local v8, i$:I
    :goto_2
    if-ge v8, v11, :cond_5

    aget-object v15, v2, v8

    .line 366
    .local v15, s:Ljava/lang/String;
    if-nez v15, :cond_3

    .line 365
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 369
    :cond_3
    if-nez v10, :cond_4

    .line 370
    const-string v18, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    :cond_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    const/4 v10, 0x0

    .line 374
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 376
    .end local v15           #s:Ljava/lang/String;
    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/activity/MailShareActivity;->receivers:Ljava/lang/String;

    .line 377
    if-eqz v5, :cond_8

    .line 378
    new-instance v16, Ljava/lang/StringBuilder;

    .end local v16           #sb:Ljava/lang/StringBuilder;
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 379
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    const/4 v10, 0x1

    .line 380
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 381
    .restart local v13       #mail:Ljava/lang/String;
    if-nez v10, :cond_6

    .line 382
    const-string v18, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    :cond_6
    const/4 v10, 0x0

    .line 385
    const-string v18, "\""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    const-string v18, "\""

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 389
    .end local v13           #mail:Ljava/lang/String;
    :cond_7
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 390
    .local v3, bundle:Landroid/os/Bundle;
    const-string v18, "invalidMails"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 401
    .end local v3           #bundle:Landroid/os/Bundle;
    .end local v8           #i$:Ljava/util/Iterator;
    :goto_5
    return-void

    .line 392
    .local v8, i$:I
    :cond_8
    if-nez v6, :cond_9

    .line 393
    const/16 v18, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(I)V

    goto :goto_5

    .line 395
    :cond_9
    const/16 v18, 0x3

    move/from16 v0, v18

    if-le v4, v0, :cond_a

    .line 396
    const/16 v18, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(I)V

    goto :goto_5

    .line 398
    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/youdao/note/activity/MailShareActivity;->doSendMail()V

    goto :goto_5
.end method

.method private doSendMail()V
    .locals 7

    .prologue
    .line 294
    const/4 v5, 0x5

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(I)V

    .line 295
    iget-object v5, p0, Lcom/youdao/note/activity/MailShareActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v6, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    .line 296
    .local v2, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const v5, 0x7f07007a

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 297
    .local v4, subjectView:Landroid/widget/EditText;
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 298
    .local v3, subject:Ljava/lang/String;
    const v5, 0x7f07007c

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 299
    .local v1, messageView:Landroid/widget/EditText;
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 300
    .local v0, message:Ljava/lang/String;
    iget-object v5, p0, Lcom/youdao/note/activity/MailShareActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v6, p0, Lcom/youdao/note/activity/MailShareActivity;->receivers:Ljava/lang/String;

    invoke-virtual {v5, v2, v6, v3, v0}, Lcom/youdao/note/task/TaskManager;->mailShare(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v5, p0, Lcom/youdao/note/activity/MailShareActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v5}, Lcom/youdao/note/LogRecorder;->addSendMailTimes()Z

    .line 302
    return-void
.end method

.method private hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 268
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 270
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    const v1, 0x7f070076

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 271
    return-void
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 238
    const v0, 0x7f070079

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/MailShareActivity$10;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/MailShareActivity$10;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    const v0, 0x7f07007e

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/MailShareActivity$11;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/MailShareActivity$11;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    const v0, 0x7f070076

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/MailShareActivity$12;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/MailShareActivity$12;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 265
    return-void
.end method

.method private initUI()V
    .locals 9

    .prologue
    .line 97
    iget-object v7, p0, Lcom/youdao/note/activity/MailShareActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v8, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 98
    .local v1, meta:Lcom/youdao/note/data/NoteMeta;
    iget-object v7, p0, Lcom/youdao/note/activity/MailShareActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v1}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v2

    .line 99
    .local v2, note:Lcom/youdao/note/data/Note;
    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 100
    .local v5, title:Ljava/lang/String;
    const v7, 0x7f07007a

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 101
    .local v0, editTitle:Landroid/widget/EditText;
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const v7, 0x7f07007d

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 103
    .local v4, snapshotView:Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    invoke-static {v7}, Lcom/youdao/note/data/TempFile;->newNoteSnapshot(Ljava/lang/String;)Lcom/youdao/note/data/TempFile;

    move-result-object v6

    .line 104
    .local v6, tmp:Lcom/youdao/note/data/TempFile;
    invoke-virtual {v6}, Lcom/youdao/note/data/TempFile;->getContentBytes()[B

    move-result-object v7

    invoke-static {v7}, Lcom/youdao/note/utils/ImageUtils;->bytes2bitmap([B)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 105
    .local v3, snapshot:Landroid/graphics/Bitmap;
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 106
    return-void
.end method

.method private onMailShareFailed(Ljava/lang/Exception;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 312
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->removeDialog(I)V

    .line 314
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/youdao/note/exceptions/ServerException;

    if-nez v1, :cond_1

    .line 315
    :cond_0
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(I)V

    .line 329
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 317
    check-cast v0, Lcom/youdao/note/exceptions/ServerException;

    .line 318
    .local v0, se:Lcom/youdao/note/exceptions/ServerException;
    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v1

    const/16 v2, 0xe9

    if-ne v1, v2, :cond_2

    .line 319
    const v1, 0x7f0a00e2

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 320
    :cond_2
    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ServerException;->getResponseCode()I

    move-result v1

    const/16 v2, 0x12e

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v1

    const/16 v2, 0xe8

    if-ne v1, v2, :cond_4

    .line 321
    :cond_3
    const v1, 0x7f0a00e5

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 322
    :cond_4
    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v1

    const/16 v2, 0xe6

    if-ne v1, v2, :cond_5

    .line 323
    const v1, 0x7f0a00e3

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 325
    :cond_5
    const v1, 0x7f0a00e1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private onMailShareSucceed()V
    .locals 2

    .prologue
    .line 305
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->removeDialog(I)V

    .line 306
    const v0, 0x7f0a00e4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 308
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailShareActivity;->finish()V

    .line 309
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 274
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 275
    const/4 v3, -0x1

    if-ne p2, v3, :cond_1

    .line 276
    const v3, 0x7f070077

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 277
    .local v0, editText:Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 278
    .local v2, s:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 279
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 281
    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "emails"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, emails:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 283
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 286
    .end local v0           #editText:Landroid/widget/EditText;
    .end local v1           #emails:Ljava/lang/String;
    .end local v2           #s:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->showDialog(I)V

    .line 291
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    const/4 v0, 0x1

    .line 70
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->requestWindowFeature(I)Z

    .line 71
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->setRequestedOrientation(I)V

    .line 72
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailShareActivity;->setContentView(I)V

    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "noteid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->initUI()V

    .line 76
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->initListener()V

    .line 77
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .parameter "id"
    .parameter "args"

    .prologue
    const v5, 0x7f0a00aa

    const/4 v4, 0x1

    const v2, 0x7f0a00e7

    const v3, 0x7f0a00ab

    .line 111
    packed-switch p1, :pswitch_data_0

    .line 212
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/LockableActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 113
    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00e6

    new-instance v3, Lcom/youdao/note/activity/MailShareActivity$2;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/MailShareActivity$2;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$1;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$1;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 134
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$3;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$3;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$4;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$4;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 158
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00ea

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$5;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$5;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 170
    :pswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a00de

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00df

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$7;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$7;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$6;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$6;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 191
    :pswitch_5
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 192
    .local v0, dialog:Landroid/app/ProgressDialog;
    const v1, 0x7f0a00dd

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 193
    new-instance v1, Lcom/youdao/note/activity/MailShareActivity$8;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/MailShareActivity$8;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 200
    .end local v0           #dialog:Landroid/app/ProgressDialog;
    :pswitch_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a00d8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00e0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/MailShareActivity$9;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailShareActivity$9;-><init>(Lcom/youdao/note/activity/MailShareActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onDestroy()V

    .line 82
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 5
    .parameter "id"
    .parameter "dialog"
    .parameter "args"

    .prologue
    .line 218
    packed-switch p1, :pswitch_data_0

    .line 226
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 228
    :goto_0
    return-void

    .line 220
    :pswitch_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00eb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "${invalid_mails}"

    const-string v4, "invalidMails"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .local v1, msg:Ljava/lang/String;
    move-object v0, p2

    .line 222
    check-cast v0, Landroid/app/AlertDialog;

    .line 223
    .local v0, alert:Landroid/app/AlertDialog;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "bundle"

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 93
    const-string v0, "bundle_noteId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "bundle"

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    const-string v0, "bundle_noteId"

    iget-object v1, p0, Lcom/youdao/note/activity/MailShareActivity;->noteId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 2
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 405
    const/16 v1, 0x11

    if-ne p1, v1, :cond_0

    .line 406
    if-eqz p3, :cond_1

    .line 407
    invoke-direct {p0}, Lcom/youdao/note/activity/MailShareActivity;->onMailShareSucceed()V

    .line 422
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 423
    return-void

    .line 409
    :cond_1
    if-eqz p2, :cond_3

    instance-of v1, p2, Lcom/youdao/note/data/LocalErrorData;

    if-eqz v1, :cond_3

    move-object v0, p2

    .line 410
    check-cast v0, Lcom/youdao/note/data/LocalErrorData;

    .line 411
    .local v0, error:Lcom/youdao/note/data/LocalErrorData;
    invoke-virtual {v0}, Lcom/youdao/note/data/LocalErrorData;->getException()Ljava/lang/Exception;

    move-result-object v1

    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_2

    .line 412
    const v1, 0x7f0a00a1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 413
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->removeDialog(I)V

    goto :goto_0

    .line 415
    :cond_2
    invoke-virtual {v0}, Lcom/youdao/note/data/LocalErrorData;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->onMailShareFailed(Ljava/lang/Exception;)V

    goto :goto_0

    .line 418
    .end local v0           #error:Lcom/youdao/note/data/LocalErrorData;
    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/youdao/note/activity/MailShareActivity;->onMailShareFailed(Ljava/lang/Exception;)V

    goto :goto_0
.end method
