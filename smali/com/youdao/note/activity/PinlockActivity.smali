.class public Lcom/youdao/note/activity/PinlockActivity;
.super Lcom/youdao/note/activity/BaseActivity;
.source "PinlockActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/PinlockActivity$1;,
        Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;,
        Lcom/youdao/note/activity/PinlockActivity$UnLocker;,
        Lcom/youdao/note/activity/PinlockActivity$SetupLocker;,
        Lcom/youdao/note/activity/PinlockActivity$BaseLocker;,
        Lcom/youdao/note/activity/PinlockActivity$ILocker;
    }
.end annotation


# instance fields
.field private mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    .line 455
    return-void
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/PinlockActivity;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/PinlockActivity;->finishWithResult(Z)V

    return-void
.end method

.method private finishWithResult(Z)V
    .locals 1
    .parameter "succeed"

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity;->setResult(I)V

    .line 102
    :goto_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity;->finish()V

    .line 103
    return-void

    .line 100
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity;->requestWindowFeature(I)Z

    .line 43
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const-string v0, "com.youdao.note.action.SETUP_PINLOCK"

    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;

    invoke-direct {v0, p0, v2}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    .line 51
    :goto_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->initContentView()V

    .line 52
    return-void

    .line 46
    :cond_0
    const-string v0, "com.youdao.note.action.UPDATE_PINLOCK"

    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;

    invoke-direct {v0, p0, v2}, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    goto :goto_0

    .line 49
    :cond_1
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;

    invoke-direct {v0, p0, v2}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 86
    const/4 v0, 0x7

    if-lt p1, v0, :cond_1

    const/16 v0, 0x10

    if-gt p1, v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    add-int/lit8 v1, p1, -0x7

    invoke-interface {v0, v1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->onInput(I)V

    .line 93
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 88
    :cond_1
    const/16 v0, 0x43

    if-ne p1, v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->onInput(I)V

    goto :goto_0

    .line 90
    :cond_2
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->restoreState(Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onResume()V

    .line 58
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "outState"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity;->mLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->saveState(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method
