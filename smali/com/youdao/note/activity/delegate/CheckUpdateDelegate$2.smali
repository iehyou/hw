.class Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;
.super Ljava/lang/Object;
.source "CheckUpdateDelegate.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createNewVersionFoundDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)V
    .locals 0
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;->this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 111
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;->this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    #getter for: Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->access$300(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;->this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    #getter for: Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->access$100(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;->this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    #getter for: Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;
    invoke-static {v2}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->access$200(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Lcom/youdao/note/data/UpdateCheckResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/data/UpdateCheckResult;->getUpdateUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/YNoteApplication;->sendUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;->this$0:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    #getter for: Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->access$100(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 113
    return-void
.end method
