.class public Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;
.super Lcom/youdao/note/activity/delegate/BaseDelegate;
.source "CheckUpdateDelegate.java"


# static fields
.field public static final DIALOG_CHECKUPDATE_FAILED:I = 0x4

.field public static final DIALOG_CHECK_UPDATE:I = 0x2

.field public static final DIALOG_NEWVERSION_FOUND:I = 0x5

.field public static final DIALOG_NOUPDATE_FOUND:I = 0x6

.field private static final UPDATE_RESULT:Ljava/lang/String; = "check_update_result"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mTaskManager:Lcom/youdao/note/task/TaskManager;

.field private mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

.field private mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .parameter "activity"

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/delegate/BaseDelegate;-><init>(Landroid/app/Activity;)V

    .line 52
    iput-object p1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    .line 53
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 54
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getTaskManager()Lcom/youdao/note/task/TaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Lcom/youdao/note/task/TaskManager;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Lcom/youdao/note/data/UpdateCheckResult;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    return-object v0
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)Lcom/youdao/note/YNoteApplication;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method


# virtual methods
.method public checkUpdate()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 59
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->checkUpdate()V

    .line 60
    return-void
.end method

.method public createCheckUpdateFailedDialog()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a002a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public createCheckingUpdateDialog()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 82
    .local v0, progress:Landroid/app/ProgressDialog;
    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a0041

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 84
    new-instance v1, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$1;-><init>(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 90
    return-object v0
.end method

.method public createNewVersionFoundDialog()Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 101
    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    invoke-virtual {v1}, Lcom/youdao/note/data/UpdateCheckResult;->isNewVersionFoun()Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-object v0

    .line 104
    :cond_1
    const-string v1, "create new update found dialog."

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0029

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    invoke-virtual {v2}, Lcom/youdao/note/data/UpdateCheckResult;->getNewFeatures()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "<br/>"

    const-string v4, "\n"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00ab

    new-instance v3, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$2;-><init>(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00aa

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public createNoUpdateFoundDialog()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 118
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0028

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$3;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate$3;-><init>(Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCheckUpdateResult(Lcom/youdao/note/data/BaseData;Z)V
    .locals 2
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 67
    if-eqz p2, :cond_1

    .line 68
    check-cast p1, Lcom/youdao/note/data/UpdateCheckResult;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    .line 69
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 70
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    invoke-virtual {v0}, Lcom/youdao/note/data/UpdateCheckResult;->isNewVersionFoun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 78
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    .line 76
    .restart local p1
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method public onRestoreState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 144
    const-string v0, "check_update_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "check_update_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/UpdateCheckResult;

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    .line 147
    :cond_0
    return-void
.end method

.method public onSaveState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 135
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "check_update_result"

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->mUpdateCheckResult:Lcom/youdao/note/data/UpdateCheckResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 138
    :cond_0
    return-void
.end method
