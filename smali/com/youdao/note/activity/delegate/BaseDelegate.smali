.class public Lcom/youdao/note/activity/delegate/BaseDelegate;
.super Ljava/lang/Object;
.source "BaseDelegate.java"


# instance fields
.field protected context:Landroid/app/Activity;

.field protected mDataSource:Lcom/youdao/note/datasource/DataSource;

.field protected mLogRecorder:Lcom/youdao/note/LogRecorder;

.field protected mSyncManager:Lcom/youdao/note/task/SyncManager;

.field protected mTaskManager:Lcom/youdao/note/task/TaskManager;

.field protected mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->context:Landroid/app/Activity;

    .line 36
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 37
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getSyncManager()Lcom/youdao/note/task/SyncManager;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getTaskManager()Lcom/youdao/note/task/TaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 39
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 40
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    .line 41
    return-void
.end method


# virtual methods
.method public checkLogin()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/youdao/note/activity/delegate/BaseDelegate;->sendLogin()V

    .line 62
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected findViewById(I)Landroid/view/View;
    .locals 1
    .parameter "id"

    .prologue
    .line 73
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->context:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .parameter "menu"

    .prologue
    .line 49
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 0
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 57
    return-void
.end method

.method public sendLogin()V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->context:Landroid/app/Activity;

    const-class v2, Lcom/youdao/note/activity/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/youdao/note/activity/delegate/BaseDelegate;->context:Landroid/app/Activity;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 70
    return-void
.end method
