.class public Lcom/youdao/note/activity/delegate/SyncbarDelegate;
.super Lcom/youdao/note/activity/delegate/AbsSyncDelegate;
.source "SyncbarDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;
    }
.end annotation


# instance fields
.field private mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

.field private mSyncItem:Landroid/view/MenuItem;

.field private mSyncView:Landroid/view/View;

.field private syncListener:Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;-><init>(Landroid/app/Activity;)V

    .line 30
    const v0, 0x7f070083

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/SyncProgressBar;

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    .line 31
    const v0, 0x7f070080

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    new-instance v1, Lcom/youdao/note/activity/delegate/SyncbarDelegate$1;

    invoke-direct {v1, p0, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate$1;-><init>(Lcom/youdao/note/activity/delegate/SyncbarDelegate;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/SyncProgressBar;->setAnimationFinishListener(Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;)V

    .line 41
    :cond_0
    return-void
.end method

.method private disableSync()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 140
    :cond_0
    return-void
.end method

.method private dismissSyncProgress(Z)V
    .locals 1
    .parameter "succeed"

    .prologue
    .line 171
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/SyncProgressBar;->finishSync(Z)V

    .line 174
    :cond_0
    return-void
.end method

.method private enableSync()V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->enableSyncMenuItem()V

    .line 144
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 146
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    new-instance v1, Lcom/youdao/note/activity/delegate/SyncbarDelegate$3;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate$3;-><init>(Lcom/youdao/note/activity/delegate/SyncbarDelegate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    :cond_0
    return-void
.end method

.method private enableSyncMenuItem()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 159
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const v1, 0x7f0200ed

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 160
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const v1, 0x7f0a0090

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 162
    :cond_0
    return-void
.end method

.method private setOnSyncing()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    iget-object v1, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v1}, Lcom/youdao/note/task/SyncManager;->getSyncProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/SyncProgressBar;->setProgress(I)V

    .line 168
    :cond_0
    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter "item"

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0700b4

    if-ne v1, v2, :cond_1

    .line 68
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->context:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0090

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->checkLogin()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->startSync()V

    .line 77
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 79
    :cond_1
    return v0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v1}, Lcom/youdao/note/task/SyncManager;->stopSync()V

    .line 74
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->enableSync()V

    .line 75
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->dismissSyncProgress(Z)V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .parameter "menu"

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 85
    const v0, 0x7f0700b4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    .line 86
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->isSyncing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const v1, 0x1080038

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 91
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const v1, 0x7f0a00bd

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 93
    :cond_2
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->enableSyncMenuItem()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->onResume()V

    .line 46
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncView:Landroid/view/View;

    new-instance v1, Lcom/youdao/note/activity/delegate/SyncbarDelegate$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate$2;-><init>(Lcom/youdao/note/activity/delegate/SyncbarDelegate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->isSyncing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    const-string v0, "set on syncing."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->setOnSyncing()V

    .line 64
    :cond_2
    return-void
.end method

.method protected onSyncStatusChanged(Z)V
    .locals 2
    .parameter "isSyncing"

    .prologue
    .line 124
    if-eqz p1, :cond_1

    .line 125
    const-string v0, "set onsyncing. "

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->disableSync()V

    .line 127
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->setOnSyncing()V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->enableSync()V

    .line 130
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/SyncProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 4
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 100
    packed-switch p1, :pswitch_data_0

    .line 118
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 120
    :goto_0
    return-void

    .line 102
    :pswitch_0
    if-eqz p3, :cond_0

    .line 103
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->dismissSyncProgress(Z)V

    .line 108
    :goto_1
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->enableSync()V

    goto :goto_0

    .line 105
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->dismissSyncProgress(Z)V

    .line 106
    invoke-virtual {p0, p2}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->notifyError(Lcom/youdao/note/data/BaseData;)V

    goto :goto_1

    :pswitch_1
    move-object v1, p2

    .line 111
    check-cast v1, Lcom/youdao/note/data/ProgressData;

    .line 112
    .local v1, progress:Lcom/youdao/note/data/ProgressData;
    invoke-virtual {v1}, Lcom/youdao/note/data/ProgressData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v1}, Lcom/youdao/note/data/ProgressData;->getProgress()I

    move-result v0

    .line 114
    .local v0, p:I
    iget-object v2, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->mProgressBar:Lcom/youdao/note/ui/SyncProgressBar;

    invoke-virtual {v2, v0}, Lcom/youdao/note/ui/SyncProgressBar;->setProgress(I)V

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set progress bar to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setSyncListener(Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 190
    iput-object p1, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->syncListener:Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;

    .line 191
    return-void
.end method

.method public startSync()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->syncListener:Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->syncListener:Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;

    invoke-interface {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;->beforeSync()V

    .line 180
    :cond_0
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->disableSync()V

    .line 181
    invoke-direct {p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->setOnSyncing()V

    .line 182
    invoke-super {p0}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->startSync()V

    .line 183
    return-void
.end method
