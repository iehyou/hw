.class public abstract Lcom/youdao/note/activity/delegate/AbsSyncDelegate;
.super Lcom/youdao/note/activity/delegate/BaseDelegate;
.source "AbsSyncDelegate.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/delegate/BaseDelegate;-><init>(Landroid/app/Activity;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected notifyError(Lcom/youdao/note/data/BaseData;)V
    .locals 9
    .parameter "data"

    .prologue
    const v6, 0x7f0a0032

    .line 53
    move-object v1, p1

    check-cast v1, Lcom/youdao/note/data/RemoteErrorData;

    .line 54
    .local v1, error:Lcom/youdao/note/data/RemoteErrorData;
    invoke-virtual {v1}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v2

    .line 55
    .local v2, exception:Ljava/lang/Exception;
    if-nez v2, :cond_0

    .line 56
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    invoke-static {v4, v6}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 87
    :goto_0
    return-void

    .line 57
    :cond_0
    instance-of v4, v2, Lcom/youdao/note/exceptions/UnloginException;

    if-eqz v4, :cond_1

    .line 58
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const v5, 0x7f0a0033

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 59
    :cond_1
    instance-of v4, v2, Lcom/youdao/note/exceptions/ServerException;

    if-eqz v4, :cond_4

    move-object v3, v2

    .line 60
    check-cast v3, Lcom/youdao/note/exceptions/ServerException;

    .line 61
    .local v3, serverException:Lcom/youdao/note/exceptions/ServerException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ServerError Code is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/youdao/note/exceptions/ServerException;->getResponseCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v3}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v4

    const/16 v5, 0xd2

    if-ne v4, v5, :cond_2

    .line 63
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const v5, 0x7f0a002f

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 64
    :cond_2
    invoke-virtual {v3}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v4

    const/16 v5, 0xd0

    if-ne v4, v5, :cond_3

    .line 66
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const v5, 0x7f0a004f

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 68
    :cond_3
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    invoke-static {v4, v6}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 70
    .end local v3           #serverException:Lcom/youdao/note/exceptions/ServerException;
    :cond_4
    instance-of v4, v2, Lcom/youdao/note/exceptions/ResourceMissingException;

    if-eqz v4, :cond_5

    move-object v0, v2

    .line 71
    check-cast v0, Lcom/youdao/note/exceptions/ResourceMissingException;

    .line 72
    .local v0, e:Lcom/youdao/note/exceptions/ResourceMissingException;
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    iget-object v5, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0034

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ResourceMissingException;->getNoteTitle()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v0           #e:Lcom/youdao/note/exceptions/ResourceMissingException;
    :cond_5
    instance-of v4, v2, Ljava/io/IOException;

    if-eqz v4, :cond_7

    .line 79
    instance-of v4, v2, Ljava/net/SocketException;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->isUsingCMWAP()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 80
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const/16 v5, 0x75

    invoke-virtual {v4, v5}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_0

    .line 82
    :cond_6
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const v5, 0x7f0a00a1

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 85
    :cond_7
    iget-object v4, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->context:Landroid/app/Activity;

    const v5, 0x7f0a009f

    invoke-static {v4, v5}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Lcom/youdao/note/activity/delegate/BaseDelegate;->onResume()V

    .line 33
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->isSyncing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->onSyncStatusChanged(Z)V

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->onSyncStatusChanged(Z)V

    goto :goto_0
.end method

.method protected abstract onSyncStatusChanged(Z)V
.end method

.method public startSync()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/SyncManager;->startSync(Z)Z

    .line 44
    return-void
.end method

.method public syncIfNeed()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getRootMeta()Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/SyncManager;->isSyncing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/youdao/note/activity/delegate/AbsSyncDelegate;->startSync()V

    .line 50
    :cond_0
    return-void
.end method
