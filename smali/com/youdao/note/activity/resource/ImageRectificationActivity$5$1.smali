.class Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;
.super Ljava/lang/Object;
.source "ImageRectificationActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

.field final synthetic val$height:I

.field final synthetic val$imageY:F


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;FI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 210
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->this$1:Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

    iput p2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->val$imageY:F

    iput p3, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->val$height:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 213
    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->this$1:Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

    iget-object v2, v2, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->titleHeight:I
    invoke-static {v2}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$100(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->val$imageY:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->val$height:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->this$1:Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

    iget-object v3, v3, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v1, v2

    .line 214
    .local v1, yOffset:I
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 215
    .local v0, msg:Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 216
    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;->this$1:Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

    iget-object v2, v2, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->secondAnimHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$600(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 217
    return-void
.end method
