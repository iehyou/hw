.class Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;
.super Landroid/os/Handler;
.source "ImageRectificationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/resource/ImageRectificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 178
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .parameter "msg"

    .prologue
    .line 181
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    .line 182
    .local v4, bundle:Landroid/os/Bundle;
    const-string v17, "endRatio"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v6

    .line 183
    .local v6, endRatio:F
    const-string v17, "x"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v11

    .line 184
    .local v11, imageX:F
    const-string v17, "y"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v12

    .line 185
    .local v12, imageY:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v18, v0

    const v19, 0x7f070012

    invoke-virtual/range {v18 .. v19}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getHeight()I

    move-result v18

    #setter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->titleHeight:I
    invoke-static/range {v17 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$102(Lcom/youdao/note/activity/resource/ImageRectificationActivity;I)I

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    check-cast v17, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 187
    .local v7, frame:Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 188
    .local v16, width:I
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 189
    .local v9, height:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    mul-float v18, v18, v6

    #setter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndWidth:F
    invoke-static/range {v17 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$302(Lcom/youdao/note/activity/resource/ImageRectificationActivity;F)F

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    int-to-float v0, v9

    move/from16 v18, v0

    mul-float v18, v18, v6

    #setter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndHeight:F
    invoke-static/range {v17 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$402(Lcom/youdao/note/activity/resource/ImageRectificationActivity;F)F

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    new-instance v18, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    #setter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static/range {v17 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$502(Lcom/youdao/note/activity/resource/ImageRectificationActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v17

    new-instance v18, Landroid/view/ViewGroup$LayoutParams;

    const/16 v19, -0x1

    const/16 v20, -0x1

    invoke-direct/range {v18 .. v20}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v18, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static/range {v18 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->setContentView(Landroid/view/View;)V

    .line 196
    new-instance v15, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 197
    .local v15, view:Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    move/from16 v0, v16

    invoke-direct {v14, v0, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 199
    .local v14, params:Landroid/widget/RelativeLayout$LayoutParams;
    float-to-int v0, v11

    move/from16 v17, v0

    float-to-int v0, v12

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v19, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->titleHeight:I
    invoke-static/range {v19 .. v19}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$100(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)I

    move-result v19

    add-int v18, v18, v19

    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v14, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 200
    invoke-virtual {v15, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 204
    const/4 v5, 0x0

    .line 205
    .local v5, duration:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v13

    .line 206
    .local v13, numFrames:I
    const/4 v10, 0x0

    .local v10, i:I
    :goto_0
    if-ge v10, v13, :cond_0

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v17, v0

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static/range {v17 .. v17}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    move-result v17

    add-int v5, v5, v17

    .line 206
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 209
    :cond_0
    new-instance v8, Landroid/os/Handler;

    invoke-direct {v8}, Landroid/os/Handler;-><init>()V

    .line 210
    .local v8, handler:Landroid/os/Handler;
    new-instance v17, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12, v9}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5$1;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;FI)V

    int-to-long v0, v5

    move-wide/from16 v18, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-virtual {v8, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 219
    return-void
.end method
