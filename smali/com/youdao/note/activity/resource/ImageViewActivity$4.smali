.class Lcom/youdao/note/activity/resource/ImageViewActivity$4;
.super Ljava/lang/Object;
.source "ImageViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/ImageViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 133
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/resource/ImageViewActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLoadImageLibSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/resource/ImageViewActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->addImageRectificationTimes()Z

    .line 135
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    const-class v2, Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMeta"

    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    iget-object v2, v2, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 137
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, Lcom/youdao/note/activity/resource/ImageViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 138
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    const v2, 0x7f070051

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/resource/ImageViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 143
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity$4;->this$0:Lcom/youdao/note/activity/resource/ImageViewActivity;

    const v2, 0x7f0a00f6

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
