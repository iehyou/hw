.class public Lcom/youdao/note/activity/resource/ImageViewActivity;
.super Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;
.source "ImageViewActivity.java"


# static fields
.field private static final BUNDLE_CREATE_OR_EDIT:Ljava/lang/String; = "create_or_edit"

.field private static final BUNDLE_URI_CAMERA:Ljava/lang/String; = "uri4camera"

.field private static final BUNDLE_URI_VIEW:Ljava/lang/String; = "uri4view"

.field private static final DIALOG_DELETE_RESOURCE_CONFIRM:I = 0x1

.field public static final IMAGE_EXTRA_NAME:Ljava/lang/String; = "image"


# instance fields
.field private createOrEdit:Z

.field protected mLogRecorder:Lcom/youdao/note/LogRecorder;

.field private mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

.field protected mYNote:Lcom/youdao/note/YNoteApplication;

.field private uri4camera:Landroid/net/Uri;

.field private uri4view:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 55
    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/resource/ImageViewActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->updateUri()V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/resource/ImageViewActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->updateUI()V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/resource/ImageViewActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->complete()V

    return-void
.end method

.method private complete()V
    .locals 3

    .prologue
    .line 311
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 312
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMeta"

    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 313
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 314
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    .line 315
    return-void
.end method

.method private updateUI()V
    .locals 6

    .prologue
    .line 162
    :try_start_0
    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-nez v4, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    const v4, 0x7f070051

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/ImageViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 166
    .local v3, view:Landroid/widget/ImageView;
    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    if-nez v4, :cond_2

    .line 167
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->updateUri()V

    .line 169
    :cond_2
    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    invoke-static {v4}, Lcom/youdao/note/utils/FileUtils;->exist(Landroid/net/Uri;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_0

    .line 171
    :try_start_1
    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    invoke-static {v4}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 172
    .local v1, image:Landroid/graphics/Bitmap;
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 173
    .end local v1           #image:Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 174
    .local v0, e:Ljava/io/FileNotFoundException;
    :try_start_2
    const-string v4, "load image view failed!"

    invoke-static {p0, v4, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 177
    .end local v0           #e:Ljava/io/FileNotFoundException;
    .end local v3           #view:Landroid/widget/ImageView;
    :catch_1
    move-exception v0

    .line 178
    .local v0, e:Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 179
    const v4, 0x7f0a00ca

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 181
    .local v2, toast:Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 182
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto :goto_0
.end method

.method private updateUri()V
    .locals 3

    .prologue
    .line 283
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v1, :cond_0

    .line 284
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, file:Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    .line 289
    .end local v0           #file:Ljava/io/File;
    :goto_0
    return-void

    .line 287
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 215
    sparse-switch p1, :sswitch_data_0

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 217
    :sswitch_0
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_0

    .line 218
    :try_start_0
    const-string v3, "resourceMeta"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/BaseResourceMeta;

    iput-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 219
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->updateUri()V

    .line 220
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->complete()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, e:Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 276
    const v3, 0x7f0a00ca

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 277
    .local v2, toast:Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 278
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto :goto_0

    .line 224
    .end local v0           #e:Ljava/lang/OutOfMemoryError;
    .end local v2           #toast:Landroid/widget/Toast;
    :sswitch_1
    :try_start_1
    const-string v3, "On activity result called."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    if-nez v3, :cond_1

    .line 226
    const v3, 0x7f0a007f

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 227
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto :goto_0

    .line 229
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    invoke-static {v3}, Lcom/youdao/note/utils/FileUtils;->exist(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 230
    const v3, 0x7f0a007f

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 231
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto :goto_0

    .line 234
    :cond_2
    const/4 v1, 0x0

    .line 235
    .local v1, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v3, v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v3, :cond_3

    .line 236
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v1           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    check-cast v1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 241
    .restart local v1       #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :cond_3
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5, v1}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource(Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 243
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    goto :goto_0

    .line 249
    .end local v1           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :sswitch_2
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_5

    .line 250
    :cond_4
    const v3, 0x7f0a007f

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 251
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto :goto_0

    .line 253
    :cond_5
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/youdao/note/utils/FileUtils;->exist(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 254
    const v3, 0x7f0a007f

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 255
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->finish()V

    goto/16 :goto_0

    .line 258
    :cond_6
    const/4 v1, 0x0

    .line 259
    .restart local v1       #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v3, v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v3, :cond_7

    .line 260
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v1           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    check-cast v1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 266
    .restart local v1       #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :cond_7
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5, v1}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource(Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 215
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0xb -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 319
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->createOrEdit:Z

    if-eqz v1, :cond_1

    .line 320
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v1, :cond_0

    .line 321
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    check-cast v0, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 322
    .local v0, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v0}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 324
    .end local v0           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->cancleAndBack()V

    .line 327
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .parameter "dialog"

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->cancleAndBack()V

    .line 355
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x1

    .line 61
    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageViewActivity;->requestWindowFeature(I)Z

    .line 62
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    new-instance v5, Lcom/youdao/note/activity/resource/ImageViewActivity$1;

    invoke-direct {v5, p0}, Lcom/youdao/note/activity/resource/ImageViewActivity$1;-><init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V

    invoke-static {p0, v5}, Lcom/youdao/note/activity/ResourceAdder;->getInstance(Landroid/app/Activity;Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;)Lcom/youdao/note/activity/ResourceAdder;

    move-result-object v5

    iput-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    .line 89
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v5

    iput-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 90
    iget-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v5

    iput-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    .line 91
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 92
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 94
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 95
    .local v2, newIntent:Landroid/content/Intent;
    const-string v5, "android.intent.category.OPENABLE"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v5, "image/*"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const/4 v5, 0x6

    invoke-virtual {p0, v2, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 98
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.youdao.note.action.CREATE_RESOURCE"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v0, "com.youdao.note.action.CREATE_RESOURCE"

    .line 111
    .end local v2           #newIntent:Landroid/content/Intent;
    :cond_0
    :goto_0
    const-string v5, "com.youdao.note.action.CREATE_RESOURCE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 112
    iput-boolean v7, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->createOrEdit:Z

    .line 113
    const v5, 0x7f03001e

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->setContentView(I)V

    .line 114
    const v5, 0x7f070050

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/youdao/note/activity/resource/ImageViewActivity$2;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/resource/ImageViewActivity$2;-><init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :goto_1
    const v5, 0x7f070052

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/youdao/note/activity/resource/ImageViewActivity$4;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/resource/ImageViewActivity$4;-><init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    return-void

    .line 100
    :cond_1
    const-string v5, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 101
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 102
    .restart local v2       #newIntent:Landroid/content/Intent;
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyImageResource()Lcom/youdao/note/data/resource/ImageResource;

    move-result-object v4

    .line 103
    .local v4, tmpResource:Lcom/youdao/note/data/resource/ImageResource;
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v5

    iput-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 104
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v5

    check-cast v5, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v3

    .line 105
    .local v3, tmpCameraFile:Ljava/io/File;
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    iput-object v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    .line 106
    const-string v5, "output"

    iget-object v6, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const/4 v5, 0x5

    invoke-virtual {p0, v2, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 108
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.youdao.note.action.CREATE_RESOURCE"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v0, "com.youdao.note.action.CREATE_RESOURCE"

    goto :goto_0

    .line 121
    .end local v2           #newIntent:Landroid/content/Intent;
    .end local v3           #tmpCameraFile:Ljava/io/File;
    .end local v4           #tmpResource:Lcom/youdao/note/data/resource/ImageResource;
    :cond_2
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->createOrEdit:Z

    .line 122
    const v5, 0x7f03001f

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->setContentView(I)V

    .line 123
    const v5, 0x7f070054

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/resource/ImageViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/youdao/note/activity/resource/ImageViewActivity$3;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/resource/ImageViewActivity$3;-><init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "id"

    .prologue
    .line 293
    sparse-switch p1, :sswitch_data_0

    .line 306
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 295
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0036

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/activity/resource/ImageViewActivity$5;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/ImageViewActivity$5;-><init>(Lcom/youdao/note/activity/resource/ImageViewActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 304
    :sswitch_1
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0}, Lcom/youdao/note/activity/ResourceAdder;->createDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    goto :goto_0

    .line 293
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x74 -> :sswitch_1
    .end sparse-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onDestroy()V

    .line 157
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0}, Lcom/youdao/note/activity/ResourceAdder;->destroy()V

    .line 158
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 189
    const-string v1, "uri4camera"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, s:Ljava/io/Serializable;
    if-eqz v0, :cond_0

    .line 191
    check-cast v0, Ljava/lang/String;

    .end local v0           #s:Ljava/io/Serializable;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    .line 193
    :cond_0
    const-string v1, "uri4view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 194
    .restart local v0       #s:Ljava/io/Serializable;
    if-eqz v0, :cond_1

    .line 195
    check-cast v0, Ljava/lang/String;

    .end local v0           #s:Ljava/io/Serializable;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    .line 197
    :cond_1
    const-string v1, "create_or_edit"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->createOrEdit:Z

    .line 198
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 149
    const-string v0, "OnResume called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-super {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onResume()V

    .line 151
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/ImageViewActivity;->updateUI()V

    .line 152
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 203
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "uri4camera"

    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4camera:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 207
    const-string v0, "uri4view"

    iget-object v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->uri4view:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_1
    const-string v0, "create_or_edit"

    iget-boolean v1, p0, Lcom/youdao/note/activity/resource/ImageViewActivity;->createOrEdit:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 210
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 0
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 349
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 350
    return-void
.end method
