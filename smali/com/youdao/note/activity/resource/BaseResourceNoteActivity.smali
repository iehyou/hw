.class public Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "BaseResourceNoteActivity.java"


# static fields
.field private static final BUNDLE_RESOURCE_META:Ljava/lang/String; = "resourceMeta"


# instance fields
.field protected resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    return-void
.end method


# virtual methods
.method protected cancleAndBack()V
    .locals 2

    .prologue
    .line 111
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 112
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->setResult(ILandroid/content/Intent;)V

    .line 113
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->finish()V

    .line 114
    return-void
.end method

.method protected finishAndBack()V
    .locals 3

    .prologue
    .line 117
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 118
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMeta"

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 119
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->setResult(ILandroid/content/Intent;)V

    .line 120
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->finish()V

    .line 121
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resourceMeta"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    iput-object v0, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 33
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 38
    const-string v1, "resourceMeta"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 39
    .local v0, s:Ljava/io/Serializable;
    if-eqz v0, :cond_0

    .line 40
    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local v0           #s:Ljava/io/Serializable;
    iput-object v0, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 42
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "resourceMeta"

    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected saveDoodleResource(Landroid/graphics/Bitmap;)V
    .locals 4
    .parameter "image"

    .prologue
    const/16 v3, 0x64

    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, resource:Lcom/youdao/note/data/resource/DoodleResource;
    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    if-nez v1, :cond_1

    .line 98
    :cond_0
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyDoodleResource()Lcom/youdao/note/data/resource/DoodleResource;

    move-result-object v0

    .line 102
    :goto_0
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {p1, v1, v3}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/DoodleResource;->setContentBytes([B)V

    .line 104
    const/4 v1, 0x0

    invoke-static {v0, p1, v3, v1}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V

    .line 105
    invoke-virtual {v0}, Lcom/youdao/note/data/resource/DoodleResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 106
    invoke-static {}, Lcom/youdao/note/utils/BitmapMemCache;->getInstance()Lcom/youdao/note/utils/BitmapMemCache;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v3, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/utils/BitmapMemCache;->removeBitmap(Ljava/lang/String;)V

    .line 108
    return-void

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    .end local v0           #resource:Lcom/youdao/note/data/resource/DoodleResource;
    check-cast v0, Lcom/youdao/note/data/resource/DoodleResource;

    .restart local v0       #resource:Lcom/youdao/note/data/resource/DoodleResource;
    goto :goto_0
.end method

.method protected saveHandwriteResource(Landroid/graphics/Bitmap;)V
    .locals 4
    .parameter "image"

    .prologue
    const/16 v3, 0x64

    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, resource:Lcom/youdao/note/data/resource/HandwriteResource;
    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v1, v1, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    if-nez v1, :cond_1

    .line 82
    :cond_0
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyHandwriteResource()Lcom/youdao/note/data/resource/HandwriteResource;

    move-result-object v0

    .line 87
    :goto_0
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {p1, v1, v3}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/resource/HandwriteResource;->setContentBytes([B)V

    .line 89
    const/4 v1, 0x0

    invoke-static {v0, p1, v3, v1}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V

    .line 90
    invoke-virtual {v0}, Lcom/youdao/note/data/resource/HandwriteResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 91
    invoke-static {}, Lcom/youdao/note/utils/BitmapMemCache;->getInstance()Lcom/youdao/note/utils/BitmapMemCache;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v3, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/utils/BitmapMemCache;->removeBitmap(Ljava/lang/String;)V

    .line 92
    return-void

    .line 84
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v0

    .end local v0           #resource:Lcom/youdao/note/data/resource/HandwriteResource;
    check-cast v0, Lcom/youdao/note/data/resource/HandwriteResource;

    .restart local v0       #resource:Lcom/youdao/note/data/resource/HandwriteResource;
    goto :goto_0
.end method

.method protected saveImageResource(Landroid/graphics/Bitmap;I)V
    .locals 4
    .parameter "image"
    .parameter "picFrom"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 67
    .local v1, resource:Lcom/youdao/note/data/resource/ImageResource;
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v2, v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-nez v2, :cond_1

    .line 68
    :cond_0
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyImageResource()Lcom/youdao/note/data/resource/ImageResource;

    move-result-object v1

    .line 72
    :goto_0
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-virtual {v2, p2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setPicFrom(I)V

    .line 73
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getImageQuality()I

    move-result v0

    .line 74
    .local v0, quarity:I
    const/4 v2, 0x1

    invoke-static {v1, p1, v0, v2}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V

    .line 75
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 76
    return-void

    .line 70
    .end local v0           #quarity:I
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v3, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v1

    .end local v1           #resource:Lcom/youdao/note/data/resource/ImageResource;
    check-cast v1, Lcom/youdao/note/data/resource/ImageResource;

    .restart local v1       #resource:Lcom/youdao/note/data/resource/ImageResource;
    goto :goto_0
.end method

.method protected saveImageResource(Landroid/net/Uri;I)V
    .locals 4
    .parameter "imageUri"
    .parameter "picFrom"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, resource:Lcom/youdao/note/data/resource/ImageResource;
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    instance-of v2, v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-nez v2, :cond_1

    .line 55
    :cond_0
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyImageResource()Lcom/youdao/note/data/resource/ImageResource;

    move-result-object v1

    .line 59
    :goto_0
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-virtual {v2, p2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setPicFrom(I)V

    .line 60
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getImageQuality()I

    move-result v0

    .line 61
    .local v0, quarity:I
    const/4 v2, 0x1

    invoke-static {v1, p1, v0, v2}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/net/Uri;IZ)V

    .line 62
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 63
    return-void

    .line 57
    .end local v0           #quarity:I
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v3, p0, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v1

    .end local v1           #resource:Lcom/youdao/note/data/resource/ImageResource;
    check-cast v1, Lcom/youdao/note/data/resource/ImageResource;

    .restart local v1       #resource:Lcom/youdao/note/data/resource/ImageResource;
    goto :goto_0
.end method
