.class Lcom/youdao/note/activity/resource/DoodleActivity$3;
.super Ljava/lang/Object;
.source "DoodleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/DoodleActivity;->initViewListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/DoodleActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-object p1, p0, Lcom/youdao/note/activity/resource/DoodleActivity$3;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 88
    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity$3;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    const v3, 0x7f07001b

    invoke-virtual {v2, v3}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;

    .line 89
    .local v0, colorBox:Lcom/youdao/note/ui/skitch/tool/ColorBoxView;
    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity$3;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    const v3, 0x7f07001a

    invoke-virtual {v2, v3}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;

    .line 90
    .local v1, sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 91
    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->dismiss()V

    .line 98
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 94
    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->dismiss()V

    .line 96
    :cond_1
    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->show()V

    goto :goto_0
.end method
