.class Lcom/youdao/note/activity/resource/HandwritingActivity$6;
.super Ljava/lang/Object;
.source "HandwritingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/HandwritingActivity;->initViewListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 141
    iput-object p1, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .parameter "v"

    .prologue
    .line 144
    :try_start_0
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    #getter for: Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;
    invoke-static {v8}, Lcom/youdao/note/activity/resource/HandwritingActivity;->access$100(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 145
    .local v2, contentBitmap:Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    const v9, 0x7f070039

    invoke-virtual {v8, v9}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 146
    .local v4, head:Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 149
    .local v5, headBitmap:Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 150
    .local v1, canvas:Landroid/graphics/Canvas;
    invoke-virtual {v4, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 152
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 157
    .local v0, bitmap:Landroid/graphics/Bitmap;
    const/4 v8, -0x1

    invoke-virtual {v0, v8}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 158
    new-instance v1, Landroid/graphics/Canvas;

    .end local v1           #canvas:Landroid/graphics/Canvas;
    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 159
    .restart local v1       #canvas:Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 160
    .local v7, paint:Landroid/graphics/Paint;
    const/high16 v8, -0x100

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 161
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 162
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v5, v8, v9, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 167
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    const/4 v10, 0x0

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2, v8, v9, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 174
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-virtual {v8, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->saveHandwriteResource(Landroid/graphics/Bitmap;)V

    .line 175
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 176
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 177
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 178
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    .line 180
    .local v6, mYnote:Lcom/youdao/note/YNoteApplication;
    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    iget-object v9, v9, Lcom/youdao/note/activity/resource/HandwritingActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v9}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    #getter for: Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
    invoke-static {v10}, Lcom/youdao/note/activity/resource/HandwritingActivity;->access$200(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->addOrUpdateSkitchMeta(Ljava/lang/String;Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 184
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-virtual {v8}, Lcom/youdao/note/activity/resource/HandwritingActivity;->finishAndBack()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #canvas:Landroid/graphics/Canvas;
    .end local v2           #contentBitmap:Landroid/graphics/Bitmap;
    .end local v4           #head:Landroid/view/View;
    .end local v5           #headBitmap:Landroid/graphics/Bitmap;
    .end local v6           #mYnote:Lcom/youdao/note/YNoteApplication;
    .end local v7           #paint:Landroid/graphics/Paint;
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v3

    .line 186
    .local v3, e:Ljava/lang/OutOfMemoryError;
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    const v9, 0x7f0a00ca

    invoke-static {v8, v9}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 188
    iget-object v8, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$6;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-virtual {v8}, Lcom/youdao/note/activity/resource/HandwritingActivity;->finish()V

    goto :goto_0
.end method
