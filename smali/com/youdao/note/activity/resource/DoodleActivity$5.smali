.class Lcom/youdao/note/activity/resource/DoodleActivity$5;
.super Ljava/lang/Object;
.source "DoodleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/DoodleActivity;->initViewListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/DoodleActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 116
    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    #getter for: Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;
    invoke-static {v2}, Lcom/youdao/note/activity/resource/DoodleActivity;->access$000(Lcom/youdao/note/activity/resource/DoodleActivity;)Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    move-result-object v2

    invoke-interface {v2}, Lcom/youdao/note/ui/skitch/doodle/IDoodle;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 117
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-virtual {v2, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->saveDoodleResource(Landroid/graphics/Bitmap;)V

    .line 118
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 119
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    .line 120
    .local v1, mYnote:Lcom/youdao/note/YNoteApplication;
    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    iget-object v3, v3, Lcom/youdao/note/activity/resource/DoodleActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    #getter for: Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;
    invoke-static {v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->access$100(Lcom/youdao/note/activity/resource/DoodleActivity;)Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->addOrUpdateSkitchMeta(Ljava/lang/String;Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 121
    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity$5;->this$0:Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-virtual {v2}, Lcom/youdao/note/activity/resource/DoodleActivity;->finishAndBack()V

    .line 122
    return-void
.end method
