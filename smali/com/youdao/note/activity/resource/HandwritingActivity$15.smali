.class Lcom/youdao/note/activity/resource/HandwritingActivity$15;
.super Ljava/lang/Object;
.source "HandwritingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/resource/HandwritingActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 280
    iput-object p1, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$15;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/4 v1, 0x0

    .line 283
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$15;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    #getter for: Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;
    invoke-static {v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->access$000(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->switchToNormalMode()V

    .line 284
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$15;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    #getter for: Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;
    invoke-static {v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->access$000(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->postInvalidate()V

    .line 285
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setHandwriteMode(I)V

    .line 286
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setShowHandwriteModeTips(Z)V

    .line 287
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity$15;->this$0:Lcom/youdao/note/activity/resource/HandwritingActivity;

    const v1, 0x7f0a00fe

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 288
    return-void
.end method
