.class public Lcom/youdao/note/activity/resource/ImageRectificationActivity;
.super Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;
.source "ImageRectificationActivity.java"


# static fields
.field public static final DIALOG_ID_AUTO_DETECT:I = 0x0

.field public static final DIALOG_ID_COMPLETE:I = 0x1


# instance fields
.field private animation:Landroid/graphics/drawable/AnimationDrawable;

.field private confirmView:Landroid/widget/RelativeLayout;

.field private correlationConfirmHandler:Landroid/os/Handler;

.field private firstAnimHandler:Landroid/os/Handler;

.field private firstAnimationEndHeight:F

.field private firstAnimationEndWidth:F

.field private image:Landroid/graphics/Bitmap;

.field private mainView:Landroid/view/View;

.field private secondAnimHandler:Landroid/os/Handler;

.field private titleHeight:I

.field private view:Lcom/youdao/note/ui/ImageRectificationView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;-><init>()V

    .line 178
    new-instance v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$5;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimHandler:Landroid/os/Handler;

    .line 222
    new-instance v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->secondAnimHandler:Landroid/os/Handler;

    .line 269
    new-instance v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$7;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$7;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->correlationConfirmHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Lcom/youdao/note/ui/ImageRectificationView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->titleHeight:I

    return v0
.end method

.method static synthetic access$102(Lcom/youdao/note/activity/resource/ImageRectificationActivity;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    iput p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->titleHeight:I

    return p1
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method static synthetic access$202(Lcom/youdao/note/activity/resource/ImageRectificationActivity;Landroid/graphics/drawable/AnimationDrawable;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;

    return-object p1
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndWidth:F

    return v0
.end method

.method static synthetic access$302(Lcom/youdao/note/activity/resource/ImageRectificationActivity;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    iput p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndWidth:F

    return p1
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndHeight:F

    return v0
.end method

.method static synthetic access$402(Lcom/youdao/note/activity/resource/ImageRectificationActivity;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    iput p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndHeight:F

    return p1
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$502(Lcom/youdao/note/activity/resource/ImageRectificationActivity;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 38
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$600(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->secondAnimHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->correlationConfirmHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 38
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->mainView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public completeRectification(Landroid/graphics/Bitmap;Landroid/graphics/drawable/AnimationDrawable;FLandroid/graphics/PointF;)V
    .locals 6
    .parameter "image"
    .parameter "anim"
    .parameter "endRatio"
    .parameter "imagePos"

    .prologue
    .line 155
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    .line 156
    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    check-cast v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 158
    .local v2, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :try_start_0
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v4

    invoke-virtual {p0, p1, v4}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->saveImageResource(Landroid/graphics/Bitmap;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    iput-object p2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;

    .line 163
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    .line 164
    .local v3, message:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165
    .local v0, data:Landroid/os/Bundle;
    const-string v4, "endRatio"

    invoke-virtual {v0, v4, p3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 166
    const-string v4, "x"

    iget v5, p4, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 167
    const-string v4, "y"

    iget v5, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 168
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 169
    iget-object v4, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 170
    return-void

    .line 159
    .end local v0           #data:Landroid/os/Bundle;
    .end local v3           #message:Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 160
    .local v1, e:Ljava/io/IOException;
    const-string v4, "save image failed!"

    invoke-static {p0, v4, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 297
    const/16 v1, 0xb

    if-ne p1, v1, :cond_0

    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_0

    .line 298
    const-string v1, "resourceMeta"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/BaseResourceMeta;

    iput-object v1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 299
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 300
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMeta"

    iget-object v2, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 301
    invoke-virtual {p0, v3, v0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->setResult(ILandroid/content/Intent;)V

    .line 302
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->finish()V

    .line 304
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 55
    :try_start_0
    const-string v7, "WhiteBoardActivity create"

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->requestWindowFeature(I)Z

    .line 57
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->setRequestedOrientation(I)V

    .line 58
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 60
    .local v2, layoutInflator:Landroid/view/LayoutInflater;
    const v7, 0x7f03001a

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->mainView:Landroid/view/View;

    .line 61
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->mainView:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->setContentView(Landroid/view/View;)V

    .line 62
    const v7, 0x7f07004d

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/youdao/note/ui/ImageRectificationView;

    iput-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 64
    :try_start_1
    iget-object v3, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    check-cast v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 65
    .local v3, meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v5

    .line 66
    .local v5, tmpFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 67
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    .line 72
    :goto_0
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v7, v8, :cond_0

    .line 73
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 74
    .local v4, temp:Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 75
    iput-object v4, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    .line 77
    .end local v4           #temp:Landroid/graphics/Bitmap;
    :cond_0
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;

    iget-object v8, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/youdao/note/ui/ImageRectificationView;->setImage(Landroid/graphics/Bitmap;)V

    .line 78
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/youdao/note/ui/ImageRectificationView;->setBackgroundColor(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 82
    .end local v3           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    .end local v5           #tmpFile:Ljava/io/File;
    :goto_1
    const v7, 0x7f070049

    :try_start_2
    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Lcom/youdao/note/activity/resource/ImageRectificationActivity$1;

    invoke-direct {v8, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$1;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v7, 0x7f07004a

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Lcom/youdao/note/activity/resource/ImageRectificationActivity$2;

    invoke-direct {v8, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$2;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v7, 0x7f07004b

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Lcom/youdao/note/activity/resource/ImageRectificationActivity$3;

    invoke-direct {v8, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$3;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v7, 0x7f07004c

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Lcom/youdao/note/activity/resource/ImageRectificationActivity$4;

    invoke-direct {v8, p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$4;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    .line 118
    .end local v2           #layoutInflator:Landroid/view/LayoutInflater;
    :goto_2
    return-void

    .line 69
    .restart local v2       #layoutInflator:Landroid/view/LayoutInflater;
    .restart local v3       #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    .restart local v5       #tmpFile:Ljava/io/File;
    :cond_1
    :try_start_3
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v8, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    .local v1, file:Ljava/io/File;
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 79
    .end local v1           #file:Ljava/io/File;
    .end local v3           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    .end local v5           #tmpFile:Ljava/io/File;
    :catch_0
    move-exception v0

    .line 80
    .local v0, e:Ljava/io/IOException;
    :try_start_4
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    const-string v8, "get resource by meta failed"

    invoke-static {v7, v8, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 108
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #layoutInflator:Landroid/view/LayoutInflater;
    :catch_1
    move-exception v0

    .line 109
    .local v0, e:Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 110
    const v7, 0x7f0a00ca

    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    .line 111
    .local v6, toast:Landroid/widget/Toast;
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 112
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_2

    .line 113
    iget-object v7, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 114
    iput-object v11, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->finish()V

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, dialog:Landroid/app/ProgressDialog;
    packed-switch p1, :pswitch_data_0

    .line 139
    :goto_0
    return-object v0

    .line 125
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    .end local v0           #dialog:Landroid/app/ProgressDialog;
    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 126
    .restart local v0       #dialog:Landroid/app/ProgressDialog;
    const-string v1, "\u6b63\u5728\u68c0\u6d4b\uff0c\u8bf7\u7a0d\u5019..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 128
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 131
    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    .end local v0           #dialog:Landroid/app/ProgressDialog;
    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 132
    .restart local v0       #dialog:Landroid/app/ProgressDialog;
    const-string v1, "\u6b63\u5728\u5904\u7406\uff0c\u8bf7\u7a0d\u5019..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 134
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->menuHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 145
    const-string v0, "WhiteboardActivity destroyed"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-super {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onDestroy()V

    .line 147
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 148
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->view:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->onDestroy()V

    .line 149
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 150
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 308
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->mainView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->setContentView(Landroid/view/View;)V

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;

    .line 312
    const/4 v0, 0x1

    .line 315
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
