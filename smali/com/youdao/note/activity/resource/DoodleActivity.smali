.class public Lcom/youdao/note/activity/resource/DoodleActivity;
.super Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;
.source "DoodleActivity.java"


# static fields
.field private static final DIALOG_BACK:I = 0x2

.field private static final DIALOG_TRASH:I = 0x1


# instance fields
.field private doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

.field private mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

.field private oldDoodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/resource/DoodleActivity;)Lcom/youdao/note/ui/skitch/doodle/IDoodle;
    .locals 1
    .parameter "x0"

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/resource/DoodleActivity;)Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;
    .locals 1
    .parameter "x0"

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/resource/DoodleActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/DoodleActivity;->giveupDoodle()V

    return-void
.end method

.method private giveupDoodle()V
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    .line 67
    .local v0, mYnote:Lcom/youdao/note/YNoteApplication;
    iget-object v1, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->oldDoodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->oldDoodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->addOrUpdateSkitchMeta(Ljava/lang/String;Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/DoodleActivity;->cancleAndBack()V

    .line 71
    return-void
.end method

.method private initViewListener()V
    .locals 2

    .prologue
    .line 73
    const v0, 0x7f07001d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$1;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v0, 0x7f070020

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$2;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v0, 0x7f07001e

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$3;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$3;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v0, 0x7f07001f

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$4;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$4;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    const v0, 0x7f070022

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$5;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$5;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/DoodleActivity;->showDialog(I)V

    .line 179
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter "savedInstanceState"

    .prologue
    const/16 v6, 0x8

    .line 42
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->requestWindowFeature(I)Z

    .line 43
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v4, 0x7f030008

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->setContentView(I)V

    .line 45
    const v4, 0x7f070019

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/ui/skitch/doodle/DoodleView;

    iput-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    .line 46
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/DoodleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 47
    .local v1, intent:Landroid/content/Intent;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    .line 48
    .local v2, mYnote:Lcom/youdao/note/YNoteApplication;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.youdao.note.action.EDIT_DOODLE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v4

    iget-object v5, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->getSkitchMeta(Ljava/lang/String;)Lcom/youdao/note/ui/skitch/ISkitchMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    iput-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    .line 50
    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v4}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->copy()Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    move-result-object v4

    iput-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->oldDoodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    .line 51
    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->setIsNew(Z)V

    .line 55
    :goto_0
    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    iget-object v5, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-interface {v4, v5}, Lcom/youdao/note/ui/skitch/doodle/IDoodle;->initSkitchMeta(Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 56
    const v4, 0x7f07001a

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;

    .line 57
    .local v3, sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->setSkitchCanvas(Lcom/youdao/note/ui/skitch/ISkitch;)V

    .line 59
    invoke-virtual {v3, v6}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->setVisibility(I)V

    .line 60
    const v4, 0x7f07001b

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/DoodleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;

    .line 61
    .local v0, colorBox:Lcom/youdao/note/ui/skitch/tool/ColorBoxView;
    iget-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->mDoodleView:Lcom/youdao/note/ui/skitch/doodle/IDoodle;

    invoke-virtual {v0, v4}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->setSkitchCanvas(Lcom/youdao/note/ui/skitch/ISkitch;)V

    .line 62
    invoke-virtual {v0, v6}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->setVisibility(I)V

    .line 63
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/DoodleActivity;->initViewListener()V

    .line 64
    return-void

    .line 53
    .end local v0           #colorBox:Lcom/youdao/note/ui/skitch/tool/ColorBoxView;
    .end local v3           #sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    :cond_0
    new-instance v4, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-direct {v4}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;-><init>()V

    iput-object v4, p0, Lcom/youdao/note/activity/resource/DoodleActivity;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const v3, 0x7f0a00aa

    const/4 v2, 0x1

    .line 127
    packed-switch p1, :pswitch_data_0

    .line 172
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 129
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00d4

    new-instance v2, Lcom/youdao/note/activity/resource/DoodleActivity$7;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$7;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$6;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$6;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 151
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/activity/resource/DoodleActivity$9;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$9;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/DoodleActivity$8;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/DoodleActivity$8;-><init>(Lcom/youdao/note/activity/resource/DoodleActivity;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
