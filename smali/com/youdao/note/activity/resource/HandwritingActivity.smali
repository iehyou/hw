.class public Lcom/youdao/note/activity/resource/HandwritingActivity;
.super Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;
.source "HandwritingActivity.java"


# static fields
.field private static final DIALOG_BACK:I = 0x2

.field public static final DIALOG_MODE_TIPS:I = 0x3

.field private static final DIALOG_TRASH:I = 0x1


# instance fields
.field private handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

.field private mHandler:Landroid/os/Handler;

.field private oldHandwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

.field private pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

.field private writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    .line 48
    iput-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
    .locals 1
    .parameter "x0"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    return-object v0
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/resource/HandwritingActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->dismissHandwriteGuide()V

    return-void
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/resource/HandwritingActivity;)Lcom/youdao/note/YNoteApplication;
    .locals 1
    .parameter "x0"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/resource/HandwritingActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->giveupHandwrite()V

    return-void
.end method

.method private dismissHandwriteGuide()V
    .locals 4

    .prologue
    .line 216
    const v2, 0x7f070045

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 217
    .local v1, guide:Landroid/view/View;
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 218
    .local v0, animation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 219
    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 220
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 221
    return-void
.end method

.method private giveupHandwrite()V
    .locals 4

    .prologue
    .line 224
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    .line 225
    .local v0, mYnote:Lcom/youdao/note/YNoteApplication;
    iget-object v1, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->oldHandwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    if-eqz v1, :cond_0

    .line 226
    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->oldHandwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->addOrUpdateSkitchMeta(Ljava/lang/String;Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->cancleAndBack()V

    .line 229
    return-void
.end method

.method private initViewListener()V
    .locals 2

    .prologue
    .line 113
    const v0, 0x7f07003e

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$2;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    const v0, 0x7f070040

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$3;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$3;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const v0, 0x7f07003f

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$4;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$4;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    const v0, 0x7f070041

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$5;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$5;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    const v0, 0x7f070044

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$6;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$6;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    const v0, 0x7f070042

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$7;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$7;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const v0, 0x7f070046

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$8;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$8;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$9;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$9;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 321
    const v0, 0x7f070045

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 322
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->showDialog(I)V

    .line 326
    :goto_0
    return-void

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->giveupHandwrite()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .parameter "savedInstanceState"

    .prologue
    const v9, 0x7f070045

    const/4 v8, 0x0

    .line 55
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/HandwritingActivity;->requestWindowFeature(I)Z

    .line 56
    const v4, 0x7f030016

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/HandwritingActivity;->setContentView(I)V

    .line 57
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v4, 0x7f07003a

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    .line 59
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 60
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.youdao.note.action.EDIT_HANDWRITE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 61
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v4

    iget-object v5, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->getSkitchMeta(Ljava/lang/String;)Lcom/youdao/note/ui/skitch/ISkitchMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    .line 63
    :try_start_0
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v4}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->copy()Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    move-result-object v4

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->oldHandwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_0
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v4, v8}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->setIsNew(Z)V

    .line 73
    :goto_1
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    iget-object v5, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->initSkitchMeta(Lcom/youdao/note/ui/skitch/ISkitchMeta;)V

    .line 74
    const v4, 0x7f07003c

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    .line 75
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 76
    .local v0, dm:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 77
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->init(FII)V

    .line 78
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    iget-object v5, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V

    .line 79
    const v4, 0x7f07003b

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;

    .line 80
    .local v3, sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->setSkitchCanvas(Lcom/youdao/note/ui/skitch/ISkitch;)V

    .line 81
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->setVisibility(I)V

    .line 83
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->isShowHandwriteGuide()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.youdao.note.action.NEW_HANDWRITE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 84
    invoke-virtual {p0, v9}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :goto_2
    invoke-direct {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->initViewListener()V

    .line 89
    iget-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/youdao/note/activity/resource/HandwritingActivity$1;

    invoke-direct {v5, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$1;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 96
    return-void

    .line 64
    .end local v0           #dm:Landroid/util/DisplayMetrics;
    .end local v3           #sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    :catch_0
    move-exception v1

    .line 65
    .local v1, e:Ljava/lang/OutOfMemoryError;
    const v4, 0x7f0a00ca

    invoke-static {p0, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 66
    invoke-virtual {p0}, Lcom/youdao/note/activity/resource/HandwritingActivity;->finish()V

    goto :goto_0

    .line 70
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    :cond_0
    new-instance v4, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-direct {v4}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;-><init>()V

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    .line 71
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->oldHandwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    goto/16 :goto_1

    .line 86
    .restart local v0       #dm:Landroid/util/DisplayMetrics;
    .restart local v3       #sliderView:Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
    :cond_1
    invoke-virtual {p0, v9}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const v3, 0x7f0a00aa

    const/4 v2, 0x1

    .line 233
    packed-switch p1, :pswitch_data_0

    .line 300
    invoke-super {p0, p1}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 235
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00cc

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00cd

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ce

    new-instance v2, Lcom/youdao/note/activity/resource/HandwritingActivity$11;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$11;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$10;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$10;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 257
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00cf

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/activity/resource/HandwritingActivity$13;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$13;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/resource/HandwritingActivity$12;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$12;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 278
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00fb

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00fc

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ff

    new-instance v2, Lcom/youdao/note/activity/resource/HandwritingActivity$15;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$15;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0100

    new-instance v2, Lcom/youdao/note/activity/resource/HandwritingActivity$14;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/resource/HandwritingActivity$14;-><init>(Lcom/youdao/note/activity/resource/HandwritingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onPause()V

    .line 108
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->onPause()V

    .line 109
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->setVisibility(I)V

    .line 110
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Lcom/youdao/note/activity/resource/BaseResourceNoteActivity;->onResume()V

    .line 101
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->onResume()V

    .line 103
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter "event"

    .prologue
    const/4 v5, 0x1

    .line 306
    const-string v3, "HandWriteActivity on touch called."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->pageView:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    .line 308
    .local v0, canvas:Landroid/view/View;
    const/4 v3, 0x2

    new-array v2, v3, [I

    .line 309
    .local v2, location:[I
    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 310
    aget v3, v2, v5

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int v1, v3, v4

    .line 311
    .local v1, canvasBottom:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    add-int/lit8 v4, v1, -0x14

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 312
    iget-object v3, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->setVisibility(I)V

    .line 313
    iget-object v3, p0, Lcom/youdao/note/activity/resource/HandwritingActivity;->writeViewLayout:Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;

    invoke-virtual {v3, p1}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 315
    :cond_0
    return v5
.end method
