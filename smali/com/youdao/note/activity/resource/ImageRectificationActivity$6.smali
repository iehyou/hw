.class Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;
.super Landroid/os/Handler;
.source "ImageRectificationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/resource/ImageRectificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 222
    iput-object p1, p0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .parameter "msg"

    .prologue
    .line 225
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    .line 226
    .local v20, yOffset:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    int-to-float v0, v5

    move/from16 v19, v0

    .line 227
    .local v19, screenWidth:F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    int-to-float v0, v5

    move/from16 v18, v0

    .line 228
    .local v18, screenHeight:F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 229
    new-instance v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    invoke-direct {v13, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 230
    .local v13, imageView:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$700(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v13, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 231
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v13, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 232
    new-instance v16, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    mul-int/lit8 v7, v20, 0x2

    add-int/2addr v6, v7

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 236
    .local v16, params:Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v5, 0x0

    div-int/lit8 v6, v20, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 237
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$700(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    .line 240
    .local v14, imageWidth:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->image:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$700(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    .line 241
    .local v12, imageHeight:I
    const/high16 v17, 0x3f80

    .line 242
    .local v17, scale:F
    int-to-float v5, v14

    div-float v5, v5, v19

    move/from16 v0, v17

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 243
    int-to-float v5, v12

    div-float v5, v5, v18

    move/from16 v0, v17

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 244
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndWidth:F
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$300(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)F

    move-result v5

    int-to-float v6, v14

    div-float/2addr v5, v6

    mul-float v2, v5, v17

    .line 245
    .local v2, fromX:F
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->firstAnimationEndHeight:F
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$400(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)F

    move-result v5

    int-to-float v6, v12

    div-float/2addr v5, v6

    mul-float v4, v5, v17

    .line 246
    .local v4, fromY:F
    int-to-float v5, v14

    div-float v5, v19, v5

    int-to-float v6, v12

    div-float v6, v18, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    mul-float v3, v5, v17

    .line 247
    .local v3, to:F
    const/high16 v5, 0x3f80

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 250
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    const/4 v6, 0x1

    const/high16 v7, 0x3f00

    const/4 v8, 0x1

    const/high16 v9, 0x3f00

    move v5, v3

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 252
    .local v1, scaleAnimation:Landroid/view/animation/ScaleAnimation;
    const-wide/16 v5, 0x320

    invoke-virtual {v1, v5, v6}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 253
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->confirmView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$500(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 254
    new-instance v10, Landroid/os/Handler;

    invoke-direct {v10}, Landroid/os/Handler;-><init>()V

    .line 255
    .local v10, handler:Landroid/os/Handler;
    new-instance v5, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6$1;-><init>(Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;)V

    const-wide/16 v6, 0x320

    invoke-virtual {v10, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 261
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v15

    .line 262
    .local v15, num:I
    const/4 v11, 0x0

    .local v11, i:I
    :goto_0
    if-ge v11, v15, :cond_0

    .line 263
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    #getter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v5}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$200(Lcom/youdao/note/activity/resource/ImageRectificationActivity;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/graphics/drawable/AnimationDrawable;->getFrame(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 262
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 265
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/activity/resource/ImageRectificationActivity$6;->this$0:Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    const/4 v6, 0x0

    #setter for: Lcom/youdao/note/activity/resource/ImageRectificationActivity;->animation:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v5, v6}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->access$202(Lcom/youdao/note/activity/resource/ImageRectificationActivity;Landroid/graphics/drawable/AnimationDrawable;)Landroid/graphics/drawable/AnimationDrawable;

    .line 266
    return-void
.end method
