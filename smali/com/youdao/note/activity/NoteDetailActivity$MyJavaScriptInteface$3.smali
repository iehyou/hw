.class Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;
.super Ljava/lang/Object;
.source "NoteDetailActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->viewResource(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

.field final synthetic val$resourceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 399
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iput-object p2, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->val$resourceId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private openResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 4
    .parameter "meta"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 433
    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->genRelativePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isImage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const/4 v1, 0x1

    #setter for: Lcom/youdao/note/activity/NoteDetailActivity;->mImgSelected:Z
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->access$502(Lcom/youdao/note/activity/NoteDetailActivity;Z)Z

    .line 435
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #setter for: Lcom/youdao/note/activity/NoteDetailActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-static {v0, p1}, Lcom/youdao/note/activity/NoteDetailActivity;->access$602(Lcom/youdao/note/activity/NoteDetailActivity;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 436
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->addViewPicTimes()Z

    .line 437
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/DataSource;->existResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->sendToViewResource()V
    invoke-static {v0}, Lcom/youdao/note/activity/NoteDetailActivity;->access$700(Lcom/youdao/note/activity/NoteDetailActivity;)V

    .line 459
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-virtual {v0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->removeDialog(I)V

    .line 442
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-virtual {v0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->showDialog(I)V

    .line 458
    :goto_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0, p1}, Lcom/youdao/note/task/TaskManager;->pullResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const/4 v1, 0x0

    #setter for: Lcom/youdao/note/activity/NoteDetailActivity;->mImgSelected:Z
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->access$502(Lcom/youdao/note/activity/NoteDetailActivity;Z)Z

    .line 445
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->addViewAttachTimes()Z

    .line 446
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/DataSource;->existResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 447
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->sendToViewResource()V
    invoke-static {v0}, Lcom/youdao/note/activity/NoteDetailActivity;->access$700(Lcom/youdao/note/activity/NoteDetailActivity;)V

    .line 448
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file:///android_asset/arrow.png"

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->access$800(Lcom/youdao/note/activity/NoteDetailActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/youdao/note/utils/UnitUtils;->getSize(J)Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->updateDownloadProgress(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->access$900(Lcom/youdao/note/activity/NoteDetailActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 454
    :cond_2
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file:///android_asset/juhua.gif"

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->access$800(Lcom/youdao/note/activity/NoteDetailActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 403
    :try_start_0
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resourceId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->val$resourceId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v7, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->val$resourceId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/youdao/note/datasource/DataSource;->getResourceMeta(Ljava/lang/String;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    .line 407
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    if-eqz v2, :cond_0

    .line 408
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v7, v7, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v7, v7, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v2}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 410
    .local v4, uri:Landroid/net/Uri;
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    #setter for: Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;
    invoke-static {v6, v7}, Lcom/youdao/note/activity/NoteDetailActivity;->access$402(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/content/Intent;)Landroid/content/Intent;

    .line 411
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #getter for: Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;
    invoke-static {v6}, Lcom/youdao/note/activity/NoteDetailActivity;->access$400(Lcom/youdao/note/activity/NoteDetailActivity;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 413
    .local v5, url:Ljava/lang/String;
    invoke-static {v5}, Lcom/youdao/note/utils/FileUtils;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 414
    .local v1, extension:Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 415
    .local v3, type:Ljava/lang/String;
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #getter for: Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;
    invoke-static {v6}, Lcom/youdao/note/activity/NoteDetailActivity;->access$400(Lcom/youdao/note/activity/NoteDetailActivity;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6, v4, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    invoke-direct {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->openResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 424
    .end local v1           #extension:Ljava/lang/String;
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v3           #type:Ljava/lang/String;
    .end local v4           #uri:Landroid/net/Uri;
    .end local v5           #url:Ljava/lang/String;
    :goto_0
    return-void

    .line 418
    .restart local v2       #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Lcom/youdao/note/activity/NoteDetailActivity;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 420
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :catch_0
    move-exception v0

    .line 421
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "no application."

    invoke-static {p0, v6, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 422
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$3;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v6, v6, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/youdao/note/activity/NoteDetailActivity;->showDialog(I)V

    goto :goto_0
.end method
