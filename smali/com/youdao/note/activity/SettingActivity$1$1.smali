.class Lcom/youdao/note/activity/SettingActivity$1$1;
.super Landroid/os/AsyncTask;
.source "SettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/SettingActivity$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/SettingActivity$1;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/SettingActivity$1;)V
    .locals 0
    .parameter

    .prologue
    .line 315
    iput-object p1, p0, Lcom/youdao/note/activity/SettingActivity$1$1;->this$1:Lcom/youdao/note/activity/SettingActivity$1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 315
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SettingActivity$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .parameter "params"

    .prologue
    .line 319
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity$1$1;->this$1:Lcom/youdao/note/activity/SettingActivity$1;

    iget-object v0, v0, Lcom/youdao/note/activity/SettingActivity$1;->this$0:Lcom/youdao/note/activity/SettingActivity;

    #getter for: Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v0}, Lcom/youdao/note/activity/SettingActivity;->access$000(Lcom/youdao/note/activity/SettingActivity;)Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->clearData()V

    .line 320
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity$1$1;->this$1:Lcom/youdao/note/activity/SettingActivity$1;

    iget-object v0, v0, Lcom/youdao/note/activity/SettingActivity$1;->this$0:Lcom/youdao/note/activity/SettingActivity;

    #getter for: Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v0}, Lcom/youdao/note/activity/SettingActivity;->access$000(Lcom/youdao/note/activity/SettingActivity;)Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->logOut()V

    .line 321
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 315
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SettingActivity$1$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 326
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity$1$1;->this$1:Lcom/youdao/note/activity/SettingActivity$1;

    iget-object v0, v0, Lcom/youdao/note/activity/SettingActivity$1;->this$0:Lcom/youdao/note/activity/SettingActivity;

    #getter for: Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v0}, Lcom/youdao/note/activity/SettingActivity;->access$000(Lcom/youdao/note/activity/SettingActivity;)Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity$1$1;->this$1:Lcom/youdao/note/activity/SettingActivity$1;

    iget-object v1, v1, Lcom/youdao/note/activity/SettingActivity$1;->this$0:Lcom/youdao/note/activity/SettingActivity;

    const-string v2, "com.youdao.note.action.login"

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/YNoteApplication;->sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V

    .line 329
    return-void
.end method
