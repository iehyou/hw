.class Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;
.super Lcom/youdao/note/data/adapter/BaseListAdapter;
.source "ResourceListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/ResourceListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResourceListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/data/adapter/BaseListAdapter",
        "<",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">;"
    }
.end annotation


# instance fields
.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field final synthetic this$0:Lcom/youdao/note/activity/ResourceListActivity;


# direct methods
.method public constructor <init>(Lcom/youdao/note/activity/ResourceListActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .parameter
    .parameter "context"
    .parameter "resourceId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 269
    .local p4, datas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    iput-object p1, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->this$0:Lcom/youdao/note/activity/ResourceListActivity;

    .line 270
    invoke-direct {p0, p2, p3, p4}, Lcom/youdao/note/data/adapter/BaseListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 271
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 272
    return-void
.end method

.method private fillView(ILandroid/view/View;)V
    .locals 10
    .parameter "position"
    .parameter "view"

    .prologue
    .line 291
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 292
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    const v6, 0x7f0700a8

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 293
    .local v5, thumbnail:Landroid/widget/ImageView;
    const v6, 0x7f0700a9

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 294
    .local v4, resourceSize:Landroid/widget/TextView;
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getSize(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    const v6, 0x7f0700aa

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 296
    .local v0, detail:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/youdao/note/data/resource/ResourceUtils;->hasThumbnail(Lcom/youdao/note/data/resource/IResourceMeta;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 297
    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 298
    invoke-static {}, Lcom/youdao/note/utils/BitmapMemCache;->getInstance()Lcom/youdao/note/utils/BitmapMemCache;

    move-result-object v6

    iget-object v7, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v2}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Landroid/widget/ImageView;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v9

    invoke-virtual {v6, v7, v8, v9}, Lcom/youdao/note/utils/BitmapMemCache;->getBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object v1, v2

    .line 301
    check-cast v1, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    .line 302
    .local v1, imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\u00d7"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getHeight()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    .end local v1           #imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
    :goto_0
    const v6, 0x7f070054

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    new-instance v7, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter$1;

    invoke-direct {v7, p0, p1}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter$1;-><init>(Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;I)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 318
    return-void

    .line 305
    :cond_0
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/youdao/note/utils/FileUtils;->getIconResouceId(Ljava/lang/String;)I

    move-result v3

    .line 306
    .local v3, res:I
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 307
    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 308
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected createViewFromResource(ILandroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3
    .parameter "position"
    .parameter "parent"
    .parameter "resourceId"

    .prologue
    .line 285
    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 286
    .local v0, view:Landroid/view/View;
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->fillView(ILandroid/view/View;)V

    .line 287
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 276
    if-eqz p2, :cond_0

    .line 277
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->fillView(ILandroid/view/View;)V

    .line 280
    .end local p2
    :goto_0
    return-object p2

    .restart local p2
    :cond_0
    iget v0, p0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->mResourceId:I

    invoke-virtual {p0, p1, p3, v0}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;->createViewFromResource(ILandroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
