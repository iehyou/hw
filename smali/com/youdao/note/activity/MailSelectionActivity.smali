.class public Lcom/youdao/note/activity/MailSelectionActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "MailSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/MailSelectionActivity$ListItem;,
        Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;,
        Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    }
.end annotation


# static fields
.field private static final BUNDLE_EMAILERS:Ljava/lang/String; = "emailers"

.field private static final DIALOG_RECEIVERS_LIMIT:I = 0x1

.field private static final INDEX_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final INDEX_SHARP:Ljava/lang/String; = "#"

.field private static final PATTERN:Ljava/lang/String; = "${email}<${name}>"


# instance fields
.field private emailers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/activity/MailSelectionActivity$Emailer;",
            ">;"
        }
    .end annotation
.end field

.field private listView:Landroid/widget/ListView;

.field private toast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 316
    const-string v0, "^[a-zA-Z]+.*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/activity/MailSelectionActivity;->INDEX_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    .line 558
    return-void
.end method

.method static synthetic access$1000(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity;->scrollTo(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/youdao/note/activity/MailSelectionActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/youdao/note/activity/MailSelectionActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/youdao/note/activity/MailSelectionActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->updateButtonText()V

    return-void
.end method

.method static synthetic access$600(Lcom/youdao/note/activity/MailSelectionActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->hideSoftKeyboard()V

    return-void
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/MailSelectionActivity;)Landroid/widget/Toast;
    .locals 1
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$800(Lcom/youdao/note/activity/MailSelectionActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->selectionDone()V

    return-void
.end method

.method static synthetic access$900(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/MailSelectionActivity;->doSearch(Ljava/lang/String;)V

    return-void
.end method

.method private doSearch(Ljava/lang/String;)V
    .locals 14
    .parameter "keyword"

    .prologue
    const/4 v13, 0x1

    const/4 v3, 0x0

    const/4 v12, 0x0

    .line 245
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 246
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 247
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 248
    .local v1, uri:Landroid/net/Uri;
    new-array v2, v13, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v12

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/youdao/note/activity/MailSelectionActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 249
    .local v8, cursor:Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v11, newList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/youdao/note/activity/MailSelectionActivity$Emailer;>;"
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 252
    .local v6, contactId:J
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, i$:Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    .line 253
    .local v9, e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->contactId:J
    invoke-static {v9}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$400(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-nez v0, :cond_1

    .line 254
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    .end local v6           #contactId:J
    .end local v9           #e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_2
    invoke-direct {p0, v11, v13}, Lcom/youdao/note/activity/MailSelectionActivity;->initUI(Ljava/util/ArrayList;Z)V

    .line 262
    .end local v1           #uri:Landroid/net/Uri;
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v11           #newList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/youdao/note/activity/MailSelectionActivity$Emailer;>;"
    :goto_1
    return-void

    .line 260
    :cond_3
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v12}, Lcom/youdao/note/activity/MailSelectionActivity;->initUI(Ljava/util/ArrayList;Z)V

    goto :goto_1
.end method

.method private getIndex(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "s"

    .prologue
    .line 318
    if-nez p1, :cond_0

    .line 319
    const-string v0, "#"

    .line 325
    :goto_0
    return-object v0

    .line 321
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 322
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/youdao/note/activity/MailSelectionActivity;->INDEX_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2

    .line 323
    :cond_1
    const-string v0, "#"

    goto :goto_0

    .line 325
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSelectedCount()I
    .locals 4

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    .local v0, count:I
    iget-object v3, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    .line 344
    .local v1, e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$1200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    .end local v1           #e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    :cond_1
    return v0
.end method

.method private hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 311
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MailSelectionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 313
    .local v0, inputMethodManager:Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 314
    return-void
.end method

.method private initData()V
    .locals 11

    .prologue
    .line 90
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "data1"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "display_name"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "data2"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "data3"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    const-string v3, "contact_id"

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "sort_key"

    aput-object v3, v2, v0

    const-string v3, "mimetype=\'vnd.android.cursor.item/email_v2\'"

    const/4 v4, 0x0

    const-string v5, "sort_key COLLATE LOCALIZED ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/youdao/note/activity/MailSelectionActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 96
    .local v6, cursor:Landroid/database/Cursor;
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 97
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance v8, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    const/4 v0, 0x0

    invoke-direct {v8, v0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;-><init>(Lcom/youdao/note/activity/MailSelectionActivity$1;)V

    .line 99
    .local v8, e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->address:Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$102(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;

    .line 100
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->name:Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$202(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;

    .line 101
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 102
    .local v10, type:I
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 103
    .local v7, customLabel:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v10, v7}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v9

    .line 105
    .local v9, emailType:Ljava/lang/CharSequence;
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->type:Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$302(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;

    .line 106
    const/4 v0, 0x5

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->contactId:J
    invoke-static {v8, v0, v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$402(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;J)J

    .line 107
    const/4 v0, 0x6

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity;->getIndex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    #setter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->index:Ljava/lang/String;
    invoke-static {v8, v0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$502(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    .end local v7           #customLabel:Ljava/lang/String;
    .end local v8           #e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    .end local v9           #emailType:Ljava/lang/CharSequence;
    .end local v10           #type:I
    :cond_0
    return-void
.end method

.method private initListener()V
    .locals 5

    .prologue
    .line 141
    const v3, 0x7f07006e

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/MailSelectionActivity$2;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/MailSelectionActivity$2;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    const v3, 0x7f07006f

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/MailSelectionActivity$3;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/MailSelectionActivity$3;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v3, 0x7f070067

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    .line 154
    .local v2, searchBar:Landroid/widget/AutoCompleteTextView;
    const v3, 0x7f070068

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 155
    .local v0, clearButton:Landroid/widget/ImageButton;
    new-instance v3, Lcom/youdao/note/activity/MailSelectionActivity$4;

    invoke-direct {v3, p0, v2, v0}, Lcom/youdao/note/activity/MailSelectionActivity$4;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;Landroid/widget/AutoCompleteTextView;Landroid/widget/ImageButton;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 175
    new-instance v3, Lcom/youdao/note/activity/MailSelectionActivity$5;

    invoke-direct {v3, p0, v2}, Lcom/youdao/note/activity/MailSelectionActivity$5;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;Landroid/widget/AutoCompleteTextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    const v3, 0x7f07006b

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/ui/AlphabetScrollbar;

    .line 183
    .local v1, scrollbar:Lcom/youdao/note/ui/AlphabetScrollbar;
    new-instance v3, Lcom/youdao/note/activity/MailSelectionActivity$6;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/MailSelectionActivity$6;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v1, v3}, Lcom/youdao/note/ui/AlphabetScrollbar;->setOnAlphabetListener(Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;)V

    .line 194
    iget-object v3, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    new-instance v4, Lcom/youdao/note/activity/MailSelectionActivity$7;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/MailSelectionActivity$7;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 234
    return-void
.end method

.method private initUI(Ljava/util/ArrayList;Z)V
    .locals 6
    .parameter
    .parameter "isSearch"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/activity/MailSelectionActivity$Emailer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/youdao/note/activity/MailSelectionActivity$Emailer;>;"
    const v4, 0x7f070069

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 114
    .local v1, contacts:Landroid/view/View;
    const v4, 0x7f07006b

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/ui/AlphabetScrollbar;

    .line 115
    .local v3, scrollbar:Lcom/youdao/note/ui/AlphabetScrollbar;
    iget-object v4, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    new-instance v5, Lcom/youdao/note/activity/MailSelectionActivity$1;

    invoke-direct {v5, p0}, Lcom/youdao/note/activity/MailSelectionActivity$1;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    const v4, 0x7f07006c

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 124
    .local v2, emptyView:Landroid/view/View;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 125
    :cond_0
    invoke-static {v1}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 126
    invoke-static {v2}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 133
    :goto_0
    if-eqz p2, :cond_2

    .line 134
    invoke-static {v3}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 138
    :goto_1
    return-void

    .line 128
    :cond_1
    invoke-static {v2}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 129
    invoke-static {v1}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 130
    new-instance v0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;

    invoke-direct {v0, p0, p1, p0, p2}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/util/List;Landroid/content/Context;Z)V

    .line 131
    .local v0, adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    iget-object v4, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 136
    .end local v0           #adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    :cond_2
    invoke-static {v3}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    goto :goto_1
.end method

.method private scrollTo(Ljava/lang/String;)V
    .locals 4
    .parameter "index"

    .prologue
    .line 237
    iget-object v2, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;

    .line 238
    .local v0, adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getPositionByAlphabet(Ljava/lang/String;)I

    move-result v1

    .line 239
    .local v1, position:I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 242
    :cond_0
    return-void
.end method

.method private selectionDone()V
    .locals 10

    .prologue
    .line 265
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    .local v6, sb:Ljava/lang/StringBuilder;
    const/4 v4, 0x1

    .line 267
    .local v4, isFirst:Z
    const/4 v0, 0x0

    .line 268
    .local v0, count:I
    iget-object v7, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;

    .line 269
    .local v1, e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$1200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 270
    if-nez v4, :cond_1

    .line 271
    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_1
    const/4 v4, 0x0

    .line 274
    const-string v7, "${email}<${name}>"

    const-string v8, "${email}"

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->address:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$100(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "${name}"

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->name:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->access$200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 275
    .local v5, s:Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    .end local v1           #e:Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
    .end local v5           #s:Ljava/lang/String;
    :cond_2
    const/4 v7, 0x3

    if-le v0, v7, :cond_3

    .line 280
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/youdao/note/activity/MailSelectionActivity;->showDialog(I)V

    .line 287
    :goto_1
    return-void

    .line 282
    :cond_3
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 283
    .local v3, intent:Landroid/content/Intent;
    const-string v7, "emails"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    const/4 v7, -0x1

    invoke-virtual {p0, v7, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->setResult(ILandroid/content/Intent;)V

    .line 285
    invoke-virtual {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->finish()V

    goto :goto_1
.end method

.method private updateButtonText()V
    .locals 4

    .prologue
    const v3, 0x7f0a00f1

    .line 350
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->getSelectedCount()I

    move-result v1

    .line 351
    .local v1, count:I
    const v2, 0x7f07006e

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 352
    .local v0, btn:Landroid/widget/Button;
    if-nez v1, :cond_0

    .line 353
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/MailSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 76
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity;->requestWindowFeature(I)Z

    .line 77
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity;->setRequestedOrientation(I)V

    .line 78
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity;->setContentView(I)V

    .line 80
    const v0, 0x7f07006a

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;

    .line 81
    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;

    .line 82
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03002a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 83
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setDuration(I)V

    .line 84
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 85
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->initData()V

    .line 86
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity;->initListener()V

    .line 87
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter "id"
    .parameter "args"

    .prologue
    .line 292
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 293
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/activity/MailSelectionActivity$8;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MailSelectionActivity$8;-><init>(Lcom/youdao/note/activity/MailSelectionActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 305
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/LockableActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 337
    const-string v0, "emailers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    .line 338
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 339
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/youdao/note/activity/MailSelectionActivity;->initUI(Ljava/util/ArrayList;Z)V

    .line 331
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onResume()V

    .line 332
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 361
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 362
    const-string v0, "emailers"

    iget-object v1, p0, Lcom/youdao/note/activity/MailSelectionActivity;->emailers:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 364
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 365
    return-void
.end method
