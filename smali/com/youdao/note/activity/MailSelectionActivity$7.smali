.class Lcom/youdao/note/activity/MailSelectionActivity$7;
.super Ljava/lang/Object;
.source "MailSelectionActivity.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/MailSelectionActivity;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/MailSelectionActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/MailSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 194
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$7;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    .prologue
    .line 204
    iget-object v4, p0, Lcom/youdao/note/activity/MailSelectionActivity$7;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    const v5, 0x7f07006b

    invoke-virtual {v4, v5}, Lcom/youdao/note/activity/MailSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/ui/AlphabetScrollbar;

    .line 205
    .local v3, scrollbar:Lcom/youdao/note/ui/AlphabetScrollbar;
    invoke-virtual {v3}, Lcom/youdao/note/ui/AlphabetScrollbar;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 206
    iget-object v4, p0, Lcom/youdao/note/activity/MailSelectionActivity$7;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity;->listView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/youdao/note/activity/MailSelectionActivity;->access$1100(Lcom/youdao/note/activity/MailSelectionActivity;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;

    .line 207
    .local v0, adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    if-nez v0, :cond_1

    .line 216
    .end local v0           #adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    :cond_0
    :goto_0
    return-void

    .line 210
    .restart local v0       #adapter:Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;
    :cond_1
    invoke-virtual {v0, p2}, Lcom/youdao/note/activity/MailSelectionActivity$EmailerListAdapter;->getAlphabetByPosition(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, alphabet:Ljava/lang/String;
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 212
    .local v2, msg:Landroid/os/Message;
    const/4 v4, 0x1

    iput v4, v2, Landroid/os/Message;->what:I

    .line 213
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "alphabet"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v4, v3, Lcom/youdao/note/ui/AlphabetScrollbar;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 199
    return-void
.end method
