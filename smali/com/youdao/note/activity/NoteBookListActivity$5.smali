.class Lcom/youdao/note/activity/NoteBookListActivity$5;
.super Ljava/lang/Object;
.source "NoteBookListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteBookListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteBookListActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteBookListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 279
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 283
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->newBookEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$800(Lcom/youdao/note/activity/NoteBookListActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    #setter for: Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$702(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 285
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$700(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    const v2, 0x7f0a0014

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;
    invoke-static {v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$700(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/youdao/note/activity/NoteBookListActivity;->checkNotebookNameExist(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$900(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$700(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/youdao/note/data/DataFactory;->newNotebookMeta(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 292
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, v0}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    .line 294
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$5;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #calls: Lcom/youdao/note/activity/NoteBookListActivity;->update()V
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$400(Lcom/youdao/note/activity/NoteBookListActivity;)V

    goto :goto_0
.end method
