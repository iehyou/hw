.class public Lcom/youdao/note/activity/MainActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "MainActivity.java"


# static fields
.field private static final BUNDLE_SHOW_LICENSE:Ljava/lang/String; = "license"


# instance fields
.field private mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

.field private magicNumbers:[Ljava/lang/Integer;

.field private queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resumedTime:J

.field private showingLicense:Z

.field private syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x12

    const/4 v3, 0x0

    .line 47
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 51
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/activity/MainActivity;->magicNumbers:[Ljava/lang/Integer;

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/activity/MainActivity;->resumedTime:J

    .line 58
    iput-boolean v3, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    .line 60
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/MainActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/youdao/note/activity/MainActivity;->checkReport()V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/MainActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/youdao/note/activity/MainActivity;->checkNewFeatureGuide()V

    return-void
.end method

.method static synthetic access$202(Lcom/youdao/note/activity/MainActivity;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    return p1
.end method

.method private checkNewFeatureGuide()V
    .locals 2

    .prologue
    .line 197
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isFirstTime()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/GuideActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 201
    .end local v0           #intent:Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private checkReport()V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->isEverReport()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/youdao/note/activity/MainActivity;->resumedTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    sget v1, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_FIRSTLAUNCH:I

    invoke-virtual {v0, v1}, Lcom/youdao/note/LogRecorder;->report(I)V

    .line 237
    :cond_0
    return-void
.end method


# virtual methods
.method protected needLock()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 113
    packed-switch p1, :pswitch_data_0

    .line 123
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 125
    :goto_1
    return-void

    .line 115
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 116
    const-string v0, "login succeed."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->syncIfNeed()V

    goto :goto_0

    .line 119
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 68
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MainActivity;->requestWindowFeature(I)Z

    .line 69
    const v1, 0x7f03002c

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/MainActivity;->setContentView(I)V

    .line 70
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    new-instance v1, Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    .line 72
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    new-instance v2, Lcom/youdao/note/activity/MainActivity$1;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/MainActivity$1;-><init>(Lcom/youdao/note/activity/MainActivity;)V

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->setSyncListener(Lcom/youdao/note/activity/delegate/SyncbarDelegate$SyncListener;)V

    .line 79
    const-string v1, "com.youdao.note.action.FINISH"

    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->finishLockable()V

    .line 82
    :cond_0
    new-instance v1, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    .line 87
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->isSizeSet()Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 89
    .local v0, metric:Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 90
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->setScreenParameters(III)V

    .line 92
    .end local v0           #metric:Landroid/util/DisplayMetrics;
    :cond_1
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter "id"

    .prologue
    .line 145
    sparse-switch p1, :sswitch_data_0

    .line 192
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 147
    :sswitch_0
    iget-object v3, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createCheckingUpdateDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 149
    :sswitch_1
    iget-object v3, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createCheckUpdateFailedDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 151
    :sswitch_2
    iget-object v3, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createNewVersionFoundDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 153
    :sswitch_3
    iget-object v3, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createNoUpdateFoundDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 155
    :sswitch_4
    new-instance v0, Lcom/youdao/note/ui/YNoteDialog;

    const v3, 0x7f0a0039

    invoke-direct {v0, p0, v3}, Lcom/youdao/note/ui/YNoteDialog;-><init>(Landroid/content/Context;I)V

    .line 156
    .local v0, dialog:Lcom/youdao/note/ui/YNoteDialog;
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 157
    .local v1, inflater:Landroid/view/LayoutInflater;
    const v3, 0x7f030020

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 158
    .local v2, view:Landroid/view/View;
    invoke-virtual {v0, v2}, Lcom/youdao/note/ui/YNoteDialog;->setView(Landroid/view/View;)V

    .line 159
    const v3, 0x7f0a00c9

    invoke-virtual {v0, v3}, Lcom/youdao/note/ui/YNoteDialog;->setTitle(I)V

    .line 160
    const v3, 0x7f070057

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/MainActivity$2;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/MainActivity$2;-><init>(Lcom/youdao/note/activity/MainActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    const v3, 0x7f070058

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/MainActivity$3;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/MainActivity$3;-><init>(Lcom/youdao/note/activity/MainActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    new-instance v3, Lcom/youdao/note/activity/MainActivity$4;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/MainActivity$4;-><init>(Lcom/youdao/note/activity/MainActivity;)V

    invoke-virtual {v0, v3}, Lcom/youdao/note/ui/YNoteDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 183
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    goto :goto_0

    .line 186
    .end local v0           #dialog:Lcom/youdao/note/ui/YNoteDialog;
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    .end local v2           #view:Landroid/view/View;
    :sswitch_5
    invoke-static {p0}, Lcom/youdao/note/ui/DialogFactory;->getAboutDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 189
    :sswitch_6
    invoke-static {p0}, Lcom/youdao/note/ui/DialogFactory;->getNewFeaturesDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x6c -> :sswitch_5
        0x6d -> :sswitch_4
        0x72 -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 294
    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 295
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f05

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 296
    const/4 v1, 0x1

    return v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 301
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onDestroy()V

    .line 302
    return-void
.end method

.method protected onIntentSend(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, clazz:Ljava/lang/String;
    const-class v1, Lcom/youdao/note/activity/NewNoteActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->addIconNewTimes()Z

    .line 229
    .end local v0           #clazz:Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/youdao/note/activity/MainActivity;->checkReport()V

    .line 230
    return-void

    .line 223
    .restart local v0       #clazz:Ljava/lang/String;
    :cond_1
    const-class v1, Lcom/youdao/note/activity/NoteListActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 224
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->addIconAllTimes()Z

    goto :goto_0

    .line 225
    :cond_2
    const-class v1, Lcom/youdao/note/activity/NoteBookListActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v1}, Lcom/youdao/note/LogRecorder;->addIconNotebookTimes()Z

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x0

    .line 306
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " keycode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 308
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/activity/MainActivity;->magicNumbers:[Ljava/lang/Integer;

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 309
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_0

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/activity/MainActivity;->magicNumbers:[Ljava/lang/Integer;

    array-length v2, v2

    if-ne v1, v2, :cond_2

    .line 312
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->queue:Ljava/util/Queue;

    new-array v2, v0, [Ljava/lang/Integer;

    invoke-interface {v1, v2}, Ljava/util/Queue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/MainActivity;->magicNumbers:[Ljava/lang/Integer;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    iget-object v2, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->isDebug()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Lcom/youdao/note/YNoteApplication;->setDebug(Z)V

    .line 314
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->clearData()V

    .line 315
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->sendLogin()V

    .line 318
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/LockableActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 205
    const-string v0, "Main on new intent called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.youdao.note.action.login"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->sendLogin()V

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const-string v0, "com.youdao.note.action.FINISH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->finishLockable()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .parameter "item"

    .prologue
    const v8, 0x7f0700ba

    const/4 v7, 0x2

    const/4 v4, 0x1

    .line 247
    iget-object v5, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v5, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 284
    :goto_0
    return v4

    .line 250
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b7

    if-ne v5, v6, :cond_1

    .line 251
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/youdao/note/activity/WebActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    .local v2, intent:Landroid/content/Intent;
    const-string v5, "web_conetnt"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 253
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 255
    .end local v2           #intent:Landroid/content/Intent;
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b8

    if-ne v5, v6, :cond_2

    .line 256
    iget-object v5, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->logOut()V

    .line 257
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/youdao/note/activity/LoginActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .restart local v2       #intent:Landroid/content/Intent;
    const/4 v5, 0x3

    invoke-virtual {p0, v2, v5}, Lcom/youdao/note/activity/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 260
    .end local v2           #intent:Landroid/content/Intent;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 261
    const/16 v5, 0x6c

    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/MainActivity;->showDialog(I)V

    goto :goto_0

    .line 263
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    if-ne v5, v8, :cond_4

    .line 264
    const-string v5, "mailto:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 265
    .local v1, emailTOUri:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v2, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 266
    .restart local v2       #intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a005b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, body:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a005a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 268
    .local v3, subject:Ljava/lang/String;
    const-string v5, "text/plain"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v5, "android.intent.extra.EMAIL"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    const-string v5, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v5, "web_conetnt"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 276
    .end local v0           #body:Ljava/lang/String;
    .end local v1           #emailTOUri:Landroid/net/Uri;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #subject:Ljava/lang/String;
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0700bb

    if-ne v4, v5, :cond_5

    .line 277
    iget-object v4, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->stopAllTask()V

    .line 278
    invoke-virtual {p0}, Lcom/youdao/note/activity/MainActivity;->finish()V

    .line 279
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V

    .line 281
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0700b9

    if-ne v4, v5, :cond_6

    .line 282
    iget-object v4, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v4}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->checkUpdate()V

    .line 284
    :cond_6
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 241
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 242
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 101
    const-string v0, "license"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    .line 102
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onRestoreState(Landroid/os/Bundle;)V

    .line 103
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onResume()V

    .line 130
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onResume()V

    .line 131
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->licenseAccepted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    iget-boolean v0, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    if-nez v0, :cond_0

    .line 133
    const/16 v0, 0x6d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/MainActivity;->showDialog(I)V

    .line 138
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->syncIfNeed()V

    .line 139
    const-string v0, "onResume called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/youdao/note/activity/MainActivity;->resumedTime:J

    .line 141
    return-void

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/activity/MainActivity;->checkNewFeatureGuide()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 95
    const-string v0, "license"

    iget-boolean v1, p0, Lcom/youdao/note/activity/MainActivity;->showingLicense:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 96
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onSaveState(Landroid/os/Bundle;)V

    .line 97
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 98
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onStart()V

    .line 109
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 1
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 323
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 324
    const/16 v0, 0x15

    if-ne p1, v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/youdao/note/activity/MainActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p2, p3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onCheckUpdateResult(Lcom/youdao/note/data/BaseData;Z)V

    .line 329
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0
.end method
