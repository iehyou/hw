.class public Lcom/youdao/note/activity/EditNoteActivity;
.super Lcom/youdao/note/activity/BaseEditActivity;
.source "EditNoteActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/EditNoteActivity$YNoteWebViewClient;,
        Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;
    }
.end annotation


# static fields
.field private static final JS_GETBODY:Ljava/lang/String; = "javascript:window.Htmlout.onBodyFetched(document.body.innerHTML, %s)"


# instance fields
.field private final handler:Landroid/os/Handler;

.field private mEditTextView:Landroid/widget/EditText;

.field private mEditView:Landroid/view/View;

.field private saveButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    .line 46
    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;

    .line 48
    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->saveButton:Landroid/view/View;

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->handler:Landroid/os/Handler;

    .line 236
    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/EditNoteActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->dismissFooterBar()V

    return-void
.end method

.method private dismissFooterBar()V
    .locals 2

    .prologue
    .line 300
    const v0, 0x7f07002d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 301
    return-void
.end method

.method private editDone()V
    .locals 4

    .prologue
    .line 177
    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\""

    const-string v3, "\\\""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, "\\n"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, editText:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "javascript:editDone(\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->hideEditView()V

    .line 180
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->showFooterBar()V

    .line 181
    return-void
.end method

.method private hideEditView()V
    .locals 3

    .prologue
    .line 290
    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 292
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/EditNoteActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 293
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 297
    return-void
.end method

.method private showFooterBar()V
    .locals 2

    .prologue
    .line 304
    const v0, 0x7f07002d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 305
    return-void
.end method


# virtual methods
.method protected initControls()V
    .locals 3

    .prologue
    .line 152
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->initControls()V

    .line 153
    const v2, 0x7f07002e

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->saveButton:Landroid/view/View;

    .line 154
    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->saveButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v2, 0x7f070014

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 156
    .local v1, okView:Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 158
    .local v0, cancelView:Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    return-void
.end method

.method protected loadNote(Lcom/youdao/note/data/Note;)V
    .locals 6
    .parameter "note"

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->loadNote(Lcom/youdao/note/data/Note;)V

    .line 129
    iget-boolean v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->loadDismissed:Z

    if-nez v0, :cond_0

    .line 130
    const/16 v0, 0xc9

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->dismissDialog(I)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    iput-object p1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    .line 134
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/"

    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getEditableHtml()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    return-void
.end method

.method protected obtainNote(Z)V
    .locals 7
    .parameter "saveOnObtained"

    .prologue
    .line 188
    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, title:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "modify title is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    .line 191
    .local v0, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteMeta;->setTitle(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    const-string v3, "javascript:window.Htmlout.onBodyFetched(document.body.innerHTML, %s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070014

    if-ne v0, v1, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->editDone()V

    .line 174
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070015

    if-ne v0, v1, :cond_1

    .line 166
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->hideEditView()V

    .line 167
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->showFooterBar()V

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f07002e

    if-ne v0, v1, :cond_2

    .line 169
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->editDone()V

    .line 170
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->obtainNote(Z)V

    goto :goto_0

    .line 172
    :cond_2
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->requestWindowFeature(I)Z

    .line 58
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->setContentView(I)V

    .line 59
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f070032

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    .line 61
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new webview is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;-><init>(Lcom/youdao/note/activity/EditNoteActivity;)V

    const-string v2, "Htmlout"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/youdao/note/activity/EditNoteActivity$YNoteWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/youdao/note/activity/EditNoteActivity$YNoteWebViewClient;-><init>(Lcom/youdao/note/activity/EditNoteActivity;Lcom/youdao/note/activity/EditNoteActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 65
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 66
    const v0, 0x7f070031

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;

    .line 68
    const-string v0, "onCreate called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->oriNoteBookId:Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mNoteBookId:Ljava/lang/String;

    .line 70
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-gt v0, v1, :cond_0

    .line 71
    const v0, 0x7f0a0017

    invoke-static {p0, v0}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 73
    :cond_0
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 245
    packed-switch p1, :pswitch_data_0

    .line 261
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 247
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 248
    .local v0, progressDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/youdao/note/activity/EditNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0099

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 251
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 252
    new-instance v1, Lcom/youdao/note/activity/EditNoteActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/EditNoteActivity$1;-><init>(Lcom/youdao/note/activity/EditNoteActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->onDestroy()V

    .line 106
    const-string v0, "OnDestroy called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 267
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 268
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->editDone()V

    .line 269
    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 281
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 282
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->hideEditView()V

    .line 283
    invoke-direct {p0}, Lcom/youdao/note/activity/EditNoteActivity;->showFooterBar()V

    .line 284
    const/4 v0, 0x1

    .line 286
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseEditActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->onPause()V

    .line 124
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 148
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->onResume()V

    .line 118
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter "bundle"

    .prologue
    .line 142
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 143
    return-void
.end method
