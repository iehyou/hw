.class public Lcom/youdao/note/activity/LoginActivity;
.super Lcom/youdao/note/activity/BaseActivity;
.source "LoginActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field private static final BUNDLE_OFFLIE_RESOURCES:Ljava/lang/String; = "offline_resources"

.field private static final BUNDLE_OFFLINE_NOTES:Ljava/lang/String; = "offline_notes"

.field private static final LOGINING_DIALOG:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private account:Landroid/widget/EditText;

.field private mHelpButton:Landroid/widget/Button;

.field private mLayout:Lcom/youdao/note/ui/ResizeableLayout;

.field private mLoginButton:Landroid/widget/Button;

.field private mLoginLogo:Landroid/view/View;

.field private mLoginResult:Lcom/youdao/note/data/LoginResult;

.field private mRegistButton:Landroid/widget/Button;

.field private mRegistHelpView:Landroid/view/View;

.field private offlineNotes:[Lcom/youdao/note/data/Note;

.field private password:Landroid/widget/EditText;

.field private resourceLists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/youdao/note/activity/LoginActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/activity/LoginActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginButton:Landroid/widget/Button;

    .line 66
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistButton:Landroid/widget/Button;

    .line 68
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mHelpButton:Landroid/widget/Button;

    .line 70
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    .line 72
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    .line 74
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginLogo:Landroid/view/View;

    .line 76
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistHelpView:Landroid/view/View;

    .line 78
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mLayout:Lcom/youdao/note/ui/ResizeableLayout;

    .line 82
    new-array v0, v2, [Lcom/youdao/note/data/Note;

    iput-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    .line 205
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginLogo:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistHelpView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/LoginActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/youdao/note/activity/LoginActivity;->clearLoginStatuse()V

    return-void
.end method

.method private clearLoginStatuse()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setPersistCookie(Ljava/lang/String;)Z

    .line 304
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setUserName(Ljava/lang/String;)Z

    .line 305
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setPassword(Ljava/lang/String;)Z

    .line 306
    return-void
.end method

.method private isValidUserName(Ljava/lang/String;)Z
    .locals 2
    .parameter "userName"

    .prologue
    const/4 v0, 0x0

    .line 363
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 369
    :cond_0
    :goto_0
    return v0

    .line 366
    :cond_1
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 369
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private login()V
    .locals 4

    .prologue
    .line 325
    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, userName:Ljava/lang/String;
    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, passWord:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 328
    :cond_0
    const v3, 0x7f0a0022

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 333
    :goto_0
    return-void

    .line 331
    :cond_1
    invoke-static {v0}, Lcom/youdao/note/utils/MD5Digester;->digest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 332
    .local v1, pwdMd5:Ljava/lang/String;
    invoke-direct {p0, v2, v1}, Lcom/youdao/note/activity/LoginActivity;->startLogin(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private loginFailed(Lcom/youdao/note/data/BaseData;)V
    .locals 5
    .parameter "data"

    .prologue
    .line 282
    move-object v0, p1

    check-cast v0, Lcom/youdao/note/data/RemoteErrorData;

    .line 283
    .local v0, error:Lcom/youdao/note/data/RemoteErrorData;
    invoke-virtual {v0}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v1

    .line 284
    .local v1, exception:Ljava/lang/Exception;
    instance-of v3, v1, Lcom/youdao/note/exceptions/LoginException;

    if-eqz v3, :cond_3

    move-object v2, v1

    .line 285
    check-cast v2, Lcom/youdao/note/exceptions/LoginException;

    .line 286
    .local v2, le:Lcom/youdao/note/exceptions/LoginException;
    invoke-direct {p0}, Lcom/youdao/note/activity/LoginActivity;->clearLoginStatuse()V

    .line 287
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/LoginException;->getLoginResultCode()I

    move-result v3

    const/16 v4, 0x1cc

    if-ne v3, v4, :cond_1

    .line 288
    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 289
    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->selectAll()V

    .line 290
    const v3, 0x7f0a00be

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 300
    .end local v2           #le:Lcom/youdao/note/exceptions/LoginException;
    :cond_0
    :goto_0
    return-void

    .line 291
    .restart local v2       #le:Lcom/youdao/note/exceptions/LoginException;
    :cond_1
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/LoginException;->getLoginResultCode()I

    move-result v3

    const/16 v4, 0x1a4

    if-ne v3, v4, :cond_2

    .line 292
    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 293
    const v3, 0x7f0a00bf

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 294
    :cond_2
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/LoginException;->getLoginResultCode()I

    move-result v3

    const/16 v4, 0x19c

    if-ne v3, v4, :cond_0

    .line 295
    const v3, 0x7f0a0015

    invoke-static {p0, v3}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 298
    .end local v2           #le:Lcom/youdao/note/exceptions/LoginException;
    :cond_3
    invoke-virtual {p0}, Lcom/youdao/note/activity/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0003

    invoke-static {v3, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private startLogin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "userName"
    .parameter "passWord"

    .prologue
    .line 336
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 340
    const-string v2, "@"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@163.com"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 343
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 344
    .local v1, tmpUserName:Ljava/lang/String;
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, tmpPassWord:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "password is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0, v1}, Lcom/youdao/note/activity/LoginActivity;->isValidUserName(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 347
    const v2, 0x7f0a003a

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 350
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 353
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 354
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->hideKeyboard(Landroid/os/IBinder;)V

    .line 358
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v2, v1, v0}, Lcom/youdao/note/task/TaskManager;->login(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->showDialog(I)V

    goto :goto_0

    .line 355
    :cond_5
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 356
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->hideKeyboard(Landroid/os/IBinder;)V

    goto :goto_1
.end method


# virtual methods
.method public bindEvents()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->mHelpButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 376
    packed-switch p1, :pswitch_data_0

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 378
    :pswitch_0
    const-string v2, "Regist succeed."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 379
    if-eqz p3, :cond_0

    .line 380
    const-string v2, "username"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, userName:Ljava/lang/String;
    const-string v2, "password"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, passWord:Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lcom/youdao/note/activity/LoginActivity;->startLogin(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 310
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070060

    if-ne v1, v2, :cond_1

    .line 311
    invoke-direct {p0}, Lcom/youdao/note/activity/LoginActivity;->login()V

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070063

    if-ne v1, v2, :cond_2

    .line 313
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/WebActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "web_conetnt"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 315
    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 316
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070064

    if-ne v1, v2, :cond_0

    .line 317
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/WebActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v1, "web_conetnt"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 319
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 88
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->requestWindowFeature(I)Z

    .line 89
    const v2, 0x7f030025

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->setContentView(I)V

    .line 90
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v2, 0x7f070060

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginButton:Landroid/widget/Button;

    .line 92
    const v2, 0x7f07005e

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    .line 93
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getLastLoginUserName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getLastLoginUserName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->account:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v2, 0x7f07005f

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    .line 98
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->password:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 100
    const v2, 0x7f070063

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistButton:Landroid/widget/Button;

    .line 101
    const v2, 0x7f070064

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mHelpButton:Landroid/widget/Button;

    .line 102
    const v2, 0x7f070061

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mLoginLogo:Landroid/view/View;

    .line 103
    const v2, 0x7f070062

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mRegistHelpView:Landroid/view/View;

    .line 104
    const v2, 0x7f070065

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 106
    .local v0, mCopyRight:Landroid/view/View;
    const v2, 0x7f07005c

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 107
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_1

    .line 108
    check-cast v1, Lcom/youdao/note/ui/ResizeableLayout;

    .end local v1           #view:Landroid/view/View;
    iput-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->mLayout:Lcom/youdao/note/ui/ResizeableLayout;

    .line 109
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mLayout:Lcom/youdao/note/ui/ResizeableLayout;

    new-instance v3, Lcom/youdao/note/activity/LoginActivity$1;

    invoke-direct {v3, p0, v0}, Lcom/youdao/note/activity/LoginActivity$1;-><init>(Lcom/youdao/note/activity/LoginActivity;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Lcom/youdao/note/ui/ResizeableLayout;->setSizeChangeListener(Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/LoginActivity;->bindEvents()V

    .line 129
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 130
    iget-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getTaskManager()Lcom/youdao/note/task/TaskManager;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/LoginActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 131
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    .line 175
    packed-switch p1, :pswitch_data_0

    .line 187
    :goto_0
    return-object v0

    .line 177
    :pswitch_0
    const v1, 0x7f0a0067

    const/4 v2, 0x0

    new-instance v3, Lcom/youdao/note/activity/LoginActivity$2;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/LoginActivity$2;-><init>(Lcom/youdao/note/activity/LoginActivity;)V

    invoke-static {v0, v1, p0, v2, v3}, Lcom/youdao/note/ui/DialogFactory;->getCancleableProgressDialog(Ljava/lang/String;ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 158
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onDestroy()V

    .line 159
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "actionId is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    if-ne p2, v0, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/youdao/note/activity/LoginActivity;->login()V

    .line 397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 193
    const/4 v1, 0x4

    if-ne v1, p1, :cond_0

    .line 194
    sget-object v1, Lcom/youdao/note/activity/LoginActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "key code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " found. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 196
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/youdao/note/activity/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 197
    invoke-virtual {p0}, Lcom/youdao/note/activity/LoginActivity;->finish()V

    .line 198
    const/4 v1, 0x1

    .line 200
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 142
    const-string v0, "offline_notes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "offline_notes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Lcom/youdao/note/data/Note;

    check-cast v0, [Lcom/youdao/note/data/Note;

    iput-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    .line 148
    :cond_0
    const-string v0, "offline_resources"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 150
    const-string v0, "offline_resources"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    .line 153
    :cond_1
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 154
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onResume()V

    .line 165
    sget-object v0, Lcom/youdao/note/activity/LoginActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume called."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 134
    const-string v0, "offline_notes"

    iget-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 135
    const-string v0, "offline_resources"

    iget-object v1, p0, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 136
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 137
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 19
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 209
    packed-switch p1, :pswitch_data_0

    .line 279
    .end local p2
    :goto_0
    :pswitch_0
    return-void

    .line 211
    .restart local p2
    :pswitch_1
    if-eqz p3, :cond_2

    .line 212
    check-cast p2, Lcom/youdao/note/data/LoginResult;

    .end local p2
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v16

    if-nez v16, :cond_1

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/datasource/DataSource;->listAllNotesAsArray()[Lcom/youdao/note/data/NoteMeta;

    move-result-object v13

    .line 216
    .local v13, offlineNoteMetas:[Lcom/youdao/note/data/NoteMeta;
    array-length v0, v13

    move/from16 v16, v0

    move/from16 v0, v16

    new-array v0, v0, [Lcom/youdao/note/data/Note;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    .line 217
    new-instance v16, Ljava/util/ArrayList;

    array-length v0, v13

    move/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    .line 218
    const/4 v4, 0x0

    .local v4, i:I
    :goto_1
    array-length v0, v13

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v4, v0, :cond_0

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v17, v0

    aget-object v18, v13, v4

    invoke-virtual/range {v17 .. v18}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v17

    aput-object v17, v16, v4

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v17, v0

    aget-object v18, v13, v4

    invoke-virtual/range {v18 .. v18}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 223
    :cond_0
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Found "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    array-length v0, v13

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " offline notes."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    .end local v4           #i:I
    .end local v13           #offlineNoteMetas:[Lcom/youdao/note/data/NoteMeta;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/data/LoginResult;->getPersistCookie()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/YNoteApplication;->setPersistCookie(Ljava/lang/String;)Z

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/data/LoginResult;->getUserName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/YNoteApplication;->setUserName(Ljava/lang/String;)Z

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/data/LoginResult;->getPassWord()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/YNoteApplication;->setPassword(Ljava/lang/String;)Z

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/task/TaskManager;->pullUserMeta(Z)V

    goto/16 :goto_0

    .line 231
    .restart local p2
    :cond_2
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/LoginActivity;->removeDialog(I)V

    .line 232
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/youdao/note/activity/LoginActivity;->loginFailed(Lcom/youdao/note/data/BaseData;)V

    goto/16 :goto_0

    .line 236
    :pswitch_2
    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/LoginActivity;->removeDialog(I)V

    .line 237
    if-eqz p3, :cond_6

    .line 238
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "isEverlogin is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v15, p2

    .line 239
    check-cast v15, Lcom/youdao/note/data/UserMeta;

    .line 240
    .local v15, userMeta:Lcom/youdao/note/data/UserMeta;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v16

    if-nez v16, :cond_5

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v10

    .line 242
    .local v10, mLogRecorder:Lcom/youdao/note/LogRecorder;
    sget v16, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_FISTLOGIN:I

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/youdao/note/LogRecorder;->report(I)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/YNoteApplication;->setEverLogin()Z

    .line 244
    const/4 v7, 0x0

    .line 245
    .local v7, idx:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/youdao/note/activity/LoginActivity;->offlineNotes:[Lcom/youdao/note/data/Note;

    .local v2, arr$:[Lcom/youdao/note/data/Note;
    array-length v9, v2

    .local v9, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    move v6, v5

    .end local v5           #i$:I
    .local v6, i$:I
    move v8, v7

    .end local v7           #idx:I
    .local v8, idx:I
    :goto_2
    if-ge v6, v9, :cond_5

    aget-object v12, v2, v6

    .line 247
    .local v12, note:Lcom/youdao/note/data/Note;
    if-nez v12, :cond_4

    move v7, v8

    .line 245
    .end local v6           #i$:I
    .end local v8           #idx:I
    .restart local v7       #idx:I
    :cond_3
    add-int/lit8 v5, v6, 0x1

    .restart local v5       #i$:I
    move v6, v5

    .end local v5           #i$:I
    .restart local v6       #i$:I
    move v8, v7

    .end local v7           #idx:I
    .restart local v8       #idx:I
    goto :goto_2

    .line 250
    :cond_4
    invoke-virtual {v12}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v16

    invoke-virtual {v15}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/data/NoteMeta;->setNoteBook(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v12}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v16

    invoke-virtual {v15}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V

    .line 252
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "default note book is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v15}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->resourceLists:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    add-int/lit8 v7, v8, 0x1

    .end local v8           #idx:I
    .restart local v7       #idx:I
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/ArrayList;

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .end local v6           #i$:I
    .local v5, i$:Ljava/util/Iterator;
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 259
    .local v11, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "add resource "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v11}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v14

    .line 262
    .local v14, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<*>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z

    goto :goto_4

    .line 266
    .end local v2           #arr$:[Lcom/youdao/note/data/Note;
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v7           #idx:I
    .end local v9           #len$:I
    .end local v10           #mLogRecorder:Lcom/youdao/note/LogRecorder;
    .end local v11           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v12           #note:Lcom/youdao/note/data/Note;
    .end local v14           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<*>;"
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mLoginResult:Lcom/youdao/note/data/LoginResult;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/youdao/note/data/LoginResult;->getUserName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v15}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/activity/LoginActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/youdao/note/datasource/DataSource;->listAllNotes()Landroid/database/Cursor;

    move-result-object v3

    .line 268
    .local v3, cursor:Landroid/database/Cursor;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "After login total notes count is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 270
    const/16 v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/LoginActivity;->setResult(I)V

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/activity/LoginActivity;->finish()V

    goto/16 :goto_0

    .line 273
    .end local v3           #cursor:Landroid/database/Cursor;
    .end local v15           #userMeta:Lcom/youdao/note/data/UserMeta;
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/youdao/note/activity/LoginActivity;->loginFailed(Lcom/youdao/note/data/BaseData;)V

    goto/16 :goto_0

    .line 255
    .restart local v2       #arr$:[Lcom/youdao/note/data/Note;
    .restart local v6       #i$:I
    .restart local v8       #idx:I
    .restart local v9       #len$:I
    .restart local v10       #mLogRecorder:Lcom/youdao/note/LogRecorder;
    .restart local v12       #note:Lcom/youdao/note/data/Note;
    .restart local v15       #userMeta:Lcom/youdao/note/data/UserMeta;
    :catch_0
    move-exception v16

    goto/16 :goto_3

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
