.class Lcom/youdao/note/activity/SearchActivity$SearchAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchActivity.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;
    }
.end annotation


# instance fields
.field private mFilter:Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

.field private matchedWords:[Ljava/lang/String;

.field final synthetic this$0:Lcom/youdao/note/activity/SearchActivity;


# direct methods
.method public constructor <init>(Lcom/youdao/note/activity/SearchActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 440
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->this$0:Lcom/youdao/note/activity/SearchActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 438
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->mFilter:Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

    .line 443
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->matchedWords:[Ljava/lang/String;

    .line 441
    return-void
.end method

.method static synthetic access$302(Lcom/youdao/note/activity/SearchActivity$SearchAdapter;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 436
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->matchedWords:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->matchedWords:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->mFilter:Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;-><init>(Lcom/youdao/note/activity/SearchActivity$SearchAdapter;)V

    iput-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->mFilter:Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->mFilter:Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 452
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->matchedWords:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 457
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 462
    move-object v1, p2

    .line 464
    .local v1, view:Landroid/view/View;
    if-nez v1, :cond_0

    .line 465
    iget-object v2, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->this$0:Lcom/youdao/note/activity/SearchActivity;

    invoke-virtual {v2}, Lcom/youdao/note/activity/SearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030047

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 467
    :cond_0
    const v2, 0x7f0700ac

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 468
    .local v0, text:Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    new-instance v2, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$1;-><init>(Lcom/youdao/note/activity/SearchActivity$SearchAdapter;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 477
    return-object v1
.end method
