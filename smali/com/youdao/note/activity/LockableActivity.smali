.class public abstract Lcom/youdao/note/activity/LockableActivity;
.super Lcom/youdao/note/activity/BaseActivity;
.source "LockableActivity.java"


# static fields
.field private static final BUNDLE_LOCK_STATE:Ljava/lang/String; = "unlocked"

.field private static sLockCount:I

.field private static sUnlocked:Z


# instance fields
.field private mFinished:Z

.field private mNeedLock:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-boolean v0, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    .line 23
    sput v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/youdao/note/activity/LockableActivity;->mFinished:Z

    .line 27
    iput-boolean v0, p0, Lcom/youdao/note/activity/LockableActivity;->mNeedLock:Z

    return-void
.end method

.method public static setUnLocked(Z)V
    .locals 0
    .parameter "unlocked"

    .prologue
    .line 97
    sput-boolean p0, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    .line 98
    return-void
.end method


# virtual methods
.method protected finishLockable()V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/youdao/note/activity/LockableActivity;->finish()V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/LockableActivity;->mFinished:Z

    .line 94
    return-void
.end method

.method protected needLock()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/youdao/note/activity/LockableActivity;->mNeedLock:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 76
    packed-switch p1, :pswitch_data_0

    .line 87
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 89
    :goto_0
    return-void

    .line 78
    :pswitch_0
    if-nez p2, :cond_0

    .line 79
    iget-object v0, p0, Lcom/youdao/note/activity/LockableActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v1, "com.youdao.note.action.FINISH"

    invoke-virtual {v0, p0, v1}, Lcom/youdao/note/YNoteApplication;->sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/youdao/note/activity/LockableActivity;->finishLockable()V

    goto :goto_0

    .line 82
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 41
    const-string v0, "unlocked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    .line 42
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 43
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onResume()V

    .line 48
    const-string v1, "com.youdao.note.action.FINISH"

    invoke-virtual {p0}, Lcom/youdao/note/activity/LockableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    sget v1, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    sget-boolean v1, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/activity/LockableActivity;->needLock()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/LockableActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isPinlockEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/LockableActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/youdao/note/activity/LockableActivity;->mFinished:Z

    if-nez v1, :cond_0

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 53
    .local v0, intent:Landroid/content/Intent;
    const/16 v1, 0xe

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/LockableActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 36
    const-string v0, "unlocked"

    sget-boolean v1, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 37
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onStart()V

    .line 32
    sget v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    .line 33
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/youdao/note/activity/BaseActivity;->onStop()V

    .line 60
    sget v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    .line 61
    sget v0, Lcom/youdao/note/activity/LockableActivity;->sLockCount:I

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    sput-boolean v0, Lcom/youdao/note/activity/LockableActivity;->sUnlocked:Z

    .line 64
    :cond_0
    return-void
.end method

.method protected setNeedLock(Z)V
    .locals 0
    .parameter "needLock"

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/youdao/note/activity/LockableActivity;->mNeedLock:Z

    .line 72
    return-void
.end method
