.class Lcom/youdao/note/activity/ResourceAdder$1;
.super Lcom/youdao/note/task/local/AddImageResourceTask;
.source "ResourceAdder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/ResourceAdder;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/ResourceAdder;[Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 89
    iput-object p1, p0, Lcom/youdao/note/activity/ResourceAdder$1;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-direct {p0, p2, p3, p4}, Lcom/youdao/note/task/local/AddImageResourceTask;-><init>([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 93
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$1;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFailed(Ljava/lang/Exception;)V

    .line 94
    return-void
.end method

.method protected bridge synthetic onFinishAll(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 89
    check-cast p1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/ResourceAdder$1;->onFinishAll(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-void
.end method

.method protected onFinishAll(Lcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 103
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$1;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFinishAll(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 104
    return-void
.end method

.method protected bridge synthetic onFinishOne(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 89
    check-cast p1, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/ResourceAdder$1;->onFinishOne(Lcom/youdao/note/data/resource/ImageResourceMeta;)V

    return-void
.end method

.method protected onFinishOne(Lcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$1;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFinishOne(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 99
    return-void
.end method

.method protected onResouceTooLarge(Ljava/lang/String;J)V
    .locals 3
    .parameter "fileName"
    .parameter "fileSize"

    .prologue
    .line 108
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$1;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    #getter for: Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/youdao/note/activity/ResourceAdder;->access$000(Lcom/youdao/note/activity/ResourceAdder;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a00f4

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 109
    return-void
.end method
