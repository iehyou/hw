.class Lcom/youdao/note/activity/GuideActivity$GuideAdapter;
.super Landroid/widget/BaseAdapter;
.source "GuideActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/GuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GuideAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/GuideActivity;

.field private view1:Landroid/view/View;

.field private view2:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/youdao/note/activity/GuideActivity;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 52
    iput-object p1, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->this$0:Lcom/youdao/note/activity/GuideActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030014

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view1:Landroid/view/View;

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030015

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view2:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view2:Landroid/view/View;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    #setter for: Lcom/youdao/note/activity/GuideActivity;->mLeftButton:Landroid/widget/Button;
    invoke-static {p1, v0}, Lcom/youdao/note/activity/GuideActivity;->access$002(Lcom/youdao/note/activity/GuideActivity;Landroid/widget/Button;)Landroid/widget/Button;

    .line 58
    #getter for: Lcom/youdao/note/activity/GuideActivity;->mLeftButton:Landroid/widget/Button;
    invoke-static {p1}, Lcom/youdao/note/activity/GuideActivity;->access$000(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    #getter for: Lcom/youdao/note/activity/GuideActivity;->mLeftButton:Landroid/widget/Button;
    invoke-static {p1}, Lcom/youdao/note/activity/GuideActivity;->access$000(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/GuideActivity$GuideAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/youdao/note/activity/GuideActivity$GuideAdapter$1;-><init>(Lcom/youdao/note/activity/GuideActivity$GuideAdapter;Lcom/youdao/note/activity/GuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view2:Landroid/view/View;

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    #setter for: Lcom/youdao/note/activity/GuideActivity;->mRightButton:Landroid/widget/Button;
    invoke-static {p1, v0}, Lcom/youdao/note/activity/GuideActivity;->access$102(Lcom/youdao/note/activity/GuideActivity;Landroid/widget/Button;)Landroid/widget/Button;

    .line 68
    #getter for: Lcom/youdao/note/activity/GuideActivity;->mRightButton:Landroid/widget/Button;
    invoke-static {p1}, Lcom/youdao/note/activity/GuideActivity;->access$100(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 69
    #getter for: Lcom/youdao/note/activity/GuideActivity;->mRightButton:Landroid/widget/Button;
    invoke-static {p1}, Lcom/youdao/note/activity/GuideActivity;->access$100(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/GuideActivity$GuideAdapter$2;

    invoke-direct {v1, p0, p1}, Lcom/youdao/note/activity/GuideActivity$GuideAdapter$2;-><init>(Lcom/youdao/note/activity/GuideActivity$GuideAdapter;Lcom/youdao/note/activity/GuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :cond_1
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x2

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 90
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 95
    if-nez p1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view1:Landroid/view/View;

    .line 98
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;->view2:Landroid/view/View;

    goto :goto_0
.end method
