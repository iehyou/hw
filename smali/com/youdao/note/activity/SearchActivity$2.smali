.class Lcom/youdao/note/activity/SearchActivity$2;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/SearchActivity;->doLocalSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/youdao/note/data/ListNoteMetas;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/SearchActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/SearchActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 259
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity$2;->this$0:Lcom/youdao/note/activity/SearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;
    .locals 2
    .parameter "params"

    .prologue
    .line 262
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$2;->this$0:Lcom/youdao/note/activity/SearchActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/SearchActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->searchNotes(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 259
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SearchActivity$2;->doInBackground([Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Lcom/youdao/note/data/ListNoteMetas;)V
    .locals 3
    .parameter "listNoteMetas"

    .prologue
    .line 267
    iget-object v0, p0, Lcom/youdao/note/activity/SearchActivity$2;->this$0:Lcom/youdao/note/activity/SearchActivity;

    const/16 v1, 0xa

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/youdao/note/activity/SearchActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 268
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 259
    check-cast p1, Lcom/youdao/note/data/ListNoteMetas;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/SearchActivity$2;->onPostExecute(Lcom/youdao/note/data/ListNoteMetas;)V

    return-void
.end method
