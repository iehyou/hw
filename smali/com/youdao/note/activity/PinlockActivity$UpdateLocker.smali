.class Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;
.super Ljava/lang/Object;
.source "PinlockActivity.java"

# interfaces
.implements Lcom/youdao/note/activity/PinlockActivity$ILocker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/PinlockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateLocker"
.end annotation


# static fields
.field private static final BUNDLE_CODE_VERIFIED:Ljava/lang/String; = "code_verified"


# instance fields
.field private mCodeVerified:Z

.field private mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

.field private mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

.field final synthetic this$0:Lcom/youdao/note/activity/PinlockActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/PinlockActivity;)V
    .locals 3
    .parameter

    .prologue
    .line 455
    iput-object p1, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    .line 461
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    .line 463
    new-instance v0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker$1;-><init>(Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;)V

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 455
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;)V

    return-void
.end method

.method static synthetic access$602(Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 455
    iput-boolean p1, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    return p1
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;)Lcom/youdao/note/activity/PinlockActivity$ILocker;
    .locals 1
    .parameter "x0"

    .prologue
    .line 455
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    return-object v0
.end method


# virtual methods
.method public initContentView()V
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->initContentView()V

    .line 489
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->initContentView()V

    goto :goto_0
.end method

.method public onInput(I)V
    .locals 1
    .parameter "code"

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->onInput(I)V

    .line 480
    :goto_0
    return-void

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->onInput(I)V

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 504
    const-string v0, "code_verified"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    .line 505
    iget-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->restoreState(Landroid/os/Bundle;)V

    .line 510
    :goto_0
    return-void

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->restoreState(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "state"

    .prologue
    .line 493
    iget-boolean v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mSetupLocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->saveState(Landroid/os/Bundle;)V

    .line 498
    :goto_0
    const-string v0, "code_verified"

    iget-boolean v1, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mCodeVerified:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 500
    return-void

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UpdateLocker;->mUnlocker:Lcom/youdao/note/activity/PinlockActivity$ILocker;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/PinlockActivity$ILocker;->saveState(Landroid/os/Bundle;)V

    goto :goto_0
.end method
