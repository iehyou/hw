.class Lcom/youdao/note/activity/FunActivity$1;
.super Landroid/os/Handler;
.source "FunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/FunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/FunActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/FunActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .parameter "msg"

    .prologue
    const/4 v8, -0x2

    const/4 v7, 0x0

    .line 68
    iget v5, p1, Landroid/os/Message;->what:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 69
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$000(Lcom/youdao/note/activity/FunActivity;)I

    move-result v5

    const-string v6, "\u5176\u5b9e\u8fd9\u4e16\u4e0a\u6700\u6050\u6016\u7684\uff0c\u4e0d\u662f\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\uff0c\u800c\u662f\u5728\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\u7684\u65f6\u5019\uff0c\u4f60\u4f38\u51fa\u4e86\u624b\uff0c\u53d1\u73b0\u4e0a\u9762\u8d6b\u7136\u957f\u7740\u516d\u4e2a\u6307\u5934\u3002\u2014\u2014\u300a\u5450\u558a\u4e0d\u8d77\u6765\u300b"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 70
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCanceled:Z
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$100(Lcom/youdao/note/activity/FunActivity;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    new-instance v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 75
    .local v4, textView:Landroid/widget/TextView;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 78
    .local v1, layoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$000(Lcom/youdao/note/activity/FunActivity;)I

    move-result v5

    iget-object v6, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCharPerLine:I
    invoke-static {v6}, Lcom/youdao/note/activity/FunActivity;->access$200(Lcom/youdao/note/activity/FunActivity;)I

    move-result v6

    rem-int/2addr v5, v6

    mul-int/lit8 v2, v5, 0x28

    .line 79
    .local v2, marginLeft:I
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$000(Lcom/youdao/note/activity/FunActivity;)I

    move-result v5

    iget-object v6, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCharPerLine:I
    invoke-static {v6}, Lcom/youdao/note/activity/FunActivity;->access$200(Lcom/youdao/note/activity/FunActivity;)I

    move-result v6

    div-int/2addr v5, v6

    mul-int/lit8 v3, v5, 0x28

    .line 80
    .local v3, marginTop:I
    invoke-virtual {v1, v2, v3, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 81
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mContainer:Landroid/view/ViewGroup;
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$300(Lcom/youdao/note/activity/FunActivity;)Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 82
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    const/high16 v5, 0x4220

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 84
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\u5176\u5b9e\u8fd9\u4e16\u4e0a\u6700\u6050\u6016\u7684\uff0c\u4e0d\u662f\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\uff0c\u800c\u662f\u5728\u4f38\u624b\u4e0d\u89c1\u4e94\u6307\u7684\u65f6\u5019\uff0c\u4f60\u4f38\u51fa\u4e86\u624b\uff0c\u53d1\u73b0\u4e0a\u9762\u8d6b\u7136\u957f\u7740\u516d\u4e2a\u6307\u5934\u3002\u2014\u2014\u300a\u5450\u558a\u4e0d\u8d77\u6765\u300b"

    iget-object v7, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mCurrentCharIdx:I
    invoke-static {v7}, Lcom/youdao/note/activity/FunActivity;->access$000(Lcom/youdao/note/activity/FunActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    #getter for: Lcom/youdao/note/activity/FunActivity;->mChars:Ljava/util/List;
    invoke-static {v5}, Lcom/youdao/note/activity/FunActivity;->access$400(Lcom/youdao/note/activity/FunActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v5, p0, Lcom/youdao/note/activity/FunActivity$1;->this$0:Lcom/youdao/note/activity/FunActivity;

    const v6, 0x7f04000a

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 88
    .local v0, animation:Landroid/view/animation/Animation;
    const-string v5, "Char"

    const-string v6, "handled message"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v5, Lcom/youdao/note/activity/FunActivity$1$1;

    invoke-direct {v5, p0, v4}, Lcom/youdao/note/activity/FunActivity$1$1;-><init>(Lcom/youdao/note/activity/FunActivity$1;Landroid/widget/TextView;)V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 120
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method
