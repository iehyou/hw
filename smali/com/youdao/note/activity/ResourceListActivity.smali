.class public Lcom/youdao/note/activity/ResourceListActivity;
.super Landroid/app/ListActivity;
.source "ResourceListActivity.java"

# interfaces
.implements Lcom/youdao/note/activity/ActivityConsts;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;
    }
.end annotation


# static fields
.field private static final BUNDLE_DELETED_IDX:Ljava/lang/String; = "delete_idx"

.field private static final BUNDLE_RESOURCE_META_LIST:Ljava/lang/String; = "resource_list"

.field private static final DIALOG_DELETE_RESOURCE_CONFIRM:I = 0x1


# instance fields
.field private mClickedResouceIdx:I

.field protected mDataSource:Lcom/youdao/note/datasource/DataSource;

.field protected mYNote:Lcom/youdao/note/YNoteApplication;

.field private resPos:I

.field private resourceMetaList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    .line 72
    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 74
    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I

    .line 260
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/ResourceListActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I

    return v0
.end method

.method static synthetic access$002(Lcom/youdao/note/activity/ResourceListActivity;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 62
    iput p1, p0, Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I

    return p1
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/ResourceListActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/ResourceListActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity()V

    return-void
.end method

.method private backEdiitActivityWithAudio(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 2
    .parameter "audioMeta"

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 241
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMeta"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 242
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity(Landroid/content/Intent;)V

    .line 243
    return-void
.end method

.method private backEditActivity()V
    .locals 1

    .prologue
    .line 256
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 257
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method private backEditActivity(Landroid/content/Intent;)V
    .locals 4
    .parameter "intent"

    .prologue
    .line 246
    const-string v2, "resourceMetaList"

    iget-object v3, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 247
    const-string v2, "ready to back"

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v2, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 249
    .local v1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 251
    .end local v1           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p0, v2, p1}, Lcom/youdao/note/activity/ResourceListActivity;->setResult(ILandroid/content/Intent;)V

    .line 252
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->finish()V

    .line 253
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 196
    const/16 v5, 0x8

    if-ne p1, v5, :cond_3

    .line 197
    if-ne p2, v6, :cond_1

    if-eqz p3, :cond_1

    .line 198
    const-string v5, "resourceMeta"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 199
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v5, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/AbstractImageResource;

    .line 201
    .local v4, resource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/AbstractImageResource;->getContentBytes()[B

    move-result-object v0

    .line 202
    .local v0, bytes:[B
    const/4 v5, 0x0

    array-length v6, v0

    invoke-static {v0, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 203
    .local v1, image:Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->getImageQuality()I

    move-result v5

    invoke-static {v4, v1, v5, v7}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V

    .line 204
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 205
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    invoke-virtual {v5, v6, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 206
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->onContentChanged()V

    .line 228
    .end local v0           #bytes:[B
    .end local v1           #image:Landroid/graphics/Bitmap;
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v4           #resource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    if-ne p2, v7, :cond_0

    .line 208
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 209
    .restart local v2       #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 210
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "to be removed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v5, v2}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 212
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 213
    invoke-direct {p0}, Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity()V

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->onContentChanged()V

    goto :goto_0

    .line 218
    .end local v2           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_3
    const/16 v5, 0x13

    if-eq p1, v5, :cond_4

    const/16 v5, 0x14

    if-ne p1, v5, :cond_0

    .line 220
    :cond_4
    if-ne p2, v6, :cond_0

    if-eqz p3, :cond_0

    .line 221
    const-string v5, "resourceMeta"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 222
    .restart local v2       #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v5, v2}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/AbstractImageResource;

    .line 224
    .local v3, resource:Lcom/youdao/note/data/resource/AbstractImageResource;,"Lcom/youdao/note/data/resource/AbstractImageResource<*>;"
    iget-object v5, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/AbstractImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 225
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->onContentChanged()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/ResourceListActivity;->requestWindowFeature(I)Z

    .line 84
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 85
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 86
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/ResourceListActivity;->setContentView(I)V

    .line 88
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resourceMetaList"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    .line 91
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/youdao/note/activity/ResourceListActivity;->finish()V

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got resource "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    .line 120
    packed-switch p1, :pswitch_data_0

    .line 146
    :goto_0
    return-object v0

    .line 122
    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0036

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00ab

    new-instance v3, Lcom/youdao/note/activity/ResourceListActivity$1;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/ResourceListActivity$1;-><init>(Lcom/youdao/note/activity/ResourceListActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00aa

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 232
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/youdao/note/activity/ResourceListActivity;->backEditActivity()V

    .line 234
    const/4 v0, 0x1

    .line 236
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 9
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 152
    iput p3, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    .line 153
    iget-object v7, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    iget v8, p0, Lcom/youdao/note/activity/ResourceListActivity;->resPos:I

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 154
    .local v4, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    .line 155
    .local v3, mYnote:Lcom/youdao/note/YNoteApplication;
    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v7

    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->getSkitchMeta(Ljava/lang/String;)Lcom/youdao/note/ui/skitch/ISkitchMeta;

    move-result-object v5

    .line 156
    .local v5, skitchMeta:Lcom/youdao/note/ui/skitch/ISkitchMeta;
    if-nez v5, :cond_3

    .line 157
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v7

    if-nez v7, :cond_1

    .line 158
    new-instance v2, Landroid/content/Intent;

    const-class v7, Lcom/youdao/note/activity/resource/ImageViewActivity;

    invoke-direct {v2, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    .local v2, intent:Landroid/content/Intent;
    const-string v7, "com.youdao.note.action.VIEW_RESOURCE"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    const-string v7, "resourceMeta"

    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 161
    const/16 v7, 0x8

    invoke-virtual {p0, v2, v7}, Lcom/youdao/note/activity/ResourceListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 192
    .end local v2           #intent:Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/youdao/note/utils/FileUtils;->isPlayable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 164
    invoke-direct {p0, v4}, Lcom/youdao/note/activity/ResourceListActivity;->backEdiitActivityWithAudio(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 166
    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/youdao/note/activity/ResourceListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v4}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 167
    .local v1, file:Ljava/io/File;
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 168
    .local v6, uri:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 169
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v7

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/ResourceListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const v7, 0x7f0a0083

    invoke-static {p0, v7}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 181
    .end local v0           #e:Landroid/content/ActivityNotFoundException;
    .end local v1           #file:Ljava/io/File;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v6           #uri:Landroid/net/Uri;
    :cond_3
    instance-of v7, v5, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    if-eqz v7, :cond_4

    .line 182
    new-instance v2, Landroid/content/Intent;

    const-class v7, Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-direct {v2, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 183
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v7, "com.youdao.note.action.EDIT_HANDWRITE"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    const-string v7, "resourceMeta"

    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 185
    const/16 v7, 0x13

    invoke-virtual {p0, v2, v7}, Lcom/youdao/note/activity/ResourceListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 186
    .end local v2           #intent:Landroid/content/Intent;
    :cond_4
    instance-of v7, v5, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    if-eqz v7, :cond_0

    .line 187
    new-instance v2, Landroid/content/Intent;

    const-class v7, Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-direct {v2, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 188
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v7, "com.youdao.note.action.EDIT_DOODLE"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v7, "resourceMeta"

    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 190
    const/16 v7, 0x14

    invoke-virtual {p0, v2, v7}, Lcom/youdao/note/activity/ResourceListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "state"

    .prologue
    .line 113
    const-string v0, "delete_idx"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I

    .line 114
    const-string v0, "resource_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    .line 115
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 116
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 98
    new-instance v0, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;

    const v1, 0x7f030043

    iget-object v2, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/youdao/note/activity/ResourceListActivity$ResourceListAdapter;-><init>(Lcom/youdao/note/activity/ResourceListActivity;Landroid/content/Context;ILjava/util/List;)V

    .line 99
    .local v0, adapter:Landroid/widget/ListAdapter;
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/ResourceListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 101
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 105
    const-string v0, "delete_idx"

    iget v1, p0, Lcom/youdao/note/activity/ResourceListActivity;->mClickedResouceIdx:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    const-string v0, "resource_list"

    iget-object v1, p0, Lcom/youdao/note/activity/ResourceListActivity;->resourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 107
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method
