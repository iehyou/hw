.class public Lcom/youdao/note/activity/NoteDetailActivity;
.super Lcom/youdao/note/activity/AbstractLoadNoteActivity;
.source "NoteDetailActivity.java"

# interfaces
.implements Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;
    }
.end annotation


# static fields
.field private static final DOWNLOAD_DIALOG:I = 0x4

.field private static final JS_GETBODY:Ljava/lang/String; = "javascript:window.View.onBodyFetched(document.body.innerHTML)"

.field private static final JS_UPDATE_PROGRESS_TEXT:Ljava/lang/String; = "javascript:updateResourceDownloadProgress(\"%s\", \"%s\")"

.field private static final JS_UPDATE_RESOURCE_BUTTON:Ljava/lang/String; = "javascript:updateResourceButtonImage(\"%s\", \"%s\")"

.field private static final NO_APP_DIALOG:I = 0x3

.field private static final NO_RESOURCE_FOUND_DIALOG:I = 0x5


# instance fields
.field private final guide_right:I

.field private final guide_top:I

.field private final handler:Landroid/os/Handler;

.field private lastWidth:I

.field private mFirstLoad:Z

.field private mImgSelected:Z

.field private mIntent:Landroid/content/Intent;

.field private mNoteLoding:Landroid/view/View;

.field private mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mPullAuto:Z

.field private mRefreshItem:Landroid/view/MenuItem;

.field private resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;-><init>()V

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->handler:Landroid/os/Handler;

    .line 84
    iput-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 86
    iput-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    .line 88
    iput-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mFirstLoad:Z

    .line 96
    iput-boolean v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPullAuto:Z

    .line 98
    iput-boolean v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mImgSelected:Z

    .line 100
    const/16 v0, 0x9b

    iput v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->guide_right:I

    .line 101
    const/16 v0, 0x96

    iput v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->guide_top:I

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->lastWidth:I

    .line 526
    iput-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/NoteDetailActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 73
    iget v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->lastWidth:I

    return v0
.end method

.method static synthetic access$002(Lcom/youdao/note/activity/NoteDetailActivity;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 73
    iput p1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->lastWidth:I

    return p1
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/NoteDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->showMailGuide()V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/NoteDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->mailShare()V

    return-void
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/NoteDetailActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 73
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/NoteDetailActivity;)Landroid/content/Intent;
    .locals 1
    .parameter "x0"

    .prologue
    .line 73
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$402(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 73
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$502(Lcom/youdao/note/activity/NoteDetailActivity;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mImgSelected:Z

    return p1
.end method

.method static synthetic access$602(Lcom/youdao/note/activity/NoteDetailActivity;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 73
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    return-object p1
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/NoteDetailActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->sendToViewResource()V

    return-void
.end method

.method static synthetic access$800(Lcom/youdao/note/activity/NoteDetailActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/youdao/note/activity/NoteDetailActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/activity/NoteDetailActivity;->updateDownloadProgress(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private checkIntent()Z
    .locals 4

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    const/high16 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 470
    .local v0, infos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 471
    const v1, 0x7f0a0083

    invoke-static {p0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 473
    const/4 v1, 0x0

    .line 475
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private enableRefresh()V
    .locals 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 709
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 712
    :cond_0
    return-void
.end method

.method private initListener()V
    .locals 3

    .prologue
    .line 156
    const v2, 0x7f07008b

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/ui/audio/AudioPlayerBar;

    iput-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    .line 157
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v2, p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->setAudioPlayListener(Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;)V

    .line 159
    const v2, 0x7f070093

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 160
    .local v1, mailShareBtn:Landroid/widget/ImageView;
    new-instance v2, Lcom/youdao/note/activity/NoteDetailActivity$3;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/NoteDetailActivity$3;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    const v2, 0x7f070092

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 177
    .local v0, editButton:Landroid/widget/ImageView;
    new-instance v2, Lcom/youdao/note/activity/NoteDetailActivity$4;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/NoteDetailActivity$4;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    return-void
.end method

.method private initUI()V
    .locals 7

    .prologue
    const v6, 0x7f07008c

    .line 117
    iget-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setDrawingCacheEnabled(Z)V

    .line 118
    iget-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;)V

    const-string v5, "View"

    invoke-virtual {v3, v4, v5}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    const v3, 0x7f07008a

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    .line 122
    iget-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->isShowMailGuide()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 124
    .local v1, guide:Landroid/view/View;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    const v3, 0x7f070088

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 126
    .local v0, frameLayout:Landroid/view/View;
    new-instance v2, Lcom/youdao/note/activity/NoteDetailActivity$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/youdao/note/activity/NoteDetailActivity$1;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/view/View;Landroid/view/View;)V

    .line 136
    .local v2, listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 137
    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteDetailActivity$2;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/youdao/note/activity/NoteDetailActivity$2;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 153
    .end local v0           #frameLayout:Landroid/view/View;
    .end local v1           #guide:Landroid/view/View;
    .end local v2           #listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private mailShare()V
    .locals 6

    .prologue
    .line 715
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 716
    .local v2, snapshot:Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v4}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/youdao/note/data/TempFile;->newNoteSnapshot(Ljava/lang/String;)Lcom/youdao/note/data/TempFile;

    move-result-object v3

    .line 717
    .local v3, tmp:Lcom/youdao/note/data/TempFile;
    invoke-static {v2}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/TempFile;->setContentBytes([B)V

    .line 719
    :try_start_0
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v4}, Lcom/youdao/note/datasource/DataSource;->getTempFileCache()Lcom/youdao/note/datasource/localcache/TempFileCache;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/youdao/note/datasource/localcache/TempFileCache;->updateTempFile(Lcom/youdao/note/data/TempFile;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/youdao/note/activity/MailShareActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 724
    .local v1, intent:Landroid/content/Intent;
    const-string v4, "noteid"

    iget-object v5, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v5}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 725
    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 726
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v4}, Lcom/youdao/note/LogRecorder;->addShareMailTimes()Z

    .line 727
    return-void

    .line 720
    .end local v1           #intent:Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 721
    .local v0, e:Ljava/io/IOException;
    const-string v4, ""

    invoke-static {p0, v4, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private refreshNote()V
    .locals 2

    .prologue
    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPullAuto:Z

    .line 317
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/TaskManager;->pullNoteIfNeed(Lcom/youdao/note/data/NoteMeta;)V

    .line 318
    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->loadDismissed:Z

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private sendToViewResource()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 507
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/NoteDetailActivity;->setNeedLock(Z)V

    .line 508
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->checkIntent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 524
    :goto_0
    return-void

    .line 511
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 512
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/utils/FileUtils;->isPlayable(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 513
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v2, v3}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->setVisibility(I)V

    .line 515
    :try_start_0
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v2}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 516
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v2, v1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->play(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->setVisibility(I)V

    .line 520
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to play uri "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 523
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private showMailGuide()V
    .locals 36

    .prologue
    .line 190
    const v31, 0x7f070088

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 191
    .local v12, frameLayout:Landroid/view/View;
    const v31, 0x7f070093

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 192
    .local v3, btn_mail:Landroid/view/View;
    const v31, 0x7f07008c

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/NoteDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 193
    .local v13, guide:Landroid/widget/ImageView;
    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 194
    .local v20, location:[I
    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 195
    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v24

    .line 196
    .local v24, screenWidth:I
    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v23

    .line 197
    .local v23, screenHeight:I
    const/16 v31, 0x0

    aget v31, v20, v31

    sub-int v31, v24, v31

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v32

    div-int/lit8 v32, v32, 0x2

    sub-int v4, v31, v32

    .line 198
    .local v4, btn_right:I
    rsub-int v0, v4, 0x9b

    move/from16 v18, v0

    .line 200
    .local v18, guide_margin_right:I
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v31

    div-int/lit8 v5, v31, 0x2

    .line 201
    .local v5, btn_top:I
    rsub-int v0, v5, 0x96

    move/from16 v19, v0

    .line 203
    .local v19, guide_margin_top:I
    const/16 v29, 0x0

    .line 205
    .local v29, srcBitmap:Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v21, Landroid/util/DisplayMetrics;

    invoke-direct/range {v21 .. v21}, Landroid/util/DisplayMetrics;-><init>()V

    .line 206
    .local v21, metrics:Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/activity/NoteDetailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 207
    const/4 v11, 0x0

    .line 208
    .local v11, fileName:Ljava/lang/String;
    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v31, v0

    const/16 v32, 0x78

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_14

    .line 209
    const-string v11, "mail_share_guide_ldpi.png"

    .line 217
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/activity/NoteDetailActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v29

    .line 222
    .end local v11           #fileName:Ljava/lang/String;
    .end local v21           #metrics:Landroid/util/DisplayMetrics;
    :goto_1
    invoke-virtual {v13}, Landroid/widget/ImageView;->getWidth()I

    move-result v31

    invoke-virtual {v13}, Landroid/widget/ImageView;->getHeight()I

    move-result v32

    sget-object v33, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v31 .. v33}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 223
    .local v8, dstBitmap:Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7}, Landroid/graphics/Canvas;-><init>()V

    .line 224
    .local v7, canvas:Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 226
    .local v22, paint:Landroid/graphics/Paint;
    invoke-virtual {v7, v8}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 228
    new-instance v30, Landroid/graphics/Rect;

    invoke-direct/range {v30 .. v30}, Landroid/graphics/Rect;-><init>()V

    .line 229
    .local v30, srcRect:Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 231
    .local v9, dstRect:Landroid/graphics/Rect;
    const/16 v25, 0x0

    .line 232
    .local v25, screenX1:I
    const/16 v27, 0x0

    .line 233
    .local v27, screenY1:I
    move/from16 v26, v24

    .line 234
    .local v26, screenX2:I
    move/from16 v28, v23

    .line 236
    .local v28, screenY2:I
    add-int v31, v26, v18

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    sub-int v14, v31, v32

    .line 237
    .local v14, guideX1:I
    sub-int v16, v27, v19

    .line 238
    .local v16, guideY1:I
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v31

    add-int v15, v14, v31

    .line 239
    .local v15, guideX2:I
    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    add-int v17, v16, v31

    .line 241
    .local v17, guideY2:I
    sub-int v31, v25, v14

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 242
    sub-int v31, v26, v14

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 243
    sub-int v31, v27, v16

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 244
    sub-int v31, v28, v16

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 246
    sub-int v31, v14, v25

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->left:I

    .line 247
    sub-int v31, v15, v25

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->right:I

    .line 248
    sub-int v31, v16, v27

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->top:I

    .line 249
    sub-int v31, v17, v27

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->bottom:I

    .line 251
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    if-gez v31, :cond_0

    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 252
    :cond_0
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    if-gez v31, :cond_1

    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 253
    :cond_1
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v31, v0

    if-gez v31, :cond_2

    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 254
    :cond_2
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v31, v0

    if-gez v31, :cond_3

    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 255
    :cond_3
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    if-gez v31, :cond_4

    const/16 v31, 0x0

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->left:I

    .line 256
    :cond_4
    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    if-gez v31, :cond_5

    const/16 v31, 0x0

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->right:I

    .line 257
    :cond_5
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v31, v0

    if-gez v31, :cond_6

    const/16 v31, 0x0

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->top:I

    .line 258
    :cond_6
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v31, v0

    if-gez v31, :cond_7

    const/16 v31, 0x0

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->bottom:I

    .line 260
    :cond_7
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_8

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 261
    :cond_8
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_9

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 262
    :cond_9
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v31, v0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_a

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 263
    :cond_a
    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v31, v0

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_b

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, v30

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 264
    :cond_b
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_c

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v31

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->left:I

    .line 265
    :cond_c
    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_d

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v31

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->right:I

    .line 266
    :cond_d
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v31, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_e

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->top:I

    .line 267
    :cond_e
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v31, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-le v0, v1, :cond_f

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v31

    move/from16 v0, v31

    iput v0, v9, Landroid/graphics/Rect;->bottom:I

    .line 269
    :cond_f
    const/16 v31, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v7, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 271
    const/16 v31, 0x5

    const/16 v32, 0x5

    move-object/from16 v0, v29

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    .line 272
    .local v6, c:I
    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 274
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v31, v0

    if-lez v31, :cond_10

    .line 275
    new-instance v31, Landroid/graphics/Rect;

    const/16 v32, 0x0

    const/16 v33, 0x0

    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v34, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v35

    invoke-direct/range {v31 .. v35}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 277
    :cond_10
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v31, v0

    if-lez v31, :cond_11

    .line 278
    new-instance v31, Landroid/graphics/Rect;

    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v32, v0

    const/16 v33, 0x0

    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v34, v0

    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v35, v0

    invoke-direct/range {v31 .. v35}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 280
    :cond_11
    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v31, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-ge v0, v1, :cond_12

    .line 281
    new-instance v31, Landroid/graphics/Rect;

    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v34

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v35

    invoke-direct/range {v31 .. v35}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 283
    :cond_12
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v31, v0

    if-lez v31, :cond_13

    .line 284
    new-instance v31, Landroid/graphics/Rect;

    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v32, v0

    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v33, v0

    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v34, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v35

    invoke-direct/range {v31 .. v35}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 287
    :cond_13
    invoke-virtual {v13, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 288
    invoke-virtual {v13}, Landroid/widget/ImageView;->postInvalidate()V

    .line 290
    return-void

    .line 210
    .end local v6           #c:I
    .end local v7           #canvas:Landroid/graphics/Canvas;
    .end local v8           #dstBitmap:Landroid/graphics/Bitmap;
    .end local v9           #dstRect:Landroid/graphics/Rect;
    .end local v14           #guideX1:I
    .end local v15           #guideX2:I
    .end local v16           #guideY1:I
    .end local v17           #guideY2:I
    .end local v22           #paint:Landroid/graphics/Paint;
    .end local v25           #screenX1:I
    .end local v26           #screenX2:I
    .end local v27           #screenY1:I
    .end local v28           #screenY2:I
    .end local v30           #srcRect:Landroid/graphics/Rect;
    .restart local v11       #fileName:Ljava/lang/String;
    .restart local v21       #metrics:Landroid/util/DisplayMetrics;
    :cond_14
    :try_start_1
    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v31, v0

    const/16 v32, 0xa0

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_15

    .line 211
    const-string v11, "mail_share_guide_mdpi.png"

    goto/16 :goto_0

    .line 212
    :cond_15
    move-object/from16 v0, v21

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v31, v0

    const/16 v32, 0x140

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_16

    .line 213
    const-string v11, "mail_share_guide_xdpi.png"

    goto/16 :goto_0

    .line 215
    :cond_16
    const-string v11, "mail_share_guide_hdpi.png"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 218
    .end local v11           #fileName:Ljava/lang/String;
    .end local v21           #metrics:Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v10

    .line 219
    .local v10, e:Ljava/io/IOException;
    const-string v31, ""

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-static {v0, v1, v10}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method private updateDownloadProgress(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "resourceId"
    .parameter "progress"

    .prologue
    .line 501
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "javascript:updateResourceDownloadProgress(\"%s\", \"%s\")"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method private updateNoteContent()V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "javascript:window.View.onBodyFetched(document.body.innerHTML)"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 482
    return-void
.end method

.method private updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "resourceId"
    .parameter "imageSrc"

    .prologue
    .line 487
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "javascript:updateResourceButtonImage(\"%s\", \"%s\")"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 489
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->updateNoteContent()V

    .line 490
    return-void
.end method


# virtual methods
.method editCurrentNote()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/youdao/note/YNoteApplication;->sendEditNote(Landroid/app/Activity;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method protected loadNote(Lcom/youdao/note/data/Note;)V
    .locals 7
    .parameter "note"

    .prologue
    .line 330
    iget-object v6, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    monitor-enter v6

    .line 331
    :try_start_0
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    monitor-exit v6

    .line 357
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->loadDismissed:Z

    if-nez v0, :cond_1

    .line 336
    const/16 v0, 0xc9

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->dismissDialog(I)V

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->loadDismissed:Z

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 340
    const/16 v0, 0x69

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->removeDialog(I)V

    .line 343
    :cond_2
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mFirstLoad:Z

    if-eqz v0, :cond_4

    .line 344
    :cond_3
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    :cond_4
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getHtml()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mFirstLoad:Z

    if-eqz v0, :cond_6

    .line 348
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reload html is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/"

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getHtml()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_6
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mFirstLoad:Z

    .line 356
    monitor-exit v6

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 575
    packed-switch p1, :pswitch_data_0

    .line 588
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 590
    :goto_0
    return-void

    .line 577
    :pswitch_0
    const/4 v1, -0x1

    if-ne v1, p2, :cond_0

    .line 578
    const-string v1, "note"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/Note;

    .line 580
    .local v0, note:Lcom/youdao/note/data/Note;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Note edit ok. notebook is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 581
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->loadNote(Lcom/youdao/note/data/Note;)V

    goto :goto_0

    .line 584
    .end local v0           #note:Lcom/youdao/note/data/Note;
    :cond_0
    const-string v1, "Note edit cancled."

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 575
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onClose()V
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 760
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .parameter "mp"

    .prologue
    .line 744
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 745
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter "newConfig"

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 104
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->setVolumeControlStream(I)V

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->requestWindowFeature(I)Z

    .line 106
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->setContentView(I)V

    .line 107
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0a00ab

    .line 594
    sparse-switch p1, :sswitch_data_0

    .line 633
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 596
    :sswitch_0
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v2}, Lcom/youdao/note/LogRecorder;->addNoteDetailTimes()Z

    .line 597
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/ui/DialogFactory;->getNoteDetailDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 600
    :sswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0083

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 603
    .local v0, noAppDialog:Landroid/app/AlertDialog;
    goto :goto_0

    .line 605
    .end local v0           #noAppDialog:Landroid/app/AlertDialog;
    :sswitch_2
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 606
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->resourceMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 607
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 608
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 609
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 611
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v3, Lcom/youdao/note/activity/NoteDetailActivity$5;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/NoteDetailActivity$5;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 617
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 619
    :sswitch_3
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a003b

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .local v1, noResourceDialog:Landroid/app/AlertDialog;
    move-object v0, v1

    .line 622
    goto :goto_0

    .line 624
    .end local v1           #noResourceDialog:Landroid/app/AlertDialog;
    :sswitch_4
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    new-instance v3, Lcom/youdao/note/activity/NoteDetailActivity$6;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/NoteDetailActivity$6;-><init>(Lcom/youdao/note/activity/NoteDetailActivity;)V

    invoke-static {p0, v2, v3}, Lcom/youdao/note/ui/DialogFactory;->getDeleteConfirmDialog(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 594
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x64 -> :sswitch_4
        0x69 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 542
    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 543
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f050001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 544
    const/4 v1, 0x1

    return v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 730
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onDestroy()V

    .line 731
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 732
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0}, Lcom/youdao/note/datasource/DataSource;->getTempFileCache()Lcom/youdao/note/datasource/localcache/TempFileCache;

    move-result-object v0

    const-string v1, "-note-snapshot.jpg"

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/localcache/TempFileCache;->clean(Ljava/lang/String;)V

    .line 733
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    .line 749
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .parameter "item"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 549
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700bd

    if-ne v2, v3, :cond_1

    .line 550
    iput-boolean v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPullAuto:Z

    .line 551
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 552
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v3, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v3}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/youdao/note/task/TaskManager;->pullNote(Lcom/youdao/note/data/NoteMeta;Z)V

    .line 571
    :cond_0
    :goto_0
    return v5

    .line 553
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700be

    if-ne v2, v3, :cond_2

    .line 554
    const/16 v2, 0x69

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->showDialog(I)V

    goto :goto_0

    .line 555
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f070054

    if-ne v2, v3, :cond_3

    .line 556
    const/16 v2, 0x64

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteDetailActivity;->showDialog(I)V

    goto :goto_0

    .line 557
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700bc

    if-ne v2, v3, :cond_4

    .line 559
    :try_start_0
    const-class v2, Landroid/webkit/WebView;

    const-string v3, "emulateShiftHeld"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 560
    .local v1, method:Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    const v2, 0x7f0a0019

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 562
    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v2}, Lcom/youdao/note/LogRecorder;->addContentCopyTimes()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 563
    .end local v1           #method:Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 564
    .local v0, e:Ljava/lang/NoSuchMethodException;
    const v2, 0x7f0a0017

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 565
    .end local v0           #e:Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 566
    .local v0, e:Ljava/lang/Exception;
    const v2, 0x7f0a0018

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 568
    .end local v0           #e:Ljava/lang/Exception;
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0700bf

    if-ne v2, v3, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->editCurrentNote()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->stopAll()V

    .line 738
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onPause()V

    .line 739
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPlayer:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->pause()V

    .line 740
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 530
    const v0, 0x7f0700bd

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    .line 531
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 537
    :goto_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mRefreshItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 299
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onResume()V

    .line 300
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->initUI()V

    .line 301
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->initListener()V

    .line 302
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->loadDismissed:Z

    if-eqz v0, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->refreshNote()V

    .line 307
    :goto_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "javascript:recover()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 308
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteDetailActivity;->setNeedLock(Z)V

    .line 309
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mNoteLoding:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0, p0}, Lcom/youdao/note/YNoteApplication;->sendToSearch(Landroid/app/Activity;)V

    .line 362
    const/4 v0, 0x0

    return v0
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 0
    .parameter "mp"

    .prologue
    .line 755
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 294
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onStart()V

    .line 295
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 8
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    const/4 v5, 0x4

    .line 638
    sparse-switch p1, :sswitch_data_0

    .line 699
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 640
    :sswitch_0
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->enableRefresh()V

    .line 641
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0

    .line 644
    :sswitch_1
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->enableRefresh()V

    .line 645
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0

    .line 648
    :sswitch_2
    if-eqz p3, :cond_0

    move-object v3, p2

    .line 649
    check-cast v3, Lcom/youdao/note/data/ProgressData;

    .line 650
    .local v3, progress:Lcom/youdao/note/data/ProgressData;
    const-string v4, "progress updated called."

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 651
    invoke-virtual {v3}, Lcom/youdao/note/data/ProgressData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/youdao/note/data/ProgressData;->getTotalSize()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/youdao/note/data/ProgressData;->getProgress()I

    move-result v7

    invoke-static {v5, v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getSize(JI)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->updateDownloadProgress(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v4, :cond_0

    .line 656
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Lcom/youdao/note/data/ProgressData;->getProgress()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setProgress(I)V

    goto :goto_0

    .line 662
    .end local v3           #progress:Lcom/youdao/note/data/ProgressData;
    :sswitch_3
    if-eqz p3, :cond_2

    .line 663
    iget-boolean v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mImgSelected:Z

    if-eqz v4, :cond_1

    .line 664
    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->removeDialog(I)V

    .line 665
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteDetailActivity;->sendToViewResource()V

    goto :goto_0

    :cond_1
    move-object v3, p2

    .line 667
    check-cast v3, Lcom/youdao/note/data/ProgressData;

    .line 668
    .restart local v3       #progress:Lcom/youdao/note/data/ProgressData;
    invoke-virtual {v3}, Lcom/youdao/note/data/ProgressData;->getId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file:///android_asset/arrow.png"

    invoke-direct {p0, v4, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v3           #progress:Lcom/youdao/note/data/ProgressData;
    :cond_2
    move-object v0, p2

    .line 672
    check-cast v0, Lcom/youdao/note/data/RemoteErrorData;

    .line 673
    .local v0, error:Lcom/youdao/note/data/RemoteErrorData;
    const/4 v2, 0x0

    .line 674
    .local v2, exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    invoke-virtual {v0}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v4

    instance-of v4, v4, Lcom/youdao/note/exceptions/DownloadResourceException;

    if-eqz v4, :cond_3

    .line 675
    invoke-virtual {v0}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v2

    .end local v2           #exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    check-cast v2, Lcom/youdao/note/exceptions/DownloadResourceException;

    .line 678
    .restart local v2       #exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    :cond_3
    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 679
    invoke-virtual {p0, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->dismissDialog(I)V

    .line 681
    :cond_4
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v4, v4, Ljava/io/IOException;

    if-eqz v4, :cond_5

    .line 682
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/io/IOException;

    .line 683
    .local v1, ex:Ljava/io/IOException;
    const-string v4, "No space left on device"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 684
    const-string v4, "No space left on device."

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 685
    const v4, 0x7f0a0098

    invoke-static {p0, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 686
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getResourceId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file:///android_asset/download.png"

    invoke-direct {p0, v4, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 691
    .end local v1           #ex:Ljava/io/IOException;
    :cond_5
    const v4, 0x7f0a0082

    invoke-static {p0, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 692
    if-eqz v2, :cond_0

    .line 693
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getResourceId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file:///android_asset/arrow.png"

    invoke-direct {p0, v4, v5}, Lcom/youdao/note/activity/NoteDetailActivity;->updateResourceButtonImage(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 638
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xe -> :sswitch_0
    .end sparse-switch
.end method

.method protected showError()Z
    .locals 1

    .prologue
    .line 704
    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteDetailActivity;->mPullAuto:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
