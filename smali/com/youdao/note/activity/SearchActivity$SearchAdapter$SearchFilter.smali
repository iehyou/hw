.class Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;
.super Landroid/widget/Filter;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/SearchActivity$SearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchFilter"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/SearchActivity$SearchAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 488
    iput-object p1, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 9
    .parameter "constraint"

    .prologue
    .line 492
    if-nez p1, :cond_0

    .line 493
    const/4 v4, 0x0

    .line 513
    :goto_0
    return-object v4

    .line 495
    :cond_0
    new-instance v4, Landroid/widget/Filter$FilterResults;

    invoke-direct {v4}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 496
    .local v4, results:Landroid/widget/Filter$FilterResults;
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 497
    .local v5, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    iget-object v6, v6, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->this$0:Lcom/youdao/note/activity/SearchActivity;

    invoke-virtual {v6}, Lcom/youdao/note/activity/SearchActivity;->getAllSearchHistory()Landroid/database/Cursor;

    move-result-object v1

    .line 499
    .local v1, cursor:Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Lcom/youdao/note/utils/CursorHelper;

    invoke-direct {v2, v1}, Lcom/youdao/note/utils/CursorHelper;-><init>(Landroid/database/Cursor;)V

    .line 500
    .local v2, helper:Lcom/youdao/note/utils/CursorHelper;
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 501
    const-string v6, "query"

    invoke-virtual {v2, v6}, Lcom/youdao/note/utils/CursorHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 502
    .local v3, query:Ljava/lang/String;
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 503
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "matched query "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 504
    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 513
    .end local v2           #helper:Lcom/youdao/note/utils/CursorHelper;
    .end local v3           #query:Ljava/lang/String;
    :catchall_0
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6

    .line 506
    .restart local v2       #helper:Lcom/youdao/note/utils/CursorHelper;
    .restart local v3       #query:Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unmatched query "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 509
    .end local v3           #query:Ljava/lang/String;
    :cond_2
    iput-object v5, v4, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 510
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v6

    iput v6, v4, Landroid/widget/Filter$FilterResults;->count:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 513
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3
    .parameter "constraint"
    .parameter "results"

    .prologue
    .line 519
    if-eqz p2, :cond_0

    iget v1, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-nez v1, :cond_1

    .line 520
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    invoke-virtual {v1}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->notifyDataSetInvalidated()V

    .line 527
    :goto_0
    return-void

    .line 523
    :cond_1
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashSet;

    .line 524
    .local v0, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    #setter for: Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->matchedWords:[Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->access$302(Lcom/youdao/note/activity/SearchActivity$SearchAdapter;[Ljava/lang/String;)[Ljava/lang/String;

    .line 525
    iget-object v1, p0, Lcom/youdao/note/activity/SearchActivity$SearchAdapter$SearchFilter;->this$1:Lcom/youdao/note/activity/SearchActivity$SearchAdapter;

    invoke-virtual {v1}, Lcom/youdao/note/activity/SearchActivity$SearchAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
