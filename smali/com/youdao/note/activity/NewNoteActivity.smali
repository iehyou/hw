.class public Lcom/youdao/note/activity/NewNoteActivity;
.super Lcom/youdao/note/activity/BaseEditActivity;
.source "NewNoteActivity.java"

# interfaces
.implements Lcom/youdao/note/utils/PreferenceKeys;


# instance fields
.field private entryFrom:Ljava/lang/String;

.field private mContentView:Landroid/widget/EditText;

.field private mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

.field private typeFrom:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mContentView:Landroid/widget/EditText;

    .line 44
    iput-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->entryFrom:Ljava/lang/String;

    return-void
.end method

.method private checkIdx(II)I
    .locals 1
    .parameter "x"
    .parameter "y"

    .prologue
    .line 121
    if-ge p1, p2, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .end local p1
    :goto_0
    return p1

    .restart local p1
    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method private dispatchIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->needLogin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, action:Ljava/lang/String;
    const-string v1, "com.youdao.note.action.CREATE_TEXT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 190
    const-string v1, "TextNoteTimes"

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    goto :goto_0

    .line 191
    :cond_2
    const-string v1, "com.youdao.note.action.CREATE_SNAPSHOT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->onTakePhoto()V

    .line 193
    const-string v1, "CameraNoteTimes"

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    goto :goto_0

    .line 194
    :cond_3
    const-string v1, "com.youdao.note.action.CREATE_GALLARY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 195
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->onPickPhoto()V

    goto :goto_0

    .line 196
    :cond_4
    const-string v1, "com.youdao.note.action.CREATE_HANDWRITE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 197
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->onHandwrite()V

    goto :goto_0

    .line 198
    :cond_5
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 199
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/NewNoteActivity;->handleIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 200
    :cond_6
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 201
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/NewNoteActivity;->handleIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 202
    :cond_7
    const-string v1, "com.youdao.note.action.CREATE_RECORD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->onRecord()V

    .line 204
    const-string v1, "RecordNoteTimes"

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 9
    .parameter "intent"

    .prologue
    .line 209
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 210
    .local v1, bundle:Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 217
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, action:Ljava/lang/String;
    :try_start_0
    const-string v7, "android.intent.extra.TITLE"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 220
    .local v3, title:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 221
    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_2
    :goto_1
    const-string v7, "android.intent.extra.TEXT"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 229
    .local v2, content:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 230
    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->mContentView:Landroid/widget/EditText;

    invoke-virtual {v7, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :cond_3
    const-string v7, "android.intent.action.SEND"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 233
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 235
    .local v4, uri:Landroid/net/Uri;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "uri is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v7, v4}, Lcom/youdao/note/activity/ResourceAdder;->addResourceFromUri(Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    .end local v4           #uri:Landroid/net/Uri;
    :cond_4
    :goto_2
    const-string v7, "Finish extract text notes."

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 223
    .end local v2           #content:Ljava/lang/String;
    :cond_5
    :try_start_1
    const-string v7, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #title:Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 224
    .restart local v3       #title:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 225
    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 249
    .end local v3           #title:Ljava/lang/String;
    :catchall_0
    move-exception v7

    const-string v8, "Finish extract text notes."

    invoke-static {p0, v8}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    throw v7

    .line 239
    .restart local v2       #content:Ljava/lang/String;
    .restart local v3       #title:Ljava/lang/String;
    :cond_6
    :try_start_2
    const-string v7, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 240
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 242
    .local v6, uriList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const/4 v7, 0x0

    new-array v7, v7, [Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/net/Uri;

    .line 246
    .local v5, uriArray:[Landroid/net/Uri;
    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v7, v5}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private setNoteBook()V
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "noteBook"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, noteBookId:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1, v0}, Lcom/youdao/note/data/Note;->setNoteBookId(Ljava/lang/String;)V

    .line 104
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NewNoteActivity;->updateNoteBookTitle(Ljava/lang/String;)V

    .line 105
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    iget-object v2, p0, Lcom/youdao/note/activity/NewNoteActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/Note;->setNoteBookId(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected initControls()V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->initControls()V

    .line 110
    const v1, 0x7f07002e

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NewNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 111
    .local v0, saveButton:Landroid/view/View;
    new-instance v1, Lcom/youdao/note/activity/NewNoteActivity$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/NewNoteActivity$2;-><init>(Lcom/youdao/note/activity/NewNoteActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    return-void
.end method

.method protected needLogin()Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected obtainNote(Z)V
    .locals 8
    .parameter "saveOnObtained"

    .prologue
    const/4 v7, 0x0

    .line 125
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 127
    .local v4, title:Ljava/lang/String;
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mContentView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, content:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v5}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 130
    .local v3, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 131
    const-string v5, "title is empty"

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 133
    const-string v5, "content is not empty"

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 135
    .local v1, idx:I
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 136
    const/16 v5, 0x3002

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 137
    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 138
    const v5, 0xff01

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 139
    const/16 v5, 0x21

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 140
    const/16 v5, 0x3f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 141
    const v5, 0xff1f

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-direct {p0, v5, v1}, Lcom/youdao/note/activity/NewNoteActivity;->checkIdx(II)I

    move-result v1

    .line 142
    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 172
    .end local v1           #idx:I
    :cond_0
    :goto_0
    invoke-virtual {v3, v4}, Lcom/youdao/note/data/NoteMeta;->setTitle(Ljava/lang/String;)V

    .line 173
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-static {v0}, Lcom/youdao/note/utils/HTMLUtils;->escAndEncodeHTML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 174
    if-eqz p1, :cond_1

    .line 175
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->saveNote()V

    .line 176
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    iget-object v6, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    iget-object v7, p0, Lcom/youdao/note/activity/NewNoteActivity;->entryFrom:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/youdao/note/LogRecorder;->addNote(Ljava/lang/String;Ljava/lang/String;)Z

    .line 178
    :cond_1
    return-void

    .line 144
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/youdao/note/activity/NewNoteActivity;->typeFrom:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 146
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 148
    :pswitch_1
    iget-object v5, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 150
    .local v2, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 151
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a003d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 153
    :cond_3
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a003e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 159
    .end local v2           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :pswitch_2
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0040

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 161
    goto/16 :goto_0

    .line 163
    :pswitch_3
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a003f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 165
    goto/16 :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 52
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NewNoteActivity;->requestWindowFeature(I)Z

    .line 53
    const v1, 0x7f03002e

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NewNoteActivity;->setContentView(I)V

    .line 54
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    new-instance v1, Lcom/youdao/note/activity/NewNoteActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/NewNoteActivity$1;-><init>(Lcom/youdao/note/activity/NewNoteActivity;)V

    invoke-static {p0, v1}, Lcom/youdao/note/activity/ResourceAdder;->getInstance(Landroid/app/Activity;Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;)Lcom/youdao/note/activity/ResourceAdder;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    .line 76
    new-instance v1, Lcom/youdao/note/data/Note;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/youdao/note/data/Note;-><init>(Z)V

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    .line 77
    invoke-direct {p0}, Lcom/youdao/note/activity/NewNoteActivity;->setNoteBook()V

    .line 78
    iget-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->oriNoteBookId:Ljava/lang/String;

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mNoteBookId:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 81
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "entry_from"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->entryFrom:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->entryFrom:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 83
    const-string v1, "WidgetAddTimes"

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->entryFrom:Ljava/lang/String;

    .line 85
    :cond_0
    const v1, 0x7f070030

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NewNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/youdao/note/activity/NewNoteActivity;->mContentView:Landroid/widget/EditText;

    .line 86
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/NewNoteActivity;->dispatchIntent(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .parameter "id"
    .parameter "args"

    .prologue
    .line 255
    packed-switch p1, :pswitch_data_0

    .line 259
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseEditActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 257
    :pswitch_0
    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0}, Lcom/youdao/note/activity/ResourceAdder;->createDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    goto :goto_0

    .line 255
    nop

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Lcom/youdao/note/activity/BaseEditActivity;->onDestroy()V

    .line 282
    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceAdder:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0}, Lcom/youdao/note/activity/ResourceAdder;->destroy()V

    .line 283
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 264
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mContentView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/NewNoteActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/EditFooterBar;->isRecording()Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-virtual {p0}, Lcom/youdao/note/activity/NewNoteActivity;->finish()V

    .line 269
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseEditActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .parameter "intent"

    .prologue
    .line 91
    const-string v0, "on new intent called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/NewNoteActivity;->dispatchIntent(Landroid/content/Intent;)V

    .line 93
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 94
    return-void
.end method
