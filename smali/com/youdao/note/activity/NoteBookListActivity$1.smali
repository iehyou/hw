.class Lcom/youdao/note/activity/NoteBookListActivity$1;
.super Ljava/lang/Object;
.source "NoteBookListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteBookListActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteBookListActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteBookListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 213
    packed-switch p2, :pswitch_data_0

    .line 225
    :goto_0
    return-void

    .line 215
    :pswitch_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    const/16 v1, 0x73

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->showDialog(I)V

    goto :goto_0

    .line 218
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;
    invoke-static {v2}, Lcom/youdao/note/activity/NoteBookListActivity;->access$100(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    #setter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$002(Lcom/youdao/note/activity/NoteBookListActivity;Lcom/youdao/note/data/NoteBook;)Lcom/youdao/note/data/NoteBook;

    .line 219
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    #getter for: Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-static {v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v1

    #setter for: Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->access$202(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity$1;->this$0:Lcom/youdao/note/activity/NoteBookListActivity;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->showDialog(I)V

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
