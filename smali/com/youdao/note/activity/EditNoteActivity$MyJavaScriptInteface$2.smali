.class Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;
.super Ljava/lang/Object;
.source "EditNoteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->editContent(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

.field final synthetic val$content:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 215
    iput-object p1, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iput-object p2, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->val$content:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private showEditView(Ljava/lang/String;)V
    .locals 2
    .parameter "content"

    .prologue
    .line 223
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    #getter for: Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/youdao/note/activity/EditNoteActivity;->access$200(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    #getter for: Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/EditNoteActivity;->access$300(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 225
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    #getter for: Lcom/youdao/note/activity/EditNoteActivity;->mEditView:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/EditNoteActivity;->access$300(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    #getter for: Lcom/youdao/note/activity/EditNoteActivity;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/youdao/note/activity/EditNoteActivity;->access$200(Lcom/youdao/note/activity/EditNoteActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 230
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    #calls: Lcom/youdao/note/activity/EditNoteActivity;->dismissFooterBar()V
    invoke-static {v0}, Lcom/youdao/note/activity/EditNoteActivity;->access$400(Lcom/youdao/note/activity/EditNoteActivity;)V

    .line 231
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->val$content:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$2;->showEditView(Ljava/lang/String;)V

    .line 220
    return-void
.end method
