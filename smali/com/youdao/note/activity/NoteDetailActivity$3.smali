.class Lcom/youdao/note/activity/NoteDetailActivity$3;
.super Ljava/lang/Object;
.source "NoteDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteDetailActivity;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteDetailActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 160
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    const/4 v2, 0x0

    .line 163
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const v1, 0x7f0a00ef

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 174
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    const v1, 0x7f0a00f0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$3;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    #calls: Lcom/youdao/note/activity/NoteDetailActivity;->mailShare()V
    invoke-static {v0}, Lcom/youdao/note/activity/NoteDetailActivity;->access$200(Lcom/youdao/note/activity/NoteDetailActivity;)V

    goto :goto_0
.end method
