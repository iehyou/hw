.class Lcom/youdao/note/activity/PinlockActivity$SetupLocker;
.super Lcom/youdao/note/activity/PinlockActivity$BaseLocker;
.source "PinlockActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/PinlockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetupLocker"
.end annotation


# instance fields
.field private mCodeHint:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/youdao/note/activity/PinlockActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/PinlockActivity;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 307
    iput-object p1, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    .line 309
    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->mCodeHint:Landroid/widget/TextView;

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;)V

    return-void
.end method

.method private showInputAgain()V
    .locals 2

    .prologue
    .line 341
    const v0, 0x7f0a0021

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->setLabel(I)V

    .line 342
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->mCodeHint:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 343
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->setLabelBgColor(I)V

    .line 344
    return-void
.end method


# virtual methods
.method public initContentView()V
    .locals 2

    .prologue
    .line 331
    invoke-super {p0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->initContentView()V

    .line 332
    const v0, 0x7f0a0024

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->setLabel(I)V

    .line 333
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v1, 0x7f07009f

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->mCodeHint:Landroid/widget/TextView;

    .line 334
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->setLabelBgColor(I)V

    .line 335
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getCodeInputTimes()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 336
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->showInputAgain()V

    .line 338
    :cond_0
    return-void
.end method

.method public onLockCodeCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 313
    invoke-super {p0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->onLockCodeCompleted()V

    .line 314
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getCodeInputTimes()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 315
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->showInputAgain()V

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getCodeInputTimes()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getCurrentCodes()I

    move-result v0

    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getPreviousCodes()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 318
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/PinlockActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->getCurrentCodes()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->setPinLock(I)Z

    .line 319
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    #calls: Lcom/youdao/note/activity/PinlockActivity;->finishWithResult(Z)V
    invoke-static {v0, v2}, Lcom/youdao/note/activity/PinlockActivity;->access$400(Lcom/youdao/note/activity/PinlockActivity;Z)V

    goto :goto_0

    .line 321
    :cond_2
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->clear()V

    .line 322
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->mCodeHint:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->mCodeHint:Landroid/widget/TextView;

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 324
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/PinlockActivity$SetupLocker;->setLabelBgColor(I)V

    goto :goto_0
.end method
