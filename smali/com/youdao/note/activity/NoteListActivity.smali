.class public Lcom/youdao/note/activity/NoteListActivity;
.super Lcom/youdao/note/activity/BaseListFileActivity;
.source "NoteListActivity.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$DATA_TYPE;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/youdao/note/activity/ActivityConsts;


# static fields
.field private static final LONGCLICKED_NOTEMETA:Ljava/lang/String; = "longclicked_notemeta"


# instance fields
.field private mEmptyNotebooks:Landroid/view/View;

.field private mEmptyNotes:Landroid/view/View;

.field private mEmptyTip:Landroid/widget/TextView;

.field private mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

.field private mNoteBook:Lcom/youdao/note/data/NoteBook;

.field private mNoteBookId:Ljava/lang/String;

.field private syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseListFileActivity;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBook:Lcom/youdao/note/data/NoteBook;

    .line 39
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotes:Landroid/view/View;

    .line 41
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotebooks:Landroid/view/View;

    .line 43
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyTip:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/NoteListActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->updateData()V

    return-void
.end method

.method private initControllers()V
    .locals 3

    .prologue
    .line 61
    const v1, 0x7f070094

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotes:Landroid/view/View;

    .line 62
    const v1, 0x7f070095

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotebooks:Landroid/view/View;

    .line 63
    const v1, 0x7f070096

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyTip:Landroid/widget/TextView;

    .line 64
    const v1, 0x7f070097

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 65
    .local v0, newNoteButton:Landroid/view/View;
    new-instance v1, Lcom/youdao/note/activity/NoteListActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/NoteListActivity$1;-><init>(Lcom/youdao/note/activity/NoteListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "noteBook"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBook:Lcom/youdao/note/data/NoteBook;

    .line 74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "note book id is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method

.method private prepareEmptyView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 214
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotes:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotebooks:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyTip:Landroid/widget/TextView;

    const v1, 0x7f0a00ad

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 223
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotes:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyNotebooks:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mEmptyTip:Landroid/widget/TextView;

    const v1, 0x7f0a00bc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private declared-synchronized updateAdapter(Landroid/database/Cursor;)V
    .locals 8
    .parameter "cursor"

    .prologue
    const/4 v2, 0x0

    .line 193
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/NoteListActivity;->startManagingCursor(Landroid/database/Cursor;)V

    .line 195
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 196
    .local v1, resources:Landroid/content/res/Resources;
    new-instance v0, Lcom/youdao/note/data/adapter/SectionAdapter;

    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    iget-object v3, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    if-nez v3, :cond_1

    const v3, 0x7f0a0059

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-object v5, p0, Lcom/youdao/note/activity/NoteListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v5

    if-eqz v5, :cond_0

    const v2, 0x7f0a0043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-direct {v0, v4, p1, v3, v2}, Lcom/youdao/note/data/adapter/SectionAdapter;-><init>(Landroid/view/LayoutInflater;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .local v0, adapter:Lcom/youdao/note/data/adapter/SectionAdapter;
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 205
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->prepareEmptyView()V

    .line 206
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :goto_1
    monitor-exit p0

    return-void

    .line 196
    .end local v0           #adapter:Lcom/youdao/note/data/adapter/SectionAdapter;
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBook:Lcom/youdao/note/data/NoteBook;

    invoke-virtual {v5}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v5

    const-string v6, "%"

    const-string v7, "%%"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v5, 0x7f0a0058

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 208
    .restart local v0       #adapter:Lcom/youdao/note/data/adapter/SectionAdapter;
    :cond_2
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 209
    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mListView:Landroid/widget/ListView;

    iget v3, p0, Lcom/youdao/note/activity/NoteListActivity;->mListPost:I

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 193
    .end local v0           #adapter:Lcom/youdao/note/data/adapter/SectionAdapter;
    .end local v1           #resources:Landroid/content/res/Resources;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private updateData()V
    .locals 3

    .prologue
    .line 175
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    const/4 v0, 0x0

    .line 177
    .local v0, cursor:Landroid/database/Cursor;
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 178
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->listAllNotes()Landroid/database/Cursor;

    move-result-object v0

    .line 185
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "total notes count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    if-eqz v0, :cond_1

    .line 187
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/NoteListActivity;->updateAdapter(Landroid/database/Cursor;)V

    .line 190
    .end local v0           #cursor:Landroid/database/Cursor;
    :cond_1
    return-void

    .line 180
    .restart local v0       #cursor:Landroid/database/Cursor;
    :cond_2
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    if-nez v1, :cond_3

    .line 181
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteListActivity;->finish()V

    .line 183
    :cond_3
    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->listNoteByNotebook(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 145
    packed-switch p1, :pswitch_data_0

    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/BaseListFileActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 147
    :pswitch_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->updateData()V

    goto :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteListActivity;->requestWindowFeature(I)Z

    .line 53
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteListActivity;->setContentView(I)V

    .line 54
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    new-instance v0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    .line 56
    const-string v0, "onCreate called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->initControllers()V

    .line 58
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter "id"

    .prologue
    .line 124
    sparse-switch p1, :sswitch_data_0

    .line 139
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 126
    :sswitch_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-static {p0, v0}, Lcom/youdao/note/ui/DialogFactory;->getNoteLongClickMenu(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 128
    :sswitch_1
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    new-instance v1, Lcom/youdao/note/activity/NoteListActivity$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/NoteListActivity$2;-><init>(Lcom/youdao/note/activity/NoteListActivity;)V

    invoke-static {p0, v0, v1}, Lcom/youdao/note/ui/DialogFactory;->getDeleteConfirmDialog(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 136
    :sswitch_2
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->addNoteDetailTimes()Z

    .line 137
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/ui/DialogFactory;->getNoteDetailDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x69 -> :sswitch_2
        0x6a -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 117
    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 118
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f050002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 119
    const/4 v1, 0x1

    return v1
.end method

.method public onIntentSend(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 80
    const-string v0, "com.youdao.note.action.CREATE_TEXT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "noteBook"

    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mNoteBookId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    :cond_0
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/16 v3, 0x6a

    const/4 v1, 0x1

    .line 228
    iget-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/adapter/SectionItem;

    .line 229
    .local v0, item:Lcom/youdao/note/data/adapter/SectionItem;
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 230
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 232
    const/16 v2, 0x69

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteListActivity;->removeDialog(I)V

    .line 233
    const/16 v2, 0x68

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteListActivity;->removeDialog(I)V

    .line 234
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/NoteListActivity;->removeDialog(I)V

    .line 235
    const/16 v2, 0x64

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteListActivity;->removeDialog(I)V

    .line 236
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/NoteListActivity;->showDialog(I)V

    .line 240
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter "item"

    .prologue
    .line 251
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 245
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 246
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 108
    const-string v1, "longclicked_notemeta"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 109
    .local v0, s:Ljava/io/Serializable;
    if-eqz v0, :cond_0

    .line 110
    check-cast v0, Lcom/youdao/note/data/NoteMeta;

    .end local v0           #s:Ljava/io/Serializable;
    iput-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 112
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 113
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->onResume()V

    .line 90
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->updateData()V

    .line 91
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onResume()V

    .line 92
    const-string v0, "onResume called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 100
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "longclicked_notemeta"

    iget-object v1, p0, Lcom/youdao/note/activity/NoteListActivity;->mLongClickedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 103
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 1
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 158
    iget-object v0, p0, Lcom/youdao/note/activity/NoteListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 159
    sparse-switch p1, :sswitch_data_0

    .line 171
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/BaseListFileActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 172
    return-void

    .line 161
    :sswitch_0
    if-eqz p3, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->updateData()V

    goto :goto_0

    .line 166
    :sswitch_1
    if-eqz p3, :cond_0

    .line 167
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteListActivity;->updateData()V

    goto :goto_0

    .line 159
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method
