.class Lcom/youdao/note/activity/PinlockActivity$UnLocker;
.super Lcom/youdao/note/activity/PinlockActivity$BaseLocker;
.source "PinlockActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/PinlockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnLocker"
.end annotation


# static fields
.field private static final APP_LOCKED_KEY:Ljava/lang/String; = "locked"

.field private static final MAX_RETRY_TIMES:I = 0x3


# instance fields
.field private mLockPreference:Landroid/content/SharedPreferences;

.field private mPasswordLayout:Landroid/view/View;

.field final synthetic this$0:Lcom/youdao/note/activity/PinlockActivity;


# direct methods
.method private constructor <init>(Lcom/youdao/note/activity/PinlockActivity;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 353
    iput-object p1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {p0, p1, v3}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V

    .line 358
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const-string v1, "lock"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/activity/PinlockActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mLockPreference:Landroid/content/SharedPreferences;

    .line 364
    iput-object v3, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mPasswordLayout:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/PinlockActivity;Lcom/youdao/note/activity/PinlockActivity$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;-><init>(Lcom/youdao/note/activity/PinlockActivity;)V

    return-void
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/PinlockActivity$UnLocker;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->veryfyPassword()V

    return-void
.end method

.method private showInputAgain()V
    .locals 1

    .prologue
    .line 402
    const v0, 0x7f0a001e

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabel(I)V

    .line 403
    const v0, 0x7f0a001f

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setSubLabel(I)V

    .line 404
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabelBgColor(I)V

    .line 405
    return-void
.end method

.method private switch2PasswordView()V
    .locals 3

    .prologue
    .line 421
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v2, 0x7f070061

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 422
    .local v0, logo:Landroid/view/View;
    if-eqz v0, :cond_0

    .line 423
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 425
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v2, 0x7f07009c

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    iget-object v2, v2, Lcom/youdao/note/activity/PinlockActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabelBgColor(I)V

    .line 427
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->hideCodeBox()V

    .line 428
    const v1, 0x7f0a001c

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabel(I)V

    .line 429
    const v1, 0x7f0a001d

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setSubLabel(I)V

    .line 430
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mPasswordLayout:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 431
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->hideKeyboard()V

    .line 432
    return-void
.end method

.method private verifyPassword(Ljava/lang/String;)Z
    .locals 2
    .parameter "password"

    .prologue
    .line 439
    invoke-static {p1}, Lcom/youdao/note/utils/MD5Digester;->digest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/PinlockActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private verifyPinCode(I)Z
    .locals 2
    .parameter "pinCode"

    .prologue
    .line 435
    invoke-static {p1}, Lcom/youdao/note/utils/IdUtils;->genPinCode(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/PinlockActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getPinLock()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private veryfyPassword()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 408
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v2, 0x7f07005f

    invoke-virtual {v1, v2}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 409
    .local v0, passwordView:Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->verifyPassword(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/PinlockActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1, v3}, Lcom/youdao/note/YNoteApplication;->setPinlockEnable(Z)Z

    .line 411
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mLockPreference:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "locked"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 412
    iget-object v1, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    #calls: Lcom/youdao/note/activity/PinlockActivity;->finishWithResult(Z)V
    invoke-static {v1, v4}, Lcom/youdao/note/activity/PinlockActivity;->access$400(Lcom/youdao/note/activity/PinlockActivity;Z)V

    .line 418
    :goto_0
    return-void

    .line 414
    :cond_0
    const v1, 0x7f0a001e

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabel(I)V

    .line 415
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabelBgColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public initContentView()V
    .locals 3

    .prologue
    .line 383
    invoke-super {p0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->initContentView()V

    .line 384
    const v0, 0x7f0a0023

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->setLabel(I)V

    .line 385
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v1, 0x7f07009b

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mPasswordLayout:Landroid/view/View;

    .line 386
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const v1, 0x7f070014

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/PinlockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/PinlockActivity$UnLocker$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker$1;-><init>(Lcom/youdao/note/activity/PinlockActivity$UnLocker;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mLockPreference:Landroid/content/SharedPreferences;

    const-string v1, "locked"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->switch2PasswordView()V

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->getCodeInputTimes()I

    move-result v0

    if-lez v0, :cond_0

    .line 396
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->showInputAgain()V

    goto :goto_0
.end method

.method public onLockCodeCompleted()V
    .locals 3

    .prologue
    .line 368
    invoke-super {p0}, Lcom/youdao/note/activity/PinlockActivity$BaseLocker;->onLockCodeCompleted()V

    .line 369
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->getCodeInputTimes()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 370
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->mLockPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "locked"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 371
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->switch2PasswordView()V

    .line 379
    :goto_0
    return-void

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->getCurrentCodes()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->verifyPinCode(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->onLockCodeVerified()V

    goto :goto_0

    .line 376
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->showInputAgain()V

    goto :goto_0
.end method

.method protected onLockCodeVerified()V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/youdao/note/activity/PinlockActivity$UnLocker;->this$0:Lcom/youdao/note/activity/PinlockActivity;

    const/4 v1, 0x1

    #calls: Lcom/youdao/note/activity/PinlockActivity;->finishWithResult(Z)V
    invoke-static {v0, v1}, Lcom/youdao/note/activity/PinlockActivity;->access$400(Lcom/youdao/note/activity/PinlockActivity;Z)V

    .line 444
    return-void
.end method
