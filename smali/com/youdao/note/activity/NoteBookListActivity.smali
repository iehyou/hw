.class public Lcom/youdao/note/activity/NoteBookListActivity;
.super Lcom/youdao/note/activity/BaseListFileActivity;
.source "NoteBookListActivity.java"


# static fields
.field private static final BUNDLE_LONGCLICKED_NOTEBOOKID:Ljava/lang/String; = "longclicked_notebook"

.field private static final MENU_DELETENOTEBOOK:I = 0x1

.field private static final MENU_RENAMENOTEBOOK:I


# instance fields
.field private fromRenameDialog:Z

.field private mLongclickedNotebookId:Ljava/lang/String;

.field private newBookEditText:Landroid/widget/EditText;

.field private newBookText:Ljava/lang/String;

.field private newBookView:Landroid/view/View;

.field private renameBookText:Ljava/lang/String;

.field private renameBookView:Landroid/view/View;

.field private renameEditText:Landroid/widget/EditText;

.field private selectedNoteBook:Lcom/youdao/note/data/NoteBook;

.field private showing_DIALOG_NOTEBOOK_EXIST:Z

.field private syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseListFileActivity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->showing_DIALOG_NOTEBOOK_EXIST:Z

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;

    return-object v0
.end method

.method static synthetic access$002(Lcom/youdao/note/activity/NoteBookListActivity;Lcom/youdao/note/data/NoteBook;)Lcom/youdao/note/data/NoteBook;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->selectedNoteBook:Lcom/youdao/note/data/NoteBook;

    return-object p1
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/youdao/note/activity/NoteBookListActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->showing_DIALOG_NOTEBOOK_EXIST:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/youdao/note/activity/NoteBookListActivity;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->showing_DIALOG_NOTEBOOK_EXIST:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/youdao/note/activity/NoteBookListActivity;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->fromRenameDialog:Z

    return v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/NoteBookListActivity;)Lcom/youdao/note/data/NoteBook;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->getSelectedNoteMeta()Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/NoteBookListActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->update()V

    return-void
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/NoteBookListActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/activity/NoteBookListActivity;->checkNotebookNameExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/NoteBookListActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    iput-object p1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/youdao/note/activity/NoteBookListActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$900(Lcom/youdao/note/activity/NoteBookListActivity;Ljava/lang/String;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/NoteBookListActivity;->checkNotebookNameExist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private checkNotebookNameExist(Ljava/lang/String;)Z
    .locals 1
    .parameter "title"

    .prologue
    .line 351
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/activity/NoteBookListActivity;->checkNotebookNameExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private checkNotebookNameExist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .parameter "title"
    .parameter "notebookId"

    .prologue
    .line 342
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 343
    .local v0, newNoteBookMeta:Lcom/youdao/note/data/NoteBook;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 344
    const/16 v1, 0x71

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->showDialog(I)V

    .line 345
    const/4 v1, 0x1

    .line 347
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getSelectedNoteMeta()Lcom/youdao/note/data/NoteBook;
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    return-object v0
.end method

.method private update()V
    .locals 7

    .prologue
    const v6, 0x7f0700a6

    const/16 v5, 0x8

    .line 117
    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v4

    if-nez v4, :cond_0

    .line 118
    const v4, 0x102000a

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/NoteBookListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 119
    .local v3, view:Landroid/view/View;
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 120
    const-string v4, "set require login layout visiable"

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/NoteBookListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 122
    .local v2, requireLoginView:Landroid/view/View;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 131
    .end local v3           #view:Landroid/view/View;
    :goto_0
    return-void

    .line 124
    .end local v2           #requireLoginView:Landroid/view/View;
    :cond_0
    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v4}, Lcom/youdao/note/datasource/DataSource;->listAllNoteBooksAsList()Ljava/util/List;

    move-result-object v1

    .line 125
    .local v1, notebooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    new-instance v0, Lcom/youdao/note/data/adapter/NoteBookListAdapter;

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/data/adapter/NoteBookListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 126
    .local v0, adapter:Lcom/youdao/note/data/adapter/NoteBookListAdapter;
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteBookListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 127
    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/NoteBookListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 128
    .restart local v2       #requireLoginView:Landroid/view/View;
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mListView:Landroid/widget/ListView;

    iget v5, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mListPost:I

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method


# virtual methods
.method protected initComponent()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->initComponent()V

    .line 70
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f070085

    const v1, 0x7f030030

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteBookListActivity;->requestWindowFeature(I)Z

    .line 58
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/NoteBookListActivity;->setContentView(I)V

    .line 59
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookView:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;

    .line 61
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookView:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookEditText:Landroid/widget/EditText;

    .line 63
    new-instance v0, Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    .line 64
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 9
    .parameter "id"

    .prologue
    const/4 v6, 0x0

    const v8, 0x1080027

    const v5, 0x7f0a00ab

    const v7, 0x7f0a00aa

    .line 201
    packed-switch p1, :pswitch_data_0

    .line 333
    :pswitch_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 203
    :pswitch_1
    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    .line 205
    .local v2, noteBook:Lcom/youdao/note/data/NoteBook;
    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const v1, 0x7f06000a

    .line 206
    .local v1, menuId:I
    :goto_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteBookListActivity$1;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteBookListActivity$1;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 205
    .end local v1           #menuId:I
    :cond_0
    const v1, 0x7f060009

    goto :goto_1

    .line 229
    .end local v2           #noteBook:Lcom/youdao/note/data/NoteBook;
    :pswitch_2
    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    .line 231
    .restart local v2       #noteBook:Lcom/youdao/note/data/NoteBook;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "${name}"

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a004d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "${name}"

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a00a6

    new-instance v5, Lcom/youdao/note/activity/NoteBookListActivity$3;

    invoke-direct {v5, p0}, Lcom/youdao/note/activity/NoteBookListActivity$3;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteBookListActivity$2;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteBookListActivity$2;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v7, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 256
    .end local v2           #noteBook:Lcom/youdao/note/data/NoteBook;
    :pswitch_3
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a004e

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteBookListActivity$4;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteBookListActivity$4;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v7, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 277
    :pswitch_4
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a004b

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteBookListActivity$5;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteBookListActivity$5;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v7, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 300
    :pswitch_5
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0072

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/activity/NoteBookListActivity$6;

    invoke-direct {v4, p0}, Lcom/youdao/note/activity/NoteBookListActivity$6;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v3, v5, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 314
    .local v0, dl:Landroid/app/Dialog;
    new-instance v3, Lcom/youdao/note/activity/NoteBookListActivity$7;

    invoke-direct {v3, p0}, Lcom/youdao/note/activity/NoteBookListActivity$7;-><init>(Lcom/youdao/note/activity/NoteBookListActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto/16 :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x6e
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 88
    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 89
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f050003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 90
    const/4 v1, 0x1

    return v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const-string v2, "on item click called."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteBook;

    .line 157
    .local v1, noteBook:Lcom/youdao/note/data/NoteBook;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/youdao/note/activity/NoteListActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "noteBook"

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const/4 v2, 0x4

    invoke-virtual {p0, v0, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 160
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/16 v2, 0x6e

    .line 357
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/data/NoteBook;

    .line 358
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    invoke-virtual {v0}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    .line 359
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->removeDialog(I)V

    .line 360
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/NoteBookListActivity;->showDialog(I)V

    .line 361
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 74
    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v1, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    :goto_0
    return v0

    .line 77
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0700c0

    if-ne v1, v2, :cond_1

    .line 78
    const-string v1, ""

    iput-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;

    .line 79
    const/16 v1, 0x70

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/NoteBookListActivity;->showDialog(I)V

    goto :goto_0

    .line 81
    :cond_1
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 5
    .parameter "id"
    .parameter "dialog"

    .prologue
    const/4 v2, 0x1

    .line 174
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/BaseListFileActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 175
    packed-switch p1, :pswitch_data_0

    .line 197
    :goto_0
    :pswitch_0
    return-void

    .line 177
    :pswitch_1
    iput-boolean v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->fromRenameDialog:Z

    .line 178
    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameBookText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->renameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    goto :goto_0

    .line 182
    :pswitch_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->fromRenameDialog:Z

    .line 183
    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->newBookText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 186
    :pswitch_3
    iput-boolean v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->showing_DIALOG_NOTEBOOK_EXIST:Z

    goto :goto_0

    .line 189
    :pswitch_4
    iget-object v2, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v3, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    .local v1, noteBook:Lcom/youdao/note/data/NoteBook;
    move-object v0, p2

    .line 190
    check-cast v0, Landroid/app/AlertDialog;

    .line 191
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "${name}"

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 193
    invoke-virtual {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "${name}"

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x6f
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 148
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 149
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 102
    const-string v0, "longclicked_notebook"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "longclicked_notebook"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    .line 105
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 106
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onResume()V

    .line 111
    invoke-super {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->onResume()V

    .line 112
    const-string v0, "on Resume called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->update()V

    .line 114
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "longclicked_notebook"

    iget-object v1, p0, Lcom/youdao/note/activity/NoteBookListActivity;->mLongclickedNotebookId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseListFileActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 99
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 1
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 135
    iget-object v0, p0, Lcom/youdao/note/activity/NoteBookListActivity;->syncbarDelegate:Lcom/youdao/note/activity/delegate/SyncbarDelegate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/youdao/note/activity/delegate/SyncbarDelegate;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 136
    packed-switch p1, :pswitch_data_0

    .line 143
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/BaseListFileActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 144
    return-void

    .line 138
    :pswitch_0
    if-eqz p3, :cond_0

    .line 139
    invoke-direct {p0}, Lcom/youdao/note/activity/NoteBookListActivity;->update()V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
