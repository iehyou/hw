.class public Lcom/youdao/note/activity/ResourceAdder;
.super Ljava/lang/Object;
.source "ResourceAdder.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/youdao/note/activity/ResourceAdder;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

.field private mDialog:Landroid/app/ProgressDialog;

.field private mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/youdao/note/task/local/AbsAddResourceTask",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;)V
    .locals 0
    .parameter "activity"
    .parameter "callback"

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    .line 65
    iput-object p2, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/ResourceAdder;)Landroid/app/Activity;
    .locals 1
    .parameter "x0"

    .prologue
    .line 32
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static getInstance(Landroid/app/Activity;Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;)Lcom/youdao/note/activity/ResourceAdder;
    .locals 2
    .parameter "activity"
    .parameter "callback"

    .prologue
    .line 52
    sget-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/youdao/note/activity/ResourceAdder;

    invoke-direct {v0, p0, p1}, Lcom/youdao/note/activity/ResourceAdder;-><init>(Landroid/app/Activity;Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;)V

    sput-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    .line 58
    :goto_0
    sget-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    .line 59
    sget-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    return-object v0

    .line 55
    :cond_0
    sget-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    iput-object p0, v0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    .line 56
    sget-object v0, Lcom/youdao/note/activity/ResourceAdder;->mInstance:Lcom/youdao/note/activity/ResourceAdder;

    iput-object p1, v0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    goto :goto_0
.end method


# virtual methods
.method public addImageResource(Landroid/net/Uri;I)V
    .locals 2
    .parameter "uri"
    .parameter "picFrom"

    .prologue
    .line 119
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 120
    return-void
.end method

.method public addImageResource(Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 2
    .parameter "uri"
    .parameter "picFrom"
    .parameter "meta"

    .prologue
    .line 115
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0, p2, p3}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 116
    return-void
.end method

.method public addImageResource([Landroid/net/Uri;)V
    .locals 2
    .parameter "uriArray"

    .prologue
    .line 123
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 124
    return-void
.end method

.method public addImageResource([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 2
    .parameter "uriArray"
    .parameter "picFrom"
    .parameter "meta"

    .prologue
    .line 88
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 89
    new-instance v0, Lcom/youdao/note/activity/ResourceAdder$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/youdao/note/activity/ResourceAdder$1;-><init>(Lcom/youdao/note/activity/ResourceAdder;[Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    .line 111
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    sget-object v1, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_VOIDS:[Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 112
    return-void
.end method

.method protected addImageResources([Landroid/net/Uri;)V
    .locals 0
    .parameter "uriArray"

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource([Landroid/net/Uri;)V

    .line 85
    return-void
.end method

.method public addOtherResource(Landroid/net/Uri;I)V
    .locals 2
    .parameter "uri"
    .parameter "resourceType"

    .prologue
    .line 128
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 129
    if-nez p2, :cond_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You should use the addImageResource()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_0
    new-instance v0, Lcom/youdao/note/activity/ResourceAdder$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/youdao/note/activity/ResourceAdder$2;-><init>(Lcom/youdao/note/activity/ResourceAdder;Landroid/net/Uri;I)V

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    .line 153
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    sget-object v1, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_VOIDS:[Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 154
    return-void
.end method

.method protected addResourceFromUri(Landroid/net/Uri;)V
    .locals 3
    .parameter "uri"

    .prologue
    const/4 v2, 0x1

    .line 71
    if-eqz p1, :cond_0

    .line 72
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, fileName:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/utils/FileUtils;->isImage(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    invoke-virtual {p0, p1, v2}, Lcom/youdao/note/activity/ResourceAdder;->addImageResource(Landroid/net/Uri;I)V

    .line 81
    .end local v0           #fileName:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 75
    .restart local v0       #fileName:Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->isAudio(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    const/4 v1, 0x4

    invoke-virtual {p0, p1, v1}, Lcom/youdao/note/activity/ResourceAdder;->addOtherResource(Landroid/net/Uri;I)V

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p0, p1, v2}, Lcom/youdao/note/activity/ResourceAdder;->addOtherResource(Landroid/net/Uri;I)V

    goto :goto_0
.end method

.method public createDialog()Landroid/app/ProgressDialog;
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    .line 167
    :goto_0
    return-object v0

    .line 162
    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    .line 163
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 164
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 165
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 166
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a009a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mDialog:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 172
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->cancel(Z)Z

    .line 178
    :cond_1
    iput-object v2, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    .line 179
    iput-object v2, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    .line 180
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .parameter "dialog"

    .prologue
    .line 206
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mTask:Lcom/youdao/note/task/local/AbsAddResourceTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->stop(Z)Z

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    invoke-interface {v0}, Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;->onCancle()V

    .line 212
    :cond_1
    return-void
.end method

.method public onFailed(Ljava/lang/Exception;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 183
    const-string v0, ""

    invoke-static {p0, v0, p1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 185
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a00f3

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 186
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;->onFailed(Ljava/lang/Exception;)V

    .line 189
    :cond_0
    return-void
.end method

.method public onFinishAll(Lcom/youdao/note/data/resource/IResourceMeta;)V
    .locals 2
    .parameter "resourceMeta"

    .prologue
    .line 198
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 199
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;->onFinishAll(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 202
    :cond_0
    return-void
.end method

.method public onFinishOne(Lcom/youdao/note/data/resource/IResourceMeta;)V
    .locals 1
    .parameter "resourceMeta"

    .prologue
    .line 192
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder;->mCallback:Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;

    invoke-interface {v0, p1}, Lcom/youdao/note/activity/ResourceAdder$ResourceAddCallback;->onFinishOne(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 195
    :cond_0
    return-void
.end method
