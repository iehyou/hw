.class Lcom/youdao/note/activity/MailSelectionActivity$6;
.super Ljava/lang/Object;
.source "MailSelectionActivity.java"

# interfaces
.implements Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/MailSelectionActivity;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/MailSelectionActivity;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/MailSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$6;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAlphabetSelected(Ljava/lang/String;)V
    .locals 1
    .parameter "alphabet"

    .prologue
    .line 186
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$6;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->hideSoftKeyboard()V
    invoke-static {v0}, Lcom/youdao/note/activity/MailSelectionActivity;->access$600(Lcom/youdao/note/activity/MailSelectionActivity;)V

    .line 187
    if-eqz p1, :cond_0

    .line 188
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$6;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/youdao/note/activity/MailSelectionActivity;->access$700(Lcom/youdao/note/activity/MailSelectionActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$6;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #getter for: Lcom/youdao/note/activity/MailSelectionActivity;->toast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/youdao/note/activity/MailSelectionActivity;->access$700(Lcom/youdao/note/activity/MailSelectionActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 190
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$6;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->scrollTo(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/youdao/note/activity/MailSelectionActivity;->access$1000(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/lang/String;)V

    .line 192
    :cond_0
    return-void
.end method
