.class Lcom/youdao/note/activity/MailSelectionActivity$4;
.super Ljava/lang/Object;
.source "MailSelectionActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/MailSelectionActivity;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/MailSelectionActivity;

.field final synthetic val$clearButton:Landroid/widget/ImageButton;

.field final synthetic val$searchBar:Landroid/widget/AutoCompleteTextView;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/MailSelectionActivity;Landroid/widget/AutoCompleteTextView;Landroid/widget/ImageButton;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 155
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    iput-object p2, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$searchBar:Landroid/widget/AutoCompleteTextView;

    iput-object p3, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$clearButton:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .parameter "s"

    .prologue
    .line 166
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$searchBar:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$clearButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewGone(Landroid/view/View;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$clearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->val$clearButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 163
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 158
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$4;->this$0:Lcom/youdao/note/activity/MailSelectionActivity;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    #calls: Lcom/youdao/note/activity/MailSelectionActivity;->doSearch(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/youdao/note/activity/MailSelectionActivity;->access$900(Lcom/youdao/note/activity/MailSelectionActivity;Ljava/lang/String;)V

    .line 159
    return-void
.end method
