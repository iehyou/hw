.class public abstract Lcom/youdao/note/activity/AbstractLoadNoteActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "AbstractLoadNoteActivity.java"


# static fields
.field private static final BUNDLE_NOTE_ID:Ljava/lang/String; = "bundle_noteId"

.field protected static final DIALOG_LOADING:I = 0xc9


# instance fields
.field protected loadDismissed:Z

.field protected mNote:Lcom/youdao/note/data/Note;

.field private mNoteId:Ljava/lang/String;

.field protected mTitleView:Landroid/widget/TextView;

.field protected mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mTitleView:Landroid/widget/TextView;

    .line 41
    iput-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    .line 43
    iput-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mWebView:Landroid/webkit/WebView;

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    .line 49
    iput-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected failedLoadNote(Lcom/youdao/note/data/RemoteErrorData;)V
    .locals 3
    .parameter "errorData"

    .prologue
    .line 178
    invoke-virtual {p1}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 179
    .local v0, exception:Ljava/lang/Exception;
    iget-boolean v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    .line 180
    .local v1, preLoadDismissed:Z
    iget-boolean v2, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    if-nez v2, :cond_0

    .line 181
    const/16 v2, 0xc9

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->dismissDialog(I)V

    .line 182
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    .line 184
    :cond_0
    instance-of v2, v0, Ljava/io/IOException;

    if-eqz v2, :cond_2

    .line 185
    const v2, 0x7f0a00a1

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 191
    :goto_0
    if-nez v1, :cond_1

    .line 192
    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->finish()V

    .line 194
    :cond_1
    return-void

    .line 186
    :cond_2
    instance-of v2, p0, Lcom/youdao/note/activity/BaseEditActivity;

    if-eqz v2, :cond_3

    .line 187
    const v2, 0x7f0a0035

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 189
    :cond_3
    const v2, 0x7f0a003c

    invoke-static {p0, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected abstract loadNote(Lcom/youdao/note/data/Note;)V
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v3, 0x7f070030

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 56
    .local v2, view:Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v3, v2, Landroid/webkit/WebView;

    if-eqz v3, :cond_0

    .line 57
    check-cast v2, Landroid/webkit/WebView;

    .end local v2           #view:Landroid/view/View;
    iput-object v2, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mWebView:Landroid/webkit/WebView;

    .line 60
    :cond_0
    const v3, 0x7f070089

    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mTitleView:Landroid/widget/TextView;

    .line 61
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    .line 62
    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "noteid"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    .line 63
    iget-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 64
    iget-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v4, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v0

    .line 65
    .local v0, noteMeta:Lcom/youdao/note/data/NoteMeta;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enter NoteDetail "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, title:Ljava/lang/String;
    iget-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    new-instance v3, Lcom/youdao/note/data/Note;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/youdao/note/data/Note;-><init>(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    .line 70
    .end local v0           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    .end local v1           #title:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v3, 0x1

    .line 100
    packed-switch p1, :pswitch_data_0

    .line 118
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 102
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 103
    .local v0, progressDialog:Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0099

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 106
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 107
    new-instance v1, Lcom/youdao/note/activity/AbstractLoadNoteActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity$1;-><init>(Lcom/youdao/note/activity/AbstractLoadNoteActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "bundle"

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 95
    const-string v0, "bundle_noteId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onResume()V

    .line 75
    iget-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v0

    .line 77
    .local v0, note:Lcom/youdao/note/data/Note;
    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadNote(Lcom/youdao/note/data/Note;)V

    .line 84
    .end local v0           #note:Lcom/youdao/note/data/Note;
    :cond_0
    :goto_0
    return-void

    .line 80
    .restart local v0       #note:Lcom/youdao/note/data/Note;
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->showLoadingDialog()V

    .line 81
    iget-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v2, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->pullNote(Lcom/youdao/note/data/NoteMeta;Z)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "bundle"

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    const-string v0, "bundle_noteId"

    iget-object v1, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mNoteId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 8
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 123
    sparse-switch p1, :sswitch_data_0

    .line 164
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/LockableActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 166
    .end local p2
    :cond_0
    :goto_0
    return-void

    .line 125
    .restart local p2
    :sswitch_0
    if-nez p3, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->showError()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 126
    check-cast p2, Lcom/youdao/note/data/RemoteErrorData;

    .end local p2
    invoke-virtual {p0, p2}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->failedLoadNote(Lcom/youdao/note/data/RemoteErrorData;)V

    goto :goto_0

    .line 130
    .restart local p2
    :sswitch_1
    if-eqz p3, :cond_1

    .line 131
    check-cast p2, Lcom/youdao/note/data/Note;

    .end local p2
    invoke-virtual {p0, p2}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadNote(Lcom/youdao/note/data/Note;)V

    goto :goto_0

    .line 132
    .restart local p2
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->showError()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    check-cast p2, Lcom/youdao/note/data/RemoteErrorData;

    .end local p2
    invoke-virtual {p0, p2}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->failedLoadNote(Lcom/youdao/note/data/RemoteErrorData;)V

    goto :goto_0

    .line 137
    .restart local p2
    :sswitch_2
    if-eqz p3, :cond_2

    .line 138
    const-string v4, "Got notified of thumbnail."

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p2

    .line 139
    check-cast v3, Lcom/youdao/note/data/Thumbnail;

    .line 140
    .local v3, thumbnail:Lcom/youdao/note/data/Thumbnail;
    iget-object v4, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_0

    .line 141
    iget-object v4, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "javascript:e=document.getElementById(\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/youdao/note/data/Thumbnail;->getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\");e.src=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v3}, Lcom/youdao/note/data/Thumbnail;->getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .end local v3           #thumbnail:Lcom/youdao/note/data/Thumbnail;
    :cond_2
    move-object v0, p2

    .line 147
    check-cast v0, Lcom/youdao/note/data/RemoteErrorData;

    .line 148
    .local v0, error:Lcom/youdao/note/data/RemoteErrorData;
    const/4 v2, 0x0

    .line 149
    .local v2, exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    invoke-virtual {v0}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v4

    instance-of v4, v4, Lcom/youdao/note/exceptions/DownloadResourceException;

    if-eqz v4, :cond_3

    .line 150
    invoke-virtual {v0}, Lcom/youdao/note/data/RemoteErrorData;->getException()Ljava/lang/Exception;

    move-result-object v2

    .end local v2           #exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    check-cast v2, Lcom/youdao/note/exceptions/DownloadResourceException;

    .line 153
    .restart local v2       #exception:Lcom/youdao/note/exceptions/DownloadResourceException;
    :cond_3
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v4, v4, Ljava/io/IOException;

    if-eqz v4, :cond_0

    .line 154
    invoke-virtual {v2}, Lcom/youdao/note/exceptions/DownloadResourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/io/IOException;

    .line 155
    .local v1, ex:Ljava/io/IOException;
    const-string v4, "No space left on device"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    const-string v4, "No space left on device."

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    const v4, 0x7f0a0098

    invoke-static {p0, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 123
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5 -> :sswitch_2
        0xe -> :sswitch_0
    .end sparse-switch
.end method

.method protected showError()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method protected showLoadingDialog()V
    .locals 1

    .prologue
    .line 169
    const/16 v0, 0xc9

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->showDialog(I)V

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->loadDismissed:Z

    .line 171
    return-void
.end method
