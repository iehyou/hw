.class public Lcom/youdao/note/activity/SettingActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/youdao/note/utils/Consts;
.implements Lcom/youdao/note/task/TaskManager$DataUpdateListener;


# static fields
.field private static final DIALOG_CLEARING:I = 0x3

.field private static final DIALOG_CLEAR_CONFIRM:I = 0x1

.field private static final DIALOG_NETWORK_ERROR:I = 0x7

.field private static final DIALOG_OPENGL2_NOT_AVAILABLE:I = 0x9

.field private static final FANCY_HANDWRITE:I = 0x0

.field private static final NORMAL_HANDWRITE:I = 0x1


# instance fields
.field private handwritePref:Landroid/preference/ListPreference;

.field private lastSyncKey:Ljava/lang/String;

.field private mAutoSyncKey:Ljava/lang/String;

.field private mCheckUpdateKey:Ljava/lang/String;

.field private mClearDataKey:Ljava/lang/String;

.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field private mHandwriteModeKey:Ljava/lang/String;

.field private mImageQualityKey:Ljava/lang/String;

.field private mLoginKey:Ljava/lang/String;

.field private mPinlockKey:Ljava/lang/String;

.field private mSetPinlockKey:Ljava/lang/String;

.field private mSyncManager:Lcom/youdao/note/task/SyncManager;

.field private mTaskManager:Lcom/youdao/note/task/TaskManager;

.field private mTrigerCount:I

.field private mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

.field private mUsedSpaceKey:Ljava/lang/String;

.field private mYNote:Lcom/youdao/note/YNoteApplication;

.field private userNameKey:Ljava/lang/String;

.field private userNamePreference:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->userNameKey:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->lastSyncKey:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mUsedSpaceKey:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mAutoSyncKey:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mCheckUpdateKey:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mImageQualityKey:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mHandwriteModeKey:Ljava/lang/String;

    .line 62
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mClearDataKey:Ljava/lang/String;

    .line 78
    iput-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    .line 266
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/SettingActivity;)Lcom/youdao/note/YNoteApplication;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/SettingActivity;)Landroid/preference/ListPreference;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/SettingActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/youdao/note/activity/SettingActivity;->updateHandwritePref()V

    return-void
.end method

.method private getSpaceSummary(Lcom/youdao/note/data/UserMeta;)Ljava/lang/String;
    .locals 8
    .parameter "userMeta"

    .prologue
    .line 223
    const-string v0, "%.2f%% %.2fM/%.0fM"

    .line 224
    .local v0, SUMMARY_TEMPLATE:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/youdao/note/data/UserMeta;->getUsedSpace()J

    move-result-wide v3

    long-to-double v3, v3

    const-wide/high16 v5, 0x4059

    mul-double/2addr v3, v5

    invoke-virtual {p1}, Lcom/youdao/note/data/UserMeta;->getQuotaSpace()J

    move-result-wide v5

    long-to-double v5, v5

    div-double v1, v3, v5

    .line 226
    .local v1, percent:D
    const-string v3, "%.2f%% %.2fM/%.0fM"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/youdao/note/data/UserMeta;->getUsedSpace()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getSizeInMegaByte(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/youdao/note/data/UserMeta;->getQuotaSpace()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/youdao/note/utils/UnitUtils;->getSizeInMegaByte(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private initPreference()V
    .locals 12

    .prologue
    .line 172
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mCheckUpdateKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 173
    .local v1, checkUpdatePreference:Landroid/preference/Preference;
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 175
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mHandwriteModeKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    check-cast v10, Landroid/preference/ListPreference;

    iput-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    .line 176
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v10}, Lcom/youdao/note/YNoteApplication;->isOpengGL2Available()Z

    move-result v10

    if-nez v10, :cond_0

    .line 177
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/youdao/note/activity/SettingActivity;->updateHandwritePref()V

    .line 181
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v10}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 182
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mClearDataKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 183
    .local v2, clearDataPreference:Landroid/preference/Preference;
    if-eqz v2, :cond_1

    .line 184
    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 186
    :cond_1
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 187
    .local v6, pinlockEnablePreference:Landroid/preference/Preference;
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mSetPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 188
    .local v7, setPinlockPreference:Landroid/preference/Preference;
    invoke-virtual {v7, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 189
    invoke-virtual {v6, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 191
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v10}, Lcom/youdao/note/YNoteApplication;->isPinlockEnable()Z

    move-result v10

    invoke-virtual {v7, v10}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 192
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->userNameKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    iput-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    .line 193
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    iget-object v11, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v11}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    invoke-virtual {v10, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 195
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v10}, Lcom/youdao/note/datasource/DataSource;->getUserMeta()Lcom/youdao/note/data/UserMeta;

    move-result-object v9

    .line 196
    .local v9, userMeta:Lcom/youdao/note/data/UserMeta;
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->lastSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 197
    .local v4, lastSyncePreference:Landroid/preference/Preference;
    invoke-virtual {v9}, Lcom/youdao/note/data/UserMeta;->getLastSynceTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/youdao/note/utils/StringUtils;->getPrettyTimeDetail(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mUsedSpaceKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    .line 200
    .local v8, usedSpacePreference:Landroid/preference/Preference;
    invoke-direct {p0, v9}, Lcom/youdao/note/activity/SettingActivity;->getSpaceSummary(Lcom/youdao/note/data/UserMeta;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mAutoSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 202
    .local v0, autoSyncPreference:Landroid/preference/ListPreference;
    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 203
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mImageQualityKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    .line 205
    .local v3, imageQualityPreference:Landroid/preference/ListPreference;
    invoke-virtual {v3, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 206
    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v3, v10}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 213
    .end local v0           #autoSyncPreference:Landroid/preference/ListPreference;
    .end local v2           #clearDataPreference:Landroid/preference/Preference;
    .end local v3           #imageQualityPreference:Landroid/preference/ListPreference;
    .end local v4           #lastSyncePreference:Landroid/preference/Preference;
    .end local v6           #pinlockEnablePreference:Landroid/preference/Preference;
    .end local v7           #setPinlockPreference:Landroid/preference/Preference;
    .end local v8           #usedSpacePreference:Landroid/preference/Preference;
    .end local v9           #userMeta:Lcom/youdao/note/data/UserMeta;
    :goto_0
    return-void

    .line 210
    :cond_2
    iget-object v10, p0, Lcom/youdao/note/activity/SettingActivity;->mLoginKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 211
    .local v5, loginPreference:Landroid/preference/Preference;
    invoke-virtual {v5, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0
.end method

.method private updateHandwritePref()V
    .locals 4

    .prologue
    .line 216
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 217
    .local v0, idx:I
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 220
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v2, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 371
    sparse-switch p1, :sswitch_data_0

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 373
    :sswitch_0
    if-ne p2, v2, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->finish()V

    goto :goto_0

    .line 375
    :cond_1
    if-nez p2, :cond_0

    .line 376
    const-string v2, "canceled."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :sswitch_1
    if-nez p2, :cond_2

    .line 381
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 382
    .local v0, pinlockEnablePreference:Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 383
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2, v4}, Lcom/youdao/note/YNoteApplication;->setPinlockEnable(Z)Z

    .line 384
    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->onContentChanged()V

    goto :goto_0

    .line 386
    .end local v0           #pinlockEnablePreference:Landroid/preference/CheckBoxPreference;
    :cond_2
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mSetPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 387
    .local v1, updatePinLockPreference:Landroid/preference/Preference;
    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 388
    invoke-static {v3}, Lcom/youdao/note/activity/LockableActivity;->setUnLocked(Z)V

    goto :goto_0

    .line 392
    .end local v1           #updatePinLockPreference:Landroid/preference/Preference;
    :sswitch_2
    if-ne p2, v2, :cond_0

    .line 393
    invoke-static {v3}, Lcom/youdao/note/activity/LockableActivity;->setUnLocked(Z)V

    goto :goto_0

    .line 397
    :sswitch_3
    if-ne p2, v2, :cond_3

    .line 398
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2, v4}, Lcom/youdao/note/YNoteApplication;->setPinlockEnable(Z)Z

    .line 399
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mSetPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 400
    .restart local v1       #updatePinLockPreference:Landroid/preference/Preference;
    invoke-virtual {v1, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    .line 402
    .end local v1           #updatePinLockPreference:Landroid/preference/Preference;
    :cond_3
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2, v3}, Lcom/youdao/note/YNoteApplication;->setPinlockEnable(Z)Z

    .line 403
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mPinlockKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 404
    .restart local v0       #pinlockEnablePreference:Landroid/preference/CheckBoxPreference;
    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 371
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xf -> :sswitch_1
        0x10 -> :sswitch_2
        0x11 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    new-instance v1, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    .line 100
    const-string v1, "OnCreate called."

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 102
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 103
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getSyncManager()Lcom/youdao/note/task/SyncManager;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    .line 104
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->getTaskManager()Lcom/youdao/note/task/TaskManager;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 105
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v1, p0}, Lcom/youdao/note/task/TaskManager;->registDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V

    .line 106
    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 107
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mCheckUpdateKey:Ljava/lang/String;

    .line 108
    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->userNameKey:Ljava/lang/String;

    .line 109
    const v1, 0x7f0a0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->lastSyncKey:Ljava/lang/String;

    .line 110
    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUsedSpaceKey:Ljava/lang/String;

    .line 111
    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mAutoSyncKey:Ljava/lang/String;

    .line 113
    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mClearDataKey:Ljava/lang/String;

    .line 114
    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mLoginKey:Ljava/lang/String;

    .line 115
    const v1, 0x7f0a000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mImageQualityKey:Ljava/lang/String;

    .line 116
    const v1, 0x7f0a000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mHandwriteModeKey:Ljava/lang/String;

    .line 117
    const v1, 0x7f0a0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mPinlockKey:Ljava/lang/String;

    .line 118
    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mSetPinlockKey:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    const v1, 0x7f050006

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SettingActivity;->addPreferencesFromResource(I)V

    .line 124
    :goto_0
    invoke-direct {p0}, Lcom/youdao/note/activity/SettingActivity;->initPreference()V

    .line 125
    const-string v1, "com.youdao.note.action.CHECK_UPDATE"

    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->checkUpdate()V

    .line 130
    :cond_0
    return-void

    .line 122
    :cond_1
    const v1, 0x7f050007

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SettingActivity;->addPreferencesFromResource(I)V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f0a00ab

    .line 304
    packed-switch p1, :pswitch_data_0

    .line 366
    :goto_0
    :pswitch_0
    return-object v0

    .line 307
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0053

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/SettingActivity$1;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/SettingActivity$1;-><init>(Lcom/youdao/note/activity/SettingActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00aa

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 335
    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 336
    .local v0, clearProgress:Landroid/app/ProgressDialog;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 337
    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 342
    .end local v0           #clearProgress:Landroid/app/ProgressDialog;
    :pswitch_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a00a1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 346
    :pswitch_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0011

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/youdao/note/activity/SettingActivity$2;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/SettingActivity$2;-><init>(Lcom/youdao/note/activity/SettingActivity;)V

    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 356
    :pswitch_5
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createCheckingUpdateDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 358
    :pswitch_6
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createCheckUpdateFailedDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 360
    :pswitch_7
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createNewVersionFoundDialog()Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 362
    :pswitch_8
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->createNoUpdateFoundDialog()Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 304
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 156
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0, p0}, Lcom/youdao/note/task/TaskManager;->unregistDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V

    .line 157
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 12
    .parameter "preference"
    .parameter "newValue"

    .prologue
    const/4 v11, 0x1

    .line 233
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mAutoSyncKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v8, p2

    .line 234
    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 235
    .local v7, syncMinute:I
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    invoke-virtual {v8, v7}, Lcom/youdao/note/task/SyncManager;->resetTime(I)V

    .line 236
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mAutoSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 237
    .local v0, autoSyncPreference:Landroid/preference/ListPreference;
    check-cast p2, Ljava/lang/String;

    .end local p2
    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 238
    .local v1, idx:I
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v8

    aget-object v8, v8, v1

    invoke-virtual {v0, v8}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 263
    .end local v0           #autoSyncPreference:Landroid/preference/ListPreference;
    .end local v1           #idx:I
    .end local v7           #syncMinute:I
    :cond_0
    :goto_0
    return v11

    .line 239
    .restart local p2
    :cond_1
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mImageQualityKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v2, p2

    .line 240
    check-cast v2, Ljava/lang/String;

    .line 241
    .local v2, imageQuality:Ljava/lang/String;
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mImageQualityKey:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/youdao/note/activity/SettingActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    .line 242
    .local v3, imageQualityPreference:Landroid/preference/ListPreference;
    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 243
    .restart local v1       #idx:I
    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v8

    aget-object v8, v8, v1

    invoke-virtual {v3, v8}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 245
    .end local v1           #idx:I
    .end local v2           #imageQuality:Ljava/lang/String;
    .end local v3           #imageQualityPreference:Landroid/preference/ListPreference;
    :cond_2
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mHandwriteModeKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v5, p2

    .line 246
    check-cast v5, Ljava/lang/String;

    .line 247
    .local v5, mode:Ljava/lang/String;
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    invoke-virtual {v8, v5}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    .line 248
    .restart local v1       #idx:I
    if-nez v1, :cond_3

    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v8}, Lcom/youdao/note/YNoteApplication;->isOpengGL2Available()Z

    move-result v8

    if-nez v8, :cond_3

    .line 249
    const/16 v8, 0x9

    invoke-virtual {p0, v8}, Lcom/youdao/note/activity/SettingActivity;->showDialog(I)V

    goto :goto_0

    .line 252
    :cond_3
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->handwritePref:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/youdao/note/activity/SettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060006

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v1

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 253
    .end local v1           #idx:I
    .end local v5           #mode:Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/youdao/note/activity/SettingActivity;->mPinlockKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 254
    check-cast p2, Ljava/lang/Boolean;

    .end local p2
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 255
    .local v6, pinlockEnable:Z
    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {v4, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 256
    .local v4, intent:Landroid/content/Intent;
    if-eqz v6, :cond_5

    .line 257
    const-string v8, "com.youdao.note.action.SETUP_PINLOCK"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const/16 v8, 0xf

    invoke-virtual {p0, v4, v8}, Lcom/youdao/note/activity/SettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 260
    :cond_5
    const/16 v8, 0x11

    invoke-virtual {p0, v4, v8}, Lcom/youdao/note/activity/SettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .parameter "preference"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 270
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pre clicked "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    iget-object v3, p0, Lcom/youdao/note/activity/SettingActivity;->mCheckUpdateKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 272
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v2}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->checkUpdate()V

    .line 273
    iget v2, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    .line 274
    iget v2, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    const/4 v3, 0x5

    if-le v2, v3, :cond_0

    .line 275
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    if-eqz v2, :cond_0

    .line 276
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 279
    :cond_0
    const-string v2, "Check update."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    :goto_0
    return v1

    .line 281
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/activity/SettingActivity;->mLoginKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 282
    iget-object v2, p0, Lcom/youdao/note/activity/SettingActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v3, "com.youdao.note.action.login"

    invoke-virtual {v2, p0, v3}, Lcom/youdao/note/YNoteApplication;->sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_2
    iget-object v3, p0, Lcom/youdao/note/activity/SettingActivity;->mClearDataKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 285
    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/SettingActivity;->showDialog(I)V

    :cond_3
    :goto_1
    move v1, v2

    .line 299
    goto :goto_0

    .line 286
    :cond_4
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->userNameKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 287
    iget v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    .line 288
    iget v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    const/16 v3, 0x32

    if-le v1, v3, :cond_3

    .line 289
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/FunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/SettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 291
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->userNamePreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 292
    const/16 v1, -0x64

    iput v1, p0, Lcom/youdao/note/activity/SettingActivity;->mTrigerCount:I

    goto :goto_1

    .line 294
    .end local v0           #intent:Landroid/content/Intent;
    :cond_5
    iget-object v1, p0, Lcom/youdao/note/activity/SettingActivity;->mSetPinlockKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 295
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/PinlockActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 296
    .restart local v0       #intent:Landroid/content/Intent;
    const-string v1, "com.youdao.note.action.UPDATE_PINLOCK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/SettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 167
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onRestoreState(Landroid/os/Bundle;)V

    .line 168
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 150
    const-string v0, "onResume called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "outState"

    .prologue
    .line 161
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onSaveState(Landroid/os/Bundle;)V

    .line 162
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 163
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 1
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 416
    const/16 v0, 0x15

    if-ne p1, v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/youdao/note/activity/SettingActivity;->mUpdateDelegate:Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;

    invoke-virtual {v0, p2, p3}, Lcom/youdao/note/activity/delegate/CheckUpdateDelegate;->onCheckUpdateResult(Lcom/youdao/note/data/BaseData;Z)V

    .line 419
    :cond_0
    return-void
.end method
