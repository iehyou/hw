.class Lcom/youdao/note/activity/NoteDetailActivity$2;
.super Ljava/lang/Object;
.source "NoteDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteDetailActivity;->initUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/NoteDetailActivity;

.field final synthetic val$frameLayout:Landroid/view/View;

.field final synthetic val$guide:Landroid/view/View;

.field final synthetic val$listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteDetailActivity;Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iput-object p2, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$frameLayout:Landroid/view/View;

    iput-object p3, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    iput-object p4, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$guide:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter "v"
    .parameter "event"

    .prologue
    const/16 v3, 0x8

    .line 140
    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$frameLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$listener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 141
    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$guide:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 142
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 143
    .local v0, animation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 144
    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$guide:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 145
    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->val$guide:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$2;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/NoteDetailActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/youdao/note/YNoteApplication;->setShowMailGuide(Z)V

    .line 147
    const/4 v1, 0x1

    return v1
.end method
