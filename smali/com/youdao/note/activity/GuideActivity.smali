.class public Lcom/youdao/note/activity/GuideActivity;
.super Lcom/youdao/note/activity/BaseActivity;
.source "GuideActivity.java"

# interfaces
.implements Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/GuideActivity$GuideAdapter;
    }
.end annotation


# static fields
.field private static final VIEW_COUNT:I = 0x2


# instance fields
.field private mLeftButton:Landroid/widget/Button;

.field private mRightButton:Landroid/widget/Button;

.field private mViewFlow:Lcom/youdao/note/ui/viewflow/ViewFlow;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;-><init>()V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 25
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity;->mLeftButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$002(Lcom/youdao/note/activity/GuideActivity;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 25
    iput-object p1, p0, Lcom/youdao/note/activity/GuideActivity;->mLeftButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/GuideActivity;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 25
    iget-object v0, p0, Lcom/youdao/note/activity/GuideActivity;->mRightButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$102(Lcom/youdao/note/activity/GuideActivity;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 25
    iput-object p1, p0, Lcom/youdao/note/activity/GuideActivity;->mRightButton:Landroid/widget/Button;

    return-object p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 35
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/GuideActivity;->requestWindowFeature(I)Z

    .line 36
    const v1, 0x7f030013

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/GuideActivity;->setContentView(I)V

    .line 37
    invoke-super {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v1, 0x7f070033

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/GuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/ui/viewflow/ViewFlow;

    iput-object v1, p0, Lcom/youdao/note/activity/GuideActivity;->mViewFlow:Lcom/youdao/note/ui/viewflow/ViewFlow;

    .line 39
    iget-object v1, p0, Lcom/youdao/note/activity/GuideActivity;->mViewFlow:Lcom/youdao/note/ui/viewflow/ViewFlow;

    new-instance v2, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/GuideActivity$GuideAdapter;-><init>(Lcom/youdao/note/activity/GuideActivity;)V

    invoke-virtual {v1, v2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setAdapter(Landroid/widget/Adapter;)V

    .line 40
    const v1, 0x7f070034

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/GuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/viewflow/FlowIndicator;

    .line 41
    .local v0, indicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;
    iget-object v1, p0, Lcom/youdao/note/activity/GuideActivity;->mViewFlow:Lcom/youdao/note/ui/viewflow/ViewFlow;

    invoke-virtual {v1, v0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setFlowIndicator(Lcom/youdao/note/ui/viewflow/FlowIndicator;)V

    .line 42
    iget-object v1, p0, Lcom/youdao/note/activity/GuideActivity;->mViewFlow:Lcom/youdao/note/ui/viewflow/ViewFlow;

    invoke-virtual {v1, p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setOnViewSwitchListener(Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;)V

    .line 43
    iget-object v1, p0, Lcom/youdao/note/activity/GuideActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->setNotFirst()V

    .line 44
    return-void
.end method

.method public onSwitched(Landroid/view/View;I)V
    .locals 0
    .parameter "view"
    .parameter "position"

    .prologue
    .line 107
    return-void
.end method
