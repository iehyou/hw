.class Lcom/youdao/note/activity/ResourceAdder$2;
.super Lcom/youdao/note/task/local/AddOtherResourceTask;
.source "ResourceAdder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/ResourceAdder;->addOtherResource(Landroid/net/Uri;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/ResourceAdder;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/ResourceAdder;Landroid/net/Uri;I)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 132
    iput-object p1, p0, Lcom/youdao/note/activity/ResourceAdder$2;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/local/AddOtherResourceTask;-><init>(Landroid/net/Uri;I)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 145
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$2;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFailed(Ljava/lang/Exception;)V

    .line 146
    return-void
.end method

.method protected onFinishAll(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 140
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$2;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFinishAll(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 141
    return-void
.end method

.method protected onFinishOne(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter "meta"

    .prologue
    .line 135
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$2;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    invoke-virtual {v0, p1}, Lcom/youdao/note/activity/ResourceAdder;->onFinishOne(Lcom/youdao/note/data/resource/IResourceMeta;)V

    .line 136
    return-void
.end method

.method protected onResouceTooLarge(Ljava/lang/String;J)V
    .locals 3
    .parameter "fileName"
    .parameter "fileSize"

    .prologue
    .line 150
    iget-object v0, p0, Lcom/youdao/note/activity/ResourceAdder$2;->this$0:Lcom/youdao/note/activity/ResourceAdder;

    #getter for: Lcom/youdao/note/activity/ResourceAdder;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/youdao/note/activity/ResourceAdder;->access$000(Lcom/youdao/note/activity/ResourceAdder;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a00f4

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 151
    return-void
.end method
