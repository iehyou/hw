.class public interface abstract Lcom/youdao/note/activity/ActivityConsts$ACTION;
.super Ljava/lang/Object;
.source "ActivityConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/ActivityConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ACTION"
.end annotation


# static fields
.field public static final ACCESS_DENIED:Ljava/lang/String; = "com.youdao.note.action.ACCESS_DENIED"

.field public static final CHECK_UPDATE:Ljava/lang/String; = "com.youdao.note.action.CHECK_UPDATE"

.field public static final CORRELATION_CONFIRM:Ljava/lang/String; = "com.youdao.note.action.CORRELATION_CONFIRM"

.field public static final CREATE_AUDIO:Ljava/lang/String; = "com.youdao.note.action.CREATE_AUDIO"

.field public static final CREATE_GALLARY:Ljava/lang/String; = "com.youdao.note.action.CREATE_GALLARY"

.field public static final CREATE_HANDWRITE:Ljava/lang/String; = "com.youdao.note.action.CREATE_HANDWRITE"

.field public static final CREATE_PREFIX:Ljava/lang/String; = "com.youdao.note.action.CREATE_"

.field public static final CREATE_RECORD:Ljava/lang/String; = "com.youdao.note.action.CREATE_RECORD"

.field public static final CREATE_RESOURCE:Ljava/lang/String; = "com.youdao.note.action.CREATE_RESOURCE"

.field public static final CREATE_SNAPSHOT:Ljava/lang/String; = "com.youdao.note.action.CREATE_SNAPSHOT"

.field public static final CREATE_TEXT:Ljava/lang/String; = "com.youdao.note.action.CREATE_TEXT"

.field public static final DOODLE:Ljava/lang/String; = "com.youdao.note.action.DOODLE"

.field public static final EDIT_DOODLE:Ljava/lang/String; = "com.youdao.note.action.EDIT_DOODLE"

.field public static final EDIT_HANDWRITE:Ljava/lang/String; = "com.youdao.note.action.EDIT_HANDWRITE"

.field public static final FINISH:Ljava/lang/String; = "com.youdao.note.action.FINISH"

.field public static final LOGIN:Ljava/lang/String; = "com.youdao.note.action.login"

.field public static final NEW_HANDWRITE:Ljava/lang/String; = "com.youdao.note.action.NEW_HANDWRITE"

.field public static final PREFIX:Ljava/lang/String; = "com.youdao.note.action."

.field public static final RECORD_PAUSE:Ljava/lang/String; = "com.youdao.note.action.RECORD_PAUSE"

.field public static final RECORD_RECORD:Ljava/lang/String; = "com.youdao.note.action.RECORD_RECORD"

.field public static final RECORD_STOP:Ljava/lang/String; = "com.youdao.note.action.RECORD_STOP"

.field public static final SETUP_PINLOCK:Ljava/lang/String; = "com.youdao.note.action.SETUP_PINLOCK"

.field public static final UPDATE_PINLOCK:Ljava/lang/String; = "com.youdao.note.action.UPDATE_PINLOCK"

.field public static final VIEW_RESOURCE:Ljava/lang/String; = "com.youdao.note.action.VIEW_RESOURCE"
