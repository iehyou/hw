.class public interface abstract Lcom/youdao/note/activity/ActivityConsts$REQUEST_CODE;
.super Ljava/lang/Object;
.source "ActivityConsts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/ActivityConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "REQUEST_CODE"
.end annotation


# static fields
.field public static final CLEAR_PINLOCK:I = 0x11

.field public static final DELETE:I = 0x1

.field public static final DOODLE:I = 0xc

.field public static final EDIT:I = 0x2

.field public static final EDIT_DOODLE:I = 0x14

.field public static final EDIT_HANDWRITE:I = 0x13

.field public static final HANDWRITING:I = 0xd

.field public static final IMAGE_RECTIFICATION:I = 0xb

.field public static final LOGIN:I = 0x3

.field public static final NOTEBOOK:I = 0x4

.field public static final NOTEMETA:I = 0xa

.field public static final PICKPHOTO:I = 0x6

.field public static final REGIST:I = 0x9

.field public static final SETUP_PINLOCK:I = 0xf

.field public static final SNAPSHOT:I = 0x5

.field public static final UNLOCK:I = 0xe

.field public static final UPDATE_PINLOCK:I = 0x10

.field public static final VIEW_DETAIL:I = 0x0

.field public static final VIEW_IMAGE:I = 0x8

.field public static final VIEW_RESOURCE:I = 0x12

.field public static final VIEW_RESOURCE_LIST:I = 0x7
