.class public abstract Lcom/youdao/note/activity/BaseEditActivity;
.super Lcom/youdao/note/activity/AbstractLoadNoteActivity;
.source "BaseEditActivity.java"

# interfaces
.implements Lcom/youdao/note/ui/EditFooterBar$EditActionListener;


# static fields
.field private static final DIALOG_CANCEL_CONFIRM:I = 0x2

.field protected static final DIALOG_SAVING:I = 0x1

.field private static final SAVE_RESULT_DEVICE_FULL:I = 0x2

.field private static final SAVE_RESULT_FAILED:I = 0x1

.field private static final SAVE_RESULT_SUCCEED:I = 0x0

.field private static final THUMBNAIL_TEMPLATE:Ljava/lang/String; = "<img border=\"0\" src=\"%s\" filelength=\"%d\" id=\"%s\"  class=\"android-resource-img\" onclick=\"window.View.viewResource(\'%s\')\" /></br><span>&nbsp;</br></span>"


# instance fields
.field protected mFooter:Lcom/youdao/note/ui/EditFooterBar;

.field protected mNoteBookId:Ljava/lang/String;

.field private mNoteBookTitle:Landroid/widget/TextView;

.field protected mResourceButton:Lcom/youdao/note/ui/ResourceButton;

.field protected mResourceMetaList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;"
        }
    .end annotation
.end field

.field protected oriNoteBookId:Ljava/lang/String;

.field savingDialog:Landroid/app/ProgressDialog;

.field private savingTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    .line 94
    iput-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    .line 273
    iput-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingTask:Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/BaseEditActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;->appendResources()V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/BaseEditActivity;Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->generateSnippet(Lcom/youdao/note/data/NoteMeta;)V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/BaseEditActivity;)Landroid/os/AsyncTask;
    .locals 1
    .parameter "x0"

    .prologue
    .line 77
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingTask:Landroid/os/AsyncTask;

    return-object v0
.end method

.method private appendResources()V
    .locals 17

    .prologue
    .line 431
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_4

    .line 432
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 433
    .local v11, sb:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v12}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    const/4 v1, 0x0

    .line 435
    .local v1, cameraCount:I
    const/4 v2, 0x0

    .line 436
    .local v2, doodleCount:I
    const/4 v4, 0x0

    .line 437
    .local v4, handwriteCount:I
    const/4 v8, 0x0

    .line 438
    .local v8, photoCount:I
    const/4 v9, 0x0

    .line 439
    .local v9, recordCount:I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 440
    .local v7, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getType()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 461
    :cond_0
    :goto_1
    :pswitch_0
    instance-of v12, v7, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    if-eqz v12, :cond_2

    .line 462
    const-string v12, "<img border=\"0\" src=\"%s\" filelength=\"%d\" id=\"%s\"  class=\"android-resource-img\" onclick=\"window.View.viewResource(\'%s\')\" /></br><span>&nbsp;</br></span>"

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v7}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-virtual {v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    invoke-virtual {v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    move-object v6, v7

    .line 442
    check-cast v6, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 443
    .local v6, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v6}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 444
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 445
    :cond_1
    invoke-virtual {v6}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    .line 446
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 450
    .end local v6           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :pswitch_2
    add-int/lit8 v2, v2, 0x1

    .line 451
    goto :goto_1

    .line 453
    :pswitch_3
    add-int/lit8 v4, v4, 0x1

    .line 454
    goto :goto_1

    .line 456
    :pswitch_4
    add-int/lit8 v9, v9, 0x1

    .line 457
    goto :goto_1

    .line 467
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-static {v12, v7}, Lcom/youdao/note/utils/HTMLUtils;->createResourceNode(Lcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/resource/BaseResourceMeta;)Lorg/htmlcleaner/TagNode;

    move-result-object v10

    .line 469
    .local v10, resourceNode:Lorg/htmlcleaner/TagNode;
    :try_start_0
    invoke-static {v10}, Lcom/youdao/note/utils/HTMLUtils;->serilizeHtml(Lorg/htmlcleaner/TagNode;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v12}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v12

    invoke-virtual {v12}, Lcom/youdao/note/data/NoteMeta;->setHasAttachment()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 471
    :catch_0
    move-exception v3

    .line 472
    .local v3, e:Ljava/io/IOException;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to append resource "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-static {v0, v12, v3}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 477
    .end local v3           #e:Ljava/io/IOException;
    .end local v7           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v10           #resourceNode:Lorg/htmlcleaner/TagNode;
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 478
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v12, v1}, Lcom/youdao/note/LogRecorder;->addCameraTimes(I)Z

    .line 479
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v12, v8}, Lcom/youdao/note/LogRecorder;->addPhotoTimes(I)Z

    .line 480
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v12, v2}, Lcom/youdao/note/LogRecorder;->addPaintTimes(I)Z

    .line 481
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v12, v4}, Lcom/youdao/note/LogRecorder;->addArtTimes(I)Z

    .line 482
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v12, v9}, Lcom/youdao/note/LogRecorder;->addRecordTimes(I)Z

    .line 483
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "new body is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v13}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 485
    .end local v1           #cameraCount:I
    .end local v2           #doodleCount:I
    .end local v4           #handwriteCount:I
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v8           #photoCount:I
    .end local v9           #recordCount:I
    .end local v11           #sb:Ljava/lang/StringBuilder;
    :cond_4
    return-void

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private generateSnippet(Lcom/youdao/note/data/NoteMeta;)V
    .locals 17
    .parameter "noteMeta"

    .prologue
    .line 359
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual/range {p1 .. p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 360
    .local v7, resourceList:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    if-lez v14, :cond_2

    .line 361
    const/4 v11, -0x1

    .line 362
    .local v11, snippetCandidtaeIdx:I
    const-wide/16 v5, 0x1800

    .line 363
    .local v5, maxSize:J
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v14

    if-ge v2, v14, :cond_1

    .line 364
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v14}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/youdao/note/utils/FileUtils;->isImage(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 365
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v15, v14}, Lcom/youdao/note/datasource/DataSource;->getResourceLength(Lcom/youdao/note/data/resource/BaseResourceMeta;)J

    move-result-wide v8

    .line 367
    .local v8, size:J
    cmp-long v14, v8, v5

    if-lez v14, :cond_0

    .line 368
    move v11, v2

    .line 369
    move-wide v5, v8

    .line 363
    .end local v8           #size:J
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 374
    :cond_1
    const/4 v14, -0x1

    if-le v11, v14, :cond_2

    .line 375
    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    .line 377
    .local v4, imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
    invoke-virtual {v4}, Lcom/youdao/note/data/resource/AbstractImageResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/youdao/note/data/NoteMeta;->setSnippetFID(Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v10

    .line 379
    .local v10, snippet:Lcom/youdao/note/data/Snippet;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v14, v4}, Lcom/youdao/note/datasource/DataSource;->getThumbnail(Lcom/youdao/note/data/resource/AbstractImageResourceMeta;)Lcom/youdao/note/data/Thumbnail;

    move-result-object v13

    .line 380
    .local v13, thumbnail:Lcom/youdao/note/data/Thumbnail;
    if-eqz v10, :cond_2

    .line 381
    invoke-virtual {v13}, Lcom/youdao/note/data/Thumbnail;->getContentBytes()[B

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v13}, Lcom/youdao/note/data/Thumbnail;->getContentBytes()[B

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    invoke-static/range {v14 .. v16}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 383
    .local v3, image:Landroid/graphics/Bitmap;
    const/16 v14, 0x5a

    invoke-static {v3, v14}, Lcom/youdao/note/utils/ImageUtils;->cropImage(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 384
    .local v12, snippetImage:Landroid/graphics/Bitmap;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "size is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 386
    sget-object v14, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v15, 0x64

    invoke-static {v12, v14, v15}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v1

    .line 387
    .local v1, bytes:[B
    invoke-virtual {v10, v1}, Lcom/youdao/note/data/Snippet;->setContentBytes([B)V

    .line 388
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v14, v10}, Lcom/youdao/note/datasource/DataSource;->writeSnippet(Lcom/youdao/note/data/Snippet;)Z

    .line 392
    .end local v1           #bytes:[B
    .end local v2           #i:I
    .end local v3           #image:Landroid/graphics/Bitmap;
    .end local v4           #imageMeta:Lcom/youdao/note/data/resource/AbstractImageResourceMeta;
    .end local v5           #maxSize:J
    .end local v10           #snippet:Lcom/youdao/note/data/Snippet;
    .end local v11           #snippetCandidtaeIdx:I
    .end local v12           #snippetImage:Landroid/graphics/Bitmap;
    .end local v13           #thumbnail:Lcom/youdao/note/data/Thumbnail;
    :cond_2
    return-void
.end method

.method private sentToResourceManageActivity()V
    .locals 3

    .prologue
    .line 257
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/youdao/note/activity/ResourceListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "resourceMetaList"

    iget-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 260
    const-string v1, "noteMeta"

    iget-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v2}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 261
    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 262
    return-void
.end method

.method private updateResources()V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceButton:Lcom/youdao/note/ui/ResourceButton;

    iget-object v1, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/ResourceButton;->updateResourceCount(I)V

    .line 563
    return-void
.end method


# virtual methods
.method protected addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter "resourceMeta"

    .prologue
    .line 553
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setNoteId(Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;->updateResources()V

    .line 556
    return-void
.end method

.method public cancleEdit()V
    .locals 3

    .prologue
    .line 139
    iget-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 140
    .local v1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget-object v2, p0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2, v1}, Lcom/youdao/note/datasource/DataSource;->deleteResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 142
    .end local v1           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/BaseEditActivity;->setResult(I)V

    .line 143
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseEditActivity;->finish()V

    .line 144
    return-void
.end method

.method protected finishEdit(I)V
    .locals 9
    .parameter "saveResult"

    .prologue
    const/4 v8, 0x2

    .line 395
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 396
    .local v0, data:Landroid/content/Intent;
    const-string v6, "note"

    iget-object v7, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 397
    const/4 v6, -0x1

    invoke-virtual {p0, v6, v0}, Lcom/youdao/note/activity/BaseEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 398
    if-nez p1, :cond_2

    .line 399
    const v6, 0x7f0a0095

    invoke-static {p0, v6}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 405
    :goto_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->getSkitchManager()Lcom/youdao/note/ui/skitch/SkitchMetaManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->destroy()V

    .line 406
    iget-object v6, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 407
    .local v4, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    instance-of v6, v4, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v6, :cond_0

    move-object v3, v4

    .line 408
    check-cast v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 409
    .local v3, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v6

    if-eq v6, v8, :cond_1

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 411
    :cond_1
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v5

    .line 412
    .local v5, tmpFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v1

    .line 413
    .local v1, deleted:Z
    if-nez v1, :cond_0

    .line 414
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete tmp file:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 400
    .end local v1           #deleted:Z
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    .end local v4           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v5           #tmpFile:Ljava/io/File;
    :cond_2
    if-ne p1, v8, :cond_3

    .line 401
    const v6, 0x7f0a0097

    invoke-static {p0, v6}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 403
    :cond_3
    const v6, 0x7f0a0096

    invoke-static {p0, v6}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 419
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_4
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseEditActivity;->finish()V

    .line 420
    return-void
.end method

.method protected initControls()V
    .locals 1

    .prologue
    .line 130
    const v0, 0x7f07002d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/EditFooterBar;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    .line 131
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v0, p0}, Lcom/youdao/note/ui/EditFooterBar;->setActionListener(Lcom/youdao/note/ui/EditFooterBar$EditActionListener;)V

    .line 132
    const v0, 0x7f070024

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/ResourceButton;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceButton:Lcom/youdao/note/ui/ResourceButton;

    .line 133
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceButton:Lcom/youdao/note/ui/ResourceButton;

    invoke-virtual {v0, p0}, Lcom/youdao/note/ui/ResourceButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    const v0, 0x7f070086

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookTitle:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f07008d

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method protected loadNote(Lcom/youdao/note/data/Note;)V
    .locals 1
    .parameter "note"

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteBook()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->updateNoteBookTitle(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method protected abstract obtainNote(Z)V
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v5, -0x1

    .line 490
    const-string v4, "OnActivityResult called."

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 491
    packed-switch p1, :pswitch_data_0

    .line 547
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 549
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.youdao.note.action.CREATE_TEXT"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 550
    return-void

    .line 493
    :pswitch_1
    iget-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v4}, Lcom/youdao/note/ui/EditFooterBar;->recoverPos()V

    .line 494
    if-ne p2, v5, :cond_0

    if-eqz p3, :cond_0

    .line 495
    const-string v4, "resourceMeta"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/HandwriteResourceMeta;

    .line 497
    .local v3, meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/BaseEditActivity;->addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 501
    .end local v3           #meta:Lcom/youdao/note/data/resource/HandwriteResourceMeta;
    :pswitch_2
    iget-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v4}, Lcom/youdao/note/ui/EditFooterBar;->recoverPos()V

    .line 502
    if-ne p2, v5, :cond_0

    if-eqz p3, :cond_0

    .line 503
    const-string v4, "resourceMeta"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/DoodleResourceMeta;

    .line 505
    .local v3, meta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/BaseEditActivity;->addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 509
    .end local v3           #meta:Lcom/youdao/note/data/resource/DoodleResourceMeta;
    :pswitch_3
    iget-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v4}, Lcom/youdao/note/ui/EditFooterBar;->recoverPos()V

    .line 510
    if-ne p2, v5, :cond_0

    if-eqz p3, :cond_0

    .line 511
    const-string v4, "resourceMeta"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 513
    .local v3, meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setPicFrom(I)V

    .line 514
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/BaseEditActivity;->addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 518
    .end local v3           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :pswitch_4
    iget-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v4}, Lcom/youdao/note/ui/EditFooterBar;->recoverPos()V

    .line 519
    if-ne p2, v5, :cond_0

    if-eqz p3, :cond_0

    .line 520
    const-string v4, "resourceMeta"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 522
    .restart local v3       #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setPicFrom(I)V

    .line 523
    invoke-virtual {p0, v3}, Lcom/youdao/note/activity/BaseEditActivity;->addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    goto :goto_0

    .line 529
    .end local v3           #meta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    :pswitch_5
    if-ne p2, v5, :cond_0

    if-eqz p3, :cond_0

    .line 531
    const-string v4, "resourceMetaList"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    .line 533
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;->updateResources()V

    .line 534
    const-string v4, "resourceMeta"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 535
    .local v1, audioToPlay:Ljava/io/Serializable;
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 536
    check-cast v0, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 538
    .local v0, audioMeta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :try_start_0
    iget-object v4, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    iget-object v5, p0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v5, v0}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/youdao/note/ui/EditFooterBar;->play(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 539
    :catch_0
    move-exception v2

    .line 540
    .local v2, e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to play resource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v2}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 491
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 162
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f07008d

    if-ne v0, v1, :cond_2

    .line 163
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    const/16 v0, 0x6b

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->showDialog(I)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const/16 v0, 0x68

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->showDialog(I)V

    goto :goto_0

    .line 169
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070024

    if-ne v0, v1, :cond_0

    .line 170
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;->sentToResourceManageActivity()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseEditActivity;->initControls()V

    .line 102
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseEditActivity;->updateResources()V

    .line 103
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->setVolumeControlStream(I)V

    .line 104
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 105
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mTitleView:Landroid/widget/TextView;

    new-instance v1, Lcom/youdao/note/activity/BaseEditActivity$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/BaseEditActivity$1;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const v3, 0x7f0a00ab

    const/4 v2, 0x1

    .line 568
    sparse-switch p1, :sswitch_data_0

    .line 624
    invoke-super {p0, p1}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 570
    :sswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0037

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 574
    :sswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    .line 575
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0a0094

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 578
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 579
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/youdao/note/activity/BaseEditActivity$3;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/BaseEditActivity$3;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 587
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 589
    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0051

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/youdao/note/activity/BaseEditActivity$5;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/BaseEditActivity$5;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00aa

    new-instance v2, Lcom/youdao/note/activity/BaseEditActivity$4;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/BaseEditActivity$4;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 610
    :sswitch_3
    iget-object v1, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNote:Lcom/youdao/note/data/Note;

    if-eqz v1, :cond_0

    .line 613
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookId:Ljava/lang/String;

    new-instance v1, Lcom/youdao/note/activity/BaseEditActivity$6;

    invoke-direct {v1, p0}, Lcom/youdao/note/activity/BaseEditActivity$6;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    invoke-static {p0, v0, v1}, Lcom/youdao/note/ui/DialogFactory;->getNoteBookPickDialog(Landroid/app/Activity;Ljava/lang/String;Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 568
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x68 -> :sswitch_3
        0x6b -> :sswitch_0
    .end sparse-switch
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    .line 211
    iget-object v5, p0, Lcom/youdao/note/activity/BaseEditActivity;->mResourceMetaList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 212
    .local v3, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    instance-of v5, v3, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v5, :cond_0

    move-object v2, v3

    .line 213
    check-cast v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 214
    .local v2, imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getPicFrom()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 216
    :cond_1
    invoke-virtual {v2}, Lcom/youdao/note/data/resource/ImageResourceMeta;->getTempFile()Ljava/io/File;

    move-result-object v4

    .line 217
    .local v4, tmpFile:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v0

    .line 218
    .local v0, deleted:Z
    if-nez v0, :cond_0

    .line 219
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delete tmp file:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 224
    .end local v0           #deleted:Z
    .end local v2           #imageMeta:Lcom/youdao/note/data/resource/ImageResourceMeta;
    .end local v3           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v4           #tmpFile:Ljava/io/File;
    :cond_2
    iget-object v5, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v5}, Lcom/youdao/note/ui/EditFooterBar;->destroy()V

    .line 225
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onDestroy()V

    .line 226
    return-void
.end method

.method public onDoodle()V
    .locals 2

    .prologue
    .line 181
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/resource/DoodleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 182
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.youdao.note.action.DOODLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const/16 v1, 0xc

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 184
    return-void
.end method

.method public onError(I)V
    .locals 2
    .parameter "errorCode"

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Action error, error code is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 246
    return-void
.end method

.method public onHandwrite()V
    .locals 2

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/resource/HandwritingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.youdao.note.action.NEW_HANDWRITE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 191
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 629
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 630
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->showDialog(I)V

    .line 631
    const/4 v0, 0x1

    .line 633
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter "intent"

    .prologue
    .line 124
    const-string v0, "statusBar"

    const-string v1, "entry_from"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->addRecordFromBarTimes()Z

    .line 127
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onPause()V

    .line 206
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/EditFooterBar;->pause()V

    .line 207
    return-void
.end method

.method public onPickPhoto()V
    .locals 2

    .prologue
    .line 198
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/resource/ImageViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 199
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 201
    return-void
.end method

.method public onRecord()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mFooter:Lcom/youdao/note/ui/EditFooterBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/EditFooterBar;->startRecord()V

    .line 250
    return-void
.end method

.method public onRecordFinished(Lcom/youdao/note/data/resource/AudioResourceMeta;)V
    .locals 1
    .parameter "audioMeta"

    .prologue
    .line 240
    const v0, 0x7f0a0085

    invoke-static {p0, v0}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 241
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/BaseEditActivity;->addResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 242
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0, p0}, Lcom/youdao/note/YNoteApplication;->sendToSearch(Landroid/app/Activity;)V

    .line 267
    const/4 v0, 0x0

    return v0
.end method

.method public onTakePhoto()V
    .locals 2

    .prologue
    .line 233
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/resource/ImageViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 236
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 0
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 640
    invoke-super {p0, p1, p2, p3}, Lcom/youdao/note/activity/AbstractLoadNoteActivity;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    .line 641
    return-void
.end method

.method protected saveNote()V
    .locals 2

    .prologue
    .line 279
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseEditActivity;->showDialog(I)V

    .line 280
    new-instance v0, Lcom/youdao/note/activity/BaseEditActivity$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/BaseEditActivity$2;-><init>(Lcom/youdao/note/activity/BaseEditActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingTask:Landroid/os/AsyncTask;

    .line 355
    iget-object v0, p0, Lcom/youdao/note/activity/BaseEditActivity;->savingTask:Landroid/os/AsyncTask;

    sget-object v1, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_VOIDS:[Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 356
    return-void
.end method

.method protected updateNoteBookTitle(Ljava/lang/String;)V
    .locals 3
    .parameter "noteBookId"

    .prologue
    .line 151
    iget-object v1, p0, Lcom/youdao/note/activity/BaseEditActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 152
    .local v0, noteBook:Lcom/youdao/note/data/NoteBook;
    if-eqz v0, :cond_0

    .line 153
    iget-object v1, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/activity/BaseEditActivity;->mNoteBookTitle:Landroid/widget/TextView;

    const v2, 0x7f0a002e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
