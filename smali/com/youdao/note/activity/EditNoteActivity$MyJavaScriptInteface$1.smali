.class Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;
.super Ljava/lang/Object;
.source "EditNoteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->onBodyFetched(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

.field final synthetic val$body:Ljava/lang/String;

.field final synthetic val$saveOnObtained:Z


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 198
    iput-object p1, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iput-object p2, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->val$body:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->val$saveOnObtained:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    const-string v1, "body fecthed."

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "body is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->val$body:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity;->mNote:Lcom/youdao/note/data/Note;

    iget-object v1, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->val$body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 206
    iget-boolean v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->val$saveOnObtained:Z

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    invoke-virtual {v0}, Lcom/youdao/note/activity/EditNoteActivity;->saveNote()V

    .line 208
    iget-object v0, p0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface$1;->this$1:Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/EditNoteActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/EditNoteActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder;->addEditNoteTimes()Z

    .line 210
    :cond_0
    return-void
.end method
