.class Lcom/youdao/note/activity/LoginActivity$1;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/LoginActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/activity/LoginActivity;

.field final synthetic val$mCopyRight:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/LoginActivity;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/youdao/note/activity/LoginActivity$1;->this$0:Lcom/youdao/note/activity/LoginActivity;

    iput-object p2, p0, Lcom/youdao/note/activity/LoginActivity$1;->val$mCopyRight:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSizeChanged(IIII)V
    .locals 6
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    const/16 v2, 0x8

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "on size changed called."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    if-ge p2, p4, :cond_1

    .line 117
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->this$0:Lcom/youdao/note/activity/LoginActivity;

    #getter for: Lcom/youdao/note/activity/LoginActivity;->mLoginLogo:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/LoginActivity;->access$000(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->this$0:Lcom/youdao/note/activity/LoginActivity;

    #getter for: Lcom/youdao/note/activity/LoginActivity;->mRegistHelpView:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/LoginActivity;->access$100(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->val$mCopyRight:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    sub-int v0, p2, p4

    int-to-double v0, v0

    int-to-double v2, p4

    const-wide v4, 0x3fd3333333333333L

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->this$0:Lcom/youdao/note/activity/LoginActivity;

    #getter for: Lcom/youdao/note/activity/LoginActivity;->mLoginLogo:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/LoginActivity;->access$000(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 122
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->this$0:Lcom/youdao/note/activity/LoginActivity;

    #getter for: Lcom/youdao/note/activity/LoginActivity;->mRegistHelpView:Landroid/view/View;
    invoke-static {v0}, Lcom/youdao/note/activity/LoginActivity;->access$100(Lcom/youdao/note/activity/LoginActivity;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lcom/youdao/note/activity/LoginActivity$1;->val$mCopyRight:Landroid/view/View;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    goto :goto_0
.end method
