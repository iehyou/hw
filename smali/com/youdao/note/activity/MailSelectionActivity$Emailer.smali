.class Lcom/youdao/note/activity/MailSelectionActivity$Emailer;
.super Ljava/lang/Object;
.source "MailSelectionActivity.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/activity/MailSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Emailer"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x6115030f277863f6L


# instance fields
.field private address:Ljava/lang/String;

.field private contactId:J

.field private index:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private selected:Z

.field private type:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/activity/MailSelectionActivity$1;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 367
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->address:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->address:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->selected:Z

    return p1
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 367
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 367
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->type:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->type:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)J
    .locals 2
    .parameter "x0"

    .prologue
    .line 367
    iget-wide v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->contactId:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;J)J
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-wide p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->contactId:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 367
    iget-object v0, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->index:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/youdao/note/activity/MailSelectionActivity$Emailer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 367
    iput-object p1, p0, Lcom/youdao/note/activity/MailSelectionActivity$Emailer;->index:Ljava/lang/String;

    return-object p1
.end method
