.class Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;
.super Ljava/lang/Object;
.source "NoteDetailActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->onBodyFetched(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

.field final synthetic val$noteBody:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 384
    iput-object p1, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iput-object p2, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->val$noteBody:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 387
    const-string v0, "Note content updated."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->val$noteBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 390
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v0, v0, Lcom/youdao/note/activity/NoteDetailActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v1, p0, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface$2;->this$1:Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;

    iget-object v1, v1, Lcom/youdao/note/activity/NoteDetailActivity$MyJavaScriptInteface;->this$0:Lcom/youdao/note/activity/NoteDetailActivity;

    iget-object v1, v1, Lcom/youdao/note/activity/NoteDetailActivity;->mNote:Lcom/youdao/note/data/Note;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->updateNoteCache(Lcom/youdao/note/data/Note;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    :goto_0
    return-void

    .line 391
    :catch_0
    move-exception v0

    goto :goto_0
.end method
