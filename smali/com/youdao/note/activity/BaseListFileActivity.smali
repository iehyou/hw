.class public abstract Lcom/youdao/note/activity/BaseListFileActivity;
.super Lcom/youdao/note/activity/LockableActivity;
.source "BaseListFileActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field protected mEmptyView:Landroid/view/View;

.field private mHeadView:Landroid/view/View;

.field protected mListPost:I

.field protected mListView:Landroid/widget/ListView;

.field protected notesTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/youdao/note/activity/LockableActivity;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    .line 42
    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->notesTitle:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    .line 46
    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    .line 48
    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListPost:I

    return-void
.end method

.method private updateFixedHeader(Lcom/youdao/note/data/adapter/SectionItem;)V
    .locals 8
    .parameter "item"

    .prologue
    .line 157
    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 158
    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v2

    .line 161
    .local v2, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const/4 v1, 0x0

    .line 165
    .local v1, labelText:Ljava/lang/String;
    invoke-virtual {v2}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v4, v4, Lcom/youdao/note/data/adapter/SectionAdapter;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v4, Lcom/youdao/note/data/adapter/SectionAdapter;

    invoke-virtual {v4}, Lcom/youdao/note/data/adapter/SectionAdapter;->getUnsyncedTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0043

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 172
    :goto_0
    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    const v5, 0x7f070017

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 173
    .local v3, textView:Landroid/widget/TextView;
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    const v5, 0x7f070018

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    .local v0, countView:Landroid/widget/TextView;
    iget-object v4, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v4, Lcom/youdao/note/data/adapter/SectionAdapter;

    invoke-virtual {v4, v1}, Lcom/youdao/note/data/adapter/SectionAdapter;->getSectionCount(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    return-void

    .line 170
    .end local v0           #countView:Landroid/widget/TextView;
    .end local v3           #textView:Landroid/widget/TextView;
    :cond_1
    sget-object v4, Lcom/youdao/note/ui/ViewInflatter;->labelFormator:Ljava/text/SimpleDateFormat;

    new-instance v5, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected needLock()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/youdao/note/activity/LockableActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseListFileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/YNoteApplication;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 54
    const v0, 0x7f07005a

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->notesTitle:Landroid/widget/TextView;

    .line 55
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    .line 56
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    .line 57
    const v0, 0x7f07005b

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 61
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onDestroy()V

    .line 93
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const-string v3, "on item click called."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v3, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/youdao/note/data/adapter/SectionItem;

    if-eqz v3, :cond_0

    .line 108
    iget-object v3, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/youdao/note/data/adapter/SectionItem;

    .line 109
    .local v2, sectionItem:Lcom/youdao/note/data/adapter/SectionItem;
    invoke-virtual {v2}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v3

    if-nez v3, :cond_1

    .line 110
    const-string v3, "Label clicked."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    .end local v2           #sectionItem:Lcom/youdao/note/data/adapter/SectionItem;
    :cond_0
    :goto_0
    return-void

    .line 111
    .restart local v2       #sectionItem:Lcom/youdao/note/data/adapter/SectionItem;
    :cond_1
    invoke-virtual {v2}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 112
    invoke-virtual {v2}, Lcom/youdao/note/data/adapter/SectionItem;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 113
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/youdao/note/activity/NoteDetailActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "noteid"

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "noteMeta is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Lcom/youdao/note/activity/BaseListFileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 117
    iget-object v3, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v3}, Lcom/youdao/note/LogRecorder;->addViewDetailTimes()Z

    goto :goto_0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/youdao/note/activity/LockableActivity;->onResume()V

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "set list post to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListPost:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    .prologue
    .line 138
    iget-object v2, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 140
    .local v1, o:Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/youdao/note/data/adapter/SectionItem;

    if-nez v2, :cond_1

    .line 154
    .end local v1           #o:Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .restart local v1       #o:Ljava/lang/Object;
    :cond_1
    move-object v0, v1

    .line 143
    check-cast v0, Lcom/youdao/note/data/adapter/SectionItem;

    .line 144
    .local v0, item:Lcom/youdao/note/data/adapter/SectionItem;
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v2

    if-nez v2, :cond_2

    .line 145
    const-string v2, "ddfadfasdf"

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v2, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    add-int/lit8 v3, p2, 0x1

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #item:Lcom/youdao/note/data/adapter/SectionItem;
    check-cast v0, Lcom/youdao/note/data/adapter/SectionItem;

    .line 147
    .restart local v0       #item:Lcom/youdao/note/data/adapter/SectionItem;
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->updateFixedHeader(Lcom/youdao/note/data/adapter/SectionItem;)V

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 149
    invoke-direct {p0, v0}, Lcom/youdao/note/activity/BaseListFileActivity;->updateFixedHeader(Lcom/youdao/note/data/adapter/SectionItem;)V

    goto :goto_0

    .line 150
    :cond_3
    invoke-virtual {v0}, Lcom/youdao/note/data/adapter/SectionItem;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 151
    iget-object v2, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 130
    if-nez p2, :cond_0

    .line 131
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListPost:I

    .line 133
    :cond_0
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 3
    .parameter "listAdapter"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 69
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :cond_1
    :goto_0
    return-void

    .line 75
    :cond_2
    iput-object p1, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mAdapter:Landroid/widget/ListAdapter;

    .line 76
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mHeadView:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 81
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 83
    :cond_3
    const-string v0, "set empty gone."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/youdao/note/activity/BaseListFileActivity;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
