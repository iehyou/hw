.class public Lcom/youdao/note/activity/WebActivity;
.super Landroid/app/Activity;
.source "WebActivity.java"

# interfaces
.implements Lcom/youdao/note/activity/ActivityConsts;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/activity/WebActivity$RegistWebClient;,
        Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;
    }
.end annotation


# static fields
.field public static final FEED_BACK:I = 0x2

.field private static final FEED_BACK_URL:Ljava/lang/String; = "http://m.note.youdao.com/soft-manager/feedback?soft=note"

.field public static final HELP:I = 0x1

.field private static final HELP_URL:Ljava/lang/String; = "http://note.youdao.com/help.html"

.field private static final LOGIN_PAGE:Ljava/lang/String; = "http://note.elibom.youdao.com/noteproxy/login"

.field private static final LOGIN_REG:Ljava/util/regex/Pattern; = null

.field private static final LOGIN_SUCCEED:Ljava/lang/String; = "http://note.elibom.youdao.com/noteproxy/list"

.field private static final MESSAGE_FINISH:I = 0x1

.field public static final REGIST:I = 0x3

.field private static final REGIST_URL:Ljava/lang/String; = "http://note.elibom.youdao.com/noteproxy/register?action=Register"


# instance fields
.field private mContentType:I

.field private mFeedBackLoadTimes:I

.field private mHandler:Landroid/os/Handler;

.field private mPassWord:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;

.field private mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    const-string v0, "http://note\\.elibom\\.youdao\\.com/noteproxy/login\\?.*username=([^&]*)&password=(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/activity/WebActivity;->LOGIN_REG:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    const/4 v0, 0x3

    iput v0, p0, Lcom/youdao/note/activity/WebActivity;->mContentType:I

    .line 53
    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    .line 55
    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/activity/WebActivity;->mFeedBackLoadTimes:I

    .line 65
    new-instance v0, Lcom/youdao/note/activity/WebActivity$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/WebActivity$1;-><init>(Lcom/youdao/note/activity/WebActivity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mHandler:Landroid/os/Handler;

    .line 156
    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/activity/WebActivity;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget v0, p0, Lcom/youdao/note/activity/WebActivity;->mFeedBackLoadTimes:I

    return v0
.end method

.method static synthetic access$208(Lcom/youdao/note/activity/WebActivity;)I
    .locals 2
    .parameter "x0"

    .prologue
    .line 34
    iget v0, p0, Lcom/youdao/note/activity/WebActivity;->mFeedBackLoadTimes:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/youdao/note/activity/WebActivity;->mFeedBackLoadTimes:I

    return v0
.end method

.method static synthetic access$300(Lcom/youdao/note/activity/WebActivity;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/youdao/note/activity/WebActivity;->LOGIN_REG:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/youdao/note/activity/WebActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 34
    iput-object p1, p0, Lcom/youdao/note/activity/WebActivity;->mUserName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/youdao/note/activity/WebActivity;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/youdao/note/activity/WebActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 34
    iput-object p1, p0, Lcom/youdao/note/activity/WebActivity;->mPassWord:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/youdao/note/activity/WebActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 34
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method private loadHtml()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    iget v0, p0, Lcom/youdao/note/activity/WebActivity;->mContentType:I

    packed-switch v0, :pswitch_data_0

    .line 107
    :goto_0
    return-void

    .line 94
    :pswitch_0
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "http://note.youdao.com/help.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;

    invoke-direct {v1, p0, v3}, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://m.note.youdao.com/soft-manager/feedback?soft=note&keyfrom="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;

    invoke-direct {v1, p0, v3}, Lcom/youdao/note/activity/WebActivity$PageLoadWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://note.elibom.youdao.com/noteproxy/register?action=Register&keyfrom="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/activity/WebActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/youdao/note/activity/WebActivity$RegistWebClient;

    invoke-direct {v1, p0, v3}, Lcom/youdao/note/activity/WebActivity$RegistWebClient;-><init>(Lcom/youdao/note/activity/WebActivity;Lcom/youdao/note/activity/WebActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setTitle()V
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/youdao/note/activity/WebActivity;->mContentType:I

    packed-switch v0, :pswitch_data_0

    .line 133
    :goto_0
    return-void

    .line 122
    :pswitch_0
    const v0, 0x7f0a006a

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/WebActivity;->setTitle(I)V

    goto :goto_0

    .line 125
    :pswitch_1
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/WebActivity;->setTitle(I)V

    goto :goto_0

    .line 128
    :pswitch_2
    const v0, 0x7f0a0069

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/WebActivity;->setTitle(I)V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 112
    const/4 v0, 0x0

    .line 113
    .local v0, mContainer:Landroid/view/ViewGroup;
    const v1, 0x7f07002f

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/WebActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #mContainer:Landroid/view/ViewGroup;
    check-cast v0, Landroid/view/ViewGroup;

    .line 114
    .restart local v0       #mContainer:Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 117
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 78
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/WebActivity;->requestWindowFeature(I)Z

    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const v1, 0x7f03004c

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/WebActivity;->setContentView(I)V

    .line 81
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 82
    invoke-virtual {p0}, Lcom/youdao/note/activity/WebActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 83
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "web_conetnt"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/youdao/note/activity/WebActivity;->mContentType:I

    .line 84
    const v1, 0x7f0700b0

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/WebActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/youdao/note/activity/WebActivity;->mWebView:Landroid/webkit/WebView;

    .line 85
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/WebActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 86
    invoke-direct {p0}, Lcom/youdao/note/activity/WebActivity;->setTitle()V

    .line 87
    invoke-direct {p0}, Lcom/youdao/note/activity/WebActivity;->loadHtml()V

    .line 88
    return-void
.end method
