.class public Lcom/youdao/note/activity/BaseActivity;
.super Landroid/app/Activity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/youdao/note/activity/ActivityConsts;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/youdao/note/task/TaskManager$DataUpdateListener;
.implements Lcom/youdao/note/utils/Consts;
.implements Landroid/content/DialogInterface$OnCancelListener;


# static fields
.field protected static final DIALOG_ABOUT:I = 0x6c

.field public static final DIALOG_CMWAP:I = 0x75

.field public static final DIALOG_DELETE_CONFIRM:I = 0x64

.field protected static final DIALOG_DELETE_NOTEBOOK:I = 0x73

.field public static final DIALOG_DETAIL:I = 0x69

.field public static final DIALOG_ITEM:I = 0x6a

.field protected static final DIALOG_LICENSE:I = 0x6d

.field protected static final DIALOG_LOADING_RESOURCE:I = 0x74

.field public static final DIALOG_NEED_LOGIN:I = 0x6b

.field protected static final DIALOG_NEW_FEATURES:I = 0x72

.field protected static final DIALOG_NEW_NOTEBOOK:I = 0x70

.field public static final DIALOG_NOTEBOOKS:I = 0x68

.field protected static final DIALOG_NOTEBOOK_EXIST:I = 0x71

.field protected static final DIALOG_NOTEBOOK_MENU:I = 0x6e

.field protected static final DIALOG_RENAME_NOTEBOOK:I = 0x6f


# instance fields
.field private baseDelegate:Lcom/youdao/note/activity/delegate/BaseDelegate;

.field protected mDataSource:Lcom/youdao/note/datasource/DataSource;

.field protected mLogRecorder:Lcom/youdao/note/LogRecorder;

.field protected mSyncManager:Lcom/youdao/note/task/SyncManager;

.field protected mTaskManager:Lcom/youdao/note/task/TaskManager;

.field protected mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/activity/BaseActivity;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;->sendToSearch()V

    return-void
.end method

.method private sendToSearch()V
    .locals 4

    .prologue
    .line 235
    const v2, 0x7f070067

    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 236
    .local v1, textView:Landroid/widget/EditText;
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/youdao/note/activity/SearchActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 238
    .local v0, intent:Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 239
    if-eqz v1, :cond_0

    .line 240
    const-string v2, "query"

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    :cond_0
    const-string v2, "noteBook"

    const-string v3, "/"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    :cond_1
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 246
    return-void
.end method


# virtual methods
.method protected checkLogin()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->baseDelegate:Lcom/youdao/note/activity/delegate/BaseDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/BaseDelegate;->checkLogin()Z

    move-result v0

    return v0
.end method

.method protected hideKeyboard(Landroid/os/IBinder;)V
    .locals 2
    .parameter "binder"

    .prologue
    .line 212
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/BaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 213
    .local v0, imm:Landroid/view/inputmethod/InputMethodManager;
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 214
    return-void
.end method

.method protected initComponent()V
    .locals 8

    .prologue
    .line 147
    const v6, 0x7f07007f

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 148
    .local v2, newNoteView:Landroid/view/View;
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$1;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$1;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v2, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 155
    const v6, 0x7f070082

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 156
    .local v3, noteBookListView:Landroid/view/View;
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$2;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$2;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v3, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 167
    const v6, 0x7f070081

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 168
    .local v4, noteListView:Landroid/view/View;
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$3;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$3;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v4, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 179
    const v6, 0x7f0700c3

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 180
    .local v5, searchActivity:Landroid/view/View;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$4;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$4;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v5, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 188
    const v6, 0x7f070003

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 189
    .local v1, mainActivity:Landroid/view/View;
    if-eqz v1, :cond_0

    .line 190
    instance-of v6, p0, Lcom/youdao/note/activity/MainActivity;

    if-eqz v6, :cond_1

    .line 191
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 202
    :cond_0
    :goto_0
    const v6, 0x7f070066

    invoke-virtual {p0, v6}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 203
    .local v0, loginActivity:Landroid/view/View;
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$6;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$6;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v0, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 209
    return-void

    .line 193
    .end local v0           #loginActivity:Landroid/view/View;
    :cond_1
    new-instance v6, Lcom/youdao/note/activity/BaseActivity$5;

    invoke-direct {v6, p0}, Lcom/youdao/note/activity/BaseActivity$5;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {p0, v1, v6}, Lcom/youdao/note/activity/BaseActivity;->setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 305
    packed-switch p1, :pswitch_data_0

    .line 322
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 307
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 308
    const-string v0, "login succeed."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    instance-of v0, p0, Lcom/youdao/note/activity/MainActivity;

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/youdao/note/YNoteApplication;->sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :cond_1
    if-nez p2, :cond_2

    .line 313
    const-string v0, "canceled."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->finish()V

    goto :goto_0

    .line 318
    :cond_2
    const-string v0, "login failed."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .parameter "dialog"

    .prologue
    .line 397
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter "dialog"
    .parameter "which"

    .prologue
    const/16 v0, 0x6c

    .line 347
    if-ne p2, v0, :cond_0

    .line 348
    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseActivity;->dismissDialog(I)V

    .line 350
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .parameter "v"

    .prologue
    .line 356
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 139
    const/4 v0, 0x0

    .line 140
    .local v0, mContainer:Landroid/view/ViewGroup;
    const v1, 0x7f07002f

    invoke-virtual {p0, v1}, Lcom/youdao/note/activity/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #mContainer:Landroid/view/ViewGroup;
    check-cast v0, Landroid/view/ViewGroup;

    .line 141
    .restart local v0       #mContainer:Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 144
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 95
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getSyncManager()Lcom/youdao/note/task/SyncManager;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mSyncManager:Lcom/youdao/note/task/SyncManager;

    .line 96
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getTaskManager()Lcom/youdao/note/task/TaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 97
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 98
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mLogRecorder:Lcom/youdao/note/LogRecorder;

    .line 99
    new-instance v0, Lcom/youdao/note/activity/delegate/BaseDelegate;

    invoke-direct {v0, p0}, Lcom/youdao/note/activity/delegate/BaseDelegate;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->baseDelegate:Lcom/youdao/note/activity/delegate/BaseDelegate;

    .line 100
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->initComponent()V

    .line 101
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter "id"
    .parameter "args"

    .prologue
    .line 361
    packed-switch p1, :pswitch_data_0

    .line 380
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 363
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00f7

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00f9

    new-instance v2, Lcom/youdao/note/activity/BaseActivity$8;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/BaseActivity$8;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00fa

    new-instance v2, Lcom/youdao/note/activity/BaseActivity$7;

    invoke-direct {v2, p0}, Lcom/youdao/note/activity/BaseActivity$7;-><init>(Lcom/youdao/note/activity/BaseActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 361
    :pswitch_data_0
    .packed-switch 0x75
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 119
    return-void
.end method

.method protected onIntentSend(Landroid/content/Intent;)V
    .locals 0
    .parameter "intent"

    .prologue
    .line 216
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .parameter "item"

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x1

    .line 270
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b7

    if-ne v5, v6, :cond_0

    .line 271
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/youdao/note/activity/WebActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    .local v2, intent:Landroid/content/Intent;
    const-string v5, "web_conetnt"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 299
    .end local v2           #intent:Landroid/content/Intent;
    :goto_0
    return v4

    .line 275
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b8

    if-ne v5, v6, :cond_1

    .line 276
    iget-object v5, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->logOut()V

    .line 277
    iget-object v5, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v6, "com.youdao.note.action.login"

    invoke-virtual {v5, p0, v6}, Lcom/youdao/note/YNoteApplication;->sendMainActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b5

    if-ne v5, v6, :cond_2

    .line 280
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/youdao/note/activity/SettingActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 281
    .restart local v2       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 283
    .end local v2           #intent:Landroid/content/Intent;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700ba

    if-ne v5, v6, :cond_4

    .line 284
    const/16 v4, 0x6c

    invoke-virtual {p0, v4}, Lcom/youdao/note/activity/BaseActivity;->showDialog(I)V

    .line 299
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 285
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const v6, 0x7f0700b6

    if-ne v5, v6, :cond_3

    .line 286
    const-string v5, "mailto:"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 287
    .local v1, emailTOUri:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v2, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 288
    .restart local v2       #intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a005b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, body:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a005a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 290
    .local v3, subject:Ljava/lang/String;
    const-string v5, "text/plain"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string v5, "android.intent.extra.EMAIL"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    const-string v5, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    const-string v5, "web_conetnt"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 295
    invoke-virtual {p0, v2}, Lcom/youdao/note/activity/BaseActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0, p0}, Lcom/youdao/note/task/TaskManager;->unregistDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V

    .line 112
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->stopAll()V

    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 114
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 251
    const v1, 0x7f0700b8

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 252
    .local v0, sigoutItem:Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 253
    iget-object v1, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    const v1, 0x7f02009d

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 255
    const v1, 0x7f0a008f

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 261
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 257
    :cond_1
    const v1, 0x7f0a0068

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 258
    const v1, 0x7f020095

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0, p0}, Lcom/youdao/note/task/TaskManager;->registDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V

    .line 106
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 107
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/youdao/note/activity/BaseActivity;->sendToSearch()V

    .line 330
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 124
    const-string v0, "onStart called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isEverLogin()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "com.youdao.note.action.login"

    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    instance-of v0, p0, Lcom/youdao/note/activity/LoginActivity;

    if-nez v0, :cond_2

    .line 127
    const-string v0, "Lognin"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-virtual {p0}, Lcom/youdao/note/activity/BaseActivity;->sendLogin()V

    .line 130
    :cond_2
    return-void
.end method

.method public onUpdate(ILcom/youdao/note/data/BaseData;Z)V
    .locals 0
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 343
    return-void
.end method

.method protected sendLogin()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/youdao/note/activity/BaseActivity;->baseDelegate:Lcom/youdao/note/activity/delegate/BaseDelegate;

    invoke-virtual {v0}, Lcom/youdao/note/activity/delegate/BaseDelegate;->sendLogin()V

    .line 266
    return-void
.end method

.method protected sendNewNote()V
    .locals 1

    .prologue
    .line 219
    const-string v0, "com.youdao.note.action.CREATE_TEXT"

    invoke-virtual {p0, v0}, Lcom/youdao/note/activity/BaseActivity;->sendNewNote(Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method protected sendNewNote(Ljava/lang/String;)V
    .locals 3
    .parameter "action"

    .prologue
    .line 223
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/youdao/note/activity/NewNoteActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 224
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string v1, "entry_from"

    const-string v2, "AppAddTimes"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/activity/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 227
    return-void
.end method

.method protected setOnClickListener(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0
    .parameter "view"
    .parameter "l"

    .prologue
    .line 334
    if-eqz p1, :cond_0

    .line 335
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    :cond_0
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0
    .parameter "intent"
    .parameter "requestCode"

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/youdao/note/activity/BaseActivity;->onIntentSend(Landroid/content/Intent;)V

    .line 231
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 232
    return-void
.end method
