.class Lcom/youdao/note/YNoteApplication$2;
.super Lcom/youdao/note/task/network/LoginTask;
.source "YNoteApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/YNoteApplication;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/YNoteApplication;


# direct methods
.method constructor <init>(Lcom/youdao/note/YNoteApplication;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 149
    iput-object p1, p0, Lcom/youdao/note/YNoteApplication$2;->this$0:Lcom/youdao/note/YNoteApplication;

    invoke-direct {p0, p2, p3, p4}, Lcom/youdao/note/task/network/LoginTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/Exception;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 156
    instance-of v0, p1, Lcom/youdao/note/exceptions/LoginException;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication$2;->this$0:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->logOut()V

    .line 159
    :cond_0
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/LoginResult;)V
    .locals 1
    .parameter "loginResult"

    .prologue
    .line 152
    iget-object v0, p0, Lcom/youdao/note/YNoteApplication$2;->this$0:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0, p1}, Lcom/youdao/note/YNoteApplication;->setCookie(Lcom/youdao/note/data/LoginResult;)Z

    .line 153
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 149
    check-cast p1, Lcom/youdao/note/data/LoginResult;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/YNoteApplication$2;->onSucceed(Lcom/youdao/note/data/LoginResult;)V

    return-void
.end method
