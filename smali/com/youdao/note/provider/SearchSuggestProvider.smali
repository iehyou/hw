.class public Lcom/youdao/note/provider/SearchSuggestProvider;
.super Landroid/content/SearchRecentSuggestionsProvider;
.source "SearchSuggestProvider.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = null

.field public static final MODE:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/youdao/note/provider/SearchSuggestProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/provider/SearchSuggestProvider;->AUTHORITY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/SearchRecentSuggestionsProvider;-><init>()V

    .line 22
    sget-object v0, Lcom/youdao/note/provider/SearchSuggestProvider;->AUTHORITY:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/provider/SearchSuggestProvider;->setupSuggestions(Ljava/lang/String;I)V

    .line 23
    return-void
.end method
