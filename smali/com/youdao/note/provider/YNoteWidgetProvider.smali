.class public Lcom/youdao/note/provider/YNoteWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "YNoteWidgetProvider.java"

# interfaces
.implements Lcom/youdao/note/activity/ActivityConsts;
.implements Lcom/youdao/note/utils/PreferenceKeys;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 15
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    .prologue
    .line 33
    move-object/from16 v0, p3

    array-length v1, v0

    .line 34
    .local v1, N:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 35
    aget v2, p3, v3

    .line 36
    .local v2, appWidgetId:I
    new-instance v12, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f030001

    invoke-direct {v12, v13, v14}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 37
    .local v12, views:Landroid/widget/RemoteViews;
    new-instance v10, Landroid/content/Intent;

    const-class v13, Lcom/youdao/note/activity/SearchActivity;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    .local v10, searchIntent:Landroid/content/Intent;
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v10, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    .line 40
    .local v11, searchPendingIntent:Landroid/app/PendingIntent;
    const v13, 0x7f070004

    invoke-virtual {v12, v13, v11}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 42
    new-instance v7, Landroid/content/Intent;

    const-class v13, Lcom/youdao/note/activity/NewNoteActivity;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    .local v7, newNoteIntent:Landroid/content/Intent;
    const-string v13, "entry_from"

    const-string v14, "WidgetAddTimes"

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const-string v13, "com.youdao.note.action.CREATE_TEXT"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v7, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 48
    .local v9, newTextNoewPendingIntent:Landroid/app/PendingIntent;
    const v13, 0x7f070005

    invoke-virtual {v12, v13, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 51
    const-string v13, "com.youdao.note.action.CREATE_SNAPSHOT"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v7, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 53
    .local v8, newSnapshotPendingIntent:Landroid/app/PendingIntent;
    const v13, 0x7f070006

    invoke-virtual {v12, v13, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 56
    const-string v13, "com.youdao.note.action.CREATE_RECORD"

    invoke-virtual {v7, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v7, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 58
    .local v6, newGallaryPendingIntent:Landroid/app/PendingIntent;
    const v13, 0x7f070007

    invoke-virtual {v12, v13, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 60
    new-instance v4, Landroid/content/Intent;

    const-class v13, Lcom/youdao/note/activity/MainActivity;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v4, mainIntent:Landroid/content/Intent;
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v4, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 62
    .local v5, mainPendingIntent:Landroid/app/PendingIntent;
    const v13, 0x7f070003

    invoke-virtual {v12, v13, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 63
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v12}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 34
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 65
    .end local v2           #appWidgetId:I
    .end local v4           #mainIntent:Landroid/content/Intent;
    .end local v5           #mainPendingIntent:Landroid/app/PendingIntent;
    .end local v6           #newGallaryPendingIntent:Landroid/app/PendingIntent;
    .end local v7           #newNoteIntent:Landroid/content/Intent;
    .end local v8           #newSnapshotPendingIntent:Landroid/app/PendingIntent;
    .end local v9           #newTextNoewPendingIntent:Landroid/app/PendingIntent;
    .end local v10           #searchIntent:Landroid/content/Intent;
    .end local v11           #searchPendingIntent:Landroid/app/PendingIntent;
    .end local v12           #views:Landroid/widget/RemoteViews;
    :cond_0
    return-void
.end method
