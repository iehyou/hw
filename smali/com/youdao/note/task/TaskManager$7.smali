.class Lcom/youdao/note/task/TaskManager$7;
.super Lcom/youdao/note/task/network/GetNoteTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullNote(Lcom/youdao/note/data/NoteMeta;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$async:Z


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteMeta;Z)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter

    .prologue
    .line 282
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$7;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-boolean p3, p0, Lcom/youdao/note/task/TaskManager$7;->val$async:Z

    invoke-direct {p0, p2}, Lcom/youdao/note/task/network/GetNoteTask;-><init>(Lcom/youdao/note/data/NoteMeta;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 292
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$7;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 293
    iget-boolean v0, p0, Lcom/youdao/note/task/TaskManager$7;->val$async:Z

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$7;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x2

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 296
    :cond_0
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/Note;)V
    .locals 3
    .parameter "note"

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/youdao/note/task/TaskManager$7;->val$async:Z

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$7;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 288
    :cond_0
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 282
    check-cast p1, Lcom/youdao/note/data/Note;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$7;->onSucceed(Lcom/youdao/note/data/Note;)V

    return-void
.end method
