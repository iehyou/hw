.class Lcom/youdao/note/task/TaskManager$10;
.super Lcom/youdao/note/task/network/GetResourceTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field progress:Lcom/youdao/note/data/ProgressData;

.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;IILcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 5
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter

    .prologue
    .line 341
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$10;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p6, p0, Lcom/youdao/note/task/TaskManager$10;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/youdao/note/task/network/GetResourceTask;-><init>(Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;II)V

    .line 343
    new-instance v0, Lcom/youdao/note/data/ProgressData;

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$10;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/youdao/note/task/TaskManager$10;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/youdao/note/data/ProgressData;-><init>(Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->progress:Lcom/youdao/note/data/ProgressData;

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 365
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x9

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 366
    return-void
.end method

.method public onPostExecute(Ljava/io/File;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 348
    invoke-super {p0, p1}, Lcom/youdao/note/task/network/GetResourceTask;->onPostExecute(Ljava/lang/Object;)V

    .line 349
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/youdao/note/task/TaskManager;->access$200(Lcom/youdao/note/task/TaskManager;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$10;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 341
    check-cast p1, Ljava/io/File;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$10;->onPostExecute(Ljava/io/File;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 4
    .parameter "values"

    .prologue
    .line 359
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->progress:Lcom/youdao/note/data/ProgressData;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/ProgressData;->setProgress(I)V

    .line 360
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$10;->progress:Lcom/youdao/note/data/ProgressData;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 361
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 341
    check-cast p1, [Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$10;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public onSucceed(Ljava/io/File;)V
    .locals 4
    .parameter "file"

    .prologue
    .line 354
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$10;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$10;->progress:Lcom/youdao/note/data/ProgressData;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 355
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 341
    check-cast p1, Ljava/io/File;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$10;->onSucceed(Ljava/io/File;)V

    return-void
.end method
