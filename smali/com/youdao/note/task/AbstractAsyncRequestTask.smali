.class public abstract Lcom/youdao/note/task/AbstractAsyncRequestTask;
.super Lcom/youdao/note/task/AsyncTaskWithExecuteResult;
.source "AbstractAsyncRequestTask.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$Request;
.implements Lcom/youdao/note/utils/Consts$APIS;
.implements Lcom/youdao/note/utils/Consts;
.implements Lcom/youdao/note/task/IManageableTask;
.implements Lcom/youdao/note/utils/EmptyInstance;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/youdao/note/task/AsyncTaskWithExecuteResult",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "TT;>;",
        "Lcom/youdao/note/utils/Consts$Request;",
        "Lcom/youdao/note/utils/Consts$APIS;",
        "Lcom/youdao/note/utils/Consts;",
        "Lcom/youdao/note/task/IManageableTask;",
        "Lcom/youdao/note/utils/EmptyInstance;"
    }
.end annotation


# static fields
.field private static final APN_PROJECTION:[Ljava/lang/String;

.field private static final APN_URI:Landroid/net/Uri;

.field private static mClient:Lorg/apache/http/client/HttpClient;


# instance fields
.field protected mManager:Lcom/youdao/note/task/RequestManager;

.field private mPost:Lorg/apache/http/client/methods/HttpPost;

.field protected mRequestUrl:Ljava/lang/String;

.field private mStatusCode:I

.field protected needAuth:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->APN_URI:Landroid/net/Uri;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "apn"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "proxy"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "port"

    aput-object v2, v0, v1

    sput-object v0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->APN_PROJECTION:[Ljava/lang/String;

    .line 71
    const-string v0, "ynote-android"

    invoke-static {v0}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mClient:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter "requestUrl"

    .prologue
    .line 85
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;-><init>(Ljava/lang/String;Z)V

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .parameter "requestUrl"
    .parameter "needAuth"

    .prologue
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->needAuth:Z

    .line 69
    iput-object v1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    .line 78
    iput-object v1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mStatusCode:I

    .line 94
    iput-object p1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    .line 95
    iput-boolean p2, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->needAuth:Z

    .line 96
    return-void
.end method

.method private addCookie(Lorg/apache/http/client/methods/HttpPost;)V
    .locals 5
    .parameter "post"

    .prologue
    .line 329
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getPersistCookie()Ljava/lang/String;

    move-result-object v0

    .line 330
    .local v0, persistCookie:Ljava/lang/String;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getSessionCookie()Ljava/lang/String;

    move-result-object v1

    .line 331
    .local v1, sessionCookie:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "persist cookie is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n session cookie is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    const-string v2, "cookie"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void
.end method

.method private checkSessionCookie(Lorg/apache/http/HttpResponse;)V
    .locals 2
    .parameter "response"

    .prologue
    .line 192
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-static {p1}, Lcom/youdao/note/utils/WebUtils;->getSessionCookie(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, sessionCookie:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/youdao/note/YNoteApplication;->setSessionCookie(Ljava/lang/String;)Z

    .line 196
    :cond_0
    return-void
.end method

.method private dump(Lorg/apache/http/client/methods/HttpPost;)V
    .locals 3
    .parameter "post"

    .prologue
    .line 258
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    if-eqz v2, :cond_0

    .line 259
    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    check-cast v1, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    .line 261
    .local v1, ueEntiry:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/utils/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    .end local v1           #ueEntiry:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :cond_0
    :goto_0
    return-void

    .line 262
    .restart local v1       #ueEntiry:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :catch_0
    move-exception v0

    .line 263
    .local v0, e:Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setUpApn(Lorg/apache/http/client/HttpClient;)V
    .locals 7
    .parameter "client"

    .prologue
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v5, 0x0

    .line 199
    invoke-virtual {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getApn()[Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, apn:[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 201
    aget-object v2, v0, v5

    .line 202
    .local v2, apnProxy:Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v1, v0, v4

    .line 203
    .local v1, apnPort:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 204
    const/16 v3, 0x50

    .line 205
    .local v3, port:I
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 206
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 208
    :cond_0
    invoke-virtual {p0, p1, v2, v3}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->addProxy(Lorg/apache/http/client/HttpClient;Ljava/lang/String;I)V

    .line 214
    .end local v1           #apnPort:Ljava/lang/String;
    .end local v2           #apnProxy:Ljava/lang/String;
    .end local v3           #port:I
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/youdao/note/YNoteApplication;->setIsCmwap(Z)V

    .line 213
    invoke-interface {p1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.route.default-proxy"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    goto :goto_0
.end method


# virtual methods
.method public addProxy(Lorg/apache/http/client/HttpClient;Ljava/lang/String;I)V
    .locals 3
    .parameter "client"
    .parameter "host"
    .parameter "port"

    .prologue
    .line 247
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "host "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    const-string v1, "10.0.0.172"

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/youdao/note/YNoteApplication;->setIsCmwap(Z)V

    .line 253
    :goto_0
    new-instance v0, Lorg/apache/http/HttpHost;

    invoke-direct {v0, p2, p3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 254
    .local v0, proxy:Lorg/apache/http/HttpHost;
    invoke-interface {p1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v2, "http.route.default-proxy"

    invoke-interface {v1, v2, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 255
    return-void

    .line 251
    .end local v0           #proxy:Lorg/apache/http/HttpHost;
    :cond_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/youdao/note/YNoteApplication;->setIsCmwap(Z)V

    goto :goto_0
.end method

.method public execute()V
    .locals 1

    .prologue
    .line 102
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    sget-object v0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->EMPTY_STRINGS:[Ljava/lang/String;

    invoke-super {p0, v0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 103
    return-void
.end method

.method protected failed(ILorg/apache/http/HttpResponse;Ljava/lang/Exception;)Ljava/lang/Object;
    .locals 4
    .parameter "statusCode"
    .parameter "response"
    .parameter "e"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/apache/http/HttpResponse;",
            "Ljava/lang/Exception;",
            ")TT;"
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v3, 0x0

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Returned status code is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p3}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 295
    if-nez p2, :cond_0

    .line 296
    :try_start_0
    const-string v1, "PostMetod is null."

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    invoke-virtual {p0, p3}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->onFailed(Ljava/lang/Exception;)V

    .line 305
    return-object v3

    .line 298
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "response "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "return content is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getResponseAsInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/utils/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, e1:Ljava/lang/Exception;
    const-string v1, "Failed to log response."

    invoke-static {p0, v1, p3}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected getApn()[Ljava/lang/String;
    .locals 9

    .prologue
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v3, 0x0

    .line 217
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/youdao/note/task/AbstractAsyncRequestTask;->APN_URI:Landroid/net/Uri;

    sget-object v2, Lcom/youdao/note/task/AbstractAsyncRequestTask;->APN_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 220
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 223
    .local v7, apnProxy:Ljava/lang/String;
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 225
    .local v6, apnPort:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const-string v6, "80"

    .line 228
    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v7, v3, v0

    const/4 v0, 0x1

    aput-object v6, v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 237
    .end local v6           #apnPort:Ljava/lang/String;
    .end local v7           #apnProxy:Ljava/lang/String;
    :goto_0
    return-object v3

    .line 235
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method protected getPostMethod()Lorg/apache/http/client/methods/HttpPost;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 309
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request url is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 311
    .local v0, post:Lorg/apache/http/client/methods/HttpPost;
    iget-boolean v1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->needAuth:Z

    if-eqz v1, :cond_0

    .line 312
    invoke-direct {p0, v0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->addCookie(Lorg/apache/http/client/methods/HttpPost;)V

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->needGzip()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_1
    const-string v1, "Accept-Charset"

    const-string v2, "GBK,utf-8;q=0.7,*;q=0.3"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    return-object v0
.end method

.method protected getResponseAsInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 4
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 338
    .local v1, is:Ljava/io/InputStream;
    const-string v3, "Content-Encoding"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 339
    .local v0, contentEncoding:Lorg/apache/http/Header;
    invoke-virtual {p0, v0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->isGzip(Lorg/apache/http/Header;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 340
    const-string v3, "Use gzip!"

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 341
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .end local v1           #is:Ljava/io/InputStream;
    .local v2, is:Ljava/io/InputStream;
    move-object v1, v2

    .line 343
    .end local v2           #is:Ljava/io/InputStream;
    .restart local v1       #is:Ljava/io/InputStream;
    :cond_0
    return-object v1
.end method

.method protected handleResponse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 4
    .parameter "is"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 286
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-static {p1}, Lcom/youdao/note/utils/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 287
    .local v0, bytes:[B
    new-instance v1, Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 288
    .local v1, responseStr:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got response "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    invoke-virtual {p0, v1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->handleResponse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method protected handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "response"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 352
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2
    .parameter "response"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 275
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getResponseAsInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    .line 276
    .local v0, is:Ljava/io/InputStream;
    invoke-virtual {p0, v0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->handleResponse(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic innerRun([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->innerRun([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs innerRun([Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v2, 0x0

    .line 140
    const/4 v1, 0x0

    .line 142
    .local v1, response:Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->isNetworkAvailable()Z

    move-result v3

    if-nez v3, :cond_2

    .line 143
    new-instance v2, Lcom/youdao/note/exceptions/NetworkNotAvaliableException;

    const-string v3, "Network not avaliable"

    iget-object v4, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/youdao/note/exceptions/NetworkNotAvaliableException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_0

    .line 183
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 185
    :cond_0
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v3, :cond_1

    .line 186
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v3, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    :cond_1
    throw v2

    .line 145
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getPostMethod()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v3

    iput-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    .line 146
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_5

    .line 182
    if-eqz v1, :cond_3

    .line 183
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 185
    :cond_3
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v3, :cond_4

    .line 186
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v3, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    :cond_4
    :goto_0
    return-object v2

    .line 149
    :cond_5
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start to execute task "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mRequestUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 151
    const-string v3, "Task has been cancelled."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 182
    if-eqz v1, :cond_6

    .line 183
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 185
    :cond_6
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v3, :cond_4

    .line 186
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v3, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    goto :goto_0

    .line 154
    :cond_7
    :try_start_3
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {p0, v3}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->dump(Lorg/apache/http/client/methods/HttpPost;)V

    .line 155
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 157
    .local v0, networkInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    .line 162
    sget-object v3, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mClient:Lorg/apache/http/client/HttpClient;

    invoke-direct {p0, v3}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->setUpApn(Lorg/apache/http/client/HttpClient;)V

    .line 167
    :goto_1
    sget-object v3, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mClient:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-interface {v3, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 168
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    iput v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mStatusCode:I

    .line 169
    invoke-virtual {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 170
    const-string v3, "Task has been cancelled."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 182
    if-eqz v1, :cond_8

    .line 183
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 185
    :cond_8
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v3, :cond_4

    .line 186
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v3, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    goto/16 :goto_0

    .line 164
    :cond_9
    :try_start_4
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/youdao/note/YNoteApplication;->setIsCmwap(Z)V

    .line 165
    sget-object v3, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const-string v4, "http.route.default-proxy"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    goto :goto_1

    .line 173
    :cond_a
    const/16 v2, 0xc8

    iget v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mStatusCode:I

    if-gt v2, v3, :cond_c

    iget v2, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mStatusCode:I

    const/16 v3, 0x12c

    if-ge v2, v3, :cond_c

    .line 175
    invoke-direct {p0, v1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->checkSessionCookie(Lorg/apache/http/HttpResponse;)V

    .line 176
    invoke-virtual {p0, v1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 182
    if-eqz v1, :cond_b

    .line 183
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 185
    :cond_b
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v3, :cond_4

    .line 186
    iget-object v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v3, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    goto/16 :goto_0

    .line 178
    :cond_c
    :try_start_5
    new-instance v2, Lcom/youdao/note/exceptions/ServerException;

    iget v3, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mStatusCode:I

    invoke-virtual {p0, v1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getResponseAsInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/youdao/note/utils/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/youdao/note/exceptions/ServerException;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method protected isGzip(Lorg/apache/http/Header;)Z
    .locals 2
    .parameter "contentEncoding"

    .prologue
    .line 347
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gzip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected needGzip()Z
    .locals 1

    .prologue
    .line 322
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public setRequestManager(Lcom/youdao/note/task/RequestManager;)V
    .locals 0
    .parameter "manager"

    .prologue
    .line 122
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    iput-object p1, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mManager:Lcom/youdao/note/task/RequestManager;

    .line 123
    return-void
.end method

.method public stop(Z)Z
    .locals 1
    .parameter "mayInterruptIfRunning"

    .prologue
    .line 129
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    iget-object v0, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/youdao/note/task/AbstractAsyncRequestTask;->mPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 132
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public syncExecute()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, this:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    sget-object v1, Lcom/youdao/note/task/AbstractAsyncRequestTask;->EMPTY_STRINGS:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 114
    .local v0, ret:Ljava/lang/Object;,"TT;"
    invoke-virtual {p0, v0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->onPostExecute(Ljava/lang/Object;)V

    .line 115
    return-object v0
.end method
