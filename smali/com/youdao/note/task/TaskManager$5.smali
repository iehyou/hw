.class Lcom/youdao/note/task/TaskManager$5;
.super Lcom/youdao/note/task/network/DelFileTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->delNote(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$entryPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter

    .prologue
    .line 211
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$5;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p3, p0, Lcom/youdao/note/task/TaskManager$5;->val$entryPath:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/youdao/note/task/network/DelFileTask;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 220
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$5;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 221
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$5;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0xc

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 222
    return-void
.end method

.method public onSucceed(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 214
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$5;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v0}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$5;->val$entryPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Ljava/lang/String;)Z

    .line 215
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$5;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0xc

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 216
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 211
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$5;->onSucceed(Ljava/lang/Boolean;)V

    return-void
.end method
