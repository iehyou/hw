.class public abstract Lcom/youdao/note/task/local/AbsAddResourceTask;
.super Lcom/youdao/note/task/LocalTask;
.source "AbsAddResourceTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">",
        "Lcom/youdao/note/task/LocalTask",
        "<TT;TT;>;"
    }
.end annotation


# instance fields
.field protected datasource:Lcom/youdao/note/datasource/DataSource;

.field private mHandler:Landroid/os/Handler;

.field protected uriArray:[Landroid/net/Uri;

.field protected ynote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2
    .parameter "uri"

    .prologue
    .line 25
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/task/local/AbsAddResourceTask;-><init>([Landroid/net/Uri;)V

    .line 26
    return-void
.end method

.method public constructor <init>([Landroid/net/Uri;)V
    .locals 1
    .parameter "uriArray"

    .prologue
    .line 29
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    invoke-direct {p0}, Lcom/youdao/note/task/LocalTask;-><init>()V

    .line 20
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->ynote:Lcom/youdao/note/YNoteApplication;

    .line 21
    iget-object v0, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->ynote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->datasource:Lcom/youdao/note/datasource/DataSource;

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->mHandler:Landroid/os/Handler;

    .line 30
    iput-object p1, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->uriArray:[Landroid/net/Uri;

    .line 31
    return-void
.end method


# virtual methods
.method protected abstract doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected onExecute()Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    const/4 v6, 0x0

    .line 37
    .local v6, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;,"TT;"
    iget-object v0, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->uriArray:[Landroid/net/Uri;

    .local v0, arr$:[Landroid/net/Uri;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v7, v0, v4

    .line 38
    .local v7, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/youdao/note/task/local/AbsAddResourceTask;->isCancelled()Z

    move-result v8

    if-nez v8, :cond_0

    .line 40
    invoke-static {v7}, Lcom/youdao/note/utils/FileUtils;->getFileSize(Landroid/net/Uri;)J

    move-result-wide v2

    .line 41
    .local v2, fileSize:J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  max size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->ynote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v9}, Lcom/youdao/note/YNoteApplication;->getMaxResourceSize()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v8, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->ynote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v8}, Lcom/youdao/note/YNoteApplication;->getMaxResourceSize()J

    move-result-wide v8

    cmp-long v8, v2, v8

    if-lez v8, :cond_1

    .line 43
    invoke-static {v7}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, fileName:Ljava/lang/String;
    iget-object v8, p0, Lcom/youdao/note/task/local/AbsAddResourceTask;->mHandler:Landroid/os/Handler;

    new-instance v9, Lcom/youdao/note/task/local/AbsAddResourceTask$1;

    invoke-direct {v9, p0, v1, v2, v3}, Lcom/youdao/note/task/local/AbsAddResourceTask$1;-><init>(Lcom/youdao/note/task/local/AbsAddResourceTask;Ljava/lang/String;J)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 37
    .end local v1           #fileName:Ljava/lang/String;
    .end local v2           #fileSize:J
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 52
    .restart local v2       #fileSize:J
    :cond_1
    invoke-virtual {p0, v7}, Lcom/youdao/note/task/local/AbsAddResourceTask;->doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v6

    .line 53
    const/4 v8, 0x1

    new-array v8, v8, [Lcom/youdao/note/data/resource/BaseResourceMeta;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-virtual {p0, v8}, Lcom/youdao/note/task/local/AbsAddResourceTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1

    .line 58
    .end local v2           #fileSize:J
    .end local v7           #uri:Landroid/net/Uri;
    :cond_2
    return-object v6
.end method

.method protected bridge synthetic onExecute()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 18
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    invoke-virtual {p0}, Lcom/youdao/note/task/local/AbsAddResourceTask;->onExecute()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected abstract onFinishAll(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method protected abstract onFinishOne(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method protected varargs onProgressUpdate([Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    .local p1, values:[Lcom/youdao/note/data/resource/BaseResourceMeta;,"[TT;"
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/task/local/AbsAddResourceTask;->onFinishOne(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 68
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 18
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    check-cast p1, [Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->onProgressUpdate([Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    return-void
.end method

.method protected abstract onResouceTooLarge(Ljava/lang/String;J)V
.end method

.method protected onSucceed(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    .local p1, result:Lcom/youdao/note/data/resource/BaseResourceMeta;,"TT;"
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->onFinishAll(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 73
    const-string v0, "succeed called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 18
    .local p0, this:Lcom/youdao/note/task/local/AbsAddResourceTask;,"Lcom/youdao/note/task/local/AbsAddResourceTask<TT;>;"
    check-cast p1, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/local/AbsAddResourceTask;->onSucceed(Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    return-void
.end method
