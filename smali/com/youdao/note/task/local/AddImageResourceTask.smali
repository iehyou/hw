.class public abstract Lcom/youdao/note/task/local/AddImageResourceTask;
.super Lcom/youdao/note/task/local/AbsAddResourceTask;
.source "AddImageResourceTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/local/AbsAddResourceTask",
        "<",
        "Lcom/youdao/note/data/resource/ImageResourceMeta;",
        ">;"
    }
.end annotation


# instance fields
.field private meta:Lcom/youdao/note/data/resource/ImageResourceMeta;

.field private picFrom:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;I)V
    .locals 2
    .parameter "uri"
    .parameter "picFrom"

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/youdao/note/task/local/AddImageResourceTask;-><init>([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 25
    return-void
.end method

.method public constructor <init>([Landroid/net/Uri;)V
    .locals 2
    .parameter "uriArray"

    .prologue
    .line 28
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/youdao/note/task/local/AddImageResourceTask;-><init>([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V

    .line 29
    return-void
.end method

.method public constructor <init>([Landroid/net/Uri;ILcom/youdao/note/data/resource/ImageResourceMeta;)V
    .locals 0
    .parameter "uriArray"
    .parameter "picFrom"
    .parameter "meta"

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/youdao/note/task/local/AbsAddResourceTask;-><init>([Landroid/net/Uri;)V

    .line 33
    iput-object p3, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->meta:Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 34
    iput p2, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->picFrom:I

    .line 35
    return-void
.end method


# virtual methods
.method protected bridge synthetic doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/local/AddImageResourceTask;->doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/ImageResourceMeta;

    move-result-object v0

    return-object v0
.end method

.method protected doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/ImageResourceMeta;
    .locals 7
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 39
    const/4 v3, 0x0

    .line 40
    .local v3, resource:Lcom/youdao/note/data/resource/ImageResource;
    iget-object v4, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->meta:Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-nez v4, :cond_1

    .line 41
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, fileName:Ljava/lang/String;
    invoke-static {v0}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyImageResource(Ljava/lang/String;)Lcom/youdao/note/data/resource/ImageResource;

    move-result-object v3

    .line 46
    .end local v0           #fileName:Ljava/lang/String;
    :goto_0
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->getBitmapFromUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 47
    .local v1, image:Landroid/graphics/Bitmap;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-static {v1, v4, v5}, Lcom/youdao/note/utils/ImageUtils;->bitmap2bytes(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/youdao/note/data/resource/ImageResource;->setContentBytes([B)V

    .line 48
    if-eqz v1, :cond_2

    .line 49
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/ImageResourceMeta;

    iget v5, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->picFrom:I

    invoke-virtual {v4, v5}, Lcom/youdao/note/data/resource/ImageResourceMeta;->setPicFrom(I)V

    .line 50
    iget-object v4, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->ynote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getImageQuality()I

    move-result v2

    .line 51
    .local v2, quarity:I
    invoke-static {v3, v1, v2, v6}, Lcom/youdao/note/utils/ImageUtils;->persistImage(Lcom/youdao/note/data/resource/AbstractImageResource;Landroid/graphics/Bitmap;IZ)V

    .line 53
    iget v4, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->picFrom:I

    if-ne v4, v6, :cond_0

    .line 54
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-virtual {v4, p1}, Lcom/youdao/note/data/resource/ImageResourceMeta;->copyTempPicture(Landroid/net/Uri;)V

    .line 56
    :cond_0
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/ImageResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/ImageResourceMeta;

    .line 59
    .end local v2           #quarity:I
    :goto_1
    return-object v4

    .line 44
    .end local v1           #image:Landroid/graphics/Bitmap;
    :cond_1
    iget-object v4, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->datasource:Lcom/youdao/note/datasource/DataSource;

    iget-object v5, p0, Lcom/youdao/note/task/local/AddImageResourceTask;->meta:Lcom/youdao/note/data/resource/ImageResourceMeta;

    invoke-virtual {v4, v5}, Lcom/youdao/note/datasource/DataSource;->getImageResource(Lcom/youdao/note/data/resource/ImageResourceMeta;)Lcom/youdao/note/data/resource/ImageResource;

    move-result-object v3

    goto :goto_0

    .line 58
    .restart local v1       #image:Landroid/graphics/Bitmap;
    :cond_2
    const-string v4, "Got null for image"

    invoke-static {p0, v4}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    const/4 v4, 0x0

    goto :goto_1
.end method
