.class public abstract Lcom/youdao/note/task/local/AddOtherResourceTask;
.super Lcom/youdao/note/task/local/AbsAddResourceTask;
.source "AddOtherResourceTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/local/AbsAddResourceTask",
        "<",
        "Lcom/youdao/note/data/resource/BaseResourceMeta;",
        ">;"
    }
.end annotation


# instance fields
.field private type:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;I)V
    .locals 0
    .parameter "uri"
    .parameter "resourceType"

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/youdao/note/task/local/AbsAddResourceTask;-><init>(Landroid/net/Uri;)V

    .line 25
    iput p2, p0, Lcom/youdao/note/task/local/AddOtherResourceTask;->type:I

    .line 26
    return-void
.end method


# virtual methods
.method protected doAddResource(Landroid/net/Uri;)Lcom/youdao/note/data/resource/BaseResourceMeta;
    .locals 6
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    iget v4, p0, Lcom/youdao/note/task/local/AddOtherResourceTask;->type:I

    invoke-static {v4}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyResourceMeta(I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v2

    .line 31
    .local v2, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    instance-of v4, v2, Lcom/youdao/note/data/resource/ImageResourceMeta;

    if-eqz v4, :cond_0

    .line 32
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "You should use AddImageResourceTask"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 34
    :cond_0
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->getFileNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, fileName:Ljava/lang/String;
    invoke-virtual {v2, v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setFileName(Ljava/lang/String;)V

    .line 36
    iget-object v4, p0, Lcom/youdao/note/task/local/AddOtherResourceTask;->datasource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v4, v2}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v3

    .line 37
    .local v3, path:Ljava/lang/String;
    invoke-static {p1, v3}, Lcom/youdao/note/utils/FileUtils;->copyFile(Landroid/net/Uri;Ljava/lang/String;)V

    .line 38
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setLength(J)V

    .line 40
    return-object v2
.end method
