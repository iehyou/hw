.class Lcom/youdao/note/task/TaskManager$8;
.super Lcom/youdao/note/task/network/GetResourceTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullThumbnail(Lcom/youdao/note/data/Thumbnail;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$thumbnail:Lcom/youdao/note/data/Thumbnail;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;IILcom/youdao/note/data/Thumbnail;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter

    .prologue
    .line 302
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$8;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p6, p0, Lcom/youdao/note/task/TaskManager$8;->val$thumbnail:Lcom/youdao/note/data/Thumbnail;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/youdao/note/task/network/GetResourceTask;-><init>(Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;II)V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 310
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$8;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x5

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 311
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$8;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 312
    return-void
.end method

.method public onSucceed(Ljava/io/File;)V
    .locals 4
    .parameter "file"

    .prologue
    .line 305
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$8;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$8;->val$thumbnail:Lcom/youdao/note/data/Thumbnail;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 306
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 302
    check-cast p1, Ljava/io/File;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$8;->onSucceed(Ljava/io/File;)V

    return-void
.end method
