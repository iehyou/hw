.class Lcom/youdao/note/task/TaskManager$11;
.super Lcom/youdao/note/task/network/GetUserMetaTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullNotes(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$async:Z


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 379
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-boolean p2, p0, Lcom/youdao/note/task/TaskManager$11;->val$async:Z

    invoke-direct {p0}, Lcom/youdao/note/task/network/GetUserMetaTask;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 416
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x5

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 417
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 418
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/UserMeta;)V
    .locals 7
    .parameter "userMeta"

    .prologue
    .line 382
    iget-object v4, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->yNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v4}, Lcom/youdao/note/task/TaskManager;->access$300(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v3

    .line 383
    .local v3, userName:Ljava/lang/String;
    iget-object v4, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v4}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v4

    invoke-virtual {v4, v3, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 384
    iget-object v4, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v4}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/datasource/DataSource;->getRootMeta()Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    .line 385
    .local v2, localRoot:Lcom/youdao/note/data/NoteBook;
    iget-object v4, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "local root version is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 387
    if-nez v2, :cond_1

    const/4 v0, -0x1

    .line 389
    .local v0, curVersion:I
    :goto_0
    new-instance v1, Lcom/youdao/note/task/TaskManager$11$1;

    const-string v4, "/"

    invoke-direct {v1, p0, v4, v0, p1}, Lcom/youdao/note/task/TaskManager$11$1;-><init>(Lcom/youdao/note/task/TaskManager$11;Ljava/lang/String;ILcom/youdao/note/data/UserMeta;)V

    .line 410
    .local v1, listFileTask:Lcom/youdao/note/task/network/GetFileMetaTask;
    iget-object v4, p0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    iget-boolean v5, p0, Lcom/youdao/note/task/TaskManager$11;->val$async:Z

    #calls: Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z
    invoke-static {v4, v5, v1}, Lcom/youdao/note/task/TaskManager;->access$400(Lcom/youdao/note/task/TaskManager;ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 412
    .end local v0           #curVersion:I
    .end local v1           #listFileTask:Lcom/youdao/note/task/network/GetFileMetaTask;
    .end local v2           #localRoot:Lcom/youdao/note/data/NoteBook;
    :cond_0
    return-void

    .line 387
    .restart local v2       #localRoot:Lcom/youdao/note/data/NoteBook;
    :cond_1
    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 379
    check-cast p1, Lcom/youdao/note/data/UserMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$11;->onSucceed(Lcom/youdao/note/data/UserMeta;)V

    return-void
.end method
