.class Lcom/youdao/note/task/TaskManager$2;
.super Lcom/youdao/note/task/network/CheckUpdateTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->checkUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;)V
    .locals 0
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$2;->this$0:Lcom/youdao/note/task/TaskManager;

    invoke-direct {p0}, Lcom/youdao/note/task/network/CheckUpdateTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 128
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$2;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x15

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 130
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/UpdateCheckResult;)V
    .locals 3
    .parameter "result"

    .prologue
    .line 123
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$2;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x15

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 124
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 119
    check-cast p1, Lcom/youdao/note/data/UpdateCheckResult;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$2;->onSucceed(Lcom/youdao/note/data/UpdateCheckResult;)V

    return-void
.end method
