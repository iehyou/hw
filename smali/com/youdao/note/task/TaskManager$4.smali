.class Lcom/youdao/note/task/TaskManager$4;
.super Lcom/youdao/note/task/network/MetaUpdateTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->updateMeta(Lcom/youdao/note/data/NoteBook;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$noteBook:Lcom/youdao/note/data/NoteBook;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter

    .prologue
    .line 185
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p3, p0, Lcom/youdao/note/task/TaskManager$4;->val$noteBook:Lcom/youdao/note/data/NoteBook;

    invoke-direct {p0, p2}, Lcom/youdao/note/task/network/MetaUpdateTask;-><init>(Lcom/youdao/note/data/NoteBook;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    const/16 v3, 0x10

    .line 196
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 197
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->val$noteBook:Lcom/youdao/note/data/NoteBook;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/youdao/note/exceptions/ServerException;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v0}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$4;->val$noteBook:Lcom/youdao/note/data/NoteBook;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    .line 199
    invoke-virtual {p0}, Lcom/youdao/note/task/TaskManager$4;->clearError()V

    .line 200
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 204
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    new-instance v1, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v1, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0
.end method

.method public onSucceed(Ljava/lang/Integer;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 188
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->val$noteBook:Lcom/youdao/note/data/NoteBook;

    invoke-virtual {v0}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v0}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$4;->val$noteBook:Lcom/youdao/note/data/NoteBook;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$4;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 192
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 185
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$4;->onSucceed(Ljava/lang/Integer;)V

    return-void
.end method
