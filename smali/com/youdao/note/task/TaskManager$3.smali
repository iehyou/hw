.class Lcom/youdao/note/task/TaskManager$3;
.super Lcom/youdao/note/task/network/GetUserMetaTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullUserMeta(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;)V
    .locals 0
    .parameter

    .prologue
    .line 140
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$3;->this$0:Lcom/youdao/note/task/TaskManager;

    invoke-direct {p0}, Lcom/youdao/note/task/network/GetUserMetaTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 148
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$3;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 149
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$3;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0xd

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 150
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/UserMeta;)V
    .locals 3
    .parameter "userMeta"

    .prologue
    .line 143
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$3;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0xd

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 144
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 140
    check-cast p1, Lcom/youdao/note/data/UserMeta;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$3;->onSucceed(Lcom/youdao/note/data/UserMeta;)V

    return-void
.end method
