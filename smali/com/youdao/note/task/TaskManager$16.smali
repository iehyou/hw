.class Lcom/youdao/note/task/TaskManager$16;
.super Lcom/youdao/note/task/network/MailShareTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->mailShare(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 585
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$16;->this$0:Lcom/youdao/note/task/TaskManager;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/youdao/note/task/network/MailShareTask;-><init>(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 589
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$16;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x11

    new-instance v2, Lcom/youdao/note/data/LocalErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/LocalErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 590
    const-string v0, ""

    invoke-static {p0, v0, p1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 591
    invoke-super {p0, p1}, Lcom/youdao/note/task/network/MailShareTask;->onFailed(Ljava/lang/Exception;)V

    .line 592
    return-void
.end method

.method protected onSucceed(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "result"

    .prologue
    .line 596
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$16;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x11

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 597
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 585
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$16;->onSucceed(Ljava/lang/Boolean;)V

    return-void
.end method
