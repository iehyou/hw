.class public Lcom/youdao/note/task/NoteResolver;
.super Ljava/lang/Object;
.source "NoteResolver.java"


# static fields
.field private static instance:Lcom/youdao/note/task/NoteResolver;


# instance fields
.field private mDataSource:Lcom/youdao/note/datasource/DataSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/task/NoteResolver;->instance:Lcom/youdao/note/task/NoteResolver;

    return-void
.end method

.method private constructor <init>(Lcom/youdao/note/datasource/DataSource;)V
    .locals 1
    .parameter "dataSource"

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 49
    iput-object p1, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 50
    return-void
.end method

.method private checkConfilct(Lcom/youdao/note/data/NoteMeta;)V
    .locals 10
    .parameter "noteMeta"

    .prologue
    const/4 v9, 0x1

    .line 150
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v3

    .line 151
    .local v3, localNoteMeta:Lcom/youdao/note/data/NoteMeta;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Server note is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found local note is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 155
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getNoteContentVersion(Ljava/lang/String;)I

    move-result v0

    .line 157
    .local v0, contentVersion:I
    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 184
    .end local v0           #contentVersion:I
    :cond_0
    :goto_0
    return-void

    .line 160
    .restart local v0       #contentVersion:I
    :cond_1
    if-ltz v0, :cond_2

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v7

    if-ne v7, v0, :cond_3

    .line 161
    :cond_2
    invoke-virtual {p1, v9}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    goto :goto_0

    .line 165
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fount conflict note "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v3}, Lcom/youdao/note/datasource/DataSource;->reGenerateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 167
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 169
    .local v4, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setNoteId(Ljava/lang/String;)V

    .line 170
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v4}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v5

    .line 172
    .local v5, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, v5}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z

    goto :goto_1

    .line 176
    .end local v0           #contentVersion:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v4           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v5           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "not fount conflict note "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    :cond_5
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    .line 179
    .local v2, localNoteBook:Lcom/youdao/note/data/NoteBook;
    iget-object v7, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7}, Lcom/youdao/note/datasource/DataSource;->getUserMeta()Lcom/youdao/note/data/UserMeta;

    move-result-object v6

    .line 180
    .local v6, userMeta:Lcom/youdao/note/data/UserMeta;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 181
    invoke-virtual {v6}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/youdao/note/data/NoteMeta;->setNoteBook(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p1, v9}, Lcom/youdao/note/data/NoteMeta;->setDirty(Z)V

    goto/16 :goto_0
.end method

.method private createNoteBooks(Ljava/util/List;)Z
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, noteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteBook;

    .line 288
    .local v1, noteBookMeta:Lcom/youdao/note/data/NoteBook;
    invoke-direct {p0, v1}, Lcom/youdao/note/task/NoteResolver;->resolveNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 289
    const/4 v2, 0x0

    .line 292
    .end local v1           #noteBookMeta:Lcom/youdao/note/data/NoteBook;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private deleteNoteBooks(Ljava/util/List;)Z
    .locals 17
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, deleteNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/youdao/note/data/NoteBook;

    .line 218
    .local v12, noteBook:Lcom/youdao/note/data/NoteBook;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v12}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v5

    .line 220
    .local v5, localNoteBook:Lcom/youdao/note/data/NoteBook;
    if-eqz v5, :cond_3

    .line 221
    invoke-virtual {v5}, Lcom/youdao/note/data/NoteBook;->isDirty()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 222
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNotebookId()Ljava/lang/String;

    move-result-object v10

    .line 223
    .local v10, newNoteBookId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v12}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v8

    .line 225
    .local v8, myNoteBook:Lcom/youdao/note/data/NoteBook;
    invoke-virtual {v8, v10}, Lcom/youdao/note/data/NoteBook;->setNoteBookId(Ljava/lang/String;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v8}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    .line 227
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v12}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/youdao/note/datasource/DataSource;->listNotesByNotebookAsList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/youdao/note/data/NoteMeta;

    .line 229
    .local v11, note:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v11}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v13

    .line 230
    .local v13, oldNoteId:Ljava/lang/String;
    invoke-virtual {v11}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 231
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v11}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v9

    .line 232
    .local v9, newNote:Lcom/youdao/note/data/Note;
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNoteId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Lcom/youdao/note/data/Note;->setNoteId(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v9, v10}, Lcom/youdao/note/data/Note;->setNoteBookId(Ljava/lang/String;)V

    .line 234
    invoke-virtual {v9, v10}, Lcom/youdao/note/data/Note;->setServerNoteBook(Ljava/lang/String;)V

    .line 236
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v9}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v11}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/youdao/note/datasource/DataSource;->getResourceMetasByNoteId(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 243
    .local v6, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    invoke-virtual {v9}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setNoteId(Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v6}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v14

    .line 245
    .local v14, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v14}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResource(Lcom/youdao/note/data/resource/AbstractResource;)Z

    goto :goto_3

    .line 237
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v6           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    .end local v14           #resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    :catch_0
    move-exception v1

    .line 239
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 248
    .end local v1           #e:Ljava/io/IOException;
    .end local v9           #newNote:Lcom/youdao/note/data/Note;
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v13}, Lcom/youdao/note/datasource/DataSource;->deleteNoteById(Ljava/lang/String;)Z

    goto :goto_1

    .line 252
    .end local v8           #myNoteBook:Lcom/youdao/note/data/NoteBook;
    .end local v10           #newNoteBookId:Ljava/lang/String;
    .end local v11           #note:Lcom/youdao/note/data/NoteMeta;
    .end local v13           #oldNoteId:Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v12}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/youdao/note/datasource/DataSource;->listNotesByNotebookAsList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/youdao/note/data/NoteMeta;

    .line 254
    .restart local v11       #note:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v11}, Lcom/youdao/note/data/NoteMeta;->needSync()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 255
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v11}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v7

    .line 256
    .local v7, myNote:Lcom/youdao/note/data/Note;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v11}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 257
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15}, Lcom/youdao/note/datasource/DataSource;->getDefaultNoteBookId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/youdao/note/data/Note;->setNoteBookId(Ljava/lang/String;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15}, Lcom/youdao/note/datasource/DataSource;->getDefaultNoteBookId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/youdao/note/data/Note;->setServerNoteBook(Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/youdao/note/utils/IdUtils;->genNoteId()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/youdao/note/data/Note;->setNoteId(Ljava/lang/String;)V

    .line 261
    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v7}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 262
    :catch_1
    move-exception v15

    goto :goto_4

    .line 266
    .end local v7           #myNote:Lcom/youdao/note/data/Note;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v11}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    goto :goto_4

    .line 271
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v11           #note:Lcom/youdao/note/data/NoteMeta;
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v15, v12}, Lcom/youdao/note/datasource/DataSource;->deleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    goto/16 :goto_0

    .line 273
    .end local v5           #localNoteBook:Lcom/youdao/note/data/NoteBook;
    .end local v12           #noteBook:Lcom/youdao/note/data/NoteBook;
    :cond_4
    const/4 v15, 0x1

    return v15
.end method

.method private deleteNotes(Ljava/util/List;)Z
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteMeta;

    .line 278
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-direct {p0, v1}, Lcom/youdao/note/task/NoteResolver;->checkConfilct(Lcom/youdao/note/data/NoteMeta;)V

    .line 279
    iget-object v2, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 280
    const/4 v2, 0x0

    .line 283
    .end local v1           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getInstance(Lcom/youdao/note/datasource/DataSource;)Lcom/youdao/note/task/NoteResolver;
    .locals 2
    .parameter "dataSource"

    .prologue
    .line 40
    const-class v1, Lcom/youdao/note/task/NoteResolver;

    monitor-enter v1

    .line 41
    :try_start_0
    sget-object v0, Lcom/youdao/note/task/NoteResolver;->instance:Lcom/youdao/note/task/NoteResolver;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/youdao/note/task/NoteResolver;->instance:Lcom/youdao/note/task/NoteResolver;

    iget-object v0, v0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    if-eq v0, p0, :cond_1

    .line 42
    :cond_0
    new-instance v0, Lcom/youdao/note/task/NoteResolver;

    invoke-direct {v0, p0}, Lcom/youdao/note/task/NoteResolver;-><init>(Lcom/youdao/note/datasource/DataSource;)V

    sput-object v0, Lcom/youdao/note/task/NoteResolver;->instance:Lcom/youdao/note/task/NoteResolver;

    .line 44
    :cond_1
    sget-object v0, Lcom/youdao/note/task/NoteResolver;->instance:Lcom/youdao/note/task/NoteResolver;

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private resolveNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 7
    .parameter "noteBook"

    .prologue
    const/4 v3, 0x0

    .line 188
    iget-object v4, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaByTitle(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v2

    .line 190
    .local v2, oriNoteBook:Lcom/youdao/note/data/NoteBook;
    if-nez v2, :cond_1

    move v1, v3

    .line 195
    .local v1, isSameNoteBook:Z
    :goto_0
    iget-object v4, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v4, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 213
    :cond_0
    :goto_1
    return v3

    .line 190
    .end local v1           #isSameNoteBook:Z
    :cond_1
    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 198
    .restart local v1       #isSameNoteBook:Z
    :cond_2
    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    .line 200
    :try_start_0
    iget-object v4, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/youdao/note/datasource/DataSource;->moveNotesBetweenNoteBook(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 203
    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v3

    if-ltz v3, :cond_4

    .line 204
    iget-object v3, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v3, v2}, Lcom/youdao/note/datasource/DataSource;->markDeleteNotebook(Lcom/youdao/note/data/NoteBook;)Z

    .line 213
    :cond_3
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 206
    :cond_4
    iget-object v3, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v3, v2}, Lcom/youdao/note/datasource/DataSource;->deleteNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    :try_end_0
    .catch Lcom/youdao/note/exceptions/NoteBookNotExistException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, e:Lcom/youdao/note/exceptions/NoteBookNotExistException;
    const-string v3, "Shouldn\'t got exception here."

    invoke-static {p0, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private updateDefaultNoteBook(Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "defaultNoteBook"

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateNoteBooks(Ljava/util/List;)Z
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteBook;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, noteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteBook;

    .line 142
    .local v1, noteBook:Lcom/youdao/note/data/NoteBook;
    invoke-direct {p0, v1}, Lcom/youdao/note/task/NoteResolver;->resolveNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 143
    const/4 v2, 0x0

    .line 146
    .end local v1           #noteBook:Lcom/youdao/note/data/NoteBook;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private updateNotes(Ljava/util/List;)Z
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/data/NoteMeta;

    .line 128
    .local v1, noteMeta:Lcom/youdao/note/data/NoteMeta;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update note "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0, v1}, Lcom/youdao/note/task/NoteResolver;->checkConfilct(Lcom/youdao/note/data/NoteMeta;)V

    .line 130
    iget-object v2, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2, v1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    const/4 v2, 0x0

    .line 134
    .end local v1           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized resolveNotes(Lcom/youdao/note/data/UserMeta;Lcom/youdao/note/data/PathCollection;)Z
    .locals 12
    .parameter "userMeta"
    .parameter "pathCollection"

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lcom/youdao/note/data/PathCollection;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    if-nez v10, :cond_0

    .line 54
    const/4 v10, 0x1

    .line 116
    :goto_0
    monitor-exit p0

    return v10

    .line 56
    :cond_0
    :try_start_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "start to resolve notes, total "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Lcom/youdao/note/data/PathCollection;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Lcom/youdao/note/data/PathCollection;->getRoot()Lcom/youdao/note/data/NoteBook;

    move-result-object v5

    .line 58
    .local v5, newRoot:Lcom/youdao/note/data/NoteBook;
    const/4 v1, 0x0

    .line 59
    .local v1, defaultNoteBook:Lcom/youdao/note/data/NoteBook;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v3, deletedNoteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v0, addedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v2, deletedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v9, otherNotes:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v8, otherNoteBoooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    invoke-virtual {p2}, Lcom/youdao/note/data/PathCollection;->getNoteMetas()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/youdao/note/data/NoteMeta;

    .line 66
    .local v7, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v7}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 67
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 53
    .end local v0           #addedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .end local v1           #defaultNoteBook:Lcom/youdao/note/data/NoteBook;
    .end local v2           #deletedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .end local v3           #deletedNoteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v5           #newRoot:Lcom/youdao/note/data/NoteBook;
    .end local v7           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    .end local v8           #otherNoteBoooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .end local v9           #otherNotes:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10

    .line 70
    .restart local v0       #addedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .restart local v1       #defaultNoteBook:Lcom/youdao/note/data/NoteBook;
    .restart local v2       #deletedNoteBooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .restart local v3       #deletedNoteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    .restart local v4       #i$:Ljava/util/Iterator;
    .restart local v5       #newRoot:Lcom/youdao/note/data/NoteBook;
    .restart local v7       #noteMeta:Lcom/youdao/note/data/NoteMeta;
    .restart local v8       #otherNoteBoooks:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    .restart local v9       #otherNotes:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    :cond_1
    :try_start_2
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 73
    .end local v7           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_2
    invoke-virtual {p2}, Lcom/youdao/note/data/PathCollection;->getNoteBooks()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/youdao/note/data/NoteBook;

    .line 74
    .local v6, noteBook:Lcom/youdao/note/data/NoteBook;
    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/youdao/note/data/UserMeta;->getDefaultNoteBook()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 75
    move-object v1, v6

    .line 76
    goto :goto_2

    .line 78
    :cond_3
    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->isDeleted()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 79
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 83
    :cond_4
    iget-object v10, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/youdao/note/datasource/DataSource;->exsitNoteBook(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 84
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 87
    :cond_5
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 90
    .end local v6           #noteBook:Lcom/youdao/note/data/NoteBook;
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "notes dispatched. "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " notebooks deleted."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " notebooks added."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " notes deleted."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " updated."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, v1}, Lcom/youdao/note/task/NoteResolver;->updateDefaultNoteBook(Lcom/youdao/note/data/NoteBook;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 96
    invoke-direct {p0, v0}, Lcom/youdao/note/task/NoteResolver;->createNoteBooks(Ljava/util/List;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 97
    const-string v10, "notebooks added."

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, v2}, Lcom/youdao/note/task/NoteResolver;->deleteNoteBooks(Ljava/util/List;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 99
    const-string v10, "notebooks deleted."

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0, v8}, Lcom/youdao/note/task/NoteResolver;->updateNoteBooks(Ljava/util/List;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 101
    invoke-direct {p0, v3}, Lcom/youdao/note/task/NoteResolver;->deleteNotes(Ljava/util/List;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 102
    const-string v10, "notemetas deleted."

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, v9}, Lcom/youdao/note/task/NoteResolver;->updateNotes(Ljava/util/List;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 104
    const-string v10, "notemetas updated."

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "try to update root note meta"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v10, p0, Lcom/youdao/note/task/NoteResolver;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v10, v5}, Lcom/youdao/note/datasource/DataSource;->updateRootNoteBookMeta(Lcom/youdao/note/data/NoteBook;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    goto/16 :goto_0

    .line 116
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_0
.end method
