.class Lcom/youdao/note/task/TaskManager$15;
.super Lcom/youdao/note/task/network/MoveNoteTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->moveNote(Lcom/youdao/note/data/NoteMeta;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$noteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Ljava/lang/String;Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 558
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$15;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p4, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/network/MoveNoteTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/Exception;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 568
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$15;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v1, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 569
    instance-of v1, p1, Lcom/youdao/note/exceptions/ServerException;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 570
    check-cast v0, Lcom/youdao/note/exceptions/ServerException;

    .line 571
    .local v0, se:Lcom/youdao/note/exceptions/ServerException;
    invoke-virtual {v0}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v1

    const/16 v2, 0xd1

    if-ne v1, v2, :cond_0

    .line 572
    invoke-virtual {p0}, Lcom/youdao/note/task/TaskManager$15;->clearError()V

    .line 573
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$15;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v1}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Lcom/youdao/note/data/NoteMeta;)Z

    .line 576
    .end local v0           #se:Lcom/youdao/note/exceptions/ServerException;
    :cond_0
    return-void
.end method

.method public onSucceed(Ljava/lang/Integer;)V
    .locals 2
    .parameter "version"

    .prologue
    .line 561
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteMeta;->setVersion(I)V

    .line 562
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/NoteMeta;->setServerNoteBook(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$15;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v0}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$15;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, v1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNoteMeta(Lcom/youdao/note/data/NoteMeta;)Z

    .line 564
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 558
    check-cast p1, Ljava/lang/Integer;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$15;->onSucceed(Ljava/lang/Integer;)V

    return-void
.end method
