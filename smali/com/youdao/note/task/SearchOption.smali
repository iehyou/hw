.class public Lcom/youdao/note/task/SearchOption;
.super Ljava/lang/Object;
.source "SearchOption.java"


# instance fields
.field private mKeyWord:Ljava/lang/String;

.field private mNoteBook:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/youdao/note/task/SearchOption;->mKeyWord:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/youdao/note/task/SearchOption;->mNoteBook:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getKeyWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/youdao/note/task/SearchOption;->mKeyWord:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteBook()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/youdao/note/task/SearchOption;->mNoteBook:Ljava/lang/String;

    return-object v0
.end method

.method public setKeyWord(Ljava/lang/String;)V
    .locals 0
    .parameter "mKeyWord"

    .prologue
    .line 21
    iput-object p1, p0, Lcom/youdao/note/task/SearchOption;->mKeyWord:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setNoteBook(Ljava/lang/String;)V
    .locals 0
    .parameter "mNoteBook"

    .prologue
    .line 29
    iput-object p1, p0, Lcom/youdao/note/task/SearchOption;->mNoteBook:Ljava/lang/String;

    .line 30
    return-void
.end method
