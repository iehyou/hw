.class Lcom/youdao/note/task/TaskManager$11$1;
.super Lcom/youdao/note/task/network/GetFileMetaTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager$11;->onSucceed(Lcom/youdao/note/data/UserMeta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field resolveResult:Z

.field final synthetic this$1:Lcom/youdao/note/task/TaskManager$11;

.field final synthetic val$userMeta:Lcom/youdao/note/data/UserMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager$11;Ljava/lang/String;ILcom/youdao/note/data/UserMeta;)V
    .locals 1
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 390
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$11$1;->this$1:Lcom/youdao/note/task/TaskManager$11;

    iput-object p4, p0, Lcom/youdao/note/task/TaskManager$11$1;->val$userMeta:Lcom/youdao/note/data/UserMeta;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/network/GetFileMetaTask;-><init>(Ljava/lang/String;I)V

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/task/TaskManager$11$1;->resolveResult:Z

    return-void
.end method


# virtual methods
.method protected handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 395
    invoke-super {p0, p1}, Lcom/youdao/note/task/network/GetFileMetaTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;

    move-result-object v0

    .line 396
    .local v0, pathCollection:Lcom/youdao/note/data/PathCollection;
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$11$1;->this$1:Lcom/youdao/note/task/TaskManager$11;

    iget-object v2, v2, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v2}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    invoke-static {v2}, Lcom/youdao/note/task/NoteResolver;->getInstance(Lcom/youdao/note/datasource/DataSource;)Lcom/youdao/note/task/NoteResolver;

    move-result-object v1

    .line 398
    .local v1, resolver:Lcom/youdao/note/task/NoteResolver;
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$11$1;->val$userMeta:Lcom/youdao/note/data/UserMeta;

    invoke-virtual {v1, v2, v0}, Lcom/youdao/note/task/NoteResolver;->resolveNotes(Lcom/youdao/note/data/UserMeta;Lcom/youdao/note/data/PathCollection;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/youdao/note/task/TaskManager$11$1;->resolveResult:Z

    .line 399
    return-object v0
.end method

.method protected bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 390
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$11$1;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;

    move-result-object v0

    return-object v0
.end method

.method public onSucceed(Lcom/youdao/note/data/PathCollection;)V
    .locals 4
    .parameter "listNoteMetas"

    .prologue
    .line 404
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$11$1;->this$1:Lcom/youdao/note/task/TaskManager$11;

    iget-boolean v0, v0, Lcom/youdao/note/task/TaskManager$11;->val$async:Z

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$11$1;->this$1:Lcom/youdao/note/task/TaskManager$11;

    iget-object v0, v0, Lcom/youdao/note/task/TaskManager$11;->this$0:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/youdao/note/task/TaskManager$11$1;->resolveResult:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 408
    :cond_0
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 390
    check-cast p1, Lcom/youdao/note/data/PathCollection;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$11$1;->onSucceed(Lcom/youdao/note/data/PathCollection;)V

    return-void
.end method
