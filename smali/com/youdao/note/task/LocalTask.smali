.class public abstract Lcom/youdao/note/task/LocalTask;
.super Lcom/youdao/note/task/AsyncTaskWithExecuteResult;
.source "LocalTask.java"

# interfaces
.implements Lcom/youdao/note/task/IManageableTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/youdao/note/task/AsyncTaskWithExecuteResult",
        "<",
        "Ljava/lang/Void;",
        "TProgress;TResult;>;",
        "Lcom/youdao/note/task/IManageableTask;"
    }
.end annotation


# instance fields
.field protected mManager:Lcom/youdao/note/task/RequestManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    invoke-direct {p0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic innerRun([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 11
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/LocalTask;->innerRun([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs innerRun([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 2
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    :try_start_0
    const-string v0, "innerRunn called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0}, Lcom/youdao/note/task/LocalTask;->onExecute()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/youdao/note/task/LocalTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/youdao/note/task/LocalTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v1, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    :cond_0
    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/youdao/note/task/LocalTask;->mManager:Lcom/youdao/note/task/RequestManager;

    if-eqz v1, :cond_1

    .line 40
    iget-object v1, p0, Lcom/youdao/note/task/LocalTask;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v1, p0}, Lcom/youdao/note/task/RequestManager;->removeTask(Lcom/youdao/note/task/IManageableTask;)V

    :cond_1
    throw v0
.end method

.method protected abstract onExecute()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public setRequestManager(Lcom/youdao/note/task/RequestManager;)V
    .locals 0
    .parameter "manager"

    .prologue
    .line 30
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    iput-object p1, p0, Lcom/youdao/note/task/LocalTask;->mManager:Lcom/youdao/note/task/RequestManager;

    .line 31
    return-void
.end method

.method public stop(Z)Z
    .locals 1
    .parameter "mayInterruptIfRunning"

    .prologue
    .line 25
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    invoke-super {p0, p1}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public syncExecute()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, this:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<TProgress;TResult;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v1}, Lcom/youdao/note/task/LocalTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 19
    .local v0, ret:Ljava/lang/Object;,"TResult;"
    invoke-virtual {p0, v0}, Lcom/youdao/note/task/LocalTask;->onPostExecute(Ljava/lang/Object;)V

    .line 20
    return-object v0
.end method
