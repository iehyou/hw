.class public Lcom/youdao/note/task/BaseApiRequestTask;
.super Lcom/youdao/note/task/AbstractAsyncRequestTask;
.source "BaseApiRequestTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/youdao/note/task/AbstractAsyncRequestTask",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mArgs:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .parameter "basePath"
    .parameter "method"
    .parameter "extraArgs"

    .prologue
    .line 48
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Z)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Z)V
    .locals 2
    .parameter "basePath"
    .parameter "method"
    .parameter "extraArgs"
    .parameter "needAuth"

    .prologue
    .line 42
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yws/mapi/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Lcom/youdao/note/utils/WebUtils;->constructRequestUrl(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Z)V

    .line 43
    iput-object p3, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .parameter "requestUrl"
    .parameter "needAuth"

    .prologue
    .line 37
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/task/AbstractAsyncRequestTask;-><init>(Ljava/lang/String;Z)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    .line 38
    return-void
.end method


# virtual methods
.method public getDataSource()Lcom/youdao/note/datasource/DataSource;
    .locals 1

    .prologue
    .line 75
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getPostMethod()Lorg/apache/http/client/methods/HttpPost;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    invoke-super {p0}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->getPostMethod()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v4

    .line 54
    .local v4, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v3, params:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    iget-object v5, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    if-eqz v5, :cond_1

    .line 56
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v5, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 57
    iget-object v5, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    add-int/lit8 v6, v1, 0x1

    aget-object v5, v5, v6

    if-eqz v5, :cond_0

    .line 58
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v6, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/youdao/note/task/BaseApiRequestTask;->mArgs:[Ljava/lang/Object;

    add-int/lit8 v8, v1, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 66
    .end local v1           #i:I
    :cond_1
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v2

    .line 67
    .local v2, logger:Lcom/youdao/note/LogRecorder;
    invoke-virtual {v2, v3}, Lcom/youdao/note/LogRecorder;->addGeneralParameter(Ljava/util/List;)V

    .line 68
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v5, "utf-8"

    invoke-direct {v0, v3, v5}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 69
    .local v0, entity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 70
    return-object v4
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFailed(Ljava/lang/Exception;)V
    .locals 0
    .parameter "e"

    .prologue
    .line 88
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    return-void
.end method

.method protected onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p0, this:Lcom/youdao/note/task/BaseApiRequestTask;,"Lcom/youdao/note/task/BaseApiRequestTask<TT;>;"
    .local p1, result:Ljava/lang/Object;,"TT;"
    return-void
.end method
