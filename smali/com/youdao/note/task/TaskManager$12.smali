.class Lcom/youdao/note/task/TaskManager$12;
.super Lcom/youdao/note/task/TaskManager$CheckNeedPullTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullNotesIfNecessary(Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$async:Z


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 432
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-boolean p2, p0, Lcom/youdao/note/task/TaskManager$12;->val$async:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/TaskManager$CheckNeedPullTask;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/task/TaskManager$1;)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 456
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 457
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check need pull failed. async is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/youdao/note/task/TaskManager$12;->val$async:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 458
    iget-boolean v0, p0, Lcom/youdao/note/task/TaskManager$12;->val$async:Z

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0x14

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 462
    :cond_0
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/PathCollection;)V
    .locals 4
    .parameter "listNoteMetas"

    .prologue
    .line 437
    invoke-virtual {p1}, Lcom/youdao/note/data/PathCollection;->getRoot()Lcom/youdao/note/data/NoteBook;

    move-result-object v1

    .line 438
    .local v1, root:Lcom/youdao/note/data/NoteBook;
    if-nez v1, :cond_0

    .line 452
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v2}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/datasource/DataSource;->getRootMeta()Lcom/youdao/note/data/NoteBook;

    move-result-object v0

    .line 442
    .local v0, localRoot:Lcom/youdao/note/data/NoteBook;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    invoke-virtual {v1}, Lcom/youdao/note/data/NoteBook;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->isServerVersionNewer(Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)Z
    invoke-static {v2, v1, v0}, Lcom/youdao/note/task/TaskManager;->access$600(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/youdao/note/task/TaskManager$12;->mNeedPull:Z

    .line 445
    iget-boolean v2, p0, Lcom/youdao/note/task/TaskManager$12;->mNeedPull:Z

    if-eqz v2, :cond_1

    .line 446
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    const-string v3, "Server version has newer version, start to sync."

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 448
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    iget-boolean v3, p0, Lcom/youdao/note/task/TaskManager$12;->val$async:Z

    invoke-virtual {v2, v3}, Lcom/youdao/note/task/TaskManager;->pullNotes(Z)V

    goto :goto_0

    .line 450
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$12;->this$0:Lcom/youdao/note/task/TaskManager;

    const-string v3, "Data base is the neweast version."

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 432
    check-cast p1, Lcom/youdao/note/data/PathCollection;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$12;->onSucceed(Lcom/youdao/note/data/PathCollection;)V

    return-void
.end method
