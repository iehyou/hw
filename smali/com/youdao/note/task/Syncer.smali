.class Lcom/youdao/note/task/Syncer;
.super Lcom/youdao/note/task/AsyncTaskWithExecuteResult;
.source "Syncer.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$DATA_TYPE;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/task/Syncer$SyncFinishListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/AsyncTaskWithExecuteResult",
        "<",
        "Ljava/lang/Void;",
        "Lcom/youdao/note/data/ProgressData;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/youdao/note/utils/Consts$DATA_TYPE;"
    }
.end annotation


# static fields
.field private static final FAKE_UPDATE_INTERVAL:J = 0x3e8L

.field private static final FAKE_UPDATE_V:I = 0x32

.field private static FIXED_REQUEST_SIZE:I = 0x0

.field private static final MESSAGE_FAKE_UPDATE:I = 0x1


# instance fields
.field private bRunInBackGround:Z

.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field private mHandler:Landroid/os/Handler;

.field private mStartTime:J

.field private mSyncFinishListener:Lcom/youdao/note/task/Syncer$SyncFinishListener;

.field private mTaskManager:Lcom/youdao/note/task/TaskManager;

.field private mYNote:Lcom/youdao/note/YNoteApplication;

.field private progress:Lcom/youdao/note/data/ProgressData;

.field private totalSize:I

.field private uploadedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/16 v0, 0x3e8

    sput v0, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/datasource/DataSource;Z)V
    .locals 3
    .parameter "taskManager"
    .parameter "dataSource"
    .parameter "runInBackground"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 90
    invoke-direct {p0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 44
    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 46
    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 51
    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mSyncFinishListener:Lcom/youdao/note/task/Syncer$SyncFinishListener;

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/youdao/note/task/Syncer;->mStartTime:J

    .line 74
    new-instance v0, Lcom/youdao/note/task/Syncer$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/task/Syncer$1;-><init>(Lcom/youdao/note/task/Syncer;)V

    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mHandler:Landroid/os/Handler;

    .line 98
    sget v0, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    mul-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    .line 100
    iput v2, p0, Lcom/youdao/note/task/Syncer;->uploadedSize:I

    .line 102
    new-instance v0, Lcom/youdao/note/data/ProgressData;

    invoke-direct {v0, v2}, Lcom/youdao/note/data/ProgressData;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->progress:Lcom/youdao/note/data/ProgressData;

    .line 91
    iput-object p1, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 92
    iput-object p2, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 93
    iput-boolean p3, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    .line 94
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/task/Syncer;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/task/Syncer;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private calculatePushSize(Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/NoteMeta;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p1, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    .local p2, dirtyResourcesList:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/NoteMeta;

    .line 262
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 263
    iget v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    sget v6, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    .line 270
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v5, v4}, Lcom/youdao/note/datasource/DataSource;->getDirtyResourcesOf(Lcom/youdao/note/data/NoteMeta;)Ljava/util/List;

    move-result-object v0

    .line 272
    .local v0, dirtyResources:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 274
    .local v3, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    iget v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    int-to-long v5, v5

    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getLength()J

    move-result-wide v7

    add-long/2addr v5, v7

    long-to-int v5, v5

    iput v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    goto :goto_1

    .line 265
    .end local v0           #dirtyResources:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_2
    iget v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    int-to-long v5, v5

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getLength()J

    move-result-wide v7

    add-long/2addr v5, v7

    long-to-int v5, v5

    iput v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    .line 266
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isMoved()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 267
    iget v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    sget v6, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    add-int/2addr v5, v6

    iput v5, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    goto :goto_0

    .line 277
    .end local v4           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_3
    return-void
.end method

.method private innerSync()Ljava/lang/Boolean;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    const-string v9, "Syncer is running"

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 128
    .local v3, msg:Landroid/os/Message;
    iget v9, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    int-to-double v9, v9

    const-wide v11, 0x3feeb851eb851eb8L

    mul-double/2addr v9, v11

    double-to-int v9, v9

    iput v9, v3, Landroid/os/Message;->arg1:I

    .line 129
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v9, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 133
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/youdao/note/task/TaskManager;->pullNotesIfNecessary(Z)Z

    .line 138
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDirtyNoteBookMetas()Ljava/util/List;

    move-result-object v7

    .line 139
    .local v7, notebookMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteBook;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/youdao/note/data/NoteBook;

    .line 140
    .local v6, notebookMeta:Lcom/youdao/note/data/NoteBook;
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v6, v10}, Lcom/youdao/note/task/TaskManager;->updateMeta(Lcom/youdao/note/data/NoteBook;Z)V

    goto :goto_0

    .line 146
    .end local v6           #notebookMeta:Lcom/youdao/note/data/NoteBook;
    :cond_0
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v9}, Lcom/youdao/note/datasource/DataSource;->getDirtyAndMovedNoteMetas()Ljava/util/List;

    move-result-object v5

    .line 147
    .local v5, noteMetas:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/NoteMeta;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    .local v0, dirtyResourcesList:Ljava/util/List;,"Ljava/util/List<Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;>;"
    invoke-direct {p0, v5, v0}, Lcom/youdao/note/task/Syncer;->calculatePushSize(Ljava/util/List;Ljava/util/List;)V

    .line 154
    sget v9, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    .line 156
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Total size is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Total dirty notes are "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    const/4 v8, 0x1

    .line 160
    .local v8, succeed:Z
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/NoteMeta;

    .line 161
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 162
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Try to delete note "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getEntryPath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/youdao/note/task/TaskManager;->delNote(Ljava/lang/String;Z)V

    .line 164
    sget v9, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    goto :goto_1

    .line 167
    .end local v4           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_2
    const/4 v1, 0x0

    .line 168
    .local v1, i:I
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/youdao/note/data/NoteMeta;

    .line 169
    .restart local v4       #noteMeta:Lcom/youdao/note/data/NoteMeta;
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isMoved()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 170
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Try to move note "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v4, v10}, Lcom/youdao/note/task/TaskManager;->moveNote(Lcom/youdao/note/data/NoteMeta;Z)Z

    move-result v9

    and-int/2addr v8, v9

    .line 172
    sget v9, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    .line 175
    :cond_3
    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isDeleted()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->isDirty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 176
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Try to push note "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    invoke-virtual {p0, v4, v9}, Lcom/youdao/note/task/Syncer;->pushNote(Lcom/youdao/note/data/NoteMeta;Ljava/util/List;)Z

    move-result v9

    and-int/2addr v8, v9

    .line 179
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 182
    .end local v4           #noteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_5
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/youdao/note/task/TaskManager;->pullNotesIfNecessary(Z)Z

    .line 183
    sget v9, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    .line 188
    iget-object v9, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/youdao/note/task/TaskManager;->pullNotesIfNecessary(Z)Z

    move-result v9

    if-nez v9, :cond_7

    const/4 v9, 0x1

    :goto_3
    and-int/2addr v8, v9

    .line 189
    sget v9, Lcom/youdao/note/task/Syncer;->FIXED_REQUEST_SIZE:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    .line 190
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "synce result is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    if-eqz v8, :cond_6

    .line 192
    invoke-direct {p0}, Lcom/youdao/note/task/Syncer;->updateLastSyncTime()V

    .line 194
    :cond_6
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    return-object v9

    .line 188
    :cond_7
    const/4 v9, 0x0

    goto :goto_3
.end method

.method private updateLastSyncTime()V
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1}, Lcom/youdao/note/datasource/DataSource;->getUserMeta()Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    .line 202
    .local v0, userMeta:Lcom/youdao/note/data/UserMeta;
    if-nez v0, :cond_0

    .line 203
    iget-object v1, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/youdao/note/task/TaskManager;->pullUserMeta(Z)V

    .line 205
    :cond_0
    if-nez v0, :cond_1

    .line 211
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/data/UserMeta;->setLastSynceTime(J)V

    .line 209
    iget-object v1, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getUserName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z

    goto :goto_0
.end method


# virtual methods
.method public getProgress()I
    .locals 4

    .prologue
    const/16 v1, 0x64

    .line 291
    iget v2, p0, Lcom/youdao/note/task/Syncer;->uploadedSize:I

    mul-int/lit8 v2, v2, 0x64

    iget v3, p0, Lcom/youdao/note/task/Syncer;->totalSize:I

    div-int v0, v2, v3

    .line 292
    .local v0, ret:I
    if-lt v0, v1, :cond_0

    .line 293
    iget-object v2, p0, Lcom/youdao/note/task/Syncer;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 295
    :cond_0
    if-ge v0, v1, :cond_1

    .end local v0           #ret:I
    :goto_0
    return v0

    .restart local v0       #ret:I
    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 348
    iget-wide v0, p0, Lcom/youdao/note/task/Syncer;->mStartTime:J

    return-wide v0
.end method

.method protected varargs innerRun([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .parameter "params"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    const-string v0, "User not login return false"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/youdao/note/exceptions/UnloginException;

    invoke-direct {v0}, Lcom/youdao/note/exceptions/UnloginException;-><init>()V

    throw v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->syncOnlyUnderWifi()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->isWifiAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "Not under wifi, sync cancled."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/task/Syncer;->innerSync()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic innerRun([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/Syncer;->innerRun([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isRunInBackGround()Z
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    return v0
.end method

.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    if-nez v0, :cond_0

    .line 334
    const-string v0, "Failed to sync"

    invoke-static {p0, v0, p1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x6

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 337
    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 308
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mSyncFinishListener:Lcom/youdao/note/task/Syncer$SyncFinishListener;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mSyncFinishListener:Lcom/youdao/note/task/Syncer$SyncFinishListener;

    invoke-interface {v0}, Lcom/youdao/note/task/Syncer$SyncFinishListener;->onSyncFinished()V

    .line 311
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sync result is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 312
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/task/Syncer;->setExecuteResult(Z)V

    .line 315
    :cond_1
    invoke-super {p0, p1}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->onPostExecute(Ljava/lang/Object;)V

    .line 316
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/Syncer;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/youdao/note/data/ProgressData;)V
    .locals 4
    .parameter "newProgress"

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    if-nez v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x7

    const/4 v2, 0x0

    aget-object v2, p1, v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 304
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 39
    check-cast p1, [Lcom/youdao/note/data/ProgressData;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/Syncer;->onProgressUpdate([Lcom/youdao/note/data/ProgressData;)V

    return-void
.end method

.method protected onSucceed(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "result"

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/youdao/note/task/Syncer;->bRunInBackGround:Z

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 326
    :cond_0
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 39
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/Syncer;->onSucceed(Ljava/lang/Boolean;)V

    return-void
.end method

.method public publishProgress(J)V
    .locals 3
    .parameter "newUploadedSize"

    .prologue
    .line 285
    iget v0, p0, Lcom/youdao/note/task/Syncer;->uploadedSize:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/youdao/note/task/Syncer;->uploadedSize:I

    .line 286
    iget-object v0, p0, Lcom/youdao/note/task/Syncer;->progress:Lcom/youdao/note/data/ProgressData;

    invoke-virtual {p0}, Lcom/youdao/note/task/Syncer;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/data/ProgressData;->setProgress(I)V

    .line 287
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/youdao/note/data/ProgressData;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/youdao/note/task/Syncer;->progress:Lcom/youdao/note/data/ProgressData;

    aput-object v2, v0, v1

    invoke-super {p0, v0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->publishProgress([Ljava/lang/Object;)V

    .line 288
    return-void
.end method

.method public pushNote(Lcom/youdao/note/data/NoteMeta;Ljava/util/List;)Z
    .locals 9
    .parameter "noteMeta"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/NoteMeta;",
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p2, dirtyResources:Ljava/util/List;,"Ljava/util/List<Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    const/4 v6, 0x0

    .line 225
    new-instance v5, Ljava/util/HashMap;

    const/4 v7, 0x4

    invoke-direct {v5, v7}, Ljava/util/HashMap;-><init>(I)V

    .line 227
    .local v5, resourcesMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/BaseResourceMeta;

    .line 229
    .local v3, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :try_start_0
    new-instance v2, Lcom/youdao/note/task/Syncer$2;

    invoke-direct {v2, p0}, Lcom/youdao/note/task/Syncer$2;-><init>(Lcom/youdao/note/task/Syncer;)V

    .line 235
    .local v2, listener:Lorg/apache/http/entity/mime/MultipartUploadListener;
    iget-object v7, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v7, v3, v2}, Lcom/youdao/note/task/TaskManager;->pushResource(Lcom/youdao/note/data/resource/BaseResourceMeta;Lorg/apache/http/entity/mime/MultipartUploadListener;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_1

    .line 250
    .end local v2           #listener:Lorg/apache/http/entity/mime/MultipartUploadListener;
    .end local v3           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_0
    :goto_1
    return v6

    .line 238
    .restart local v3       #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :catch_0
    move-exception v0

    .line 239
    .local v0, e:Ljava/io/FileNotFoundException;
    new-instance v6, Lcom/youdao/note/exceptions/ResourceMissingException;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/youdao/note/exceptions/ResourceMissingException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v6

    .line 241
    .end local v0           #e:Ljava/io/FileNotFoundException;
    .restart local v2       #listener:Lorg/apache/http/entity/mime/MultipartUploadListener;
    :cond_1
    invoke-virtual {v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 243
    .end local v2           #listener:Lorg/apache/http/entity/mime/MultipartUploadListener;
    .end local v3           #meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/youdao/note/task/Syncer;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v7, p1}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v4

    .line 246
    .local v4, note:Lcom/youdao/note/data/Note;
    iget-object v7, p0, Lcom/youdao/note/task/Syncer;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v7, v4, v5}, Lcom/youdao/note/task/TaskManager;->pushNote(Lcom/youdao/note/data/Note;Ljava/util/Map;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 249
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getLength()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/youdao/note/task/Syncer;->publishProgress(J)V

    .line 250
    const/4 v6, 0x1

    goto :goto_1
.end method

.method public setOnSyncFinishListener(Lcom/youdao/note/task/Syncer$SyncFinishListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 356
    iput-object p1, p0, Lcom/youdao/note/task/Syncer;->mSyncFinishListener:Lcom/youdao/note/task/Syncer$SyncFinishListener;

    .line 357
    return-void
.end method

.method protected syncExecute()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 365
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v1}, Lcom/youdao/note/task/Syncer;->doInBackground([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 366
    .local v0, ret:Ljava/lang/Boolean;
    invoke-virtual {p0, v0}, Lcom/youdao/note/task/Syncer;->onPostExecute(Ljava/lang/Boolean;)V

    .line 367
    return-object v0
.end method

.method protected bridge synthetic syncExecute()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/youdao/note/task/Syncer;->syncExecute()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
