.class public Lcom/youdao/note/task/SyncManager;
.super Ljava/lang/Object;
.source "SyncManager.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$DATA_TYPE;


# static fields
.field private static final DELAY:J = 0xea60L

.field private static final SYNCER_CHECK_INTERVAL:J = 0x1d4c0L

.field private static syncer:Lcom/youdao/note/task/Syncer;


# instance fields
.field private lock:Ljava/lang/Object;

.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field private mTaskManager:Lcom/youdao/note/task/TaskManager;

.field private timer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/datasource/DataSource;)V
    .locals 1
    .parameter "taskManager"
    .parameter "dataSource"

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 33
    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 35
    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    .line 46
    iput-object p1, p0, Lcom/youdao/note/task/SyncManager;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    .line 47
    iput-object p2, p0, Lcom/youdao/note/task/SyncManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 48
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getAutoSyncPeriod()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/task/SyncManager;->resetTime(I)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/task/SyncManager;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100()Lcom/youdao/note/task/Syncer;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/youdao/note/task/Syncer;)Lcom/youdao/note/task/Syncer;
    .locals 0
    .parameter "x0"

    .prologue
    .line 29
    sput-object p0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    return-object p0
.end method


# virtual methods
.method public getSyncProgress()I
    .locals 2

    .prologue
    .line 156
    iget-object v1, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    invoke-virtual {v0}, Lcom/youdao/note/task/Syncer;->getProgress()I

    move-result v0

    monitor-exit v1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isSyncing()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 126
    iget-object v1, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    sget-object v2, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    if-nez v2, :cond_0

    .line 128
    monitor-exit v1

    .line 130
    :goto_0
    return v0

    :cond_0
    sget-object v2, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    invoke-virtual {v2}, Lcom/youdao/note/task/Syncer;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resetTime(I)V
    .locals 6
    .parameter "minute"

    .prologue
    const-wide/32 v2, 0xea60

    .line 52
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    .line 56
    :cond_0
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 57
    new-instance v0, Ljava/util/Timer;

    const-string v1, "YNoteSyncerTimer"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    .line 58
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/youdao/note/task/SyncManager$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/task/SyncManager$1;-><init>(Lcom/youdao/note/task/SyncManager;)V

    int-to-long v4, p1

    mul-long/2addr v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 79
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/youdao/note/task/SyncManager$2;

    invoke-direct {v1, p0}, Lcom/youdao/note/task/SyncManager$2;-><init>(Lcom/youdao/note/task/SyncManager;)V

    const-wide/32 v4, 0x1d4c0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sync period reset to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minutes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    :cond_1
    return-void
.end method

.method public startSync(Z)Z
    .locals 5
    .parameter "runInBackGround"

    .prologue
    const/4 v4, 0x1

    .line 106
    iget-object v1, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 107
    :try_start_0
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    sget-object v2, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    invoke-virtual {v2}, Lcom/youdao/note/task/Syncer;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 108
    const-string v0, "syncer is running or pendding, return false."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "syncer status "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    invoke-virtual {v2}, Lcom/youdao/note/task/Syncer;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->cancel(Z)Z

    .line 112
    :cond_0
    const-string v0, "Start to sync."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    new-instance v0, Lcom/youdao/note/task/Syncer;

    iget-object v2, p0, Lcom/youdao/note/task/SyncManager;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    iget-object v3, p0, Lcom/youdao/note/task/SyncManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-direct {v0, v2, v3, p1}, Lcom/youdao/note/task/Syncer;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/datasource/DataSource;Z)V

    sput-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    .line 114
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    new-instance v2, Lcom/youdao/note/task/SyncManager$3;

    invoke-direct {v2, p0}, Lcom/youdao/note/task/SyncManager$3;-><init>(Lcom/youdao/note/task/SyncManager;)V

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->setOnSyncFinishListener(Lcom/youdao/note/task/Syncer$SyncFinishListener;)V

    .line 120
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    sget-object v2, Lcom/youdao/note/utils/EmptyInstance;->EMPTY_VOIDS:[Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    monitor-exit v1

    return v4

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopAutoSync()V
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->cancel(Z)Z

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 152
    :cond_1
    monitor-exit v1

    .line 153
    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopSync()V
    .locals 3

    .prologue
    .line 135
    iget-object v1, p0, Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 136
    :try_start_0
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->cancel(Z)Z

    .line 138
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/task/SyncManager;->syncer:Lcom/youdao/note/task/Syncer;

    .line 139
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager;->mTaskManager:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager;->stopAll()V

    .line 141
    :cond_0
    monitor-exit v1

    .line 142
    return-void

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
