.class Lcom/youdao/note/task/TaskManager$6;
.super Lcom/youdao/note/task/network/GetFileMetaTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pullNoteIfNeed(Lcom/youdao/note/data/NoteMeta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$noteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;ILcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 248
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p4, p0, Lcom/youdao/note/task/TaskManager$6;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/network/GetFileMetaTask;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected onFailed(Ljava/lang/Exception;)V
    .locals 4
    .parameter "e"

    .prologue
    .line 267
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 268
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v1, 0xe

    new-instance v2, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v2, p1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 269
    return-void
.end method

.method public onSucceed(Lcom/youdao/note/data/PathCollection;)V
    .locals 6
    .parameter "pathCollection"

    .prologue
    const/4 v5, 0x1

    .line 251
    invoke-virtual {p1}, Lcom/youdao/note/data/PathCollection;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 252
    invoke-virtual {p1}, Lcom/youdao/note/data/PathCollection;->getFirstNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v1

    .line 253
    .local v1, newNoteMeta:Lcom/youdao/note/data/NoteMeta;
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v2}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/task/TaskManager$6;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v3}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->getNoteContentVersion(Ljava/lang/String;)I

    move-result v0

    .line 254
    .local v0, contentVersion:I
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Local content version is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    invoke-virtual {v1}, Lcom/youdao/note/data/NoteMeta;->getVersion()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 256
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    const-string v3, "Start to pull note."

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    invoke-virtual {v2, v1, v5}, Lcom/youdao/note/task/TaskManager;->pullNote(Lcom/youdao/note/data/NoteMeta;Z)V

    .line 263
    .end local v0           #contentVersion:I
    .end local v1           #newNoteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_0
    :goto_0
    return-void

    .line 259
    .restart local v0       #contentVersion:I
    .restart local v1       #newNoteMeta:Lcom/youdao/note/data/NoteMeta;
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    .line 260
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$6;->this$0:Lcom/youdao/note/task/TaskManager;

    const-string v3, "Local content is the newest version."

    invoke-static {v2, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 248
    check-cast p1, Lcom/youdao/note/data/PathCollection;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$6;->onSucceed(Lcom/youdao/note/data/PathCollection;)V

    return-void
.end method
