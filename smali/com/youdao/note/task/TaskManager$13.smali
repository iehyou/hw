.class Lcom/youdao/note/task/TaskManager$13;
.super Lcom/youdao/note/task/network/PutResourceTask;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/TaskManager;->pushResource(Lcom/youdao/note/data/resource/BaseResourceMeta;Lorg/apache/http/entity/mime/MultipartUploadListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/TaskManager;

.field final synthetic val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/resource/AbstractResource;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartUploadListener;Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 0
    .parameter
    .parameter
    .parameter "x1"
    .parameter "x2"
    .parameter

    .prologue
    .line 488
    .local p2, x0:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager$13;->this$0:Lcom/youdao/note/task/TaskManager;

    iput-object p5, p0, Lcom/youdao/note/task/TaskManager$13;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-direct {p0, p2, p3, p4}, Lcom/youdao/note/task/network/PutResourceTask;-><init>(Lcom/youdao/note/data/resource/AbstractResource;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartUploadListener;)V

    return-void
.end method


# virtual methods
.method public onFailed(Ljava/lang/Exception;)V
    .locals 1
    .parameter "e"

    .prologue
    .line 505
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager$13;->this$0:Lcom/youdao/note/task/TaskManager;

    #calls: Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/youdao/note/task/TaskManager;->access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V

    .line 506
    return-void
.end method

.method public bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 488
    check-cast p1, Lorg/json/JSONObject;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/TaskManager$13;->onSucceed(Lorg/json/JSONObject;)V

    return-void
.end method

.method public onSucceed(Lorg/json/JSONObject;)V
    .locals 4
    .parameter "object"

    .prologue
    .line 492
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$13;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    const-string v2, "version"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setVersion(I)V

    .line 493
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$13;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    const-string v2, "sz"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setLength(J)V

    .line 494
    const-string v1, "src"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$13;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    const-string v2, "src"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/resource/BaseResourceMeta;->setSrc(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 500
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager$13;->this$0:Lcom/youdao/note/task/TaskManager;

    #getter for: Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-static {v1}, Lcom/youdao/note/task/TaskManager;->access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/task/TaskManager$13;->val$meta:Lcom/youdao/note/data/resource/BaseResourceMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateResourceMeta(Lcom/youdao/note/data/resource/BaseResourceMeta;)Z

    .line 501
    return-void

    .line 497
    :catch_0
    move-exception v0

    .line 498
    .local v0, e:Lorg/json/JSONException;
    const-string v1, "Server return data error"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
