.class public Lcom/youdao/note/task/network/GetUserMetaTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "GetUserMetaTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/UserMeta;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    const-string v0, "user"

    const-string v1, "get"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {p1}, Lcom/youdao/note/data/UserMeta;->fromJsonString(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    .line 28
    .local v0, userMeta:Lcom/youdao/note/data/UserMeta;
    invoke-virtual {p0}, Lcom/youdao/note/task/network/GetUserMetaTask;->getUserName()Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, userName:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/task/network/GetUserMetaTask;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateUserMeta(Ljava/lang/String;Lcom/youdao/note/data/UserMeta;)Z

    .line 30
    return-object v0
.end method

.method public bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/GetUserMetaTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/UserMeta;

    move-result-object v0

    return-object v0
.end method
