.class public Lcom/youdao/note/task/network/ListFileTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "ListFileTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/PathCollection;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    const-string v0, "/"

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/youdao/note/task/network/ListFileTask;-><init>(Ljava/lang/String;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .parameter "baseVersion"

    .prologue
    .line 29
    const-string v0, "/"

    invoke-direct {p0, v0, p1}, Lcom/youdao/note/task/network/ListFileTask;-><init>(Ljava/lang/String;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 14
    .parameter "path"
    .parameter "baseVersion"

    .prologue
    .line 33
    const/4 v2, -0x1

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const/16 v13, 0x3e8

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v13}, Lcom/youdao/note/task/network/ListFileTask;-><init>(Ljava/lang/String;IIJJJJII)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIJJJJII)V
    .locals 5
    .parameter "path"
    .parameter "baseVersion"
    .parameter "listMode"
    .parameter "ctBegin"
    .parameter "ctEnd"
    .parameter "mtBegin"
    .parameter "mtEnd"
    .parameter "begin"
    .parameter "length"

    .prologue
    .line 54
    const-string v0, "list"

    const-string v1, "get"

    const/16 v2, 0x12

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "p"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "bv"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "m"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "cb"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "ce"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "mb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "me"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "b"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "l"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;
    .locals 1
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-static {p1}, Lcom/youdao/note/data/PathCollection;->fromJsonString(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/ListFileTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/PathCollection;

    move-result-object v0

    return-object v0
.end method
