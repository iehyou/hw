.class public Lcom/youdao/note/task/network/DelFileTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "DelFileTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mPaths:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .parameter "pathList"

    .prologue
    const/4 v5, 0x1

    .line 29
    const-string v0, "filemeta"

    const-string v1, "rm"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ps"

    aput-object v4, v2, v3

    aput-object p1, v2, v5

    invoke-direct {p0, v0, v1, v2, v5}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Z)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/network/DelFileTask;->mPaths:[Ljava/lang/String;

    .line 32
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/task/network/DelFileTask;->mPaths:[Ljava/lang/String;

    .line 33
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1
    .parameter "paths"

    .prologue
    .line 24
    const-string v0, ","

    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/youdao/note/task/network/DelFileTask;-><init>(Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/youdao/note/task/network/DelFileTask;->mPaths:[Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method protected handleResponse(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 6
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v4, 0x1

    .line 37
    .local v4, succeed:Z
    iget-object v0, p0, Lcom/youdao/note/task/network/DelFileTask;->mPaths:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 38
    .local v1, entryPath:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/youdao/note/task/network/DelFileTask;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/youdao/note/datasource/DataSource;->deleteNote(Ljava/lang/String;)Z

    move-result v5

    and-int/2addr v4, v5

    .line 37
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 40
    .end local v1           #entryPath:Ljava/lang/String;
    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    return-object v5
.end method

.method protected bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/DelFileTask;->handleResponse(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
