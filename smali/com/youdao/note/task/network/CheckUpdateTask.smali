.class public Lcom/youdao/note/task/network/CheckUpdateTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "CheckUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/UpdateCheckResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG_DOWNLOADURL:Ljava/lang/String; = "downloadurl"

.field private static final TAG_FEATURES:Ljava/lang/String; = "release_note"

.field private static final TAG_VERSION:Ljava/lang/String; = "latestversion"

.field private static mYNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/task/network/CheckUpdateTask;->mYNote:Lcom/youdao/note/YNoteApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/youdao/note/task/network/CheckUpdateTask;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getUpdateUrl()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Z)V

    .line 46
    return-void
.end method

.method private getValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;
    .locals 3
    .parameter "nodeList"

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-interface {p1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 75
    .local v0, node:Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 76
    .local v1, tmpNodeList:Lorg/w3c/dom/NodeList;
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected handleResponse(Ljava/io/InputStream;)Lcom/youdao/note/data/UpdateCheckResult;
    .locals 13
    .parameter "input"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 50
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v4

    .line 51
    .local v4, factory:Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 52
    .local v0, builder:Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, p1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 53
    .local v1, document:Lorg/w3c/dom/Document;
    const-string v9, "latestversion"

    invoke-interface {v1, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 54
    .local v8, nodeList:Lorg/w3c/dom/NodeList;
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ne v9, v12, :cond_0

    .line 55
    invoke-direct {p0, v8}, Lcom/youdao/note/task/network/CheckUpdateTask;->getValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v7

    .line 56
    .local v7, newVersionName:Ljava/lang/String;
    const-string v9, "downloadurl"

    invoke-interface {v1, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 57
    .local v2, download:Lorg/w3c/dom/NodeList;
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ne v9, v12, :cond_0

    .line 58
    invoke-direct {p0, v2}, Lcom/youdao/note/task/network/CheckUpdateTask;->getValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, downloadUrl:Ljava/lang/String;
    sget-object v9, Lcom/youdao/note/task/network/CheckUpdateTask;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v9}, Lcom/youdao/note/YNoteApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    sget-object v10, Lcom/youdao/note/task/network/CheckUpdateTask;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v10}, Lcom/youdao/note/YNoteApplication;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    iget-object v6, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 61
    .local v6, myVersionName:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 63
    const-string v9, "release_note"

    invoke-interface {v1, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 65
    .local v5, features:Lorg/w3c/dom/NodeList;
    new-instance v9, Lcom/youdao/note/data/UpdateCheckResult;

    invoke-direct {p0, v5}, Lcom/youdao/note/task/network/CheckUpdateTask;->getValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v12, v3, v10}, Lcom/youdao/note/data/UpdateCheckResult;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 70
    .end local v2           #download:Lorg/w3c/dom/NodeList;
    .end local v3           #downloadUrl:Ljava/lang/String;
    .end local v5           #features:Lorg/w3c/dom/NodeList;
    .end local v6           #myVersionName:Ljava/lang/String;
    .end local v7           #newVersionName:Ljava/lang/String;
    :goto_0
    return-object v9

    :cond_0
    new-instance v9, Lcom/youdao/note/data/UpdateCheckResult;

    invoke-direct {v9}, Lcom/youdao/note/data/UpdateCheckResult;-><init>()V

    goto :goto_0
.end method

.method protected bridge synthetic handleResponse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/CheckUpdateTask;->handleResponse(Ljava/io/InputStream;)Lcom/youdao/note/data/UpdateCheckResult;

    move-result-object v0

    return-object v0
.end method

.method protected onFailed(Ljava/lang/Exception;)V
    .locals 0
    .parameter "e"

    .prologue
    .line 98
    return-void
.end method

.method protected onSucceed(Lcom/youdao/note/data/UpdateCheckResult;)V
    .locals 0
    .parameter "updateFound"

    .prologue
    .line 87
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 27
    check-cast p1, Lcom/youdao/note/data/UpdateCheckResult;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/CheckUpdateTask;->onSucceed(Lcom/youdao/note/data/UpdateCheckResult;)V

    return-void
.end method
