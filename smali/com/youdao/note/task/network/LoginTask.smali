.class public abstract Lcom/youdao/note/task/network/LoginTask;
.super Lcom/youdao/note/task/AbstractAsyncRequestTask;
.source "LoginTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/AbstractAsyncRequestTask",
        "<",
        "Lcom/youdao/note/data/LoginResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final PERSIST_COOKIE:Ljava/lang/String; = "&persist_cookie="


# instance fields
.field private mPasswdMd5:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter "userName"
    .parameter "pwdMd5"
    .parameter "userIp"

    .prologue
    const/4 v4, 0x0

    .line 37
    const-string v0, "http://reg.163.com/services/userlogin?username=%s&password=%s&type=1&userip=%s&product=note&savelogin=1&passtype=0&back_url=http://www.youdao.com"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/youdao/note/task/AbstractAsyncRequestTask;-><init>(Ljava/lang/String;Z)V

    .line 39
    iput-object p1, p0, Lcom/youdao/note/task/network/LoginTask;->mUserName:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/youdao/note/task/network/LoginTask;->mPasswdMd5:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected handleResponse(Lorg/apache/http/HttpResponse;)Lcom/youdao/note/data/LoginResult;
    .locals 12
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "response is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/LoginTask;->getResponseAsInputStream(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v9

    invoke-static {v9}, Lcom/youdao/note/utils/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 49
    .local v1, bytes:[B
    new-instance v7, Ljava/lang/String;

    const-string v9, "utf-8"

    invoke-direct {v7, v1, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 50
    .local v7, responseStr:Ljava/lang/String;
    const-string v9, "460"

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 51
    new-instance v9, Lcom/youdao/note/exceptions/LoginException;

    const/16 v10, 0x1cc

    invoke-direct {v9, v10}, Lcom/youdao/note/exceptions/LoginException;-><init>(I)V

    throw v9

    .line 53
    :cond_0
    const-string v9, "420"

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 54
    new-instance v9, Lcom/youdao/note/exceptions/LoginException;

    const/16 v10, 0x1a4

    invoke-direct {v9, v10}, Lcom/youdao/note/exceptions/LoginException;-><init>(I)V

    throw v9

    .line 56
    :cond_1
    const-string v9, "412"

    invoke-virtual {v7, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 57
    new-instance v9, Lcom/youdao/note/exceptions/LoginException;

    const/16 v10, 0x19c

    invoke-direct {v9, v10}, Lcom/youdao/note/exceptions/LoginException;-><init>(I)V

    throw v9

    .line 60
    :cond_2
    const-string v9, "\n"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    .local v4, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v5, v0, v2

    .line 61
    .local v5, line:Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "cookie="

    invoke-virtual {v5, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 62
    const-string v9, "&persist_cookie="

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 63
    .local v3, idx:I
    const-string v9, "&persist_cookie="

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, persistCookie:Ljava/lang/String;
    invoke-static {p1}, Lcom/youdao/note/utils/WebUtils;->getSessionCookie(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, sessionCookie:Ljava/lang/String;
    new-instance v9, Lcom/youdao/note/data/LoginResult;

    iget-object v10, p0, Lcom/youdao/note/task/network/LoginTask;->mUserName:Ljava/lang/String;

    iget-object v11, p0, Lcom/youdao/note/task/network/LoginTask;->mPasswdMd5:Ljava/lang/String;

    invoke-direct {v9, v10, v11, v6, v8}, Lcom/youdao/note/data/LoginResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v9

    .line 60
    .end local v3           #idx:I
    .end local v6           #persistCookie:Ljava/lang/String;
    .end local v8           #sessionCookie:Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v5           #line:Ljava/lang/String;
    :cond_4
    new-instance v9, Lcom/youdao/note/exceptions/LoginException;

    const/4 v10, -0x1

    invoke-direct {v9, v10}, Lcom/youdao/note/exceptions/LoginException;-><init>(I)V

    throw v9
.end method

.method protected bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/LoginTask;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/youdao/note/data/LoginResult;

    move-result-object v0

    return-object v0
.end method

.method protected onFailed(Ljava/lang/Exception;)V
    .locals 0
    .parameter "e"

    .prologue
    .line 76
    return-void
.end method
