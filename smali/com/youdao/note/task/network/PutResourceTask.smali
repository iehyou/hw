.class public Lcom/youdao/note/task/network/PutResourceTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "PutResourceTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field private listener:Lorg/apache/http/entity/mime/MultipartUploadListener;

.field private mFid:Ljava/lang/String;

.field private mResource:Lcom/youdao/note/data/resource/AbstractResource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/resource/AbstractResource;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartUploadListener;)V
    .locals 3
    .parameter
    .parameter "fid"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/resource/AbstractResource",
            "<+",
            "Lcom/youdao/note/data/resource/IResourceMeta;",
            ">;",
            "Ljava/lang/String;",
            "Lorg/apache/http/entity/mime/MultipartUploadListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    const-string v0, "file"

    const-string v1, "putres"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/youdao/note/task/network/PutResourceTask;->mResource:Lcom/youdao/note/data/resource/AbstractResource;

    .line 45
    iput-object p2, p0, Lcom/youdao/note/task/network/PutResourceTask;->mFid:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/youdao/note/task/network/PutResourceTask;->listener:Lorg/apache/http/entity/mime/MultipartUploadListener;

    .line 47
    return-void
.end method


# virtual methods
.method protected getPostMethod()Lorg/apache/http/client/methods/HttpPost;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-super {p0}, Lcom/youdao/note/task/BaseApiRequestTask;->getPostMethod()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v1

    .line 53
    .local v1, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v0, Lorg/apache/http/entity/mime/MultipartEntity;

    iget-object v2, p0, Lcom/youdao/note/task/network/PutResourceTask;->listener:Lorg/apache/http/entity/mime/MultipartUploadListener;

    invoke-direct {v0, v2}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/MultipartUploadListener;)V

    .line 54
    .local v0, multipartEntity:Lorg/apache/http/entity/mime/MultipartEntity;
    const-string v2, "fid"

    new-instance v3, Lorg/apache/http/entity/mime/content/StringBody;

    iget-object v4, p0, Lcom/youdao/note/task/network/PutResourceTask;->mFid:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 55
    const-string v2, "keyfrom"

    new-instance v3, Lorg/apache/http/entity/mime/content/StringBody;

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 57
    const-string v2, "bs"

    new-instance v3, Lorg/apache/http/entity/mime/content/FileBody;

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/youdao/note/task/network/PutResourceTask;->mResource:Lcom/youdao/note/data/resource/AbstractResource;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/AbstractResource;->getAbslutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/youdao/note/task/network/PutResourceTask;->mResource:Lcom/youdao/note/data/resource/AbstractResource;

    invoke-virtual {v5}, Lcom/youdao/note/data/resource/AbstractResource;->getFileName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "application/octet-stream"

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 61
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 62
    return-object v1
.end method

.method public bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/PutResourceTask;->handleResponse(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public handleResponse(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, jsonObject:Lorg/json/JSONObject;
    const-string v3, "url"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, url:Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/youdao/note/utils/YNoteUtils;->extractResource(Ljava/lang/String;I)Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v1

    .line 70
    .local v1, meta:Lcom/youdao/note/data/resource/BaseResourceMeta;
    if-eqz v1, :cond_0

    .line 71
    const-string v3, "version"

    invoke-virtual {v1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getVersion()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 73
    :cond_0
    return-object v0
.end method
