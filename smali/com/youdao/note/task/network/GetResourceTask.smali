.class public Lcom/youdao/note/task/network/GetResourceTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "GetResourceTask.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Ljava/io/File;",
        ">;",
        "Lcom/youdao/note/utils/Consts;"
    }
.end annotation


# static fields
.field private static final DEFAULT_CONTENT_LENGTH:J = 0x400L

.field private static final PROGRES_UPSATE_THREAD:I = 0x5

.field private static final UPDATE_TIME_THREAD:J = 0x3e8L


# instance fields
.field private mLastProgress:I

.field private mLastUpdateTime:J

.field private mMeta:Lcom/youdao/note/data/resource/IResourceMeta;

.field private mPath:Ljava/lang/String;

.field private outFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;II)V
    .locals 5
    .parameter "path"
    .parameter "meta"
    .parameter "w"
    .parameter "h"

    .prologue
    .line 48
    const-string v0, "file"

    const-string v1, "getres"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "fid"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p2}, Lcom/youdao/note/data/resource/IResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "v"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-interface {p2}, Lcom/youdao/note/data/resource/IResourceMeta;->getVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "w"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "h"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    const-wide/16 v0, -0x3e8

    iput-wide v0, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastUpdateTime:J

    .line 80
    const/4 v0, -0x5

    iput v0, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastProgress:I

    .line 52
    iput-object p1, p0, Lcom/youdao/note/task/network/GetResourceTask;->mPath:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/youdao/note/task/network/GetResourceTask;->mMeta:Lcom/youdao/note/data/resource/IResourceMeta;

    .line 54
    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/youdao/note/datasource/DataSource;->getTmpResourcePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    .line 55
    return-void
.end method


# virtual methods
.method protected getPostMethod()Lorg/apache/http/client/methods/HttpPost;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/youdao/note/exceptions/DownloadResourceException;
        }
    .end annotation

    .prologue
    .line 60
    :try_start_0
    invoke-super {p0}, Lcom/youdao/note/task/BaseApiRequestTask;->getPostMethod()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v1

    .line 61
    .local v1, post:Lorg/apache/http/client/methods/HttpPost;
    iget-object v2, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const-string v2, "Range"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    return-object v1

    .line 65
    .end local v1           #post:Lorg/apache/http/client/methods/HttpPost;
    :catch_0
    move-exception v0

    .line 66
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Lcom/youdao/note/exceptions/DownloadResourceException;

    iget-object v3, p0, Lcom/youdao/note/task/network/GetResourceTask;->mMeta:Lcom/youdao/note/data/resource/IResourceMeta;

    invoke-interface {v3}, Lcom/youdao/note/data/resource/IResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/youdao/note/exceptions/DownloadResourceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected handleResponse(Lorg/apache/http/HttpResponse;)Ljava/io/File;
    .locals 15
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/youdao/note/exceptions/DownloadResourceException;
        }
    .end annotation

    .prologue
    .line 84
    :try_start_0
    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 85
    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-static {v9}, Lcom/youdao/note/utils/FileUtils;->createNewFile(Ljava/io/File;)Z

    .line 87
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v5

    .line 88
    .local v5, length:J
    const-wide/16 v9, 0x0

    cmp-long v9, v5, v9

    if-gez v9, :cond_1

    .line 89
    const-wide/16 v5, 0x400

    .line 94
    :cond_1
    new-instance v8, Ljava/io/FileOutputStream;

    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    const/4 v10, 0x1

    invoke-direct {v8, v9, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .line 95
    .local v8, writer:Ljava/io/FileOutputStream;
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 97
    .local v4, is:Ljava/io/InputStream;
    const/16 v9, 0x80

    :try_start_1
    new-array v0, v9, [B

    .line 98
    .local v0, buffer:[B
    const/4 v1, 0x0

    .line 99
    .local v1, count:I
    :cond_2
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_6

    .line 100
    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 101
    invoke-virtual {p0}, Lcom/youdao/note/task/network/GetResourceTask;->isCancelled()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v9

    if-eqz v9, :cond_3

    .line 102
    const/4 v2, 0x0

    .line 115
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 116
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 121
    :goto_1
    return-object v2

    .line 104
    :cond_3
    :try_start_3
    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v11, 0x64

    mul-long/2addr v9, v11

    div-long/2addr v9, v5

    long-to-int v7, v9

    .line 105
    .local v7, nextProgress:I
    iget v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastProgress:I

    add-int/lit8 v9, v9, 0x5

    if-lt v7, v9, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastUpdateTime:J

    const-wide/16 v13, 0x3e8

    add-long/2addr v11, v13

    cmp-long v9, v9, v11

    if-gez v9, :cond_5

    :cond_4
    const/16 v9, 0x64

    if-lt v7, v9, :cond_2

    .line 109
    :cond_5
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Integer;

    const/4 v10, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/youdao/note/task/network/GetResourceTask;->publishProgress([Ljava/lang/Object;)V

    .line 110
    iput v7, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastProgress:I

    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->mLastUpdateTime:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 115
    .end local v0           #buffer:[B
    .end local v1           #count:I
    .end local v7           #nextProgress:I
    :catchall_0
    move-exception v9

    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 116
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v9
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 122
    .end local v4           #is:Ljava/io/InputStream;
    .end local v5           #length:J
    .end local v8           #writer:Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 123
    .local v3, e:Ljava/lang/Exception;
    new-instance v9, Lcom/youdao/note/exceptions/DownloadResourceException;

    iget-object v10, p0, Lcom/youdao/note/task/network/GetResourceTask;->mMeta:Lcom/youdao/note/data/resource/IResourceMeta;

    invoke-interface {v10}, Lcom/youdao/note/data/resource/IResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lcom/youdao/note/exceptions/DownloadResourceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    .line 115
    .end local v3           #e:Ljava/lang/Exception;
    .restart local v0       #buffer:[B
    .restart local v1       #count:I
    .restart local v4       #is:Ljava/io/InputStream;
    .restart local v5       #length:J
    .restart local v8       #writer:Ljava/io/FileOutputStream;
    :cond_6
    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 116
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 118
    new-instance v2, Ljava/io/File;

    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->mPath:Ljava/lang/String;

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v2, dstFile:Ljava/io/File;
    iget-object v9, p0, Lcom/youdao/note/task/network/GetResourceTask;->outFile:Ljava/io/File;

    invoke-virtual {v9, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1
.end method

.method protected bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/GetResourceTask;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected needGzip()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method
