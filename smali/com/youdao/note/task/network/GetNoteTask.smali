.class public Lcom/youdao/note/task/network/GetNoteTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "GetNoteTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/Note;",
        ">;"
    }
.end annotation


# instance fields
.field private mNoteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/NoteMeta;)V
    .locals 1
    .parameter "noteMeta"

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getEntryPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/youdao/note/task/network/GetNoteTask;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/youdao/note/task/network/GetNoteTask;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 28
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter "path"

    .prologue
    const/4 v1, 0x0

    .line 31
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/youdao/note/task/network/GetNoteTask;-><init>(Ljava/lang/String;III)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 6
    .parameter "path"
    .parameter "version"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v5, 0x1

    .line 35
    const-string v0, "file"

    const-string v1, "get"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "p"

    aput-object v4, v2, v3

    aput-object p1, v2, v5

    const/4 v3, 0x2

    const-string v4, "v"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "w"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "h"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2, v5}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Z)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/network/GetNoteTask;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    .line 41
    return-void
.end method


# virtual methods
.method protected handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/Note;
    .locals 4
    .parameter "noteContent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v1, Lcom/youdao/note/data/Note;

    iget-object v2, p0, Lcom/youdao/note/task/network/GetNoteTask;->mNoteMeta:Lcom/youdao/note/data/NoteMeta;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/youdao/note/data/Note;-><init>(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;)V

    .line 46
    .local v1, note:Lcom/youdao/note/data/Note;
    :try_start_0
    invoke-virtual {v1}, Lcom/youdao/note/data/Note;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/youdao/note/task/network/GetNoteTask;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/youdao/note/utils/HTMLUtils;->convertToLocalHtml(Ljava/lang/String;Lcom/youdao/note/datasource/DataSource;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/youdao/note/task/network/GetNoteTask;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/youdao/note/datasource/DataSource;->insertOrUpdateNote(Lcom/youdao/note/data/Note;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-object v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "Failed to convert html to local version."

    invoke-static {p0, v2, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 52
    invoke-virtual {v1, p1}, Lcom/youdao/note/data/Note;->setBody(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/GetNoteTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/Note;

    move-result-object v0

    return-object v0
.end method
