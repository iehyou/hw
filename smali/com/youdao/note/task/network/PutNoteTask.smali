.class public Lcom/youdao/note/task/network/PutNoteTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "PutNoteTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/youdao/note/data/Note;Ljava/util/Map;)V
    .locals 6
    .parameter "note"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/Note;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, dirtyResources:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getEntryPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getAuthor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/youdao/note/utils/HTMLUtils;->convertToServerHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/task/network/PutNoteTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter "path"
    .parameter "source"
    .parameter "author"
    .parameter "title"
    .parameter "html"

    .prologue
    .line 33
    const-string v0, "file"

    const-string v1, "putfile"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "src"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    const-string v4, "au"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    const/4 v3, 0x4

    const-string v4, "tl"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p4, v2, v3

    const/4 v3, 0x6

    const-string v4, "p"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aput-object p1, v2, v3

    const/16 v3, 0x8

    const-string v4, "bs"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    aput-object p5, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected handleResponse(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PutNoteTask "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, jsonObject:Lorg/json/JSONObject;
    const-string v1, "v"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/PutNoteTask;->handleResponse(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
