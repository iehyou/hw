.class public Lcom/youdao/note/task/network/GetTrashTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "GetTrashTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/ListNoteMetas;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(III)V
    .locals 5
    .parameter "listMode"
    .parameter "begin"
    .parameter "length"

    .prologue
    .line 22
    const-string v0, "trash"

    const-string v1, "get"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "m"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "b"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "l"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;
    .locals 2
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 29
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/youdao/note/data/ListNoteMetas;->fromJsonString(Ljava/lang/String;ZZ)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/GetTrashTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method
