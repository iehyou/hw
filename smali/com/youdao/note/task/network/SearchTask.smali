.class public Lcom/youdao/note/task/network/SearchTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "SearchTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Lcom/youdao/note/data/ListNoteMetas;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 15
    .parameter "keyWord"

    .prologue
    .line 22
    const-string v2, "/"

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const/16 v13, 0x3e8

    const/4 v14, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v14}, Lcom/youdao/note/task/network/SearchTask;-><init>(Ljava/lang/String;Ljava/lang/String;IJJJJIIZ)V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IJJJJIIZ)V
    .locals 5
    .parameter "keyWord"
    .parameter "noteBook"
    .parameter "listMode"
    .parameter "createTimeBegin"
    .parameter "createTimeEnd"
    .parameter "modifyTimeBegin"
    .parameter "modifyTimeEnd"
    .parameter "begin"
    .parameter "length"
    .parameter "thin"

    .prologue
    .line 30
    const-string v0, "search"

    const-string v1, "get"

    const/16 v2, 0x14

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "kw"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string v4, "nb"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    const-string v4, "m"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "cb"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "ce"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "mb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "me"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-static/range {p10 .. p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "b"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "l"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "th"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    invoke-static/range {p14 .. p14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;
    .locals 1
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 47
    invoke-static {p1, v0, v0}, Lcom/youdao/note/data/ListNoteMetas;->fromJsonString(Ljava/lang/String;ZZ)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/network/SearchTask;->handleResponse(Ljava/lang/String;)Lcom/youdao/note/data/ListNoteMetas;

    move-result-object v0

    return-object v0
.end method
