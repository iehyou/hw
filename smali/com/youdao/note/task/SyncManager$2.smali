.class Lcom/youdao/note/task/SyncManager$2;
.super Ljava/util/TimerTask;
.source "SyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/task/SyncManager;->resetTime(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/task/SyncManager;


# direct methods
.method constructor <init>(Lcom/youdao/note/task/SyncManager;)V
    .locals 0
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/youdao/note/task/SyncManager$2;->this$0:Lcom/youdao/note/task/SyncManager;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 82
    iget-object v0, p0, Lcom/youdao/note/task/SyncManager$2;->this$0:Lcom/youdao/note/task/SyncManager;

    #getter for: Lcom/youdao/note/task/SyncManager;->lock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/youdao/note/task/SyncManager;->access$000(Lcom/youdao/note/task/SyncManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 83
    :try_start_0
    invoke-static {}, Lcom/youdao/note/task/SyncManager;->access$100()Lcom/youdao/note/task/Syncer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/youdao/note/task/SyncManager;->access$100()Lcom/youdao/note/task/Syncer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/task/Syncer;->isRunInBackGround()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/youdao/note/task/SyncManager;->access$100()Lcom/youdao/note/task/Syncer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/task/Syncer;->getStartTime()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x1d4c0

    sub-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/youdao/note/task/SyncManager;->access$100()Lcom/youdao/note/task/Syncer;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/youdao/note/task/Syncer;->cancel(Z)Z

    .line 88
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/youdao/note/task/SyncManager;->access$102(Lcom/youdao/note/task/Syncer;)Lcom/youdao/note/task/Syncer;

    .line 89
    const-string v0, "calcel a background syncer task."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    :cond_0
    monitor-exit v1

    .line 92
    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
