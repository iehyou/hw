.class public Lcom/youdao/note/task/TaskManager;
.super Ljava/lang/Object;
.source "TaskManager.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$DATA_TYPE;
.implements Lcom/youdao/note/utils/Consts$APIS;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/task/TaskManager$CheckNeedPullTask;,
        Lcom/youdao/note/task/TaskManager$DataUpdateListener;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/youdao/note/task/TaskManager;


# instance fields
.field private downloadingResourceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/task/TaskManager$DataUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mDataSource:Lcom/youdao/note/datasource/DataSource;

.field private mManager:Lcom/youdao/note/task/RequestManager;

.field private yNote:Lcom/youdao/note/YNoteApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/youdao/note/task/TaskManager;->mInstance:Lcom/youdao/note/task/TaskManager;

    return-void
.end method

.method public constructor <init>(Lcom/youdao/note/datasource/DataSource;)V
    .locals 2
    .parameter "dataSource"

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    .line 76
    iput-object v1, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 78
    iput-object v1, p0, Lcom/youdao/note/task/TaskManager;->yNote:Lcom/youdao/note/YNoteApplication;

    .line 80
    iput-object v1, p0, Lcom/youdao/note/task/TaskManager;->mManager:Lcom/youdao/note/task/RequestManager;

    .line 333
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;

    .line 85
    iput-object p1, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    .line 86
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/task/TaskManager;->yNote:Lcom/youdao/note/YNoteApplication;

    .line 87
    new-instance v0, Lcom/youdao/note/task/RequestManager;

    invoke-direct {v0}, Lcom/youdao/note/task/RequestManager;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/task/TaskManager;->mManager:Lcom/youdao/note/task/RequestManager;

    .line 88
    sput-object p0, Lcom/youdao/note/task/TaskManager;->mInstance:Lcom/youdao/note/task/TaskManager;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/task/TaskManager;Ljava/lang/Exception;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/youdao/note/task/TaskManager;->checkError(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/datasource/DataSource;
    .locals 1
    .parameter "x0"

    .prologue
    .line 68
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/task/TaskManager;)Ljava/util/Map;
    .locals 1
    .parameter "x0"

    .prologue
    .line 68
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/youdao/note/task/TaskManager;)Lcom/youdao/note/YNoteApplication;
    .locals 1
    .parameter "x0"

    .prologue
    .line 68
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->yNote:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/task/TaskManager;ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/task/TaskManager;->isServerVersionNewer(Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)Z

    move-result v0

    return v0
.end method

.method private checkError(Ljava/lang/Exception;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 686
    instance-of v0, p1, Lcom/youdao/note/exceptions/ServerException;

    if-eqz v0, :cond_0

    .line 687
    check-cast p1, Lcom/youdao/note/exceptions/ServerException;

    .end local p1
    invoke-virtual {p1}, Lcom/youdao/note/exceptions/ServerException;->getErrorCode()I

    move-result v0

    const/16 v1, 0xcf

    if-ne v0, v1, :cond_0

    .line 688
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.youdao.note.action.ACCESS_DENIED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/youdao/note/YNoteApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 691
    :cond_0
    return-void
.end method

.method private checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V
    .locals 1
    .parameter "async"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Params:",
            "Ljava/lang/Object;",
            "Progress:",
            "Ljava/lang/Object;",
            "Result:",
            "Ljava/lang/Object;",
            ">(Z",
            "Lcom/youdao/note/task/AsyncTaskWithExecuteResult",
            "<TParams;TProgress;TResult;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 231
    .local p2, task:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    invoke-virtual {p2}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->isSucceed()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 232
    invoke-virtual {p2}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 234
    :cond_0
    return-void
.end method

.method private executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z
    .locals 1
    .parameter "async"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z",
            "Lcom/youdao/note/task/AbstractAsyncRequestTask",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 702
    .local p2, task:Lcom/youdao/note/task/AbstractAsyncRequestTask;,"Lcom/youdao/note/task/AbstractAsyncRequestTask<TT;>;"
    if-nez p1, :cond_0

    .line 703
    invoke-virtual {p2}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->syncExecute()Ljava/lang/Object;

    .line 704
    invoke-virtual {p2}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->isSucceed()Z

    move-result v0

    .line 708
    :goto_0
    return v0

    .line 706
    :cond_0
    invoke-virtual {p2}, Lcom/youdao/note/task/AbstractAsyncRequestTask;->execute()V

    .line 707
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v0, p2}, Lcom/youdao/note/task/RequestManager;->manageTask(Lcom/youdao/note/task/IManageableTask;)V

    .line 708
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private executeTask(ZLcom/youdao/note/task/LocalTask;)Z
    .locals 1
    .parameter "async"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z",
            "Lcom/youdao/note/task/LocalTask",
            "<*TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 713
    .local p2, task:Lcom/youdao/note/task/LocalTask;,"Lcom/youdao/note/task/LocalTask<*TT;>;"
    if-nez p1, :cond_0

    .line 714
    invoke-virtual {p2}, Lcom/youdao/note/task/LocalTask;->syncExecute()Ljava/lang/Object;

    .line 715
    invoke-virtual {p2}, Lcom/youdao/note/task/LocalTask;->isSucceed()Z

    move-result v0

    .line 719
    :goto_0
    return v0

    .line 717
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p2, v0}, Lcom/youdao/note/task/LocalTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 718
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v0, p2}, Lcom/youdao/note/task/RequestManager;->manageTask(Lcom/youdao/note/task/IManageableTask;)V

    .line 719
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getInstance()Lcom/youdao/note/task/TaskManager;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/youdao/note/task/TaskManager;->mInstance:Lcom/youdao/note/task/TaskManager;

    return-object v0
.end method

.method private isServerVersionNewer(Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)Z
    .locals 2
    .parameter "noteMeta"
    .parameter "localNoteMeta"

    .prologue
    .line 661
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v0

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteBook;->getVersion()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public checkUpdate()V
    .locals 5

    .prologue
    .line 119
    :try_start_0
    new-instance v0, Lcom/youdao/note/task/TaskManager$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/task/TaskManager$2;-><init>(Lcom/youdao/note/task/TaskManager;)V

    .line 132
    .local v0, checkUpdateTask:Lcom/youdao/note/task/network/CheckUpdateTask;
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v0           #checkUpdateTask:Lcom/youdao/note/task/network/CheckUpdateTask;
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v1

    .line 134
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "Faild to check version update."

    invoke-static {p0, v2, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    const/16 v2, 0x15

    new-instance v3, Lcom/youdao/note/data/RemoteErrorData;

    invoke-direct {v3, v1}, Lcom/youdao/note/data/RemoteErrorData;-><init>(Ljava/lang/Exception;)V

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/youdao/note/task/TaskManager;->updateResult(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0
.end method

.method public delNote(Ljava/lang/String;Z)V
    .locals 1
    .parameter "entryPath"
    .parameter "async"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 211
    new-instance v0, Lcom/youdao/note/task/TaskManager$5;

    invoke-direct {v0, p0, p1, p1}, Lcom/youdao/note/task/TaskManager$5;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v0, deleteFileTask:Lcom/youdao/note/task/network/DelFileTask;
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 225
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 226
    return-void
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "userName"
    .parameter "passWord"

    .prologue
    .line 101
    new-instance v0, Lcom/youdao/note/task/TaskManager$1;

    invoke-static {}, Lcom/youdao/note/utils/IpUtils;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/youdao/note/task/TaskManager$1;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .local v0, loginTask:Lcom/youdao/note/task/network/LoginTask;
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 114
    return-void
.end method

.method public mailShare(Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .parameter "noteMeta"
    .parameter "receivers"
    .parameter "subject"
    .parameter "message"

    .prologue
    .line 585
    new-instance v0, Lcom/youdao/note/task/TaskManager$16;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/task/TaskManager$16;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteMeta;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    .local v0, task:Lcom/youdao/note/task/network/MailShareTask;
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 601
    return-void
.end method

.method public moveNote(Lcom/youdao/note/data/NoteMeta;Z)Z
    .locals 3
    .parameter "noteMeta"
    .parameter "async"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Try to move "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    new-instance v0, Lcom/youdao/note/task/TaskManager$15;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getEntryPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/youdao/note/task/TaskManager$15;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Ljava/lang/String;Lcom/youdao/note/data/NoteMeta;)V

    .line 578
    .local v0, moveNoteTask:Lcom/youdao/note/task/network/MoveNoteTask;
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 579
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 580
    invoke-virtual {v0}, Lcom/youdao/note/task/network/MoveNoteTask;->isSucceed()Z

    move-result v1

    return v1
.end method

.method public pullNote(Lcom/youdao/note/data/NoteMeta;Z)V
    .locals 1
    .parameter "noteMeta"
    .parameter "async"

    .prologue
    .line 282
    new-instance v0, Lcom/youdao/note/task/TaskManager$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/youdao/note/task/TaskManager$7;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteMeta;Z)V

    .line 298
    .local v0, getFileTask:Lcom/youdao/note/task/network/GetNoteTask;
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 299
    return-void
.end method

.method public pullNoteIfNeed(Lcom/youdao/note/data/NoteMeta;)V
    .locals 4
    .parameter "noteMeta"

    .prologue
    .line 243
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v2, p1}, Lcom/youdao/note/datasource/DataSource;->getNote(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Note;

    move-result-object v1

    .line 244
    .local v1, note:Lcom/youdao/note/data/Note;
    if-nez v1, :cond_0

    .line 245
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/datasource/DataSource;->removeNoteContentVersion(Ljava/lang/String;)Z

    .line 247
    :cond_0
    new-instance v0, Lcom/youdao/note/task/TaskManager$6;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getEntryPath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-direct {v0, p0, v2, v3, p1}, Lcom/youdao/note/task/TaskManager$6;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;ILcom/youdao/note/data/NoteMeta;)V

    .line 271
    .local v0, getFileMetaTask:Lcom/youdao/note/task/network/GetFileMetaTask;
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 272
    return-void
.end method

.method public pullNotes(Z)V
    .locals 1
    .parameter "async"

    .prologue
    .line 379
    new-instance v0, Lcom/youdao/note/task/TaskManager$11;

    invoke-direct {v0, p0, p1}, Lcom/youdao/note/task/TaskManager$11;-><init>(Lcom/youdao/note/task/TaskManager;Z)V

    .line 420
    .local v0, getUserMetaTask:Lcom/youdao/note/task/network/GetUserMetaTask;
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 421
    return-void
.end method

.method public pullNotesIfNecessary(Z)Z
    .locals 2
    .parameter "async"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 432
    new-instance v0, Lcom/youdao/note/task/TaskManager$12;

    invoke-direct {v0, p0, p1}, Lcom/youdao/note/task/TaskManager$12;-><init>(Lcom/youdao/note/task/TaskManager;Z)V

    .line 464
    .local v0, checkNeedPullTask:Lcom/youdao/note/task/TaskManager$CheckNeedPullTask;
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 465
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 466
    invoke-virtual {v0}, Lcom/youdao/note/task/TaskManager$CheckNeedPullTask;->isNeedPull()Z

    move-result v1

    return v1
.end method

.method public pullResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)V
    .locals 8
    .parameter "meta"

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 336
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 370
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    new-instance v0, Lcom/youdao/note/task/TaskManager$10;

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move v5, v4

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/youdao/note/task/TaskManager$10;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;IILcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 368
    .local v0, grTask:Lcom/youdao/note/task/network/GetResourceTask;
    invoke-direct {p0, v7, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    goto :goto_0
.end method

.method public pullThumbnail(Lcom/youdao/note/data/Thumbnail;)V
    .locals 7
    .parameter "thumbnail"

    .prologue
    const/16 v4, 0x320

    .line 302
    new-instance v0, Lcom/youdao/note/task/TaskManager$8;

    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {p1}, Lcom/youdao/note/data/Thumbnail;->getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getThumbnailPath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/youdao/note/data/Thumbnail;->getImageMeta()Lcom/youdao/note/data/resource/AbstractImageResourceMeta;

    move-result-object v3

    move-object v1, p0

    move v5, v4

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/youdao/note/task/TaskManager$8;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;Lcom/youdao/note/data/resource/IResourceMeta;IILcom/youdao/note/data/Thumbnail;)V

    .line 314
    .local v0, grTask:Lcom/youdao/note/task/network/GetResourceTask;
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 315
    return-void
.end method

.method public pullUserMeta(Z)V
    .locals 1
    .parameter "async"

    .prologue
    .line 140
    new-instance v0, Lcom/youdao/note/task/TaskManager$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/task/TaskManager$3;-><init>(Lcom/youdao/note/task/TaskManager;)V

    .line 153
    .local v0, getUserMetaTask:Lcom/youdao/note/task/network/GetUserMetaTask;
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 154
    return-void
.end method

.method public pushNote(Lcom/youdao/note/data/Note;Ljava/util/Map;)Z
    .locals 7
    .parameter "note"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/youdao/note/data/Note;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/youdao/note/data/resource/BaseResourceMeta;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p2, dirtyResources:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/youdao/note/data/resource/BaseResourceMeta;>;"
    const/4 v6, 0x0

    .line 522
    if-nez p1, :cond_0

    .line 523
    const/4 v1, 0x1

    .line 551
    :goto_0
    return v1

    .line 525
    :cond_0
    invoke-virtual {p1}, Lcom/youdao/note/data/Note;->getNoteMeta()Lcom/youdao/note/data/NoteMeta;

    move-result-object v4

    .line 526
    .local v4, noteMeta:Lcom/youdao/note/data/NoteMeta;
    new-instance v0, Lcom/youdao/note/task/TaskManager$14;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/task/TaskManager$14;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/Note;Ljava/util/Map;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/data/Note;)V

    .line 549
    .local v0, putNoteTask:Lcom/youdao/note/task/network/PutNoteTask;
    invoke-direct {p0, v6, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 550
    invoke-direct {p0, v6, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 551
    invoke-virtual {v0}, Lcom/youdao/note/task/network/PutNoteTask;->isSucceed()Z

    move-result v1

    goto :goto_0
.end method

.method public pushResource(Lcom/youdao/note/data/resource/BaseResourceMeta;Lorg/apache/http/entity/mime/MultipartUploadListener;)Z
    .locals 7
    .parameter "meta"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 485
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "try to push resource "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->mDataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, p1}, Lcom/youdao/note/datasource/DataSource;->getResource(Lcom/youdao/note/data/resource/BaseResourceMeta;)Lcom/youdao/note/data/resource/AbstractResource;

    move-result-object v2

    .line 487
    .local v2, resource:Lcom/youdao/note/data/resource/AbstractResource;,"Lcom/youdao/note/data/resource/AbstractResource<+Lcom/youdao/note/data/resource/IResourceMeta;>;"
    new-instance v0, Lcom/youdao/note/task/TaskManager$13;

    invoke-virtual {p1}, Lcom/youdao/note/data/resource/BaseResourceMeta;->getResourceId()Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/task/TaskManager$13;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/resource/AbstractResource;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartUploadListener;Lcom/youdao/note/data/resource/BaseResourceMeta;)V

    .line 508
    .local v0, putResourceTask:Lcom/youdao/note/task/network/PutResourceTask;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "put resource result is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/youdao/note/task/network/PutResourceTask;->isSucceed()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 509
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/youdao/note/task/network/PutResourceTask;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 510
    invoke-direct {p0, v6, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 511
    invoke-direct {p0, v6, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 512
    invoke-virtual {v0}, Lcom/youdao/note/task/network/PutResourceTask;->isSucceed()Z

    move-result v1

    return v1
.end method

.method public registDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V
    .locals 2
    .parameter "listener"

    .prologue
    .line 666
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 667
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 668
    monitor-exit v1

    .line 669
    return-void

    .line 668
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public searchNotes(Ljava/lang/String;)V
    .locals 2
    .parameter "keyword"

    .prologue
    .line 318
    new-instance v0, Lcom/youdao/note/task/TaskManager$9;

    invoke-direct {v0, p0, p1}, Lcom/youdao/note/task/TaskManager$9;-><init>(Lcom/youdao/note/task/TaskManager;Ljava/lang/String;)V

    .line 330
    .local v0, searchTask:Lcom/youdao/note/task/network/SearchTask;
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 331
    return-void
.end method

.method public stopAll()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->mManager:Lcom/youdao/note/task/RequestManager;

    invoke-virtual {v0}, Lcom/youdao/note/task/RequestManager;->stopAll()V

    .line 97
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->downloadingResourceMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 98
    return-void
.end method

.method public unregistDataListener(Lcom/youdao/note/task/TaskManager$DataUpdateListener;)V
    .locals 2
    .parameter "listener"

    .prologue
    .line 672
    iget-object v1, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 673
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 674
    monitor-exit v1

    .line 675
    return-void

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateMeta(Lcom/youdao/note/data/NoteBook;Z)V
    .locals 1
    .parameter "noteBook"
    .parameter "async"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 185
    new-instance v0, Lcom/youdao/note/task/TaskManager$4;

    invoke-direct {v0, p0, p1, p1}, Lcom/youdao/note/task/TaskManager$4;-><init>(Lcom/youdao/note/task/TaskManager;Lcom/youdao/note/data/NoteBook;Lcom/youdao/note/data/NoteBook;)V

    .line 206
    .local v0, updateNoteBook:Lcom/youdao/note/task/network/MetaUpdateTask;
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->executeTask(ZLcom/youdao/note/task/AbstractAsyncRequestTask;)Z

    .line 207
    invoke-direct {p0, p2, v0}, Lcom/youdao/note/task/TaskManager;->checkTaskResult(ZLcom/youdao/note/task/AsyncTaskWithExecuteResult;)V

    .line 208
    return-void
.end method

.method public updateResult(ILcom/youdao/note/data/BaseData;Z)V
    .locals 4
    .parameter "dataType"
    .parameter "data"
    .parameter "succeed"

    .prologue
    .line 678
    iget-object v3, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    monitor-enter v3

    .line 679
    :try_start_0
    iget-object v2, p0, Lcom/youdao/note/task/TaskManager;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/task/TaskManager$DataUpdateListener;

    .line 680
    .local v1, listener:Lcom/youdao/note/task/TaskManager$DataUpdateListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/youdao/note/task/TaskManager$DataUpdateListener;->onUpdate(ILcom/youdao/note/data/BaseData;Z)V

    goto :goto_0

    .line 682
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/youdao/note/task/TaskManager$DataUpdateListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 683
    return-void
.end method
