.class public abstract Lcom/youdao/note/task/AsyncTaskWithExecuteResult;
.super Landroid/os/AsyncTask;
.source "AsyncTaskWithExecuteResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# instance fields
.field private mException:Ljava/lang/Exception;

.field private mExtraInfo:Ljava/lang/Object;

.field private mSucceed:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    .line 21
    iput-object v1, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mException:Ljava/lang/Exception;

    .line 23
    iput-object v1, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mExtraInfo:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected clearError()V
    .locals 1

    .prologue
    .line 70
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mException:Ljava/lang/Exception;

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    .line 72
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    .local p1, params:[Ljava/lang/Object;,"[TParams;"
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->innerRun([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 38
    :goto_0
    return-object v1

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "Got error in AsyncTaskWithExecuteResult"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    iput-object v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mException:Ljava/lang/Exception;

    .line 37
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    .line 38
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 58
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mException:Ljava/lang/Exception;

    return-object v0
.end method

.method protected getExtraErrorInfo()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mExtraInfo:Ljava/lang/Object;

    return-object v0
.end method

.method protected varargs abstract innerRun([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 27
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    iget-boolean v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    return v0
.end method

.method protected abstract onFailed(Ljava/lang/Exception;)V
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    .local p1, result:Ljava/lang/Object;,"TResult;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "got result is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    if-nez v0, :cond_1

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mException:Ljava/lang/Exception;

    invoke-virtual {p0, v0}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->onFailed(Ljava/lang/Exception;)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string v0, "call on succeed."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->onSucceed(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected abstract onSucceed(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation
.end method

.method public setExecuteResult(Z)V
    .locals 0
    .parameter "succeed"

    .prologue
    .line 54
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    iput-boolean p1, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mSucceed:Z

    .line 55
    return-void
.end method

.method protected setExtracErrorInfo(Ljava/lang/Object;)V
    .locals 0
    .parameter "o"

    .prologue
    .line 62
    .local p0, this:Lcom/youdao/note/task/AsyncTaskWithExecuteResult;,"Lcom/youdao/note/task/AsyncTaskWithExecuteResult<TParams;TProgress;TResult;>;"
    iput-object p1, p0, Lcom/youdao/note/task/AsyncTaskWithExecuteResult;->mExtraInfo:Ljava/lang/Object;

    .line 63
    return-void
.end method

.method protected abstract syncExecute()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation
.end method
