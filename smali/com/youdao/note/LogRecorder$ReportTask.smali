.class Lcom/youdao/note/LogRecorder$ReportTask;
.super Lcom/youdao/note/task/BaseApiRequestTask;
.source "LogRecorder.java"

# interfaces
.implements Lcom/youdao/note/utils/Consts$APIS;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/LogRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReportTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/BaseApiRequestTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/youdao/note/utils/Consts$APIS;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/LogRecorder;


# direct methods
.method public constructor <init>(Lcom/youdao/note/LogRecorder;)V
    .locals 3
    .parameter

    .prologue
    .line 395
    iput-object p1, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    .line 396
    const-string v0, "ilogrpt"

    const-string v1, "put"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/youdao/note/task/BaseApiRequestTask;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    return-void
.end method


# virtual methods
.method protected getPostMethod()Lorg/apache/http/client/methods/HttpPost;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 401
    invoke-super {p0}, Lcom/youdao/note/task/BaseApiRequestTask;->getPostMethod()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v5

    .line 402
    .local v5, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 403
    .local v4, params:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->lock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/youdao/note/LogRecorder;->access$000(Lcom/youdao/note/LogRecorder;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 404
    :try_start_0
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/youdao/note/LogRecorder;->access$100(Lcom/youdao/note/LogRecorder;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    .line 405
    .local v3, map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Total parameter size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 406
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 407
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #map:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #calls: Lcom/youdao/note/LogRecorder;->getLastReportTime()J
    invoke-static {v6}, Lcom/youdao/note/LogRecorder;->access$200(Lcom/youdao/note/LogRecorder;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 411
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "StatDuration"

    const-string v8, "0"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    :goto_1
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "PassCodeStatus"

    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v6}, Lcom/youdao/note/LogRecorder;->access$300(Lcom/youdao/note/LogRecorder;)Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->isPinlockEnable()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "1"

    :goto_2
    invoke-direct {v7, v8, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "AutoSyncStatus"

    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-static {v6}, Lcom/youdao/note/LogRecorder;->access$300(Lcom/youdao/note/LogRecorder;)Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->getAutoSyncPeriod()I

    move-result v6

    if-lez v6, :cond_4

    const-string v6, "1"

    :goto_3
    invoke-direct {v7, v8, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v6}, Lcom/youdao/note/LogRecorder;->getError()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 421
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "error"

    iget-object v8, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v8}, Lcom/youdao/note/LogRecorder;->getError()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    :cond_1
    iget-object v6, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    invoke-virtual {v6, v4}, Lcom/youdao/note/LogRecorder;->addGeneralParameter(Ljava/util/List;)V

    .line 424
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v6, "utf-8"

    invoke-direct {v0, v4, v6}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 425
    .local v0, entity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    invoke-virtual {v5, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 426
    return-object v5

    .line 413
    .end local v0           #entity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    :cond_2
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "StatDuration"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v10, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #calls: Lcom/youdao/note/LogRecorder;->getLastReportTime()J
    invoke-static {v10}, Lcom/youdao/note/LogRecorder;->access$200(Lcom/youdao/note/LogRecorder;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/32 v10, 0x36ee80

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 418
    :cond_3
    const-string v6, "0"

    goto :goto_2

    .line 419
    :cond_4
    const-string v6, "0"

    goto :goto_3
.end method

.method protected handleResponse(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .parameter "response"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got reponse for log report."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 431
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic handleResponse(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 393
    invoke-virtual {p0, p1}, Lcom/youdao/note/LogRecorder$ReportTask;->handleResponse(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onSucceed(Ljava/lang/Boolean;)V
    .locals 3
    .parameter "result"

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/youdao/note/LogRecorder;->access$100(Lcom/youdao/note/LogRecorder;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 436
    iget-object v0, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #getter for: Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/youdao/note/LogRecorder;->access$100(Lcom/youdao/note/LogRecorder;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddCameraTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ViewPicTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NoteDetailTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TextNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WbCorrectTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "EditNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SearchNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ViewNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CameraNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddPhotoTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ViewAttachTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AttachNum"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WidgetAddTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AppAddTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WbCorrectTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddArtTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddPaintTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "IconNewTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "IconSyncTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "IconAllTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "IconNotebookTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ContentCopyTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RecordNoteTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AddRecordTimes"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RecordFromBar"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 462
    iget-object v0, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    const-string v1, "Succeed"

    invoke-static {v0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcom/youdao/note/LogRecorder$ReportTask;->this$0:Lcom/youdao/note/LogRecorder;

    #calls: Lcom/youdao/note/LogRecorder;->updateReportTime()Z
    invoke-static {v0}, Lcom/youdao/note/LogRecorder;->access$400(Lcom/youdao/note/LogRecorder;)Z

    .line 464
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 393
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/LogRecorder$ReportTask;->onSucceed(Ljava/lang/Boolean;)V

    return-void
.end method
