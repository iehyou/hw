.class public final Lcom/youdao/note/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final about_dialog:I = 0x7f030000

.field public static final app_widget:I = 0x7f030001

.field public static final audio_player_bar:I = 0x7f030002

.field public static final audio_recorder_bar:I = 0x7f030003

.field public static final dialog:I = 0x7f030004

.field public static final dialog_divider:I = 0x7f030005

.field public static final dialog_item:I = 0x7f030006

.field public static final divider:I = 0x7f030007

.field public static final doodle_activity:I = 0x7f030008

.field public static final doodle_buttons:I = 0x7f030009

.field public static final doodle_footer_buttons:I = 0x7f03000a

.field public static final edit_buttons:I = 0x7f03000b

.field public static final edit_footer_bar:I = 0x7f03000c

.field public static final edit_footer_buttons:I = 0x7f03000d

.field public static final edit_footer_layout:I = 0x7f03000e

.field public static final edit_note_activity:I = 0x7f03000f

.field public static final edit_title:I = 0x7f030010

.field public static final fun_activity:I = 0x7f030011

.field public static final go_image:I = 0x7f030012

.field public static final guid_activity_layout:I = 0x7f030013

.field public static final guide1_layout:I = 0x7f030014

.field public static final guide2_layout:I = 0x7f030015

.field public static final handwrite_activity:I = 0x7f030016

.field public static final handwrite_buttons:I = 0x7f030017

.field public static final handwrite_footer_buttons:I = 0x7f030018

.field public static final handwrite_guide_layout:I = 0x7f030019

.field public static final image_rectification_activity:I = 0x7f03001a

.field public static final image_rectification_confirm:I = 0x7f03001b

.field public static final image_rectification_divider:I = 0x7f03001c

.field public static final image_view:I = 0x7f03001d

.field public static final image_view_create:I = 0x7f03001e

.field public static final image_view_previous:I = 0x7f03001f

.field public static final license_dialog:I = 0x7f030020

.field public static final list_label:I = 0x7f030021

.field public static final list_view:I = 0x7f030022

.field public static final listview_with_head:I = 0x7f030023

.field public static final lock_box:I = 0x7f030024

.field public static final login_activity:I = 0x7f030025

.field public static final login_right_now_layout:I = 0x7f030026

.field public static final mail_selection_activity:I = 0x7f030027

.field public static final mail_selection_list_item:I = 0x7f030028

.field public static final mail_selection_list_section:I = 0x7f030029

.field public static final mail_selection_tost:I = 0x7f03002a

.field public static final mail_share_activity:I = 0x7f03002b

.field public static final main_acvitity:I = 0x7f03002c

.field public static final main_header:I = 0x7f03002d

.field public static final new_note_activity:I = 0x7f03002e

.field public static final new_note_divider:I = 0x7f03002f

.field public static final note_book_dialog:I = 0x7f030030

.field public static final note_book_item:I = 0x7f030031

.field public static final note_detail_activity:I = 0x7f030032

.field public static final note_detail_dialog:I = 0x7f030033

.field public static final note_detail_title:I = 0x7f030034

.field public static final note_list_activity:I = 0x7f030035

.field public static final note_list_container:I = 0x7f030036

.field public static final note_list_title:I = 0x7f030037

.field public static final note_meta_item:I = 0x7f030038

.field public static final note_title:I = 0x7f030039

.field public static final notebook_list_activty:I = 0x7f03003a

.field public static final notification:I = 0x7f03003b

.field public static final password_dialog:I = 0x7f03003c

.field public static final password_layout:I = 0x7f03003d

.field public static final pinlock_activity:I = 0x7f03003e

.field public static final recommend_layout:I = 0x7f03003f

.field public static final record_notification_bar:I = 0x7f030040

.field public static final require_login_layout:I = 0x7f030041

.field public static final resource_button:I = 0x7f030042

.field public static final resource_item:I = 0x7f030043

.field public static final resource_list_activity:I = 0x7f030044

.field public static final search_file_activity:I = 0x7f030045

.field public static final search_history_item:I = 0x7f030046

.field public static final search_suggest:I = 0x7f030047

.field public static final search_title:I = 0x7f030048

.field public static final sync_progress:I = 0x7f030049

.field public static final test:I = 0x7f03004a

.field public static final unlogin_notebook_activity:I = 0x7f03004b

.field public static final web_activity:I = 0x7f03004c

.field public static final write_view:I = 0x7f03004d

.field public static final ynote_toast:I = 0x7f03004e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
