.class public Lcom/youdao/note/ui/YNoteDialog;
.super Landroid/app/AlertDialog;
.source "YNoteDialog.java"


# instance fields
.field private cancelButton:Landroid/view/View;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:Landroid/view/ViewGroup;

.field private okButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "ctx"

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object v2, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    .line 28
    iput-object v2, p0, Lcom/youdao/note/ui/YNoteDialog;->mInflater:Landroid/view/LayoutInflater;

    .line 30
    iput-object v2, p0, Lcom/youdao/note/ui/YNoteDialog;->okButton:Landroid/view/View;

    .line 32
    iput-object v2, p0, Lcom/youdao/note/ui/YNoteDialog;->cancelButton:Landroid/view/View;

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mInflater:Landroid/view/LayoutInflater;

    .line 37
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030004

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    .line 38
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f070014

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->okButton:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->okButton:Landroid/view/View;

    new-instance v1, Lcom/youdao/note/ui/YNoteDialog$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/ui/YNoteDialog$1;-><init>(Lcom/youdao/note/ui/YNoteDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    const v1, 0x7f070015

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->cancelButton:Landroid/view/View;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .parameter "context"
    .parameter "titleid"

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/YNoteDialog;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0, p2}, Lcom/youdao/note/ui/YNoteDialog;->setTitle(I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .parameter "context"
    .parameter "title"

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/YNoteDialog;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-virtual {p0, p2}, Lcom/youdao/note/ui/YNoteDialog;->setTitle(Ljava/lang/String;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/YNoteDialog;->setContentView(Landroid/view/View;)V

    .line 62
    return-void
.end method

.method public setNegativeListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .parameter "l"

    .prologue
    .line 89
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->cancelButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->cancelButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method public setPositiveListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .parameter "l"

    .prologue
    .line 85
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->okButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public setTitle(I)V
    .locals 3
    .parameter "resid"

    .prologue
    .line 70
    iget-object v1, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f070012

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    .local v0, titleView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 72
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3
    .parameter "title"

    .prologue
    .line 65
    iget-object v1, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f070012

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    .local v0, titleView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void
.end method

.method public setView(I)V
    .locals 2
    .parameter "resid"

    .prologue
    .line 81
    iget-object v0, p0, Lcom/youdao/note/ui/YNoteDialog;->mInflater:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/YNoteDialog;->setView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 75
    iget-object v1, p0, Lcom/youdao/note/ui/YNoteDialog;->mLayout:Landroid/view/ViewGroup;

    const v2, 0x7f070013

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 77
    .local v0, container:Landroid/view/ViewGroup;
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method
