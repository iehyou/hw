.class final Lcom/youdao/note/ui/DialogFactory$5;
.super Ljava/lang/Object;
.source "DialogFactory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/DialogFactory;->getNoteLongClickMenu(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$noteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 181
    packed-switch p2, :pswitch_data_0

    .line 198
    :goto_0
    return-void

    .line 183
    :pswitch_0
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteMeta;->getNoteId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/YNoteApplication;->sendEditNote(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$activity:Landroid/app/Activity;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    .line 193
    :pswitch_2
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$5;->val$activity:Landroid/app/Activity;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
