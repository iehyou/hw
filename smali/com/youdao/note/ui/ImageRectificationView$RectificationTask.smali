.class Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
.super Landroid/os/AsyncTask;
.source "ImageRectificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/ImageRectificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RectificationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private destImg:Landroid/graphics/Bitmap;

.field private frame:Landroid/graphics/Bitmap;

.field private temp:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/youdao/note/ui/ImageRectificationView;


# direct methods
.method private constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;)V
    .locals 0
    .parameter

    .prologue
    .line 277
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 277
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 277
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 53
    .parameter "params"

    .prologue
    .line 302
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$300(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v5

    #calls: Lcom/youdao/note/ui/ImageRectificationView;->convertPoints(Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    invoke-static {v3, v4, v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$400(Lcom/youdao/note/ui/ImageRectificationView;Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;

    move-result-object v28

    .line 303
    .local v28, imagePoints:[Landroid/graphics/PointF;
    const v39, 0x7f7fffff

    .local v39, minX:F
    const v40, 0x7f7fffff

    .local v40, minY:F
    const v36, -0x800001

    .local v36, maxX:F
    const v37, -0x800001

    .line 304
    .local v37, maxY:F
    const/16 v17, 0x0

    .local v17, i:I
    :goto_0
    move-object/from16 v0, v28

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_8

    .line 305
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-int/lit8 v4, v17, 0x1

    aget-object v4, v28, v4

    iget v4, v4, Landroid/graphics/PointF;->x:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 306
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v3, v3, v39

    if-gez v3, :cond_0

    .line 307
    aget-object v3, v28, v17

    iget v0, v3, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    .line 309
    :cond_0
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v3, v3, v36

    if-lez v3, :cond_1

    .line 310
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v0, v3, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    .line 320
    :cond_1
    :goto_1
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-int/lit8 v4, v17, 0x1

    aget-object v4, v28, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    .line 321
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v3, v40

    if-gez v3, :cond_2

    .line 322
    aget-object v3, v28, v17

    iget v0, v3, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    .line 324
    :cond_2
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v3, v37

    if-lez v3, :cond_3

    .line 325
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v0, v3, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    .line 304
    :cond_3
    :goto_2
    add-int/lit8 v17, v17, 0x2

    goto :goto_0

    .line 313
    :cond_4
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v3, v3, v39

    if-gez v3, :cond_5

    .line 314
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v0, v3, Landroid/graphics/PointF;->x:F

    move/from16 v39, v0

    .line 316
    :cond_5
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v3, v3, v36

    if-lez v3, :cond_1

    .line 317
    aget-object v3, v28, v17

    iget v0, v3, Landroid/graphics/PointF;->x:F

    move/from16 v36, v0

    goto :goto_1

    .line 328
    :cond_6
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v3, v40

    if-gez v3, :cond_7

    .line 329
    add-int/lit8 v3, v17, 0x1

    aget-object v3, v28, v3

    iget v0, v3, Landroid/graphics/PointF;->y:F

    move/from16 v40, v0

    .line 331
    :cond_7
    aget-object v3, v28, v17

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v3, v37

    if-lez v3, :cond_3

    .line 332
    aget-object v3, v28, v17

    iget v0, v3, Landroid/graphics/PointF;->y:F

    move/from16 v37, v0

    goto :goto_2

    .line 336
    :cond_8
    sub-float v51, v36, v39

    .line 337
    .local v51, xWide:F
    sub-float v52, v37, v40

    .line 338
    .local v52, yWide:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$700(Lcom/youdao/note/ui/ImageRectificationView;)I

    move-result v3

    int-to-float v3, v3

    div-float v3, v3, v51

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$800(Lcom/youdao/note/ui/ImageRectificationView;)I

    move-result v4

    int-to-float v4, v4

    div-float v4, v4, v52

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v47

    .line 339
    .local v47, scale:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v41

    .line 340
    .local v41, originalImage:Landroid/graphics/Bitmap;
    const-string v3, "mImageSrc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    const/high16 v47, 0x4000

    .line 342
    const/high16 v3, 0x3f80

    cmpg-float v3, v47, v3

    if-gez v3, :cond_a

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 344
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    move/from16 v0, v47

    move/from16 v1, v47

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v7}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v41

    .line 348
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v3, v4, :cond_9

    .line 349
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x1

    move-object/from16 v0, v41

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v48

    .line 350
    .local v48, temp:Landroid/graphics/Bitmap;
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->recycle()V

    .line 351
    move-object/from16 v41, v48

    .line 353
    .end local v48           #temp:Landroid/graphics/Bitmap;
    :cond_9
    move-object/from16 v20, v28

    .local v20, arr$:[Landroid/graphics/PointF;
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v33, v0

    .local v33, len$:I
    const/16 v27, 0x0

    .local v27, i$:I
    :goto_3
    move/from16 v0, v27

    move/from16 v1, v33

    if-ge v0, v1, :cond_a

    aget-object v42, v20, v27

    .line 354
    .local v42, point:Landroid/graphics/PointF;
    move-object/from16 v0, v42

    iget v3, v0, Landroid/graphics/PointF;->x:F

    mul-float v3, v3, v47

    move-object/from16 v0, v42

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 355
    move-object/from16 v0, v42

    iget v3, v0, Landroid/graphics/PointF;->y:F

    mul-float v3, v3, v47

    move-object/from16 v0, v42

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 353
    add-int/lit8 v27, v27, 0x1

    goto :goto_3

    .line 358
    .end local v20           #arr$:[Landroid/graphics/PointF;
    .end local v27           #i$:I
    .end local v33           #len$:I
    .end local v42           #point:Landroid/graphics/PointF;
    :cond_a
    const-string v3, "originalImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    invoke-static/range {v41 .. v41}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v34

    .line 362
    .local v34, matImage:Lorg/opencv/core/Mat;
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 363
    .local v22, cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/16 v17, 0x0

    :goto_4
    const/4 v3, 0x4

    move/from16 v0, v17

    if-ge v0, v3, :cond_b

    .line 364
    new-instance v3, Lorg/opencv/core/Point;

    aget-object v4, v28, v17

    iget v4, v4, Landroid/graphics/PointF;->x:F

    float-to-double v4, v4

    aget-object v6, v28, v17

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-double v6, v6

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/opencv/core/Point;-><init>(DD)V

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    .line 366
    :cond_b
    invoke-static/range {v22 .. v22}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v35

    .line 367
    .local v35, matVertices:Lorg/opencv/core/Mat;
    invoke-virtual/range {v34 .. v34}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v3

    invoke-virtual/range {v35 .. v35}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lcom/youdao/note/tool/img/ImageProcess;->getDestImageSize(JJ)[I

    move-result-object v8

    .line 368
    .local v8, destSize:[I
    if-eqz v8, :cond_c

    array-length v3, v8

    const/4 v4, 0x2

    if-ge v3, v4, :cond_d

    .line 369
    :cond_c
    const/4 v3, 0x0

    .line 454
    .end local v8           #destSize:[I
    .end local v17           #i:I
    .end local v22           #cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .end local v28           #imagePoints:[Landroid/graphics/PointF;
    .end local v34           #matImage:Lorg/opencv/core/Mat;
    .end local v35           #matVertices:Lorg/opencv/core/Mat;
    .end local v36           #maxX:F
    .end local v37           #maxY:F
    .end local v39           #minX:F
    .end local v40           #minY:F
    .end local v41           #originalImage:Landroid/graphics/Bitmap;
    .end local v47           #scale:F
    .end local v51           #xWide:F
    .end local v52           #yWide:F
    :goto_5
    return-object v3

    .line 371
    .restart local v8       #destSize:[I
    .restart local v17       #i:I
    .restart local v22       #cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .restart local v28       #imagePoints:[Landroid/graphics/PointF;
    .restart local v34       #matImage:Lorg/opencv/core/Mat;
    .restart local v35       #matVertices:Lorg/opencv/core/Mat;
    .restart local v36       #maxX:F
    .restart local v37       #maxY:F
    .restart local v39       #minX:F
    .restart local v40       #minY:F
    .restart local v41       #originalImage:Landroid/graphics/Bitmap;
    .restart local v47       #scale:F
    .restart local v51       #xWide:F
    .restart local v52       #yWide:F
    :cond_d
    const/4 v3, 0x0

    aget v3, v8, v3

    const/4 v4, 0x1

    aget v4, v8, v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    .line 372
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 373
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    invoke-static {v3}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v23

    .line 375
    .local v23, destMat:Lorg/opencv/core/Mat;
    invoke-virtual/range {v34 .. v34}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v2

    invoke-virtual/range {v35 .. v35}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v4

    invoke-virtual/range {v23 .. v23}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v6

    invoke-static/range {v2 .. v8}, Lcom/youdao/note/tool/img/ImageProcess;->rectify(JJJ[I)V

    .line 377
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    move-object/from16 v0, v23

    invoke-static {v0, v3}, Lcom/youdao/note/tool/img/ImageProcess;->MatToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)Z

    .line 379
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 380
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$1000(Lcom/youdao/note/ui/ImageRectificationView;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 381
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v14

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    .line 382
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v3, v4, :cond_e

    .line 383
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    .line 384
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 385
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3, v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$1102(Lcom/youdao/note/ui/ImageRectificationView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 387
    :cond_e
    new-instance v19, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 389
    .local v19, anim:Landroid/graphics/drawable/AnimationDrawable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v50

    .line 390
    .local v50, width:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    .line 391
    .local v26, height:I
    const/16 v17, 0x0

    :goto_6
    const/4 v3, 0x7

    move/from16 v0, v17

    if-gt v0, v3, :cond_11

    .line 392
    new-instance v46, Lorg/opencv/core/Point;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->areaX:F
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1200(Lcom/youdao/note/ui/ImageRectificationView;)F

    move-result v3

    float-to-double v3, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->areaY:F
    invoke-static {v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$1300(Lcom/youdao/note/ui/ImageRectificationView;)F

    move-result v5

    float-to-double v5, v5

    move-object/from16 v0, v46

    invoke-direct {v0, v3, v4, v5, v6}, Lorg/opencv/core/Point;-><init>(DD)V

    .line 393
    .local v46, refPoint:Lorg/opencv/core/Point;
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v50

    move/from16 v1, v26

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    .line 394
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    invoke-static {v3}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v25

    .line 397
    .local v25, frameMat:Lorg/opencv/core/Mat;
    new-instance v44, Ljava/util/ArrayList;

    invoke-direct/range {v44 .. v44}, Ljava/util/ArrayList;-><init>()V

    .line 398
    .local v44, pointsVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/16 v32, 0x0

    .local v32, j:I
    :goto_7
    const/4 v3, 0x4

    move/from16 v0, v32

    if-ge v0, v3, :cond_f

    .line 399
    new-instance v3, Lorg/opencv/core/Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$300(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v4

    aget-object v4, v4, v32

    iget v4, v4, Landroid/graphics/PointF;->x:F

    float-to-double v4, v4

    move-object/from16 v0, v46

    iget-wide v6, v0, Lorg/opencv/core/Point;->x:D

    sub-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/youdao/note/ui/ImageRectificationView;->access$300(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v32

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-double v6, v6

    move-object/from16 v0, v46

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    sub-double/2addr v6, v9

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/opencv/core/Point;-><init>(DD)V

    move-object/from16 v0, v44

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 398
    add-int/lit8 v32, v32, 0x1

    goto :goto_7

    .line 401
    :cond_f
    invoke-static/range {v44 .. v44}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v43

    .line 402
    .local v43, pointsMat:Lorg/opencv/core/Mat;
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 403
    .local v30, imagePointsVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/16 v32, 0x0

    :goto_8
    const/4 v3, 0x4

    move/from16 v0, v32

    if-ge v0, v3, :cond_10

    .line 404
    new-instance v3, Lorg/opencv/core/Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$1400(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v4

    aget-object v4, v4, v32

    iget v4, v4, Landroid/graphics/PointF;->x:F

    float-to-double v4, v4

    move-object/from16 v0, v46

    iget-wide v6, v0, Lorg/opencv/core/Point;->x:D

    sub-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/youdao/note/ui/ImageRectificationView;->access$1400(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v6

    aget-object v6, v6, v32

    iget v6, v6, Landroid/graphics/PointF;->y:F

    float-to-double v6, v6

    move-object/from16 v0, v46

    iget-wide v9, v0, Lorg/opencv/core/Point;->y:D

    sub-double/2addr v6, v9

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/opencv/core/Point;-><init>(DD)V

    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    add-int/lit8 v32, v32, 0x1

    goto :goto_8

    .line 407
    :cond_10
    invoke-static/range {v30 .. v30}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v29

    .line 408
    .local v29, imagePointsMat:Lorg/opencv/core/Mat;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v31

    .line 410
    .local v31, imgMat:Lorg/opencv/core/Mat;
    invoke-virtual/range {v31 .. v31}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v9

    invoke-virtual/range {v43 .. v43}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v11

    invoke-virtual/range {v29 .. v29}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v13

    invoke-virtual/range {v25 .. v25}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v15

    const/16 v18, 0x7

    invoke-static/range {v9 .. v18}, Lcom/youdao/note/tool/img/ImageProcess;->getAnimFrame(JJJJII)V

    .line 412
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    move-object/from16 v0, v25

    invoke-static {v0, v3}, Lcom/youdao/note/tool/img/ImageProcess;->MatToBitmap(Lorg/opencv/core/Mat;Landroid/graphics/Bitmap;)Z

    .line 414
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 415
    const/high16 v3, 0x3f80

    const v4, 0x3e4ccccc

    move/from16 v0, v17

    int-to-float v5, v0

    mul-float/2addr v4, v5

    const/high16 v5, 0x40e0

    div-float/2addr v4, v5

    sub-float v45, v3, v4

    .line 416
    .local v45, ratio:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v3

    move/from16 v0, v45

    move/from16 v1, v45

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 417
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;

    move-result-object v14

    const/4 v15, 0x1

    move/from16 v12, v50

    move/from16 v13, v26

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    .line 419
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v50

    move/from16 v1, v26

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    .line 420
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 421
    .local v2, canvas:Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1500(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Paint;

    move-result-object v3

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1500(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Paint;

    move-result-object v3

    const/high16 v4, -0x100

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 423
    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v50

    int-to-float v5, v0

    move/from16 v0, v26

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v7}, Lcom/youdao/note/ui/ImageRectificationView;->access$1500(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Paint;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    move/from16 v0, v50

    int-to-float v4, v0

    const/high16 v5, 0x3f80

    sub-float v5, v5, v45

    mul-float/2addr v4, v5

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    move/from16 v0, v26

    int-to-float v5, v0

    const/high16 v6, 0x3f80

    sub-float v6, v6, v45

    mul-float/2addr v5, v6

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;
    invoke-static {v6}, Lcom/youdao/note/ui/ImageRectificationView;->access$1500(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Paint;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 425
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    .line 428
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    const/16 v4, 0x64

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    .line 391
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_6

    .line 430
    .end local v2           #canvas:Landroid/graphics/Canvas;
    .end local v25           #frameMat:Lorg/opencv/core/Mat;
    .end local v29           #imagePointsMat:Lorg/opencv/core/Mat;
    .end local v30           #imagePointsVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .end local v31           #imgMat:Lorg/opencv/core/Mat;
    .end local v32           #j:I
    .end local v43           #pointsMat:Lorg/opencv/core/Mat;
    .end local v44           #pointsVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .end local v45           #ratio:F
    .end local v46           #refPoint:Lorg/opencv/core/Point;
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v3}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/activity/resource/ImageRectificationActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    const v5, 0x3f4ccccd

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/youdao/note/ui/ImageRectificationView;->access$1400(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/youdao/note/activity/resource/ImageRectificationActivity;->completeRectification(Landroid/graphics/Bitmap;Landroid/graphics/drawable/AnimationDrawable;FLandroid/graphics/PointF;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    .end local v8           #destSize:[I
    .end local v17           #i:I
    .end local v19           #anim:Landroid/graphics/drawable/AnimationDrawable;
    .end local v22           #cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .end local v23           #destMat:Lorg/opencv/core/Mat;
    .end local v26           #height:I
    .end local v28           #imagePoints:[Landroid/graphics/PointF;
    .end local v34           #matImage:Lorg/opencv/core/Mat;
    .end local v35           #matVertices:Lorg/opencv/core/Mat;
    .end local v36           #maxX:F
    .end local v37           #maxY:F
    .end local v39           #minX:F
    .end local v40           #minY:F
    .end local v41           #originalImage:Landroid/graphics/Bitmap;
    .end local v47           #scale:F
    .end local v50           #width:I
    .end local v51           #xWide:F
    .end local v52           #yWide:F
    :goto_9
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 432
    :catch_0
    move-exception v24

    .line 433
    .local v24, e:Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 434
    new-instance v38, Landroid/os/Message;

    invoke-direct/range {v38 .. v38}, Landroid/os/Message;-><init>()V

    .line 435
    .local v38, message:Landroid/os/Message;
    new-instance v21, Landroid/os/Bundle;

    invoke-direct/range {v21 .. v21}, Landroid/os/Bundle;-><init>()V

    .line 436
    .local v21, bundle:Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v3}, Lcom/youdao/note/ui/ImageRectificationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v49

    .line 437
    .local v49, tip:Ljava/lang/String;
    const-string v3, "message"

    move-object/from16 v0, v21

    move-object/from16 v1, v49

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    move-object/from16 v0, v38

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 439
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->toastHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1600(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 440
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_12

    .line 441
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 442
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->destImg:Landroid/graphics/Bitmap;

    .line 444
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_13

    .line 445
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 446
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->frame:Landroid/graphics/Bitmap;

    .line 448
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_14

    .line 449
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 450
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->temp:Landroid/graphics/Bitmap;

    .line 452
    :cond_14
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_9
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 277
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 287
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :goto_0
    return-void

    .line 288
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 282
    return-void
.end method
