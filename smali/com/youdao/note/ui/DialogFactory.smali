.class public Lcom/youdao/note/ui/DialogFactory;
.super Ljava/lang/Object;
.source "DialogFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;,
        Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method static synthetic access$000(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/youdao/note/ui/DialogFactory;->sendUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public static getAboutDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 5
    .parameter "activity"

    .prologue
    .line 86
    new-instance v0, Lcom/youdao/note/ui/YNoteDialog;

    const v3, 0x7f0a0039

    invoke-direct {v0, p0, v3}, Lcom/youdao/note/ui/YNoteDialog;-><init>(Landroid/content/Context;I)V

    .line 87
    .local v0, dialog:Lcom/youdao/note/ui/YNoteDialog;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 88
    .local v1, inflater:Landroid/view/LayoutInflater;
    const/high16 v3, 0x7f03

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 89
    .local v2, view:Landroid/view/View;
    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/youdao/note/ui/DialogFactory$2;

    invoke-direct {v4, p0}, Lcom/youdao/note/ui/DialogFactory$2;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {v0, v2}, Lcom/youdao/note/ui/YNoteDialog;->setView(Landroid/view/View;)V

    .line 96
    return-object v0
.end method

.method public static getCancleableProgressDialog(IILandroid/app/Activity;I)Landroid/app/ProgressDialog;
    .locals 2
    .parameter "titleId"
    .parameter "messageId"
    .parameter "activity"
    .parameter "style"

    .prologue
    .line 237
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 238
    .local v0, title:Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {v0, p1, p2, p3, v1}, Lcom/youdao/note/ui/DialogFactory;->getCancleableProgressDialog(Ljava/lang/String;ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v1

    return-object v1
.end method

.method public static getCancleableProgressDialog(Ljava/lang/String;ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;
    .locals 2
    .parameter "title"
    .parameter "messageId"
    .parameter "activity"
    .parameter "style"
    .parameter "l"

    .prologue
    .line 220
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 221
    .local v0, dialog:Landroid/app/ProgressDialog;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 222
    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 224
    :cond_0
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    .line 225
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 227
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 228
    invoke-virtual {v0, p3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 229
    if-eqz p4, :cond_2

    .line 230
    invoke-virtual {v0, p4}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 232
    :cond_2
    return-object v0
.end method

.method public static getDeleteConfirmDialog(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)Landroid/app/Dialog;
    .locals 3
    .parameter "activity"
    .parameter "deletedNoteMeta"
    .parameter "callback"

    .prologue
    .line 205
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "\u786e\u8ba4\u5220\u9664\uff1f"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "\u5220\u9664\u540e\u65e0\u6cd5\u6062\u590d"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/ui/DialogFactory$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/youdao/note/ui/DialogFactory$6;-><init>(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static getNewFeaturesDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 3
    .parameter "activity"

    .prologue
    .line 100
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00d6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00d7

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00ab

    new-instance v2, Lcom/youdao/note/ui/DialogFactory$3;

    invoke-direct {v2}, Lcom/youdao/note/ui/DialogFactory$3;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static getNoteBookPickDialog(Landroid/app/Activity;Ljava/lang/String;Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;)Landroid/app/Dialog;
    .locals 9
    .parameter "activity"
    .parameter "noteBookId"
    .parameter "callback"

    .prologue
    .line 51
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v6

    invoke-virtual {v6}, Lcom/youdao/note/datasource/DataSource;->listAllNoteBooksAsArray()[Lcom/youdao/note/data/NoteBook;

    move-result-object v3

    .line 53
    .local v3, noteBooks:[Lcom/youdao/note/data/NoteBook;
    const/4 v0, 0x0

    .line 54
    .local v0, checkItemId:I
    const/4 v5, 0x0

    .line 55
    .local v5, tmpOriNoteBookId:Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    array-length v6, v3

    if-ge v1, v6, :cond_0

    .line 56
    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 57
    move v0, v1

    .line 58
    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v5

    .line 62
    :cond_0
    move-object v4, v5

    .line 63
    .local v4, oriNoteBookId:Ljava/lang/String;
    array-length v6, v3

    new-array v2, v6, [Ljava/lang/String;

    .line 64
    .local v2, noteBookTitles:[Ljava/lang/String;
    const/4 v1, 0x0

    :goto_1
    array-length v6, v3

    if-ge v1, v6, :cond_2

    .line 65
    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 55
    .end local v2           #noteBookTitles:[Ljava/lang/String;
    .end local v4           #oriNoteBookId:Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    .restart local v2       #noteBookTitles:[Ljava/lang/String;
    .restart local v4       #oriNoteBookId:Ljava/lang/String;
    :cond_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v7, "\u9009\u62e9\u7b14\u8bb0\u672c"

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/youdao/note/ui/DialogFactory$1;

    invoke-direct {v7, p0, p2, v4, v3}, Lcom/youdao/note/ui/DialogFactory$1;-><init>(Landroid/app/Activity;Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;Ljava/lang/String;[Lcom/youdao/note/data/NoteBook;)V

    invoke-virtual {v6, v2, v0, v7}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f0a00aa

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6
.end method

.method public static getNoteDetailDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 11
    .parameter "activity"
    .parameter "noteId"

    .prologue
    .line 117
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v0

    .line 118
    .local v0, dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-virtual {v0, p1}, Lcom/youdao/note/datasource/DataSource;->getNoteMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteMeta;

    move-result-object v5

    .line 119
    .local v5, noteMeta:Lcom/youdao/note/data/NoteMeta;
    const/4 v4, 0x0

    .line 120
    .local v4, noteBookTitle:Ljava/lang/String;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v8

    if-nez v8, :cond_0

    .line 121
    const-string v4, "\u672a\u767b\u5f55\u7b14\u8bb0\u672c"

    .line 127
    :goto_0
    invoke-virtual {v5}, Lcom/youdao/note/data/NoteMeta;->getSourceUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 129
    .local v6, sourceUrl:Ljava/lang/String;
    new-instance v2, Lcom/youdao/note/ui/YNoteDialog;

    const v8, 0x7f0a00a5

    invoke-direct {v2, p0, v8}, Lcom/youdao/note/ui/YNoteDialog;-><init>(Landroid/content/Context;I)V

    .line 130
    .local v2, dialog:Lcom/youdao/note/ui/YNoteDialog;
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 131
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030033

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 132
    .local v1, detailView:Landroid/view/View;
    const v8, 0x7f07008d

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 133
    .local v7, textView:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\u7b14\u8bb0\u672c\uff1a"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    const v8, 0x7f07008e

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #textView:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 135
    .restart local v7       #textView:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\u66f4\u65b0\u65f6\u95f4\uff1a"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/youdao/note/utils/StringUtils;->getPrettyTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    const v8, 0x7f07008f

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #textView:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 137
    .restart local v7       #textView:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\u521b\u5efa\u65f6\u95f4\uff1a"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/youdao/note/data/NoteMeta;->getCreateTime()J

    move-result-wide v9

    invoke-static {v9, v10}, Lcom/youdao/note/utils/StringUtils;->getPrettyTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 139
    const v8, 0x7f070091

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .end local v7           #textView:Landroid/widget/TextView;
    check-cast v7, Landroid/widget/TextView;

    .line 140
    .restart local v7       #textView:Landroid/widget/TextView;
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v8, Lcom/youdao/note/ui/DialogFactory$4;

    invoke-direct {v8, v6, p0}, Lcom/youdao/note/ui/DialogFactory$4;-><init>(Ljava/lang/String;Landroid/app/Activity;)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    :goto_1
    invoke-virtual {v2, v1}, Lcom/youdao/note/ui/YNoteDialog;->setView(Landroid/view/View;)V

    .line 156
    return-object v2

    .line 123
    .end local v1           #detailView:Landroid/view/View;
    .end local v2           #dialog:Lcom/youdao/note/ui/YNoteDialog;
    .end local v3           #inflater:Landroid/view/LayoutInflater;
    .end local v6           #sourceUrl:Ljava/lang/String;
    .end local v7           #textView:Landroid/widget/TextView;
    :cond_0
    invoke-virtual {v5}, Lcom/youdao/note/data/NoteMeta;->getNoteBook()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/youdao/note/datasource/DataSource;->getNoteBookMetaById(Ljava/lang/String;)Lcom/youdao/note/data/NoteBook;

    move-result-object v8

    invoke-virtual {v8}, Lcom/youdao/note/data/NoteBook;->getTitle()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 153
    .restart local v1       #detailView:Landroid/view/View;
    .restart local v2       #dialog:Lcom/youdao/note/ui/YNoteDialog;
    .restart local v3       #inflater:Landroid/view/LayoutInflater;
    .restart local v6       #sourceUrl:Ljava/lang/String;
    .restart local v7       #textView:Landroid/widget/TextView;
    :cond_1
    const v8, 0x7f070090

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public static getNoteLongClickMenu(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)Landroid/app/Dialog;
    .locals 6
    .parameter "activity"
    .parameter "noteMeta"

    .prologue
    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, MENU_EDIT:I
    const/4 v0, 0x1

    .line 174
    .local v0, MENU_DELETE:I
    const/4 v1, 0x2

    .line 175
    .local v1, MENU_DETAIL:I
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060002

    new-instance v5, Lcom/youdao/note/ui/DialogFactory$5;

    invoke-direct {v5, p0, p1}, Lcom/youdao/note/ui/DialogFactory$5;-><init>(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method private static sendUrl(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .parameter "activity"
    .parameter "url"

    .prologue
    .line 161
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 162
    .local v1, intent:Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 163
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    .end local v1           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-class v2, Lcom/youdao/note/ui/DialogFactory;

    const-string v3, "sb.."

    invoke-static {v2, v3, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
