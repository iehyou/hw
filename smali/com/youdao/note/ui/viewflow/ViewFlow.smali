.class public Lcom/youdao/note/ui/viewflow/ViewFlow;
.super Landroid/widget/AdapterView;
.source "ViewFlow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;,
        Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/Adapter;",
        ">;"
    }
.end annotation


# static fields
.field private static final INVALID_SCREEN:I = -0x1

.field private static final SNAP_VELOCITY:I = 0x3e8

.field private static final TOUCH_STATE_REST:I = 0x0

.field private static final TOUCH_STATE_SCROLLING:I = 0x1


# instance fields
.field private mAdapter:Landroid/widget/Adapter;

.field private mCurrentAdapterIndex:I

.field private mCurrentBufferIndex:I

.field private mCurrentScreen:I

.field private mDataSetObserver:Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;

.field private mFirstLayout:Z

.field private mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mLastOrientation:I

.field private mLastScrollDirection:I

.field private mLoadedViews:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMaximumVelocity:I

.field private mNextScreen:I

.field private mScroller:Landroid/widget/Scroller;

.field private mSideBuffer:I

.field private mTouchSlop:I

.field private mTouchState:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

.field private orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    const/4 v1, -0x1

    .line 104
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x2

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    .line 67
    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mFirstLayout:Z

    .line 74
    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastOrientation:I

    .line 76
    new-instance v0, Lcom/youdao/note/ui/viewflow/ViewFlow$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/viewflow/ViewFlow$1;-><init>(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    iput-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 105
    const/4 v0, 0x3

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 106
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->init()V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .parameter "context"
    .parameter "sideBuffer"

    .prologue
    const/4 v1, -0x1

    .line 110
    invoke-direct {p0, p1}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x2

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    .line 67
    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mFirstLayout:Z

    .line 74
    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastOrientation:I

    .line 76
    new-instance v0, Lcom/youdao/note/ui/viewflow/ViewFlow$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/viewflow/ViewFlow$1;-><init>(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    iput-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 111
    iput p2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 112
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->init()V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 116
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const/4 v1, 0x2

    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 61
    iput v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    .line 67
    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 68
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mFirstLayout:Z

    .line 74
    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastOrientation:I

    .line 76
    new-instance v1, Lcom/youdao/note/ui/viewflow/ViewFlow$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/ui/viewflow/ViewFlow$1;-><init>(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    iput-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 117
    sget-object v1, Lcom/youdao/note/R$styleable;->ViewFlow:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 119
    .local v0, styledAttrs:Landroid/content/res/TypedArray;
    const/4 v1, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    .line 120
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->init()V

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/viewflow/ViewFlow;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/viewflow/ViewFlow;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 48
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/youdao/note/ui/viewflow/ViewFlow;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 48
    iput p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/viewflow/ViewFlow;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 48
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/viewflow/ViewFlow;)Landroid/widget/Adapter;
    .locals 1
    .parameter "x0"

    .prologue
    .line 48
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/ui/viewflow/ViewFlow;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->resetFocus()V

    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 124
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    .line 125
    new-instance v1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    .line 126
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 128
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchSlop:I

    .line 129
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mMaximumVelocity:I

    .line 130
    return-void
.end method

.method private logBuffer()V
    .locals 3

    .prologue
    .line 687
    const-string v0, "viewflow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Size of mLoadedViews: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "X: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    const-string v0, "viewflow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IndexInAdapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", IndexInBuffer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    return-void
.end method

.method private makeAndAddView(IZLandroid/view/View;)Landroid/view/View;
    .locals 2
    .parameter "position"
    .parameter "addToEnd"
    .parameter "convertView"

    .prologue
    .line 658
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v1, p1, p3, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 659
    .local v0, view:Landroid/view/View;
    if-eqz p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v0, p2, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setupChild(Landroid/view/View;ZZ)Landroid/view/View;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private postViewSwitched(I)V
    .locals 5
    .parameter "direction"

    .prologue
    const/4 v4, 0x1

    .line 584
    if-nez p1, :cond_0

    .line 640
    :goto_0
    return-void

    .line 587
    :cond_0
    if-lez p1, :cond_5

    .line 588
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    .line 589
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    .line 591
    const/4 v1, 0x0

    .line 594
    .local v1, recycleView:Landroid/view/View;
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    if-le v2, v3, :cond_1

    .line 595
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    .end local v1           #recycleView:Landroid/view/View;
    check-cast v1, Landroid/view/View;

    .line 596
    .restart local v1       #recycleView:Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->detachViewFromParent(Landroid/view/View;)V

    .line 598
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    .line 602
    :cond_1
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    add-int v0, v2, v3

    .line 603
    .local v0, newBufferIndex:I
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 604
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-direct {p0, v0, v4, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 628
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->requestLayout()V

    .line 629
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-direct {p0, v2, v4}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setVisibleView(IZ)V

    .line 630
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    if-eqz v2, :cond_3

    .line 631
    iget-object v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    iget v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    invoke-interface {v3, v2, v4}, Lcom/youdao/note/ui/viewflow/FlowIndicator;->onSwitched(Landroid/view/View;I)V

    .line 634
    :cond_3
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

    if-eqz v2, :cond_4

    .line 635
    iget-object v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    iget v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v2, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    invoke-interface {v3, v2, v4}, Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;->onSwitched(Landroid/view/View;I)V

    .line 639
    :cond_4
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->logBuffer()V

    goto :goto_0

    .line 608
    .end local v0           #newBufferIndex:I
    .end local v1           #recycleView:Landroid/view/View;
    :cond_5
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    .line 609
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    .line 610
    const/4 v1, 0x0

    .line 613
    .restart local v1       #recycleView:Landroid/view/View;
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    if-le v2, v3, :cond_6

    .line 614
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v1

    .end local v1           #recycleView:Landroid/view/View;
    check-cast v1, Landroid/view/View;

    .line 615
    .restart local v1       #recycleView:Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->detachViewFromParent(Landroid/view/View;)V

    .line 619
    :cond_6
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    sub-int v0, v2, v3

    .line 620
    .restart local v0       #newBufferIndex:I
    const/4 v2, -0x1

    if-le v0, v2, :cond_2

    .line 621
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 623
    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    goto :goto_1
.end method

.method private resetFocus()V
    .locals 4

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->logBuffer()V

    .line 569
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 570
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->removeAllViewsInLayout()V

    .line 572
    const/4 v1, 0x0

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 575
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 576
    iget v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    if-ne v0, v1, :cond_0

    .line 577
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    .line 574
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 579
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->logBuffer()V

    .line 580
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->requestLayout()V

    .line 581
    return-void
.end method

.method private setVisibleView(IZ)V
    .locals 6
    .parameter "indexInBuffer"
    .parameter "uiThread"

    .prologue
    const/4 v4, 0x0

    .line 443
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    .line 445
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    sub-int v3, v0, v1

    .line 446
    .local v3, dx:I
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 448
    if-nez v3, :cond_0

    .line 449
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    add-int/2addr v0, v3

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    add-int/2addr v2, v3

    iget-object v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/youdao/note/ui/viewflow/ViewFlow;->onScrollChanged(IIII)V

    .line 450
    :cond_0
    if-eqz p2, :cond_1

    .line 451
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->invalidate()V

    .line 454
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->postInvalidate()V

    goto :goto_0
.end method

.method private setupChild(Landroid/view/View;ZZ)Landroid/view/View;
    .locals 4
    .parameter "child"
    .parameter "addToEnd"
    .parameter "recycle"

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 643
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 645
    .local v0, p:Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 646
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    .end local v0           #p:Landroid/view/ViewGroup$LayoutParams;
    const/4 v3, -0x2

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    .line 650
    .restart local v0       #p:Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    if-eqz p3, :cond_2

    .line 651
    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 654
    :goto_1
    return-object p1

    :cond_1
    move v1, v2

    .line 651
    goto :goto_0

    .line 653
    :cond_2
    if-eqz p2, :cond_3

    :goto_2
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private snapToDestination()V
    .locals 4

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v0

    .line 402
    .local v0, screenWidth:I
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getScrollX()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    div-int v1, v2, v0

    .line 405
    .local v1, whichScreen:I
    invoke-direct {p0, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToScreen(I)V

    .line 406
    return-void
.end method

.method private snapToScreen(I)V
    .locals 7
    .parameter "whichScreen"

    .prologue
    const/4 v2, 0x0

    .line 409
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastScrollDirection:I

    .line 410
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    :goto_0
    return-void

    .line 413
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 415
    iput p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 417
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v0

    mul-int v6, p1, v0

    .line 418
    .local v6, newX:I
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getScrollX()I

    move-result v0

    sub-int v3, v6, v0

    .line 419
    .local v3, delta:I
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getScrollX()I

    move-result v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 420
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 425
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/ui/viewflow/ViewFlow;->scrollTo(II)V

    .line 427
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->postInvalidate()V

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    if-eq v0, v3, :cond_0

    .line 429
    const/4 v0, 0x0

    iget v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    .line 431
    iput v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 432
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastScrollDirection:I

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->postViewSwitched(I)V

    goto :goto_0
.end method

.method public getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 503
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 497
    iget v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    iget v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewsCount()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter "newConfig"

    .prologue
    .line 133
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastOrientation:I

    if-eq v0, v1, :cond_0

    .line 134
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastOrientation:I

    .line 135
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->orientationChangeListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 137
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .parameter "ev"

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v11

    if-nez v11, :cond_0

    .line 191
    const/4 v11, 0x0

    .line 283
    :goto_0
    return v11

    .line 193
    :cond_0
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v11, :cond_1

    .line 194
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v11

    iput-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 196
    :cond_1
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v11, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 199
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 200
    .local v6, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    .line 202
    .local v9, y:F
    packed-switch v0, :pswitch_data_0

    .line 283
    :cond_2
    :goto_1
    const/4 v11, 0x0

    goto :goto_0

    .line 208
    :pswitch_0
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->isFinished()Z

    move-result v11

    if-nez v11, :cond_3

    .line 209
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->abortAnimation()V

    .line 213
    :cond_3
    iput v6, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    .line 214
    iput v9, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionY:F

    .line 216
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->isFinished()Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v11, 0x0

    :goto_2
    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto :goto_1

    :cond_4
    const/4 v11, 0x1

    goto :goto_2

    .line 222
    :pswitch_1
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    sub-float v11, v6, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v7, v11

    .line 223
    .local v7, xDiff:I
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionY:F

    sub-float v11, v9, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v10, v11

    .line 225
    .local v10, yDiff:I
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchSlop:I

    if-le v7, v11, :cond_7

    if-le v7, v10, :cond_7

    const/4 v8, 0x1

    .line 227
    .local v8, xMoved:Z
    :goto_3
    if-eqz v8, :cond_5

    .line 229
    const/4 v11, 0x1

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    .line 232
    :cond_5
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 234
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    sub-float/2addr v11, v6

    float-to-int v2, v11

    .line 235
    .local v2, deltaX:I
    iput v6, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    .line 237
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getScrollX()I

    move-result v3

    .line 238
    .local v3, scrollX:I
    if-gez v2, :cond_8

    .line 239
    if-lez v3, :cond_6

    .line 240
    neg-int v11, v3

    invoke-static {v11, v2}, Ljava/lang/Math;->max(II)I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/youdao/note/ui/viewflow/ViewFlow;->scrollBy(II)V

    .line 250
    :cond_6
    :goto_4
    const/4 v11, 0x1

    goto :goto_0

    .line 225
    .end local v2           #deltaX:I
    .end local v3           #scrollX:I
    .end local v8           #xMoved:Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    .line 242
    .restart local v2       #deltaX:I
    .restart local v3       #scrollX:I
    .restart local v8       #xMoved:Z
    :cond_8
    if-lez v2, :cond_6

    .line 243
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    sub-int/2addr v11, v3

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v12

    sub-int v1, v11, v12

    .line 246
    .local v1, availableToScroll:I
    if-lez v1, :cond_6

    .line 247
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/youdao/note/ui/viewflow/ViewFlow;->scrollBy(II)V

    goto :goto_4

    .line 255
    .end local v1           #availableToScroll:I
    .end local v2           #deltaX:I
    .end local v3           #scrollX:I
    .end local v7           #xDiff:I
    .end local v8           #xMoved:Z
    .end local v10           #yDiff:I
    :pswitch_2
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    .line 256
    iget-object v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 257
    .local v4, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v11, 0x3e8

    iget v12, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mMaximumVelocity:I

    int-to-float v12, v12

    invoke-virtual {v4, v11, v12}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 258
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v11

    float-to-int v5, v11

    .line 260
    .local v5, velocityX:I
    const/16 v11, 0x3e8

    if-le v5, v11, :cond_a

    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    if-lez v11, :cond_a

    .line 262
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    add-int/lit8 v11, v11, -0x1

    invoke-direct {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToScreen(I)V

    .line 271
    :goto_5
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v11, :cond_9

    .line 272
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v11}, Landroid/view/VelocityTracker;->recycle()V

    .line 273
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 277
    .end local v4           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v5           #velocityX:I
    :cond_9
    const/4 v11, 0x0

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto/16 :goto_1

    .line 263
    .restart local v4       #velocityTracker:Landroid/view/VelocityTracker;
    .restart local v5       #velocityX:I
    :cond_a
    const/16 v11, -0x3e8

    if-ge v5, v11, :cond_b

    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v11, v12, :cond_b

    .line 266
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    add-int/lit8 v11, v11, 0x1

    invoke-direct {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToScreen(I)V

    goto :goto_5

    .line 268
    :cond_b
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToDestination()V

    goto :goto_5

    .line 281
    .end local v4           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v5           #velocityX:I
    :pswitch_3
    const/4 v11, 0x0

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto/16 :goto_1

    .line 202
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 174
    const/4 v1, 0x0

    .line 176
    .local v1, childLeft:I
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v3

    .line 177
    .local v3, count:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    if-ge v4, v3, :cond_1

    .line 178
    invoke-virtual {p0, v4}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 179
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    .line 180
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 181
    .local v2, childWidth:I
    const/4 v5, 0x0

    add-int v6, v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 183
    add-int/2addr v1, v2

    .line 177
    .end local v2           #childWidth:I
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 186
    .end local v0           #child:Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v2, 0x4000

    const/4 v1, 0x0

    .line 145
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 147
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 148
    .local v9, width:I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 149
    .local v10, widthMode:I
    if-eq v10, v2, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewFlow can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 155
    .local v7, heightMode:I
    if-eq v7, v2, :cond_1

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewFlow can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v6

    .line 162
    .local v6, count:I
    const/4 v8, 0x0

    .local v8, i:I
    :goto_0
    if-ge v8, v6, :cond_2

    .line 163
    invoke-virtual {p0, v8}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 162
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 166
    :cond_2
    iget-boolean v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mFirstLayout:Z

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    mul-int v3, v2, v9

    move v2, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 168
    iput-boolean v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mFirstLayout:Z

    .line 170
    :cond_3
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .parameter "h"
    .parameter "v"
    .parameter "oldh"
    .parameter "oldv"

    .prologue
    .line 387
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->onScrollChanged(IIII)V

    .line 388
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    if-eqz v1, :cond_0

    .line 394
    iget v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    iget v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    add-int v0, p1, v1

    .line 396
    .local v0, hPerceived:I
    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    invoke-interface {v1, v0, p2, p3, p4}, Lcom/youdao/note/ui/viewflow/FlowIndicator;->onScrolled(IIII)V

    .line 398
    .end local v0           #hPerceived:I
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .parameter "ev"

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v11

    if-nez v11, :cond_0

    .line 289
    const/4 v11, 0x0

    .line 382
    :goto_0
    return v11

    .line 291
    :cond_0
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v11, :cond_1

    .line 292
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v11

    iput-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 294
    :cond_1
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v11, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 297
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 298
    .local v6, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    .line 300
    .local v9, y:F
    packed-switch v0, :pswitch_data_0

    .line 382
    :cond_2
    :goto_1
    const/4 v11, 0x1

    goto :goto_0

    .line 306
    :pswitch_0
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->isFinished()Z

    move-result v11

    if-nez v11, :cond_3

    .line 307
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->abortAnimation()V

    .line 311
    :cond_3
    iput v6, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    .line 312
    iput v9, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionY:F

    .line 314
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->isFinished()Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v11, 0x0

    :goto_2
    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto :goto_1

    :cond_4
    const/4 v11, 0x1

    goto :goto_2

    .line 320
    :pswitch_1
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    sub-float v11, v6, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v7, v11

    .line 321
    .local v7, xDiff:I
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionY:F

    sub-float v11, v9, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v10, v11

    .line 323
    .local v10, yDiff:I
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchSlop:I

    if-le v7, v11, :cond_7

    if-le v7, v10, :cond_7

    const/4 v8, 0x1

    .line 325
    .local v8, xMoved:Z
    :goto_3
    if-eqz v8, :cond_5

    .line 327
    const/4 v11, 0x1

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    .line 330
    :cond_5
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 332
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    sub-float/2addr v11, v6

    float-to-int v2, v11

    .line 333
    .local v2, deltaX:I
    iput v6, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLastMotionX:F

    .line 335
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getScrollX()I

    move-result v3

    .line 336
    .local v3, scrollX:I
    if-gez v2, :cond_8

    .line 337
    if-lez v3, :cond_6

    .line 338
    neg-int v11, v3

    invoke-static {v11, v2}, Ljava/lang/Math;->max(II)I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/youdao/note/ui/viewflow/ViewFlow;->scrollBy(II)V

    .line 348
    :cond_6
    :goto_4
    const/4 v11, 0x1

    goto :goto_0

    .line 323
    .end local v2           #deltaX:I
    .end local v3           #scrollX:I
    .end local v8           #xMoved:Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    .line 340
    .restart local v2       #deltaX:I
    .restart local v3       #scrollX:I
    .restart local v8       #xMoved:Z
    :cond_8
    if-lez v2, :cond_6

    .line 341
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    sub-int/2addr v11, v3

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getWidth()I

    move-result v12

    sub-int v1, v11, v12

    .line 344
    .local v1, availableToScroll:I
    if-lez v1, :cond_6

    .line 345
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/youdao/note/ui/viewflow/ViewFlow;->scrollBy(II)V

    goto :goto_4

    .line 353
    .end local v1           #availableToScroll:I
    .end local v2           #deltaX:I
    .end local v3           #scrollX:I
    .end local v7           #xDiff:I
    .end local v8           #xMoved:Z
    .end local v10           #yDiff:I
    :pswitch_2
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    .line 354
    iget-object v4, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 355
    .local v4, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v11, 0x3e8

    iget v12, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mMaximumVelocity:I

    int-to-float v12, v12

    invoke-virtual {v4, v11, v12}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 356
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v11

    float-to-int v5, v11

    .line 358
    .local v5, velocityX:I
    const/16 v11, 0x3e8

    if-le v5, v11, :cond_a

    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    if-lez v11, :cond_a

    .line 360
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    add-int/lit8 v11, v11, -0x1

    invoke-direct {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToScreen(I)V

    .line 369
    :goto_5
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v11, :cond_9

    .line 370
    iget-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v11}, Landroid/view/VelocityTracker;->recycle()V

    .line 371
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 375
    .end local v4           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v5           #velocityX:I
    :cond_9
    const/4 v11, 0x0

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto/16 :goto_1

    .line 361
    .restart local v4       #velocityTracker:Landroid/view/VelocityTracker;
    .restart local v5       #velocityX:I
    :cond_a
    const/16 v11, -0x3e8

    if-ge v5, v11, :cond_b

    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v11, v12, :cond_b

    .line 364
    iget v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentScreen:I

    add-int/lit8 v11, v11, 0x1

    invoke-direct {p0, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToScreen(I)V

    goto :goto_5

    .line 366
    :cond_b
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToDestination()V

    goto :goto_5

    .line 379
    .end local v4           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v5           #velocityX:I
    :pswitch_3
    invoke-direct {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->snapToDestination()V

    .line 380
    const/4 v11, 0x0

    iput v11, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mTouchState:I

    goto/16 :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 1
    .parameter "adapter"

    .prologue
    .line 474
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setAdapter(Landroid/widget/Adapter;I)V

    .line 475
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;I)V
    .locals 2
    .parameter "adapter"
    .parameter "initialPosition"

    .prologue
    .line 478
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mDataSetObserver:Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 482
    :cond_0
    iput-object p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    .line 484
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 485
    new-instance v0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;-><init>(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    iput-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mDataSetObserver:Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;

    .line 486
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mDataSetObserver:Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 489
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 493
    :cond_2
    :goto_0
    return-void

    .line 492
    :cond_3
    invoke-virtual {p0, p2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setSelection(I)V

    goto :goto_0
.end method

.method public setFlowIndicator(Lcom/youdao/note/ui/viewflow/FlowIndicator;)V
    .locals 1
    .parameter "flowIndicator"

    .prologue
    .line 512
    iput-object p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    .line 513
    iget-object v0, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    invoke-interface {v0, p0}, Lcom/youdao/note/ui/viewflow/FlowIndicator;->setViewFlow(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    .line 514
    return-void
.end method

.method public setOnViewSwitchListener(Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 464
    iput-object p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

    .line 465
    return-void
.end method

.method public setSelection(I)V
    .locals 13
    .parameter "position"

    .prologue
    const/4 v9, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 518
    const/4 v8, -0x1

    iput v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mNextScreen:I

    .line 519
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8, v12}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 520
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    if-nez v8, :cond_1

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    invoke-static {p1, v11}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 524
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v8}, Landroid/widget/Adapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 526
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .local v5, recycleViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/view/View;>;"
    :goto_1
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 529
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, recycleView:Landroid/view/View;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 530
    invoke-virtual {p0, v4}, Lcom/youdao/note/ui/viewflow/ViewFlow;->detachViewFromParent(Landroid/view/View;)V

    goto :goto_1

    .line 533
    .end local v4           #recycleView:Landroid/view/View;
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    move-object v8, v9

    :goto_2
    invoke-direct {p0, p1, v12, v8}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 535
    .local v0, currentView:Landroid/view/View;
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v8, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 537
    const/4 v3, 0x1

    .local v3, offset:I
    :goto_3
    iget v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mSideBuffer:I

    sub-int/2addr v8, v3

    if-ltz v8, :cond_8

    .line 538
    sub-int v2, p1, v3

    .line 539
    .local v2, leftIndex:I
    add-int v6, p1, v3

    .line 540
    .local v6, rightIndex:I
    if-ltz v2, :cond_3

    .line 541
    iget-object v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_6

    move-object v8, v9

    :goto_4
    invoke-direct {p0, v2, v11, v8}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 543
    :cond_3
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;

    invoke-interface {v8}, Landroid/widget/Adapter;->getCount()I

    move-result v8

    if-ge v6, v8, :cond_4

    .line 544
    iget-object v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_7

    move-object v8, v9

    :goto_5
    invoke-direct {p0, v6, v12, v8}, Lcom/youdao/note/ui/viewflow/ViewFlow;->makeAndAddView(IZLandroid/view/View;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 537
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 533
    .end local v0           #currentView:Landroid/view/View;
    .end local v2           #leftIndex:I
    .end local v3           #offset:I
    .end local v6           #rightIndex:I
    :cond_5
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    goto :goto_2

    .line 541
    .restart local v0       #currentView:Landroid/view/View;
    .restart local v2       #leftIndex:I
    .restart local v3       #offset:I
    .restart local v6       #rightIndex:I
    :cond_6
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    goto :goto_4

    .line 544
    :cond_7
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    goto :goto_5

    .line 548
    .end local v2           #leftIndex:I
    .end local v6           #rightIndex:I
    :cond_8
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    invoke-virtual {v8, v0}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    iput v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    .line 549
    iput p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    .line 551
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 552
    .local v7, view:Landroid/view/View;
    invoke-virtual {p0, v7, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->removeDetachedView(Landroid/view/View;Z)V

    goto :goto_6

    .line 554
    .end local v7           #view:Landroid/view/View;
    :cond_9
    invoke-virtual {p0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->requestLayout()V

    .line 555
    iget v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-direct {p0, v8, v11}, Lcom/youdao/note/ui/viewflow/ViewFlow;->setVisibleView(IZ)V

    .line 556
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    if-eqz v8, :cond_a

    .line 557
    iget-object v9, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mIndicator:Lcom/youdao/note/ui/viewflow/FlowIndicator;

    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    iget v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v8, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    iget v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    invoke-interface {v9, v8, v10}, Lcom/youdao/note/ui/viewflow/FlowIndicator;->onSwitched(Landroid/view/View;I)V

    .line 560
    :cond_a
    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

    if-eqz v8, :cond_0

    .line 561
    iget-object v9, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mViewSwitchListener:Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;

    iget-object v8, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mLoadedViews:Ljava/util/LinkedList;

    iget v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I

    invoke-virtual {v8, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    iget v10, p0, Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I

    invoke-interface {v9, v8, v10}, Lcom/youdao/note/ui/viewflow/ViewFlow$ViewSwitchListener;->onSwitched(Landroid/view/View;I)V

    goto/16 :goto_0
.end method
