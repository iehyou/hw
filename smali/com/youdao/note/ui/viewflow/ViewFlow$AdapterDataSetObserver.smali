.class Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "ViewFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/viewflow/ViewFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdapterDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/viewflow/ViewFlow;)V
    .locals 0
    .parameter

    .prologue
    .line 662
    iput-object p1, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    .prologue
    .line 666
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    iget-object v3, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    #getter for: Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentBufferIndex:I
    invoke-static {v3}, Lcom/youdao/note/ui/viewflow/ViewFlow;->access$200(Lcom/youdao/note/ui/viewflow/ViewFlow;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/youdao/note/ui/viewflow/ViewFlow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 667
    .local v1, v:Landroid/view/View;
    if-eqz v1, :cond_0

    .line 668
    const/4 v0, 0x0

    .local v0, index:I
    :goto_0
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    #getter for: Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;
    invoke-static {v2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->access$300(Lcom/youdao/note/ui/viewflow/ViewFlow;)Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 669
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    #getter for: Lcom/youdao/note/ui/viewflow/ViewFlow;->mAdapter:Landroid/widget/Adapter;
    invoke-static {v2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->access$300(Lcom/youdao/note/ui/viewflow/ViewFlow;)Landroid/widget/Adapter;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 670
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    #setter for: Lcom/youdao/note/ui/viewflow/ViewFlow;->mCurrentAdapterIndex:I
    invoke-static {v2, v0}, Lcom/youdao/note/ui/viewflow/ViewFlow;->access$102(Lcom/youdao/note/ui/viewflow/ViewFlow;I)I

    .line 675
    .end local v0           #index:I
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/ui/viewflow/ViewFlow$AdapterDataSetObserver;->this$0:Lcom/youdao/note/ui/viewflow/ViewFlow;

    #calls: Lcom/youdao/note/ui/viewflow/ViewFlow;->resetFocus()V
    invoke-static {v2}, Lcom/youdao/note/ui/viewflow/ViewFlow;->access$400(Lcom/youdao/note/ui/viewflow/ViewFlow;)V

    .line 676
    return-void

    .line 668
    .restart local v0       #index:I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 681
    return-void
.end method
