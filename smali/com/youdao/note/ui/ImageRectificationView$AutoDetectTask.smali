.class Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
.super Landroid/os/AsyncTask;
.source "ImageRectificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/ImageRectificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoDetectTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private showDialog:Z

.field final synthetic this$0:Lcom/youdao/note/ui/ImageRectificationView;


# direct methods
.method private constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;)V
    .locals 0
    .parameter

    .prologue
    .line 225
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 225
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 13
    .parameter "params"

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 252
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 253
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_1

    .line 254
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Lcom/youdao/note/tool/img/ImageProcess;->BitmapToMat(Landroid/graphics/Bitmap;)Lorg/opencv/core/Mat;

    move-result-object v2

    .line 256
    .local v2, matImage:Lorg/opencv/core/Mat;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v0, cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v12, :cond_0

    .line 258
    new-instance v4, Lorg/opencv/core/Point;

    invoke-direct {v4}, Lorg/opencv/core/Point;-><init>()V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    :cond_0
    invoke-static {v0}, Lorg/opencv/utils/Converters;->vector_Point_to_Mat(Ljava/util/List;)Lorg/opencv/core/Mat;

    move-result-object v3

    .line 261
    .local v3, matVertices:Lorg/opencv/core/Mat;
    invoke-virtual {v2}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v4

    invoke-virtual {v3}, Lorg/opencv/core/Mat;->getNativeObjAddr()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/youdao/note/tool/img/ImageProcess;->quadrangleLocation(JJ)V

    .line 262
    invoke-static {v3, v0}, Lorg/opencv/utils/Converters;->Mat_to_vector_Point(Lorg/opencv/core/Mat;Ljava/util/List;)V

    .line 263
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-static {v0}, Lcom/youdao/note/tool/img/ImageProcess;->convertOpenCVPoint(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4, v5}, Lcom/youdao/note/ui/ImageRectificationView;->access$102(Lcom/youdao/note/ui/ImageRectificationView;Ljava/util/List;)Ljava/util/List;

    .line 264
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/youdao/note/tool/img/ImageProcess;->sortVertices(Ljava/util/List;)V

    .line 266
    .end local v0           #cvVertices:Ljava/util/List;,"Ljava/util/List<Lorg/opencv/core/Point;>;"
    .end local v1           #i:I
    .end local v2           #matImage:Lorg/opencv/core/Mat;
    .end local v3           #matVertices:Lorg/opencv/core/Mat;
    :cond_1
    iget-object v5, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    new-array v7, v12, [Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    aput-object v4, v7, v8

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    aput-object v4, v7, v9

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    aput-object v4, v7, v10

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    aput-object v4, v7, v11

    #calls: Lcom/youdao/note/ui/ImageRectificationView;->convertPoints(Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    invoke-static {v6, v9, v7}, Lcom/youdao/note/ui/ImageRectificationView;->access$400(Lcom/youdao/note/ui/ImageRectificationView;Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;

    move-result-object v4

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;
    invoke-static {v5, v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$302(Lcom/youdao/note/ui/ImageRectificationView;[Landroid/graphics/PointF;)[Landroid/graphics/PointF;

    .line 271
    :goto_1
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v4}, Lcom/youdao/note/ui/ImageRectificationView;->postInvalidate()V

    .line 272
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->convertAutoDetectHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$600(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 273
    const/4 v4, 0x0

    return-object v4

    .line 269
    :cond_2
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #calls: Lcom/youdao/note/ui/ImageRectificationView;->setInitialPoints()V
    invoke-static {v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$500(Lcom/youdao/note/ui/ImageRectificationView;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 225
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->showDialog:Z

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "AutoDetectTask over"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->showDialog:Z

    .line 233
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    iput-boolean v1, p0, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->showDialog:Z

    goto :goto_0
.end method
