.class Lcom/youdao/note/ui/ImageRectificationView$1;
.super Landroid/os/Handler;
.source "ImageRectificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/ImageRectificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/ImageRectificationView;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;)V
    .locals 0
    .parameter

    .prologue
    .line 462
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 465
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 501
    :goto_0
    return-void

    .line 467
    :pswitch_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    new-instance v1, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {v1, v2, v4}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;-><init>(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$1;)V

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    invoke-static {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->access$1702(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    .line 468
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$1700(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 471
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #calls: Lcom/youdao/note/ui/ImageRectificationView;->rotate(Z)V
    invoke-static {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->access$1900(Lcom/youdao/note/ui/ImageRectificationView;Z)V

    goto :goto_0

    .line 474
    :pswitch_2
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #calls: Lcom/youdao/note/ui/ImageRectificationView;->rotate(Z)V
    invoke-static {v0, v3}, Lcom/youdao/note/ui/ImageRectificationView;->access$1900(Lcom/youdao/note/ui/ImageRectificationView;Z)V

    goto :goto_0

    .line 477
    :pswitch_3
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    new-instance v1, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {v1, v2, v4}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;-><init>(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$1;)V

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    invoke-static {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->access$2002(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    .line 478
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$2000(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 481
    :pswitch_4
    const-string v0, "Event cancel task"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$1700(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$1700(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;->cancel(Z)Z

    .line 484
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    invoke-static {v0, v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$1702(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$2000(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 487
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    invoke-static {v0}, Lcom/youdao/note/ui/ImageRectificationView;->access$2000(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;->cancel(Z)Z

    .line 488
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    invoke-static {v0, v4}, Lcom/youdao/note/ui/ImageRectificationView;->access$2002(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    .line 491
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 495
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView$1;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 496
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 492
    :catch_1
    move-exception v0

    goto :goto_1

    .line 465
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
