.class public Lcom/youdao/note/ui/ResizeableLayout;
.super Landroid/widget/RelativeLayout;
.source "ResizeableLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;
    }
.end annotation


# instance fields
.field private mHeight:I

.field private mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

    .line 22
    iput v1, p0, Lcom/youdao/note/ui/ResizeableLayout;->mHeight:I

    .line 23
    iput v1, p0, Lcom/youdao/note/ui/ResizeableLayout;->mWidth:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

    .line 22
    iput v1, p0, Lcom/youdao/note/ui/ResizeableLayout;->mHeight:I

    .line 23
    iput v1, p0, Lcom/youdao/note/ui/ResizeableLayout;->mWidth:I

    .line 34
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 5
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

    invoke-virtual {p0}, Lcom/youdao/note/ui/ResizeableLayout;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/youdao/note/ui/ResizeableLayout;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/youdao/note/ui/ResizeableLayout;->mWidth:I

    iget v4, p0, Lcom/youdao/note/ui/ResizeableLayout;->mHeight:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;->onSizeChanged(IIII)V

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/ResizeableLayout;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mHeight:I

    .line 50
    invoke-virtual {p0}, Lcom/youdao/note/ui/ResizeableLayout;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/ResizeableLayout;->mWidth:I

    .line 51
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 52
    return-void
.end method

.method public setSizeChangeListener(Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 37
    iput-object p1, p0, Lcom/youdao/note/ui/ResizeableLayout;->mSizeChangeListener:Lcom/youdao/note/ui/ResizeableLayout$SizeChangeListener;

    .line 38
    return-void
.end method
