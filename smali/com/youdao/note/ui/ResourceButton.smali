.class public Lcom/youdao/note/ui/ResourceButton;
.super Landroid/widget/FrameLayout;
.source "ResourceButton.java"


# instance fields
.field private mResourceCount:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/ResourceButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030042

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 37
    const v0, 0x7f0700a7

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/ResourceButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/ResourceButton;->mResourceCount:Landroid/widget/TextView;

    .line 38
    return-void
.end method


# virtual methods
.method public updateResourceCount(I)V
    .locals 2
    .parameter "count"

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/ResourceButton;->setVisibility(I)V

    .line 49
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/ResourceButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/ResourceButton;->setVisibility(I)V

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/ui/ResourceButton;->mResourceCount:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
