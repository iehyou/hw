.class final Lcom/youdao/note/ui/DialogFactory$1;
.super Ljava/lang/Object;
.source "DialogFactory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/DialogFactory;->getNoteBookPickDialog(Landroid/app/Activity;Ljava/lang/String;Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$callback:Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;

.field final synthetic val$noteBooks:[Lcom/youdao/note/data/NoteBook;

.field final synthetic val$oriNoteBookId:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;Ljava/lang/String;[Lcom/youdao/note/data/NoteBook;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$callback:Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;

    iput-object p3, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$oriNoteBookId:Ljava/lang/String;

    iput-object p4, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$noteBooks:[Lcom/youdao/note/data/NoteBook;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 76
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$activity:Landroid/app/Activity;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    .line 77
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$callback:Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$callback:Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;

    iget-object v1, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$oriNoteBookId:Ljava/lang/String;

    iget-object v2, p0, Lcom/youdao/note/ui/DialogFactory$1;->val$noteBooks:[Lcom/youdao/note/data/NoteBook;

    aget-object v2, v2, p2

    invoke-virtual {v2}, Lcom/youdao/note/data/NoteBook;->getNoteBookId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/ui/DialogFactory$NotebookChooseCallback;->onNoteMoved(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method
