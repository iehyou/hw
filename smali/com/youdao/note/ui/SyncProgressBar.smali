.class public Lcom/youdao/note/ui/SyncProgressBar;
.super Landroid/widget/FrameLayout;
.source "SyncProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;
    }
.end annotation


# static fields
.field private static final MESSAGE_UPDATE:I = 0x1

.field private static final UPDATE_INTERVAL:J = 0x64L


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mListener:Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;

.field private volatile mNextProgress:I

.field private mPercentageView:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/SyncProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/youdao/note/ui/SyncProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Lcom/youdao/note/ui/SyncProgressBar$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/SyncProgressBar$1;-><init>(Lcom/youdao/note/ui/SyncProgressBar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mHandler:Landroid/os/Handler;

    .line 105
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030049

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 106
    const v0, 0x7f0700ae

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/SyncProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    .line 107
    const v0, 0x7f0700af

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/SyncProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mPercentageView:Landroid/widget/TextView;

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/SyncProgressBar;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter "x0"

    .prologue
    .line 26
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/SyncProgressBar;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 26
    iget v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mNextProgress:I

    return v0
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/SyncProgressBar;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/SyncProgressBar;->innerSetProgress(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/SyncProgressBar;)Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 26
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mListener:Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;

    return-object v0
.end method

.method private innerSetProgress(I)V
    .locals 3
    .parameter "p"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 85
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mPercentageView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method


# virtual methods
.method public finishSync(Z)V
    .locals 2
    .parameter "succeed"

    .prologue
    const/4 v1, 0x1

    .line 121
    if-eqz p1, :cond_0

    .line 122
    const/16 v0, 0x64

    iput v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mNextProgress:I

    .line 123
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 128
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 126
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/SyncProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAnimationFinishListener(Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 89
    iput-object p1, p0, Lcom/youdao/note/ui/SyncProgressBar;->mListener:Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;

    .line 90
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .parameter "p"

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/youdao/note/ui/SyncProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/SyncProgressBar;->setVisibility(I)V

    .line 113
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/SyncProgressBar;->innerSetProgress(I)V

    .line 115
    :cond_0
    iput p1, p0, Lcom/youdao/note/ui/SyncProgressBar;->mNextProgress:I

    .line 116
    iget-object v0, p0, Lcom/youdao/note/ui/SyncProgressBar;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 118
    return-void
.end method
