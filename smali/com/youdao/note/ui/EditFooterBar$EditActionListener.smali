.class public interface abstract Lcom/youdao/note/ui/EditFooterBar$EditActionListener;
.super Ljava/lang/Object;
.source "EditFooterBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/EditFooterBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EditActionListener"
.end annotation


# static fields
.field public static final ERROR_AUDIO_PLAY:I = -0x2

.field public static final ERROR_RECORD_INIT:I = -0x1


# virtual methods
.method public abstract onDoodle()V
.end method

.method public abstract onError(I)V
.end method

.method public abstract onHandwrite()V
.end method

.method public abstract onPickPhoto()V
.end method

.method public abstract onRecordFinished(Lcom/youdao/note/data/resource/AudioResourceMeta;)V
.end method

.method public abstract onTakePhoto()V
.end method
