.class Lcom/youdao/note/ui/AsyncImageView$2;
.super Lcom/youdao/note/task/AbstractAsyncRequestTask;
.source "AsyncImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/AsyncImageView;->loadUri(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/youdao/note/task/AbstractAsyncRequestTask",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/AsyncImageView;

.field final synthetic val$uri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/AsyncImageView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/youdao/note/ui/AsyncImageView$2;->this$0:Lcom/youdao/note/ui/AsyncImageView;

    iput-object p4, p0, Lcom/youdao/note/ui/AsyncImageView$2;->val$uri:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/AbstractAsyncRequestTask;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected handleResponse(Ljava/io/InputStream;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method protected bridge synthetic handleResponse(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/AsyncImageView$2;->handleResponse(Ljava/io/InputStream;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onFailed(Ljava/lang/Exception;)V
    .locals 3
    .parameter "e"

    .prologue
    .line 98
    iget-object v0, p0, Lcom/youdao/note/ui/AsyncImageView$2;->this$0:Lcom/youdao/note/ui/AsyncImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load snippet failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/AsyncImageView$2;->val$uri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    return-void
.end method

.method protected onSucceed(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .parameter "img"

    .prologue
    .line 93
    iget-object v0, p0, Lcom/youdao/note/ui/AsyncImageView$2;->this$0:Lcom/youdao/note/ui/AsyncImageView;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/AsyncImageView;->onFinishLoadImage(Landroid/graphics/drawable/Drawable;)V

    .line 94
    return-void
.end method

.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 84
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/AsyncImageView$2;->onSucceed(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
