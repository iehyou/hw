.class final Lcom/youdao/note/ui/DialogFactory$6;
.super Ljava/lang/Object;
.source "DialogFactory.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/DialogFactory;->getDeleteConfirmDialog(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$callback:Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;

.field final synthetic val$deletedNoteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$deletedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    iput-object p3, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$callback:Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 211
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$deletedNoteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/YNoteApplication;->markDeleteNote(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)V

    .line 213
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$6;->val$callback:Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;

    invoke-interface {v0}, Lcom/youdao/note/ui/DialogFactory$NoteDeletedCallback;->onNoteDeleted()V

    .line 214
    return-void
.end method
