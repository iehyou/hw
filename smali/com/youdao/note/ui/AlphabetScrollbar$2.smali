.class Lcom/youdao/note/ui/AlphabetScrollbar$2;
.super Landroid/os/Handler;
.source "AlphabetScrollbar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/AlphabetScrollbar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/AlphabetScrollbar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V
    .locals 0
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar$2;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 114
    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 115
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "alphabet"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, s:Ljava/lang/String;
    const/4 v1, -0x1

    .line 117
    .local v1, index:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-static {}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$400()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 118
    invoke-static {}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$400()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 119
    move v1, v0

    .line 123
    :cond_0
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 124
    iget-object v3, p0, Lcom/youdao/note/ui/AlphabetScrollbar$2;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #calls: Lcom/youdao/note/ui/AlphabetScrollbar;->hightLightAlphabet(I)V
    invoke-static {v3, v1}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$200(Lcom/youdao/note/ui/AlphabetScrollbar;I)V

    .line 125
    iget-object v3, p0, Lcom/youdao/note/ui/AlphabetScrollbar$2;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    invoke-virtual {v3}, Lcom/youdao/note/ui/AlphabetScrollbar;->postInvalidate()V

    .line 128
    .end local v0           #i:I
    .end local v1           #index:I
    .end local v2           #s:Ljava/lang/String;
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 129
    return-void

    .line 117
    .restart local v0       #i:I
    .restart local v1       #index:I
    .restart local v2       #s:Ljava/lang/String;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
