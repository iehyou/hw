.class final Lcom/youdao/note/ui/DialogFactory$4;
.super Ljava/lang/Object;
.source "DialogFactory.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/DialogFactory;->getNoteDetailDialog(Landroid/app/Activity;Ljava/lang/String;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$sourceUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 141
    iput-object p1, p0, Lcom/youdao/note/ui/DialogFactory$4;->val$sourceUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/youdao/note/ui/DialogFactory$4;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 145
    iget-object v0, p0, Lcom/youdao/note/ui/DialogFactory$4;->val$sourceUrl:Ljava/lang/String;

    .line 146
    .local v0, url:Ljava/lang/String;
    iget-object v1, p0, Lcom/youdao/note/ui/DialogFactory$4;->val$sourceUrl:Ljava/lang/String;

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/DialogFactory$4;->val$activity:Landroid/app/Activity;

    #calls: Lcom/youdao/note/ui/DialogFactory;->sendUrl(Landroid/app/Activity;Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/youdao/note/ui/DialogFactory;->access$000(Landroid/app/Activity;Ljava/lang/String;)V

    .line 150
    return-void
.end method
