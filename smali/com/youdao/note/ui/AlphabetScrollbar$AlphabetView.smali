.class Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;
.super Landroid/widget/TextView;
.source "AlphabetScrollbar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/AlphabetScrollbar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlphabetView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/AlphabetScrollbar;


# direct methods
.method public constructor <init>(Lcom/youdao/note/ui/AlphabetScrollbar;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter "alphabet"

    .prologue
    const/4 v3, 0x0

    .line 145
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    .line 146
    #getter for: Lcom/youdao/note/ui/AlphabetScrollbar;->context:Landroid/content/Context;
    invoke-static {p1}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$500(Lcom/youdao/note/ui/AlphabetScrollbar;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 147
    invoke-virtual {p0, p2}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setTag(Ljava/lang/Object;)V

    .line 148
    invoke-virtual {p0, p2}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p0, v3}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setBackgroundColor(I)V

    .line 150
    const-string v0, "#FFFFFF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setTextColor(I)V

    .line 151
    const/4 v0, 0x2

    const/high16 v1, 0x4120

    invoke-virtual {p0, v0, v1}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setTextSize(IF)V

    .line 152
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setGravity(I)V

    .line 153
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/high16 v2, 0x3f80

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    return-void
.end method
