.class public Lcom/youdao/note/ui/EditFooterBar;
.super Landroid/widget/FrameLayout;
.source "EditFooterBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;
.implements Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/EditFooterBar$EditActionListener;
    }
.end annotation


# instance fields
.field private audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

.field private mActionBar:Landroid/view/View;

.field private mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

.field private mContext:Landroid/content/Context;

.field private mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

.field private mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/EditFooterBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    iput-object p1, p0, Lcom/youdao/note/ui/EditFooterBar;->mContext:Landroid/content/Context;

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 81
    const v0, 0x7f07002a

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/EditFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    .line 82
    const v0, 0x7f07002c

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/EditFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/audio/AudioRecordBar;

    iput-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    .line 83
    const v0, 0x7f07002b

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/EditFooterBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/audio/AudioPlayerBar;

    iput-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    .line 88
    invoke-direct {p0}, Lcom/youdao/note/ui/EditFooterBar;->init()V

    .line 89
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    const v1, 0x7f070026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    const v1, 0x7f070027

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    const v1, 0x7f070028

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    const v1, 0x7f070025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v0, p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->setAudioRecordListener(Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;)V

    .line 98
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0, p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->setAudioPlayListener(Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;)V

    .line 99
    return-void
.end method

.method private nofityError(I)V
    .locals 1
    .parameter "errorCode"

    .prologue
    .line 179
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onError(I)V

    .line 182
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 262
    invoke-virtual {p0}, Lcom/youdao/note/ui/EditFooterBar;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZI)V

    .line 265
    :cond_0
    return-void
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 124
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070026

    if-ne v0, v1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    invoke-interface {v0}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onTakePhoto()V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070027

    if-ne v0, v1, :cond_2

    .line 129
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    invoke-interface {v0}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onPickPhoto()V

    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070029

    if-ne v0, v1, :cond_3

    .line 133
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    invoke-interface {v0}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onDoodle()V

    goto :goto_0

    .line 136
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070028

    if-ne v0, v1, :cond_4

    .line 137
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    invoke-interface {v0}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onHandwrite()V

    goto :goto_0

    .line 140
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070025

    if-ne v0, v1, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/youdao/note/ui/EditFooterBar;->startRecord()V

    goto :goto_0
.end method

.method public onClose()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 250
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 253
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .parameter "mp"

    .prologue
    .line 225
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    .line 235
    const/4 v0, 0x0

    return v0
.end method

.method public onRecordFinished(Ljava/io/File;I)V
    .locals 4
    .parameter "file"
    .parameter "nextAction"

    .prologue
    .line 186
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->setVisibility(I)V

    .line 188
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/data/resource/AudioResourceMeta;->setLength(J)V

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    iget-object v2, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-interface {v1, v2}, Lcom/youdao/note/ui/EditFooterBar$EditActionListener;->onRecordFinished(Lcom/youdao/note/data/resource/AudioResourceMeta;)V

    .line 195
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    .line 198
    :cond_2
    const/4 v1, 0x1

    if-ne v1, p2, :cond_3

    .line 200
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v1, p1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->play(Ljava/io/File;)V

    .line 201
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->pause()V

    .line 202
    iget-object v1, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-static {v1}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :goto_0
    return-void

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "Failed to play audio."

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    const/4 v1, -0x2

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/EditFooterBar;->nofityError(I)V

    .line 210
    .end local v0           #e:Ljava/lang/Exception;
    :cond_3
    const/4 v1, 0x2

    if-ne v1, p2, :cond_4

    .line 211
    invoke-virtual {p0}, Lcom/youdao/note/ui/EditFooterBar;->startRecord()V

    goto :goto_0

    .line 214
    :cond_4
    invoke-virtual {p0}, Lcom/youdao/note/ui/EditFooterBar;->onClose()V

    goto :goto_0
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 0
    .parameter "mp"

    .prologue
    .line 246
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->pause()V

    .line 258
    return-void
.end method

.method public play(Ljava/lang/String;)V
    .locals 2
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 106
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->play(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/youdao/note/ui/EditFooterBar;->mPlayerBar:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-static {v0}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 110
    return-void
.end method

.method public recoverPos()V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public setActionListener(Lcom/youdao/note/ui/EditFooterBar$EditActionListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 102
    iput-object p1, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionListener:Lcom/youdao/note/ui/EditFooterBar$EditActionListener;

    .line 103
    return-void
.end method

.method public startRecord()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    .line 147
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->isLoadRecortLibSuccess()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 148
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/youdao/note/ui/EditFooterBar;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a004a

    invoke-static {v3, v4}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 172
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/audio/AudioRecordBar;->setVisibility(I)V

    .line 153
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->mActionBar:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-static {v3}, Lcom/youdao/note/utils/UIUtilities;->makeViewVisiable(Landroid/view/View;)V

    .line 157
    :try_start_0
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    if-nez v3, :cond_1

    .line 158
    invoke-static {}, Lcom/youdao/note/data/resource/ResourceUtils;->genEmptyAudioResource()Lcom/youdao/note/data/resource/AudioResource;

    move-result-object v1

    .line 159
    .local v1, audioResource:Lcom/youdao/note/data/resource/AudioResource;
    invoke-virtual {v1}, Lcom/youdao/note/data/resource/AudioResource;->getMeta()Lcom/youdao/note/data/resource/BaseResourceMeta;

    move-result-object v3

    check-cast v3, Lcom/youdao/note/data/resource/AudioResourceMeta;

    iput-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    .line 161
    .end local v1           #audioResource:Lcom/youdao/note/data/resource/AudioResource;
    :cond_1
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/ui/EditFooterBar;->audioMeta:Lcom/youdao/note/data/resource/AudioResourceMeta;

    invoke-virtual {v3, v4}, Lcom/youdao/note/datasource/DataSource;->getResourcePath(Lcom/youdao/note/data/resource/IResourceMeta;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, absolutePath:Ljava/lang/String;
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->mRecorderBar:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v3, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->init(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    .end local v0           #absolutePath:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 166
    .local v2, e:Ljava/io/IOException;
    const/4 v3, -0x1

    invoke-direct {p0, v3}, Lcom/youdao/note/ui/EditFooterBar;->nofityError(I)V

    goto :goto_0

    .line 170
    .end local v2           #e:Ljava/io/IOException;
    :cond_2
    iget-object v3, p0, Lcom/youdao/note/ui/EditFooterBar;->mContext:Landroid/content/Context;

    const v4, 0x7f0a00f5

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
