.class public Lcom/youdao/note/ui/AsyncImageView;
.super Landroid/widget/ImageView;
.source "AsyncImageView.java"


# instance fields
.field private mTask:Lcom/youdao/note/task/network/GetSnippetTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/AsyncImageView;[B)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/AsyncImageView;->loadImg([B)V

    return-void
.end method

.method private loadImg([B)V
    .locals 3
    .parameter "bytes"

    .prologue
    .line 80
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-static {p1, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AsyncImageView;->onFinishLoadImage(Landroid/graphics/drawable/Drawable;)V

    .line 81
    return-void
.end method


# virtual methods
.method public loadSnippet(Lcom/youdao/note/data/NoteMeta;)V
    .locals 8
    .parameter "noteMeta"

    .prologue
    .line 53
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v6

    .line 54
    .local v6, mYNote:Lcom/youdao/note/YNoteApplication;
    invoke-virtual {v6}, Lcom/youdao/note/YNoteApplication;->getDataSource()Lcom/youdao/note/datasource/DataSource;

    move-result-object v4

    .line 55
    .local v4, dataSource:Lcom/youdao/note/datasource/DataSource;
    invoke-virtual {v4, p1}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v7

    .line 56
    .local v7, snippet:Lcom/youdao/note/data/Snippet;
    if-eqz v7, :cond_1

    .line 57
    iget-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/task/network/GetSnippetTask;->cancel(Z)Z

    .line 60
    :cond_0
    invoke-virtual {v7}, Lcom/youdao/note/data/Snippet;->exist()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-virtual {v7}, Lcom/youdao/note/data/Snippet;->getContentBytes()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/AsyncImageView;->loadImg([B)V

    .line 77
    :cond_1
    :goto_0
    return-void

    .line 63
    :cond_2
    new-instance v0, Lcom/youdao/note/ui/AsyncImageView$1;

    invoke-virtual {v7}, Lcom/youdao/note/data/Snippet;->parseFID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/youdao/note/data/Snippet;->parseVERSION()I

    move-result v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/ui/AsyncImageView$1;-><init>(Lcom/youdao/note/ui/AsyncImageView;Ljava/lang/String;ILcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/NoteMeta;)V

    iput-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    .line 74
    iget-object v0, p0, Lcom/youdao/note/ui/AsyncImageView;->mTask:Lcom/youdao/note/task/network/GetSnippetTask;

    invoke-virtual {v0}, Lcom/youdao/note/task/network/GetSnippetTask;->execute()V

    goto :goto_0
.end method

.method public loadUri(Ljava/lang/String;)V
    .locals 2
    .parameter "uri"

    .prologue
    .line 84
    new-instance v0, Lcom/youdao/note/ui/AsyncImageView$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p1}, Lcom/youdao/note/ui/AsyncImageView$2;-><init>(Lcom/youdao/note/ui/AsyncImageView;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/youdao/note/ui/AsyncImageView$2;->execute()V

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "load snipt image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public onFinishLoadImage(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .parameter "img"

    .prologue
    .line 105
    const-string v0, "async img loaded."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    return-void
.end method
