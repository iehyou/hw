.class Lcom/youdao/note/ui/ImageRectificationView$2;
.super Landroid/os/Handler;
.source "ImageRectificationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/ImageRectificationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/ImageRectificationView;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/ImageRectificationView;)V
    .locals 0
    .parameter

    .prologue
    .line 504
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    const/4 v3, 0x0

    .line 507
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v2}, Lcom/youdao/note/ui/ImageRectificationView;->access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0a00c6

    :goto_0
    invoke-static {v4, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 509
    .local v0, toast:Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 510
    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v2}, Lcom/youdao/note/ui/ImageRectificationView;->access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    #setter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v4, v2}, Lcom/youdao/note/ui/ImageRectificationView;->access$002(Lcom/youdao/note/ui/ImageRectificationView;Z)Z

    .line 511
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/ImageRectificationView;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f070049

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 512
    .local v1, view:Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView$2;->this$0:Lcom/youdao/note/ui/ImageRectificationView;

    #getter for: Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z
    invoke-static {v2}, Lcom/youdao/note/ui/ImageRectificationView;->access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f020079

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 514
    return-void

    .line 507
    .end local v0           #toast:Landroid/widget/Toast;
    .end local v1           #view:Landroid/widget/ImageView;
    :cond_0
    const v2, 0x7f0a00c7

    goto :goto_0

    .restart local v0       #toast:Landroid/widget/Toast;
    :cond_1
    move v2, v3

    .line 510
    goto :goto_1

    .line 512
    .restart local v1       #view:Landroid/widget/ImageView;
    :cond_2
    const v2, 0x7f020078

    goto :goto_2
.end method
