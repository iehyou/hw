.class Lcom/youdao/note/ui/AlphabetScrollbar$1;
.super Ljava/lang/Object;
.source "AlphabetScrollbar.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/AlphabetScrollbar;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/AlphabetScrollbar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .parameter "v"
    .parameter "event"

    .prologue
    const/4 v8, 0x1

    .line 72
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x4

    if-eq v6, v7, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-ne v6, v8, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v8

    .line 75
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 77
    :cond_2
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    invoke-virtual {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->show()V

    .line 78
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    .line 79
    .local v5, y:F
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #getter for: Lcom/youdao/note/ui/AlphabetScrollbar;->scrollbarTop:I
    invoke-static {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$000(Lcom/youdao/note/ui/AlphabetScrollbar;)I

    move-result v6

    int-to-float v6, v6

    sub-float v3, v5, v6

    .line 80
    .local v3, offset:F
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    invoke-virtual {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->getHeight()I

    move-result v6

    int-to-float v1, v6

    .line 81
    .local v1, height:F
    const/high16 v6, 0x41d8

    div-float v0, v1, v6

    .line 82
    .local v0, childHeight:F
    div-float v6, v3, v0

    float-to-int v2, v6

    .line 83
    .local v2, index:I
    if-gez v2, :cond_3

    const/4 v2, 0x0

    .line 84
    :cond_3
    const/16 v6, 0x1a

    if-le v2, v6, :cond_4

    const/16 v2, 0x1a

    .line 85
    :cond_4
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #getter for: Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I
    invoke-static {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$100(Lcom/youdao/note/ui/AlphabetScrollbar;)I

    move-result v6

    if-eq v2, v6, :cond_0

    .line 88
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #calls: Lcom/youdao/note/ui/AlphabetScrollbar;->hightLightAlphabet(I)V
    invoke-static {v6, v2}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$200(Lcom/youdao/note/ui/AlphabetScrollbar;I)V

    .line 89
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #setter for: Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I
    invoke-static {v6, v2}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$102(Lcom/youdao/note/ui/AlphabetScrollbar;I)I

    .line 90
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #getter for: Lcom/youdao/note/ui/AlphabetScrollbar;->listener:Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;
    invoke-static {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$300(Lcom/youdao/note/ui/AlphabetScrollbar;)Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 91
    invoke-static {}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$400()[Ljava/lang/String;

    move-result-object v6

    aget-object v4, v6, v2

    .line 92
    .local v4, s:Ljava/lang/String;
    iget-object v6, p0, Lcom/youdao/note/ui/AlphabetScrollbar$1;->this$0:Lcom/youdao/note/ui/AlphabetScrollbar;

    #getter for: Lcom/youdao/note/ui/AlphabetScrollbar;->listener:Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;
    invoke-static {v6}, Lcom/youdao/note/ui/AlphabetScrollbar;->access$300(Lcom/youdao/note/ui/AlphabetScrollbar;)Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;->onAlphabetSelected(Ljava/lang/String;)V

    goto :goto_0
.end method
