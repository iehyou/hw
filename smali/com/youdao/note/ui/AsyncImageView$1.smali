.class Lcom/youdao/note/ui/AsyncImageView$1;
.super Lcom/youdao/note/task/network/GetSnippetTask;
.source "AsyncImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/youdao/note/ui/AsyncImageView;->loadSnippet(Lcom/youdao/note/data/NoteMeta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/AsyncImageView;

.field final synthetic val$dataSource:Lcom/youdao/note/datasource/DataSource;

.field final synthetic val$noteMeta:Lcom/youdao/note/data/NoteMeta;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/AsyncImageView;Ljava/lang/String;ILcom/youdao/note/datasource/DataSource;Lcom/youdao/note/data/NoteMeta;)V
    .locals 0
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/youdao/note/ui/AsyncImageView$1;->this$0:Lcom/youdao/note/ui/AsyncImageView;

    iput-object p4, p0, Lcom/youdao/note/ui/AsyncImageView$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    iput-object p5, p0, Lcom/youdao/note/ui/AsyncImageView$1;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/task/network/GetSnippetTask;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic onSucceed(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 63
    check-cast p1, [B

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/AsyncImageView$1;->onSucceed([B)V

    return-void
.end method

.method protected onSucceed([B)V
    .locals 3
    .parameter "bytes"

    .prologue
    .line 66
    iget-object v1, p0, Lcom/youdao/note/ui/AsyncImageView$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    iget-object v2, p0, Lcom/youdao/note/ui/AsyncImageView$1;->val$noteMeta:Lcom/youdao/note/data/NoteMeta;

    invoke-virtual {v1, v2}, Lcom/youdao/note/datasource/DataSource;->getSnippet(Lcom/youdao/note/data/NoteMeta;)Lcom/youdao/note/data/Snippet;

    move-result-object v0

    .line 67
    .local v0, snippet:Lcom/youdao/note/data/Snippet;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0, p1}, Lcom/youdao/note/data/Snippet;->setContentBytes([B)V

    .line 69
    iget-object v1, p0, Lcom/youdao/note/ui/AsyncImageView$1;->val$dataSource:Lcom/youdao/note/datasource/DataSource;

    invoke-virtual {v1, v0}, Lcom/youdao/note/datasource/DataSource;->writeSnippet(Lcom/youdao/note/data/Snippet;)Z

    .line 70
    iget-object v1, p0, Lcom/youdao/note/ui/AsyncImageView$1;->this$0:Lcom/youdao/note/ui/AsyncImageView;

    invoke-virtual {v0}, Lcom/youdao/note/data/Snippet;->getContentBytes()[B

    move-result-object v2

    #calls: Lcom/youdao/note/ui/AsyncImageView;->loadImg([B)V
    invoke-static {v1, v2}, Lcom/youdao/note/ui/AsyncImageView;->access$000(Lcom/youdao/note/ui/AsyncImageView;[B)V

    .line 72
    :cond_0
    return-void
.end method
