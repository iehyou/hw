.class public interface abstract Lcom/youdao/note/ui/skitch/ISkitch;
.super Ljava/lang/Object;
.source "ISkitch.java"


# virtual methods
.method public abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getPaintWidth()F
.end method

.method public abstract getPaintWidthRatio()F
.end method

.method public abstract initSkitchMeta(Lcom/youdao/note/ui/skitch/ISkitchMeta;)V
.end method

.method public abstract setPaintWidth(F)V
.end method

.method public abstract trash()V
.end method

.method public abstract undo()V
.end method
