.class public interface abstract Lcom/youdao/note/ui/skitch/trace/ITraceManager;
.super Ljava/lang/Object;
.source "ITraceManager.java"


# virtual methods
.method public abstract addTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V
.end method

.method public abstract clear()V
.end method

.method public abstract copy()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
.end method

.method public abstract drawTraces(Landroid/graphics/Canvas;F)V
.end method

.method public abstract getLeftMost()F
.end method

.method public abstract getRightMost()F
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract onTouch(F)V
.end method

.method public abstract removeLastTrace()V
.end method

.method public abstract removeTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V
.end method
