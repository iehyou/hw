.class public abstract Lcom/youdao/note/ui/skitch/trace/AbstractTrace;
.super Ljava/lang/Object;
.source "AbstractTrace.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/trace/ITrace;


# instance fields
.field private mCurrentX:F

.field private mCurrentY:F

.field private mLastX:F

.field private mLastY:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastX:F

    .line 17
    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastY:F

    .line 18
    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentX:F

    .line 19
    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentY:F

    return-void
.end method

.method private innerTrace(FF)V
    .locals 1
    .parameter "x"
    .parameter "y"

    .prologue
    .line 40
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentX:F

    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastX:F

    .line 41
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentY:F

    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastY:F

    .line 42
    iput p1, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentX:F

    .line 43
    iput p2, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mCurrentY:F

    .line 44
    return-void
.end method


# virtual methods
.method public continueTrace(FF)V
    .locals 0
    .parameter "x"
    .parameter "y"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->innerTrace(FF)V

    .line 29
    return-void
.end method

.method protected getLastX()F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastX:F

    return v0
.end method

.method protected getLastY()F
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->mLastY:F

    return v0
.end method

.method public startTrace(FF)V
    .locals 0
    .parameter "x"
    .parameter "y"

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->innerTrace(FF)V

    .line 24
    return-void
.end method
