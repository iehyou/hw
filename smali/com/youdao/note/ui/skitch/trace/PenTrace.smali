.class public Lcom/youdao/note/ui/skitch/trace/PenTrace;
.super Lcom/youdao/note/ui/skitch/trace/AbstractTrace;
.source "PenTrace.java"


# static fields
.field private static final DEFAULT_PEN_WIDTH:F = 20.0f


# instance fields
.field mPaint:Landroid/graphics/Paint;

.field mPath:Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const/high16 v0, 0x41a0

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;-><init>(F)V

    .line 43
    return-void
.end method

.method public constructor <init>(F)V
    .locals 3
    .parameter "width"

    .prologue
    const/4 v2, 0x1

    .line 30
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;-><init>()V

    .line 27
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    .line 31
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 32
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 34
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 35
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 36
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 37
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 38
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 39
    return-void
.end method


# virtual methods
.method public continueTrace(FF)V
    .locals 6
    .parameter "x"
    .parameter "y"

    .prologue
    const/high16 v5, 0x4000

    .line 63
    const-string v0, "pen tool continue trace called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-super {p0, p1, p2}, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->continueTrace(FF)V

    .line 65
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->getLastX()F

    move-result v1

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->getLastY()F

    move-result v2

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->getLastX()F

    move-result v3

    add-float/2addr v3, p1

    div-float/2addr v3, v5

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->getLastY()F

    move-result v4

    add-float/2addr v4, p2

    div-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 66
    return-void
.end method

.method public bridge synthetic copy()Lcom/youdao/note/ui/skitch/trace/ITrace;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->copy()Lcom/youdao/note/ui/skitch/trace/PenTrace;

    move-result-object v0

    return-object v0
.end method

.method public copy()Lcom/youdao/note/ui/skitch/trace/PenTrace;
    .locals 3

    .prologue
    .line 83
    new-instance v0, Lcom/youdao/note/ui/skitch/trace/PenTrace;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;-><init>()V

    .line 84
    .local v0, trace:Lcom/youdao/note/ui/skitch/trace/PenTrace;
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    .line 85
    new-instance v1, Landroid/graphics/Path;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    invoke-direct {v1, v2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v1, v0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    .line 86
    return-object v0
.end method

.method public drawTrace(Landroid/graphics/Canvas;F)V
    .locals 4
    .parameter "canvas"
    .parameter "scale"

    .prologue
    const/4 v3, 0x1

    .line 70
    const-string v2, "pen on draw called."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v2, Landroid/graphics/PaintFlagsDrawFilter;

    invoke-direct {v2, v3, v3}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 72
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 73
    .local v0, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v0, p2, p2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 74
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    invoke-virtual {v2, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 75
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 76
    .local v1, oldWidth:F
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    mul-float v3, v1, p2

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 77
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 78
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 79
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 50
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    return-void
.end method

.method public setWidth(F)V
    .locals 1
    .parameter "width"

    .prologue
    .line 46
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    return-void
.end method

.method public startTrace(FF)V
    .locals 1
    .parameter "x"
    .parameter "y"

    .prologue
    .line 55
    const-string v0, "pen tool start trace called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1, p2}, Lcom/youdao/note/ui/skitch/trace/AbstractTrace;->startTrace(FF)V

    .line 57
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 58
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/PenTrace;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 59
    return-void
.end method
