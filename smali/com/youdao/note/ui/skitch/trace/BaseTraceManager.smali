.class public Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;
.super Ljava/lang/Object;
.source "BaseTraceManager.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/trace/ITraceManager;


# static fields
.field private static final EMPTY_X:F = -1.0f


# instance fields
.field private leftMost:F

.field private mTraces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/youdao/note/ui/skitch/trace/ITrace;",
            ">;"
        }
    .end annotation
.end field

.field private rightMost:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x4080

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    .line 84
    iput v1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    .line 85
    iput v1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    return-void
.end method


# virtual methods
.method public addTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V
    .locals 1
    .parameter "trace"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    .line 58
    const/high16 v0, -0x4080

    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    iput v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    .line 59
    return-void
.end method

.method public copy()Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;
    .locals 5

    .prologue
    .line 71
    new-instance v2, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;

    invoke-direct {v2}, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;-><init>()V

    .line 72
    .local v2, manager:Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 73
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, v2, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    .line 74
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/trace/ITrace;

    .line 75
    .local v0, e:Lcom/youdao/note/ui/skitch/trace/ITrace;
    iget-object v3, v2, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITrace;->copy()Lcom/youdao/note/ui/skitch/trace/ITrace;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    .end local v0           #e:Lcom/youdao/note/ui/skitch/trace/ITrace;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_0
    iget v3, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    iput v3, v2, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    .line 79
    iget v3, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    iput v3, v2, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    .line 80
    return-object v2
.end method

.method public bridge synthetic copy()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->copy()Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;

    move-result-object v0

    return-object v0
.end method

.method public drawTraces(Landroid/graphics/Canvas;F)V
    .locals 3
    .parameter "canvas"
    .parameter "scale"

    .prologue
    .line 25
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-nez v2, :cond_1

    .line 31
    :cond_0
    return-void

    .line 28
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/ui/skitch/trace/ITrace;

    .line 29
    .local v1, trace:Lcom/youdao/note/ui/skitch/trace/ITrace;
    invoke-interface {v1, p1, p2}, Lcom/youdao/note/ui/skitch/trace/ITrace;->drawTrace(Landroid/graphics/Canvas;F)V

    goto :goto_0
.end method

.method public getLeftMost()F
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    return v0
.end method

.method public getRightMost()F
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    iget v1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 64
    :cond_0
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouch(F)V
    .locals 2
    .parameter "x"

    .prologue
    const/high16 v1, -0x4080

    .line 99
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 100
    :cond_0
    iput p1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->leftMost:F

    .line 102
    :cond_1
    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    .line 103
    :cond_2
    iput p1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->rightMost:F

    .line 106
    :cond_3
    return-void
.end method

.method public removeLastTrace()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 46
    :cond_0
    return-void
.end method

.method public removeTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V
    .locals 1
    .parameter "traceToBeRemoved"

    .prologue
    .line 50
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;->mTraces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    return-void
.end method
