.class public Lcom/youdao/note/ui/skitch/handwrite/NBrush;
.super Ljava/lang/Object;
.source "NBrush.java"


# instance fields
.field private newTexture:[F

.field newVertics:[F

.field private offScreenVertexBuffer:Ljava/nio/FloatBuffer;

.field private offScreenVertices:[F

.field private onScreenVertexBuffer:Ljava/nio/FloatBuffer;

.field private onScreenVertices:[F

.field private texture:[F

.field private textureBuffer:Ljava/nio/FloatBuffer;

.field private textures:[I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-array v1, v2, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    .line 48
    new-array v1, v2, [F

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertices:[F

    .line 55
    new-array v1, v2, [F

    fill-array-data v1, :array_2

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    .line 63
    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textures:[I

    .line 65
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    array-length v1, v1

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    .line 67
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    array-length v1, v1

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newTexture:[F

    .line 82
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 83
    .local v0, byteBuf:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    .line 85
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 86
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 88
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertices:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 89
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 90
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    .line 91
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertices:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 92
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 95
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 96
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 97
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    .line 98
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 99
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 106
    return-void

    .line 41
    nop

    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 48
    :array_1
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 55
    :array_2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private buildMipmap(ILjavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)V
    .locals 7
    .parameter "texidx"
    .parameter "gl"
    .parameter "bitmap"

    .prologue
    const/16 v6, 0xde1

    const/4 v5, 0x1

    .line 275
    const/4 v2, 0x0

    .line 277
    .local v2, level:I
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 278
    .local v1, height:I
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 280
    .local v3, width:I
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textures:[I

    aget v4, v4, p1

    invoke-interface {p2, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 282
    :goto_0
    if-lt v1, v5, :cond_0

    if-lt v3, v5, :cond_0

    .line 284
    const/4 v4, 0x0

    invoke-static {v6, v2, p3, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 287
    if-eq v1, v5, :cond_0

    if-ne v3, v5, :cond_1

    .line 303
    :cond_0
    return-void

    .line 292
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 295
    div-int/lit8 v1, v1, 0x2

    .line 296
    div-int/lit8 v3, v3, 0x2

    .line 297
    invoke-static {p3, v3, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 300
    .local v0, bitmap2:Landroid/graphics/Bitmap;
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->recycle()V

    .line 301
    move-object p3, v0

    .line 302
    goto :goto_0
.end method

.method private loadTextureFromResource(IILjavax/microedition/khronos/opengles/GL10;Landroid/content/Context;Z)V
    .locals 9
    .parameter "texidx"
    .parameter "resourceID"
    .parameter "gl"
    .parameter "context"
    .parameter "useMipmap"

    .prologue
    const/16 v8, 0x2801

    const/16 v7, 0x2800

    const/4 v6, 0x0

    const v5, 0x46180400

    const/16 v4, 0xde1

    .line 222
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 223
    .local v1, is:Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 226
    .local v0, bitmap:Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 230
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 231
    const/4 v1, 0x0

    .line 234
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " * "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textures:[I

    aget v2, v2, p1

    invoke-interface {p3, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 238
    if-eqz p5, :cond_0

    .line 240
    invoke-virtual {p0, p3}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->checkGLError(Ljavax/microedition/khronos/opengles/GL;)V

    .line 241
    const v2, 0x461c0c00

    invoke-interface {p3, v4, v8, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 243
    invoke-virtual {p0, p3}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->checkGLError(Ljavax/microedition/khronos/opengles/GL;)V

    .line 244
    invoke-interface {p3, v4, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 246
    invoke-virtual {p0, p3}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->checkGLError(Ljavax/microedition/khronos/opengles/GL;)V

    .line 255
    invoke-direct {p0, p1, p3, v0}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->buildMipmap(ILjavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)V

    .line 270
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 271
    return-void

    .line 229
    :catchall_0
    move-exception v2

    .line 230
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 231
    const/4 v1, 0x0

    .line 232
    :goto_2
    throw v2

    .line 260
    :cond_0
    invoke-interface {p3, v4, v8, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 262
    invoke-interface {p3, v4, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 266
    invoke-static {v4, v6, v0, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto :goto_1

    .line 232
    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_2
.end method


# virtual methods
.method checkGLError(Ljavax/microedition/khronos/opengles/GL;)V
    .locals 4
    .parameter "gl"

    .prologue
    .line 74
    check-cast p1, Ljavax/microedition/khronos/opengles/GL10;

    .end local p1
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    .line 75
    .local v0, error:I
    if-eqz v0, :cond_0

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLError 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLError 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_0
    return-void
.end method

.method public draw(Ljavax/microedition/khronos/opengles/GL10;FFFFF)V
    .locals 9
    .parameter "gl"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "bgAlpha"
    .parameter "fgAlpha"

    .prologue
    const v8, 0x46180400

    const/high16 v7, 0x4000

    const/4 v6, 0x0

    const/16 v5, 0xde1

    const/4 v4, 0x0

    .line 118
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertices:[F

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertices:[F

    aget v2, v2, v0

    mul-float/2addr v2, p4

    aput v2, v1, v0

    .line 120
    rem-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    aget v2, v1, v0

    div-float v3, p4, v7

    sub-float v3, p2, v3

    add-float/2addr v2, v3

    aput v2, v1, v0

    .line 118
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    aget v2, v1, v0

    div-float v3, p4, v7

    sub-float v3, p3, v3

    add-float/2addr v2, v3

    aput v2, v1, v0

    goto :goto_1

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 127
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 129
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 130
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 134
    const/4 v1, 0x2

    const/16 v2, 0x1406

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->offScreenVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v1, v2, v4, v3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 135
    const/4 v1, 0x2

    const/16 v2, 0x1406

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v1, v2, v4, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 137
    invoke-interface {p1, v6, v6, v6, p5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 138
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textures:[I

    aget v1, v1, v4

    invoke-interface {p1, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 139
    const/16 v1, 0x2801

    invoke-interface {p1, v5, v1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 141
    const/16 v1, 0x2800

    invoke-interface {p1, v5, v1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 143
    const/16 v1, 0x2200

    const/16 v2, 0xbe2

    invoke-interface {p1, v5, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 144
    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-interface {p1, v1, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 156
    return-void
.end method

.method public drawOnScreen(Ljavax/microedition/khronos/opengles/GL10;IIIII)V
    .locals 7
    .parameter "gl"
    .parameter "targetTextuerId"
    .parameter "width"
    .parameter "height"
    .parameter "frameWidth"
    .parameter "frameHeight"

    .prologue
    .line 160
    const-string v3, "Draw on Screen called."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 165
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertices:[F

    aget v4, v4, v0

    aput v4, v3, v0

    .line 166
    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_0

    .line 167
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    aget v4, v3, v0

    int-to-float v5, p3

    mul-float/2addr v4, v5

    aput v4, v3, v0

    .line 164
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_0
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    aget v4, v3, v0

    int-to-float v5, p4

    mul-float/2addr v4, v5

    aput v4, v3, v0

    goto :goto_1

    .line 172
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newVertics:[F

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 173
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 175
    const-wide/high16 v3, 0x3ff0

    int-to-double v5, p3

    mul-double/2addr v3, v5

    int-to-double v5, p5

    div-double/2addr v3, v5

    double-to-float v2, v3

    .line 176
    .local v2, rW:F
    const-wide/high16 v3, 0x3ff0

    int-to-double v5, p4

    mul-double/2addr v3, v5

    int-to-double v5, p6

    div-double/2addr v3, v5

    double-to-float v1, v3

    .line 177
    .local v1, rH:F
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 178
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newTexture:[F

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->texture:[F

    aget v4, v4, v0

    aput v4, v3, v0

    .line 179
    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_2

    .line 180
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newTexture:[F

    aget v4, v3, v0

    mul-float/2addr v4, v2

    aput v4, v3, v0

    .line 177
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 182
    :cond_2
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newTexture:[F

    aget v4, v3, v0

    mul-float/2addr v4, v1

    aput v4, v3, v0

    goto :goto_3

    .line 185
    :cond_3
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->newTexture:[F

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 186
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 189
    const/16 v3, 0x1700

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 190
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 191
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {p1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 192
    const v3, 0x84c0

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glActiveTexture(I)V

    .line 193
    const/16 v3, 0xde1

    invoke-interface {p1, v3, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 194
    const/16 v3, 0xde1

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {p1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 195
    const/4 v3, 0x2

    const/16 v4, 0x1406

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->onScreenVertexBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 196
    const/4 v3, 0x2

    const/16 v4, 0x1406

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textureBuffer:Ljava/nio/FloatBuffer;

    invoke-interface {p1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 197
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-interface {p1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 199
    const-string v3, "Draw on Screen ended."

    invoke-static {p0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public loadGLTexture(Ljavax/microedition/khronos/opengles/GL10;Landroid/content/Context;)V
    .locals 6
    .parameter "gl"
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 213
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->textures:[I

    invoke-interface {p1, v0, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 214
    const v2, 0x7f0200e8

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->loadTextureFromResource(IILjavax/microedition/khronos/opengles/GL10;Landroid/content/Context;Z)V

    .line 216
    return-void
.end method
