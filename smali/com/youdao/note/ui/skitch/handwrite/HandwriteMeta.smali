.class public Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
.super Ljava/lang/Object;
.source "HandwriteMeta.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/ISkitchMeta;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# instance fields
.field private isNew:Z

.field private paintWidth:F

.field private wordWidthList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    .line 25
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getHandWritePaintWidth()F

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->paintWidth:F

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->isNew:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Lcom/youdao/note/ui/skitch/ISkitchMeta;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->copy()Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    move-result-object v0

    return-object v0
.end method

.method public copy()Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
    .locals 6

    .prologue
    .line 72
    new-instance v3, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-direct {v3}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;-><init>()V

    .line 73
    .local v3, meta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;
    iget-boolean v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->isNew:Z

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->setIsNew(Z)V

    .line 74
    iget v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->paintWidth:F

    invoke-virtual {v3, v4}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->setPaintWidth(F)V

    .line 75
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v2, list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/Bitmap;>;"
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 77
    .local v0, e:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    .end local v0           #e:Landroid/graphics/Bitmap;
    :cond_0
    invoke-virtual {v3, v2}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->setWordWidthList(Ljava/util/List;)V

    .line 80
    return-object v3
.end method

.method public getPaintWidth()F
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->paintWidth:F

    return v0
.end method

.method public getWordWidthList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 59
    :cond_0
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNew()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->isNew:Z

    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 64
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 65
    .local v0, b:Landroid/graphics/Bitmap;
    sget-object v2, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->BLANK_CHARACTER:Landroid/graphics/Bitmap;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-eq v0, v2, :cond_0

    .line 66
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 69
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method public setIsNew(Z)V
    .locals 0
    .parameter "b"

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->isNew:Z

    .line 49
    return-void
.end method

.method public setPaintWidth(F)V
    .locals 1
    .parameter "paintWidth"

    .prologue
    .line 39
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/YNoteApplication;->setHandWritePaintWidth(F)V

    .line 40
    iput p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->paintWidth:F

    .line 41
    return-void
.end method

.method public setWordWidthList(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p1, wordWidthList:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    .line 34
    return-void
.end method

.method public trash()V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->onDestroy()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->wordWidthList:Ljava/util/List;

    .line 54
    return-void
.end method
