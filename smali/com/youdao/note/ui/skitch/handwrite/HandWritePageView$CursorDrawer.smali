.class Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;
.super Landroid/os/AsyncTask;
.source "HandWritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CursorDrawer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final INTERVAL:J = 0x1f4L


# instance fields
.field private volatile delay:Z

.field private volatile running:Z

.field final synthetic this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;


# direct methods
.method private constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 340
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 343
    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    .line 344
    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;-><init>(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;)V

    return-void
.end method


# virtual methods
.method public delay()V
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay:Z

    .line 361
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 340
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .parameter "arg0"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 365
    const/4 v0, 0x1

    .line 366
    .local v0, r:Z
    :goto_0
    iget-boolean v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    if-eqz v3, :cond_2

    .line 367
    iget-boolean v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay:Z

    if-nez v3, :cond_0

    .line 368
    new-array v3, v1, [Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p0, v3}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->publishProgress([Ljava/lang/Object;)V

    .line 369
    if-nez v0, :cond_1

    move v0, v1

    .line 371
    :cond_0
    :goto_1
    iput-boolean v2, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay:Z

    .line 373
    const-wide/16 v3, 0x1f4

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 374
    :catch_0
    move-exception v3

    goto :goto_0

    :cond_1
    move v0, v2

    .line 369
    goto :goto_1

    .line 378
    :cond_2
    const/4 v1, 0x0

    return-object v1
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Boolean;)V
    .locals 2
    .parameter "values"

    .prologue
    .line 383
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    #calls: Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->drawCursor(Z)V
    invoke-static {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->access$100(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;Z)V

    .line 385
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 340
    check-cast p1, [Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->onProgressUpdate([Ljava/lang/Boolean;)V

    return-void
.end method

.method public startDrawing()V
    .locals 1

    .prologue
    .line 347
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    if-nez v0, :cond_0

    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    .line 349
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 351
    :cond_0
    return-void
.end method

.method public stopDrawing()V
    .locals 1

    .prologue
    .line 353
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    if-eqz v0, :cond_0

    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->running:Z

    .line 355
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->cancel(Z)Z

    .line 357
    :cond_0
    return-void
.end method
