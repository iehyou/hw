.class public Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;
.super Landroid/view/View;
.source "HandwriteViewHeader.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# instance fields
.field private final topLine:Landroid/graphics/Bitmap;

.field private final topWidget:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topWidget:Landroid/graphics/Bitmap;

    .line 25
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topWidget:Landroid/graphics/Bitmap;

    .line 25
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topWidget:Landroid/graphics/Bitmap;

    .line 25
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    .line 28
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .parameter "canvas"

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x0

    .line 40
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 41
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getWidth()I

    move-result v4

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->HEADER_W:F

    float-to-int v5, v5

    div-int v0, v4, v5

    .line 42
    .local v0, count:I
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getWidth()I

    move-result v4

    div-int/2addr v4, v0

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topWidget:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v2, v4

    .line 44
    .local v2, pad:F
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 45
    .local v3, paint:Landroid/graphics/Paint;
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 48
    const/high16 v4, 0x4000

    div-float v1, v2, v4

    .local v1, index:F
    :goto_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 49
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topWidget:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v1, v5, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 48
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getWidth()I

    move-result v4

    div-int/2addr v4, v0

    int-to-float v4, v4

    add-float/2addr v1, v4

    goto :goto_0

    .line 51
    :cond_0
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v6, Landroid/graphics/RectF;

    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->SPACING:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->topLine:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getWidth()I

    move-result v9

    sget v10, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->SPACING:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteViewHeader;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v4, v5, v6, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 53
    return-void
.end method
