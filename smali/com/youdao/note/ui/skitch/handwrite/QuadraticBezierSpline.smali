.class public Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;
.super Ljava/lang/Object;
.source "QuadraticBezierSpline.java"


# instance fields
.field private mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mStarted:Z

    .line 18
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 19
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 20
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 21
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    return-void
.end method

.method private get(DDDD)F
    .locals 6
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "t"

    .prologue
    const-wide/high16 v4, 0x4000

    .line 63
    mul-double v0, v4, p3

    mul-double v2, v4, p1

    sub-double/2addr v0, v2

    mul-double/2addr v0, p7

    add-double/2addr v0, p1

    mul-double v2, v4, p3

    sub-double v2, p1, v2

    add-double/2addr v2, p5

    mul-double/2addr v2, p7

    mul-double/2addr v2, p7

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private getWidth(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v3, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v5, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->get(DDDD)F

    move-result v0

    return v0
.end method

.method private getX(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 51
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v5, v0

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->get(DDDD)F

    move-result v0

    return v0
.end method

.method private getY(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 55
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v5, v0

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->get(DDDD)F

    move-result v0

    return v0
.end method


# virtual methods
.method public addPoint(FFD)V
    .locals 7
    .parameter "x"
    .parameter "y"
    .parameter "width"

    .prologue
    const/high16 v4, 0x4000

    .line 29
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mStarted:Z

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 31
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 32
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 33
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mStarted:Z

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mFirstPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;)V

    .line 37
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;)V

    .line 38
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mSecondPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v1, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v2, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    add-float/2addr v1, v2

    div-float/2addr v1, v4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v2, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v3, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    add-float/2addr v2, v3

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mControlPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v3, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    add-double/2addr v3, v5

    const-wide/high16 v5, 0x4000

    div-double/2addr v3, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 41
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mNextPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 42
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mStarted:Z

    .line 26
    return-void
.end method

.method public getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V
    .locals 4
    .parameter "point"
    .parameter "t"

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->mStarted:Z

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->getX(D)F

    move-result v0

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->getY(D)F

    move-result v1

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->getWidth(D)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 48
    :cond_0
    return-void
.end method
