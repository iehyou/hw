.class public Lcom/youdao/note/ui/skitch/handwrite/WriteView2;
.super Landroid/view/View;
.source "WriteView2.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IWrite;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# static fields
.field private static final MAX_WIDTH:I = 0x28

.field private static final MAX_WIDTH_STEP:D = 0.1

.field private static final STAND_WIDTH:I = 0x14


# instance fields
.field private dstRect:Landroid/graphics/Rect;

.field private mBackGroundPaint:Landroid/graphics/Paint;

.field private mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mDensity:F

.field private mDot:Landroid/graphics/Bitmap;

.field private mDot2:Landroid/graphics/Bitmap;

.field private mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mForeGroundPaint:Landroid/graphics/Paint;

.field private mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

.field private mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mLastSpeed:D

.field private mMaxWidth:F

.field private mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

.field private mPaint:Landroid/graphics/Paint;

.field private mStandWidth:F

.field private matrix:Landroid/graphics/Matrix;

.field private tmpBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v1, 0x5

    .line 72
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mPaint:Landroid/graphics/Paint;

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mForeGroundPaint:Landroid/graphics/Paint;

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBackGroundPaint:Landroid/graphics/Paint;

    .line 54
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

    .line 55
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->matrix:Landroid/graphics/Matrix;

    .line 108
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    .line 110
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 111
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastSpeed:D

    .line 73
    const-string v0, "WriteView2 created"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method private drawPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;)V
    .locals 10
    .parameter "point"

    .prologue
    const/4 v9, 0x0

    const-wide v7, 0x3fe999999999999aL

    const-wide/high16 v5, 0x4000

    .line 207
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    mul-double/2addr v3, v7

    sub-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 208
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    mul-double/2addr v3, v7

    sub-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 209
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    mul-double/2addr v3, v7

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 210
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    mul-double/2addr v3, v7

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 211
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDot2:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mForeGroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 213
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    sub-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 214
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    sub-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 215
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 216
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget v1, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v1

    iget-wide v3, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 217
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDot:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBackGroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 218
    return-void
.end method

.method private drawToPoint(FFJ)V
    .locals 27
    .parameter "x"
    .parameter "y"
    .parameter "time"

    .prologue
    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    .line 135
    new-instance v21, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v0, v21

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>(FF)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-wide/from16 v0, p3

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->time:J

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mStandWidth:F

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v23

    mul-float v22, v22, v23

    const/high16 v23, 0x4000

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    .line 141
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    move/from16 v21, v0

    sub-float v4, p1, v21

    .line 142
    .local v4, diffX:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    move/from16 v21, v0

    sub-float v5, p2, v21

    .line 143
    .local v5, diffY:F
    mul-float v21, v4, v4

    mul-float v22, v5, v5

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v23, v0

    div-double v21, v21, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    move/from16 v23, v0

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v23, v0

    div-double v6, v21, v23

    .line 146
    .local v6, dis:D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->time:J

    move-wide/from16 v21, v0

    cmp-long v21, p3, v21

    if-nez v21, :cond_4

    .line 147
    const-wide/high16 v13, 0x3ff0

    .line 151
    .local v13, speed:D
    :goto_0
    const-wide v21, 0x3fe6666666666666L

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastSpeed:D

    move-wide/from16 v23, v0

    mul-double v21, v21, v23

    const-wide v23, 0x3fd3333333333333L

    mul-double v23, v23, v13

    add-double v13, v21, v23

    .line 152
    move-object/from16 v0, p0

    iput-wide v13, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastSpeed:D

    .line 153
    double-to-int v3, v6

    .line 154
    .local v3, diff:I
    if-nez v3, :cond_1

    .line 155
    const/4 v3, 0x1

    .line 158
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mStandWidth:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v22

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x4008

    mul-double v21, v21, v23

    const-wide/high16 v23, 0x3fe0

    add-double v23, v23, v13

    div-double v11, v21, v23

    .line 160
    .local v11, nextWidth:D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mMaxWidth:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    cmpl-double v21, v11, v21

    if-lez v21, :cond_2

    .line 161
    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mMaxWidth:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v11, v0

    .line 163
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    move-wide/from16 v21, v0

    sub-double v21, v11, v21

    int-to-double v0, v3

    move-wide/from16 v23, v0

    div-double v15, v21, v23

    .line 164
    .local v15, stepWidth:D
    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->abs(D)D

    move-result-wide v21

    const-wide v23, 0x3fb999999999999aL

    cmpl-double v21, v21, v23

    if-lez v21, :cond_3

    .line 165
    const-wide/16 v21, 0x0

    cmpl-double v21, v15, v21

    if-lez v21, :cond_5

    const-wide v15, 0x3fb999999999999aL

    .line 167
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    move-wide/from16 v21, v0

    int-to-double v0, v3

    move-wide/from16 v23, v0

    mul-double v23, v23, v15

    add-double v11, v21, v23

    .line 168
    int-to-float v0, v3

    move/from16 v21, v0

    div-float v21, v4, v21

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v17, v0

    .line 169
    .local v17, stepX:D
    int-to-float v0, v3

    move/from16 v21, v0

    div-float v21, v5, v21

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v19, v0

    .line 171
    .local v19, stepY:D
    const-string v21, "Write"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "speed is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\n next width is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\n diff is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v11, v12}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->addPoint(FFD)V

    .line 177
    const/high16 v9, -0x4080

    .local v9, lastCx:F
    const/high16 v10, -0x4080

    .line 178
    .local v10, lastCy:F
    const/4 v8, 0x0

    .local v8, i:I
    :goto_2
    if-gt v8, v3, :cond_8

    .line 181
    move-object/from16 v0, p0

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide v23, 0x3feccccccccccccdL

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_6

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    int-to-double v0, v8

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x3ff0

    mul-double v23, v23, v25

    int-to-double v0, v3

    move-wide/from16 v25, v0

    div-double v23, v23, v25

    invoke-virtual/range {v21 .. v24}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V

    .line 189
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    move/from16 v21, v0

    cmpl-float v21, v21, v9

    if-nez v21, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    move/from16 v21, v0

    cmpl-float v21, v21, v10

    if-nez v21, :cond_7

    .line 178
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 149
    .end local v3           #diff:I
    .end local v8           #i:I
    .end local v9           #lastCx:F
    .end local v10           #lastCy:F
    .end local v11           #nextWidth:D
    .end local v13           #speed:D
    .end local v15           #stepWidth:D
    .end local v17           #stepX:D
    .end local v19           #stepY:D
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->time:J

    move-wide/from16 v21, v0

    sub-long v21, p3, v21

    move-wide/from16 v0, v21

    long-to-double v0, v0

    move-wide/from16 v21, v0

    div-double v13, v6, v21

    .restart local v13       #speed:D
    goto/16 :goto_0

    .line 165
    .restart local v3       #diff:I
    .restart local v11       #nextWidth:D
    .restart local v15       #stepWidth:D
    :cond_5
    const-wide v15, -0x4046666666666666L

    goto/16 :goto_1

    .line 184
    .restart local v8       #i:I
    .restart local v9       #lastCx:F
    .restart local v10       #lastCy:F
    .restart local v17       #stepX:D
    .restart local v19       #stepY:D
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    int-to-double v0, v8

    move-wide/from16 v24, v0

    mul-double v24, v24, v17

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    move/from16 v22, v0

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    int-to-double v0, v8

    move-wide/from16 v24, v0

    mul-double v24, v24, v19

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    move-wide/from16 v22, v0

    int-to-double v0, v8

    move-wide/from16 v24, v0

    mul-double v24, v24, v15

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    goto/16 :goto_3

    .line 192
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v9, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v10, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->drawPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->dstRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_4

    .line 200
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-wide/from16 v0, p3

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->time:J

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mLastPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iput-wide v11, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    .line 204
    return-void
.end method


# virtual methods
.method public finishWrite()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V
    .locals 8
    .parameter "getBitmapCallback"

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->matrix:Landroid/graphics/Matrix;

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 234
    .local v7, smallWord:Landroid/graphics/Bitmap;
    invoke-interface {p1, v7}, Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;->onBitmapObtained(Landroid/graphics/Bitmap;)V

    .line 235
    return-void
.end method

.method public init(FII)V
    .locals 5
    .parameter "density"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 77
    iput p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    .line 78
    const/high16 v1, 0x41a0

    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mStandWidth:F

    .line 79
    const/high16 v1, 0x4220

    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mMaxWidth:F

    .line 80
    const-string v1, "Write"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "density is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDensity:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    .line 83
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->tmpBitmap:Landroid/graphics/Bitmap;

    .line 85
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->tmpBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    .line 86
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 88
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 89
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 90
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200e8

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDot:Landroid/graphics/Bitmap;

    .line 92
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200e7

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mDot2:Landroid/graphics/Bitmap;

    .line 94
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mForeGroundPaint:Landroid/graphics/Paint;

    const/16 v2, 0xc8

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 95
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBackGroundPaint:Landroid/graphics/Paint;

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 96
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    const-wide/high16 v1, 0x3ff0

    sget v3, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->WORD_HEIGHT:I

    int-to-double v3, v3

    mul-double/2addr v1, v3

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    double-to-float v0, v1

    .line 99
    .local v0, scale:F
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 100
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .parameter "canvas"

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 106
    return-void
.end method

.method public onFinishWrite()V
    .locals 3

    .prologue
    .line 239
    const-string v0, "OnFinishtWrite called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 241
    return-void
.end method

.method public onStartWrite()V
    .locals 2

    .prologue
    .line 245
    const-string v0, "OnStartWrite called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 247
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->invalidate()V

    .line 248
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .parameter "event"

    .prologue
    const/4 v4, 0x1

    .line 119
    const-string v0, "on Touch for writeview2 called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->touch()V

    .line 121
    invoke-static {p1}, Lcom/youdao/note/utils/CompatUtils;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline;->clear()V

    .line 124
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->drawToPoint(FFJ)V

    .line 126
    invoke-static {p1}, Lcom/youdao/note/utils/CompatUtils;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 127
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->drawToPoint(FFJ)V

    .line 130
    :cond_1
    return v4
.end method

.method public setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
    .locals 0
    .parameter "handWriteCanvas"

    .prologue
    .line 225
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 226
    return-void
.end method

.method public setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter "monitor"

    .prologue
    .line 114
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView2;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    .line 115
    return-void
.end method
