.class public Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;
.super Ljava/lang/Object;
.source "QuadraticBezierSpline2.java"


# instance fields
.field private mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 5
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 6
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 7
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    return-void
.end method

.method private get(DDDD)F
    .locals 6
    .parameter "a"
    .parameter "b"
    .parameter "c"
    .parameter "t"

    .prologue
    const-wide/high16 v4, 0x4000

    .line 69
    mul-double v0, v4, p3

    mul-double v2, v4, p1

    sub-double/2addr v0, v2

    mul-double/2addr v0, p7

    add-double/2addr v0, p1

    mul-double v2, v4, p3

    sub-double v2, p1, v2

    add-double/2addr v2, p5

    mul-double/2addr v2, p7

    mul-double/2addr v2, p7

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private getWidth(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 65
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v3, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v5, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->get(DDDD)F

    move-result v0

    return v0
.end method

.method private getX(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    float-to-double v5, v0

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->get(DDDD)F

    move-result v0

    return v0
.end method

.method private getY(D)F
    .locals 9
    .parameter "t"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v0, v0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    float-to-double v5, v0

    move-object v0, p0

    move-wide v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->get(DDDD)F

    move-result v0

    return v0
.end method


# virtual methods
.method public AddNode(FFF)V
    .locals 10
    .parameter "x"
    .parameter "y"
    .parameter "w"

    .prologue
    const/high16 v9, 0x4000

    .line 29
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v6, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v7, v7, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 30
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v6, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v7, v7, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 32
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    add-float/2addr v4, p1

    div-float v2, v4, v9

    .line 33
    .local v2, midx:F
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    add-float/2addr v4, p2

    div-float v3, v4, v9

    .line 34
    .local v3, midy:F
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    float-to-double v6, p3

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000

    div-double v0, v4, v6

    .line 36
    .local v0, midw:D
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 38
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    float-to-double v5, p3

    invoke-virtual {v4, p1, p2, v5, v6}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 39
    return-void
.end method

.method public End()V
    .locals 10

    .prologue
    const/high16 v9, 0x4000

    .line 43
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v6, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v7, v7, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 45
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    add-float/2addr v4, v5

    div-float v2, v4, v9

    .line 46
    .local v2, midx:F
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    add-float/2addr v4, v5

    div-float v3, v4, v9

    .line 47
    .local v3, midy:F
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v4, v4, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v6, v6, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000

    div-double v0, v4, v6

    .line 48
    .local v0, midw:D
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 49
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v5, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v6, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v7, v7, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 50
    return-void
.end method

.method public Init(FFDFFD)V
    .locals 12
    .parameter "srcx"
    .parameter "srcy"
    .parameter "srcw"
    .parameter "dstx"
    .parameter "dsty"
    .parameter "dstw"

    .prologue
    .line 14
    iget-object v8, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mSrcKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move-wide v0, p3

    invoke-virtual {v8, p1, p2, v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 15
    add-float v8, p1, p5

    const/high16 v9, 0x4000

    div-float v6, v8, v9

    .line 16
    .local v6, midx:F
    add-float v8, p2, p6

    const/high16 v9, 0x4000

    div-float v7, v8, v9

    .line 17
    .local v7, midy:F
    add-double v8, p3, p7

    const-wide/high16 v10, 0x4000

    div-double v4, v8, v10

    .line 18
    .local v4, midw:D
    iget-object v8, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mDstKnot:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v8, v6, v7, v4, v5}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 19
    add-float v8, v6, p1

    const/high16 v9, 0x4000

    div-float v6, v8, v9

    .line 20
    add-float v8, v7, p2

    const/high16 v9, 0x4000

    div-float v7, v8, v9

    .line 21
    add-double v8, v4, p3

    const-wide/high16 v10, 0x4000

    div-double v4, v8, v10

    .line 22
    iget-object v8, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v8, v6, v7, v4, v5}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 24
    iget-object v8, p0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->mNextControl:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    move/from16 v0, p5

    move/from16 v1, p6

    move-wide/from16 v2, p7

    invoke-virtual {v8, v0, v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 25
    return-void
.end method

.method public getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V
    .locals 4
    .parameter "point"
    .parameter "t"

    .prologue
    .line 53
    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getX(D)F

    move-result v0

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getY(D)F

    move-result v1

    invoke-direct {p0, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getWidth(D)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 54
    return-void
.end method
