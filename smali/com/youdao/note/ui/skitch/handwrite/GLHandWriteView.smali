.class public Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
.super Landroid/opengl/GLSurfaceView;
.source "GLHandWriteView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IWrite;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;,
        Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;,
        Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    }
.end annotation


# static fields
.field private static final BEGIN_COUNT:I = 0x3

.field private static final BG_ALPHA:F = 0.6f

.field private static final FG_ALPHA:F = 0.8f

.field private static final STAND_WIDTH:I = 0x14


# instance fields
.field private charCount:I

.field private character:Landroid/graphics/Bitmap;

.field private mDensity:F

.field private mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

.field private mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

.field private mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

.field private mStandWidth:F

.field private monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    .line 44
    iput-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 121
    iput v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->charCount:I

    .line 222
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;-><init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    .line 65
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-direct {v0, p0, p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;-><init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    .line 66
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 67
    invoke-virtual {p0, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->setRenderMode(I)V

    .line 69
    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    return-object v0
.end method

.method static synthetic access$102(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 40
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    return-object p1
.end method

.method static synthetic access$202(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 40
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F

    return v0
.end method

.method static synthetic access$500(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    return-object v0
.end method

.method static synthetic access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 40
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F

    return v0
.end method


# virtual methods
.method public finishWrite()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->erase()V

    .line 176
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->requestRender()V

    .line 177
    return-void
.end method

.method public getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V
    .locals 8
    .parameter "getBitmapCallback"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 125
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->finishWrite()V

    .line 126
    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->charCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->charCount:I

    .line 127
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->requestRender()V

    .line 128
    monitor-enter p0

    .line 130
    const-wide/16 v2, 0xbb8

    :try_start_0
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 135
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " * "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    invoke-interface {p1, v2}, Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;->onBitmapObtained(Landroid/graphics/Bitmap;)V

    .line 137
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;

    .line 145
    :goto_1
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->isShowHandwriteModeTips()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getLogRecorder()Lcom/youdao/note/LogRecorder;

    move-result-object v1

    .line 147
    .local v1, recorder:Lcom/youdao/note/LogRecorder;
    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->charCount:I

    const/4 v3, 0x4

    if-le v2, v3, :cond_2

    .line 148
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/youdao/note/YNoteApplication;->setShowHandwriteModeTips(Z)V

    .line 149
    invoke-virtual {v1, v7}, Lcom/youdao/note/LogRecorder;->setHandWriteMode(I)V

    .line 159
    .end local v1           #recorder:Lcom/youdao/note/LogRecorder;
    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->reset()V

    .line 161
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->resetState()V

    .line 162
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->erase()V

    .line 163
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->requestRender()V

    .line 164
    return-void

    .line 133
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 140
    :cond_1
    const-string v2, "failed to get bitmap."

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 150
    .restart local v1       #recorder:Lcom/youdao/note/LogRecorder;
    :cond_2
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getCostTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 151
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/activity/resource/HandwritingActivity;

    .line 152
    .local v0, activity:Lcom/youdao/note/activity/resource/HandwritingActivity;
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/youdao/note/activity/resource/HandwritingActivity;->showDialog(I)V

    .line 153
    invoke-virtual {v1, v6}, Lcom/youdao/note/LogRecorder;->setHandWriteMode(I)V

    goto :goto_2

    .line 155
    .end local v0           #activity:Lcom/youdao/note/activity/resource/HandwritingActivity;
    :cond_3
    invoke-virtual {v1, v7}, Lcom/youdao/note/LogRecorder;->setHandWriteMode(I)V

    goto :goto_2

    .line 131
    .end local v1           #recorder:Lcom/youdao/note/LogRecorder;
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method public getCostTime()J
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;-><init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->getAvgCost()J

    move-result-wide v0

    return-wide v0
.end method

.method public init(FII)V
    .locals 2
    .parameter "density"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 72
    iput p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F

    .line 74
    const/high16 v0, 0x41a0

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F

    .line 77
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->resetState()V

    .line 78
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->requestRender()V

    .line 81
    return-void
.end method

.method public onFinishWrite()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public onStartWrite()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "event"

    .prologue
    .line 96
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->touch()V

    .line 102
    invoke-static {p1}, Lcom/youdao/note/utils/CompatUtils;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->requestRender()V

    .line 117
    const/4 v0, 0x1

    return v0

    .line 104
    :pswitch_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->reset()V

    .line 105
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->add(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->add(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 111
    :pswitch_2
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mRenderer:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->add(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
    .locals 0
    .parameter "handWriteCanvas"

    .prologue
    .line 85
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 86
    return-void
.end method

.method public setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter "monitor"

    .prologue
    .line 90
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    .line 91
    return-void
.end method
