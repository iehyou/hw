.class public Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;
.super Landroid/widget/FrameLayout;
.source "WriteViewLayout.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IWrite;


# instance fields
.field private mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

.field private mDoneView:Landroid/view/View;

.field private mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

.field private mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v2, 0x1

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03004d

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 53
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getHandwriteMode()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 56
    const v0, 0x7f0700b2

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    const v0, 0x7f0700b3

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mDoneView:Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mDoneView:Landroid/view/View;

    new-instance v1, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout$1;-><init>(Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-void

    .line 60
    :cond_0
    const v0, 0x7f0700b1

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    goto :goto_0
.end method


# virtual methods
.method public finishWrite()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->writeStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->finishWrite()V

    .line 110
    :cond_0
    return-void
.end method

.method public getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V
    .locals 1
    .parameter "getBitmapCallback"

    .prologue
    .line 122
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V

    .line 123
    return-void
.end method

.method public init(FII)V
    .locals 1
    .parameter "density"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 87
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->init(FII)V

    .line 88
    return-void
.end method

.method public onFinishWrite()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->onFinishWrite()V

    .line 128
    const-string v0, "Before GONE"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->setVisibility(I)V

    .line 130
    const-string v0, "After GONE"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->invalidateCanvas()V

    .line 132
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->startCursorDrawer()V

    .line 133
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    instance-of v1, v1, Landroid/opengl/GLSurfaceView;

    if-eqz v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    .line 82
    .local v0, view:Landroid/opengl/GLSurfaceView;
    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 84
    .end local v0           #view:Landroid/opengl/GLSurfaceView;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 73
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    instance-of v1, v1, Landroid/opengl/GLSurfaceView;

    if-eqz v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    .line 75
    .local v0, view:Landroid/opengl/GLSurfaceView;
    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 77
    .end local v0           #view:Landroid/opengl/GLSurfaceView;
    :cond_0
    return-void
.end method

.method public onStartWrite()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->stopCursorDrawer()V

    .line 138
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->onStartWrite()V

    .line 139
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "event"

    .prologue
    .line 92
    const-string v0, "on Touch for writeviewlayout called."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
    .locals 2
    .parameter "handWriteCanvas"

    .prologue
    .line 114
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 115
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-direct {v0, p1, p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;-><init>(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;Lcom/youdao/note/ui/skitch/handwrite/IWrite;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    .line 116
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V

    .line 117
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V

    .line 118
    return-void
.end method

.method public setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 1
    .parameter "monitor"

    .prologue
    .line 143
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V

    .line 144
    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .parameter "visibility"

    .prologue
    .line 148
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 149
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mDoneView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mDoneView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :cond_0
    return-void
.end method

.method public switchToNormalMode()V
    .locals 3

    .prologue
    const v2, 0x7f0700b1

    .line 98
    const v0, 0x7f0700b2

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 99
    invoke-virtual {p0, v2}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-virtual {p0, v2}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mWriteView:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    .line 101
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteViewLayout;->setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V

    .line 102
    return-void
.end method
