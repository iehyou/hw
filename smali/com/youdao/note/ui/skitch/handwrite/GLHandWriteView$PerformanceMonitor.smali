.class Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
.super Ljava/lang/Object;
.source "GLHandWriteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerformanceMonitor"
.end annotation


# instance fields
.field private cost:J

.field private lastTime:J

.field private max:J

.field private min:J

.field private n:J

.field final synthetic this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;


# direct methods
.method private constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)V
    .locals 2
    .parameter

    .prologue
    .line 187
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J

    .line 191
    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->max:J

    .line 192
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->min:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;-><init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized getAvgCost()J
    .locals 6

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->n:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    .line 214
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->cost:J

    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->max:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->min:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->n:J

    const-wide/16 v4, 0x2

    sub-long/2addr v2, v4

    div-long/2addr v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recordTime(J)V
    .locals 6
    .parameter "now"

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 196
    iput-wide p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J

    .line 197
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->cost:J

    .line 198
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->n:J

    .line 199
    const-wide/high16 v2, -0x8000

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->max:J

    .line 200
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->min:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :goto_0
    monitor-exit p0

    return-void

    .line 202
    :cond_0
    :try_start_1
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J

    sub-long v0, p1, v2

    .line 203
    .local v0, time:J
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->max:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->max:J

    .line 204
    :cond_1
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->min:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->min:J

    .line 205
    :cond_2
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->cost:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->cost:J

    .line 206
    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->n:J

    .line 208
    iput-wide p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    .end local v0           #time:J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized reset()V
    .locals 2

    .prologue
    .line 218
    monitor-enter p0

    const-wide/16 v0, -0x1

    :try_start_0
    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->lastTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    monitor-exit p0

    return-void

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
