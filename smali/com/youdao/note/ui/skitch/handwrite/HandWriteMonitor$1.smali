.class Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$1;
.super Ljava/lang/Object;
.source "HandWriteMonitor.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$1;->this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapObtained(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter "reference"

    .prologue
    .line 36
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$1;->this$0:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    invoke-static {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->access$000(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->addCharacter(Landroid/graphics/Bitmap;)V

    .line 39
    :cond_0
    return-void
.end method
