.class public Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;
.super Landroid/view/View;
.source "HandWritePageView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$1;,
        Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;
    }
.end annotation


# instance fields
.field private SCREEN_HEIGHT:I

.field private SCREEN_WIDTH:I

.field private cursorColor:I

.field private cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

.field private firstDraw:Z

.field private handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

.field private isWritting:Z

.field private mDensity:D

.field private paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    .line 37
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->firstDraw:Z

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->isWritting:Z

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    .line 66
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    .line 37
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->firstDraw:Z

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->isWritting:Z

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    .line 71
    return-void
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->drawCursor(Z)V

    return-void
.end method

.method private calCursorPosition()Landroid/graphics/Point;
    .locals 7

    .prologue
    .line 210
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v4}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->getWordWidthList()Ljava/util/List;

    move-result-object v3

    .line 211
    .local v3, wordWidthList:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/Bitmap;>;"
    new-instance v0, Landroid/graphics/Point;

    sget v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->PADDING_TOP:I

    invoke-direct {v0, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 212
    .local v0, cursor:Landroid/graphics/Point;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 213
    .local v1, e:Landroid/graphics/Bitmap;
    sget-object v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-ne v1, v4, :cond_0

    .line 214
    sget v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    sget v6, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 216
    :cond_0
    iget v4, v0, Landroid/graphics/Point;->x:I

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v4, v5

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    if-le v4, v5, :cond_1

    .line 217
    sget v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/graphics/Point;->y:I

    sget v6, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 219
    :cond_1
    iget v4, v0, Landroid/graphics/Point;->x:I

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v4, v5

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 223
    .end local v1           #e:Landroid/graphics/Bitmap;
    :cond_2
    return-object v0
.end method

.method private calScreenTopY(Landroid/graphics/Point;)I
    .locals 3
    .parameter "cursor"

    .prologue
    .line 227
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    sget v2, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    sub-int/2addr v1, v2

    sget v2, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->PADDING_BOTTOM:I

    sub-int/2addr v1, v2

    sget v2, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_0

    .line 228
    iget v0, p1, Landroid/graphics/Point;->y:I

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v0, v1

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->PADDING_BOTTOM:I

    add-int/2addr v0, v1

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    sub-int/2addr v0, v1

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_STROKE:I

    add-int/2addr v0, v1

    .line 230
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private drawCursor(Z)V
    .locals 1
    .parameter "visible"

    .prologue
    .line 325
    if-eqz p1, :cond_1

    .line 326
    const/high16 v0, -0x100

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    .line 330
    :goto_0
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->isWritting:Z

    if-nez v0, :cond_0

    .line 331
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->postInvalidate()V

    .line 333
    :cond_0
    return-void

    .line 328
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    goto :goto_0
.end method

.method private drawHandwrites(IILandroid/graphics/Canvas;)V
    .locals 12
    .parameter "top"
    .parameter "bottom"
    .parameter "canvas"

    .prologue
    .line 124
    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    const/high16 v8, -0x100

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->drawLines(IILandroid/graphics/Canvas;)V

    .line 126
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    sget v8, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->PADDING_BOTTOM:I

    sub-int v1, v7, v8

    .line 127
    .local v1, bottomIndex:I
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 128
    .local v5, rightIndex:I
    const/4 v4, 0x0

    .line 129
    .local v4, metaIndex:I
    iget-object v7, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v7}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->getWordWidthList()Ljava/util/List;

    move-result-object v3

    .line 130
    .local v3, list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/Bitmap;>;"
    :goto_0
    if-ge v1, p1, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 131
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 132
    .local v0, b:Landroid/graphics/Bitmap;
    sget-object v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-ne v0, v7, :cond_0

    .line 133
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 134
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 144
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 145
    goto :goto_0

    .line 136
    :cond_0
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v7, v5

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    if-le v7, v8, :cond_1

    .line 137
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 138
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 139
    goto :goto_0

    .line 141
    :cond_1
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v5, v7

    goto :goto_1

    .line 146
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_2
    :goto_2
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v7, p1

    if-ge v1, v7, :cond_7

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_7

    .line 147
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 148
    .restart local v0       #b:Landroid/graphics/Bitmap;
    sget-object v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-ne v0, v7, :cond_3

    .line 149
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 150
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 167
    :goto_3
    add-int/lit8 v4, v4, 0x1

    .line 168
    goto :goto_2

    .line 152
    :cond_3
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v7, v5

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    if-le v7, v8, :cond_4

    .line 153
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 154
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 155
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    sget v9, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    sub-int v9, v1, v9

    sub-int/2addr v9, p1

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v10

    add-int/2addr v10, v5

    sub-int v11, v1, p1

    invoke-direct {v8, v5, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 156
    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v7

    sget v8, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v7, v8

    add-int/2addr v5, v7

    goto :goto_3

    .line 158
    :cond_4
    sub-int v7, v1, p1

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharHeight(Landroid/graphics/Bitmap;)I

    move-result v8

    sub-int v2, v7, v8

    .line 159
    .local v2, dstTop:I
    if-gez v2, :cond_5

    const/4 v2, 0x0

    .line 160
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int v8, v1, p1

    sub-int/2addr v8, v2

    int-to-double v8, v8

    iget-wide v10, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    mul-double/2addr v8, v10

    double-to-int v8, v8

    sub-int v6, v7, v8

    .line 161
    .local v6, srcTop:I
    if-gez v6, :cond_6

    const/4 v6, 0x0

    .line 162
    :cond_6
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v7, v8, v6, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v9

    add-int/2addr v9, v5

    sub-int v10, v1, p1

    invoke-direct {v8, v5, v2, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 164
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v5, v7

    goto/16 :goto_3

    .line 169
    .end local v0           #b:Landroid/graphics/Bitmap;
    .end local v2           #dstTop:I
    .end local v6           #srcTop:I
    :cond_7
    :goto_4
    if-gt v1, p2, :cond_a

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_a

    .line 170
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 171
    .restart local v0       #b:Landroid/graphics/Bitmap;
    sget-object v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-ne v0, v7, :cond_8

    .line 172
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 173
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 185
    :goto_5
    add-int/lit8 v4, v4, 0x1

    .line 186
    goto :goto_4

    .line 175
    :cond_8
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v7, v5

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    iget v8, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    if-le v7, v8, :cond_9

    .line 176
    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    .line 177
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v1, v7

    .line 178
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    sget v9, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    sub-int v9, v1, v9

    sub-int/2addr v9, p1

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v10

    add-int/2addr v10, v5

    sub-int v11, v1, p1

    invoke-direct {v8, v5, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 179
    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v7

    sget v8, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    add-int/2addr v7, v8

    add-int/2addr v5, v7

    goto :goto_5

    .line 181
    :cond_9
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v8, Landroid/graphics/Rect;

    sget v9, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    sub-int v9, v1, v9

    sub-int/2addr v9, p1

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v10

    add-int/2addr v10, v5

    sub-int v11, v1, p1

    invoke-direct {v8, v5, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p3, v0, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 182
    sget v7, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getCharWidth(Landroid/graphics/Bitmap;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v5, v7

    goto :goto_5

    .line 187
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_a
    return-void
.end method

.method private drawLines(IILandroid/graphics/Canvas;)V
    .locals 7
    .parameter "top"
    .parameter "bottom"
    .parameter "canvas"

    .prologue
    .line 198
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_COLOR:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 199
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_STROKE:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 200
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 201
    sget v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    rem-int v1, p1, v1

    sub-int v6, v0, v1

    .line 202
    .local v6, linePos:I
    :goto_0
    if-ge v6, p2, :cond_0

    .line 203
    sget v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    int-to-float v1, v0

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getWidth()I

    move-result v0

    sget v3, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SPACING:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 204
    sget v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_HEIGHT:I

    add-int/2addr v6, v0

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 207
    return-void
.end method

.method private getCharHeight(Landroid/graphics/Bitmap;)I
    .locals 4
    .parameter "b"

    .prologue
    .line 190
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private getCharWidth(Landroid/graphics/Bitmap;)I
    .locals 4
    .parameter "b"

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private innerAddCharacter(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter "character"

    .prologue
    .line 260
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->getWordWidthList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    const/high16 v0, -0x100

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    .line 262
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay()V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->invalidateCanvas()V

    .line 266
    return-void
.end method


# virtual methods
.method public addBlank()V
    .locals 1

    .prologue
    .line 274
    sget-object v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->BLANK_CHARACTER:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->innerAddCharacter(Landroid/graphics/Bitmap;)V

    .line 275
    return-void
.end method

.method public addCharacter(Landroid/graphics/Bitmap;)V
    .locals 8
    .parameter "character"

    .prologue
    const/4 v7, 0x1

    .line 249
    sget v2, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 250
    .local v0, scale:F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-double v2, v2

    iget-wide v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    sget v3, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    int-to-double v3, v3

    iget-wide v5, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->mDensity:D

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {p1, v2, v3, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 253
    .local v1, scaleBitmap:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getHandwriteMode()I

    move-result v2

    if-ne v2, v7, :cond_0

    .line 254
    invoke-static {p1, v1}, Lcom/youdao/note/tool/img/ImageProcess;->smooth(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 256
    :cond_0
    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->innerAddCharacter(Landroid/graphics/Bitmap;)V

    .line 257
    return-void
.end method

.method public addReturn()V
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->innerAddCharacter(Landroid/graphics/Bitmap;)V

    .line 279
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->calCursorPosition()Landroid/graphics/Point;

    move-result-object v2

    .line 237
    .local v2, cursor:Landroid/graphics/Point;
    iget v4, v2, Landroid/graphics/Point;->y:I

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    add-int/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->PADDING_BOTTOM:I

    add-int/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->LINE_STROKE:I

    add-int/2addr v4, v5

    add-int/lit8 v3, v4, 0x1

    .line 238
    .local v3, totalH:I
    iget v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    if-ge v3, v4, :cond_0

    .line 239
    iget v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    .line 241
    :cond_0
    iget v4, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 242
    .local v0, bitmap:Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 243
    .local v1, canvas:Landroid/graphics/Canvas;
    const/4 v4, 0x0

    invoke-direct {p0, v4, v3, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->drawHandwrites(IILandroid/graphics/Canvas;)V

    .line 244
    return-object v0
.end method

.method public getPaintWidth()F
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->getPaintWidth()F

    move-result v0

    return v0
.end method

.method public getPaintWidthRatio()F
    .locals 3

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getPaintWidth()F

    move-result v0

    sget v1, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->MAX_PAINT_WIDTH:F

    sget v2, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->MIN_PAINT_WIDTH:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public initSkitchMeta(Lcom/youdao/note/ui/skitch/ISkitchMeta;)V
    .locals 0
    .parameter "skitchMeta"

    .prologue
    .line 319
    check-cast p1, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    .line 320
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->startCursorDrawer()V

    .line 321
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->invalidateCanvas()V

    .line 322
    return-void
.end method

.method public invalidateCanvas()V
    .locals 0

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->postInvalidate()V

    .line 271
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter "canvas"

    .prologue
    const/4 v1, 0x1

    .line 108
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->firstDraw:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_WIDTH:I

    .line 110
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    .line 111
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 112
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 113
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->firstDraw:Z

    .line 116
    :cond_0
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->calCursorPosition()Landroid/graphics/Point;

    move-result-object v6

    .line 117
    .local v6, cursor:Landroid/graphics/Point;
    invoke-direct {p0, v6}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->calScreenTopY(Landroid/graphics/Point;)I

    move-result v7

    .line 118
    .local v7, screenTopY:I
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->SCREEN_HEIGHT:I

    add-int/2addr v0, v7

    invoke-direct {p0, v7, v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->drawHandwrites(IILandroid/graphics/Canvas;)V

    .line 119
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    iget v0, v6, Landroid/graphics/Point;->x:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v7

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Point;->x:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Point;->y:I

    sget v4, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->WORD_HEIGHT:I

    add-int/2addr v0, v4

    sub-int/2addr v0, v7

    int-to-float v4, v0

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 121
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->firstDraw:Z

    .line 60
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->invalidate()V

    .line 61
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 62
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0
    .parameter "visibility"

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/view/View;->onWindowVisibilityChanged(I)V

    .line 77
    if-nez p1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->startCursorDrawer()V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->stopCursorDrawer()V

    goto :goto_0
.end method

.method public setIswritting(Z)V
    .locals 0
    .parameter "isWritting"

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->isWritting:Z

    .line 311
    return-void
.end method

.method public setPaintWidth(F)V
    .locals 1
    .parameter "width"

    .prologue
    .line 305
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->setPaintWidth(F)V

    .line 306
    return-void
.end method

.method public startCursorDrawer()V
    .locals 2

    .prologue
    .line 86
    monitor-enter p0

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;-><init>(Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$1;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->startDrawing()V

    .line 91
    const-string v0, "cursor drawer started"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    monitor-exit p0

    .line 93
    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stopCursorDrawer()V
    .locals 1

    .prologue
    .line 97
    monitor-enter p0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->stopDrawing()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    .line 102
    :cond_0
    monitor-exit p0

    .line 103
    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public trash()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->trash()V

    .line 300
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->invalidate()V

    .line 301
    return-void
.end method

.method public undo()V
    .locals 4

    .prologue
    .line 283
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->handwriteMeta:Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;

    invoke-virtual {v3}, Lcom/youdao/note/ui/skitch/handwrite/HandwriteMeta;->getWordWidthList()Ljava/util/List;

    move-result-object v1

    .line 284
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/Bitmap;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 285
    .local v2, size:I
    if-lez v2, :cond_2

    .line 286
    add-int/lit8 v3, v2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 287
    .local v0, b:Landroid/graphics/Bitmap;
    sget-object v3, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->RETURN_CHARACTER:Landroid/graphics/Bitmap;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->BLANK_CHARACTER:Landroid/graphics/Bitmap;

    if-eq v0, v3, :cond_0

    .line 288
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 290
    :cond_0
    const/high16 v3, -0x100

    iput v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorColor:I

    .line 291
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    if-eqz v3, :cond_1

    .line 292
    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->cursorDrawer:Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;

    invoke-virtual {v3}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView$CursorDrawer;->delay()V

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWritePageView;->invalidateCanvas()V

    .line 296
    .end local v0           #b:Landroid/graphics/Bitmap;
    :cond_2
    return-void
.end method
