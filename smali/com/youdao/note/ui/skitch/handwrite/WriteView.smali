.class public Lcom/youdao/note/ui/skitch/handwrite/WriteView;
.super Lcom/youdao/note/ui/skitch/AbstractCanvasView;
.source "WriteView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/handwrite/IWrite;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# instance fields
.field private activity:Lcom/youdao/note/activity/resource/HandwritingActivity;

.field private final mCanvas:Landroid/graphics/Canvas;

.field private mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

.field private mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

.field private traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mCanvas:Landroid/graphics/Canvas;

    .line 49
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->init(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mCanvas:Landroid/graphics/Canvas;

    .line 68
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->init(Landroid/content/Context;)V

    .line 69
    return-void
.end method


# virtual methods
.method protected createTraceTool()Lcom/youdao/note/ui/skitch/trace/ITrace;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lcom/youdao/note/ui/skitch/trace/PenTrace;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;-><init>()V

    .line 119
    .local v0, trace:Lcom/youdao/note/ui/skitch/trace/PenTrace;
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    invoke-interface {v1}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->setWidth(F)V

    .line 120
    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->setColor(I)V

    .line 121
    return-object v0
.end method

.method public finishWrite()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->writeStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->finishWrite()V

    .line 129
    :cond_0
    return-void
.end method

.method public getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V
    .locals 9
    .parameter "getBitmapCallback"

    .prologue
    .line 93
    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v5}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 94
    const-wide/high16 v5, 0x3ff0

    sget v7, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->WORD_HEIGHT:I

    int-to-double v7, v7

    mul-double/2addr v5, v7

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->getHeight()I

    move-result v7

    int-to-double v7, v7

    div-double/2addr v5, v7

    double-to-float v4, v5

    .line 95
    .local v4, scale:F
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->getWidth()I

    move-result v5

    sget v6, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->WORD_HEIGHT:I

    mul-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->getHeight()I

    move-result v6

    div-int/2addr v5, v6

    sget v6, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->WORD_HEIGHT:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 97
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v5, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mCanvas:Landroid/graphics/Canvas;

    invoke-interface {v5, v6, v4}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->drawTraces(Landroid/graphics/Canvas;F)V

    .line 99
    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v5}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->getLeftMost()F

    move-result v5

    mul-float/2addr v5, v4

    float-to-int v5, v5

    add-int/lit8 v2, v5, -0x5

    .line 100
    .local v2, leftX:I
    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v5}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->getRightMost()F

    move-result v5

    mul-float/2addr v5, v4

    float-to-int v5, v5

    add-int/lit8 v3, v5, 0x5

    .line 101
    .local v3, rightX:I
    if-gez v2, :cond_0

    const/4 v2, 0x0

    .line 102
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-le v3, v5, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 103
    :cond_1
    const/4 v5, 0x0

    sub-int v6, v3, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-static {v0, v2, v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 104
    .local v1, clipedBitmap:Landroid/graphics/Bitmap;
    invoke-interface {p1, v1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;->onBitmapObtained(Landroid/graphics/Bitmap;)V

    .line 106
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #clipedBitmap:Landroid/graphics/Bitmap;
    .end local v2           #leftX:I
    .end local v3           #rightX:I
    .end local v4           #scale:F
    :cond_2
    invoke-super {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->clearScreen()V

    .line 107
    return-void
.end method

.method protected getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    return-object v0
.end method

.method public init(FII)V
    .locals 0
    .parameter "density"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 158
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 80
    new-instance v0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    .line 82
    check-cast p1, Lcom/youdao/note/activity/resource/HandwritingActivity;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->activity:Lcom/youdao/note/activity/resource/HandwritingActivity;

    .line 83
    return-void
.end method

.method public onFinishWrite()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public onStartWrite()V
    .locals 2

    .prologue
    .line 140
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->setBackgroundColor(I)V

    .line 141
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->activity:Lcom/youdao/note/activity/resource/HandwritingActivity;

    const v1, 0x7f0700b3

    invoke-virtual {v0, v1}, Lcom/youdao/note/activity/resource/HandwritingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 142
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "event"

    .prologue
    .line 73
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->touch()V

    .line 76
    :cond_0
    invoke-super {p0, p1}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
    .locals 0
    .parameter "handWriteCanvas"

    .prologue
    .line 87
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 89
    return-void
.end method

.method public setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter "monitor"

    .prologue
    .line 148
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/WriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    .line 150
    return-void
.end method
