.class public Lcom/youdao/note/ui/skitch/handwrite/PointTracker;
.super Ljava/lang/Object;
.source "PointTracker.java"


# instance fields
.field public time:J

.field public width:D

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .parameter "x"
    .parameter "y"

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 26
    iput p2, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 27
    return-void
.end method


# virtual methods
.method public set(FFD)V
    .locals 0
    .parameter "x"
    .parameter "y"
    .parameter "width"

    .prologue
    .line 30
    iput p1, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 31
    iput p2, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 32
    iput-wide p3, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    .line 33
    return-void
.end method

.method public set(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;)V
    .locals 2
    .parameter "point"

    .prologue
    .line 36
    iget v0, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 37
    iget v0, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 38
    iget-wide v0, p1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    .line 39
    return-void
.end method
