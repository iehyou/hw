.class public interface abstract Lcom/youdao/note/ui/skitch/handwrite/IWrite;
.super Ljava/lang/Object;
.source "IWrite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;
    }
.end annotation


# virtual methods
.method public abstract finishWrite()V
.end method

.method public abstract getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V
.end method

.method public abstract init(FII)V
.end method

.method public abstract onFinishWrite()V
.end method

.method public abstract onStartWrite()V
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
.end method

.method public abstract setTouchMonotor(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
.end method
