.class Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;
.super Ljava/lang/Object;
.source "GLHandWriteView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandWriteRender"
.end annotation


# static fields
.field private static final EMPTY_X:I = -0x1

.field private static final INTERVAL:I = 0x3


# instance fields
.field b:[I

.field bt:[I

.field private eventList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/MotionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private leftMost:F

.field private mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

.field private mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

.field private mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

.field private mFinshWrite:Z

.field private mFramebuffer:I

.field private mFramebufferHeight:I

.field private mFramebufferWidth:I

.field private mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

.field private mLastEvent:Landroid/view/MotionEvent;

.field private mLastSpeed:D

.field private mLastWidth:D

.field private mNeedErase:Z

.field private mTargetTexture:I

.field private mTouchCount:I

.field private rightMost:F

.field final synthetic this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;


# direct methods
.method public constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)V
    .locals 5
    .parameter
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    const/high16 v3, -0x4080

    const/4 v2, 0x0

    .line 289
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    .line 242
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    .line 244
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    .line 253
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->eventList:Ljava/util/List;

    .line 258
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastEvent:Landroid/view/MotionEvent;

    .line 260
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 262
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    .line 269
    iput v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    .line 270
    iput v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    .line 276
    iput-boolean v4, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mNeedErase:Z

    .line 281
    iput-boolean v4, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFinshWrite:Z

    .line 286
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    .line 287
    iput-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->bt:[I

    .line 290
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    .line 291
    iput-object p2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    .line 292
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->erase()V

    .line 293
    return-void
.end method

.method private beginWidth(FFFF)F
    .locals 12
    .parameter "currentSpeed"
    .parameter "currentDist"
    .parameter "lastWidth"
    .parameter "lastSpeed"

    .prologue
    .line 452
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F
    invoke-static {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$400(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$500(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-result-object v1

    invoke-interface {v1}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x4000

    mul-float v10, v0, v1

    .line 454
    .local v10, BASE_SIZE:F
    const v4, 0x3c23d70a

    .line 455
    .local v4, MOVE_THRES_PER_DIST:F
    const v5, 0x3f333333

    .line 456
    .local v5, MOVE_MAX_THRES:F
    const v6, 0x3d4ccccd

    .line 457
    .local v6, MOVE_LOWV:F
    const v7, 0x3fa66666

    .line 458
    .local v7, MOVE_LOW_FACTOR:F
    const/high16 v8, 0x4020

    .line 459
    .local v8, MOVE_HIGHV:F
    const/high16 v9, 0x3f00

    .local v9, MOVE_HIGH_FACTOR:F
    move-object v0, p0

    move v1, p1

    move/from16 v2, p4

    move v3, p2

    move v11, p3

    .line 461
    invoke-direct/range {v0 .. v11}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->calcNewWidth(FFFFFFFFFFF)F

    move-result v0

    return v0
.end method

.method private calcNewWidth(FFFFFFFFFFF)F
    .locals 12
    .parameter "currentSpeed"
    .parameter "lastSpeed"
    .parameter "currentDist"
    .parameter "thresPerDist"
    .parameter "maxThres"
    .parameter "lowv"
    .parameter "lowfactor"
    .parameter "highv"
    .parameter "highfactor"
    .parameter "basewidth"
    .parameter "lastwidth"

    .prologue
    .line 420
    const v4, 0x3e99999a

    .line 421
    .local v4, alhpa:F
    const v5, 0x3f333333

    .line 422
    .local v5, beta:F
    mul-float v9, p1, v4

    mul-float v10, p2, v5

    add-float v7, v9, v10

    .line 424
    .local v7, speed:F
    mul-float v8, p4, p3

    .line 425
    .local v8, thres:F
    cmpg-float v9, p5, v8

    if-gez v9, :cond_0

    move/from16 v8, p5

    .line 436
    :cond_0
    move/from16 v1, p10

    .line 437
    .local v1, A:F
    div-float v9, p7, p9

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->log(D)D

    move-result-wide v9

    double-to-float v9, v9

    sub-float v10, p8, p6

    div-float v2, v9, v10

    .line 438
    .local v2, B:F
    move/from16 v0, p7

    float-to-double v9, v0

    invoke-static {v9, v10}, Ljava/lang/Math;->log(D)D

    move-result-wide v9

    double-to-float v9, v9

    mul-float v10, v2, p6

    add-float v3, v9, v10

    .line 439
    .local v3, C:F
    neg-float v9, v2

    mul-float/2addr v9, v7

    add-float/2addr v9, v3

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->exp(D)D

    move-result-wide v9

    double-to-float v9, v9

    mul-float v6, v1, v9

    .line 441
    .local v6, newW:F
    sub-float v9, v6, p10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    div-float v9, v9, p10

    cmpl-float v9, v9, v8

    if-lez v9, :cond_1

    .line 442
    const/high16 v10, 0x3f80

    sub-float v9, v6, p10

    const/4 v11, 0x0

    cmpl-float v9, v9, v11

    if-lez v9, :cond_3

    const/high16 v9, 0x3f80

    :goto_0
    mul-float/2addr v9, v8

    add-float/2addr v9, v10

    mul-float v6, p10, v9

    .line 444
    :cond_1
    sub-float v9, v6, p11

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    div-float v9, v9, p11

    cmpl-float v9, v9, v8

    if-lez v9, :cond_2

    .line 445
    const/high16 v10, 0x3f80

    sub-float v9, v6, p11

    const/4 v11, 0x0

    cmpl-float v9, v9, v11

    if-lez v9, :cond_4

    const/high16 v9, 0x3f80

    :goto_1
    mul-float/2addr v9, v8

    add-float/2addr v9, v10

    mul-float v6, p11, v9

    .line 447
    :cond_2
    return v6

    .line 442
    :cond_3
    const/high16 v9, -0x4080

    goto :goto_0

    .line 445
    :cond_4
    const/high16 v9, -0x4080

    goto :goto_1
.end method

.method private createFrameBuffer(Ljavax/microedition/khronos/opengles/GL10;III)I
    .locals 11
    .parameter "gl"
    .parameter "width"
    .parameter "height"
    .parameter "targetTextureId"

    .prologue
    .line 759
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "width is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p1

    .line 760
    check-cast v0, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;

    .line 762
    .local v0, gl11ep:Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    const/4 v1, 0x1

    new-array v8, v1, [I

    .line 763
    .local v8, framebuffers:[I
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v8, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glGenFramebuffersOES(I[II)V

    .line 764
    const/4 v1, 0x0

    aget v7, v8, v1

    .line 765
    .local v7, framebuffer:I
    const v1, 0x8d40

    invoke-interface {v0, v1, v7}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 768
    const/4 v1, 0x1

    new-array v9, v1, [I

    .line 769
    .local v9, renderbuffers:[I
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v9, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glGenRenderbuffersOES(I[II)V

    .line 770
    const/4 v1, 0x0

    aget v6, v9, v1

    .line 772
    .local v6, depthbuffer:I
    const v1, 0x8d41

    invoke-interface {v0, v1, v6}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindRenderbufferOES(II)V

    .line 773
    const v1, 0x8d41

    const v2, 0x81a5

    invoke-interface {v0, v1, v2, p2, p3}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glRenderbufferStorageOES(IIII)V

    .line 775
    const v1, 0x8d40

    const v2, 0x8d00

    const v3, 0x8d41

    invoke-interface {v0, v1, v2, v3, v6}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glFramebufferRenderbufferOES(IIII)V

    .line 779
    const v1, 0x8d40

    const v2, 0x8ce0

    const/16 v3, 0xde1

    const/4 v5, 0x0

    move v4, p4

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glFramebufferTexture2DOES(IIIII)V

    .line 782
    const v1, 0x8d40

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glCheckFramebufferStatusOES(I)I

    move-result v10

    .line 783
    .local v10, status:I
    const v1, 0x8cd5

    if-eq v10, v1, :cond_0

    .line 784
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Framebuffer is not complete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 787
    :cond_0
    const v1, 0x8d40

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 788
    return v7
.end method

.method private createTargetTexture(Ljavax/microedition/khronos/opengles/GL10;II)I
    .locals 12
    .parameter "gl"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 792
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "in crete texture width "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 795
    const/4 v0, 0x1

    new-array v11, v0, [I

    .line 796
    .local v11, textures:[I
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1, v0, v11, v1}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 798
    const/4 v0, 0x0

    aget v10, v11, v0

    .line 799
    .local v10, texture:I
    const/16 v0, 0xde1

    invoke-interface {p1, v0, v10}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 801
    const/16 v1, 0xde1

    const/4 v2, 0x0

    const/16 v3, 0x1908

    const/4 v6, 0x0

    const/16 v7, 0x1908

    const/16 v8, 0x1401

    const/4 v9, 0x0

    move-object v0, p1

    move v4, p2

    move v5, p3

    invoke-interface/range {v0 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 804
    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const v2, 0x46180400

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 807
    const/16 v0, 0xde1

    const/16 v1, 0x2800

    const v2, 0x46180400

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    .line 809
    const/16 v0, 0xde1

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterx(III)V

    .line 811
    const/16 v0, 0xde1

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterx(III)V

    .line 814
    return v10
.end method

.method private drawLine(Ljavax/microedition/khronos/opengles/GL10;FFFFFFFF)V
    .locals 14
    .parameter "gl"
    .parameter "sx"
    .parameter "sy"
    .parameter "sw"
    .parameter "dx"
    .parameter "dy"
    .parameter "dw"
    .parameter "bgAlpha"
    .parameter "fgAlpha"

    .prologue
    .line 618
    sub-float v0, p5, p2

    float-to-double v0, v0

    sub-float v5, p6, p3

    float-to-double v5, v5

    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v5}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v5

    float-to-double v5, v5

    mul-double/2addr v0, v5

    const-wide/high16 v5, 0x4000

    div-double v8, v0, v5

    .line 619
    .local v8, dist:D
    double-to-int v0, v8

    add-int/lit8 v7, v0, 0x1

    .line 620
    .local v7, count:I
    sub-float v0, p5, p2

    int-to-float v1, v7

    div-float v12, v0, v1

    .line 621
    .local v12, xstep:F
    sub-float v0, p6, p3

    int-to-float v1, v7

    div-float v13, v0, v1

    .line 622
    .local v13, ystep:F
    sub-float v0, p7, p4

    int-to-float v1, v7

    div-float v11, v0, v1

    .line 623
    .local v11, wstep:F
    move/from16 v2, p2

    .line 624
    .local v2, px:F
    move/from16 v3, p3

    .line 625
    .local v3, py:F
    move/from16 v4, p4

    .line 626
    .local v4, pw:F
    const/4 v10, 0x0

    .local v10, i:I
    :goto_0
    if-ge v10, v7, :cond_0

    .line 628
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    move-object v1, p1

    move/from16 v5, p8

    move/from16 v6, p9

    invoke-virtual/range {v0 .. v6}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->draw(Ljavax/microedition/khronos/opengles/GL10;FFFFF)V

    .line 629
    add-float/2addr v2, v12

    .line 630
    add-float/2addr v3, v13

    .line 631
    add-float/2addr v4, v11

    .line 626
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 633
    :cond_0
    return-void
.end method

.method private drawPoint(Ljavax/microedition/khronos/opengles/GL10;FFFFF)V
    .locals 7
    .parameter "gl"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "bgAlpha"
    .parameter "fgAlpha"

    .prologue
    .line 645
    const/16 v0, 0x1700

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 646
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 647
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->draw(Ljavax/microedition/khronos/opengles/GL10;FFFFF)V

    .line 648
    return-void
.end method

.method private drawPointsOnOffscreen(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 31
    .parameter "gl"

    .prologue
    .line 499
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->eventList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, i$:Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/MotionEvent;

    .line 500
    .local v26, event:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    if-eqz v3, :cond_0

    invoke-static/range {v26 .. v26}, Lcom/youdao/note/utils/CompatUtils;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v3

    if-nez v3, :cond_2

    .line 501
    :cond_0
    const-wide v19, 0x3fe999999999999aL

    .line 502
    .local v19, BEGIN_POINT_FACTOR:D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F
    invoke-static {v3}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$400(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    invoke-static {v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$500(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-result-object v6

    invoke-interface {v6}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v6

    mul-float/2addr v3, v6

    const/high16 v6, 0x4000

    mul-float/2addr v3, v6

    float-to-double v6, v3

    mul-double v6, v6, v19

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    .line 503
    new-instance v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-direct {v3}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 504
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    invoke-virtual {v3, v6, v7, v8, v9}, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->set(FFD)V

    .line 505
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    .line 541
    .end local v19           #BEGIN_POINT_FACTOR:D
    :goto_1
    invoke-static/range {v26 .. v26}, Lcom/youdao/note/utils/CompatUtils;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_1

    .line 542
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 543
    .local v4, x:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 545
    .local v5, y:F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    const/4 v6, 0x2

    if-ge v3, v6, :cond_5

    .line 547
    invoke-direct/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->endWidth()F

    move-result v16

    const/high16 v17, 0x3f80

    const/high16 v18, 0x3f80

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move v14, v4

    move v15, v5

    invoke-direct/range {v12 .. v18}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawPoint(Ljavax/microedition/khronos/opengles/GL10;FFFFF)V

    .line 578
    :goto_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    .line 579
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    .line 581
    .end local v4           #x:F
    .end local v5           #y:F
    :cond_1
    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastEvent:Landroid/view/MotionEvent;

    .line 582
    move-object/from16 v0, p0

    iget v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    goto/16 :goto_0

    .line 508
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 509
    .restart local v4       #x:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v5, v3, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 511
    .restart local v5       #y:F
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v6

    div-float v22, v3, v6

    .line 512
    .local v22, difX:F
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v3, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v6

    div-float v23, v3, v6

    .line 514
    .local v23, difY:F
    move/from16 v0, v22

    float-to-double v6, v0

    move/from16 v0, v23

    float-to-double v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v21, v0

    .line 515
    .local v21, deltaDistance:F
    move/from16 v0, v21

    float-to-int v3, v0

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v30, v3, 0x1

    .line 517
    .local v30, steps:I
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastEvent:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-float v3, v6

    div-float v3, v21, v3

    float-to-double v0, v3

    move-wide/from16 v28, v0

    .line 522
    .local v28, speed:D
    const-wide/16 v10, 0x0

    .line 523
    .local v10, newW:D
    move-object/from16 v0, p0

    iget v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    const/4 v6, 0x3

    if-ge v3, v6, :cond_3

    .line 524
    move-wide/from16 v0, v28

    double-to-float v3, v0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    double-to-float v6, v6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    double-to-float v7, v7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v1, v6, v7}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->beginWidth(FFFF)F

    move-result v3

    float-to-double v10, v3

    .line 530
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTouchCount:I

    const/4 v6, 0x2

    if-ge v3, v6, :cond_4

    .line 531
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    .line 532
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-virtual/range {v3 .. v11}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->Init(FFDFFD)V

    .line 537
    :goto_4
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    add-double v6, v6, v28

    const-wide/high16 v8, 0x4000

    div-double/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    .line 538
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    .line 539
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawToPoint(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto/16 :goto_1

    .line 526
    :cond_3
    move-wide/from16 v0, v28

    double-to-float v3, v0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastWidth:D

    double-to-float v6, v6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mLastSpeed:D

    double-to-float v7, v7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v3, v1, v6, v7}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->movingWidth(FFFF)F

    move-result v3

    float-to-double v10, v3

    goto :goto_3

    .line 534
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    double-to-float v8, v10

    invoke-virtual {v3, v6, v7, v8}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->AddNode(FFF)V

    goto :goto_4

    .line 554
    .end local v10           #newW:D
    .end local v21           #deltaDistance:F
    .end local v22           #difX:F
    .end local v23           #difY:F
    .end local v28           #speed:D
    .end local v30           #steps:I
    :cond_5
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getX()F

    move-result v24

    .line 555
    .local v24, endx:F
    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->getY()F

    move-result v25

    .line 557
    .local v25, endy:F
    sub-float v3, v24, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v6

    div-float v22, v3, v6

    .line 558
    .restart local v22       #difX:F
    sub-float v3, v25, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v6

    div-float v23, v3, v6

    .line 559
    .restart local v23       #difY:F
    move/from16 v0, v22

    float-to-double v6, v0

    move/from16 v0, v23

    float-to-double v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v21, v0

    .line 560
    .restart local v21       #deltaDistance:F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mDensity:F
    invoke-static {v3}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$600(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v3

    mul-float v3, v3, v21

    float-to-int v3, v3

    div-int/lit8 v3, v3, 0x3

    add-int/lit8 v30, v3, 0x1

    .line 562
    .restart local v30       #steps:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    const/4 v6, 0x0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v3, v0, v1, v6}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->AddNode(FFF)V

    .line 563
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawToPoint(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 565
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    invoke-virtual {v3}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->End()V

    .line 566
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawToPoint(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto/16 :goto_2

    .line 585
    .end local v4           #x:F
    .end local v5           #y:F
    .end local v21           #deltaDistance:F
    .end local v22           #difX:F
    .end local v23           #difY:F
    .end local v24           #endx:F
    .end local v25           #endy:F
    .end local v26           #event:Landroid/view/MotionEvent;
    .end local v30           #steps:I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->resetState()V

    .line 587
    const-string v3, "Draw offscreen ended"

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 588
    return-void
.end method

.method private drawToPoint(Ljavax/microedition/khronos/opengles/GL10;I)V
    .locals 15
    .parameter "gl"
    .parameter "steps"

    .prologue
    .line 591
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    const-wide/16 v6, 0x0

    invoke-virtual {v1, v2, v6, v7}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V

    .line 592
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v3, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 593
    .local v3, lastx:F
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 594
    .local v4, lasty:F
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    double-to-float v5, v1

    .line 595
    .local v5, lastw:F
    const-wide/high16 v1, 0x3ff0

    move/from16 v0, p2

    int-to-double v6, v0

    div-double v13, v1, v6

    .line 596
    .local v13, step:D
    move-wide v11, v13

    .line 597
    .local v11, i:D
    :goto_0
    const-wide/high16 v1, 0x3ff0

    cmpg-double v1, v11, v1

    if-gez v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    invoke-virtual {v1, v2, v11, v12}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V

    .line 600
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v7, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    double-to-float v8, v1

    const v9, 0x3f19999a

    const v10, 0x3f4ccccd

    move-object v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v10}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawLine(Ljavax/microedition/khronos/opengles/GL10;FFFFFFFF)V

    .line 604
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v3, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    .line 605
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v4, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    .line 606
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    double-to-float v5, v1

    .line 607
    add-double/2addr v11, v13

    goto :goto_0

    .line 609
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBezier:Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    const-wide/high16 v6, 0x3ff0

    invoke-virtual {v1, v2, v6, v7}, Lcom/youdao/note/ui/skitch/handwrite/QuadraticBezierSpline2;->getPoint(Lcom/youdao/note/ui/skitch/handwrite/PointTracker;D)V

    .line 610
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v6, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget v7, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->y:F

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mDrawPoint:Lcom/youdao/note/ui/skitch/handwrite/PointTracker;

    iget-wide v1, v1, Lcom/youdao/note/ui/skitch/handwrite/PointTracker;->width:D

    double-to-float v8, v1

    const v9, 0x3f19999a

    const v10, 0x3f4ccccd

    move-object v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v10}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawLine(Ljavax/microedition/khronos/opengles/GL10;FFFFFFFF)V

    .line 614
    return-void
.end method

.method private endWidth()F
    .locals 3

    .prologue
    .line 491
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$400(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v1

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    invoke-static {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$500(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-result-object v2

    invoke-interface {v2}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x4000

    mul-float v0, v1, v2

    .line 492
    .local v0, BASE_SIZE:F
    const v1, 0x3f99999a

    mul-float/2addr v1, v0

    return v1
.end method

.method private getColor(IIII[I)I
    .locals 11
    .parameter "w"
    .parameter "h"
    .parameter "i"
    .parameter "j"
    .parameter "b2"

    .prologue
    .line 693
    const/4 v0, 0x1

    .line 694
    .local v0, S:I
    const/4 v4, 0x0

    .line 695
    .local v4, pa:I
    const/4 v5, 0x0

    .line 696
    .local v5, pb:I
    const/4 v6, 0x0

    .line 697
    .local v6, pg:I
    const/4 v8, 0x0

    .line 699
    .local v8, pr:I
    const/4 v1, 0x0

    .line 700
    .local v1, count:I
    add-int/lit8 v2, p3, -0x1

    .local v2, ii:I
    :goto_0
    add-int/lit8 v9, p3, 0x1

    if-gt v2, v9, :cond_2

    .line 701
    if-ltz v2, :cond_1

    if-ge v2, p2, :cond_1

    .line 702
    add-int/lit8 v3, p4, -0x1

    .local v3, jj:I
    :goto_1
    add-int/lit8 v9, p4, 0x1

    if-gt v3, v9, :cond_1

    .line 703
    if-ltz v3, :cond_0

    if-ge v3, p1, :cond_0

    .line 704
    iget-object v9, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    mul-int v10, v2, p1

    add-int/2addr v10, v3

    aget v7, v9, v10

    .line 705
    .local v7, pix:I
    and-int/lit16 v9, v7, 0xff

    add-int/2addr v4, v9

    .line 706
    shr-int/lit8 v9, v7, 0x8

    and-int/lit16 v9, v9, 0xff

    add-int/2addr v5, v9

    .line 707
    shr-int/lit8 v9, v7, 0x10

    and-int/lit16 v9, v9, 0xff

    add-int/2addr v6, v9

    .line 708
    shr-int/lit8 v9, v7, 0x18

    and-int/lit16 v9, v9, 0xff

    add-int/2addr v8, v9

    .line 709
    add-int/lit8 v1, v1, 0x1

    .line 702
    .end local v7           #pix:I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 700
    .end local v3           #jj:I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 714
    :cond_2
    div-int/2addr v4, v1

    .line 715
    div-int/2addr v5, v1

    .line 716
    div-int/2addr v8, v1

    .line 717
    div-int/2addr v6, v1

    .line 718
    shl-int/lit8 v9, v8, 0x18

    shl-int/lit8 v10, v6, 0x10

    or-int/2addr v9, v10

    shl-int/lit8 v10, v5, 0x8

    or-int/2addr v9, v10

    or-int/2addr v9, v4

    return v9
.end method

.method private movingWidth(FFFF)F
    .locals 12
    .parameter "currentSpeed"
    .parameter "currentDist"
    .parameter "lastWidth"
    .parameter "lastSpeed"

    .prologue
    .line 472
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mStandWidth:F
    invoke-static {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$400(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)F

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mHandWriteCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$500(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    move-result-object v1

    invoke-interface {v1}, Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;->getPaintWidthRatio()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x4000

    mul-float v10, v0, v1

    .line 474
    .local v10, BASE_SIZE:F
    const v4, 0x3c23d70a

    .line 475
    .local v4, MOVE_THRES_PER_DIST:F
    const v5, 0x3f333333

    .line 476
    .local v5, MOVE_MAX_THRES:F
    const v6, 0x3d4ccccd

    .line 477
    .local v6, MOVE_LOWV:F
    const/high16 v7, 0x3fc0

    .line 478
    .local v7, MOVE_LOW_FACTOR:F
    const/high16 v8, 0x4020

    .line 479
    .local v8, MOVE_HIGHV:F
    const/high16 v9, 0x3f00

    .local v9, MOVE_HIGH_FACTOR:F
    move-object v0, p0

    move v1, p1

    move/from16 v2, p4

    move v3, p2

    move v11, p3

    .line 481
    invoke-direct/range {v0 .. v11}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->calcNewWidth(FFFFFFFFFFF)F

    move-result v0

    return v0
.end method

.method private resize(I)I
    .locals 3
    .parameter "x"

    .prologue
    const/4 v2, 0x1

    .line 737
    const/4 v0, 0x1

    .line 738
    .local v0, i:I
    :goto_0
    shl-int v1, v2, v0

    if-ge v1, p1, :cond_0

    .line 739
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 741
    :cond_0
    shl-int v1, v2, v0

    return v1
.end method


# virtual methods
.method public declared-synchronized add(Landroid/view/MotionEvent;)V
    .locals 4
    .parameter "event"

    .prologue
    const/high16 v3, -0x4080

    .line 321
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->eventList:Ljava/util/List;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 323
    .local v0, x:F
    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 324
    :cond_0
    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    .line 326
    :cond_1
    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 327
    :cond_2
    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :cond_3
    monitor-exit p0

    return-void

    .line 321
    .end local v0           #x:F
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized erase()V
    .locals 1

    .prologue
    .line 302
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mNeedErase:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    monitor-exit p0

    return-void

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized finishWrite()V
    .locals 1

    .prologue
    .line 311
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFinshWrite:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    monitor-exit p0

    return-void

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 15
    .parameter "gl"

    .prologue
    const/high16 v14, 0x4248

    const/high16 v13, -0x4080

    .line 344
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$100(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    move-result-object v1

    if-nez v1, :cond_0

    .line 345
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    new-instance v2, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    iget-object v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;-><init>(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$1;)V

    #setter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    invoke-static {v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$102(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->monitor:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$100(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$PerformanceMonitor;->recordTime(J)V

    .line 349
    const/high16 v1, 0x3f80

    const/high16 v2, 0x3f80

    const/high16 v3, 0x3f80

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 350
    const/16 v1, 0x4000

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 352
    move-object/from16 v0, p1

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;

    move-object v10, v0

    .line 353
    .local v10, gl11ep:Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    const v1, 0x8d40

    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebuffer:I

    invoke-interface {v10, v1, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 356
    const/16 v1, 0x1701

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 357
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 358
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v1

    int-to-float v3, v1

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v1

    int-to-float v4, v1

    const/4 v5, 0x0

    const/high16 v6, -0x4080

    const/high16 v7, 0x3f80

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    .line 360
    const/16 v1, 0x1700

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 362
    iget-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mNeedErase:Z

    if-eqz v1, :cond_7

    .line 363
    const/high16 v1, 0x3f80

    const/high16 v2, 0x3f80

    const/high16 v3, 0x3f80

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 364
    const/16 v1, 0x4000

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 365
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mNeedErase:Z

    .line 369
    :goto_0
    const v1, 0x8d40

    const/4 v2, 0x0

    invoke-interface {v10, v1, v2}, Ljavax/microedition/khronos/opengles/GL11ExtensionPack;->glBindFramebufferOES(II)V

    .line 371
    const/16 v1, 0x1701

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 372
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 373
    const/4 v2, 0x0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v1

    int-to-float v3, v1

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v1

    int-to-float v4, v1

    const/4 v5, 0x0

    const/high16 v6, -0x4080

    const/high16 v7, 0x3f80

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    .line 375
    const/16 v1, 0x1700

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 377
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    iget v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTargetTexture:I

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferWidth:I

    iget v7, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferHeight:I

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->drawOnScreen(Ljavax/microedition/khronos/opengles/GL10;IIIII)V

    .line 380
    iget-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFinshWrite:Z

    if-eqz v1, :cond_5

    .line 381
    const-string v1, "In Finish Block"

    invoke-static {p0, v1}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v5

    move-object v1, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->savePixels(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 386
    .local v8, bitmap:Landroid/graphics/Bitmap;
    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    sub-float/2addr v1, v14

    float-to-int v11, v1

    .line 387
    .local v11, leftX:I
    if-ltz v11, :cond_1

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    cmpl-float v1, v1, v13

    if-nez v1, :cond_2

    .line 388
    :cond_1
    const/4 v11, 0x0

    .line 389
    :cond_2
    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    add-float/2addr v1, v14

    float-to-int v12, v1

    .line 390
    .local v12, rightX:I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-gt v12, v1, :cond_3

    iget v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    cmpl-float v1, v1, v13

    if-nez v1, :cond_4

    .line 391
    :cond_3
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    .line 392
    :cond_4
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    const/4 v2, 0x0

    sub-int v3, v12, v11

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v8, v11, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    #setter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->character:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$202(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 398
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v11           #leftX:I
    .end local v12           #rightX:I
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    :try_start_3
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 400
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 401
    const/high16 v1, -0x4080

    :try_start_4
    iput v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->rightMost:F

    iput v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->leftMost:F

    .line 402
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mNeedErase:Z

    .line 405
    :cond_5
    iget-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFinshWrite:Z

    if-eqz v1, :cond_6

    .line 406
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFinshWrite:Z

    .line 407
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    #getter for: Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->mMonitor:Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;
    invoke-static {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->access$300(Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;)Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->finishGetCharacter()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 410
    :cond_6
    monitor-exit p0

    return-void

    .line 367
    :cond_7
    :try_start_5
    invoke-direct/range {p0 .. p1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->drawPointsOnOffscreen(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 344
    .end local v10           #gl11ep:Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 394
    .restart local v10       #gl11ep:Ljavax/microedition/khronos/opengles/GL11ExtensionPack;
    :catch_0
    move-exception v9

    .line 395
    .local v9, e:Ljava/lang/OutOfMemoryError;
    :try_start_6
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00ca

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 400
    .end local v9           #e:Ljava/lang/OutOfMemoryError;
    :catchall_1
    move-exception v1

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .parameter "gl"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v2, 0x0

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "on surface changed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " * "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 725
    invoke-interface {p1, v2, v2, p2, p3}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    .line 726
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 8
    .parameter "gl"
    .parameter "config"

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 826
    const/16 v0, 0x1f03

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v7

    .line 827
    .local v7, extensions:Ljava/lang/String;
    invoke-static {p0, v7}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 828
    const-string v0, "GL_OES_framebuffer_object"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 830
    const-string v0, "Framebuffer is not supported!"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 867
    :goto_0
    return-void

    .line 834
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->resize(I)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferWidth:I

    .line 835
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->resize(I)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferHeight:I

    .line 836
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferWidth:I

    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferHeight:I

    invoke-direct {p0, p1, v0, v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->createTargetTexture(Ljavax/microedition/khronos/opengles/GL10;II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTargetTexture:I

    .line 838
    iget v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferWidth:I

    iget v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebufferHeight:I

    iget v3, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mTargetTexture:I

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->createFrameBuffer(Ljavax/microedition/khronos/opengles/GL10;III)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mFramebuffer:I

    .line 841
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mBrush:Lcom/youdao/note/ui/skitch/handwrite/NBrush;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->this$0:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/youdao/note/ui/skitch/handwrite/NBrush;->loadGLTexture(Ljavax/microedition/khronos/opengles/GL10;Landroid/content/Context;)V

    .line 843
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v0

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v2}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v2

    invoke-interface {p1, v4, v4, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    .line 845
    const/16 v0, 0x1701

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 847
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 848
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v0

    int-to-float v2, v0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v0

    int-to-float v3, v0

    const/high16 v5, -0x4080

    const/high16 v6, 0x3f80

    move-object v0, p1

    move v4, v1

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    .line 850
    const/16 v0, 0x1700

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 852
    const/16 v0, 0xbd0

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 854
    const/16 v0, 0xde1

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 855
    const/16 v0, 0xbe2

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 859
    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x453e2000

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    .line 861
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 863
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 865
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    goto/16 :goto_0
.end method

.method public declared-synchronized resetState()V
    .locals 1

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->eventList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    monitor-exit p0

    return-void

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected savePixels(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;
    .locals 11
    .parameter "x"
    .parameter "y"
    .parameter "w"
    .parameter "h"
    .parameter "gl"

    .prologue
    .line 661
    const-string v0, "In save pixcls block."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->bt:[I

    if-nez v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    .line 664
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->mGLHandWriteView:Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->bt:[I

    .line 666
    :cond_0
    const-string v0, "In save pixcls block2."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    invoke-static {v0}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 668
    .local v7, ib:Ljava/nio/IntBuffer;
    const-string v0, "In save pixcls block3."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 669
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 670
    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object/from16 v0, p5

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 671
    const-string v0, "In save pixcls block4."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 672
    const/4 v8, 0x0

    .local v8, i:I
    :goto_0
    if-ge v8, p4, :cond_2

    .line 673
    const/4 v9, 0x0

    .local v9, j:I
    :goto_1
    if-ge v9, p3, :cond_1

    .line 675
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->bt:[I

    sub-int v1, p4, v8

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p3

    add-int/2addr v1, v9

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->b:[I

    mul-int v3, v8, p3

    add-int/2addr v3, v9

    aget v2, v2, v3

    aput v2, v0, v1

    .line 673
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 672
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 683
    .end local v9           #j:I
    :cond_2
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/GLHandWriteView$HandWriteRender;->bt:[I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, p3, p4, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 684
    .local v10, sb:Landroid/graphics/Bitmap;
    return-object v10
.end method
