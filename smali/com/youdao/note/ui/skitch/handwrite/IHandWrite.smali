.class public interface abstract Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
.super Ljava/lang/Object;
.source "IHandWrite.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/ISkitch;


# virtual methods
.method public abstract addBlank()V
.end method

.method public abstract addCharacter(Landroid/graphics/Bitmap;)V
.end method

.method public abstract addReturn()V
.end method

.method public abstract invalidateCanvas()V
.end method

.method public abstract setIswritting(Z)V
.end method

.method public abstract startCursorDrawer()V
.end method

.method public abstract stopCursorDrawer()V
.end method
