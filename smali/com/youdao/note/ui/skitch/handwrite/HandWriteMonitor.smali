.class public Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;
.super Ljava/lang/Object;
.source "HandWriteMonitor.java"


# static fields
.field private static final DEFAULT_DELAY_TIME:J = 0x320L

.field private static final MESSAGE_FINISH_GETCHARACTER:I = 0x2

.field private static final MESSAGE_FINISH_WRITE:I = 0x1


# instance fields
.field private handler:Landroid/os/Handler;

.field private mBitmapGetter:Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;

.field private mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

.field private mDelayTime:J

.field private mWriteStarted:Z

.field private mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;


# direct methods
.method public constructor <init>(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;Lcom/youdao/note/ui/skitch/handwrite/IWrite;)V
    .locals 2
    .parameter "handWriteCanvas"
    .parameter "writer"

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 28
    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    .line 29
    const-wide/16 v0, 0x320

    iput-wide v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mDelayTime:J

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriteStarted:Z

    .line 32
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$1;-><init>(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mBitmapGetter:Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;

    .line 42
    new-instance v0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor$2;-><init>(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->handler:Landroid/os/Handler;

    .line 60
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 61
    iput-object p2, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;
    .locals 1
    .parameter "x0"

    .prologue
    .line 21
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->innerFinishWrite()V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->innerFinishGetCharacter()V

    return-void
.end method

.method private innerFinishGetCharacter()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriteStarted:Z

    .line 78
    return-void
.end method

.method private innerFinishWrite()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mBitmapGetter:Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->getCharacter(Lcom/youdao/note/ui/skitch/handwrite/IWrite$IGetBitmap;)V

    .line 82
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->onFinishWrite()V

    .line 83
    return-void
.end method


# virtual methods
.method public finishGetCharacter()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 72
    return-void
.end method

.method public finishWrite()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 66
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->innerFinishWrite()V

    .line 67
    return-void
.end method

.method public setDelayTime(J)V
    .locals 0
    .parameter "time"

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mDelayTime:J

    .line 91
    return-void
.end method

.method public setHandWriteCanvas(Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;)V
    .locals 0
    .parameter "handWriteCanvas"

    .prologue
    .line 86
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mCanvas:Lcom/youdao/note/ui/skitch/handwrite/IHandWrite;

    .line 87
    return-void
.end method

.method public touch()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 98
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriteStarted:Z

    if-nez v0, :cond_0

    .line 99
    iput-boolean v3, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriteStarted:Z

    .line 100
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriter:Lcom/youdao/note/ui/skitch/handwrite/IWrite;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/handwrite/IWrite;->onStartWrite()V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->handler:Landroid/os/Handler;

    iget-wide v1, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mDelayTime:J

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 105
    return-void
.end method

.method public writeStarted()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/handwrite/HandWriteMonitor;->mWriteStarted:Z

    return v0
.end method
