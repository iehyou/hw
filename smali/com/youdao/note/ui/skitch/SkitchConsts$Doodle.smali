.class public interface abstract Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;
.super Ljava/lang/Object;
.source "SkitchConsts.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/SkitchConsts;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/SkitchConsts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Doodle"
.end annotation


# static fields
.field public static final COLOR_COUNT:I = 0x6

.field public static final DEFAULT_PAINT_COLOR:I

.field public static final MARGIN_VERTICAL:F

.field public static final PAD:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;->PAD:F

    .line 67
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;->MARGIN_VERTICAL:F

    .line 69
    sget v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb1:I

    sput v0, Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;->DEFAULT_PAINT_COLOR:I

    return-void
.end method
