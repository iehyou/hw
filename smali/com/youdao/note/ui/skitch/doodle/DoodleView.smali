.class public Lcom/youdao/note/ui/skitch/doodle/DoodleView;
.super Lcom/youdao/note/ui/skitch/AbstractCanvasView;
.source "DoodleView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/doodle/IDoodle;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;


# instance fields
.field private doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "c"

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method


# virtual methods
.method protected createTraceTool()Lcom/youdao/note/ui/skitch/trace/ITrace;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/youdao/note/ui/skitch/trace/PenTrace;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/trace/PenTrace;-><init>()V

    .line 105
    .local v0, penTrace:Lcom/youdao/note/ui/skitch/trace/PenTrace;
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->getPaintwidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->setWidth(F)V

    .line 106
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v1}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->getPaintColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/trace/PenTrace;->setColor(I)V

    .line 107
    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 62
    .local v1, bitmap:Landroid/graphics/Bitmap;
    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 63
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 64
    .local v2, canvas:Landroid/graphics/Canvas;
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020046

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 65
    .local v0, bg:Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v7, v7, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 67
    invoke-virtual {p0, v2}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->draw(Landroid/graphics/Canvas;)V

    .line 68
    return-object v1
.end method

.method public getPaintColor()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->getPaintColor()I

    move-result v0

    return v0
.end method

.method public getPaintWidth()F
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->getPaintwidth()F

    move-result v0

    return v0
.end method

.method public getPaintWidthRatio()F
    .locals 3

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getPaintWidth()F

    move-result v0

    sget v1, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->MAX_PAINT_WIDTH:F

    sget v2, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->MIN_PAINT_WIDTH:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method protected getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    return-object v0
.end method

.method public initSkitchMeta(Lcom/youdao/note/ui/skitch/ISkitchMeta;)V
    .locals 0
    .parameter "skitchMeta"

    .prologue
    .line 51
    check-cast p1, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    .end local p1
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    .line 52
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .parameter "w"
    .parameter "h"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->onSizeChanged(IIII)V

    .line 47
    return-void
.end method

.method public setPaintColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->setPaintColor(I)V

    .line 85
    return-void
.end method

.method public setPaintWidth(F)V
    .locals 1
    .parameter "width"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->doodleMeta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->setPaintwidth(F)V

    .line 57
    return-void
.end method

.method public trash()V
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->clear()V

    .line 79
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->invalidate()V

    .line 80
    return-void
.end method

.method public undo()V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->removeLastTrace()V

    .line 73
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->invalidate()V

    .line 74
    return-void
.end method
