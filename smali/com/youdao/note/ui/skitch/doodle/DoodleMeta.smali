.class public Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;
.super Ljava/lang/Object;
.source "DoodleMeta.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/ISkitchMeta;
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;


# instance fields
.field private isNew:Z

.field private paintColor:I

.field private paintwidth:F

.field private traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/trace/BaseTraceManager;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    .line 12
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getDoodlePaintWidth()F

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintwidth:F

    .line 15
    sget v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->DEFAULT_PAINT_COLOR:I

    iput v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintColor:I

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->isNew:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic copy()Lcom/youdao/note/ui/skitch/ISkitchMeta;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->copy()Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    move-result-object v0

    return-object v0
.end method

.method public copy()Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;

    invoke-direct {v0}, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;-><init>()V

    .line 72
    .local v0, meta:Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;
    iget-boolean v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->isNew:Z

    iput-boolean v1, v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->isNew:Z

    .line 73
    iget v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintColor:I

    iput v1, v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintColor:I

    .line 74
    iget v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintwidth:F

    iput v1, v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintwidth:F

    .line 75
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v1}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->copy()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v1

    iput-object v1, v0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    .line 76
    return-object v0
.end method

.method public getPaintColor()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintColor:I

    return v0
.end method

.method public getPaintwidth()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintwidth:F

    return v0
.end method

.method public getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    :cond_0
    const/4 v0, 0x1

    .line 31
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNew()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->isNew:Z

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->clear()V

    .line 24
    :cond_0
    return-void
.end method

.method public setIsNew(Z)V
    .locals 0
    .parameter "b"

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->isNew:Z

    .line 67
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .parameter "paintColor"

    .prologue
    .line 56
    iput p1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintColor:I

    .line 57
    return-void
.end method

.method public setPaintwidth(F)V
    .locals 1
    .parameter "paintWidth"

    .prologue
    .line 39
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/youdao/note/YNoteApplication;->setDoodlePaintWidth(F)V

    .line 40
    iput p1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->paintwidth:F

    .line 41
    return-void
.end method

.method public setTraceManager(Lcom/youdao/note/ui/skitch/trace/ITraceManager;)V
    .locals 0
    .parameter "traceManager"

    .prologue
    .line 48
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/doodle/DoodleMeta;->traceManager:Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    .line 49
    return-void
.end method
