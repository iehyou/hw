.class public abstract Lcom/youdao/note/ui/skitch/AbstractCanvasView;
.super Landroid/widget/ImageView;
.source "AbstractCanvasView.java"


# instance fields
.field private curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public clearScreen()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    .line 79
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->clear()V

    .line 80
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->invalidate()V

    .line 81
    return-void
.end method

.method protected abstract createTraceTool()Lcom/youdao/note/ui/skitch/trace/ITrace;
.end method

.method protected abstract getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .parameter "canvas"

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-interface {v0, p1, v1}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->drawTraces(Landroid/graphics/Canvas;F)V

    .line 75
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter "event"

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->onTouch(F)V

    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 66
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->createTraceTool()Lcom/youdao/note/ui/skitch/trace/ITrace;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    .line 43
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->addTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V

    .line 44
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/ui/skitch/trace/ITrace;->startTrace(FF)V

    .line 45
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->invalidate()V

    goto :goto_0

    .line 48
    :pswitch_1
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    if-nez v0, :cond_1

    .line 49
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->createTraceTool()Lcom/youdao/note/ui/skitch/trace/ITrace;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    .line 50
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->getTraceManager()Lcom/youdao/note/ui/skitch/trace/ITraceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/skitch/trace/ITraceManager;->addTrace(Lcom/youdao/note/ui/skitch/trace/ITrace;)V

    .line 51
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/ui/skitch/trace/ITrace;->startTrace(FF)V

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/ui/skitch/trace/ITrace;->continueTrace(FF)V

    .line 54
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->invalidate()V

    goto :goto_0

    .line 57
    :pswitch_2
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/youdao/note/ui/skitch/trace/ITrace;->continueTrace(FF)V

    .line 59
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->invalidate()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/AbstractCanvasView;->curTrace:Lcom/youdao/note/ui/skitch/trace/ITrace;

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
