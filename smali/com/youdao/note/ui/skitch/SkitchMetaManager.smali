.class public Lcom/youdao/note/ui/skitch/SkitchMetaManager;
.super Ljava/lang/Object;
.source "SkitchMetaManager.java"


# instance fields
.field private map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/youdao/note/ui/skitch/ISkitchMeta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addOrUpdateSkitchMeta(Ljava/lang/String;Lcom/youdao/note/ui/skitch/ISkitchMeta;)V
    .locals 1
    .parameter "skitchId"
    .parameter "skitchMeta"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-void
.end method

.method public containsSkitch(Ljava/lang/String;)Z
    .locals 1
    .parameter "skitchId"

    .prologue
    .line 22
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 43
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    if-nez v2, :cond_0

    .line 48
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/youdao/note/ui/skitch/ISkitchMeta;

    .line 45
    .local v1, meta:Lcom/youdao/note/ui/skitch/ISkitchMeta;
    invoke-interface {v1}, Lcom/youdao/note/ui/skitch/ISkitchMeta;->onDestroy()V

    goto :goto_1

    .line 47
    .end local v1           #meta:Lcom/youdao/note/ui/skitch/ISkitchMeta;
    :cond_1
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    goto :goto_0
.end method

.method public getSkitchMeta(Ljava/lang/String;)Lcom/youdao/note/ui/skitch/ISkitchMeta;
    .locals 1
    .parameter "skitchId"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 39
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/SkitchMetaManager;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/skitch/ISkitchMeta;

    goto :goto_0
.end method
