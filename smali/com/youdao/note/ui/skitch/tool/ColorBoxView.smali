.class public Lcom/youdao/note/ui/skitch/tool/ColorBoxView;
.super Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;
.source "ColorBoxView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$Doodle;


# static fields
.field private static final color1:Landroid/graphics/Bitmap;

.field public static final rgb1:I


# instance fields
.field private final color2:Landroid/graphics/Bitmap;

.field private final color3:Landroid/graphics/Bitmap;

.field private final color4:Landroid/graphics/Bitmap;

.field private final color5:Landroid/graphics/Bitmap;

.field private final color6:Landroid/graphics/Bitmap;

.field private colors:[Landroid/graphics/Bitmap;

.field private final colors1:Landroid/graphics/Bitmap;

.field private final colors2:Landroid/graphics/Bitmap;

.field private final colors3:Landroid/graphics/Bitmap;

.field private final colors4:Landroid/graphics/Bitmap;

.field private final colors5:Landroid/graphics/Bitmap;

.field private final colors6:Landroid/graphics/Bitmap;

.field private r1:Landroid/graphics/RectF;

.field private r2:Landroid/graphics/RectF;

.field private r3:Landroid/graphics/RectF;

.field private r4:Landroid/graphics/RectF;

.field private r5:Landroid/graphics/RectF;

.field private r6:Landroid/graphics/RectF;

.field private rects:[Landroid/graphics/RectF;

.field private final rgb2:I

.field private final rgb3:I

.field private final rgb4:I

.field private final rgb5:I

.field private final rgb6:I

.field private final rgbs:[I

.field private final selectedColors:[Landroid/graphics/Bitmap;

.field private final unSelectedColors:[Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/YNoteApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    .line 45
    sget-object v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sget-object v2, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    sput v0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb1:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .parameter "context"

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x6

    .line 62
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001f

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    .line 20
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020020

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    .line 21
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020021

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    .line 22
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020022

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    .line 23
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020023

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    .line 24
    new-array v0, v3, [Landroid/graphics/Bitmap;

    sget-object v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->unSelectedColors:[Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020024

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    .line 27
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020025

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020026

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    .line 29
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020027

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020028

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    .line 31
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020029

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    .line 32
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->selectedColors:[Landroid/graphics/Bitmap;

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    .line 37
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    .line 38
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    .line 40
    new-array v0, v3, [Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    .line 42
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    .line 46
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    .line 47
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    .line 48
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    .line 49
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    .line 50
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    .line 51
    new-array v0, v3, [I

    sget v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb1:I

    aput v1, v0, v4

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    aput v1, v0, v5

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    aput v1, v0, v6

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    aput v1, v0, v7

    const/4 v1, 0x4

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x6

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001f

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    .line 20
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020020

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    .line 21
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020021

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    .line 22
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020022

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    .line 23
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020023

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    .line 24
    new-array v0, v3, [Landroid/graphics/Bitmap;

    sget-object v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->unSelectedColors:[Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020024

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    .line 27
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020025

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020026

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    .line 29
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020027

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020028

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    .line 31
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020029

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    .line 32
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->selectedColors:[Landroid/graphics/Bitmap;

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    .line 37
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    .line 38
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    .line 40
    new-array v0, v3, [Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    .line 42
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    .line 46
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    .line 47
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    .line 48
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    .line 49
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    .line 50
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    .line 51
    new-array v0, v3, [I

    sget v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb1:I

    aput v1, v0, v4

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    aput v1, v0, v5

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    aput v1, v0, v6

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    aput v1, v0, v7

    const/4 v1, 0x4

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x6

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001f

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    .line 20
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020020

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    .line 21
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020021

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    .line 22
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020022

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    .line 23
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020023

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    .line 24
    new-array v0, v3, [Landroid/graphics/Bitmap;

    sget-object v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->unSelectedColors:[Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020024

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    .line 27
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020025

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020026

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    .line 29
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020027

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020028

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    .line 31
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020029

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    .line 32
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors1:Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors2:Landroid/graphics/Bitmap;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors3:Landroid/graphics/Bitmap;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors4:Landroid/graphics/Bitmap;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors5:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors6:Landroid/graphics/Bitmap;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->selectedColors:[Landroid/graphics/Bitmap;

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    .line 36
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    .line 37
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    .line 38
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    .line 40
    new-array v0, v3, [Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r1:Landroid/graphics/RectF;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r2:Landroid/graphics/RectF;

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r3:Landroid/graphics/RectF;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r4:Landroid/graphics/RectF;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r5:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->r6:Landroid/graphics/RectF;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    .line 42
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    .line 46
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color2:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    .line 47
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color3:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    .line 48
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color4:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    .line 49
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color5:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    .line 50
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->color6:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    .line 51
    new-array v0, v3, [I

    sget v1, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb1:I

    aput v1, v0, v4

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb2:I

    aput v1, v0, v5

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb3:I

    aput v1, v0, v6

    iget v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb4:I

    aput v1, v0, v7

    const/4 v1, 0x4

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb5:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgb6:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    .line 55
    return-void
.end method

.method private currentColor()I
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;

    check-cast v0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;

    .line 127
    .local v0, doodleView:Lcom/youdao/note/ui/skitch/doodle/DoodleView;
    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->getPaintColor()I

    move-result v1

    return v1
.end method

.method private selectColor(I)V
    .locals 3
    .parameter "index"

    .prologue
    .line 115
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 116
    if-ne v0, p1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->selectedColors:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 115
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->unSelectedColors:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    goto :goto_1

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->setColor(I)V

    .line 123
    return-void
.end method

.method private setColor(I)V
    .locals 1
    .parameter "color"

    .prologue
    .line 131
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;

    check-cast v0, Lcom/youdao/note/ui/skitch/doodle/DoodleView;

    .line 132
    .local v0, doodleView:Lcom/youdao/note/ui/skitch/doodle/DoodleView;
    invoke-virtual {v0, p1}, Lcom/youdao/note/ui/skitch/doodle/DoodleView;->setPaintColor(I)V

    .line 133
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter "canvas"

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x0

    .line 100
    invoke-super {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->onDraw(Landroid/graphics/Canvas;)V

    .line 101
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->currentColor()I

    move-result v0

    .line 102
    .local v0, color:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-ge v1, v7, :cond_0

    .line 103
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    aget v2, v2, v1

    if-ne v0, v2, :cond_1

    .line 104
    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->selectColor(I)V

    .line 108
    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v7, :cond_2

    .line 109
    iget-object v2, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v1

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->colors:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v4, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 102
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :cond_2
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 11
    .parameter "newW"
    .parameter "newH"
    .parameter "oldw"
    .parameter "oldh"

    .prologue
    .line 68
    invoke-super {p0, p1, p2, p3, p4}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->onSizeChanged(IIII)V

    .line 69
    int-to-float v8, p1

    sget v9, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->PAD:F

    const/high16 v10, 0x4040

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    sget v9, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->PAD:F

    const/high16 v10, 0x40a0

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    const/high16 v9, 0x40c0

    div-float v6, v8, v9

    .line 70
    .local v6, w1:F
    int-to-float v8, p2

    const/high16 v9, 0x4000

    sget v10, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->MARGIN_VERTICAL:F

    mul-float/2addr v9, v10

    sub-float v7, v8, v9

    .line 71
    .local v7, w2:F
    cmpg-float v8, v6, v7

    if-gez v8, :cond_0

    move v5, v6

    .line 72
    .local v5, w:F
    :goto_0
    div-int/lit8 v8, p2, 0x2

    int-to-float v8, v8

    const/high16 v9, 0x4000

    div-float v9, v5, v9

    sub-float v4, v8, v9

    .line 73
    .local v4, top:F
    add-float v0, v4, v5

    .line 74
    .local v0, bottom:F
    int-to-float v8, p1

    const/high16 v9, 0x40c0

    mul-float/2addr v9, v5

    sub-float/2addr v8, v9

    const/high16 v9, 0x4100

    div-float v3, v8, v9

    .line 75
    .local v3, pad:F
    int-to-float v8, p1

    const/high16 v9, 0x40c0

    mul-float/2addr v9, v5

    sub-float/2addr v8, v9

    const/high16 v9, 0x40a0

    mul-float/2addr v9, v3

    sub-float/2addr v8, v9

    const/high16 v9, 0x4000

    div-float v2, v8, v9

    .line 76
    .local v2, index:F
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    const/4 v8, 0x6

    if-ge v1, v8, :cond_1

    .line 77
    iget-object v8, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    aget-object v8, v8, v1

    add-float v9, v2, v5

    invoke-virtual {v8, v2, v4, v9, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 76
    add-int/lit8 v1, v1, 0x1

    add-float v8, v3, v5

    add-float/2addr v2, v8

    goto :goto_1

    .end local v0           #bottom:F
    .end local v1           #i:I
    .end local v2           #index:F
    .end local v3           #pad:F
    .end local v4           #top:F
    .end local v5           #w:F
    :cond_0
    move v5, v7

    .line 71
    goto :goto_0

    .line 79
    .restart local v0       #bottom:F
    .restart local v1       #i:I
    .restart local v2       #index:F
    .restart local v3       #pad:F
    .restart local v4       #top:F
    .restart local v5       #w:F
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter "event"

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 86
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rects:[Landroid/graphics/RectF;

    aget-object v1, v1, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->setLastTouchTime(Ljava/lang/Long;)V

    .line 89
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->rgbs:[I

    aget v1, v1, v0

    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->setColor(I)V

    .line 90
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/ColorBoxView;->postInvalidate()V

    .line 91
    const/4 v1, 0x1

    .line 95
    .end local v0           #i:I
    :goto_1
    return v1

    .line 86
    .restart local v0       #i:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    .end local v0           #i:I
    :cond_1
    invoke-super {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1
.end method
