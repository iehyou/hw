.class public Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;
.super Landroid/view/View;
.source "AbstractPaintTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;
    }
.end annotation


# instance fields
.field protected dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

.field protected skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    :goto_0
    return-void

    .line 33
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f80

    const v2, 0x3dcccccd

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 34
    .local v0, animation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 35
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->setAnimation(Landroid/view/animation/Animation;)V

    .line 36
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSkitchCanvas(Lcom/youdao/note/ui/skitch/ISkitch;)V
    .locals 0
    .parameter "skitchCanvas"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;

    .line 60
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 48
    :goto_0
    return-void

    .line 43
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3dcccccd

    const/high16 v2, 0x3f80

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 44
    .local v0, animation:Landroid/view/animation/AlphaAnimation;
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 45
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->startAnimation(Landroid/view/animation/Animation;)V

    .line 46
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->setVisibility(I)V

    .line 47
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->startDismissTimer()V

    goto :goto_0
.end method

.method public startDismissTimer()V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->cancel(Z)Z

    .line 54
    :cond_0
    new-instance v0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;-><init>(Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;Ljava/lang/Long;)V

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    .line 55
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 56
    return-void
.end method
