.class public Lcom/youdao/note/ui/skitch/tool/PaintSliderView;
.super Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;
.source "PaintSliderView.java"

# interfaces
.implements Lcom/youdao/note/ui/skitch/SkitchConsts$HandWrite;


# instance fields
.field private final sliderBg:Landroid/graphics/Bitmap;

.field private final sliderBlock:Landroid/graphics/Bitmap;

.field private final sliderBlue:Landroid/graphics/Bitmap;

.field private final sliderBrown:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020069

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020069

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    .line 26
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    .line 28
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020069

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    .line 30
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    .line 40
    return-void
.end method

.method private currentWidth()F
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;

    invoke-interface {v0}, Lcom/youdao/note/ui/skitch/ISkitch;->getPaintWidth()F

    move-result v0

    return v0
.end method

.method private getSliderValue()F
    .locals 6

    .prologue
    .line 152
    sget v3, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    add-float/2addr v3, v4

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    add-float v0, v3, v4

    .line 153
    .local v0, sliderLeft:F
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float/2addr v3, v4

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sub-float/2addr v3, v4

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float v1, v3, v4

    .line 154
    .local v1, sliderRight:F
    sub-float v2, v1, v0

    .line 155
    .local v2, sliderW:F
    invoke-direct {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->currentWidth()F

    move-result v3

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    sub-float/2addr v3, v4

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    sub-float/2addr v4, v5

    div-float/2addr v3, v4

    mul-float/2addr v3, v2

    return v3
.end method

.method private updateCurrentWidth(F)V
    .locals 8
    .parameter "sliderWidgetX"

    .prologue
    .line 141
    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    add-float/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    add-float v0, v4, v5

    .line 142
    .local v0, sliderLeft:F
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sub-float/2addr v4, v5

    sget v5, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float v1, v4, v5

    .line 143
    .local v1, sliderRight:F
    sub-float v2, v1, v0

    .line 144
    .local v2, sliderW:F
    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    sub-float v5, p1, v0

    div-float/2addr v5, v2

    sget v6, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    add-float v3, v4, v5

    .line 145
    .local v3, t:F
    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    sget v4, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_0

    .line 146
    iget-object v4, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->skitchCanvas:Lcom/youdao/note/ui/skitch/ISkitch;

    invoke-interface {v4, v3}, Lcom/youdao/note/ui/skitch/ISkitch;->setPaintWidth(F)V

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->invalidate()V

    .line 149
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .parameter "canvas"

    .prologue
    .line 63
    invoke-super/range {p0 .. p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->onDraw(Landroid/graphics/Canvas;)V

    .line 64
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 66
    .local v2, paint:Landroid/graphics/Paint;
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_BG_H:F

    sub-float v1, v9, v10

    .line 67
    .local v1, bottom_top:F
    invoke-direct/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getSliderValue()F

    move-result v8

    .line 68
    .local v8, sliderValue:F
    sget v9, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    add-float/2addr v9, v10

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    add-float v4, v9, v10

    .line 69
    .local v4, sliderLeft:F
    add-float v9, v4, v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    add-float v5, v9, v10

    .line 70
    .local v5, sliderMid:F
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getWidth()I

    move-result v9

    int-to-float v9, v9

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float/2addr v9, v10

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sub-float/2addr v9, v10

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float v6, v9, v10

    .line 71
    .local v6, sliderRight:F
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v9, v1

    const/high16 v10, 0x4000

    div-float/2addr v9, v10

    add-float/2addr v9, v1

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_H:F

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    sub-float v7, v9, v10

    .line 72
    .local v7, sliderTop:F
    sget v9, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_H:F

    add-float v3, v7, v9

    .line 74
    .local v3, sliderBottom:F
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 75
    const/high16 v9, 0x4040

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 76
    const/high16 v9, -0x100

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBg:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getWidth()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v14

    int-to-float v14, v14

    invoke-direct {v11, v12, v1, v13, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 81
    const/high16 v9, 0x4000

    div-float v9, v4, v9

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v10, v1

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float/2addr v10, v1

    sget v11, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    const/high16 v12, 0x4000

    div-float/2addr v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 83
    sget v9, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    add-float/2addr v9, v6

    sget v10, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    const/high16 v11, 0x4040

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v10, v1

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float/2addr v10, v1

    sget v11, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    const/high16 v12, 0x4040

    div-float/2addr v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 85
    sget v9, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    add-float/2addr v9, v4

    add-float/2addr v9, v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    add-float/2addr v9, v10

    const/high16 v10, 0x4000

    div-float v10, v1, v10

    invoke-direct/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->currentWidth()F

    move-result v11

    const/high16 v12, 0x4000

    div-float/2addr v11, v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 87
    sub-float v9, v5, v4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_0

    .line 88
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11, v4, v7, v5, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 96
    :goto_0
    sub-float v9, v6, v5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v10, v10

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_1

    .line 97
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v11, v6

    add-float/2addr v11, v5

    float-to-int v11, v11

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11, v5, v7, v6, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 106
    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    add-float v12, v4, v8

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v13

    int-to-float v13, v13

    sub-float/2addr v13, v1

    const/high16 v14, 0x4000

    div-float/2addr v13, v14

    add-float/2addr v13, v1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    sub-float/2addr v13, v14

    add-float v14, v4, v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    int-to-float v15, v15

    add-float/2addr v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v15

    int-to-float v15, v15

    sub-float/2addr v15, v1

    const/high16 v16, 0x4000

    div-float v15, v15, v16

    add-float/2addr v15, v1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    div-int/lit8 v16, v16, 0x2

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v15, v15, v16

    invoke-direct {v11, v12, v13, v14, v15}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 110
    return-void

    .line 91
    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    add-float/2addr v12, v4

    invoke-direct {v11, v4, v7, v12, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlue:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    add-float/2addr v12, v4

    invoke-direct {v11, v12, v7, v5, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 100
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    sub-float v12, v6, v12

    invoke-direct {v11, v12, v7, v6, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-direct {v10, v11, v12, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBrown:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    int-to-float v12, v12

    sub-float v12, v6, v12

    invoke-direct {v11, v5, v7, v12, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .parameter "event"

    .prologue
    const/4 v5, 0x1

    .line 114
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 136
    const/4 v5, 0x0

    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 119
    :pswitch_1
    iget-object v6, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->dismissTimer:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->setLastTouchTime(Ljava/lang/Long;)V

    .line 120
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_BG_H:F

    sub-float v0, v6, v7

    .line 121
    .local v0, bottom_top:F
    sget v6, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MIN_PAINT_WIDTH:F

    add-float/2addr v6, v7

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_L_PAD:F

    add-float v1, v6, v7

    .line 122
    .local v1, sliderLeft:F
    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float/2addr v6, v7

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->MAX_PAINT_WIDTH:F

    sub-float/2addr v6, v7

    sget v7, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->SLIDER_R_PAD:F

    sub-float v2, v6, v7

    .line 124
    .local v2, sliderRight:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 125
    .local v3, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 126
    .local v4, y:F
    cmpl-float v6, v3, v1

    if-ltz v6, :cond_1

    iget-object v6, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float v6, v2, v6

    cmpg-float v6, v3, v6

    if-gtz v6, :cond_1

    cmpl-float v6, v4, v0

    if-ltz v6, :cond_1

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-gtz v6, :cond_1

    .line 128
    invoke-direct {p0, v3}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->updateCurrentWidth(F)V

    goto :goto_0

    .line 129
    :cond_1
    cmpg-float v6, v3, v1

    if-gez v6, :cond_2

    cmpl-float v6, v4, v0

    if-ltz v6, :cond_2

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-gtz v6, :cond_2

    .line 130
    invoke-direct {p0, v1}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->updateCurrentWidth(F)V

    goto :goto_0

    .line 131
    :cond_2
    cmpl-float v6, v3, v2

    if-lez v6, :cond_0

    cmpl-float v6, v4, v0

    if-ltz v6, :cond_0

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-gtz v6, :cond_0

    .line 132
    iget-object v6, p0, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->sliderBlock:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float v6, v2, v6

    invoke-direct {p0, v6}, Lcom/youdao/note/ui/skitch/tool/PaintSliderView;->updateCurrentWidth(F)V

    goto/16 :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
