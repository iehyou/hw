.class public Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;
.super Landroid/os/AsyncTask;
.source "AbstractPaintTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DismissTimer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final dismissTime:J = 0xbb8L


# instance fields
.field private lastTouchTime:Ljava/lang/Long;

.field final synthetic this$0:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;


# direct methods
.method public constructor <init>(Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;Ljava/lang/Long;)V
    .locals 0
    .parameter
    .parameter "lastTouchTime"

    .prologue
    .line 66
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->this$0:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 67
    iput-object p2, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->lastTouchTime:Ljava/lang/Long;

    .line 68
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 62
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .parameter "params"

    .prologue
    .line 85
    :goto_0
    const-wide/16 v2, 0xbb8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->getLastTouchTime()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sub-long v0, v2, v4

    .line 86
    .local v0, sleepTime:J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 87
    const/4 v2, 0x0

    return-object v2

    .line 90
    :cond_0
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getLastTouchTime()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->lastTouchTime:Ljava/lang/Long;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->lastTouchTime:Ljava/lang/Long;

    monitor-exit v1

    return-object v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 62
    check-cast p1, Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .parameter "result"

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->this$0:Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;

    invoke-virtual {v0}, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool;->dismiss()V

    .line 101
    return-void
.end method

.method public setLastTouchTime(Ljava/lang/Long;)V
    .locals 2
    .parameter "lastTouchTime"

    .prologue
    .line 77
    iget-object v1, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->lastTouchTime:Ljava/lang/Long;

    monitor-enter v1

    .line 78
    :try_start_0
    iput-object p1, p0, Lcom/youdao/note/ui/skitch/tool/AbstractPaintTool$DismissTimer;->lastTouchTime:Ljava/lang/Long;

    .line 79
    monitor-exit v1

    .line 80
    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
