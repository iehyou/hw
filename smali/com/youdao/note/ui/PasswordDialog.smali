.class public Lcom/youdao/note/ui/PasswordDialog;
.super Landroid/app/Dialog;
.source "PasswordDialog.java"


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter "ctx"

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object v2, p0, Lcom/youdao/note/ui/PasswordDialog;->mInflater:Landroid/view/LayoutInflater;

    .line 26
    iput-object v2, p0, Lcom/youdao/note/ui/PasswordDialog;->mLayout:Landroid/view/ViewGroup;

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/PasswordDialog;->mInflater:Landroid/view/LayoutInflater;

    .line 34
    iget-object v0, p0, Lcom/youdao/note/ui/PasswordDialog;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03003c

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/youdao/note/ui/PasswordDialog;->mLayout:Landroid/view/ViewGroup;

    .line 35
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/youdao/note/ui/PasswordDialog;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/PasswordDialog;->setContentView(Landroid/view/View;)V

    .line 42
    return-void
.end method
