.class public Lcom/youdao/note/ui/ImageRectificationView;
.super Landroid/view/View;
.source "ImageRectificationView.java"

# interfaces
.implements Lcom/youdao/note/activity/ActivityConsts;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;,
        Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    }
.end annotation


# static fields
.field private static final ANIMATION_FRAMES:I = 0x7

.field private static final ANIMATION_SHRINK_RATIO:F = 0.8f

.field private static final DASHED_LINE_SEGMENTS:I = 0x10

.field private static final DASHED_LINE_SPACES_RATIO:F = 0.18f

.field public static final EVENT_AUTO_DETECT:I = 0x1

.field public static final EVENT_CANCEL_TASK:I = 0x5

.field public static final EVENT_COMPLETE:I = 0x4

.field public static final EVENT_ROTATE_ANTICLOCKWISE:I = 0x3

.field public static final EVENT_ROTATE_CLOCKWISE:I = 0x2

.field private static final SCALE_SIZE:F = 0.8f

.field private static final WIDTH_BORDER:F = 1.5f

.field private static final WIDTH_LINE:F = 1.0f

.field private static final crossLength:I = 0x14


# instance fields
.field private CENTRAL_POINT:Landroid/graphics/PointF;

.field private SCREEN_HEIGHT:I

.field private SCREEN_WIDTH:I

.field private areaX:F

.field private areaY:F

.field private autoDetect:Z

.field private autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

.field private convertAutoDetectHandler:Landroid/os/Handler;

.field firstDraw:Z

.field private imageRotateDegree:I

.field private imageScale:F

.field private imageVertices:[Landroid/graphics/PointF;

.field private mImage:Landroid/graphics/Bitmap;

.field private mImageSrc:Landroid/graphics/Bitmap;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mPaint:Landroid/graphics/Paint;

.field public menuHandler:Landroid/os/Handler;

.field private moving:Z

.field private movingPointIndex:I

.field private movingPointInitial:Landroid/graphics/PointF;

.field private points:[Landroid/graphics/PointF;

.field private pointsAutoDetect:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private pointsBakForDrag:[Landroid/graphics/PointF;

.field private rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

.field private toastHandler:Landroid/os/Handler;

.field private touchDownPoint:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .parameter "context"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 70
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    .line 108
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    .line 123
    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    .line 138
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    .line 142
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    .line 147
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    .line 151
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    .line 157
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->firstDraw:Z

    .line 221
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z

    .line 223
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;

    .line 462
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$1;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->menuHandler:Landroid/os/Handler;

    .line 504
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$2;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->convertAutoDetectHandler:Landroid/os/Handler;

    .line 517
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$3;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->toastHandler:Landroid/os/Handler;

    .line 878
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 880
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    .line 882
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    .line 884
    iput-boolean v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    .line 108
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    .line 123
    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    .line 138
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    .line 142
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    .line 147
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    .line 151
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    .line 157
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->firstDraw:Z

    .line 221
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z

    .line 223
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;

    .line 462
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$1;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->menuHandler:Landroid/os/Handler;

    .line 504
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$2;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->convertAutoDetectHandler:Landroid/os/Handler;

    .line 517
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$3;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->toastHandler:Landroid/os/Handler;

    .line 878
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 880
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    .line 882
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    .line 884
    iput-boolean v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    .line 108
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    .line 123
    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    .line 138
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    .line 142
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    .line 147
    new-array v0, v1, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    .line 151
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    .line 157
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->firstDraw:Z

    .line 221
    iput-boolean v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z

    .line 223
    iput-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;

    .line 462
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$1;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->menuHandler:Landroid/os/Handler;

    .line 504
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$2;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->convertAutoDetectHandler:Landroid/os/Handler;

    .line 517
    new-instance v0, Lcom/youdao/note/ui/ImageRectificationView$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/ImageRectificationView$3;-><init>(Lcom/youdao/note/ui/ImageRectificationView;)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->toastHandler:Landroid/os/Handler;

    .line 878
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 880
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    .line 882
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    .line 884
    iput-boolean v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/ImageRectificationView;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z

    return v0
.end method

.method static synthetic access$002(Lcom/youdao/note/ui/ImageRectificationView;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetect:Z

    return p1
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/ImageRectificationView;)Ljava/util/List;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/youdao/note/ui/ImageRectificationView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    return v0
.end method

.method static synthetic access$102(Lcom/youdao/note/ui/ImageRectificationView;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsAutoDetect:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/youdao/note/ui/ImageRectificationView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/youdao/note/ui/ImageRectificationView;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    return v0
.end method

.method static synthetic access$1300(Lcom/youdao/note/ui/ImageRectificationView;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    return v0
.end method

.method static synthetic access$1400(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Paint;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->toastHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;)Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->autoDetectTask:Lcom/youdao/note/ui/ImageRectificationView$AutoDetectTask;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/youdao/note/ui/ImageRectificationView;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->rotate(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/youdao/note/ui/ImageRectificationView;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/youdao/note/ui/ImageRectificationView;Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;)Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->rectificationTask:Lcom/youdao/note/ui/ImageRectificationView$RectificationTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/ImageRectificationView;)[Landroid/graphics/PointF;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$302(Lcom/youdao/note/ui/ImageRectificationView;[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 59
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    return-object p1
.end method

.method static synthetic access$400(Lcom/youdao/note/ui/ImageRectificationView;Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/ImageRectificationView;->convertPoints(Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/ui/ImageRectificationView;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/youdao/note/ui/ImageRectificationView;->setInitialPoints()V

    return-void
.end method

.method static synthetic access$600(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->convertAutoDetectHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/youdao/note/ui/ImageRectificationView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    return v0
.end method

.method static synthetic access$800(Lcom/youdao/note/ui/ImageRectificationView;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    return v0
.end method

.method static synthetic access$900(Lcom/youdao/note/ui/ImageRectificationView;)Landroid/graphics/Matrix;
    .locals 1
    .parameter "x0"

    .prologue
    .line 59
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private varargs convertPoints(Z[Landroid/graphics/PointF;)[Landroid/graphics/PointF;
    .locals 11
    .parameter "fromOriginal"
    .parameter "points"

    .prologue
    const/4 v10, 0x4

    const/high16 v9, 0x3f80

    .line 531
    if-eqz p2, :cond_0

    array-length v5, p2

    if-eq v5, v10, :cond_2

    :cond_0
    move-object v2, p2

    .line 580
    :cond_1
    return-object v2

    .line 534
    :cond_2
    new-array v2, v10, [Landroid/graphics/PointF;

    .line 535
    .local v2, result:[Landroid/graphics/PointF;
    if-eqz p1, :cond_3

    .line 536
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v10, :cond_1

    .line 537
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 538
    .local v3, x:F
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 539
    .local v4, y:F
    const/4 v1, 0x0

    .line 540
    .local v1, point:Landroid/graphics/PointF;
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    div-int/lit8 v5, v5, 0x5a

    packed-switch v5, :pswitch_data_0

    .line 554
    :goto_1
    aput-object v1, v2, v0

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 542
    :pswitch_0
    new-instance v1, Landroid/graphics/PointF;

    .end local v1           #point:Landroid/graphics/PointF;
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    invoke-direct {v1, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 543
    .restart local v1       #point:Landroid/graphics/PointF;
    goto :goto_1

    .line 545
    :pswitch_1
    new-instance v1, Landroid/graphics/PointF;

    .end local v1           #point:Landroid/graphics/PointF;
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float v7, v9, v4

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v3

    add-float/2addr v6, v7

    invoke-direct {v1, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 546
    .restart local v1       #point:Landroid/graphics/PointF;
    goto :goto_1

    .line 548
    :pswitch_2
    new-instance v1, Landroid/graphics/PointF;

    .end local v1           #point:Landroid/graphics/PointF;
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float v7, v9, v3

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float v8, v9, v4

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-direct {v1, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 549
    .restart local v1       #point:Landroid/graphics/PointF;
    goto :goto_1

    .line 551
    :pswitch_3
    new-instance v1, Landroid/graphics/PointF;

    .end local v1           #point:Landroid/graphics/PointF;
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v4

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float v8, v9, v3

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-direct {v1, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .restart local v1       #point:Landroid/graphics/PointF;
    goto :goto_1

    .line 557
    .end local v0           #i:I
    .end local v1           #point:Landroid/graphics/PointF;
    .end local v3           #x:F
    .end local v4           #y:F
    :cond_3
    const/4 v0, 0x0

    .restart local v0       #i:I
    :goto_2
    if-ge v0, v10, :cond_1

    .line 558
    const/4 v3, 0x0

    .restart local v3       #x:F
    const/4 v4, 0x0

    .line 559
    .restart local v4       #y:F
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    div-int/lit8 v5, v5, 0x5a

    packed-switch v5, :pswitch_data_1

    .line 577
    :goto_3
    new-instance v5, Landroid/graphics/PointF;

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v4

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v5, v2, v0

    .line 557
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 561
    :pswitch_4
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 562
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 563
    goto :goto_3

    .line 565
    :pswitch_5
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 566
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    aget-object v6, p2, v0

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 567
    goto :goto_3

    .line 569
    :pswitch_6
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    aget-object v6, p2, v0

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 570
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    aget-object v6, p2, v0

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 571
    goto/16 :goto_3

    .line 573
    :pswitch_7
    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    aget-object v6, p2, v0

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 574
    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v4, v5, v6

    goto/16 :goto_3

    .line 540
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 559
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private determineBoundaryPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 12
    .parameter "from"
    .parameter "to"

    .prologue
    .line 812
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-direct {p0, p2}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-direct {p0, p2}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 813
    :cond_1
    const/4 v7, 0x0

    .line 836
    :cond_2
    return-object v7

    .line 815
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 816
    .local v5, probablePoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v1, v8, :cond_5

    .line 817
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    aget-object v8, v8, v1

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    add-int/lit8 v10, v1, 0x1

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v11, v11

    rem-int/2addr v10, v11

    aget-object v9, v9, v10

    invoke-static {p1, p2, v8, v9}, Lcom/youdao/note/tool/img/ImageProcess;->getLineSegmentIntersectionPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    .line 819
    .local v4, point:Landroid/graphics/PointF;
    if-eqz v4, :cond_4

    .line 820
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 816
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 823
    .end local v4           #point:Landroid/graphics/PointF;
    :cond_5
    const/4 v7, 0x0

    .line 824
    .local v7, result:Landroid/graphics/PointF;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 825
    .restart local v4       #point:Landroid/graphics/PointF;
    if-nez v7, :cond_7

    .line 826
    move-object v7, v4

    goto :goto_1

    .line 828
    :cond_7
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v8

    if-eqz v8, :cond_8

    move-object v6, p2

    .line 829
    .local v6, refPoint:Landroid/graphics/PointF;
    :goto_2
    invoke-static {v7, v6}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v3

    .line 830
    .local v3, min:F
    invoke-static {v4, v6}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    .line 831
    .local v0, current:F
    cmpg-float v8, v0, v3

    if-gez v8, :cond_6

    .line 832
    move-object v7, v4

    goto :goto_1

    .end local v0           #current:F
    .end local v3           #min:F
    .end local v6           #refPoint:Landroid/graphics/PointF;
    :cond_8
    move-object v6, p1

    .line 828
    goto :goto_2
.end method

.method private determineScale(II)F
    .locals 5
    .parameter "width"
    .parameter "height"

    .prologue
    const v4, 0x3f4ccccd

    .line 796
    const/high16 v0, 0x3f80

    .line 797
    .local v0, xScale:F
    const/high16 v1, 0x3f80

    .line 798
    .local v1, yScale:F
    int-to-float v2, p1

    iget v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    int-to-float v3, v3

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 799
    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 801
    :cond_0
    int-to-float v2, p2

    iget v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    int-to-float v3, v3

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 802
    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    int-to-float v3, p2

    div-float v1, v2, v3

    .line 804
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    return v2
.end method

.method private drawCross(Landroid/graphics/PointF;Landroid/graphics/Canvas;)V
    .locals 7
    .parameter "p"
    .parameter "canvas"

    .prologue
    const/high16 v6, 0x4120

    .line 1066
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1067
    .local v5, paint:Landroid/graphics/Paint;
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1068
    const/high16 v0, -0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1069
    const/high16 v0, 0x3fc0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1070
    iget v0, p1, Landroid/graphics/PointF;->x:F

    sub-float v1, v0, v6

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v0, p1, Landroid/graphics/PointF;->x:F

    add-float v3, v0, v6

    iget v4, p1, Landroid/graphics/PointF;->y:F

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1071
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v0, p1, Landroid/graphics/PointF;->y:F

    sub-float v2, v0, v6

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v0, p1, Landroid/graphics/PointF;->y:F

    add-float v4, v0, v6

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1072
    return-void
.end method

.method private drawDashedLine(FFFFLandroid/graphics/Canvas;)V
    .locals 10
    .parameter "startX"
    .parameter "startY"
    .parameter "stopX"
    .parameter "stopY"
    .parameter "canvas"

    .prologue
    const v9, 0x3eb851ec

    const v8, 0x3e3851ec

    const/high16 v7, 0x4180

    .line 1048
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1049
    .local v5, paint:Landroid/graphics/Paint;
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1050
    const/high16 v0, 0x3f80

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1051
    const v0, -0xffff01

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1052
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    const/16 v0, 0x10

    if-ge v6, v0, :cond_0

    .line 1053
    sub-float v0, p3, p1

    int-to-float v1, v6

    mul-float/2addr v0, v1

    div-float/2addr v0, v7

    add-float v1, p1, v0

    sub-float v0, p4, p2

    int-to-float v2, v6

    mul-float/2addr v0, v2

    div-float/2addr v0, v7

    add-float v2, p2, v0

    sub-float v0, p3, p1

    add-int/lit8 v3, v6, 0x1

    int-to-float v3, v3

    sub-float/2addr v3, v9

    mul-float/2addr v0, v3

    div-float/2addr v0, v7

    add-float v3, p1, v0

    sub-float v0, p4, p2

    add-int/lit8 v4, v6, 0x1

    int-to-float v4, v4

    sub-float/2addr v4, v9

    mul-float/2addr v0, v4

    div-float/2addr v0, v7

    add-float v4, p2, v0

    move-object v0, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1057
    sub-float v0, p3, p1

    add-int/lit8 v1, v6, 0x1

    int-to-float v1, v1

    sub-float/2addr v1, v8

    mul-float/2addr v0, v1

    div-float/2addr v0, v7

    add-float/2addr v0, p1

    sub-float v1, p4, p2

    add-int/lit8 v2, v6, 0x1

    int-to-float v2, v2

    sub-float/2addr v2, v8

    mul-float/2addr v1, v2

    div-float/2addr v1, v7

    add-float/2addr v1, p2

    invoke-virtual {p5, v0, v1, v5}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 1052
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1060
    :cond_0
    return-void
.end method

.method private varargs drawShadow(Landroid/graphics/Canvas;[Landroid/graphics/PointF;)V
    .locals 7
    .parameter "canvas"
    .parameter "p"

    .prologue
    const/4 v6, 0x0

    .line 775
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 776
    .local v1, message:Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    array-length v4, p2

    if-ge v0, v4, :cond_1

    .line 777
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "p["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]:("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_0

    const-string v4, ""

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 776
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 777
    :cond_0
    const-string v4, ";"

    goto :goto_1

    .line 779
    :cond_1
    const-string v4, "DRAWSHADOW"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 781
    .local v3, path:Landroid/graphics/Path;
    if-eqz p2, :cond_2

    array-length v4, p2

    const/4 v5, 0x3

    if-ge v4, v5, :cond_3

    .line 793
    :cond_2
    :goto_2
    return-void

    .line 784
    :cond_3
    aget-object v4, p2, v6

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aget-object v5, p2, v6

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 785
    const/4 v0, 0x1

    :goto_3
    array-length v4, p2

    if-ge v0, v4, :cond_4

    .line 786
    aget-object v4, p2, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aget-object v5, p2, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 785
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 788
    :cond_4
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 789
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 790
    .local v2, paint:Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 791
    const/16 v4, 0x96

    invoke-virtual {v2, v4, v6, v6, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 792
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private drawShadows(Landroid/graphics/Canvas;)V
    .locals 29
    .parameter "canvas"

    .prologue
    .line 587
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v13, message:Ljava/lang/StringBuffer;
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v7, v0, :cond_0

    .line 589
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "p["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "]:("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ");"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 588
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 591
    :cond_0
    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v7, v0, :cond_1

    .line 592
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "i["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "]:("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ");"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 591
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 594
    :cond_1
    const-string v23, "POINTS CONTEXT"

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 596
    .local v19, pointList:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    const/4 v5, 0x0

    .line 597
    .local v5, checkImageVertex:Z
    const/4 v7, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v7, v0, :cond_12

    .line 598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_3

    .line 601
    const/4 v10, 0x0

    .local v10, j:I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_3

    .line 602
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    add-int/lit8 v25, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v24, v24, v25

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->determineBoundaryPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v17

    .line 603
    .local v17, point:Landroid/graphics/PointF;
    if-eqz v17, :cond_4

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v17, v23, v24

    invoke-static/range {v23 .. v23}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_4

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_2

    .line 605
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 607
    :cond_2
    const/4 v5, 0x1

    .line 597
    .end local v10           #j:I
    .end local v17           #point:Landroid/graphics/PointF;
    :cond_3
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 601
    .restart local v10       #j:I
    .restart local v17       #point:Landroid/graphics/PointF;
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 613
    .end local v10           #j:I
    .end local v17           #point:Landroid/graphics/PointF;
    :cond_5
    const/16 v21, 0x0

    .line 614
    .local v21, previous:Landroid/graphics/PointF;
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_6

    .line 615
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21           #previous:Landroid/graphics/PointF;
    check-cast v21, Landroid/graphics/PointF;

    .line 617
    .restart local v21       #previous:Landroid/graphics/PointF;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->isInPicture(Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 618
    const/4 v10, 0x0

    .restart local v10       #j:I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_3

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    add-int/lit8 v25, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v24, v24, v25

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->determineBoundaryPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v17

    .line 620
    .restart local v17       #point:Landroid/graphics/PointF;
    if-eqz v17, :cond_9

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v17, v23, v24

    invoke-static/range {v23 .. v23}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_9

    .line 621
    if-eqz v21, :cond_7

    .line 622
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->findVertexPointsBetween(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 624
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_8

    .line 625
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 627
    :cond_8
    const/4 v5, 0x0

    .line 628
    goto/16 :goto_4

    .line 618
    :cond_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_5

    .line 632
    .end local v10           #j:I
    .end local v17           #point:Landroid/graphics/PointF;
    :cond_a
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 633
    .local v9, intersectionPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    const/4 v10, 0x0

    .restart local v10       #j:I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_e

    .line 634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    add-int/lit8 v25, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v26, v0

    add-int/lit8 v27, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    rem-int v27, v27, v28

    aget-object v26, v26, v27

    invoke-static/range {v23 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->getLineSegmentIntersectionPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v17

    .line 636
    .restart local v17       #point:Landroid/graphics/PointF;
    if-eqz v17, :cond_d

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v17, v23, v24

    invoke-static/range {v23 .. v23}, Lcom/youdao/note/tool/img/ImageProcess;->containsInfinitePoint([Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_d

    .line 637
    const/4 v6, 0x0

    .line 638
    .local v6, contains:Z
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :cond_b
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/graphics/PointF;

    .line 639
    .local v18, pointF:Landroid/graphics/PointF;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 640
    const/4 v6, 0x1

    .line 644
    .end local v18           #pointF:Landroid/graphics/PointF;
    :cond_c
    if-nez v6, :cond_d

    .line 645
    move-object/from16 v0, v17

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 633
    .end local v6           #contains:Z
    .end local v8           #i$:Ljava/util/Iterator;
    :cond_d
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_6

    .line 649
    .end local v17           #point:Landroid/graphics/PointF;
    :cond_e
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v23

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_f

    .line 650
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/PointF;

    .line 651
    .local v3, a:Landroid/graphics/PointF;
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/PointF;

    .line 652
    .local v4, b:Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v12, v23, v7

    .line 653
    .local v12, m:Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v15, v23, v24

    .line 654
    .local v15, n:Landroid/graphics/PointF;
    iget v0, v15, Landroid/graphics/PointF;->x:F

    move/from16 v23, v0

    iget v0, v12, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    sub-float v23, v23, v24

    iget v0, v4, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    iget v0, v3, Landroid/graphics/PointF;->x:F

    move/from16 v25, v0

    sub-float v24, v24, v25

    mul-float v23, v23, v24

    iget v0, v15, Landroid/graphics/PointF;->y:F

    move/from16 v24, v0

    iget v0, v12, Landroid/graphics/PointF;->y:F

    move/from16 v25, v0

    sub-float v24, v24, v25

    iget v0, v4, Landroid/graphics/PointF;->y:F

    move/from16 v25, v0

    iget v0, v3, Landroid/graphics/PointF;->y:F

    move/from16 v26, v0

    sub-float v25, v25, v26

    mul-float v24, v24, v25

    add-float v23, v23, v24

    const/16 v24, 0x0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_f

    .line 655
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-interface {v9, v0, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 656
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-interface {v9, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 659
    .end local v3           #a:Landroid/graphics/PointF;
    .end local v4           #b:Landroid/graphics/PointF;
    .end local v12           #m:Landroid/graphics/PointF;
    .end local v15           #n:Landroid/graphics/PointF;
    :cond_f
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v23

    if-nez v23, :cond_11

    .line 660
    if-eqz v21, :cond_10

    .line 661
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->findVertexPointsBetween(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 663
    :cond_10
    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 665
    :cond_11
    const/4 v5, 0x1

    goto/16 :goto_4

    .line 669
    .end local v9           #intersectionPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    .end local v10           #j:I
    .end local v21           #previous:Landroid/graphics/PointF;
    :cond_12
    if-eqz v5, :cond_13

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_13

    .line 670
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/graphics/PointF;

    const/16 v24, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/graphics/PointF;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->findVertexPointsBetween(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/List;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 672
    :cond_13
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_14

    .line 673
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 675
    :cond_14
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [Landroid/graphics/PointF;

    .line 676
    .local v20, points:[Landroid/graphics/PointF;
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 677
    .local v14, message2:Ljava/lang/StringBuffer;
    const/4 v7, 0x0

    :goto_7
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v7, v0, :cond_15

    .line 678
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "p["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "]:("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    aget-object v24, v20, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    aget-object v24, v20, v7

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ");"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 677
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 680
    :cond_15
    const-string v23, "TRUEPOINTS"

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    const/4 v7, 0x0

    :goto_8
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v7, v0, :cond_22

    .line 682
    const/4 v10, 0x0

    .restart local v10       #j:I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v10, v0, :cond_23

    .line 683
    aget-object v23, v20, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v25, v25, v26

    add-int/lit8 v26, v7, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v26, v20, v26

    invoke-static/range {v23 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_17

    .line 682
    :cond_16
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 687
    :cond_17
    const/16 v22, 0x1

    .line 688
    .local v22, valid:Z
    const/4 v11, 0x0

    .local v11, k:I
    :goto_a
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v11, v0, :cond_19

    .line 689
    aget-object v23, v20, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    aget-object v25, v20, v11

    add-int/lit8 v26, v11, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v26, v20, v26

    invoke-static/range {v23 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_18

    add-int/lit8 v23, v7, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v24, v0

    rem-int v23, v23, v24

    aget-object v23, v20, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    add-int/lit8 v25, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v24, v24, v25

    aget-object v25, v20, v11

    add-int/lit8 v26, v11, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v26, v20, v26

    invoke-static/range {v23 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->isIntersect(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 692
    :cond_18
    const/16 v22, 0x0

    .line 714
    :cond_19
    :goto_b
    if-eqz v22, :cond_16

    .line 716
    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v25, v20, v7

    aput-object v25, v23, v24

    const/16 v24, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    aput-object v25, v23, v24

    const/16 v24, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v25, v25, v26

    aput-object v25, v23, v24

    const/16 v24, 0x3

    add-int/lit8 v25, v7, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v25, v20, v25

    aput-object v25, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->drawShadow(Landroid/graphics/Canvas;[Landroid/graphics/PointF;)V

    .line 718
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 719
    .local v16, paramPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    aget-object v23, v20, v7

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v11, v0

    :goto_c
    if-lez v11, :cond_20

    .line 721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int v24, v10, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 720
    add-int/lit8 v11, v11, -0x1

    goto :goto_c

    .line 695
    .end local v16           #paramPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    :cond_1a
    aget-object v23, v20, v7

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    add-int/lit8 v25, v7, 0x2

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v25, v20, v25

    invoke-static/range {v23 .. v25}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v25, v25, v26

    add-int/lit8 v26, v7, 0x2

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v26, v20, v26

    invoke-static/range {v24 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v24

    mul-float v23, v23, v24

    const/16 v24, 0x0

    cmpg-float v23, v23, v24

    if-ltz v23, :cond_1b

    aget-object v23, v20, v7

    add-int/lit8 v24, v7, 0x1

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    add-int/lit8 v25, v7, 0x3

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v26, v0

    rem-int v25, v25, v26

    aget-object v25, v20, v25

    invoke-static/range {v23 .. v25}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    add-int/lit8 v26, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v25, v25, v26

    add-int/lit8 v26, v7, 0x3

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v27, v0

    rem-int v26, v26, v27

    aget-object v26, v20, v26

    invoke-static/range {v24 .. v26}, Lcom/youdao/note/tool/img/ImageProcess;->crossProduct(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v24

    mul-float v23, v23, v24

    const/16 v24, 0x0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_1c

    .line 701
    :cond_1b
    const/16 v22, 0x0

    .line 702
    goto/16 :goto_b

    .line 704
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v10

    add-int/lit8 v24, v7, 0x2

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    invoke-static/range {v23 .. v24}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    add-int/lit8 v24, v7, 0x3

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    invoke-static/range {v23 .. v24}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-nez v23, :cond_1e

    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    aget-object v23, v23, v10

    add-int/lit8 v24, v7, 0x3

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    invoke-static/range {v23 .. v24}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v23, v0

    add-int/lit8 v24, v10, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v23, v23, v24

    add-int/lit8 v24, v7, 0x2

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    rem-int v24, v24, v25

    aget-object v24, v20, v24

    invoke-static/range {v23 .. v24}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v23

    if-eqz v23, :cond_1f

    .line 710
    :cond_1e
    const/16 v22, 0x0

    .line 711
    goto/16 :goto_b

    .line 688
    :cond_1f
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_a

    .line 723
    .restart local v16       #paramPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    :cond_20
    const/4 v11, 0x1

    :goto_d
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v11, v0, :cond_21

    .line 724
    add-int v23, v7, v11

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v24, v0

    rem-int v23, v23, v24

    aget-object v23, v20, v23

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 723
    add-int/lit8 v11, v11, 0x1

    goto :goto_d

    .line 726
    :cond_21
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v23

    new-array v0, v0, [Landroid/graphics/PointF;

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v23

    check-cast v23, [Landroid/graphics/PointF;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/youdao/note/ui/ImageRectificationView;->drawShadow(Landroid/graphics/Canvas;[Landroid/graphics/PointF;)V

    .line 731
    .end local v10           #j:I
    .end local v11           #k:I
    .end local v16           #paramPoints:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    .end local v22           #valid:Z
    :cond_22
    return-void

    .line 681
    .restart local v10       #j:I
    :cond_23
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_8
.end method

.method private drawVertexLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Canvas;)V
    .locals 8
    .parameter "a"
    .parameter "b"
    .parameter "canvas"

    .prologue
    .line 1078
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1079
    .local v5, paint:Landroid/graphics/Paint;
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1080
    const/high16 v0, -0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1081
    const/high16 v0, 0x3fc0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1082
    const v6, 0x3d4ccccd

    .line 1083
    .local v6, vertexLineRatio:F
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    add-float/2addr v3, v0

    iget v0, p1, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    iget v7, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v7

    mul-float/2addr v4, v6

    add-float/2addr v4, v0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1084
    return-void
.end method

.method private findVertexPointsBetween(Landroid/graphics/PointF;Landroid/graphics/PointF;)Ljava/util/List;
    .locals 11
    .parameter "a"
    .parameter "b"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 737
    const/4 v0, 0x0

    .local v0, aPos:I
    const/4 v1, 0x0

    .line 738
    .local v1, bPos:I
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v7, v7

    if-ge v4, v7, :cond_0

    .line 739
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    aget-object v7, v7, v4

    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    add-int/lit8 v9, v4, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v8, v8, v9

    invoke-static {v7, v8, p1}, Lcom/youdao/note/tool/img/ImageProcess;->isInSameLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 740
    move v0, v4

    .line 744
    :cond_0
    const/4 v4, 0x0

    :goto_1
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v7, v7

    if-ge v4, v7, :cond_1

    .line 745
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    aget-object v7, v7, v4

    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    add-int/lit8 v9, v4, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v8, v8, v9

    invoke-static {v7, v8, p2}, Lcom/youdao/note/tool/img/ImageProcess;->isInSameLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 746
    move v1, v4

    .line 750
    :cond_1
    if-ge v1, v0, :cond_2

    .line 751
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v7, v7

    add-int/2addr v1, v7

    .line 753
    :cond_2
    if-ne v1, v0, :cond_3

    .line 754
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    aget-object v2, v7, v0

    .line 755
    .local v2, c:Landroid/graphics/PointF;
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    add-int/lit8 v8, v0, 0x1

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v9, v9

    rem-int/2addr v8, v9

    aget-object v3, v7, v8

    .line 756
    .local v3, d:Landroid/graphics/PointF;
    iget v7, p2, Landroid/graphics/PointF;->x:F

    iget v8, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    iget v8, v3, Landroid/graphics/PointF;->x:F

    iget v9, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    iget v8, p2, Landroid/graphics/PointF;->y:F

    iget v9, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    iget v9, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_3

    .line 758
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v7, v7

    add-int/2addr v1, v7

    .line 761
    .end local v2           #c:Landroid/graphics/PointF;
    .end local v3           #d:Landroid/graphics/PointF;
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 762
    .local v5, list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    move v4, v0

    :goto_2
    if-ge v4, v1, :cond_7

    .line 763
    iget-object v7, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    add-int/lit8 v8, v4, 0x1

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v9, v9

    rem-int/2addr v8, v9

    aget-object v6, v7, v8

    .line 764
    .local v6, next:Landroid/graphics/PointF;
    invoke-static {p1, v6}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-static {p2, v6}, Lcom/youdao/note/tool/img/ImageProcess;->coarseEquals(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 765
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 762
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 738
    .end local v5           #list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    .end local v6           #next:Landroid/graphics/PointF;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 744
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 768
    .restart local v5       #list:Ljava/util/List;,"Ljava/util/List<Landroid/graphics/PointF;>;"
    :cond_7
    return-object v5
.end method

.method private isInPicture(Landroid/graphics/PointF;)Z
    .locals 5
    .parameter "p"

    .prologue
    .line 1040
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-static {v0, v1, p1}, Lcom/youdao/note/tool/img/ImageProcess;->isInRectangle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v0

    return v0
.end method

.method private onImageChanged(Z)V
    .locals 9
    .parameter "setLocation"

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 841
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 842
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 843
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    iget v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 844
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 845
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 846
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->mMatrix:Landroid/graphics/Matrix;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    .line 847
    if-eqz p1, :cond_0

    .line 848
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    .line 849
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    .line 851
    :cond_0
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v0, v0

    if-ge v7, v0, :cond_2

    .line 852
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    aget-object v0, v0, v7

    if-nez v0, :cond_1

    .line 853
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, v7

    .line 851
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 856
    :cond_2
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    div-int/lit8 v8, v0, 0x5a

    .line 857
    .local v8, indexAdjust:I
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 858
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 859
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 860
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 861
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 862
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 863
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x3

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 864
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x3

    sub-int/2addr v1, v8

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageVertices:[Landroid/graphics/PointF;

    array-length v2, v2

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 865
    return-void
.end method

.method private rotate(Z)V
    .locals 8
    .parameter "clockwise"

    .prologue
    const/4 v7, 0x1

    .line 1015
    iget v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    if-eqz p1, :cond_0

    const/16 v3, 0x5a

    :goto_0
    add-int/2addr v3, v4

    add-int/lit16 v3, v3, 0x168

    rem-int/lit16 v3, v3, 0x168

    iput v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    .line 1018
    iget v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageRotateDegree:I

    rem-int/lit16 v3, v3, 0xb4

    if-nez v3, :cond_1

    .line 1019
    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/youdao/note/ui/ImageRectificationView;->determineScale(II)F

    move-result v2

    .line 1023
    .local v2, nextScale:F
    :goto_1
    const/4 v1, 0x0

    .local v1, j:I
    :goto_2
    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 1024
    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->CENTRAL_POINT:Landroid/graphics/PointF;

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    invoke-static {v4, v5, v6, v2, p1}, Lcom/youdao/note/tool/img/ImageProcess;->rotatePoint(Landroid/graphics/PointF;Landroid/graphics/PointF;FFZ)Landroid/graphics/PointF;

    move-result-object v4

    aput-object v4, v3, v1

    .line 1023
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1015
    .end local v1           #j:I
    .end local v2           #nextScale:F
    :cond_0
    const/16 v3, -0x5a

    goto :goto_0

    .line 1021
    :cond_1
    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-direct {p0, v3, v4}, Lcom/youdao/note/ui/ImageRectificationView;->determineScale(II)F

    move-result v2

    .restart local v2       #nextScale:F
    goto :goto_1

    .line 1027
    .restart local v1       #j:I
    :cond_2
    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    .line 1028
    invoke-direct {p0, v7}, Lcom/youdao/note/ui/ImageRectificationView;->onImageChanged(Z)V

    .line 1030
    new-instance v0, Landroid/view/animation/RotateAnimation;

    if-eqz p1, :cond_3

    const/high16 v3, -0x3d4c

    :goto_3
    const/4 v4, 0x0

    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/view/animation/RotateAnimation;-><init>(FFFF)V

    .line 1031
    .local v0, animation:Landroid/view/animation/Animation;
    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1032
    invoke-virtual {v0, v7}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1033
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/ImageRectificationView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1034
    return-void

    .line 1030
    .end local v0           #animation:Landroid/view/animation/Animation;
    :cond_3
    const/high16 v3, 0x42b4

    goto :goto_3
.end method

.method private setInitialPoints()V
    .locals 8

    .prologue
    .line 868
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 869
    .local v0, x:I
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 870
    .local v1, y:I
    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    .line 871
    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    .line 872
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v3, 0x0

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 873
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v3, 0x1

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 874
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v3, 0x3

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    int-to-float v7, v1

    add-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 875
    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v3, 0x2

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    iget v6, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    int-to-float v7, v1

    add-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v2, v3

    .line 876
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 1

    .prologue
    .line 1087
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1090
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1091
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1093
    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .parameter "canvas"

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x3

    const/high16 v10, 0x3e80

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 161
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 219
    :cond_0
    return-void

    .line 164
    :cond_1
    iget-boolean v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->firstDraw:Z

    if-eqz v0, :cond_2

    .line 165
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 167
    invoke-virtual {p0}, Lcom/youdao/note/ui/ImageRectificationView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    .line 168
    invoke-virtual {p0}, Lcom/youdao/note/ui/ImageRectificationView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    .line 169
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_WIDTH:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->SCREEN_HEIGHT:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->CENTRAL_POINT:Landroid/graphics/PointF;

    .line 171
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/youdao/note/ui/ImageRectificationView;->determineScale(II)F

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    .line 172
    const-string v0, "First onImageChanged"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-direct {p0, v9}, Lcom/youdao/note/ui/ImageRectificationView;->onImageChanged(Z)V

    .line 175
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->menuHandler:Landroid/os/Handler;

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 177
    invoke-direct {p0}, Lcom/youdao/note/ui/ImageRectificationView;->setInitialPoints()V

    .line 178
    iput-boolean v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->firstDraw:Z

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 181
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 182
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3fc0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 184
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImage:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    iget v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 186
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 187
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 188
    .local v7, path:Landroid/graphics/Path;
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v8

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 189
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v9

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v12

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v12

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v11

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v11

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 193
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 195
    const/4 v6, 0x1

    .local v6, i:I
    :goto_0
    const/4 v0, 0x4

    if-ge v6, v0, :cond_3

    .line 196
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v9

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v2, v2, v8

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v10

    int-to-float v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v2, v2, v9

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v10

    int-to-float v3, v6

    mul-float/2addr v2, v3

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v11

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v3, v3, v12

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v4, v4, v11

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v10

    int-to-float v4, v6

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v11

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v4, v4, v12

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v5, v5, v11

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, v10

    int-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v4, v0

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/ui/ImageRectificationView;->drawDashedLine(FFFFLandroid/graphics/Canvas;)V

    .line 199
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v1, v1, v11

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v2, v2, v8

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v10

    int-to-float v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v8

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v2, v2, v11

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v10

    int-to-float v3, v6

    mul-float/2addr v2, v3

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v3, v3, v12

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v4, v4, v9

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v10

    int-to-float v4, v6

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v9

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v4, v4, v12

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v5, v5, v9

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, v10

    int-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v4, v0

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/youdao/note/ui/ImageRectificationView;->drawDashedLine(FFFFLandroid/graphics/Canvas;)V

    .line 195
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 203
    :cond_3
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawShadows(Landroid/graphics/Canvas;)V

    .line 204
    iget-boolean v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    if-eqz v0, :cond_5

    .line 205
    const/4 v6, 0x0

    :goto_1
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 206
    iget v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    if-ne v0, v6, :cond_4

    .line 207
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    invoke-direct {p0, v0, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawCross(Landroid/graphics/PointF;Landroid/graphics/Canvas;)V

    .line 205
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 209
    :cond_4
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    add-int/lit8 v2, v6, -0x1

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    rem-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawVertexLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Canvas;)V

    .line 210
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    add-int/lit8 v2, v6, 0x1

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    rem-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawVertexLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 214
    :cond_5
    const/4 v6, 0x0

    :goto_3
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    add-int/lit8 v2, v6, -0x1

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    rem-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawVertexLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Canvas;)V

    .line 216
    iget-object v0, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    add-int/lit8 v2, v6, 0x1

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v3, v3

    rem-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/youdao/note/ui/ImageRectificationView;->drawVertexLine(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Canvas;)V

    .line 214
    add-int/lit8 v6, v6, 0x1

    goto :goto_3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .parameter "event"

    .prologue
    .line 888
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 889
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 890
    .local v0, action:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    .line 891
    .local v6, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    .line 892
    .local v7, y:F
    if-nez v0, :cond_3

    .line 893
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    .line 895
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iput v6, v8, Landroid/graphics/PointF;->x:F

    .line 896
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iput v7, v8, Landroid/graphics/PointF;->y:F

    .line 898
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    sub-float/2addr v8, v6

    const/high16 v9, 0x4000

    mul-float/2addr v8, v9

    add-float/2addr v8, v6

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaX:F

    .line 899
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    sub-float/2addr v8, v7

    const/high16 v9, 0x4000

    mul-float/2addr v8, v9

    add-float/2addr v8, v7

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->areaY:F

    .line 900
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v2, v8, :cond_0

    .line 901
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    new-instance v9, Landroid/graphics/PointF;

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v10, v10, v2

    iget v10, v10, Landroid/graphics/PointF;->x:F

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v11, v11, v2

    iget v11, v11, Landroid/graphics/PointF;->y:F

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v9, v8, v2

    .line 902
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v8, v8, v2

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v6

    const/high16 v10, 0x4000

    mul-float/2addr v9, v10

    add-float/2addr v9, v6

    iput v9, v8, Landroid/graphics/PointF;->x:F

    .line 903
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v8, v8, v2

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v7

    const/high16 v10, 0x4000

    mul-float/2addr v9, v10

    add-float/2addr v9, v7

    iput v9, v8, Landroid/graphics/PointF;->y:F

    .line 900
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 905
    :cond_0
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    const/high16 v9, 0x4000

    mul-float/2addr v8, v9

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    .line 906
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/youdao/note/ui/ImageRectificationView;->onImageChanged(Z)V

    .line 908
    const/4 v8, 0x0

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 909
    const v4, 0x7f7fffff

    .line 910
    .local v4, minDistance:F
    const/4 v2, 0x0

    :goto_1
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v2, v8, :cond_2

    .line 911
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v8, v8, v2

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    invoke-static {v8, v9}, Lcom/youdao/note/tool/img/ImageProcess;->getDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v1

    .line 912
    .local v1, distance:F
    cmpg-float v8, v1, v4

    if-gez v8, :cond_1

    .line 913
    move v4, v1

    .line 914
    iput v2, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 910
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 918
    .end local v1           #distance:F
    :cond_2
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iput v9, v8, Landroid/graphics/PointF;->x:F

    .line 919
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v9, v9, v10

    iget v9, v9, Landroid/graphics/PointF;->y:F

    iput v9, v8, Landroid/graphics/PointF;->y:F

    .line 920
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iput v6, v8, Landroid/graphics/PointF;->x:F

    .line 921
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iput v7, v8, Landroid/graphics/PointF;->y:F

    .line 922
    invoke-virtual {p0}, Lcom/youdao/note/ui/ImageRectificationView;->postInvalidate()V

    .line 923
    const/4 v8, 0x1

    .line 1010
    .end local v2           #i:I
    .end local v4           #minDistance:F
    :goto_2
    return v8

    .line 924
    :cond_3
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_f

    .line 925
    const/4 v8, 0x2

    if-ne v0, v8, :cond_9

    .line 926
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    sub-float v9, v6, v9

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    add-float/2addr v9, v10

    iput v9, v8, Landroid/graphics/PointF;->x:F

    .line 927
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    sub-float v9, v7, v9

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointInitial:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    add-float/2addr v9, v10

    iput v9, v8, Landroid/graphics/PointF;->y:F

    .line 928
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x2

    aget-object v10, v10, v11

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v12, 0x3

    aget-object v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 929
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v11, v11, 0x2

    iget-object v12, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v12, v12

    rem-int/2addr v11, v12

    aget-object v10, v10, v11

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v12, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v12, v12, 0x3

    iget-object v13, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v13, v13

    rem-int/2addr v12, v13

    aget-object v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 932
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v5, v8, v9

    .line 933
    .local v5, temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 934
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aput-object v5, v8, v9

    .line 935
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v5, v8, v9

    .line 936
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 937
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aput-object v5, v8, v9

    .line 938
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v8, v8, 0x1

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v9, v9

    rem-int/2addr v8, v9

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 951
    .end local v5           #temp:Landroid/graphics/PointF;
    :cond_4
    :goto_3
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    invoke-static {v8}, Lcom/youdao/note/tool/img/ImageProcess;->isClockwise([Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 952
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 953
    .local v3, message:Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_4
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v2, v8, :cond_6

    .line 954
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "p["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]:("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ");"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 953
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 939
    .end local v2           #i:I
    .end local v3           #message:Ljava/lang/StringBuffer;
    :cond_5
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x3

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v10, v10, 0x1

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v11, v11

    rem-int/2addr v10, v11

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v11, v11, 0x2

    iget-object v12, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v12, v12

    rem-int/2addr v11, v12

    aget-object v10, v10, v11

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v12, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 942
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x3

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v5, v8, v9

    .line 943
    .restart local v5       #temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x3

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 944
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aput-object v5, v8, v9

    .line 945
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x3

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    aget-object v5, v8, v9

    .line 946
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v9, v9, 0x3

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    array-length v10, v10

    rem-int/2addr v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 947
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aput-object v5, v8, v9

    .line 948
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    add-int/lit8 v8, v8, 0x3

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v9, v9

    rem-int/2addr v8, v9

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    goto/16 :goto_3

    .line 956
    .end local v5           #temp:Landroid/graphics/PointF;
    .restart local v2       #i:I
    .restart local v3       #message:Ljava/lang/StringBuffer;
    :cond_6
    const-string v8, "ANTICLOCKWISE"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aget-object v5, v8, v9

    .line 958
    .restart local v5       #temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 959
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x3

    aput-object v5, v8, v9

    .line 960
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aget-object v5, v8, v9

    .line 961
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 962
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    const/4 v9, 0x3

    aput-object v5, v8, v9

    .line 963
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_8

    .line 964
    const/4 v8, 0x1

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 969
    .end local v2           #i:I
    .end local v3           #message:Ljava/lang/StringBuffer;
    .end local v5           #temp:Landroid/graphics/PointF;
    :cond_7
    :goto_5
    invoke-virtual {p0}, Lcom/youdao/note/ui/ImageRectificationView;->postInvalidate()V

    .line 970
    const/4 v8, 0x1

    goto/16 :goto_2

    .line 965
    .restart local v2       #i:I
    .restart local v3       #message:Ljava/lang/StringBuffer;
    .restart local v5       #temp:Landroid/graphics/PointF;
    :cond_8
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    .line 966
    const/4 v8, 0x3

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    goto :goto_5

    .line 971
    .end local v2           #i:I
    .end local v3           #message:Ljava/lang/StringBuffer;
    .end local v5           #temp:Landroid/graphics/PointF;
    :cond_9
    const/4 v8, 0x1

    if-ne v0, v8, :cond_f

    .line 973
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_6
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v2, v8, :cond_a

    .line 974
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v8, v8, v2

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->x:F

    iput v9, v8, Landroid/graphics/PointF;->x:F

    .line 975
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v8, v8, v2

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->y:F

    iput v9, v8, Landroid/graphics/PointF;->y:F

    .line 976
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->pointsBakForDrag:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aput-object v9, v8, v2

    .line 973
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 978
    :cond_a
    iget v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    const/high16 v9, 0x4000

    div-float/2addr v8, v9

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->imageScale:F

    .line 979
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/youdao/note/ui/ImageRectificationView;->onImageChanged(Z)V

    .line 981
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v8, v8, v9

    iget v9, v8, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float v10, v6, v10

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, v8, Landroid/graphics/PointF;->x:F

    .line 982
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    iget v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    aget-object v8, v8, v9

    iget v9, v8, Landroid/graphics/PointF;->y:F

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->touchDownPoint:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    sub-float v10, v7, v10

    const/high16 v11, 0x4000

    div-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, v8, Landroid/graphics/PointF;->y:F

    .line 983
    const/4 v8, -0x1

    iput v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->movingPointIndex:I

    .line 984
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->moving:Z

    .line 985
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x2

    aget-object v10, v10, v11

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v12, 0x3

    aget-object v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 986
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    iget-object v11, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v12, 0x2

    aget-object v11, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/youdao/note/tool/img/ImageProcess;->isValidPointsOrder(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 987
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    aget-object v5, v8, v9

    .line 988
    .restart local v5       #temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 989
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x3

    aput-object v5, v8, v9

    .line 996
    .end local v5           #temp:Landroid/graphics/PointF;
    :cond_b
    :goto_7
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    invoke-static {v8}, Lcom/youdao/note/tool/img/ImageProcess;->isClockwise([Landroid/graphics/PointF;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 997
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 998
    .restart local v3       #message:Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    :goto_8
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    array-length v8, v8

    if-ge v2, v8, :cond_d

    .line 999
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "p["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]:("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->x:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    aget-object v9, v9, v2

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ");"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 998
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 991
    .end local v3           #message:Ljava/lang/StringBuffer;
    :cond_c
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    aget-object v5, v8, v9

    .line 992
    .restart local v5       #temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 993
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aput-object v5, v8, v9

    goto :goto_7

    .line 1001
    .end local v5           #temp:Landroid/graphics/PointF;
    .restart local v3       #message:Ljava/lang/StringBuffer;
    :cond_d
    const-string v8, "ANTICLOCKWISE"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aget-object v5, v8, v9

    .line 1003
    .restart local v5       #temp:Landroid/graphics/PointF;
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    aput-object v10, v8, v9

    .line 1004
    iget-object v8, p0, Lcom/youdao/note/ui/ImageRectificationView;->points:[Landroid/graphics/PointF;

    const/4 v9, 0x3

    aput-object v5, v8, v9

    .line 1006
    .end local v3           #message:Ljava/lang/StringBuffer;
    .end local v5           #temp:Landroid/graphics/PointF;
    :cond_e
    invoke-virtual {p0}, Lcom/youdao/note/ui/ImageRectificationView;->postInvalidate()V

    .line 1007
    const/4 v8, 0x1

    goto/16 :goto_2

    .line 1010
    .end local v2           #i:I
    :cond_f
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    goto/16 :goto_2
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter "image"

    .prologue
    .line 154
    iput-object p1, p0, Lcom/youdao/note/ui/ImageRectificationView;->mImageSrc:Landroid/graphics/Bitmap;

    .line 155
    return-void
.end method
