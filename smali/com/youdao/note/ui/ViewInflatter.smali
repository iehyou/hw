.class public Lcom/youdao/note/ui/ViewInflatter;
.super Ljava/lang/Object;
.source "ViewInflatter.java"


# static fields
.field public static labelFormator:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/youdao/note/ui/ViewInflatter;->labelFormator:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inflateSectionLabel(Landroid/app/Activity;Lcom/youdao/note/data/NoteMeta;)Landroid/view/View;
    .locals 6
    .parameter "activity"
    .parameter "noteMeta"

    .prologue
    .line 30
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030007

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 31
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_0

    .line 32
    const v2, 0x7f070017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 33
    .local v0, textView:Landroid/widget/TextView;
    sget-object v2, Lcom/youdao/note/ui/ViewInflatter;->labelFormator:Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/youdao/note/data/NoteMeta;->getModifyTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    .end local v0           #textView:Landroid/widget/TextView;
    :cond_0
    return-object v1
.end method
