.class public Lcom/youdao/note/ui/AlphabetScrollbar;
.super Landroid/widget/LinearLayout;
.source "AlphabetScrollbar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;,
        Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;
    }
.end annotation


# static fields
.field public static final BUNDLE_ALPHABET:Ljava/lang/String; = "alphabet"

.field public static final MSG_ALPHABET:I = 0x1

.field private static final alphabet:[Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private firstDraw:Z

.field public handler:Landroid/os/Handler;

.field private lastIndex:I

.field private lastSelectedView:Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

.field private listener:Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;

.field private scrollbarTop:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "#"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "A"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "B"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "C"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "D"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "E"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "F"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "G"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "H"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "I"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "J"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "K"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "L"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "M"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "N"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "O"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "P"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Q"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "R"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "S"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "T"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "U"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "W"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "X"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "Z"

    aput-object v2, v0, v1

    sput-object v0, Lcom/youdao/note/ui/AlphabetScrollbar;->alphabet:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->firstDraw:Z

    .line 111
    new-instance v0, Lcom/youdao/note/ui/AlphabetScrollbar$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/AlphabetScrollbar$2;-><init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->handler:Landroid/os/Handler;

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I

    .line 43
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->context:Landroid/content/Context;

    .line 44
    invoke-direct {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->init()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->firstDraw:Z

    .line 111
    new-instance v0, Lcom/youdao/note/ui/AlphabetScrollbar$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/AlphabetScrollbar$2;-><init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->handler:Landroid/os/Handler;

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I

    .line 37
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->context:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->init()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->firstDraw:Z

    .line 111
    new-instance v0, Lcom/youdao/note/ui/AlphabetScrollbar$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/AlphabetScrollbar$2;-><init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->handler:Landroid/os/Handler;

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I

    .line 31
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->context:Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->init()V

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/AlphabetScrollbar;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->scrollbarTop:I

    return v0
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/AlphabetScrollbar;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/youdao/note/ui/AlphabetScrollbar;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 23
    iput p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/AlphabetScrollbar;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/AlphabetScrollbar;->hightLightAlphabet(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/AlphabetScrollbar;)Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->listener:Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/youdao/note/ui/AlphabetScrollbar;->alphabet:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/ui/AlphabetScrollbar;)Landroid/content/Context;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->context:Landroid/content/Context;

    return-object v0
.end method

.method private hightLightAlphabet(I)V
    .locals 2
    .parameter "index"

    .prologue
    .line 101
    if-gez p1, :cond_0

    const/4 p1, 0x0

    .line 102
    :cond_0
    const/16 v1, 0x1a

    if-le p1, v1, :cond_1

    const/16 p1, 0x1a

    .line 103
    :cond_1
    invoke-virtual {p0, p1}, Lcom/youdao/note/ui/AlphabetScrollbar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

    .line 108
    .local v0, childView:Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;
    iput-object v0, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->lastSelectedView:Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

    .line 109
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    .line 65
    sget-object v0, Lcom/youdao/note/ui/AlphabetScrollbar;->alphabet:[Ljava/lang/String;

    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 66
    .local v3, s:Ljava/lang/String;
    new-instance v4, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

    invoke-direct {v4, p0, v3}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;-><init>(Lcom/youdao/note/ui/AlphabetScrollbar;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/youdao/note/ui/AlphabetScrollbar;->addView(Landroid/view/View;)V

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v3           #s:Ljava/lang/String;
    :cond_0
    new-instance v4, Lcom/youdao/note/ui/AlphabetScrollbar$1;

    invoke-direct {v4, p0}, Lcom/youdao/note/ui/AlphabetScrollbar$1;-><init>(Lcom/youdao/note/ui/AlphabetScrollbar;)V

    invoke-virtual {p0, v4}, Lcom/youdao/note/ui/AlphabetScrollbar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 98
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 158
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->setBackgroundColor(I)V

    .line 159
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->setTextColor(I)V

    .line 160
    invoke-virtual {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->postInvalidate()V

    .line 161
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .parameter "canvas"

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 56
    iget-boolean v1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->firstDraw:Z

    if-eqz v1, :cond_0

    .line 57
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 58
    .local v0, location:[I
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->getLocationOnScreen([I)V

    .line 59
    const/4 v1, 0x1

    aget v1, v0, v1

    iput v1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->scrollbarTop:I

    .line 60
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->firstDraw:Z

    .line 62
    .end local v0           #location:[I
    :cond_0
    return-void
.end method

.method public setOnAlphabetListener(Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 137
    iput-object p1, p0, Lcom/youdao/note/ui/AlphabetScrollbar;->listener:Lcom/youdao/note/ui/AlphabetScrollbar$OnAlphabetListener;

    .line 138
    return-void
.end method

.method public setTextColor(I)V
    .locals 5
    .parameter "color"

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->getChildCount()I

    move-result v1

    .line 171
    .local v1, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 172
    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 173
    .local v3, view:Landroid/view/View;
    instance-of v4, v3, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

    if-eqz v4, :cond_0

    move-object v2, v3

    .line 174
    check-cast v2, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;

    .line 175
    .local v2, tv:Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;
    invoke-virtual {v2, p1}, Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;->setTextColor(I)V

    .line 171
    .end local v2           #tv:Lcom/youdao/note/ui/AlphabetScrollbar$AlphabetView;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    .end local v3           #view:Landroid/view/View;
    :cond_1
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 165
    const-string v0, "#FFFFFF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/AlphabetScrollbar;->setTextColor(I)V

    .line 166
    invoke-virtual {p0}, Lcom/youdao/note/ui/AlphabetScrollbar;->postInvalidate()V

    .line 167
    return-void
.end method
