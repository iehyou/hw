.class Lcom/youdao/note/ui/SyncProgressBar$1;
.super Landroid/os/Handler;
.source "SyncProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/SyncProgressBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/SyncProgressBar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/SyncProgressBar;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    const/4 v3, 0x1

    .line 50
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    invoke-virtual {v1}, Lcom/youdao/note/ui/SyncProgressBar;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_0

    .line 65
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    #getter for: Lcom/youdao/note/ui/SyncProgressBar;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/youdao/note/ui/SyncProgressBar;->access$000(Lcom/youdao/note/ui/SyncProgressBar;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v0

    .line 66
    .local v0, currentProgress:I
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    #getter for: Lcom/youdao/note/ui/SyncProgressBar;->mNextProgress:I
    invoke-static {v1}, Lcom/youdao/note/ui/SyncProgressBar;->access$100(Lcom/youdao/note/ui/SyncProgressBar;)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 67
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    add-int/lit8 v2, v0, 0x1

    #calls: Lcom/youdao/note/ui/SyncProgressBar;->innerSetProgress(I)V
    invoke-static {v1, v2}, Lcom/youdao/note/ui/SyncProgressBar;->access$200(Lcom/youdao/note/ui/SyncProgressBar;I)V

    .line 68
    const-wide/16 v1, 0x64

    invoke-virtual {p0, v3, v1, v2}, Lcom/youdao/note/ui/SyncProgressBar$1;->sendEmptyMessageDelayed(IJ)Z

    .line 70
    :cond_2
    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/youdao/note/ui/SyncProgressBar;->setVisibility(I)V

    .line 72
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    #getter for: Lcom/youdao/note/ui/SyncProgressBar;->mListener:Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;
    invoke-static {v1}, Lcom/youdao/note/ui/SyncProgressBar;->access$300(Lcom/youdao/note/ui/SyncProgressBar;)Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 73
    iget-object v1, p0, Lcom/youdao/note/ui/SyncProgressBar$1;->this$0:Lcom/youdao/note/ui/SyncProgressBar;

    #getter for: Lcom/youdao/note/ui/SyncProgressBar;->mListener:Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;
    invoke-static {v1}, Lcom/youdao/note/ui/SyncProgressBar;->access$300(Lcom/youdao/note/ui/SyncProgressBar;)Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/youdao/note/ui/SyncProgressBar$SyncAnimationListener;->onAnimationFinished()V

    .line 75
    :cond_3
    invoke-virtual {p0, v3}, Lcom/youdao/note/ui/SyncProgressBar$1;->removeMessages(I)V

    goto :goto_0
.end method
