.class public interface abstract Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;
.super Ljava/lang/Object;
.source "AudioPlayerBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/AudioPlayerBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioPlayListener"
.end annotation


# virtual methods
.method public abstract onClose()V
.end method

.method public abstract onCompletion(Landroid/media/MediaPlayer;)V
.end method

.method public abstract onError(Landroid/media/MediaPlayer;II)Z
.end method

.method public abstract onSeekComplete(Landroid/media/MediaPlayer;)V
.end method
