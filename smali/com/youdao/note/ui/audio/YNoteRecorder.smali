.class public Lcom/youdao/note/ui/audio/YNoteRecorder;
.super Ljava/lang/Object;
.source "YNoteRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;,
        Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    }
.end annotation


# static fields
.field public static final ERROR_RECORD_FAILED:I = 0x1

.field public static final ERROR_SAVE_FAILED:I = 0x2

.field public static final ERROR_SPACE_LIMIT:I = 0x3

.field public static final INITIALIZED:I = 0x1

.field private static final MESSAGE_SAVE_FAILED:I = 0x3

.field private static final MESSAGE_SPACE_LIMIT:I = 0x4

.field private static final MESSAGE_START_RECORD_FAILED:I = 0x2

.field private static final MESSAGE_VOLUME:I = 0x1

.field public static final PAUSED:I = 0x8

.field public static final RECORDING:I = 0x2

.field private static loadLibSuccess:Z


# instance fields
.field private mBufferSize:I

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

.field private mRecordThread:Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;

.field private mSampleInRate:I

.field private mState:Ljava/lang/Integer;

.field private output:Ljava/io/FileOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    const/4 v1, 0x1

    sput-boolean v1, Lcom/youdao/note/ui/audio/YNoteRecorder;->loadLibSuccess:Z

    .line 33
    :try_start_0
    const-string v1, "ynote_lib"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .local v0, e:Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 34
    .end local v0           #e:Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 35
    .restart local v0       #e:Ljava/lang/UnsatisfiedLinkError;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/youdao/note/ui/audio/YNoteRecorder;->loadLibSuccess:Z

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/File;I)V
    .locals 3
    .parameter "file"
    .parameter "sampleInRate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    .line 69
    new-instance v0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/audio/YNoteRecorder$1;-><init>(Lcom/youdao/note/ui/audio/YNoteRecorder;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;

    .line 96
    iput p2, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I

    .line 97
    iget v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mBufferSize:I

    .line 99
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->output:Ljava/io/FileOutputStream;

    .line 100
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .parameter "path"
    .parameter "sampleInRate"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/youdao/note/ui/audio/YNoteRecorder;-><init>(Ljava/io/File;I)V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/audio/YNoteRecorder;)Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I

    return v0
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/audio/YNoteRecorder;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mBufferSize:I

    return v0
.end method

.method static synthetic access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;
    .locals 1
    .parameter "x0"

    .prologue
    .line 28
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->output:Ljava/io/FileOutputStream;

    return-object v0
.end method

.method static synthetic access$600(Lcom/youdao/note/ui/audio/YNoteRecorder;[SI)[D
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/youdao/note/ui/audio/YNoteRecorder;->convertWaveData([SI)[D

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/youdao/note/ui/audio/YNoteRecorder;[D)D
    .locals 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->volumeRMS([D)D

    move-result-wide v0

    return-wide v0
.end method

.method private convertToDouble([SI)D
    .locals 4
    .parameter "bs"
    .parameter "offset"

    .prologue
    .line 311
    aget-short v2, p1, p2

    int-to-double v0, v2

    .line 312
    .local v0, d:D
    const-wide v2, 0x40dfffc000000000L

    div-double/2addr v0, v2

    .line 313
    return-wide v0
.end method

.method private convertWaveData([SI)[D
    .locals 4
    .parameter "waveData"
    .parameter "size"

    .prologue
    .line 303
    new-array v1, p2, [D

    .line 304
    .local v1, result:[D
    const/4 v0, 0x0

    .local v0, index:I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 305
    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->convertToDouble([SI)D

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    return-object v1
.end method

.method private innerStartRecord()V
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    .line 123
    new-instance v0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;-><init>(Lcom/youdao/note/ui/audio/YNoteRecorder;Lcom/youdao/note/ui/audio/YNoteRecorder$1;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mRecordThread:Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;

    .line 124
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mRecordThread:Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->start()V

    .line 125
    return-void
.end method

.method public static isLoadLibSuccess()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/youdao/note/ui/audio/YNoteRecorder;->loadLibSuccess:Z

    return v0
.end method

.method private volumeRMS([D)D
    .locals 16
    .parameter "raw"

    .prologue
    .line 283
    const-wide/16 v8, 0x0

    .line 284
    .local v8, sum:D
    move-object/from16 v0, p1

    array-length v12, v0

    if-nez v12, :cond_0

    move-wide v6, v8

    .line 299
    :goto_0
    return-wide v6

    .line 287
    :cond_0
    const/4 v5, 0x0

    .local v5, ii:I
    :goto_1
    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v5, v12, :cond_1

    .line 288
    aget-wide v12, p1, v5

    add-double/2addr v8, v12

    .line 287
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 291
    :cond_1
    move-object/from16 v0, p1

    array-length v12, v0

    int-to-double v12, v12

    div-double v1, v8, v12

    .line 292
    .local v1, average:D
    const-wide/16 v10, 0x0

    .line 293
    .local v10, sumMeanSquare:D
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v5, v12, :cond_2

    .line 294
    aget-wide v12, p1, v5

    sub-double/2addr v12, v1

    const-wide/high16 v14, 0x4000

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    add-double/2addr v10, v12

    .line 293
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 296
    :cond_2
    move-object/from16 v0, p1

    array-length v12, v0

    int-to-double v12, v12

    div-double v3, v10, v12

    .line 297
    .local v3, averageMeanSquare:D
    const-wide/high16 v12, 0x3fe0

    invoke-static {v3, v4, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    .line 299
    .local v6, rootMeanSquare:D
    goto :goto_0
.end method


# virtual methods
.method public getRecordState()I
    .locals 2

    .prologue
    .line 144
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    monitor-enter v1

    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1

    return v0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isRecording()Z
    .locals 3

    .prologue
    .line 150
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public pause()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 131
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 133
    monitor-exit v1

    .line 141
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "pause() can only be called when the state is recording."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 139
    :cond_1
    const/16 v0, 0x8

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    .line 140
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public setVolumeChangeListener(Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 108
    iput-object p1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    .line 109
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    .line 114
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "startRecord() can only be called when the state is inilized or paused."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 117
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->innerStartRecord()V

    .line 118
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    return-void
.end method

.method public stopRecord()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 160
    iget-object v2, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    monitor-enter v2

    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 162
    monitor-exit v2

    .line 178
    :goto_0
    return-void

    .line 164
    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mState:Ljava/lang/Integer;

    .line 166
    :goto_1
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder;->mRecordThread:Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;

    invoke-virtual {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->isAlive()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 177
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
