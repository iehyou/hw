.class public Lcom/youdao/note/ui/audio/AudioRecordBar;
.super Landroid/widget/LinearLayout;
.source "AudioRecordBar.java"

# interfaces
.implements Lcom/youdao/note/service/RecordService$RecordDurationListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
.implements Lcom/youdao/note/activity/ActivityConsts;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;
    }
.end annotation


# instance fields
.field private isUserPaused:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mDurationView:Landroid/widget/TextView;

.field private mFile:Ljava/io/File;

.field private mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

.field private mPauseButton:Landroid/view/View;

.field private mRecordButton:Landroid/view/View;

.field private mRecordPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mRecordService:Lcom/youdao/note/service/RecordService;

.field private mRecordServiceConnection:Landroid/content/ServiceConnection;

.field private mRecordingView:Landroid/widget/TextView;

.field private mStopButton:Landroid/view/View;

.field private mTotalTimeMs:J

.field private mVolumeView:Lcom/youdao/note/ui/audio/VolumeView;

.field private tm:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v2, 0x1

    .line 139
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    .line 74
    iput-boolean v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z

    .line 78
    new-instance v0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/audio/AudioRecordBar$1;-><init>(Lcom/youdao/note/ui/audio/AudioRecordBar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordServiceConnection:Landroid/content/ServiceConnection;

    .line 99
    new-instance v0, Lcom/youdao/note/ui/audio/AudioRecordBar$2;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/audio/AudioRecordBar$2;-><init>(Lcom/youdao/note/ui/audio/AudioRecordBar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    new-instance v0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/audio/AudioRecordBar$3;-><init>(Lcom/youdao/note/ui/audio/AudioRecordBar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 140
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mContext:Landroid/content/Context;

    .line 141
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 143
    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mPauseButton:Landroid/view/View;

    .line 144
    const v0, 0x7f07000e

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordButton:Landroid/view/View;

    .line 145
    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mStopButton:Landroid/view/View;

    .line 146
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/youdao/note/ui/audio/VolumeView;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mVolumeView:Lcom/youdao/note/ui/audio/VolumeView;

    .line 147
    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mDurationView:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordingView:Landroid/widget/TextView;

    .line 149
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mDurationView:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mStopButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->tm:Landroid/telephony/TelephonyManager;

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/audio/AudioRecordBar;)Lcom/youdao/note/service/RecordService;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/youdao/note/ui/audio/AudioRecordBar;Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/audio/AudioRecordBar;)Ljava/io/File;
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/audio/AudioRecordBar;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->startRecord()V

    return-void
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/audio/AudioRecordBar;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z

    return v0
.end method

.method static synthetic access$302(Lcom/youdao/note/ui/audio/AudioRecordBar;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z

    return p1
.end method

.method private regist()V
    .locals 5

    .prologue
    .line 177
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/youdao/note/service/RecordService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 183
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.youdao.note.action.RECORD_PAUSE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 184
    .local v1, intentFilter:Landroid/content/IntentFilter;
    const-string v2, "com.youdao.note.action.RECORD_RECORD"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 185
    const-string v2, "com.youdao.note.action.RECORD_STOP"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    const-string v2, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 189
    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->tm:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 190
    return-void
.end method

.method private startRecord()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 272
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLoadRecortLibSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    if-eqz v1, :cond_0

    .line 275
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    invoke-virtual {v1, p0}, Lcom/youdao/note/service/RecordService;->startRecord(Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 285
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mPauseButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 286
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordingView:Landroid/widget/TextView;

    const v2, 0x7f0a0044

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 287
    iget-wide v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    invoke-virtual {p0, v1, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->onDurationChanged(J)V

    .line 291
    :goto_1
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a004a

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 278
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;->onRecordFinished(Ljava/io/File;I)V

    goto :goto_0

    .line 289
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mContext:Landroid/content/Context;

    const v2, 0x7f0a00f5

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private unRegist()V
    .locals 4

    .prologue
    .line 253
    :try_start_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 254
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 255
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->tm:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    :goto_0
    return-void

    .line 256
    :catch_0
    move-exception v0

    .line 257
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "Unbind record service failed"

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public init(Ljava/io/File;)V
    .locals 1
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-static {p1}, Lcom/youdao/note/utils/FileUtils;->createNewFile(Ljava/io/File;)Z

    .line 172
    :cond_0
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    .line 173
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->regist()V

    .line 174
    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 1
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->init(Ljava/io/File;)V

    .line 166
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    const/4 v2, 0x1

    .line 198
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f07000e

    if-ne v0, v1, :cond_1

    .line 199
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->startRecord()V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f070009

    if-ne v0, v1, :cond_2

    .line 201
    iput-boolean v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z

    .line 202
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->pauseRecord()V

    goto :goto_0

    .line 203
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f07000d

    if-ne v0, v1, :cond_0

    .line 204
    invoke-virtual {p0, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(Z)V

    goto :goto_0
.end method

.method public onDurationChanged(J)V
    .locals 5
    .parameter "time"

    .prologue
    const/4 v4, 0x1

    .line 305
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getMaxResourceSize()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 306
    const/4 v0, 0x2

    invoke-virtual {p0, v4, v4, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZZI)V

    .line 307
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0047

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 309
    :cond_0
    iput-wide p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    .line 310
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mDurationView:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    return-void
.end method

.method public onError(I)V
    .locals 4
    .parameter "statusCode"

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 318
    if-ne p1, v2, :cond_1

    .line 319
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0084

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 320
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->pauseRecord()V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 322
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0087

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 323
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZZI)V

    goto :goto_0

    .line 324
    :cond_2
    if-ne p1, v3, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0086

    invoke-static {v0, v1}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    .line 326
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->pauseRecord()V

    goto :goto_0
.end method

.method public onVolumChanged(D)V
    .locals 2
    .parameter "v"

    .prologue
    .line 295
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mVolumeView:Lcom/youdao/note/ui/audio/VolumeView;

    invoke-virtual {v0, p1, p2}, Lcom/youdao/note/ui/audio/VolumeView;->setVolume(D)V

    .line 298
    :cond_0
    return-void
.end method

.method public pauseRecord()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    invoke-virtual {v0}, Lcom/youdao/note/service/RecordService;->pauseRecord()V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mPauseButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mVolumeView:Lcom/youdao/note/ui/audio/VolumeView;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/VolumeView;->pause()V

    .line 268
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordingView:Landroid/widget/TextView;

    const v1, 0x7f0a0088

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 269
    return-void
.end method

.method public setAudioRecordListener(Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 193
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

    .line 194
    return-void
.end method

.method public stopRecord(Z)V
    .locals 1
    .parameter "saveAudio"

    .prologue
    const/4 v0, 0x1

    .line 248
    invoke-virtual {p0, v0, p1, v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZZI)V

    .line 249
    return-void
.end method

.method public stopRecord(ZI)V
    .locals 1
    .parameter "saveAudio"
    .parameter "nextAction"

    .prologue
    .line 240
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZZI)V

    .line 241
    return-void
.end method

.method public stopRecord(ZZI)V
    .locals 2
    .parameter "stopService"
    .parameter "saveAudio"
    .parameter "nextAction"

    .prologue
    .line 215
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    if-eqz v0, :cond_2

    .line 216
    if-eqz p1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;

    invoke-virtual {v0}, Lcom/youdao/note/service/RecordService;->stopRecord()V

    .line 219
    :cond_0
    if-nez p2, :cond_1

    .line 220
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    .line 226
    :cond_1
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->unRegist()V

    .line 228
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mListener:Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;

    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;

    invoke-interface {v0, v1, p3}, Lcom/youdao/note/ui/audio/AudioRecordBar$AudioRecordListener;->onRecordFinished(Ljava/io/File;I)V

    .line 232
    :cond_2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar;->mTotalTimeMs:J

    .line 233
    return-void
.end method
