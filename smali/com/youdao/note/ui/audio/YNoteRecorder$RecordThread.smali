.class Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;
.super Ljava/lang/Thread;
.source "YNoteRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/YNoteRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecordThread"
.end annotation


# static fields
.field private static final RECORD_RESULT_PAUSED:I = 0x0

.field private static final RECORD_RESULT_STOPPED:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;


# direct methods
.method private constructor <init>(Lcom/youdao/note/ui/audio/YNoteRecorder;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/youdao/note/ui/audio/YNoteRecorder;Lcom/youdao/note/ui/audio/YNoteRecorder$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;-><init>(Lcom/youdao/note/ui/audio/YNoteRecorder;)V

    return-void
.end method

.method private recording(Landroid/media/AudioRecord;[S[B)I
    .locals 10
    .parameter "audioRecord"
    .parameter "buffer"
    .parameter "mp3buffer"

    .prologue
    .line 250
    const/4 v3, 0x0

    .line 252
    .local v3, readSize:I
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-virtual {v5}, Lcom/youdao/note/ui/audio/YNoteRecorder;->getRecordState()I

    move-result v4

    .line 253
    .local v4, state:I
    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    .line 254
    const/4 v5, 0x0

    .line 278
    :goto_1
    return v5

    .line 256
    :cond_1
    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    .line 278
    :cond_2
    const/4 v5, 0x1

    goto :goto_1

    .line 260
    :cond_3
    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 264
    :cond_4
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mBufferSize:I
    invoke-static {v6}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$300(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v6

    invoke-virtual {p1, p2, v5, v6}, Landroid/media/AudioRecord;->read([SII)I

    move-result v3

    .line 265
    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #calls: Lcom/youdao/note/ui/audio/YNoteRecorder;->convertWaveData([SI)[D
    invoke-static {v5, p2, v3}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$600(Lcom/youdao/note/ui/audio/YNoteRecorder;[SI)[D

    move-result-object v0

    .line 266
    .local v0, ds:[D
    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #calls: Lcom/youdao/note/ui/audio/YNoteRecorder;->volumeRMS([D)D
    invoke-static {v8, v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$700(Lcom/youdao/note/ui/audio/YNoteRecorder;[D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 267
    invoke-static {p2, p2, v3, p3}, Lcom/youdao/note/ui/audio/SimpleLame;->encode([S[SI[B)I

    move-result v2

    .line 270
    .local v2, encResult:I
    :try_start_0
    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->output:Ljava/io/FileOutputStream;
    invoke-static {v5}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$500(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p3, v6, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, e:Ljava/io/IOException;
    const-string v5, "No space left on device"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 273
    const-string v5, "No space left on device."

    invoke-static {p0, v5}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x2

    .line 188
    new-instance v0, Landroid/media/AudioRecord;

    iget-object v2, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I
    invoke-static {v2}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$200(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v2

    const/16 v3, 0x10

    iget-object v5, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mBufferSize:I
    invoke-static {v5}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$300(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 192
    .local v0, audioRecord:Landroid/media/AudioRecord;
    iget-object v2, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mBufferSize:I
    invoke-static {v2}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$300(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v2

    new-array v6, v2, [S

    .line 193
    .local v6, buffer:[S
    const-wide v2, 0x40bc200000000000L

    array-length v5, v6

    mul-int/lit8 v5, v5, 0x2

    int-to-double v10, v5

    const-wide/high16 v12, 0x3ff4

    mul-double/2addr v10, v12

    add-double/2addr v2, v10

    double-to-int v2, v2

    new-array v9, v2, [B

    .line 194
    .local v9, mp3buffer:[B
    const/16 v2, -0x13

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 196
    iget-object v2, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I
    invoke-static {v2}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$200(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v2

    iget-object v3, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mSampleInRate:I
    invoke-static {v3}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$200(Lcom/youdao/note/ui/audio/YNoteRecorder;)I

    move-result v3

    const/16 v5, 0x20

    invoke-static {v2, v1, v3, v5}, Lcom/youdao/note/ui/audio/SimpleLame;->init(IIII)V

    .line 198
    :try_start_0
    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 210
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 214
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v6, v9}, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->recording(Landroid/media/AudioRecord;[S[B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 225
    :try_start_2
    invoke-static {}, Lcom/youdao/note/ui/audio/SimpleLame;->close()V

    .line 226
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 227
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 237
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v7

    .line 200
    .local v7, e:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$400(Lcom/youdao/note/ui/audio/YNoteRecorder;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 217
    .end local v7           #e:Ljava/lang/Exception;
    :cond_1
    :try_start_3
    invoke-static {v9}, Lcom/youdao/note/ui/audio/SimpleLame;->flush([B)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    .line 219
    .local v8, flushResult:I
    :try_start_4
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->output:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$500(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 225
    :goto_1
    :try_start_5
    invoke-static {}, Lcom/youdao/note/ui/audio/SimpleLame;->close()V

    .line 226
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 227
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 232
    :goto_2
    :try_start_6
    iget-object v1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$RecordThread;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->output:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$500(Lcom/youdao/note/ui/audio/YNoteRecorder;)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    .line 233
    :catch_1
    move-exception v7

    .line 235
    .local v7, e:Ljava/io/IOException;
    const-string v1, "Audio"

    const-string v2, "Failed to close output stream"

    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 220
    .end local v7           #e:Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 221
    .restart local v7       #e:Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 224
    .end local v7           #e:Ljava/io/IOException;
    .end local v8           #flushResult:I
    :catchall_0
    move-exception v1

    .line 225
    :try_start_8
    invoke-static {}, Lcom/youdao/note/ui/audio/SimpleLame;->close()V

    .line 226
    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 227
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    .line 229
    :goto_3
    throw v1

    .line 228
    :catch_3
    move-exception v2

    goto :goto_3

    .restart local v8       #flushResult:I
    :catch_4
    move-exception v1

    goto :goto_2

    .end local v8           #flushResult:I
    :catch_5
    move-exception v1

    goto :goto_0
.end method
