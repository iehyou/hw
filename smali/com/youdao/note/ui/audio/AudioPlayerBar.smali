.class public Lcom/youdao/note/ui/audio/AudioPlayerBar;
.super Landroid/widget/LinearLayout;
.source "AudioPlayerBar.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;
    }
.end annotation


# static fields
.field private static final MESSAGE_UPDATE_PROGRESS:I = 0x1

.field private static final SEEK_BAR_STATUS_TRACK:I = 0x1

.field private static final SEEK_BAR_STATUS_UNTRACK:I


# instance fields
.field private mCurPosView:Landroid/widget/TextView;

.field private mDurationView:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

.field private mNextSeekTo:I

.field private mPauseButton:Landroid/view/View;

.field private mPlayButton:Landroid/view/View;

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarStatus:I

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBarStatus:I

    .line 65
    new-instance v0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;

    invoke-direct {v0, p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;-><init>(Lcom/youdao/note/ui/audio/AudioPlayerBar;)V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mHandler:Landroid/os/Handler;

    .line 95
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030002

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 96
    const v0, 0x7f07000a

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    .line 97
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 98
    const v0, 0x7f070008

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    .line 99
    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    .line 100
    const v0, 0x7f07000b

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f07000c

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mDurationView:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f07000d

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/media/MediaPlayer;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/ui/audio/AudioPlayerBar;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBarStatus:I

    return v0
.end method

.method static synthetic access$200(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/widget/SeekBar;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 33
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/ui/audio/AudioPlayerBar;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->postUpdateSeekBar()V

    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 240
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    invoke-interface {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;->onClose()V

    .line 243
    :cond_0
    return-void
.end method

.method private innerPlay()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    .line 123
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 124
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 125
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 126
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 127
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 128
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mDurationView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->getDuration()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 130
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    return-void
.end method

.method private postUpdateSeekBar()V
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 137
    return-void
.end method


# virtual methods
.method public getDuration()I
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 220
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070008

    if-ne v1, v2, :cond_2

    .line 221
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 222
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->resume()V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->innerPlay()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    goto :goto_0

    .line 230
    .end local v0           #e:Ljava/lang/Exception;
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070009

    if-ne v1, v2, :cond_3

    .line 231
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->pause()V

    goto :goto_0

    .line 232
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f07000d

    if-ne v1, v2, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->close()V

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .parameter "mp"

    .prologue
    .line 206
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 210
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 211
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter "mp"
    .parameter "what"
    .parameter "extra"

    .prologue
    .line 190
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;->onError(Landroid/media/MediaPlayer;II)Z

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->stop()V

    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .parameter "seekBar"
    .parameter "progress"
    .parameter "fromUser"

    .prologue
    .line 248
    iput p2, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mNextSeekTo:I

    .line 249
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;

    iget v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mNextSeekTo:I

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1
    .parameter "mp"

    .prologue
    .line 199
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    invoke-interface {v0, p1}, Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;->onSeekComplete(Landroid/media/MediaPlayer;)V

    .line 202
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .parameter "seekBar"

    .prologue
    .line 254
    const/4 v0, 0x1

    iput v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBarStatus:I

    .line 255
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .parameter "seekBar"

    .prologue
    .line 259
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_2

    .line 261
    :try_start_0
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->innerPlay()V

    .line 262
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mNextSeekTo:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    :cond_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;

    const v2, 0x7f0a0088

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 272
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBarStatus:I

    .line 273
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 267
    .end local v0           #e:Ljava/lang/Exception;
    :cond_2
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mNextSeekTo:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter "v"
    .parameter "event"

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 145
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 146
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;

    const v1, 0x7f0a0088

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public play(Landroid/net/Uri;)V
    .locals 0
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mUri:Landroid/net/Uri;

    .line 118
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->innerPlay()V

    .line 119
    return-void
.end method

.method public play(Ljava/io/File;)V
    .locals 1
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mUri:Landroid/net/Uri;

    .line 113
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->innerPlay()V

    .line 114
    return-void
.end method

.method public play(Ljava/lang/String;)V
    .locals 1
    .parameter "path"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->play(Ljava/io/File;)V

    .line 109
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You have not start any audio now."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 156
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    invoke-direct {p0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->postUpdateSeekBar()V

    .line 159
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .parameter "msec"

    .prologue
    .line 172
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 173
    return-void
.end method

.method public setAudioPlayListener(Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;)V
    .locals 0
    .parameter "listener"

    .prologue
    .line 176
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mListener:Lcom/youdao/note/ui/audio/AudioPlayerBar$AudioPlayListener;

    .line 177
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 165
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;

    .line 166
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 167
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPauseButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 169
    return-void
.end method
