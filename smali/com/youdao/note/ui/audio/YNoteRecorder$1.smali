.class Lcom/youdao/note/ui/audio/YNoteRecorder$1;
.super Landroid/os/Handler;
.source "YNoteRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/YNoteRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/audio/YNoteRecorder;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "m"

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 72
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$000(Lcom/youdao/note/ui/audio/YNoteRecorder;)Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;->onVolumChanged(D)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_2

    .line 75
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$000(Lcom/youdao/note/ui/audio/YNoteRecorder;)Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;->onError(I)V

    goto :goto_0

    .line 76
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_3

    .line 77
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$000(Lcom/youdao/note/ui/audio/YNoteRecorder;)Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;->onError(I)V

    goto :goto_0

    .line 78
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/youdao/note/ui/audio/YNoteRecorder$1;->this$0:Lcom/youdao/note/ui/audio/YNoteRecorder;

    #getter for: Lcom/youdao/note/ui/audio/YNoteRecorder;->mListener:Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/YNoteRecorder;->access$000(Lcom/youdao/note/ui/audio/YNoteRecorder;)Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/youdao/note/ui/audio/YNoteRecorder$YNoteRecordListener;->onError(I)V

    goto :goto_0
.end method
