.class Lcom/youdao/note/ui/audio/AudioRecordBar$3;
.super Landroid/telephony/PhoneStateListener;
.source "AudioRecordBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/AudioRecordBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/audio/AudioRecordBar;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .parameter "state"
    .parameter "incomingNumber"

    .prologue
    .line 114
    if-nez p1, :cond_1

    .line 115
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$300(Lcom/youdao/note/ui/audio/AudioRecordBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    const-string v0, "restart record."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #calls: Lcom/youdao/note/ui/audio/AudioRecordBar;->startRecord()V
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$200(Lcom/youdao/note/ui/audio/AudioRecordBar;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$000(Lcom/youdao/note/ui/audio/AudioRecordBar;)Lcom/youdao/note/service/RecordService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/youdao/note/service/RecordService;->getRecordStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 120
    const-string v0, "pause record."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    const/4 v1, 0x0

    #setter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->isUserPaused:Z
    invoke-static {v0, v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$302(Lcom/youdao/note/ui/audio/AudioRecordBar;Z)Z

    .line 122
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$3;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v0}, Lcom/youdao/note/ui/audio/AudioRecordBar;->pauseRecord()V

    goto :goto_0
.end method
