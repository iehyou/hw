.class public Lcom/youdao/note/ui/audio/VolumeView;
.super Landroid/view/View;
.source "VolumeView.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final A_DIFF:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final B_DIFF:I = 0x0

.field private static final COVER:I = 0x4b000000

.field private static final DEFAULT_WIDTH:I = 0x3

.field private static final END_COLOR:I = -0xbe793c

#the value of this static final field might be set in the static constructor
.field private static final G_DIFF:I = 0x0

.field private static final INTERVAL_GREY:I = -0x363637

#the value of this static final field might be set in the static constructor
.field private static final R_DIFF:I = 0x0

.field private static final SHADOW_HEIGHT:I = 0x1

.field private static final START_COLOR:I = -0x613b1c


# instance fields
.field private mMaxVolume:D

.field private mMinVolume:D

.field private mPaint:Landroid/graphics/Paint;

.field private mVolume:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const v3, -0x613b1c

    const v2, -0xbe793c

    .line 30
    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    sub-int/2addr v0, v1

    sput v0, Lcom/youdao/note/ui/audio/VolumeView;->A_DIFF:I

    .line 33
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v1

    sub-int/2addr v0, v1

    sput v0, Lcom/youdao/note/ui/audio/VolumeView;->R_DIFF:I

    .line 36
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v0

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v1

    sub-int/2addr v0, v1

    sput v0, Lcom/youdao/note/ui/audio/VolumeView;->G_DIFF:I

    .line 39
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    sub-int/2addr v0, v1

    sput v0, Lcom/youdao/note/ui/audio/VolumeView;->B_DIFF:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/ui/audio/VolumeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attrs"

    .prologue
    const-wide/16 v0, 0x0

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mVolume:D

    .line 46
    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    .line 47
    const-wide v0, 0x3fb99999a0000000L

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMaxVolume:D

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    .line 58
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 18
    .parameter "canvas"

    .prologue
    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/audio/VolumeView;->getHeight()I

    move-result v12

    .line 65
    .local v12, height:I
    invoke-virtual/range {p0 .. p0}, Lcom/youdao/note/ui/audio/VolumeView;->getWidth()I

    move-result v17

    .line 66
    .local v17, width:I
    div-int/lit8 v14, v17, 0x3

    .line 67
    .local v14, n:I
    const/4 v13, 0x0

    .local v13, i:I
    :goto_0
    if-ge v13, v14, :cond_2

    .line 68
    rem-int/lit8 v1, v13, 0x2

    if-nez v1, :cond_1

    .line 69
    add-int/lit8 v1, v13, 0x1

    mul-int/lit8 v1, v1, 0x3

    int-to-double v3, v1

    move/from16 v0, v17

    int-to-double v5, v0

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/youdao/note/ui/audio/VolumeView;->mVolume:D

    mul-double/2addr v5, v7

    cmpg-double v1, v3, v5

    if-gez v1, :cond_0

    .line 70
    add-int/lit8 v1, v13, 0x1

    int-to-double v3, v1

    const-wide/high16 v5, 0x3ff0

    mul-double/2addr v3, v5

    int-to-double v5, v14

    div-double v10, v3, v5

    .line 71
    .local v10, f:D
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    const v3, -0x613b1c

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    sget v4, Lcom/youdao/note/ui/audio/VolumeView;->A_DIFF:I

    int-to-double v4, v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    add-int/2addr v3, v4

    const v4, -0x613b1c

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v4

    sget v5, Lcom/youdao/note/ui/audio/VolumeView;->R_DIFF:I

    int-to-double v5, v5

    mul-double/2addr v5, v10

    double-to-int v5, v5

    add-int/2addr v4, v5

    const v5, -0x613b1c

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v5

    sget v6, Lcom/youdao/note/ui/audio/VolumeView;->G_DIFF:I

    int-to-double v6, v6

    mul-double/2addr v6, v10

    double-to-int v6, v6

    add-int/2addr v5, v6

    const v6, -0x613b1c

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    sget v7, Lcom/youdao/note/ui/audio/VolumeView;->B_DIFF:I

    int-to-double v7, v7

    mul-double/2addr v7, v10

    double-to-int v7, v7

    add-int/2addr v6, v7

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    .end local v10           #f:D
    :goto_1
    mul-int/lit8 v1, v13, 0x3

    int-to-float v2, v1

    .line 80
    .local v2, left:F
    const/16 v16, 0x0

    .line 81
    .local v16, top:F
    add-int/lit8 v1, v13, 0x1

    mul-int/lit8 v1, v1, 0x3

    int-to-float v15, v1

    .line 82
    .local v15, right:F
    int-to-float v9, v12

    .line 83
    .local v9, bottom:F
    const/4 v3, 0x0

    add-int/lit8 v1, v13, 0x1

    mul-int/lit8 v1, v1, 0x3

    int-to-float v4, v1

    int-to-float v5, v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x4b00

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    const/high16 v1, 0x3f80

    add-float v5, v16, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move/from16 v3, v16

    move v4, v15

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 87
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    const/high16 v1, 0x3f80

    sub-float v3, v9, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v15

    move v5, v9

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 67
    .end local v2           #left:F
    .end local v9           #bottom:F
    .end local v15           #right:F
    .end local v16           #top:F
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 77
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    const v3, -0x363637

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 90
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    mul-int/lit8 v1, v13, 0x3

    int-to-float v4, v1

    const/4 v5, 0x0

    add-int/lit8 v1, v13, 0x1

    mul-int/lit8 v1, v1, 0x3

    int-to-float v6, v1

    int-to-float v7, v12

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/youdao/note/ui/audio/VolumeView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 95
    :cond_2
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 113
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mVolume:D

    .line 114
    return-void
.end method

.method public setVolume(D)V
    .locals 6
    .parameter "d"

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 99
    iput-wide p1, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    .line 101
    :cond_0
    iget-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMaxVolume:D

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 102
    iput-wide p1, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMaxVolume:D

    .line 104
    :cond_1
    iget-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMaxVolume:D

    iget-wide v2, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    sub-double/2addr v0, v2

    const-wide v2, 0x3ddb7cdfd9d7bdbbL

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 105
    iget-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    sub-double v0, p1, v0

    iget-wide v2, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMaxVolume:D

    iget-wide v4, p0, Lcom/youdao/note/ui/audio/VolumeView;->mMinVolume:D

    sub-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mVolume:D

    .line 109
    :goto_0
    invoke-virtual {p0}, Lcom/youdao/note/ui/audio/VolumeView;->invalidate()V

    .line 110
    return-void

    .line 107
    :cond_2
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/youdao/note/ui/audio/VolumeView;->mVolume:D

    goto :goto_0
.end method
