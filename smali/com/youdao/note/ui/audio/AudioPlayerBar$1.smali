.class Lcom/youdao/note/ui/audio/AudioPlayerBar$1;
.super Landroid/os/Handler;
.source "AudioPlayerBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/AudioPlayerBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/audio/AudioPlayerBar;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 68
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$000(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBarStatus:I
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$100(Lcom/youdao/note/ui/audio/AudioPlayerBar;)I

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$200(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$000(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 72
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mCurPosView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$300(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$000(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UnitUtils;->getDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioPlayerBar;->mPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$000(Lcom/youdao/note/ui/audio/AudioPlayerBar;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioPlayerBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioPlayerBar;

    #calls: Lcom/youdao/note/ui/audio/AudioPlayerBar;->postUpdateSeekBar()V
    invoke-static {v0}, Lcom/youdao/note/ui/audio/AudioPlayerBar;->access$400(Lcom/youdao/note/ui/audio/AudioPlayerBar;)V

    .line 83
    :cond_1
    return-void
.end method
