.class Lcom/youdao/note/ui/audio/AudioRecordBar$1;
.super Ljava/lang/Object;
.source "AudioRecordBar.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/ui/audio/AudioRecordBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;


# direct methods
.method constructor <init>(Lcom/youdao/note/ui/audio/AudioRecordBar;)V
    .locals 0
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .parameter "name"
    .parameter "service"

    .prologue
    const/4 v5, 0x0

    .line 87
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    check-cast p2, Lcom/youdao/note/service/RecordService$RecorderBinder;

    .end local p2
    invoke-virtual {p2}, Lcom/youdao/note/service/RecordService$RecorderBinder;->getRecordService()Lcom/youdao/note/service/RecordService;

    move-result-object v2

    #setter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;
    invoke-static {v1, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$002(Lcom/youdao/note/ui/audio/AudioRecordBar;Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService;

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;
    invoke-static {v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$000(Lcom/youdao/note/ui/audio/AudioRecordBar;)Lcom/youdao/note/service/RecordService;

    move-result-object v1

    iget-object v2, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #getter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->mFile:Ljava/io/File;
    invoke-static {v3}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$100(Lcom/youdao/note/ui/audio/AudioRecordBar;)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v1, v2, v3, v4}, Lcom/youdao/note/service/RecordService;->init(Landroid/content/Context;Ljava/io/File;Lcom/youdao/note/service/RecordService$RecordDurationListener;)V

    .line 90
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    #calls: Lcom/youdao/note/ui/audio/AudioRecordBar;->startRecord()V
    invoke-static {v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$200(Lcom/youdao/note/ui/audio/AudioRecordBar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, e:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    const/4 v2, 0x3

    invoke-virtual {v1, v5, v5, v2}, Lcom/youdao/note/ui/audio/AudioRecordBar;->stopRecord(ZZI)V

    .line 93
    const-string v1, "Init record service failed."

    invoke-static {p0, v1, v0}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    iget-object v1, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    invoke-virtual {v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a004a

    invoke-static {v1, v2}, Lcom/youdao/note/utils/UIUtilities;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .parameter "name"

    .prologue
    .line 82
    iget-object v0, p0, Lcom/youdao/note/ui/audio/AudioRecordBar$1;->this$0:Lcom/youdao/note/ui/audio/AudioRecordBar;

    const/4 v1, 0x0

    #setter for: Lcom/youdao/note/ui/audio/AudioRecordBar;->mRecordService:Lcom/youdao/note/service/RecordService;
    invoke-static {v0, v1}, Lcom/youdao/note/ui/audio/AudioRecordBar;->access$002(Lcom/youdao/note/ui/audio/AudioRecordBar;Lcom/youdao/note/service/RecordService;)Lcom/youdao/note/service/RecordService;

    .line 83
    return-void
.end method
