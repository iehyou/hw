.class public Lcom/youdao/note/LogRecorder;
.super Ljava/lang/Object;
.source "LogRecorder.java"

# interfaces
.implements Lcom/youdao/note/utils/PreferenceKeys$STAT;
.implements Lcom/youdao/note/utils/EmptyInstance;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/youdao/note/LogRecorder$ReportTask;
    }
.end annotation


# static fields
.field private static final DELAY:J = 0x1388L

.field public static LOGIN_STATUS_FIRSTLAUNCH:I = 0x0

.field public static LOGIN_STATUS_FISTLOGIN:I = 0x0

.field public static LOGIN_STATUS_LOGIN:I = 0x0

.field public static LOGIN_STATUS_UNLOGIN:I = 0x0

.field private static final REPORT_INTERVAL:J = 0x5265c00L

.field private static final STAT_NAME:Ljava/lang/String; = "YNote-stat"

.field private static final VENDOR_KEY:Ljava/lang/String; = "vendor"


# instance fields
.field private bSizeSet:Z

.field private lock:Ljava/lang/Object;

.field private mDensityDpi:I

.field private mHeight:I

.field private mPreference:Landroid/content/SharedPreferences;

.field private mWidth:I

.field private mYNote:Lcom/youdao/note/YNoteApplication;

.field private reporter:Lcom/youdao/note/LogRecorder$ReportTask;

.field private timer:Ljava/util/Timer;

.field private vendor:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_UNLOGIN:I

    .line 48
    const/4 v0, 0x1

    sput v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_LOGIN:I

    .line 50
    const/4 v0, 0x2

    sput v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_FIRSTLAUNCH:I

    .line 52
    const/4 v0, 0x3

    sput v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_FISTLOGIN:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .parameter "context"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v3, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    .line 62
    iput-object v3, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 64
    iput-object v3, p0, Lcom/youdao/note/LogRecorder;->reporter:Lcom/youdao/note/LogRecorder$ReportTask;

    .line 66
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->lock:Ljava/lang/Object;

    .line 68
    iput-object v3, p0, Lcom/youdao/note/LogRecorder;->timer:Ljava/util/Timer;

    .line 70
    iput-object v3, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    .line 74
    iput-boolean v4, p0, Lcom/youdao/note/LogRecorder;->bSizeSet:Z

    move-object v2, p1

    .line 83
    check-cast v2, Lcom/youdao/note/YNoteApplication;

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    .line 84
    const-string v2, "YNote-stat"

    invoke-virtual {p1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    .line 87
    :try_start_0
    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 90
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "vendor"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    .line 95
    const-string v2, "yz"

    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "yz_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update vendor to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->i(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "Failed to get vendor from manfiest."

    invoke-static {p0, v2, v1}, Lcom/youdao/note/utils/L;->e(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 101
    const-string v2, "undefined"

    iput-object v2, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/youdao/note/LogRecorder;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/youdao/note/LogRecorder;)Landroid/content/SharedPreferences;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$200(Lcom/youdao/note/LogRecorder;)J
    .locals 2
    .parameter "x0"

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getLastReportTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$300(Lcom/youdao/note/LogRecorder;)Lcom/youdao/note/YNoteApplication;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    return-object v0
.end method

.method static synthetic access$400(Lcom/youdao/note/LogRecorder;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->updateReportTime()Z

    move-result v0

    return v0
.end method

.method private dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter "sb"
    .parameter "key"
    .parameter "value"

    .prologue
    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<br/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    return-void
.end method

.method private getIMEI()Ljava/lang/String;
    .locals 5

    .prologue
    .line 319
    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    const-string v3, "imei"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, imei:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 321
    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lcom/youdao/note/YNoteApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 323
    .local v1, telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 324
    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "imei"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set imei to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 327
    .end local v1           #telephonyManager:Landroid/telephony/TelephonyManager;
    :cond_0
    return-object v0
.end method

.method private getLastReportTime()J
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "last_report_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private getMid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private inc(Ljava/lang/String;I)Z
    .locals 3
    .parameter "key"
    .parameter "increment"

    .prologue
    .line 365
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/2addr v1, p2

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method

.method private varargs incKeys(I[Ljava/lang/String;)Z
    .locals 7
    .parameter "increment"
    .parameter "keys"

    .prologue
    .line 343
    iget-object v6, p0, Lcom/youdao/note/LogRecorder;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 344
    const/4 v4, 0x1

    .line 345
    .local v4, succeed:Z
    move-object v0, p2

    .local v0, arr$:[Ljava/lang/String;
    :try_start_0
    array-length v3, v0

    .local v3, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 346
    .local v2, key:Ljava/lang/String;
    invoke-direct {p0, v2, p1}, Lcom/youdao/note/LogRecorder;->inc(Ljava/lang/String;I)Z

    move-result v5

    and-int/2addr v4, v5

    .line 345
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    .end local v2           #key:Ljava/lang/String;
    :cond_0
    monitor-exit v6

    return v4

    .line 349
    .end local v1           #i$:I
    .end local v3           #len$:I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method private varargs incKeys([Ljava/lang/String;)Z
    .locals 1
    .parameter "keys"

    .prologue
    .line 339
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private report()V
    .locals 3

    .prologue
    .line 152
    invoke-static {}, Lcom/youdao/note/YNoteApplication;->getInstance()Lcom/youdao/note/YNoteApplication;

    move-result-object v1

    .line 153
    .local v1, mYNote:Lcom/youdao/note/YNoteApplication;
    sget v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_UNLOGIN:I

    .line 154
    .local v0, loginStatus:I
    invoke-virtual {v1}, Lcom/youdao/note/YNoteApplication;->isLogin()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    sget v0, Lcom/youdao/note/LogRecorder;->LOGIN_STATUS_LOGIN:I

    .line 157
    :cond_0
    invoke-virtual {p0, v0}, Lcom/youdao/note/LogRecorder;->report(I)V

    .line 158
    return-void
.end method

.method private updateReportTime()Z
    .locals 4

    .prologue
    .line 309
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_report_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addArtTimes(I)Z
    .locals 3
    .parameter "increament"

    .prologue
    .line 235
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AddArtTimes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AttachNum"

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addCameraNoteTimes()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 221
    const-string v0, "Update view cameraote times."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CameraNoteTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v3, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addCameraTimes(I)Z
    .locals 3
    .parameter "increament"

    .prologue
    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update add camera times by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AddCameraTimes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AttachNum"

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addContentCopyTimes()Z
    .locals 3

    .prologue
    .line 211
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ContentCopyTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addEditNoteTimes()Z
    .locals 3

    .prologue
    .line 277
    const-string v0, "update edit note times"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EditNoteTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addGeneralParameter(Ljava/util/List;)V
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, params:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    const-wide/high16 v7, 0x3ff0

    .line 353
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "imei"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "mid"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getMid()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "phoneVersion"

    const-string v2, "android"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "model"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "vendor"

    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "keyfrom"

    iget-object v2, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v2}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "dpi"

    iget v2, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "resolution"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/youdao/note/LogRecorder;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/youdao/note/LogRecorder;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "size"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/youdao/note/LogRecorder;->mWidth:I

    int-to-double v3, v3

    mul-double/2addr v3, v7

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    int-to-double v5, v5

    div-double/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/youdao/note/LogRecorder;->mHeight:I

    int-to-double v3, v3

    mul-double/2addr v3, v7

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    int-to-double v5, v5

    div-double/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    return-void
.end method

.method public addIconAllTimes()Z
    .locals 3

    .prologue
    .line 290
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "IconAllTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addIconNewTimes()Z
    .locals 3

    .prologue
    .line 282
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "IconNewTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addIconNotebookTimes()Z
    .locals 3

    .prologue
    .line 294
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "IconNotebookTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addIconSyncTimes()Z
    .locals 3

    .prologue
    .line 286
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "IconSyncTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addImageRectificationTimes()Z
    .locals 3

    .prologue
    .line 304
    const-string v0, "update image rectification times"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "WbCorrectTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addNote(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .parameter "typeFrom"
    .parameter "entryFrom"

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    const-string v0, "AppAddTimes"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "update add note times,  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    new-array v0, v4, [Ljava/lang/String;

    aput-object p2, v0, v2

    const-string v1, "AddNoteTimes"

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    .line 180
    :goto_0
    return v0

    .line 179
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "update add note times, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    aput-object p1, v0, v2

    aput-object p2, v0, v3

    const-string v1, "AddNoteTimes"

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public addNoteDetailTimes()Z
    .locals 3

    .prologue
    .line 197
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NoteDetailTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addPaintTimes(I)Z
    .locals 3
    .parameter "increament"

    .prologue
    .line 259
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AddPaintTimes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AttachNum"

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addPhotoTimes(I)Z
    .locals 3
    .parameter "increament"

    .prologue
    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update add photo times by "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AddPhotoTimes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AttachNum"

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addRecordFromBarTimes()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 226
    const-string v0, "Update record from bar times."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "RecordFromBar"

    aput-object v2, v0, v1

    invoke-direct {p0, v3, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addRecordTimes(I)Z
    .locals 3
    .parameter "increament"

    .prologue
    .line 239
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "AddRecordTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addSearchTimes()Z
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SearchNoteTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addSendMailTimes()Z
    .locals 3

    .prologue
    .line 193
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SendMailTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addShareMailTimes()Z
    .locals 3

    .prologue
    .line 189
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EditMailTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addTextNoteTimes()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 216
    const-string v0, "Update view textNote times."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TextNoteTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v3, v0}, Lcom/youdao/note/LogRecorder;->incKeys(I[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addViewAttachTimes()Z
    .locals 3

    .prologue
    .line 206
    const-string v0, "Update view attach times."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ViewAttachTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addViewDetailTimes()Z
    .locals 3

    .prologue
    .line 299
    const-string v0, "update view note times"

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ViewNoteTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addViewPicTimes()Z
    .locals 3

    .prologue
    .line 201
    const-string v0, "Update view image times."

    invoke-static {p0, v0}, Lcom/youdao/note/utils/L;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ViewPicTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public addWbCorrectTimes()Z
    .locals 3

    .prologue
    .line 273
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "WbCorrectTimes"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/youdao/note/LogRecorder;->incKeys([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public clearErrorLog()V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "error"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method

.method public dumpStatistics()Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/high16 v9, 0x3ff0

    .line 369
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .local v2, sb:Ljava/lang/StringBuilder;
    const-string v3, "imei"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getIMEI()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v3, "mid"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getMid()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v3, "phoneVersion"

    const-string v4, "android"

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v3, "model"

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getModel()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v3, "vendor"

    iget-object v4, p0, Lcom/youdao/note/LogRecorder;->vendor:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v3, "keyfrom"

    iget-object v4, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v4}, Lcom/youdao/note/YNoteApplication;->getKeyFrom()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v3, "dpi"

    iget v4, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v3, "resolution"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const-string v4, "PassCodeStatus"

    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->isPinlockEnable()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "1"

    :goto_0
    invoke-direct {p0, v2, v4, v3}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v4, "AutoSyncStatus"

    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->mYNote:Lcom/youdao/note/YNoteApplication;

    invoke-virtual {v3}, Lcom/youdao/note/YNoteApplication;->getAutoSyncPeriod()I

    move-result v3

    if-lez v3, :cond_1

    const-string v3, "1"

    :goto_1
    invoke-direct {p0, v2, v4, v3}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v3, "size"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mWidth:I

    int-to-double v5, v5

    mul-double/2addr v5, v9

    iget v7, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    int-to-double v7, v7

    div-double/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/youdao/note/LogRecorder;->mHeight:I

    int-to-double v5, v5

    mul-double/2addr v5, v9

    iget v7, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    int-to-double v7, v7

    div-double/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v3, "error"

    invoke-virtual {p0}, Lcom/youdao/note/LogRecorder;->getError()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v3, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 383
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/youdao/note/LogRecorder;->dumpKeyValue(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 378
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_0
    const-string v3, "0"

    goto :goto_0

    .line 379
    :cond_1
    const-string v3, "0"

    goto :goto_1

    .line 385
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getError()Ljava/lang/String;
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    const-string v1, "error"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isEverReport()Z
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    .line 168
    iget-object v1, p0, Lcom/youdao/note/LogRecorder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "last_report_time"

    const-wide/16 v3, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v2, v5

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isSizeSet()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/youdao/note/LogRecorder;->bSizeSet:Z

    return v0
.end method

.method public report(I)V
    .locals 2
    .parameter "loginStatus"

    .prologue
    .line 146
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LoginStatus"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    new-instance v0, Lcom/youdao/note/LogRecorder$ReportTask;

    invoke-direct {v0, p0}, Lcom/youdao/note/LogRecorder$ReportTask;-><init>(Lcom/youdao/note/LogRecorder;)V

    iput-object v0, p0, Lcom/youdao/note/LogRecorder;->reporter:Lcom/youdao/note/LogRecorder$ReportTask;

    .line 148
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->reporter:Lcom/youdao/note/LogRecorder$ReportTask;

    invoke-virtual {v0}, Lcom/youdao/note/LogRecorder$ReportTask;->execute()V

    .line 149
    return-void
.end method

.method public reportIfNecessary()V
    .locals 4

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/youdao/note/LogRecorder;->isEverReport()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->getLastReportTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/youdao/note/LogRecorder;->report()V

    .line 143
    :cond_0
    return-void
.end method

.method public setError(Ljava/lang/String;)V
    .locals 2
    .parameter "errorInfo"

    .prologue
    .line 127
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "error"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    return-void
.end method

.method public setHandWriteMode(I)V
    .locals 3
    .parameter "mode"

    .prologue
    .line 247
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "machine"

    const-string v2, "FineNWrite"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "machine"

    const-string v2, "BadNWrite"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setScreenParameters(III)V
    .locals 1
    .parameter "width"
    .parameter "height"
    .parameter "densityDpi"

    .prologue
    .line 106
    iput p1, p0, Lcom/youdao/note/LogRecorder;->mWidth:I

    .line 107
    iput p2, p0, Lcom/youdao/note/LogRecorder;->mHeight:I

    .line 108
    iput p3, p0, Lcom/youdao/note/LogRecorder;->mDensityDpi:I

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/youdao/note/LogRecorder;->bSizeSet:Z

    .line 110
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    .line 117
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/youdao/note/LogRecorder;->timer:Ljava/util/Timer;

    .line 118
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/youdao/note/LogRecorder$1;

    invoke-direct {v1, p0}, Lcom/youdao/note/LogRecorder$1;-><init>(Lcom/youdao/note/LogRecorder;)V

    const-wide/16 v2, 0x1388

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 124
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 162
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->reporter:Lcom/youdao/note/LogRecorder$ReportTask;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/youdao/note/LogRecorder;->reporter:Lcom/youdao/note/LogRecorder$ReportTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/youdao/note/LogRecorder$ReportTask;->stop(Z)Z

    .line 165
    :cond_0
    return-void
.end method
