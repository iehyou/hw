.class public final Lcom/youdao/note/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final about:I = 0x7f0a0089

.field public static final about_app:I = 0x7f0a0039

.field public static final accept:I = 0x7f0a002b

.field public static final account:I = 0x7f0a0073

.field public static final account_hint:I = 0x7f0a0004

.field public static final account_info:I = 0x7f0a0078

.field public static final add_resource_failed:I = 0x7f0a00f3

.field public static final all_notes:I = 0x7f0a00b8

.field public static final already_newwast:I = 0x7f0a0028

.field public static final app_name:I = 0x7f0a0001

.field public static final auto_sync:I = 0x7f0a006f

.field public static final auto_sync_key:I = 0x7f0a000a

.field public static final auto_sync_wifi_only:I = 0x7f0a006e

.field public static final auto_sync_wifi_only_key:I = 0x7f0a000b

.field public static final auto_sync_wifi_only_summary:I = 0x7f0a006d

.field public static final cancel:I = 0x7f0a00aa

.field public static final cancle_syncing:I = 0x7f0a00bd

.field public static final check_update:I = 0x7f0a006c

.field public static final check_update_key:I = 0x7f0a000c

.field public static final checking_update:I = 0x7f0a0041

.field public static final choose_recommend:I = 0x7f0a005f

.field public static final clear_confirm:I = 0x7f0a0053

.field public static final clear_data:I = 0x7f0a0050

.field public static final clear_data_key:I = 0x7f0a0010

.field public static final clear_search_history:I = 0x7f0a007b

.field public static final clearing:I = 0x7f0a0054

.field public static final close:I = 0x7f0a008b

.field public static final cmwap_button_later:I = 0x7f0a00fa

.field public static final cmwap_button_settings:I = 0x7f0a00f9

.field public static final cmwap_error_msg:I = 0x7f0a00f8

.field public static final cmwap_error_title:I = 0x7f0a00f7

.field public static final complete:I = 0x7f0a00ac

.field public static final confirm_delete_resource:I = 0x7f0a0036

.field public static final copy_paste:I = 0x7f0a001a

.field public static final copyright:I = 0x7f0a0062

.field public static final copyright_e_1:I = 0x7f0a0060

.field public static final copyright_e_2:I = 0x7f0a0061

.field public static final copyright_year:I = 0x7f0a0063

.field public static final create_note:I = 0x7f0a00b7

.field public static final defalt_notebook:I = 0x7f0a002e

.field public static final delete:I = 0x7f0a00a6

.field public static final delete_notebook_message:I = 0x7f0a004d

.field public static final delete_notebook_title:I = 0x7f0a004c

.field public static final deleted:I = 0x7f0a00a8

.field public static final deny:I = 0x7f0a002c

.field public static final detail:I = 0x7f0a00a4

.field public static final detail_title:I = 0x7f0a00a5

.field public static final device_space_full:I = 0x7f0a0030

.field public static final device_storage_low:I = 0x7f0a0086

.field public static final edit:I = 0x7f0a00a3

.field public static final else_setting:I = 0x7f0a007a

.field public static final email_subject:I = 0x7f0a005a

.field public static final empty_camera_note:I = 0x7f0a003d

.field public static final empty_content:I = 0x7f0a005c

.field public static final empty_doodle_note:I = 0x7f0a0040

.field public static final empty_gallary_note:I = 0x7f0a003e

.field public static final empty_handwrite_note:I = 0x7f0a003f

.field public static final empty_note_list:I = 0x7f0a00ad

.field public static final empty_notebook:I = 0x7f0a00bc

.field public static final empty_search_contacts:I = 0x7f0a00ed

.field public static final empty_search_history:I = 0x7f0a0055

.field public static final empty_search_result:I = 0x7f0a0056

.field public static final empty_title:I = 0x7f0a0002

.field public static final enable_pinlock_key:I = 0x7f0a0005

.field public static final exit:I = 0x7f0a008a

.field public static final failed_pick_pic:I = 0x7f0a0081

.field public static final failed_save_resource:I = 0x7f0a0082

.field public static final fancy_handwrite_not_support:I = 0x7f0a0011

.field public static final feed_back:I = 0x7f0a006b

.field public static final general_setting:I = 0x7f0a0075

.field public static final give_up_doodle:I = 0x7f0a00d5

.field public static final give_up_edit:I = 0x7f0a0051

.field public static final give_up_handwrite:I = 0x7f0a00cf

.field public static final handwrite_guide_never_show:I = 0x7f0a00d1

.field public static final handwrite_guide_start:I = 0x7f0a00d0

.field public static final handwrite_mode:I = 0x7f0a0077

.field public static final handwrite_mode_key:I = 0x7f0a000e

.field public static final handwrite_mode_tips_msg:I = 0x7f0a00fc

.field public static final handwrite_mode_tips_switched:I = 0x7f0a00fe

.field public static final handwrite_mode_tips_title:I = 0x7f0a00fb

.field public static final handwrite_mode_tips_toast:I = 0x7f0a00fd

.field public static final handwrite_save:I = 0x7f0a00cb

.field public static final hello:I = 0x7f0a0000

.field public static final help:I = 0x7f0a006a

.field public static final illegal_username:I = 0x7f0a003a

.field public static final image_quality:I = 0x7f0a0076

.field public static final image_quality_key:I = 0x7f0a000d

.field public static final input_notebook_name:I = 0x7f0a0016

.field public static final input_pinlock:I = 0x7f0a0023

.field public static final input_pinlock_again:I = 0x7f0a0021

.field public static final input_ynote_password:I = 0x7f0a001d

.field public static final last_sync_key:I = 0x7f0a0008

.field public static final lasy_sync_time:I = 0x7f0a0071

.field public static final license:I = 0x7f0a00c8

.field public static final license_title:I = 0x7f0a00c9

.field public static final load_image_lib_failed:I = 0x7f0a00f6

.field public static final load_record_lib_failed:I = 0x7f0a00f5

.field public static final loading:I = 0x7f0a0099

.field public static final loading_error_cannot_edit:I = 0x7f0a0035

.field public static final loading_note_error:I = 0x7f0a003c

.field public static final loading_resource:I = 0x7f0a009a

.field public static final local_search:I = 0x7f0a0052

.field public static final login:I = 0x7f0a0068

.field public static final login_error:I = 0x7f0a0003

.field public static final login_key:I = 0x7f0a000f

.field public static final login_require:I = 0x7f0a00b5

.field public static final login_right_now:I = 0x7f0a00bb

.field public static final login_tip:I = 0x7f0a0057

.field public static final login_tips:I = 0x7f0a00ae

.field public static final loging:I = 0x7f0a0067

.field public static final logout:I = 0x7f0a00c0

.field public static final longest_record_length:I = 0x7f0a0047

.field public static final mail_dialog_invalid_mails_empty:I = 0x7f0a00e8

.field public static final mail_dialog_invalid_mails_limit:I = 0x7f0a00ea

.field public static final mail_dialog_invalid_mails_msg:I = 0x7f0a00eb

.field public static final mail_dialog_invalid_mails_send:I = 0x7f0a00e6

.field public static final mail_dialog_invalid_mails_title:I = 0x7f0a00e7

.field public static final mail_dialog_invalid_title_empty:I = 0x7f0a00e9

.field public static final mail_dialog_send_failed:I = 0x7f0a00e0

.field public static final mail_dialog_sending:I = 0x7f0a00dd

.field public static final mail_dialog_trash_msg:I = 0x7f0a00df

.field public static final mail_dialog_trash_title:I = 0x7f0a00de

.field public static final mail_info:I = 0x7f0a00db

.field public static final mail_need_login:I = 0x7f0a00ef

.field public static final mail_need_sync:I = 0x7f0a00f0

.field public static final mail_receiver:I = 0x7f0a00d9

.field public static final mail_receiver_limit:I = 0x7f0a00e3

.field public static final mail_selection_add:I = 0x7f0a00f1

.field public static final mail_selection_cancel:I = 0x7f0a00f2

.field public static final mail_selection_contacts:I = 0x7f0a00ee

.field public static final mail_selection_search_contacts:I = 0x7f0a00ec

.field public static final mail_send:I = 0x7f0a00dc

.field public static final mail_send_failed:I = 0x7f0a00e1

.field public static final mail_send_success:I = 0x7f0a00e4

.field public static final mail_send_times_limit:I = 0x7f0a00e5

.field public static final mail_share:I = 0x7f0a001b

.field public static final mail_share_note:I = 0x7f0a00d8

.field public static final mail_size_limit:I = 0x7f0a00e2

.field public static final mail_title:I = 0x7f0a00da

.field public static final menu_title_feedback:I = 0x7f0a009c

.field public static final more:I = 0x7f0a0093

.field public static final move:I = 0x7f0a00a7

.field public static final need_login:I = 0x7f0a0037

.field public static final network_error:I = 0x7f0a00a1

.field public static final new_features_message:I = 0x7f0a00d7

.field public static final new_features_title:I = 0x7f0a00d6

.field public static final new_note:I = 0x7f0a00b6

.field public static final new_notebook:I = 0x7f0a004b

.field public static final new_version_found:I = 0x7f0a0029

.field public static final no:I = 0x7f0a0100

.field public static final no_application:I = 0x7f0a0083

.field public static final no_image_selected:I = 0x7f0a007f

.field public static final notebook_conflict:I = 0x7f0a0072

.field public static final notebook_empty_error:I = 0x7f0a0014

.field public static final notebook_notes_title:I = 0x7f0a0058

.field public static final notebooks:I = 0x7f0a00b9

.field public static final notebooks_with_colon:I = 0x7f0a00ba

.field public static final notes_titles:I = 0x7f0a0059

.field public static final ok:I = 0x7f0a00ab

.field public static final out_of_memory_tip:I = 0x7f0a00ca

.field public static final password:I = 0x7f0a0074

.field public static final password_too_many_error:I = 0x7f0a0015

.field public static final paused:I = 0x7f0a0088

.field public static final pincode_limit:I = 0x7f0a001c

.field public static final pinlock:I = 0x7f0a0027

.field public static final pinlock_differ:I = 0x7f0a0020

.field public static final pinlock_summary:I = 0x7f0a0026

.field public static final pssword_error:I = 0x7f0a00be

.field public static final quering:I = 0x7f0a009b

.field public static final recommend:I = 0x7f0a0091

.field public static final recommend_body:I = 0x7f0a005b

.field public static final recommend_by_email:I = 0x7f0a005d

.field public static final recommend_by_message:I = 0x7f0a005e

.field public static final record_added:I = 0x7f0a0085

.field public static final record_busy:I = 0x7f0a004a

.field public static final record_paused:I = 0x7f0a0046

.field public static final record_save_failed:I = 0x7f0a0087

.field public static final recording:I = 0x7f0a0044

.field public static final redo:I = 0x7f0a00c1

.field public static final refresh:I = 0x7f0a00a2

.field public static final regist:I = 0x7f0a0069

.field public static final relogin:I = 0x7f0a0033

.field public static final rename:I = 0x7f0a0031

.field public static final rename_notebook:I = 0x7f0a004e

.field public static final res_not_found:I = 0x7f0a003b

.field public static final resource_manage:I = 0x7f0a0080

.field public static final resource_missing:I = 0x7f0a0034

.field public static final resource_too_large:I = 0x7f0a00f4

.field public static final save:I = 0x7f0a00a9

.field public static final save_failed:I = 0x7f0a0096

.field public static final save_failed_device_full:I = 0x7f0a0097

.field public static final save_resource_failed_device_full:I = 0x7f0a0098

.field public static final save_succeed:I = 0x7f0a0095

.field public static final save_to_youdao:I = 0x7f0a0012

.field public static final saving:I = 0x7f0a0094

.field public static final search_failed:I = 0x7f0a007d

.field public static final search_hint:I = 0x7f0a007e

.field public static final search_history:I = 0x7f0a007c

.field public static final search_result:I = 0x7f0a0042

.field public static final search_setting_des:I = 0x7f0a0013

.field public static final service_invalid:I = 0x7f0a004f

.field public static final set_pinlock:I = 0x7f0a0024

.field public static final set_pinlock_key:I = 0x7f0a0006

.field public static final setting:I = 0x7f0a0092

.field public static final signout:I = 0x7f0a008f

.field public static final single_file_error:I = 0x7f0a00a0

.field public static final slide_to_copy:I = 0x7f0a0019

.field public static final source:I = 0x7f0a002d

.field public static final space_full:I = 0x7f0a002f

.field public static final start_record_failed:I = 0x7f0a0084

.field public static final start_use:I = 0x7f0a008c

.field public static final sync:I = 0x7f0a0090

.field public static final sync_note_failed:I = 0x7f0a009f

.field public static final sync_note_succeed:I = 0x7f0a009e

.field public static final sync_setting:I = 0x7f0a0079

.field public static final syncing:I = 0x7f0a009d

.field public static final system_error:I = 0x7f0a0018

.field public static final system_not_support:I = 0x7f0a0017

.field public static final tip_conv:I = 0x7f0a00b4

.field public static final tip_conv_head:I = 0x7f0a00b1

.field public static final tip_save:I = 0x7f0a00b2

.field public static final tip_save_head:I = 0x7f0a00af

.field public static final tip_sync:I = 0x7f0a00b3

.field public static final tip_sync_head:I = 0x7f0a00b0

.field public static final trash_doodle_message:I = 0x7f0a00d3

.field public static final trash_doodle_ok:I = 0x7f0a00d4

.field public static final trash_doodle_title:I = 0x7f0a00d2

.field public static final trash_handwrite_message:I = 0x7f0a00cd

.field public static final trash_handwrite_ok:I = 0x7f0a00ce

.field public static final trash_handwrite_title:I = 0x7f0a00cc

.field public static final try_again:I = 0x7f0a001f

.field public static final try_later:I = 0x7f0a008e

.field public static final try_now:I = 0x7f0a008d

.field public static final unknow_reason:I = 0x7f0a0032

.field public static final unsynced:I = 0x7f0a0043

.field public static final update_failed:I = 0x7f0a002a

.field public static final update_pinlock:I = 0x7f0a0025

.field public static final used_space:I = 0x7f0a0070

.field public static final used_space_key:I = 0x7f0a0009

.field public static final user_not_exist:I = 0x7f0a00bf

.field public static final username:I = 0x7f0a0007

.field public static final username_pwd_empty:I = 0x7f0a0022

.field public static final version:I = 0x7f0a0065

.field public static final versionName:I = 0x7f0a0066

.field public static final view_image:I = 0x7f0a00c2

.field public static final web_url:I = 0x7f0a0038

.field public static final website:I = 0x7f0a0064

.field public static final whiteboard_auto_detect_off:I = 0x7f0a00c7

.field public static final whiteboard_auto_detect_on:I = 0x7f0a00c6

.field public static final whiteboard_rectification:I = 0x7f0a00c3

.field public static final whiteboard_tip:I = 0x7f0a00c5

.field public static final whiteboard_titie:I = 0x7f0a00c4

.field public static final wrong_pincode:I = 0x7f0a001e

.field public static final yes:I = 0x7f0a00ff

.field public static final ynote_record_paused:I = 0x7f0a0049

.field public static final ynote_recording:I = 0x7f0a0045

.field public static final ynote_recording_format:I = 0x7f0a0048


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
