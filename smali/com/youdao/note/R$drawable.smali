.class public final Lcom/youdao/note/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/youdao/note/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final about:I = 0x7f020000

.field public static final all_notes:I = 0x7f020001

.field public static final all_notes_d:I = 0x7f020002

.field public static final all_notes_large:I = 0x7f020003

.field public static final all_notes_large_pressed:I = 0x7f020004

.field public static final all_notes_large_selector:I = 0x7f020005

.field public static final alphabet_bg:I = 0x7f020006

.field public static final arrow:I = 0x7f020007

.field public static final arrow_down:I = 0x7f020008

.field public static final attach_icon:I = 0x7f020009

.field public static final attach_icon_in_list:I = 0x7f02000a

.field public static final audio:I = 0x7f02000b

.field public static final backspace:I = 0x7f02000c

.field public static final bg:I = 0x7f02000d

.field public static final btn_edit_note:I = 0x7f02000e

.field public static final btn_mail_share_send_1:I = 0x7f02000f

.field public static final btn_mail_share_send_2:I = 0x7f020010

.field public static final button_background_selector:I = 0x7f020011

.field public static final button_bg:I = 0x7f020012

.field public static final button_bg_pressed:I = 0x7f020013

.field public static final camera:I = 0x7f020014

.field public static final camera_d:I = 0x7f020015

.field public static final checkbox:I = 0x7f020016

.field public static final checkbox_off:I = 0x7f020017

.field public static final checkbox_on:I = 0x7f020018

.field public static final chrome:I = 0x7f020019

.field public static final clear_button_d:I = 0x7f02001a

.field public static final clear_search:I = 0x7f02001b

.field public static final clear_search_pressed:I = 0x7f02001c

.field public static final close_border:I = 0x7f02001d

.field public static final colorbox_1:I = 0x7f02001e

.field public static final colorbox_2:I = 0x7f02001f

.field public static final colorbox_3:I = 0x7f020020

.field public static final colorbox_4:I = 0x7f020021

.field public static final colorbox_5:I = 0x7f020022

.field public static final colorbox_6:I = 0x7f020023

.field public static final colorbox_s1:I = 0x7f020024

.field public static final colorbox_s2:I = 0x7f020025

.field public static final colorbox_s3:I = 0x7f020026

.field public static final colorbox_s4:I = 0x7f020027

.field public static final colorbox_s5:I = 0x7f020028

.field public static final colorbox_s6:I = 0x7f020029

.field public static final complete:I = 0x7f02002a

.field public static final complete_click:I = 0x7f02002b

.field public static final complete_normal:I = 0x7f02002c

.field public static final complete_selector:I = 0x7f02002d

.field public static final copy:I = 0x7f02002e

.field public static final cross:I = 0x7f02002f

.field public static final dark_bg:I = 0x7f020030

.field public static final dark_bg_pressed:I = 0x7f020031

.field public static final dark_button_background_selector:I = 0x7f020032

.field public static final dark_button_bg:I = 0x7f020033

.field public static final dark_button_bg_pressed:I = 0x7f020034

.field public static final dark_gray_bg:I = 0x7f020035

.field public static final default_button_bg:I = 0x7f020036

.field public static final default_button_bg_pressed:I = 0x7f020037

.field public static final default_button_bg_selector:I = 0x7f020038

.field public static final delete:I = 0x7f020039

.field public static final delete_click:I = 0x7f02003a

.field public static final delete_normal:I = 0x7f02003b

.field public static final delete_resource:I = 0x7f02003c

.field public static final delete_resource_selector:I = 0x7f02003d

.field public static final delete_selector:I = 0x7f02003e

.field public static final detail:I = 0x7f02003f

.field public static final dialog_bg:I = 0x7f020040

.field public static final dialog_bg_footer:I = 0x7f020041

.field public static final dialog_bg_head:I = 0x7f020042

.field public static final dialog_button:I = 0x7f020043

.field public static final divider_horizontal_dark:I = 0x7f020044

.field public static final done:I = 0x7f020045

.field public static final doodle_bg:I = 0x7f020046

.field public static final doodle_color:I = 0x7f020047

.field public static final doodle_colorbox_bg:I = 0x7f020048

.field public static final doodle_undo:I = 0x7f020049

.field public static final edit:I = 0x7f02004a

.field public static final edit_area_bg:I = 0x7f02004b

.field public static final editor_bg:I = 0x7f02004c

.field public static final empty_notebooks:I = 0x7f02004d

.field public static final empty_notes:I = 0x7f02004e

.field public static final empty_search_result:I = 0x7f02004f

.field public static final excel:I = 0x7f020050

.field public static final feedback:I = 0x7f020051

.field public static final flash:I = 0x7f020052

.field public static final footer_dark_grey_bg_selector:I = 0x7f020053

.field public static final footer_light_grey_bg_selector:I = 0x7f020054

.field public static final gallary:I = 0x7f020055

.field public static final gallary_d:I = 0x7f020056

.field public static final go:I = 0x7f020057

.field public static final guide_1:I = 0x7f020058

.field public static final guide_2:I = 0x7f020059

.field public static final h_divider:I = 0x7f02005a

.field public static final handwrite_blank:I = 0x7f02005b

.field public static final handwrite_delete:I = 0x7f02005c

.field public static final handwrite_done:I = 0x7f02005d

.field public static final handwrite_done_pressed:I = 0x7f02005e

.field public static final handwrite_done_selector:I = 0x7f02005f

.field public static final handwrite_guide:I = 0x7f020060

.field public static final handwrite_guide_button_never_show_1:I = 0x7f020061

.field public static final handwrite_guide_button_never_show_2:I = 0x7f020062

.field public static final handwrite_guide_button_selector_never_show:I = 0x7f020063

.field public static final handwrite_guide_button_selector_start:I = 0x7f020064

.field public static final handwrite_guide_button_start_1:I = 0x7f020065

.field public static final handwrite_guide_button_start_2:I = 0x7f020066

.field public static final handwrite_paint:I = 0x7f020067

.field public static final handwrite_return:I = 0x7f020068

.field public static final handwrite_slider_bg:I = 0x7f020069

.field public static final handwrite_slider_block:I = 0x7f02006a

.field public static final handwrite_slider_blue:I = 0x7f02006b

.field public static final handwrite_slider_brown:I = 0x7f02006c

.field public static final handwrite_top_line:I = 0x7f02006d

.field public static final handwrite_top_widget:I = 0x7f02006e

.field public static final handwrite_trash:I = 0x7f02006f

.field public static final head_bg_selector:I = 0x7f020070

.field public static final head_logo:I = 0x7f020071

.field public static final html:I = 0x7f020072

.field public static final ic_delete:I = 0x7f020073

.field public static final ic_launcher:I = 0x7f020074

.field public static final icon:I = 0x7f020075

.field public static final icon_land:I = 0x7f020076

.field public static final image_rectification:I = 0x7f020077

.field public static final image_rectification_auto_detect_off:I = 0x7f020078

.field public static final image_rectification_auto_detect_on:I = 0x7f020079

.field public static final image_rectification_background_selector:I = 0x7f02007a

.field public static final image_rectification_click:I = 0x7f02007b

.field public static final image_rectification_menu_clicked:I = 0x7f02007c

.field public static final image_rectification_menu_clicked_repeat:I = 0x7f02007d

.field public static final image_rectification_menu_divider:I = 0x7f02007e

.field public static final image_rectification_menu_normal:I = 0x7f02007f

.field public static final image_rectification_menu_normal_repeat:I = 0x7f020080

.field public static final image_rectification_normal:I = 0x7f020081

.field public static final image_rectification_redo:I = 0x7f020082

.field public static final image_rectification_rotate_anticlockwise:I = 0x7f020083

.field public static final image_rectification_rotate_clockwise:I = 0x7f020084

.field public static final image_rectification_selector:I = 0x7f020085

.field public static final img:I = 0x7f020086

.field public static final input_bg:I = 0x7f020087

.field public static final input_bg_pressed:I = 0x7f020088

.field public static final input_bg_selected:I = 0x7f020089

.field public static final input_bg_selector:I = 0x7f02008a

.field public static final item_mail_share:I = 0x7f02008b

.field public static final key_bg:I = 0x7f02008c

.field public static final key_bg_pressed:I = 0x7f02008d

.field public static final key_bg_selector:I = 0x7f02008e

.field public static final label_bg:I = 0x7f02008f

.field public static final launch_border:I = 0x7f020090

.field public static final launch_icon:I = 0x7f020091

.field public static final left_rotate:I = 0x7f020092

.field public static final light_gray_bg:I = 0x7f020093

.field public static final list_item_bg_selector:I = 0x7f020094

.field public static final login:I = 0x7f020095

.field public static final login_bubble:I = 0x7f020096

.field public static final login_input_bg:I = 0x7f020097

.field public static final login_input_bg_pressed:I = 0x7f020098

.field public static final login_input_bg_selected:I = 0x7f020099

.field public static final login_input_bg_selector:I = 0x7f02009a

.field public static final login_logo:I = 0x7f02009b

.field public static final logo_transparent:I = 0x7f02009c

.field public static final logout:I = 0x7f02009d

.field public static final long_logo:I = 0x7f02009e

.field public static final long_logo_pressed:I = 0x7f02009f

.field public static final long_logo_selector:I = 0x7f0200a0

.field public static final mail_share_add_1:I = 0x7f0200a1

.field public static final mail_share_add_2:I = 0x7f0200a2

.field public static final mail_share_btn_add_selector:I = 0x7f0200a3

.field public static final mail_share_btn_send_selector:I = 0x7f0200a4

.field public static final mail_share_edit_bg:I = 0x7f0200a5

.field public static final mail_toast_bg:I = 0x7f0200a6

.field public static final more:I = 0x7f0200a7

.field public static final new_note:I = 0x7f0200a8

.field public static final new_note_bg:I = 0x7f0200a9

.field public static final new_note_bg_pressed:I = 0x7f0200aa

.field public static final new_note_camera:I = 0x7f0200ab

.field public static final new_note_d:I = 0x7f0200ac

.field public static final new_note_divider:I = 0x7f0200ad

.field public static final new_note_doodle:I = 0x7f0200ae

.field public static final new_note_gallary:I = 0x7f0200af

.field public static final new_note_handwrite:I = 0x7f0200b0

.field public static final new_note_large:I = 0x7f0200b1

.field public static final new_note_large_pressed:I = 0x7f0200b2

.field public static final new_note_large_selector:I = 0x7f0200b3

.field public static final new_note_notebook:I = 0x7f0200b4

.field public static final new_notebook:I = 0x7f0200b5

.field public static final new_notebook_d:I = 0x7f0200b6

.field public static final notebook:I = 0x7f0200b7

.field public static final notebook_d:I = 0x7f0200b8

.field public static final notebooks_large:I = 0x7f0200b9

.field public static final notebooks_large_pressed:I = 0x7f0200ba

.field public static final notebooks_large_selector:I = 0x7f0200bb

.field public static final particle:I = 0x7f0200bc

.field public static final pause:I = 0x7f0200bd

.field public static final pdf:I = 0x7f0200be

.field public static final pin_input_bg:I = 0x7f0200bf

.field public static final pin_label_bg:I = 0x7f0200c0

.field public static final pin_label_red_bg:I = 0x7f0200c1

.field public static final pinlock_container_bg:I = 0x7f0200c2

.field public static final play:I = 0x7f0200c3

.field public static final ppt:I = 0x7f0200c4

.field public static final preview:I = 0x7f0200c5

.field public static final progress_drawable:I = 0x7f0200c6

.field public static final progressbar:I = 0x7f0200c7

.field public static final rar:I = 0x7f0200c8

.field public static final recommend:I = 0x7f0200c9

.field public static final record:I = 0x7f0200ca

.field public static final refresh:I = 0x7f0200cb

.field public static final repick:I = 0x7f0200cc

.field public static final resource:I = 0x7f0200cd

.field public static final resource_bg:I = 0x7f0200ce

.field public static final resource_bg_pressed:I = 0x7f0200cf

.field public static final resource_list_divider:I = 0x7f0200d0

.field public static final retake:I = 0x7f0200d1

.field public static final right_rotate:I = 0x7f0200d2

.field public static final save_note_bg:I = 0x7f0200d3

.field public static final save_note_bg_pressed:I = 0x7f0200d4

.field public static final search:I = 0x7f0200d5

.field public static final search_button:I = 0x7f0200d6

.field public static final search_button_pressed:I = 0x7f0200d7

.field public static final search_button_selector:I = 0x7f0200d8

.field public static final search_d:I = 0x7f0200d9

.field public static final search_gray:I = 0x7f0200da

.field public static final search_input_bg:I = 0x7f0200db

.field public static final search_input_left:I = 0x7f0200dc

.field public static final search_input_middle:I = 0x7f0200dd

.field public static final search_input_right:I = 0x7f0200de

.field public static final search_logo:I = 0x7f0200df

.field public static final section_label:I = 0x7f0200e0

.field public static final seek_progress:I = 0x7f0200e1

.field public static final seek_progress_bg:I = 0x7f0200e2

.field public static final seek_thum:I = 0x7f0200e3

.field public static final seekbar_progress:I = 0x7f0200e4

.field public static final seekbar_progress_bg:I = 0x7f0200e5

.field public static final setting:I = 0x7f0200e6

.field public static final shufa2:I = 0x7f0200e7

.field public static final shufa4_alpha:I = 0x7f0200e8

.field public static final status_pause:I = 0x7f0200e9

.field public static final status_recording:I = 0x7f0200ea

.field public static final status_recording2:I = 0x7f0200eb

.field public static final stop:I = 0x7f0200ec

.field public static final sync:I = 0x7f0200ed

.field public static final sync_large:I = 0x7f0200ee

.field public static final sync_large_pressed:I = 0x7f0200ef

.field public static final sync_large_selector:I = 0x7f0200f0

.field public static final text_note:I = 0x7f0200f1

.field public static final title_filler:I = 0x7f0200f2

.field public static final title_long_logo:I = 0x7f0200f3

.field public static final title_long_logo_selector:I = 0x7f0200f4

.field public static final transparent_button_bg:I = 0x7f0200f5

.field public static final transparent_button_bg_pressed:I = 0x7f0200f6

.field public static final transparent_button_bg_selector:I = 0x7f0200f7

.field public static final txt:I = 0x7f0200f8

.field public static final unknow:I = 0x7f0200f9

.field public static final v_divider:I = 0x7f0200fa

.field public static final v_divider_d:I = 0x7f0200fb

.field public static final video:I = 0x7f0200fc

.field public static final wb:I = 0x7f0200fd

.field public static final wb_delete:I = 0x7f0200fe

.field public static final wg_divider:I = 0x7f0200ff

.field public static final wg_record:I = 0x7f020100

.field public static final widget_bg:I = 0x7f020101

.field public static final widget_bg_left:I = 0x7f020102

.field public static final widget_bg_left_pressed:I = 0x7f020103

.field public static final widget_bg_left_selector:I = 0x7f020104

.field public static final widget_bg_pressed:I = 0x7f020105

.field public static final widget_bg_right:I = 0x7f020106

.field public static final widget_bg_right_pressed:I = 0x7f020107

.field public static final widget_bg_right_selector:I = 0x7f020108

.field public static final widget_bg_selector:I = 0x7f020109

.field public static final widget_divider_d:I = 0x7f02010a

.field public static final word:I = 0x7f02010b

.field public static final xxx:I = 0x7f02010c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
